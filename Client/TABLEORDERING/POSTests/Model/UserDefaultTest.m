//
//  UserDefaultTest.m
//  POS
//
//  Created by Nha Duong Cong on 10/17/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UserDefaultModel.h"

@interface UserDefaultTest : XCTestCase

@end

@implementation UserDefaultTest{
    NSUserDefaults *_userDefault;
}

- (void)setUp
{
    [super setUp];
    _userDefault=[NSUserDefaults standardUserDefaults];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testUserDefault
{
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"congacon" forKey:@"testK"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [UserDefaultModel removeAllKeyUserDefault];
        NSString *congacon=[[NSUserDefaults standardUserDefaults] objectForKey:@"testK"];
        XCTAssert(congacon == nil, "not delete key when removeAllkey");
    }
    {
        NSString *urlServerCouchbase =@"http://sync.vn";
        [UserDefaultModel saveServerCouchbaseURL:urlServerCouchbase];
        XCTAssert([[UserDefaultModel getServerCouchbaseURL] isEqualToString:urlServerCouchbase], "save url server couchbase error");
        
        urlServerCouchbase =@"http://sync_data.vn";
        [UserDefaultModel saveServerCouchbaseURL:urlServerCouchbase];
        XCTAssert([[UserDefaultModel getServerCouchbaseURL] isEqualToString:urlServerCouchbase], "save url server couchbase error");
        
    }
    {
        BOOL firstSyncCompleted=YES;
        [UserDefaultModel saveFirstSyncCompleted:firstSyncCompleted];
        XCTAssert([UserDefaultModel wasFirstSyncCompleted]==firstSyncCompleted , "save setup first sync completed error");
        
        firstSyncCompleted=NO;
        [UserDefaultModel saveFirstSyncCompleted:firstSyncCompleted];
        XCTAssert([UserDefaultModel wasFirstSyncCompleted]==firstSyncCompleted , "save setup first sync completed error");
        
    }
    {
        NSString *tokenAuth =@"22321";
        [UserDefaultModel saveTokenAuthentication:tokenAuth];
        XCTAssert([[UserDefaultModel getTokenAuthentication] isEqualToString:tokenAuth], "save tokenAuth error");
        
        tokenAuth =@"3242";
        [UserDefaultModel saveTokenAuthentication:tokenAuth];
        XCTAssert([[UserDefaultModel getTokenAuthentication] isEqualToString:tokenAuth], "save tokenAuth error");
    }
    {
        NSString *valueRandom=[self valueRandom];
        NSDictionary *dic=@{@"key": @"value",
                           @"key2":valueRandom};
        [UserDefaultModel saveMerchantSyncInfor:dic];
        NSDictionary *regetDic=[UserDefaultModel getMerchantSyncInfor];
        XCTAssert([self compareDic:dic andDic:regetDic], "data set merchantSyncInfor error");
    }


}
-(NSString*)valueRandom{
    return [NSString stringWithFormat:@"%i",arc4random()];
}
-(BOOL)compareDic:(NSDictionary*)dic1 andDic:(NSDictionary*)dic2{
    if (dic1.count==dic2.count) {
        for (NSString *key in dic1.allKeys) {
            if (![dic1[key] isEqualToString:dic2[key]]) {
                return NO;
            }
        }
        return YES;
    }else{
        return NO;
    }
}

@end

//
//  UtilTest.m
//  POS
//
//  Created by Nha Duong Cong on 10/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
/*
 
 Test some method in some class Util
 */
#import <XCTest/XCTest.h>
#import "SyncingListener.h"
#import "MBProgressHUDListener.h"
#import "LanguageUtil.h"
#import "TapListener.h"
#import "NSString+Util.h" 
#import "PrinterManagement.h"

@interface UtilTest : XCTestCase

@end

@implementation UtilTest{
    UIView *view;
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSyncListener
{
    [SyncingListener initialize];
    [SyncingListener removeAllSyncingListerner];
    SyncingListener *syncingL=[[SyncingListener alloc] initWithBeginBlock:^{
        NSLog(@"beginblock");
    } endBlock:^(NSError *pullError, NSError *pushError) {
        NSLog(@"endblock");
    }];
    XCTAssertNotNil(syncingL, "syncing Listener nil");
    XCTAssertNotNil(syncingL.beginBlock, "Begin Block Syncing Listener Nil");
    XCTAssertNotNil(syncingL.endBlock, "End block Syncing Listerner Nil");
    XCTAssert([SyncingListener allSyncingListener].count==1, "SyncingListener not add to List");
    [syncingL performSelector:@selector(syncManagerEndSyncing:pullError:pushError:) withObject:nil withObject:nil];
    XCTAssert([SyncingListener allSyncingListener].count==0, "syncing listener not remove when endSyncing");
    
    SyncingListener *sycn= [[SyncingListener alloc] initWithBeginBlock:^{
        NSLog(@"beginblock1");
    } endBlock:^(NSError *pullError, NSError *pushError) {
        NSLog(@"endblock2");
    }];
    if (sycn) {
        XCTAssert([SyncingListener allSyncingListener].count==1, "not add syncListener to List");
        [SyncingListener removeAllSyncingListerner];
        XCTAssert([SyncingListener allSyncingListener].count==0, "Not remove all syncListener");
    }
}
-(void)testProgressHUDListener{
    [MBProgressHUDListener initialize];
    [MBProgressHUDListener removeAllHubListener];
    MBProgressHUDListener *listerner=[[MBProgressHUDListener alloc] initWithHubHidenBlock:^{
        NSLog(@"hiden block");
    }];
    XCTAssertNotNil(listerner, "sycing listerner nil");
    XCTAssertNotNil(listerner.hubHidenBlock, "Hiden block nill");
    XCTAssert([MBProgressHUDListener allHubListener].count==1, "not add listener to List");
    [listerner performSelector:@selector(hudWasHidden:) withObject:nil];
    XCTAssert([MBProgressHUDListener allHubListener].count==0, "not remove listener from list");
    MBProgressHUDListener *hubl= [[MBProgressHUDListener alloc] initWithHubHidenBlock:^{
        NSLog(@"hiden block1");
    }];
    MBProgressHUDListener *hubl2=[[MBProgressHUDListener alloc] initWithHubHidenBlock:^{
        NSLog(@"hiden block2");
    }];
    if (hubl&&hubl2) {
        XCTAssert([MBProgressHUDListener allHubListener].count==2, "not add listener to list");
        [MBProgressHUDListener removeAllHubListener];
        XCTAssert([MBProgressHUDListener allHubListener].count==0, "not remove all listener from list");
    }
}
-(void)testTapListener{
    [TapListener initialize];
    [TapListener removeAllTapListener];
    view=[[UIView alloc] init];
    TapListener *tapLis=[[TapListener alloc] initWithView:view tappedBlock:^{
        NSLog(@"tapped block");
    }];
    XCTAssertNotNil(tapLis, "tap listener nil");
    XCTAssertNotNil(tapLis.tappedBlock, "tapped block nil");
    XCTAssert([TapListener allTapListener].count==1, "not add tap listener to list");
    [tapLis performSelector:@selector(tappedToView)];
    XCTAssert([TapListener allTapListener].count==0, "not remove listener from list when tapped");
    TapListener *tap=[[TapListener alloc] initWithView:view tappedBlock:^{
        NSLog(@"tapped block 1");
    }];
    TapListener *tap2=[[TapListener alloc] initWithView:view tappedBlock:^{
        NSLog(@"tapped block 2");
    }];
    if (tap && tap2) {
        XCTAssert([TapListener allTapListener].count==2, "not add tap listener to list");
        [TapListener removeAllTapListener];
        XCTAssert([TapListener allTapListener].count==0, "not remove all listener from list");
    }
}
-(LanguageUtil*)getSharedLanguageUtil{
    return [LanguageUtil sharedLanguageUtil];
}
-(void)testLanguage{
    NSDictionary *dic=[[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"EnglishKey" ofType:@"plist"]];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"languageType"];
    LanguageUtil *language =[self getSharedLanguageUtil];
    XCTAssert(language!=nil, "language sharedInsance nil");
    XCTAssert(dic.allKeys.count ==  language.dictLanguageKey.allKeys.count, "count language not true");
    for (NSString *key in dic) {
        XCTAssert([dic[key] isEqualToString:language.dictLanguageKey[key]], " key language not true");
    }
    XCTAssert([dic isEqualToDictionary:language.dictLanguageKey], "language init not true");
    [language setLanguageWithType:LanguageTypeVietnamese];
    NSDictionary *dicV=[[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"VietnameseKey" ofType:@"plist"]];
    XCTAssert(dicV.allKeys.count ==  language.dictLanguageKey.allKeys.count, "count language not true");
    for (NSString *key in dicV) {
        XCTAssert([dicV[key] isEqualToString:language.dictLanguageKey[key]], " key language not true");
    }
    
    // check key language
    NSArray *fileNames=@[@"EnglishKey",@"VietnameseKey"];
    NSMutableArray *listDictLanguages=[[NSMutableArray alloc] init];
    for (NSString * fileN in fileNames) {
        NSString *path=[[NSBundle mainBundle] pathForResource:fileN ofType:@"plist"];
        NSDictionary *dic=[[NSDictionary alloc] initWithContentsOfFile:path];
        XCTAssert(dic!=nil, "Language %@ nil",fileN);
        if (dic) {
            [listDictLanguages addObject:dic];
        }
    }
    NSDictionary *languageStandard=[listDictLanguages firstObject];
    for (int i=1;i<listDictLanguages.count;i++) {
        NSDictionary *ortherLanguage=[listDictLanguages objectAtIndex:i];
        XCTAssert(languageStandard.count == ortherLanguage.count, "number of key not equal number of key in language Standard");
        //check all Key in language
        for (NSString *key in languageStandard.keyEnumerator) {
            if (ortherLanguage[key]==nil) {
                XCTAssert(NO, "orther language not have key %@",key);
                break;
            }
        }
        
    }
}
-(void)testValidateEmail{
    //
    NSArray *arrayEmail=@[@"congnha@gmail.com",@"cong2@gmail.com",@"congacon@vn.com"];
    for (NSString *email in arrayEmail) {
        XCTAssert([email isEmailFormat], "validate email fail");
    };
    
    NSArray *arrayNotEmail=@[@"54",@"coh",@"cong@",@"@con.com",@"@gmail.com",@"congnha@gqs@vn"];
    for (NSString *notEmail in arrayNotEmail) {
        XCTAssert(![notEmail isEmailFormat], "validate email fail");
    };
}
-(void)testEncodeUrl{
    NSString *string=@"%\"',;/\\ ?$@&*+={}[]<>";//not change
    NSString *encode=[string encodeURL];
    NSString *decode=[encode decodeURL];
    XCTAssert([encode isEqualToString:@"%25%22%27%2C%3B%2F%5C%20%3F%24%40%26*%2B%3D%7B%7D%5B%5D%3C%3E"], "encode Url error");
    XCTAssert([string isEqualToString:decode], "decode url erro");
}
-(void)testUInterger{
    NSArray *arrayUinteger =@[@"2334",@"0",@"023",@"534"];
    for (NSString *str in arrayUinteger) {
        XCTAssert([str isUIntegerFormat], "validate unsign interger fail");
    }
    NSArray *arrayNotUinteger =@[@"2334.3",@"0.5",@",0e23",@"e534"];
    for (NSString *str in arrayNotUinteger) {
        XCTAssert(![str isUIntegerFormat], "validate unsign interger fail");
    }
}
-(void)testTKAmout{
    NSArray *listTKAmount=@[@"",@".",@".23",@".2",@"1.3",@"2.22",@"232.23",@"12."];
    for (NSString *amount in listTKAmount) {
        XCTAssert([amount isTKAmountFormat], "tk amount format fail");
    }
    NSArray *listNotTKAmount=@[@"w",@".we",@".123",@"w.2",@"1,3",@"232.3a",@".23d",@"12.t"];
    for (NSString *amount in listNotTKAmount) {
        XCTAssert(![amount isTKAmountFormat], "tk amount format fail");
    }
}

-(void)testDate{
    NSDate *date =[NSDate dateWithYear:2014 month:12 day:31 timeZone:[NSTimeZone localTimeZone]];
    NSDate *dateBefore3 =[NSDate tkDateStartBeforeIn:3 toCurrentDate:date];
    XCTAssert([[dateBefore3 getStringWithFormat:@"yyyyMMdd"] isEqualToString:@"20140930"], "date start Before in fail");
    
    NSDate *dateP =[NSDate dateWithYear:2014 month:12 day:31 timeZone:[NSTimeZone localTimeZone]];
    NSDate *datePeriod3 =[NSDate tkDateStartPeriodIn:3 toCurrentDate:dateP];
    XCTAssert([[datePeriod3 getStringWithFormat:@"yyyyMMdd"] isEqualToString:@"20141001"], "date start Period in fail");
}
-(void)testIpAddressFormat{
    //test prefix Ip Address
    
    //test Ip Address format full
    
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.

}
@end

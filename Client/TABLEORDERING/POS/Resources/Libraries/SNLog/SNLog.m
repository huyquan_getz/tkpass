#import "SNLog.h"
#import "File.h"


@implementation SNLog


#pragma mark Singleton Methods
static SNLog *sharedInstance;
+ (SNLog *) logManager {
	if (sharedInstance == nil) {
		sharedInstance = [[SNLog alloc] init];
	}
	
	return sharedInstance;
}

+ (id) allocWithZone:(NSZone *)zone {
	if (sharedInstance == nil) {
		sharedInstance = [super allocWithZone:zone];
	}	
	return sharedInstance;
}


+ (void) Log: (NSString *) format, ... {
	SNLog *log = [SNLog logManager];
	va_list args;
	va_start(args, format);
	NSString *logEntry = [[NSString alloc] initWithFormat:format arguments:args];
	[log writeToLogs: 1 data:logEntry];
}
+ (void) LogFile: (NSString *) format, ... {
    SNLog *log = [SNLog logManager];
    va_list args;
    va_start(args, format);
    NSString *logEntry = [[NSString alloc] initWithFormat:format arguments:args];
    [log writeToLogs: LogLevelFile data:logEntry];
}

+ (void) Log: (NSInteger) logLevel data: (NSString *) format, ... {
	SNLog *log = [SNLog logManager];
	va_list args;
	va_start(args, format);
	NSString *logEntry = [[NSString alloc] initWithFormat:format arguments:args];
	[log writeToLogs:logLevel data:logEntry];
	
}
#pragma mark Instance Methods

- (void) writeToLogs: (NSInteger) logLevel data: (NSString *) logEntry {
	NSString *formattedLogEntry = [self formatLogEntry:logLevel data:logEntry];
	for (NSObject<SNLogStrategy> *logger in logStrategies) {
		if (logLevel >= logger.logAtLevel) {
			[logger writeToLog: logLevel data: formattedLogEntry];
		}
	}
	
}

- (id) init {
	if (self = [super init]) {
		SNConsoleLogger *consoleLogger = [[SNConsoleLogger alloc] init];
		consoleLogger.logAtLevel = 0;
		[self addLogStrategy:consoleLogger];
        
        SNFileLogger *fileLogger = [[SNFileLogger alloc] initWithPathAndSize:[File logFolder] size:2*1024*1024];
        fileLogger.logAtLevel = LogLevelFile;
        [self addLogStrategy:fileLogger];
		
		return self;
	 } else {
		 return nil;
	 }

	
}

- (void) addLogStrategy: (id<SNLogStrategy>) logStrategy {
	if (logStrategies == nil) {
		logStrategies = [[NSMutableArray alloc] init];
	}
	
	[logStrategies addObject: logStrategy];
}


- (NSString *) formatLogEntry: (NSInteger) logLevel data: (NSString *) logData {
	NSDate *now = [NSDate date];

	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
	NSString *formattedString = [dateFormatter stringFromDate:now];
	return [NSString stringWithFormat:@"[%ld] - %@ - %@",(long)logLevel, formattedString, logData];
}

@end







@implementation SNConsoleLogger
@synthesize logAtLevel;

- (void) writeToLog:(NSInteger) logLevel data:(NSString *)logData {
	printf("%s\r\n", [logData UTF8String]);
}

@end









#define keyFileLogUserDefault @"keyFileLog"
#define keyLogFileName1 @"log_file1"
#define keyLogFileName2 @"log_file2"
@implementation SNFileLogger

@synthesize logAtLevel;

- (id) initWithPathAndSize: (NSString *) folderPath size: (NSInteger) truncateSize {
	logAtLevel = 2;
	if (self = [super init]) {
        // have 2 log file
        logFilePath=[[NSUserDefaults standardUserDefaults] objectForKey:keyFileLogUserDefault];
        if (logFilePath==nil || !([logFilePath hasSuffix:keyLogFileName1] || [logFilePath hasSuffix:keyLogFileName2])) {
            logFilePath = [folderPath stringByAppendingPathComponent:keyLogFileName1];
            [[NSUserDefaults standardUserDefaults] setObject:logFilePath forKey:keyFileLogUserDefault];
        }
		truncateBytes = truncateSize;
		return self;
	} else {
		return nil;
	}
}

- (void) writeToLog:(NSInteger) logLevel data:(NSString *)logData {
	NSData *logEntry =  [[logData stringByAppendingString:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding];
	NSFileManager *fm = [[NSFileManager alloc] init];

	if(![fm fileExistsAtPath:logFilePath]) {
		[fm createFileAtPath:logFilePath contents:logEntry attributes:nil];
	} else {
		NSDictionary *attrs = [fm attributesOfItemAtPath:logFilePath error:nil];
		NSFileHandle *file = [NSFileHandle fileHandleForWritingAtPath:logFilePath];
		if ([attrs fileSize] > truncateBytes) {
            if ([logFilePath hasSuffix:keyLogFileName1]) {
                logFilePath =[logFilePath stringByReplacingOccurrencesOfString:keyLogFileName1 withString:keyLogFileName2];
            }else{
                logFilePath =[logFilePath stringByReplacingOccurrencesOfString:keyLogFileName2 withString:keyLogFileName1];
            }
            [[NSUserDefaults standardUserDefaults] setObject:logFilePath forKey:keyFileLogUserDefault];
            file =[NSFileHandle fileHandleForWritingAtPath:logFilePath];
			[file truncateFileAtOffset:0];
		}
		
		[file seekToEndOfFile];
		[file writeData:logEntry];
		[file closeFile];
	}
}
@end

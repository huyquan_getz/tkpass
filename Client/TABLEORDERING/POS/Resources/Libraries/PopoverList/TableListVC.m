//
//  TableListVC.m
//  POS
//
//  Created by Cong Nha Duong on 1/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define CellListIdentify @"cellList"
#import "TableListVC.h"
#import "TableCellList.h"

@interface TableListVC ()

@end

@implementation TableListVC
-(instancetype)initWithListString:(NSArray *)listString_ messageNoList:(NSString *)messageNoList_{
    if (self =[super init]) {
        _heighCell=50;
        listString=listString_;
        messageNoList=messageNoList_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellList" bundle:nil] forCellReuseIdentifier:CellListIdentify];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    if (listString.count==0) {
        _lbNoList.hidden=NO;
        _tbvMain.hidden=YES;
    }else{
        _lbNoList.hidden=YES;
        _tbvMain.hidden=NO;
    }
    _lbNoList.text=messageNoList;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return listString.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableCellList *cell=[tableView dequeueReusableCellWithIdentifier:CellListIdentify];
    [cell setValueDefault];
    if (_colorLine) {
        cell.vLine.backgroundColor=_colorLine;
    }
    if (_colorText) {
        cell.lbTitle.textColor=_colorText;
    }
    if (_fontText) {
        cell.lbTitle.font=_fontText;
    }
   
    cell.lbTitle.text=listString[indexPath.row];
    if (indexPath.row==listString.count-1) {
        cell.vLine.hidden=YES;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_delegate) {
        [_delegate tableList:self selectedCellIndex:indexPath.row];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

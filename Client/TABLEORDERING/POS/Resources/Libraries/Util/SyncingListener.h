//
//  SyncingListener.h
//  POS
//
//  Created by Nha Duong Cong on 10/22/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
/*
  Have SyncManageDelegate for begin syncing and end syncing
  SyncingListener add when created and remove when end block;
 */

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface SyncingListener : NSObject<SyncManagerDelegate>
@property (strong,nonatomic) void (^beginBlock)(void);
@property (strong,nonatomic) void (^endBlock)(NSError *pullError, NSError *pushError);

-(id)initWithBeginBlock:(void (^)())beginBl endBlock:(void (^)(NSError *pullError,NSError *pushError))endBl;
+(void)removeAllSyncingListerner;
+(NSArray*)allSyncingListener;
@end

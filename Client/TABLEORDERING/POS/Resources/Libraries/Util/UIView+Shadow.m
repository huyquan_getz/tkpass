//
//  UIView+Shadow.m
//  POS
//
//  Created by Nha Duong Cong on 12/12/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "UIView+Shadow.h"
#import "Constant.h"

@implementation UIView (Shadow)
-(void)setViewCoverDefault{
    [self setBackgroundColor:[UIColor colorWithRed:50/255.0 green:50/255.0 blue:50/255.0 alpha:1]];
    [self setAlpha:0.4];
}
@end

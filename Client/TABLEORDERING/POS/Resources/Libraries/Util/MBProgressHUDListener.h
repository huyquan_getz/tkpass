//
//  MBProgressHUDListener.h
//  POS
//
//  Created by Nha Duong Cong on 10/22/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface MBProgressHUDListener : NSObject<MBProgressHUDDelegate>{
    
}
@property (strong,nonatomic)void (^hubHidenBlock)(void);
-(id)initWithHubHidenBlock:(void (^)())hubHidenBl;
+(void)removeAllHubListener;
+(NSArray*)allHubListener;
@end

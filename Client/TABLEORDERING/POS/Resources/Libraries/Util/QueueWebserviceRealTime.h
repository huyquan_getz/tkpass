//
//  QueueWebserviceRealTime.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 4/13/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkUtil.h"
#import "DataWebsevice.h"

@interface QueueWebserviceRealTime : NSObject<NetworkDelegate>{
    NSString *filePathWatingRealTime;
}
+(QueueWebserviceRealTime*)sharedQueue;
-(void)addDataToQueue:(DataWebsevice*)dataWebservice;
-(void)reCallUpdating;
-(void)cancelAllOperationWebserviceRealTime;
@end

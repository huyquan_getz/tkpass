//
//  UIImage+Util.m
//  POS
//
//  Created by Nha Duong Cong on 11/4/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "UIImage+Util.h"
#import "Constant.h"
@implementation UIImage (Util)
+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    // Create a 1 by 1 pixel context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [color setFill];
    UIRectFill(rect);   // Fill it with your color
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
+(UIImage*)imageCornerTopWithColor:(UIColor*)color frame:(CGRect)frame corner:(NSInteger)corner{
    UIView *view=[[UIView alloc] initWithFrame:frame];
    view.backgroundColor=[UIColor clearColor];
    frame.size.height+=corner;
    frame.origin=CGPointZero;
    UIView *view1=[[UIView alloc] initWithFrame:frame];
    view1.backgroundColor=color;
    view1.layer.cornerRadius=corner;
    view1.clipsToBounds=YES;
    [view addSubview:view1];
    return [UIImage imageWithUIView:view];
}
//Add text to UIImage
/*
+(UIImage *)addText:(UIImage *)img text:(NSString *)text1 point:(CGPoint)point{
    int w = img.size.width;
    int h = img.size.height;
    //lon = h - lon;
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//    CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
    CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGBitmapAlphaInfoMask);
    
    CGContextDrawImage(context, CGRectMake(0, 0, w, h), img.CGImage);
    CGContextSetRGBFillColor(context, 0.0, 0.0, 1.0, 1);
	
    char* text	= (char *)[text1 cStringUsingEncoding:NSUTF8StringEncoding];// "05/05/09";
    CGContextSelectFont(context, "Helvetica Neue", 28, kCGEncodingMacRoman);
    CGContextSetTextDrawingMode(context, kCGTextFill);
    CGContextSetRGBFillColor(context, 0, 0, 0, 1);
	
    
    //rotate text
//    CGContextSetTextMatrix(context, CGAffineTransformMakeRotation( -M_PI/4 ));
    CGContextShowTextAtPoint(context, point.x , point.y, text, strlen(text));
	
    CGImageRef imageMasked = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
	
    return [UIImage imageWithCGImage:imageMasked];
}
//Add text to UIImage
+(UIImage *)convertTextToImage:(NSString*)text1 point:(CGPoint)point imageSize:(CGSize)size{
    int w = size.width;
    int h = size.height;
    //lon = h - lon;
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
    
    CGContextDrawImage(context, CGRectMake(0, 0, w, h), NULL);
    CGContextSetRGBFillColor(context, 0.0, 0.0, 1.0, 1);
    
    char* text	= (char *)[text1 cStringUsingEncoding:NSUTF8StringEncoding];// "05/05/09";
    CGContextSelectFont(context, "Helvetica Neue", 28, kCGEncodingMacRoman);
    CGContextSetTextDrawingMode(context, kCGTextFill);
    CGContextSetRGBFillColor(context, 0, 0, 0, 1);
    
    
    //rotate text
    //    CGContextSetTextMatrix(context, CGAffineTransformMakeRotation( -M_PI/4 ));
    CGContextShowTextAtPoint(context, point.x , point.y, text, strlen(text));
    
    CGImageRef imageMasked = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    return [UIImage imageWithCGImage:imageMasked];
}

+(UIImage *)imageDefaultWithSortName:(NSString *)sortName{
    if (sortName==nil || sortName.length==0) {
        return [self noImage];
    }
    NSString *stringShow=[sortName uppercaseString];
    if (stringShow.length > 3) {
        stringShow =[stringShow substringToIndex:3];
    }
    CGSize sizeText = [stringShow sizeWithAttributes:@{NSFontAttributeName:tkFontMainWithSize(28)}];
    CGPoint point = CGPointMake((80 - sizeText.width)/2 , 25);
    UIImage *image=[UIImage convertTextToImage:stringShow point:point imageSize:CGSizeMake(80, 80)];
    return image;
}
 */
+(UIImage *)noImage{
    return [UIImage imageNamed:@"noImage.png"];
}
+(UIImage *)imageWithUIView:(UIView *)view{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}
-(UIImage *)resizeImageWithSizeMax:(CGSize)size{
    return [self resizeImageWithSizeMax:size cropSquare:NO];
}
- (UIImage *) resizeImageWithSizeMax:(CGSize)size cropSquare:(BOOL)cropSquare
{  CGFloat actualHeight = self.size.height;
    CGFloat actualWidth = self.size.width;
    UIImage *orginalImage=self;
    
    if(actualWidth > size.width && actualHeight > size.height)
    {
        CGFloat tmp;
        if((actualWidth/actualHeight)<(size.width/size.height))
        {
            tmp=actualWidth;
            actualWidth=size.width;
            actualHeight=size.height*(actualHeight/tmp);
        }else
        {
            tmp=actualHeight;
            actualHeight=size.height;
            actualWidth=size.width*(actualWidth/tmp);
            
        }
        
        // scale
        CGRect rect = CGRectMake(0.0,0.0,actualWidth,actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        [self drawInRect:rect];
        orginalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    // crop square image;
    if (cropSquare) {
//        CGRect square=CGRectMake(0, 0, size.width, size.height);
        NSInteger min=MIN(actualHeight, actualWidth);
        CGRect square=CGRectMake(0, 0, min,min);
        square.origin.x=(actualWidth-min)/2;
        square.origin.y=(actualHeight-min)/2;
        CGImageRef imageRef=CGImageCreateWithImageInRect(orginalImage.CGImage, square);
        orginalImage=[UIImage imageWithCGImage:imageRef];
    }
    return orginalImage;
}

@end

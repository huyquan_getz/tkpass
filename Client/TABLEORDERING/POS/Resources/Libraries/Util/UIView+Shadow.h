//
//  UIView+Shadow.h
//  POS
//
//  Created by Nha Duong Cong on 12/12/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Shadow)
-(void)setViewCoverDefault;
@end

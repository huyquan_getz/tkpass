//
//  QueueSendMail.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 4/3/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EmailData.h"
#import "NetworkUtil.h"

@interface QueueSendMail : NSObject<NetworkDelegate>{
    NSString *filePathSaveReceiptWatingSend;
}
+(QueueSendMail*)sharedQueue;
-(void)addEmailToQueue:(EmailData*)emailData;
-(void)reSendEmails;
-(void)cancelAllOperationSendMail;
@end

//
//  CBLJSON+Util.h
//  POS
//
//  Created by Nha Duong Cong on 11/7/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <CouchbaseLite/CouchbaseLite.h>

@interface CBLJSON (Util)
+(BOOL)jsonNull:(id)object;
@end

//
//  LeftMenuController.m
//  POS
//
//  Created by Nha Duong Cong on 10/14/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define KeyCellTypeLeftMenu @"keyCellTypeLeftMenu"
#define HeightButtonManageRole 80
#import "LeftMenuController.h"
#import "Controller.h"
#import "TapListener.h"
#import "TKEmployee.h"
#import "UIImage+Util.h"
#import "TableCellLeftMenu.h"
#define CellLeftMenuIndentify @"cellLeftMenu"
@interface LeftMenuController ()

@end

@implementation LeftMenuController
+(instancetype)sharedLeftMenu{
    __strong static LeftMenuController *_leftMenuController;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _leftMenuController=[[LeftMenuController alloc] init];
    });
    return _leftMenuController;
};
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)init{
    if (self=[super init]) {
        typeCellSelected=CellLeftMenuTableOrdering;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [_tbvMain setBackgroundColor:[UIColor clearColor]];
    _vBgNavi.backgroundColor=tkColorMain;
    _lbName.backgroundColor=[UIColor clearColor];
    [self.view setBackgroundColor:tkColorMainBackground];
    CGRect frame=self.view.frame;
    frame.origin.y=HeightButtonManageRole;
    frame.size.height-=HeightButtonManageRole;
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellLeftMenu" bundle:nil] forCellReuseIdentifier:CellLeftMenuIndentify];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [_btLock setTitle:[[LanguageUtil sharedLanguageUtil] stringByKey:@"menu.bt.lock"] forState:UIControlStateNormal];
    CBLDocument *userLogin=[Controller getUserLoginedDoc];
    NSString *name=[TKEmployee getEmployeeName:userLogin];
    _lbName.text=name;
//    [_tbvMain reloadData];
    if (_tbvMain.indexPathForSelectedRow ==nil) {
        [_tbvMain selectRowAtIndexPath:[NSIndexPath indexPathForRow:typeCellSelected inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma TableViewDelegate
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellLeftMenu *cell=[tableView dequeueReusableCellWithIdentifier:CellLeftMenuIndentify];
    [cell setValueDefault];
    switch (indexPath.row) {
        case CellLeftMenuTableOrdering:
            cell.lbTitle.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"menu.text-table-ordering"];
            cell.imgISelected=[UIImage imageNamed:@"icon_tableorder_w"];
            cell.imgIDeselected=[UIImage imageNamed:@"icon_tableorder"];
            break;
        case CellLeftMenuSetting:
            cell.lbTitle.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"menu.text-settings"];
            cell.imgISelected=[UIImage imageNamed:@"icon_settings_w"];
            cell.imgIDeselected=[UIImage imageNamed:@"icon_settings"];
            break;
        default:
            break;
    }
    return cell;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (typeCellSelected == (CellLeftMenuType)indexPath.row) {
        [Controller showCenterViewWithCompletion:nil];
        return;
    }
    typeCellSelected=(CellLeftMenuType)indexPath.row;
    [self actionWithCellType:typeCellSelected];
}
- (IBAction)clickLock:(id)sender {
    LanguageUtil *languageKey=[LanguageUtil sharedLanguageUtil];
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"menu.ms-confirm-lock"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            [Controller showCashierLoginView];
        }
    }];
}

-(void)setSelectedCellType:(CellLeftMenuType)celltype withAction:(BOOL)action{
    typeCellSelected=celltype;
    if ([self isViewLoaded]) {
        [_tbvMain selectRowAtIndexPath:[NSIndexPath indexPathForRow:typeCellSelected inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    if (action) {
        [self actionWithCellType:typeCellSelected];
    }
}
-(void)actionWithCellType:(CellLeftMenuType)cellType{
    switch (cellType) {
        case CellLeftMenuTableOrdering:
            [Controller showTableOrdering];
            break;
        case CellLeftMenuSetting:
            [Controller showSettingMgt];
            break;
        default:
            break;
    }
}
@end

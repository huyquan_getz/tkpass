//
//  SlideMenuController.h
//  POS
//
//  Created by Nha Duong Cong on 10/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "MMDrawerController.h"

@interface SlideMenuController : MMDrawerController
//overide  // centerViewController alway UINavicontroller;
-(id)initWithCenterViewController:(UIViewController *)centerViewController leftDrawerViewController:(UIViewController *)leftDrawerViewController;
-(void)setCenterViewController:(UIViewController *)centerViewController withCloseAnimation:(BOOL)closeAnimated completion:(void (^)(BOOL))completion;
-(void)setCenterViewController:(UIViewController *)newCenterViewController withFullCloseAnimation:(BOOL)fullCloseAnimated completion:(void (^)(BOOL))completion;
//new
-(void)setCenterViewControllers:(NSArray*)centerViewControllers withCloseAnimation:(BOOL)closeAnimated completion:(void (^)(BOOL))completion;
-(void)setCenterViewControllers:(NSArray *)newCenterViewControllers withFullCloseAnimation:(BOOL)fullCloseAnimated completion:(void (^)(BOOL))completion;
@end

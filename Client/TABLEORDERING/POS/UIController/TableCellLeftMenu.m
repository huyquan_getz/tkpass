//
//  TableCellLeftMenu.m
//  POS
//
//  Created by Cong Nha Duong on 1/20/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableCellLeftMenu.h"
#import "Constant.h"

@implementation TableCellLeftMenu

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        _ivIndentify.image=_imgISelected;
        _lbTitle.textColor=[UIColor whiteColor];
    }else{
        if (_imgIDeselected) {
            _ivIndentify.image=_imgIDeselected;
        }else{
            _ivIndentify.image=_imgISelected;
        }
        _lbTitle.textColor=[UIColor blackColor];
    }
    [_ivIndentify setNeedsLayout];
    // Configure the view for the selected state
}
-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    if (highlighted) {
        if (self.selected) {
            _lbTitle.textColor=[UIColor blackColor];
        }else{
            _lbTitle.textColor=[UIColor grayColor];
        }
    }
}
-(void)setValueDefault{
    UIView *bgSelected=[[UIView alloc] init];
    [bgSelected setBackgroundColor:tkColorMainSelected];
    [super setSelectedBackgroundView:bgSelected];
    _vLine.backgroundColor=tkColorFrameBorder;
    _lbTitle.text=@"";
}
@end

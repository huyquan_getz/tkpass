//
//  PrintData.h
//  POS
//
//  Created by Hoang Van Quynh on 13/01/2015.
//  Copyright (c) Năm 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrintData : NSCoder
- (id)initWithImage:(UIImage*)image Height:(float)height Ip:(NSString*)ip Type:(int)type Name:(NSString*)name Infor:(NSDictionary*)dictInfor;
@property (nonatomic, copy, readwrite) NSString *strImage;
@property (nonatomic, copy, readwrite) NSString *width;
@property (nonatomic, copy, readwrite) NSString *height;
@property (nonatomic, copy, readwrite) NSString *ip;
@property (nonatomic, copy, readwrite) NSString *name;
@property (nonatomic, copy, readwrite) NSString *infor;
@property (nonatomic, copy, readwrite) NSString *type;
@property (nonatomic, copy, readwrite) NSString *status;
@property (nonatomic, copy, readwrite) NSString *index;
@end
//
//  CellProductInReceipt.h
//  POS
//
//  Created by Cong Nha Duong on 1/15/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
// 46  /80

#import <UIKit/UIKit.h>

@interface CellProductInReceipt : UIView
@property (strong,nonatomic) UILabel *lbQuantity;
@property (strong,nonatomic) UILabel *lbTitle;
@property (strong,nonatomic) UILabel *lbDiscount;
@property (strong,nonatomic) UILabel *lbAmountCrude;
@property (strong,nonatomic) UILabel *lbAmountDiscount;
-(instancetype)initWithQuantity:(NSString*)quantity_ title:(NSString*)title_ amount:(NSString*)amount;
-(instancetype)initWithQuantity:(NSString *)quantity_ title:(NSString *)title_ amount:(NSString *)amount discountTitle:(NSString*)discountTitle amountDiscount:(NSString*)amountDiscount;

-(void)setQuantity:(NSString*)quantity;
-(void)setTitle:(NSString*)title;
-(void)setAmountCrude:(NSString*)amountCrude;
-(void)setDiscount:(NSString*)discount;
-(void)setAmountDiscount:(NSString*)amountDiscount;
-(NSInteger)maxHeight;

@end

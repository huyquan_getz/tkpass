//
//  UserDefaultModel.h
//  POS
//
//  Created by Nha Duong Cong on 10/14/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface UserDefaultModel : NSObject

+(void)removeAllKeyUserDefault;
//data name for couchbase line
+(void)saveServerCouchbaseURL:(NSString*)serverURL;
+(NSString*)getServerCouchbaseURL;

+(void)saveAccountMerchant:(NSString*)documentID;
+(NSString*)getAccountMerchant;

+(void)saveFirstSyncCompleted:(BOOL)completed;
+(BOOL)wasFirstSyncCompleted;

+(void)saveTokenAuthentication:(NSString*)token;
+(NSString*)getTokenAuthentication;

+(void)saveLanguageType:(NSNumber*)languageType;
+(NSNumber*)getLanguageType;

+(void)saveMerchantSyncInfor:(NSDictionary*)merchantDataSync;
+(NSDictionary*)getMerchantSyncInfor;

+(void)saveStoreSelected:(NSString*)storeID;
+(NSString*)getStoreSelected;

+(void)saveUserLogined:(NSString*)documentID;
+(NSString*)getUserLogined;

+(void)saveRequestPincodeWhenEnterForeground:(BOOL)request;
+(BOOL)getRequestPincodeWhenEnterForeground;

+(void)saveTaxtServiceSetting:(NSString*)documentID;
+(NSString*)getTaxServiceSetting;

+(void)saveGeneralSetting:(NSString*)documentID;
+(NSString*)getGeneralSetting;

+(void)saveEncodeArchiver:(id)object forKey:(NSString*)key;
+(id)getDecodeArchiverObjectForKey:(NSString*)key;

+(void)saveTableOrderingSetting:(NSString *) documentID;
+(NSString *)getTableOrderingSetting ;

+(void)saveDeviceToken:(NSString*)deviceToken;
+(NSString*)getDeviceToken;

+(void)saveListDevicesSetting:(NSString*)listDevicesID;
+(NSString*)getListDevicesSetting;

+(void)saveApplicationNumberBadgeNotUpdate:(NSInteger)numberBadgeNotUpdate;
+(NSInteger)getApplicationNumberBadgeNotUpdate;
@end

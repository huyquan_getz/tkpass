//
//  TKGeneralSetting.h
//  POS
//
//  Created by Cong Nha Duong on 2/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TKItem.h"

@interface TKGeneralSetting : TKItem
+(NSString*)getMerchantUserID:(CBLDocument*)document;
+(NSString*)getMerchantUserOwner:(CBLDocument*)document;
+(NSString*)getCurrency:(CBLDocument*)document;
+(NSString*)getGST:(CBLDocument*)document;
@end

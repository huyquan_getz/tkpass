//
//  TKInventory.m
//  POS
//
//  Created by Cong Nha Duong on 2/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TKInventory.h"

@implementation TKInventory
+(NSString *)getAddressCombine:(CBLDocument *)document{
    NSMutableString *string=[[NSMutableString alloc] init];
    NSString *streeAddress=[[document.properties[@"streetAddress"] convertNullToNil] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *city=[[document.properties[@"city"] convertNullToNil]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *state=[[document.properties[@"state"] convertNullToNil]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *zipCode=[[document.properties[@"zipCode"] convertNullToNil]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *country=[[document.properties[@"country"] convertNullToNil]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (streeAddress.length>0) {
        [string appendString:streeAddress];
        [string appendString:@", "];
    }
    if (city.length>0) {
        [string appendString:city];
        [string appendString:@", "];
    }
    if (state.length>0) {
        [string appendString:state];
        [string appendString:@", "];
    }
    if (zipCode.length>0) {
        [string appendString:zipCode];
        [string appendString:@", "];
    }
    if (country.length>0) {
        [string appendString:country];
    }
    return string;
}
+(NSString *)getTimzone:(CBLDocument *)document{
    return document.properties[@"timeZone"];
}
+(NSString *)getName:(CBLDocument *)document{
    return [document.properties[tkKeyBusinessID] convertNullToNil];
}
+(NSString *)getDescription:(CBLDocument *)document{
    return [document.properties[tkKeyDescription] convertNullToNil];
}
+(NSString *)getContact:(CBLDocument *)document{
    return [document.properties[@"contact"] convertNullToNil];
}
@end

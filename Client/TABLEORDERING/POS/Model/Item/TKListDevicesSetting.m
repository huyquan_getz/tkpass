//
//  TKListDevicesSetting.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 4/6/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TKListDevicesSetting.h"
#import "Constant.h"

@implementation TKListDevicesSetting
+(NSArray *)getListDeviceAvailable:(CBLDocument *)document{
    NSArray *allDevices=[document.properties[@"arrayDevices"] convertNullToNil];
    NSMutableArray *devicesAvailable=[[NSMutableArray alloc] init];
    for (NSDictionary *device in allDevices) {
        if ([device[@"status"] boolValue]) {
            [devicesAvailable addObject:device];
        }
    }
    return devicesAvailable;
}
@end

//
//  TKStation.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/18/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TKItem.h"

@interface TKStation : TKItem
+(NSInteger)getStatusStation:(CBLDocument*)document;
+(BOOL)clearStation:(CBLDocument*)document;
+(NSDictionary*)customerHost:(CBLDocument*)document;
+(NSString*)orderId:(CBLDocument*)document;
@end

//
//  TKEmployee.m
//  POS
//
//  Created by Nha Duong Cong on 12/20/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TKEmployee.h"

@implementation TKEmployee
+(NSArray *)getArrayPermision:(CBLDocument *)document{
    return document.properties[tkKeyArrayPermission];
}
+(NSInteger)getEmployeeID:(CBLDocument *)document{
    return [document.properties[tkKeyEmployeeId] integerValue];
}
+(NSString*)getEmployeeName:(CBLDocument *)document{
    return document.properties[@"fullName"];
}
+(NSString *)getPincode:(CBLDocument *)document{
    return document.properties[tkKeyPinCode];
}
+(NSArray *)getRoles:(CBLDocument *)document storeIndex:(NSInteger)storeIndex{
    NSMutableArray *roles =[[NSMutableArray alloc] init];
    NSArray *arrPermission =[self getArrayPermision:document];
    for (NSDictionary *permission in arrPermission) {
        NSInteger currentIndex=-1;
        if ( [permission[tkKeyStore] convertNullToNil] && [[permission[tkKeyStore] objectForKey:@"index"]convertNullToNil]) {
            currentIndex=[[permission[tkKeyStore] objectForKey:@"index"] integerValue];
        }
        if (currentIndex== storeIndex) {
            [roles addObject:permission[tkKeyRole]];
        }
    }
    return roles;
}
+(BOOL)isMerchant:(CBLDocument *)document{
    NSArray *arrayPermission=[self getArrayPermision:document];
    for (NSDictionary *dic in arrayPermission) {
        if ([dic[@"isMerchant"] boolValue]) {
            return YES;
        }
    }
    return NO;
}
@end
    
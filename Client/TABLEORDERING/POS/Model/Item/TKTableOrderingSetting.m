//
//  TKTableOrderingSetting.m
//  TABLEORDERING
//
//  Created by Nguyen Anh Dao (Zin) on 3/17/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TKTableOrderingSetting.h"

@implementation TKTableOrderingSetting
+(BOOL)getAllowAutoAcceptOrder:(CBLDocument *)document
{
    return [[document.properties[tkKeyAllowAutoAcceptOrder] convertNullToNil] boolValue];
}
+(BOOL)getAllowPushNotification:(CBLDocument *)document
{
    return [[document.properties[tkKeyAllowPushNotification] convertNullToNil] boolValue];
}
+(BOOL)getAllowSendEmail:(CBLDocument *)document
{
    return [[document.properties[tkKeyAllowSendEmail] convertNullToNil] boolValue];
}
+(BOOL)getAllowSendSMS:(CBLDocument *)document
{
    return [[document.properties[tkKeyAllowSendSMS] convertNullToNil] boolValue];
}
+(BOOL)getAllowSouldWhenHaveNewOrder:(CBLDocument *)document{
    return [[document.properties[tkKeyAllowSound] convertNullToNil] boolValue];
}
@end

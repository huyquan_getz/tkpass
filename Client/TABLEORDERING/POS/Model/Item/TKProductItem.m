//
//  TKProductItem.m
//  POS
//
//  Created by Nha Duong Cong on 11/8/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "Constant.h"
#import "TKProductItem.h"
#import "TKVariant.h"

@implementation TKProductItem
+(NSArray *)getArrayAllVariant:(CBLDocument *)document{
    return document.properties[tkKeyArrayVariant];
}
+(NSArray *)getArrayVariantAvailable:(CBLDocument *)document{
    NSArray *allVariant=[self getArrayAllVariant:document];
    NSMutableArray *variantAv=[[NSMutableArray alloc] init];
    for (NSDictionary *variant in allVariant) {
        if ([variant[tkKeyStatus] boolValue]) {
            [variantAv addObject:variant];
        }
    }
    return variantAv;
}
+(NSInteger)getNumberInstance:(CBLDocument *)document{
    NSArray *arrayVariant=[self getArrayVariantAvailable:document];
    NSInteger total=0;
    for (NSDictionary *variant in arrayVariant) {
        if ([variant[tkKeyStatus] boolValue]) {
            NSInteger quantityV=[variant[tkKeyQuantity] integerValue];
            if (quantityV<0) {
                total=-1;
                break;
            }else{
                total+=quantityV;
            }
        }
    }
    return  total;
}
+(NSInteger)getMiniQuantityStockWarning:(CBLDocument *)document{
    NSArray *variantsAvailable=[self getArrayVariantAvailable:document];
    NSInteger miniStock = -1; // don't track
    for (NSDictionary * variant in  variantsAvailable) {
        if([TKVariant isWarningQuantity:variant]){
            if (miniStock == -1) {
                miniStock = [TKVariant getQuantity:variant];
            }else{
                miniStock  = MIN(miniStock, [TKVariant getQuantity:variant]);
            }
        }
    }
    return miniStock;
}
+(BOOL)checkVariantAvailable:(CBLDocument *)document withSKU:(NSString *)variantSKU{
    NSArray *vAvailables=[self getArrayVariantAvailable:document];
    for (NSDictionary *variant in vAvailables) {
        if ([[TKVariant getSKU:variant] isEqualToString:variantSKU]) {
            return YES;
        }
    }
    return NO;
}
+(BOOL)checkQuantityVariantAvailable:(CBLDocument *)document withSKU:(NSString *)variantSKU quanity:(NSInteger)quantityGet{
    NSArray *vAvailables=[self getArrayVariantAvailable:document];
    for (NSDictionary *variant in vAvailables) {
        if ([[TKVariant getSKU:variant] isEqualToString:variantSKU]) {
            NSInteger quantity =[TKVariant getQuantity:variant];
            if (quantity<0 || quantity >= quantityGet) {
                return YES;
            }
            break;
        }
    }
    return NO;
}
+(TKDiscount*)discount:(CBLDocument *)document{
    if ([document.properties[@"discount"] boolValue]) {
        NSDictionary *json=[document.properties[@"discountProductItem"] convertNullToNil];
        if (json) {
            return [[[TKDiscount alloc] initFromJsonData:document.properties[@"discountProductItem"]] convertNullToNil];
        }
    }
    return nil;
}
+(TKDiscount *)checkDiscount:(CBLDocument *)document{
    if ([document.properties[@"discount"] boolValue]) {
        TKDiscount *discount=[self discount:document];
        if (discount==nil || [discount expiredTime]) {
            return nil;
        }
        return discount;
    }else{
        return nil;
    }
}
@end

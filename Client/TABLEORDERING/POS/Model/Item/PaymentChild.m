//
//  PaymentChild.m
//  POS
//
//  Created by Nha Duong Cong on 12/4/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "PaymentChild.h"
#import "Constant.h"


@implementation PaymentChild
@synthesize charged;
@synthesize paymentType;
@synthesize approvedCode;
@synthesize createdDate;
@synthesize changeDue;
-(instancetype)initPaymentCashWithCharged:(double)charge_{
    if (self =[self initPaymentCashWithCharged:charge_ changeDue:0]) {
    }
    return self;
}
-(instancetype)initPaymentCashWithCharged:(double)charge_ changeDue:(double)changeDue_{
    if (self =[super init]) {
        charged=charge_;
        approvedCode=@"";
        paymentType=StringPaymentTypeCash;
        createdDate=[NSDate date];
        changeDue=changeDue_;
    }
    return self;
}
-(instancetype)initPaymentCreditWithCharged:(double)charge_ paymentType:(NSString *)paymentType_ approvedCode:(NSString *)approvedCode_{
    if (self =[super init]) {
        charged=charge_;
        approvedCode=approvedCode_;
        paymentType=paymentType_;
        createdDate =[NSDate date];
        changeDue=0;
    }
    return self;
}
-(instancetype)initFromJsonData:(NSDictionary *)jsonData{
    if (self =[super init]) {
        paymentType=jsonData[@"paymentType"];
        charged=[jsonData[@"charged"] doubleValue];
        approvedCode=[jsonData[@"approvedCode"] convertNullToNil];
        if (approvedCode==nil) {
            approvedCode=[jsonData[@"referenceCode"] convertNullToNil];
        }
        if (approvedCode==nil) {
            approvedCode=@"";
        }
        changeDue=0;
        if ([jsonData[@"changeDue"] convertNullToNil]) {
            changeDue=[jsonData[@"changeDue"] doubleValue];
        }
        createdDate=[CBLJSON dateWithJSONObject:jsonData[@"createdDate"]];
    }
    return self;
}
-(NSString *)displayName{
    return paymentType;
}
-(double)customerSent{
    return charged+changeDue;
}
-(NSDictionary *)jsonData{
    return @{
             @"paymentType":paymentType,
             @"charged":@(charged),
             @"approvedCode":approvedCode,
             @"referenceCode":approvedCode,
             @"createdDate":[CBLJSON JSONObjectWithDate:createdDate],
             @"changDue":@(changeDue)};
}
+(NSArray *)arrayJSONPaymentChildWithListPaymentChild:(NSArray *)listPaymentChild{
    NSMutableArray *arrayJsonPayment=[[NSMutableArray alloc] init];
    for (PaymentChild *paymentChild in listPaymentChild) {
        [arrayJsonPayment addObject:[paymentChild jsonData]];
    }
    return arrayJsonPayment;
}
@end

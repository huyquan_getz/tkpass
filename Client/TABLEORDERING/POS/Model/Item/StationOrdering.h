//
//  StationOrdering.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/17/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
typedef enum _TypeStatusOrdering{
    TypeStatusOrderingNone=-1,
    TypeStatusOrderingWaiting=0,
    TypeStatusOrderingConfirmed=1,
    TypeStatusOrderingServing=2,
    TypeStatusOrderingOccupied=3,
    TypeStatusOrderingEmpty=4
}TypeStatusOrdering;

#import <Foundation/Foundation.h>
#import "TKStation.h"
#import "TKOrder.h"

@interface StationOrdering : NSObject{
    CBLDocument *order;
    CBLDocument *station;
}
@property(readonly) CBLDocument *station;
@property(readonly) CBLDocument *order;
-(instancetype)initWithStation:(CBLDocument*)station_ order:(CBLDocument*)order_;
-(NSDate*)startWaitingDate;
+(NSArray*)subStationOrderingWithType:(TypeStatusOrdering)type fromStationOrderingList:(NSArray*)listStation;
-(TypeStatusOrdering)getTypeStatusStationOrdering;
-(NSString*)stationName;
-(NSString*)orderCode;
-(NSInteger)totalProductItem;
-(NSDictionary*)customerInfo;
-(NSString*)customerName;
-(NSString*)customerFirstName;
-(NSString*)customerPhone;
-(NSString*)customerEmail;
-(NSString*)customerImageURL;
-(NSString*)specialRequest;
-(NSString*)customerPhoneDisplay;// (84)1692804337
-(BOOL)isEqualStationOrdering:(StationOrdering*)stationOrderingCompare;

+(NSString*)typeStringByType:(TypeStatusOrdering)type;
@end

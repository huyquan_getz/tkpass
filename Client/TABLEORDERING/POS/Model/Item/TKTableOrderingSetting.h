//
//  TKTableOrderingSetting.h
//  TABLEORDERING
//
//  Created by Nguyen Anh Dao (Zin) on 3/17/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TKItem.h"
@interface TKTableOrderingSetting : TKItem

+(BOOL)getAllowAutoAcceptOrder:(CBLDocument *)document;
+(BOOL)getAllowPushNotification:(CBLDocument *)document;
+(BOOL)getAllowSendEmail:(CBLDocument *)document;
+(BOOL)getAllowSendSMS:(CBLDocument *)document;
+(BOOL)getAllowSouldWhenHaveNewOrder:(CBLDocument*)document;
@end

//
//  TKProduct.m
//  POS
//
//  Created by Nha Duong Cong on 11/7/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TKProduct.h"
#import "Constant.h"

@implementation TKProduct
+(NSString *)getImageUrlDefault:(CBLDocument *)document{
    id dictImage=document.properties[tkKeyImage];
    if (![dictImage isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    id arrayImage=dictImage[tkKeyArrayImage];
    if ([arrayImage isKindOfClass:[NSArray class]] && [arrayImage count]>0) {
        int imageDefault=[dictImage[tkKeyImageDefault] intValue];
        if (imageDefault <0 || imageDefault>= [arrayImage count]) {
            imageDefault=0;
        }
        return [arrayImage objectAtIndex:imageDefault];
    }
    return nil;
}
+(NSInteger)getNumberVariants:(CBLDocument *)document{
    return [document.properties[tkKeyArrayVariant] count];
}
+(NSString *)getShortName:(CBLDocument *)document{
    return document.properties[tkKeyShortName];
}
+(NSArray *)getArrayOptions:(CBLDocument *)document{
    return [document.properties[tkKeyArrayInventory] convertNullToNil];
}

@end

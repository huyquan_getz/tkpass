//
//  TKListDevicesSetting.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 4/6/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CouchbaseLite/CouchbaseLite.h>

@interface TKListDevicesSetting : NSObject
+(NSArray*)getListDeviceAvailable:(CBLDocument*)document;
@end

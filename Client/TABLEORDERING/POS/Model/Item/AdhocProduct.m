//
//  AdhocProduct.m
//  POS
//
//  Created by Cong Nha Duong on 2/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define MaxTextVariant 20
#import "AdhocProduct.h"

@implementation AdhocProduct
@synthesize title;
@synthesize comment;
@synthesize price;
@synthesize quantity;
@synthesize createdDate;
@synthesize modifiedDate;
@synthesize employeeID;
@synthesize employeeName;
@synthesize applyDiscount;
@synthesize discount;
@synthesize discountProductItem;
@synthesize categoryId;
@synthesize variantName;
-(instancetype)initWithTitle:(NSString *)title_ description:(NSString *)description_ price:(double)price_ quantity:(NSInteger)quantity_ employeeID:(NSInteger )employeeID_ employeeName:(NSString *)employeeName_ applyDiscount:(BOOL)applyDiscount_ categoryId:(NSString *)categoryId_{
    if (self=[super init]) {
        title=title_;
        comment=description_;
        price=price_;
        quantity=quantity_;
        employeeID=employeeID_;
        employeeName=employeeName_;
        NSDate *currentDate=[NSDate date];
        createdDate=currentDate;
        modifiedDate=currentDate;
        applyDiscount=applyDiscount_;
        discount=NO;
        discountProductItem=nil;
        categoryId=categoryId_;
        variantName=[self variantWithLongText:comment];
    }
    return self;
}
-(NSString*)variantWithLongText:(NSString*)text{
    NSRange range=[text rangeOfString:@"\n"];
    NSString *lineTopText;
    if (range.location!=NSNotFound) {
        lineTopText =[text substringToIndex:range.location];
    }else{
        lineTopText=text;
    }
    if (lineTopText.length>MaxTextVariant) {
        return [NSString stringWithFormat:@"%@...",[lineTopText substringToIndex:MaxTextVariant-3]];
    }else{
        return lineTopText;
    }
}
-(NSString *)nameWithVariant{
    return [NSString stringWithFormat:@"%@ (%@)",title,variantName];
}
-(id)encodeAsJSON{
    id jsonDiscountItem;
    if (discount && discountProductItem) {
        jsonDiscountItem=[discountProductItem jsonValue];
    }else{
        jsonDiscountItem=[NSNull null];
    }
    return @{
             tkKeyTitle:title,
             tkKeyDescription:comment,
             tkKeyPrice:@(price),
             tkKeyQuantity:@(quantity),
             tkKeyEmployeeId:@(employeeID),
             tkKeyEmployeeName:employeeName,
             tkKeyCreatedDate:[CBLJSON JSONObjectWithDate:createdDate],
             tkKeyModifiedDate:[CBLJSON JSONObjectWithDate:modifiedDate],
             tkKeyApplyDiscount:@(applyDiscount),
             tkKeyDiscount:@(discount),
             tkKeyDiscountProductItem:jsonDiscountItem,
             tkKeyCategoryId:categoryId,
             @"variantName":variantName
             };
}
-(void)setDiscountProductItem:(TKDiscount *)discountItem_{
    discountProductItem=(applyDiscount)?discountItem_:nil;
    discount=(discountProductItem!=nil);
}
-(TKDiscount *)discountProductItem{
    return (discount)?discountProductItem:nil;
}
-(instancetype)initWithJSON:(id)jsonObject{
    if (self=[super init]) {
        title=jsonObject[tkKeyTitle];
        comment=jsonObject[tkKeyDescription];
        price=[jsonObject[tkKeyPrice] doubleValue];
        quantity=[jsonObject[tkKeyQuantity] doubleValue];
        employeeID=[jsonObject[tkKeyEmployeeId] integerValue];
        employeeName=jsonObject[tkKeyEmployeeName];
        createdDate=[CBLJSON dateWithJSONObject:jsonObject[tkKeyCreatedDate]];
        modifiedDate=[CBLJSON dateWithJSONObject:jsonObject[tkKeyModifiedDate]];
        applyDiscount=[jsonObject[tkKeyApplyDiscount] boolValue];
        categoryId=jsonObject[tkKeyCategoryId];
        discount=[jsonObject[tkKeyDiscount] boolValue];
        NSDictionary *discountJSON=[jsonObject[tkKeyDiscountProductItem] convertNullToNil];
        if (discountJSON) {
            discountProductItem=[[TKDiscount alloc] initFromJsonData:discountJSON];
        }
        variantName=[self variantWithLongText:comment];
    }
    return self;
}
@end

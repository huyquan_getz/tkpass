//
//  TKGeneralSetting.m
//  POS
//
//  Created by Cong Nha Duong on 2/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TKGeneralSetting.h"

@implementation TKGeneralSetting
+(NSString *)getMerchantUserID:(CBLDocument *)document{
    return document.properties[tkKeyUserId];
}
+(NSString *)getMerchantUserOwner:(CBLDocument *)document{
    return document.properties[tkKeyUserOwner];
}
+(NSString*)getCurrency:(CBLDocument*)document{
    return document.properties[tkKeyCurrency];
}
+(NSString *)getGST:(CBLDocument *)document{
    return [document.properties[@"gst"] convertNullToNil];
}
@end

//
//  TKStation.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/18/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TKStation.h"
#import "StationOrdering.h"

@implementation TKStation
+(NSInteger)getStatusStation:(CBLDocument *)document{
    return [document.properties[@"statusOrdering"] integerValue];
}
+(NSString *)getName:(CBLDocument *)document{
    return document.properties[@"stationName"];
}
+(BOOL)clearStation:(CBLDocument *)document{
    [[SyncManager sharedSyncManager] updateDocument:document propertyDoc:@{@"statusOrdering":@(TypeStatusOrderingEmpty), @"customerHost":[NSNull null],@"orderId":[NSNull null]}];
    return YES;
}
+(NSDictionary *)customerHost:(CBLDocument *)document{
    return [document.properties[@"customerHost"] convertNullToNil];
}
+(NSString *)orderId:(CBLDocument *)document{
    return [document.properties[@"orderId"] convertNullToNil];
}
@end

//
//  WebserviceJSON.m
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "WebserviceJSON.h"
#import "HttpClient.h"
#import "Constant.h"
#import "UserDefaultModel.h"
#import "TKOrder.h"
#import "Controller.h"
#import <CommonCrypto/CommonDigest.h>
@implementation WebserviceJSON
+(NSDictionary *)authenticationWithBusinessName:(NSString *)businessName username:(NSString *)username password:(NSString *)password{
    NSDate *dateRequest=[NSDate date];
    NSMutableDictionary *deviceInfo=[[Controller getDeviceInfo] mutableCopy];
    [deviceInfo setObject:@(-1) forKey:tkKeyBranchIndex]; // setbranch index default
    NSDictionary *param=@{
                         @"businessName":businessName,
                         @"userName":username,
                         @"password":password,
                         @"appType":tkAppType,
                         @"deviceInfo":[CBLJSON stringWithJSONObject:deviceInfo options:0 error:nil]};
    NSMutableDictionary *dict=[[[HttpClient sharedHttpClient] postRequestWithURL:tkWebServiceAuthenUserPass parameters:param headers:nil] mutableCopy];
    [dict setObject:[CBLJSON JSONObjectWithDate:dateRequest] forKey:tkKeyRequestDate];
    if ([dict[tkKeyReturnStatus] integerValue]==ReturnCodeSuccess) {
        return [self expectedResponseAuthen:dict];
    }else{
        return dict;
    }
}
+(NSDictionary *)getTokenWithBusinessName:(NSString *)businessName username:(NSString *)username password:(NSString *)password{
    NSDate *dateRequest=[NSDate date];
    NSMutableDictionary *deviceInfo=[[Controller getDeviceInfo] mutableCopy];
    [deviceInfo setObject:@([Controller storeIndexDefault]) forKey:tkKeyBranchIndex];
    NSDictionary *param=@{
                          @"businessName":businessName,
                          @"userName":username,
                          @"password":password,
                          @"appType":tkAppType,
                          @"deviceInfo":[CBLJSON stringWithJSONObject:deviceInfo options:0 error:nil]};
    NSMutableDictionary *dict=[[[HttpClient sharedHttpClient] postRequestWithURL:tkWebServiceAuthenUserPass parameters:param headers:nil] mutableCopy];
    [dict setObject:[CBLJSON JSONObjectWithDate:dateRequest] forKey:tkKeyRequestDate];
    if ([dict[tkKeyReturnStatus] integerValue]==ReturnCodeSuccess) {
        NSDictionary *data=dict[tkKeyResult];
        long expiredTimeMili=[data[@"expires_in"] longValue];
        NSDate *dateExpired=[NSDate dateWithTimeInterval:expiredTimeMili/1000 sinceDate:dateRequest];
        NSDictionary *tokenInfo=@{tkKeyAccessToken:data[tkKeyAccessToken],
                                  @"expires_in":data[@"expires_in"],
                                  tkKeyExpiredTokenDate:[CBLJSON JSONObjectWithDate:dateExpired]};
        [dict setObject:tokenInfo forKey:tkKeyResult];
        
    }
    return dict;
}
+(NSDictionary*)expectedResponseAuthen:(NSDictionary*)response{
    NSMutableDictionary *newResponse=[response mutableCopy];
    NSMutableDictionary *newData=[response[tkKeyResult] mutableCopy];
    
    NSDate *dateRequest=[CBLJSON dateWithJSONObject:response[tkKeyRequestDate]];
    NSDictionary *dictUpdate=@{@"userAccessSync":@"smoov",@"passAccessSync":@"smoov@123"};
    //check server sync
    NSString *serverSync=newData[@"serverSync"];
    if ([serverSync hasPrefix:@"http://"] || [serverSync hasPrefix:@"https://"] ) {
        //
    }else{
        serverSync =[@"http://" stringByAppendingString:serverSync];
    }
    [newData setObject:serverSync forKey:@"serverSync"];
    
    NSArray *channels=@[newData[tkKeyMerchantID],
                        [NSString stringWithFormat:@"%@_table",newData[tkKeyMerchantID]]];
    [newData setObject:channels forKey:@"channels"];
    //check port sync
    if ([newData[@"portSync"] convertNullToNil]==nil) {
        [newData setObject:@"4984" forKey:@"portSync"];
    }
    [newData addEntriesFromDictionary:dictUpdate];
    long expiredTimeMili=[newData[@"expires_in"] longValue];
    NSDate *dateExpired=[NSDate dateWithTimeInterval:expiredTimeMili/1000 sinceDate:dateRequest];
    [newData setObject:[CBLJSON JSONObjectWithDate:dateExpired] forKey:tkKeyExpiredTokenDate];
    
    [newResponse setObject:newData forKey:tkKeyResult];
    return newResponse;
}
+(NSDictionary *)sendEmailWithData:(NSDictionary *)dataSend toEmail:(NSString *)email type:(MailType)mailType{
    NSMutableDictionary *param=[[NSMutableDictionary alloc] init];
    NSString *urlSendMail;
    
    NSDictionary *dataSync=[UserDefaultModel getMerchantSyncInfor];
    [param setObject:[self stringEmailTypeWithEmailType:mailType] forKey:@"typeEmail"];
    [param setObject:email forKey:@"emailCustomer"];
    [param setObject:dataSync[tkKeyBusinessName] forKey:@"businessName"];
    switch (mailType) {
        case MailTypeCollected:
        case MailTypeReSendCollected:
        case MailTypeReSendCancel:
        case MailTypeReSendRefund:{
            
            urlSendMail=tkWebServiceSendMailReceipt;
            [param setObject:[CBLJSON stringWithJSONObject:dataSend options:0 error:nil] forKey:@"stringOrder"];
            [param setObject:dataSync[tkKeyBusinessName] forKey:@"businessName"];
            [param setObject:email forKey:@"emailCustomer"];
            [param setObject:dataSend[tkKeyOrderStatus] forKey:@"status"];
            
            break;
        }
        case MailTypeReady:
        case MailTypeRefund:
        case MailTypeCancel:
        case MailTypeRejectCancel:{
            urlSendMail=tkWebServiceSendMailChangeStatus;
            NSMutableDictionary *data=[[NSMutableDictionary alloc] init];
            [data setObject:dataSend[tkKeyOrderCode] forKey:@"OrderNumber"];
            [data setObject:dataSync[tkKeyBusinessName] forKey:@"BusinessName"];
            if (mailType == MailTypeRefund) {
                [data setObject:[dataSend[tkKeyRefundInfo] objectForKey:tkKeyReasonRefund] forKey:@"Reason"];
            }
            [data setObject:email forKey:@"EmailCustomer"];
            [data setObject:[dataSend[tkKeyCustomerReceiver] objectForKey:tkKeyFirstName] forKey:@"FirstName"];
            NSString *stringData=[CBLJSON stringWithJSONObject:data options:0 error:nil];
            [param setObject:stringData forKey:tkKeyResult];
            break;
        }
            
        case MailTypeReportCashDrawer:{
            urlSendMail=tkWebServiceSendMailCashDrawerReport;
            NSString *stringData=[CBLJSON stringWithJSONObject:dataSend options:0 error:nil];
            [param setObject:stringData forKey:tkKeyResult];
            break;
        }
        case MailTypeOrderingConfirm:
        case MailTypeOrderingReject:
        case MailTypeOrderingCancel:
        case MailTypeOrderingReady:
        case MailTypeOrderingAmend:{
            urlSendMail=tkWebServiceSendMailChangeStatusOrdering;
            [param setObject:[self stringEmailTypeWithEmailType:mailType] forKey:@"typeEmail"];
            [param setObject:email forKey:@"emailCustomer"];
            [param setObject:dataSync[tkKeyBusinessName] forKey:@"businessName"];
            [param setObject:dataSend[@"customerName"] forKey:@"customerName"];
            [param setObject:dataSend[@"stationName"] forKey:@"tableName"];
            if (dataSend[@"message"]) {
                [param setObject:dataSend[@"message"] forKey:@"message"];
            }else{
                [param setObject:@"" forKey:@"message"];
            }
            break;
        }
            
        default:
            break;
    }
    if (urlSendMail) {
        return [self callWebserviceAuthenWithURL:urlSendMail parameters:param headers:nil];
    }
    return @{tkKeyReturnStatus:@(ReturnCodeRequestError)};
}
+(NSDictionary *)checkAndAddDevice{
    NSMutableDictionary *param=[[NSMutableDictionary alloc] init];
    NSDictionary *dataSync=[UserDefaultModel getMerchantSyncInfor];
    [param setObject:dataSync[tkKeyMerchantID] forKey:@"bucket"];
    [param setObject:tkAppType forKey:@"appType"];
    
    NSMutableDictionary *deviceInfo=[[Controller getDeviceInfo] mutableCopy];
    [deviceInfo setObject:@([Controller storeIndexDefault]) forKey:tkKeyBranchIndex];
    
    [param setObject:[CBLJSON stringWithJSONObject:deviceInfo options:0 error:nil] forKey:@"deviceInfo"];
    [SNLog Log:@"update device info:%@",param];
    return [self callWebserviceAuthenWithURL:tkWebServiceCheckAndAddDevice parameters:param headers:nil];
}
+(NSDictionary*)refundOrderWithMerchantID:(NSString*)merchantID referenceCode:(NSString*)referenceCode refundAmount:(double)refundAmount remarks:(NSString*)remarks
{
     if(tkWebServiceSmoovPayRefundOrder) {
         NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
         [paramDic setObject:merchantID forKey:@"merchantID"];
         [paramDic setObject:referenceCode forKey:@"referenceCode"];
         [paramDic setObject:@(refundAmount) forKey:@"refundAmount"];
         [paramDic setObject:remarks forKey:@"remarks"];
         NSDictionary *dict= [self callWebserviceAuthenWithURL:tkWebServiceSmoovPayRefundOrder parameters:paramDic headers:nil];
         return dict;
     }
    return @{tkKeyReturnStatus:@(ReturnCodeRequestError)};
}
+(NSDictionary *) sendNotifySMS:(NSMutableDictionary *)paramDic
{
    if(tkWebServiceSendSMS) {
        NSString *signature;
        NSString * timestamp = [NSString stringWithFormat:@"%lu",(NSInteger)[[NSDate date] timeIntervalSince1970]];
        [paramDic setObject:timestamp forKey:@"timestamp"];
        signature =[self sign:paramDic secrectKey:@"58a6fd4938a0be868058f239f4b6fc28"];
        [paramDic setObject:signature forKey:@"signature"];
        NSDictionary *dict=[[HttpClient sharedHttpClient] postRequestWithURL:tkWebServiceSendSMS parameters:paramDic headers:nil];
        // NSLog(@"Send SMS Result: %@",dict);
        return dict;
     }
     return @{tkKeyReturnStatus:@(ReturnCodeRequestError)};
}
+ (NSString*) sign:(NSDictionary*)data secrectKey:(NSString*) secrect {
    NSMutableString  *ret_sign = [[NSMutableString alloc] initWithString:@""];
    // Sort data by Key
    NSArray *sortedKeys = [[data allKeys] sortedArrayUsingSelector: @selector(compare:)];
    for(NSString *key in sortedKeys) {
        id object = [data objectForKey:key];
        [ret_sign appendFormat:@"%@=%@", key, object];
    }
      // Append secret and hash using MD5
    [ret_sign appendString:secrect];
    return [self md5HexDigest:ret_sign];
}
+ (NSString*)md5HexDigest:(NSString*)input {
    const char* str = [input UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (unsigned int)strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}
+(NSString*)stringEmailTypeWithEmailType:(MailType)typeEmail{
    switch (typeEmail) {
        case MailTypeRejectCancel:
            return @"Reject";
            break;
        case MailTypeCancel:
            return @"Cancel";
            break;
        case MailTypeRefund:
            return @"Refund";
            break;
        case MailTypeReady:
            return @"Ready";
            break;
        case MailTypeCollected:
            return @"Receipt";
            break;
        case MailTypeReSendCollected:
            return @"ReceiptResend";
            break;
        case MailTypeReSendCancel:
            return @"ReceiptCancelResend";
            break;
        case MailTypeReSendRefund:
            return @"ReceiptRefundResend";
            break;
        case MailTypeReportCashDrawer:
            return @"CashDrawerReport";
            break;
        case MailTypeOrderingConfirm:
            return @"OrderingConfirm";
            break;
        case MailTypeOrderingReject:
            return @"OrderingReject";
            break;
        case MailTypeOrderingCancel:
            return @"OrderingCancel";
            break;
        case MailTypeOrderingReady:
            return @"OrderingReady";
            break;
        case MailTypeOrderingAmend:
            return @"OrderingAmend";
            break;
        default:
            break;
    }
    return @"";
}
+(NSDictionary*)sendUpdateOrderToServerWithData:(NSDictionary *)data typeMessage:(int)typeMessage{
    if(tkWebServiceUpdateStatusOrderToServer) {
        NSDictionary *dataSync=[UserDefaultModel getMerchantSyncInfor];
        NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
        [paramDic setObject:dataSync[tkKeyMerchantID] forKey:tkKeyMerchantID];
        [paramDic setObject:[CBLJSON stringWithJSONObject:data options:0 error:nil] forKey:tkKeyResult];
        [paramDic setObject:@(typeMessage) forKey:@"typeMessage"];
        return [self callWebserviceAuthenWithURL:tkWebServiceUpdateStatusOrderToServer parameters:paramDic headers:nil];
    }
    return @{tkKeyReturnStatus:@(ReturnCodeRequestError)};

}
+(NSDictionary *)resetApplicationNumberBadge{
    NSMutableDictionary *deviceInfo=[[Controller getDeviceInfo] mutableCopy];
    [deviceInfo setObject:@([Controller storeIndexDefault]) forKey:@"brachIndex"];
    NSDictionary *param =@{
                           @"deviceInfo":[CBLJSON stringWithJSONObject:deviceInfo options:0 error:nil]
                           };
    if (tkWebServiceResetBadgeDevice) {
        return [self callWebserviceAuthenWithURL:tkWebServiceResetBadgeDevice parameters:param headers:nil];
    }else{
        return @{tkKeyReturnStatus:@(ReturnCodeRequestError)};
    }

}
+(NSDictionary*)callWebserviceAuthenWithURL:(NSString*)urlWebService parameters:(NSDictionary*)parameters headers:(NSDictionary*)headers{
    NSMutableDictionary *dataSend;
    if ([parameters isKindOfClass:[NSMutableDictionary class]]) {
        dataSend=(NSMutableDictionary*)parameters;
    }else{
        dataSend=[parameters mutableCopy];
    }
    NSDictionary *dataSync=[UserDefaultModel getMerchantSyncInfor];
    NSDate *expiredTokenDate=[CBLJSON dateWithJSONObject:dataSync[tkKeyExpiredTokenDate]];
    if (expiredTokenDate && [expiredTokenDate compare:[NSDate date]]==NSOrderedAscending) {
        
        [Controller resetToken];// reget merchant syncInfor after resetToken;
        dataSync=[UserDefaultModel getMerchantSyncInfor]; // reasign dataSync with new Token update
        
    }
    [dataSend setObject:dataSync[tkKeyAccessToken] forKey:tkKeyAuthorization];
    NSDictionary *response=[[HttpClient sharedHttpClient] postRequestWithURL:urlWebService parameters:dataSend headers:headers];
    NSInteger responseStatus=[response[tkKeyReturnStatus] integerValue];
    if (responseStatus == ReturnCodeAccessTokenExpired || responseStatus == ReturnCodeAccessTokenNotProvided) {
        
        [Controller resetToken];
        dataSync=[UserDefaultModel getMerchantSyncInfor];// reget merchant syncInfor after resetToken;
        [dataSend setObject:dataSync[tkKeyAccessToken] forKey:tkKeyAuthorization]; // reset AuthenToken fro dataSend;
        response=[[HttpClient sharedHttpClient] postRequestWithURL:urlWebService parameters:dataSend headers:headers];
        
    }
    return response;
}
@end

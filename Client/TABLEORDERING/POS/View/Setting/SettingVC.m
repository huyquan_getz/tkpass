//
//  SettingVC.m
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "SettingVC.h"
#import "Controller.h"
#import "ListPrinterVC.h"
#import "ListPrintingVC.h"
#import "GeneralSettingVC.h"


@interface SettingVC ()

@end

@implementation SettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _vFrameLeftMenu.hidden=YES;
    _vFrameCenter.hidden=YES;
    _vLine.backgroundColor=tkColorFrameBorder;
    _naviControllerCenter =[[UINavigationController alloc] init];
    [_naviControllerCenter setNavigationBarHidden:YES];
    [self addChildViewController:_naviControllerCenter];
    _naviControllerCenter.view.frame=_vFrameCenter.frame;
    [self.view addSubview:_naviControllerCenter.view];
    [_ivNaviBar setBackgroundColor:[UIColor statusNaviBar]];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    _lbTitleSetting.text=[[LanguageUtil sharedLanguageUtil]stringByKey:@"setting.title-setting"];
    [self addLeftMenuSetting];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addLeftMenuSetting{
    if (_leftMenuSettingVC) {
        [_leftMenuSettingVC.view removeFromSuperview];
        [_leftMenuSettingVC removeFromParentViewController];
        _leftMenuSettingVC=nil;
    }
    _leftMenuSettingVC=[[LeftMenuSettingVC alloc] init];
    _leftMenuSettingVC.delegate=self;
    [self addChildViewController:_leftMenuSettingVC];
    _leftMenuSettingVC.view.frame=_vFrameLeftMenu.frame;
    [self.view addSubview:_leftMenuSettingVC.view];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickMenu:(id)sender {
    [Controller showLeftMenu:self];
}
#pragma LeftMenuSettingDelegate
-(void)leftMenuSetting:(id)sender clickInStoreGeneral:(BOOL)none{
    GeneralSettingVC *generalSetting =[[GeneralSettingVC alloc] init];
    [_naviControllerCenter setViewControllers:@[generalSetting] animated:NO];
}
-(void)leftMenuSetting:(id)sender clickHardwarePrinterList:(BOOL)none{
    ListPrinterVC *listPrinter =[[ListPrinterVC alloc] init];
    [_naviControllerCenter setViewControllers:@[listPrinter] animated:NO];
}
-(void)leftMenuSetting:(id)sender clickHardwarePrinting:(BOOL)none{
    ListPrintingVC *listPrinting =[[ListPrintingVC alloc] init];
    [_naviControllerCenter setViewControllers:@[listPrinting] animated:NO];
}
@end

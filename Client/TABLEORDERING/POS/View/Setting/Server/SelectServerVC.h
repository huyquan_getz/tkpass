//
//  SelectServerVC.h
//  POS
//
//  Created by Cong Nha Duong on 3/3/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectServerVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    NSMutableArray *listServerName;
    NSIndexPath *indexPathSelected;
}
@property (weak, nonatomic) IBOutlet UIButton *btDone;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak, nonatomic) IBOutlet UITextField *tfServerName;
- (IBAction)clickDone:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbMessage;

@end

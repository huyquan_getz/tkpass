//
//  AddNewPrinterVC.h
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "ListPrinterVC.h"
#import "PopoverListController.h"

@interface AddNewPrinterVC : UIViewController<UITextFieldDelegate>{
    CBLDocument *printerDoc;
    
}

@property (weak, nonatomic) IBOutlet UIImageView *ivNavBar;
-(instancetype)initWithPrinter:(CBLDocument*)printer;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityITestConnection;
- (IBAction)clickBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btBack;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIView *vInput;
@property (weak, nonatomic) IBOutlet UIButton *btTestConnect;
- (IBAction)clickTestConnection:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
- (IBAction)clickSave:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btSave;
- (IBAction)clickRemove:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btRemove;
@property (weak, nonatomic) IBOutlet UILabel *lbTitlePrinterName;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleIpAddress;
@property (weak, nonatomic) IBOutlet UILabel *lbTitlePort;
@property (weak, nonatomic) IBOutlet UITextField *tfPrinterName;
@property (weak, nonatomic) IBOutlet UITextField *tfIpAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfPort;
@property (weak, nonatomic) IBOutlet UIView *vLine1;
@property (weak, nonatomic) IBOutlet UIView *vLine2;

@end

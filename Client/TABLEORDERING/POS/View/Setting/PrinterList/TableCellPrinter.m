//
//  TableCellPrinter.m
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableCellPrinter.h"
#import "Constant.h"

@implementation TableCellPrinter

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        _lbTitle.textColor=[UIColor whiteColor];
        _lbStatus.textColor=[UIColor whiteColor];
    }else{
        _lbTitle.textColor=[UIColor darkGrayColor];
        if (on) {
            _lbStatus.textColor=tkColorMain;
        }else{
            _lbStatus.textColor=[UIColor redColor];
        }
    }
    // Configure the view for the selected state
}
-(void)setValueDefault{
    UIView *bgSelected=[[UIView alloc] init];
    [bgSelected setBackgroundColor:tkColorMainSelected];
    [super setSelectedBackgroundView:bgSelected];
    _vLine.backgroundColor=tkColorFrameBorder;
    _lbTitle.text=@"";
    _lbStatus.text=@"";
    _activityIndicator.hidden=NO;
    on=NO;
    [_activityIndicator startAnimating];
}
-(void)setOn:(BOOL)on_{
    on=on_;
    if (on) {
        _lbStatus.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"setting.printer-list.status-online"];
        _lbStatus.textColor=tkColorMain;
    }else{
        _lbStatus.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"setting.printer-list.status-offline"];
        _lbStatus.textColor=[UIColor redColor];
    }
}
@end

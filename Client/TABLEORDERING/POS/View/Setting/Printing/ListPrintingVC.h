//
//  ListPrintingVC.h
//  POS
//
//  Created by Cong Nha Duong on 1/9/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface ListPrintingVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *listPrinting;
    CBLLiveQuery *liveQuery;
}
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btAdd;
- (IBAction)clickAdd:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbNoDefaultPrinter;
@property (weak, nonatomic) IBOutlet UIImageView *ivNaviBar;

@end

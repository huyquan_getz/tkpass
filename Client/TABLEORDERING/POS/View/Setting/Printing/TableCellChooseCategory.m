//
//  TableCellChooseCategory.m
//  POS
//
//  Created by Cong Nha Duong on 1/13/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableCellChooseCategory.h"
#import "Constant.h"

@implementation TableCellChooseCategory

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setValueDefault{
    UIView *bgSelected=[[UIView alloc] init];
    [bgSelected setBackgroundColor:tkColorFrameBorder];
    [super setSelectedBackgroundView:bgSelected];
    _vLine.backgroundColor=tkColorFrameBorder;
    _lbTitle.text=@"";
    [self setTick:NO];
    
}
-(void)setTick:(BOOL)tick{
    if (tick) {
        _ivTick.tag=YES;
        _ivTick.image=[UIImage imageNamed:@"icon_done_active.png"];
    }else{
        _ivTick.tag=NO;
        _ivTick.image=nil;
    }
}
-(BOOL)currentTick{
    return _ivTick.tag;
}
@end

//
//  GeneralSettingVC.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/11/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "GeneralSettingVC.h"
#import "Constant.h"
#import "Controller.h"
#import "TKInventory.h"

@interface GeneralSettingVC ()

@end

@implementation GeneralSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _bgBranch.backgroundColor=[UIColor whiteColor];
    _bgBranch.layer.borderColor=tkColorFrameBorder.CGColor;
    _bgBranch.layer.borderWidth=1;
    _bgBranch.layer.cornerRadius=tkCornerRadiusButton;
    _lbBranchName.textColor=tkColorMain;
    _ivNaviBar.backgroundColor=[UIColor statusNaviBar];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    CBLDocument *store=[Controller getStoreInfoDoc];
    _lbBranchName.text=[TKInventory getName:store];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

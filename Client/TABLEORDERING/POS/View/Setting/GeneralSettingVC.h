//
//  GeneralSettingVC.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/11/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralSettingVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lbTitleBranch;
@property (weak, nonatomic) IBOutlet UILabel *lbBranchName;
@property (weak, nonatomic) IBOutlet UILabel *bgBranch;
@property (weak, nonatomic) IBOutlet UIImageView *ivNaviBar;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;

@end

//
//  PinCodeVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/22/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol PinCodeDelegate <NSObject>

@required
-(void)pinCodeVerify:(id)sender completed:(BOOL)completed;

@optional
-(void)pinCodeVerify:(id)sender canceled:(BOOL)canceled;
-(void)pinCodeVerify:(id)sender switchUser:(BOOL)switchUser;
@end
#import <UIKit/UIKit.h>
#import "Constant.h"

@interface PinCodeVC : UIViewController{
    CBLDocument *employee;
    BOOL checkInLogin;
}
@property (weak,nonatomic) id<PinCodeDelegate> delegate;
-(instancetype)initWithEmployee:(CBLDocument*)employee_ checkLogin:(BOOL)checkLogin_;
- (IBAction)clickNumber:(id)sender;
- (IBAction)clickClear:(id)sender;
- (IBAction)clickEnter:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vInput;
@property (weak, nonatomic) IBOutlet UIButton *btBack;
@property (weak, nonatomic) IBOutlet UIButton *btNumber1;
@property (weak, nonatomic) IBOutlet UIButton *btNumber2;
@property (weak, nonatomic) IBOutlet UIButton *btNumber3;
@property (weak, nonatomic) IBOutlet UIButton *btNumber4;
@property (weak, nonatomic) IBOutlet UIButton *btNumber5;
@property (weak, nonatomic) IBOutlet UIButton *btNumber6;
@property (weak, nonatomic) IBOutlet UIButton *btNumber7;
@property (weak, nonatomic) IBOutlet UIButton *btNumber8;
@property (weak, nonatomic) IBOutlet UIButton *btNumber9;
@property (weak, nonatomic) IBOutlet UIButton *btNumber0;
- (IBAction)clickMoreInfo:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btMoreInfo;
@property (weak, nonatomic) IBOutlet UILabel *lbStatusMessage;
@property (weak, nonatomic) IBOutlet UIButton *btClear;
@property (weak, nonatomic) IBOutlet UIButton *btEnter;

@property (weak, nonatomic) IBOutlet UIButton *btLock;
- (IBAction)clickBack:(id)sender;
- (IBAction)clickLock:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vLine1;
@property (weak, nonatomic) IBOutlet UIView *vLine21;
@property (weak, nonatomic) IBOutlet UIView *vLine22;
@property (weak, nonatomic) IBOutlet UIView *vLine23;
@property (weak, nonatomic) IBOutlet UIView *vLine31;
@property (weak, nonatomic) IBOutlet UIView *vLine32;
@property (weak, nonatomic) IBOutlet UIView *vLine33;
@property (weak, nonatomic) IBOutlet UIView *vLine34;
@property (weak, nonatomic) IBOutlet UIView *vLine35;
@property (weak, nonatomic) IBOutlet UIView *vLine36;
@property (weak, nonatomic) IBOutlet UIView *vTextField;
@property (weak, nonatomic) IBOutlet UITextField *tfID1;
@property (weak, nonatomic) IBOutlet UITextField *tfID2;
@property (weak, nonatomic) IBOutlet UITextField *tfID3;
@property (weak, nonatomic) IBOutlet UITextField *tfID4;
@property (strong, nonatomic) IBOutlet UIView *vToptip;
@property (weak, nonatomic) IBOutlet UILabel *lbToptip;

@end

//
//  LoginVC.h
//  POS
//
//  Created by Nha Duong Cong on 10/13/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginForgotVC.h"

@interface LoginVC : UIViewController<UITextFieldDelegate,LoginForgotDelegate>{
    LoginForgotVC *loginForgotVC;
}
@property (weak, nonatomic) IBOutlet UILabel *lbStatusMessage;
@property (weak, nonatomic) IBOutlet UIView *vMainLogin;
@property (weak, nonatomic) IBOutlet UIButton *btLogin;
@property (weak, nonatomic) IBOutlet UITextField *tfBusinessName;
@property (weak, nonatomic) IBOutlet UITextField *tfUsername;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
- (IBAction)clickForgotPassword:(id)sender;
- (IBAction)clickLogin:(id)sender;
-(BOOL)validateInput;
- (IBAction)clickCreateAccount:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btCreateAccount;

@property (weak, nonatomic) IBOutlet UIButton *btForgotPass;
- (IBAction)clickBack:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btBack;
@end

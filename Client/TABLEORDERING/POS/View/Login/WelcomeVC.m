//
//  WelcomeVC.m
//  POS
//
//  Created by Nha Duong Cong on 10/30/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "WelcomeVC.h"
#import "Controller.h"

@interface WelcomeVC ()

@end

@implementation WelcomeVC{
    LanguageUtil *languageKey;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _btSignUp.enabled=NO;
    _ivTopWelcome.backgroundColor=tkColorMain;
    _btSignUp.backgroundColor=tkColorButtonBackground;
    _btSignUp.layer.cornerRadius=tkCornerRadiusButton;
    [_btSignUp.layer setShadowColor:[UIColor blackColor].CGColor];
    [_btSignUp.layer setShadowOpacity:1];
    [_btSignUp.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [_btSignUp.layer setShadowRadius:tkCornerRadiusButton];
    _btSignUp.titleLabel.font=tkFontMainTitleButton;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [_btSignIn setTitle:[languageKey stringByKey:@"login.bt.sign-in-if-have-account"] forState:UIControlStateNormal];
    [_btSignUp setTitle:[languageKey stringByKey:@"login.bt.get-start"] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickSignUp:(id)sender {
    NSString *urlSmoovPosSignIn=tkLinkSignUp;
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:urlSmoovPosSignIn]];
}
- (IBAction)clickSignIn:(id)sender {
    [Controller showMerchantLoginFirst];
}
@end

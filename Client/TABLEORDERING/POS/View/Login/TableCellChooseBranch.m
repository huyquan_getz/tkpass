//
//  TableCellChooseBranch.m
//  POS
//
//  Created by Cong Nha Duong on 1/23/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableCellChooseBranch.h"

@implementation TableCellChooseBranch

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _ivTick.hidden=!selected;
    // Configure the view for the selected state
}

@end

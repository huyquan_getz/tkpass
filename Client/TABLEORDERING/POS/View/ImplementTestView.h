//
//  ImplementTestView.h
//  POS
//
//  Created by Nha Duong Cong on 10/30/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Audio.h"

@interface ImplementTestView : NSObject{
    NSOperationQueue *myQueue11;
}
+(ImplementTestView*)sharedTestView;
-(UIViewController*)viewShow;
-(BOOL)showTest;
@end

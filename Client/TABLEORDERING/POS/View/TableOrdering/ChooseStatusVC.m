//
//  ChooseStatusVC.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "ChooseStatusVC.h"
#import "Constant.h"
#import "StatusOrderingCell.h"


@interface ChooseStatusVC ()
{
    NSMutableIndexSet *arrIndexChoose;
}
@end

@implementation ChooseStatusVC
-(instancetype)init{
    if (self =[super init]) {
        arrIndexChoose=[[NSMutableIndexSet alloc] initWithIndexesInRange:NSMakeRange(0, 6)];
        
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _btnApplyStatus.layer.cornerRadius=tkCornerRadiusButton;
    _btnApplyStatus.backgroundColor=tkColorMain;
    _tbvMain.scrollEnabled=NO;
    // Do any additional setup after loading the view from its nib.
}
-(void)resetIndexSetSelectedWithIndexSetStatus:(NSIndexSet *)indexSetStatus_{
    [arrIndexChoose removeAllIndexes];
    [indexSetStatus_ enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [arrIndexChoose addIndex:idx+1];
    }];
    if (arrIndexChoose.count==5) {
        [arrIndexChoose addIndex:0];
    }
    [_tbvMain reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 #pragma mark -  Tableview Delegate
/*

 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    StatusOrderingCell * cell = [tableView dequeueReusableCellWithIdentifier:@"statusCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"StatusOrderingCell" bundle:nil] forCellReuseIdentifier:@"statusCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"statusCell"];
        
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(StatusOrderingCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0){
        [cell.lblStatus setTextColor:[UIColor darkGrayColor]];
        cell.backgroundColor=tkColorMainBackground;
        [cell.lblStatus setText:[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.status-all-status"]];
    }else {
        cell.backgroundColor=[UIColor whiteColor];
        TypeStatusOrdering typeStatus =(int)indexPath.row-1;
        cell.lblStatus.text=[StationOrdering typeStringByType:typeStatus];
        switch (indexPath.row -1) {
            case TypeStatusOrderingWaiting:
                [cell.lblStatus setTextColor:tkColorDarkRed];
                break;
            case TypeStatusOrderingConfirmed:
                [cell.lblStatus setTextColor:tkColorPending];
                break;
            case TypeStatusOrderingServing:
                [cell.lblStatus setTextColor:tkColorReady];
                break;
            case TypeStatusOrderingOccupied:
                [cell.lblStatus setTextColor:[UIColor darkGrayColor]];
                break;
            case TypeStatusOrderingEmpty:
                [cell.lblStatus setTextColor:[UIColor darkGrayColor]];
                break;
            default:
                break;
        }
    }
cell.ivChooseStatus.hidden=![arrIndexChoose containsIndex:indexPath.row];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"%d", indexPath.row );
    if (indexPath.row==0) {
        if ([arrIndexChoose containsIndex:0]) {
            [arrIndexChoose removeAllIndexes];
        }
        else{
            [arrIndexChoose addIndexesInRange:NSMakeRange(0, 6)];
        }
        // reload cell to display
        [_tbvMain reloadData];
        
    }
    else{
        if([arrIndexChoose containsIndex:indexPath.row])
        {
            [arrIndexChoose removeIndex:indexPath.row];
            
        }
        else{
            [arrIndexChoose addIndex:indexPath.row];
        }
        //------------
        [arrIndexChoose removeIndex:0];
        if (arrIndexChoose.count == 5) {
            [arrIndexChoose addIndex:0];
        }
        // reload cell to display
        [_tbvMain reloadData];
    }
   
    
}
- (IBAction)btnApplyStatus:(id)sender {
    
    [arrIndexChoose removeIndex:0];
    NSMutableIndexSet *indexStatus = [[NSMutableIndexSet alloc]init];
    [arrIndexChoose enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [indexStatus addIndex:idx-1];
    }];
    if (_delegate) {
        [_delegate chooseStatus:self applyWithSetStatus:indexStatus];
    }
}

@end

//
//  TableOrderingListener.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
@protocol TableOrderingListenerDelegate <NSObject>
-(void)tableOrderingListener:(id)sender listStationOrderings:(NSArray*)stationOrderings stationOrderingNews:(NSArray*)stationOrderingNews;
@end

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface TableOrderingListener : NSObject{
    BOOL startedListener;
    BOOL isListening;
    CBLLiveQuery *liveQueryStation;
    CBLLiveQuery *liveQueryOrdering;
    NSMutableArray *listStations;
    NSMutableArray *listOrderings;
}
@property(weak,nonatomic) id<TableOrderingListenerDelegate> delegate;
-(instancetype)init;
+(instancetype)sharedObject;
-(void)startListener;
-(NSArray*)shareListStationOrderings;


@end

//
//  ChooseStatusVC.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#import "StationOrdering.h"

@protocol ChooseStatusDelegate <NSObject>
-(void)chooseStatus:(id)sender applyWithSetStatus:(NSIndexSet*)indexSet;

@end
#import <UIKit/UIKit.h>

@interface ChooseStatusVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    
}

@property (weak,nonatomic) id<ChooseStatusDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
- (IBAction)btnApplyStatus:(id)sender;
-(void)resetIndexSetSelectedWithIndexSetStatus:(NSIndexSet*)indexSetStatus_;
@property (weak, nonatomic) IBOutlet UIButton *btnApplyStatus;



@end

//
//  ListTableOrderingVC.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
typedef enum _SortType{
    SortTypeByStationName=0,
    SortTypeBySessionTime=1,
    SortTypeByWaitingTime=2,
    SortTypeByStatus=3,
}SortType;

#import "StationOrdering.h"
@protocol ListTableOrderingDelegate <NSObject>

-(void)listTableOrdering:(id)sender clickDetail:(StationOrdering*)stationOrdering;
-(StationOrdering*)listTableOrdering:(id)sender getStationOrderingSelected:(BOOL)none;
@end
#import <UIKit/UIKit.h>

@interface ListTableOrderingVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *listStationOrdering;
    SortType sortType;
    BOOL sortByDescending;
}
@property (weak,nonatomic) id<ListTableOrderingDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (strong, nonatomic) IBOutlet UIView *vHeaderTable;
@property (weak, nonatomic) IBOutlet UILabel *lbNoOrdering;
- (IBAction)clickSort:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btSortStation;
@property (weak, nonatomic) IBOutlet UIButton *btSortSessionTime;
@property (weak, nonatomic) IBOutlet UIButton *btSortWaitingTime;
@property (weak, nonatomic) IBOutlet UIButton *btSortStatus;
@property (weak, nonatomic) IBOutlet UIButton *btSortName;
@property (weak, nonatomic) IBOutlet UIButton *btSortOrderCode;
@property (weak, nonatomic) IBOutlet UIButton *btSortItem;
-(void)resetWithListTableOrdering:(NSArray*)listOrdering_;
@end

//
//  TableOrderingOccupiedDetailVC.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableOrderingOccupiedDetailVC.h"
#import "StationOrdering.h"
@interface TableOrderingOccupiedDetailVC ()

@end

@implementation TableOrderingOccupiedDetailVC{
    LanguageUtil *languageKey;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor clearColor];
    UIView *vCover=[[UIView alloc] initWithFrame:self.view.frame];
    [vCover setViewCoverDefault];
    [self.view insertSubview:vCover belowSubview:_vMain];
    // Do any additional setup after loading the view from its nib.
    languageKey=[LanguageUtil sharedLanguageUtil];
   
    _vMain.layer.cornerRadius=tkCornerRadiusButton;
    _vMain.clipsToBounds=YES;
    _lbTitle.layer.cornerRadius=tkCornerRadiusButton;
    [_vOrderCode setBackgroundColor:tkColorMainBackground];
    [_bgTitle setBackgroundColor:tkColorMainBackground];
    _btnClearTable.layer.cornerRadius=tkCornerRadiusButton;
    _btnClearTable.backgroundColor=tkColorMain;
}
-(void)viewWillAppear:(BOOL)animated{
    _lbTitle.text=[stationOrdering stationName];
    _headerViewDetail.lbName.text=[stationOrdering customerName];
    _headerViewDetail.lbPhoneNumber.text=[stationOrdering customerPhoneDisplay];
    _headerViewDetail.lbEmail.text=[stationOrdering customerEmail];
    [_headerViewDetail setLbStatusColor:[UIColor darkGrayColor] text:[StationOrdering typeStringByType:TypeStatusOrderingOccupied]];
    [_headerViewDetail setImageAvataWithURL:[stationOrdering customerImageURL]];
    [_btnClearTable setTitle:[languageKey stringByKey:@"ordering.dt.bt-clear-table"] forState:UIControlStateNormal];
    _lblOrderCode.text=[[languageKey stringByKey:@"ordering.list.order-code-title"] stringByAppendingString:@": 0"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickClearTable:(id)sender {
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"ordering.ms-confirm-clear-table"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    currentAlert=alert;
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            if (_delegate) {
                [_delegate tableOrderingOccupied:self clearStationOrdering:stationOrdering];
            }
        }
    }];
}

- (IBAction)clickCloseTable:(id)sender {
    if (_delegate) {
        [_delegate tableOrderingDetail:self clickCancel:YES];
    }
}
@end

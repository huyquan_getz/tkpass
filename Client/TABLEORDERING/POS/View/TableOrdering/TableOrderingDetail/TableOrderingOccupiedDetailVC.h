//
//  TableOrderingOccupiedDetailVC.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableOrderingDetailVC.h"
#import "ViewHeaderOrderingDetail.h"

@interface TableOrderingOccupiedDetailVC : TableOrderingDetailVC
@property (weak,nonatomic) id<TableOrderingOccupiedDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIView *vOrderCode;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCode;
- (IBAction)clickClearTable:(id)sender;
- (IBAction)clickCloseTable:(id)sender;
@property (weak, nonatomic) IBOutlet ViewHeaderOrderingDetail *headerViewDetail;
@property (weak, nonatomic) IBOutlet UILabel *bgTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnClearTable;

@end

//
//  TableOrderingConfirmedDetailVC.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableOrderingDetailVC.h"
#import "TKAmountDisplay.h"
#import "ViewHeaderOrderingDetail.h"
#import "OrderDetailVC.h"
@interface TableOrderingConfirmedDetailVC : TableOrderingDetailVC
{
     TKAmountDisplay *amountDisplay;
}
- (IBAction)clickCloseTable:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleOrdering;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UILabel *lblViewHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnReady;
- (IBAction)clickReady:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelAndRefund;
- (IBAction)clickCancelAndRefund:(id)sender;
@property (weak, nonatomic) id<TableOrderingConfirmedDelegate> delegate;
@property (strong,nonatomic) OrderDetailVC *orderDetailVC;
@property (weak, nonatomic) IBOutlet UIView *vFrameDetail;




@end

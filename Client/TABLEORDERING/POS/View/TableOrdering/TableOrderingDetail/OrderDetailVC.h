//
//  OrderDetailVC.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/23/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "StationOrdering.h"
#import "ViewHeaderOrderingDetail.h"

@interface OrderDetailVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    StationOrdering *stationOrdering;
}
-(instancetype)initWithStationOrdering:(StationOrdering *)stationOrdering_;
@property (weak, nonatomic) IBOutlet ViewHeaderOrderingDetail *vHeaderInfo;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleSpecail;
@property (weak, nonatomic) IBOutlet UITextView *tvSpecial;
@property (weak, nonatomic) IBOutlet UIView *bgOrderCode;
@property (weak, nonatomic) IBOutlet UILabel *lbOrderCode;
@property (weak, nonatomic) IBOutlet UIView *vLine1;
@property (weak, nonatomic) IBOutlet UIView *vLine2;
@end

//
//  TableCellProductInWaitingAmend.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/13/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved./Users/nhaduong/Downloads/tick.png
//

#import "TableCellProductInWaitingAmend.h"
#import "Constant.h"

@implementation TableCellProductInWaitingAmend

- (void)awakeFromNib {
    _vLine.backgroundColor=tkColorFrameBorder;
    _tfQuantity.layer.borderWidth=1;
    _tfQuantity.layer.borderColor=tkColorFrameBorder.CGColor;
    _tfQuantity.layer.cornerRadius=tkCornerRadiusButton;
    _tfQuantity.delegate=self;
    _tfQuantity.backgroundColor=[UIColor clearColor];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickTick:(id)sender {
    if (!_delegate || [_delegate tableCellProductInWaitingAmend:self shouldChangeStatusTick:!_btTick.selected indexPath:_indexPathCell]) {
        _btTick.selected=!_btTick.selected;
    }
}
-(void)setTick:(BOOL)tick{
    _btTick.selected=tick;
}
#pragma TextfieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        return NO;
    }
    NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if (![newString isUIntegerFormat] || newString.length>3) {
        return NO;
    }
    if (_delegate) {
        return [_delegate tableCellProductInWaitingAmend:self shouldChangeQuantity:textField to:[newString integerValue] indexPath:_indexPathCell];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (_delegate) {
        [_delegate tableCellProductInWaitingAmend:self didBeginEditing:textField indexPath:_indexPathCell];
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (_delegate) {
        [_delegate tableCellProductInWaitingAmend:self didEndEditing:textField indexPath:_indexPathCell];
    }
}
-(void)setVariant:(NSString *)variant remark:(NSString *)remark{
    _lbDescription.text=variant;
    if (remark && remark.length>0) {
        remark =[@"* " stringByAppendingString:remark];
        NSInteger heightRemark =[remark heightWithFont:_lbRemark.font width:_lbRemark.frame.size.width];
        if (heightRemark>30) {
            //2 line
            CGRect frame =_lbRemark.frame;
            frame.size.height=35;
            _lbRemark.frame=frame;
            
            frame=_vLine.frame;
            frame.origin.y=109;
            _vLine.frame=frame;
        }else{
            //1 line
            CGRect frame =_lbRemark.frame;
            frame.size.height=20;
            _lbRemark.frame=frame;
            
            frame=_vLine.frame;
            frame.origin.y=94;
            _vLine.frame=frame;
        }
        _lbRemark.text=remark;
    }else{
        _lbRemark.text=@"";
        CGRect frame=_vLine.frame;
        frame.origin.y=69;
        _vLine.frame=frame;
    }
}
+(CGFloat)heightCellWithVariant:(NSString *)variant remark:(NSString *)remark{
    if (remark && remark.length>0) {
        remark =[@"* " stringByAppendingString:remark];
        NSInteger heightRemark =[remark heightWithFont:tkFontMainWithSize(14) width:487];
        if (heightRemark>30) {
            //2 line
            return 110;
        }else{
            //1 line
            return 95;
        }
    }
    return 70;
}
@end

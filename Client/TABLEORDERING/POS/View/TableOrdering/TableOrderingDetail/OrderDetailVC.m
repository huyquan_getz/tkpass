//
//  OrderDetailVC.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/23/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define CellProductIndentify @"cellProductItem"
#import "OrderDetailVC.h"
#import "CashierBusinessModel.h"
#import "TableCellProductInDetail.h"
#import "Constant.h"
#import "TKAmountDisplay.h"
#import "TKGeneralSetting.h"
#import "Controller.h"

@interface OrderDetailVC ()

@end

@implementation OrderDetailVC{
    NSArray *listProductSingle;
    TKAmountDisplay *amountDisplay;
    LanguageUtil *languageKey;
}
-(instancetype)initWithStationOrdering:(StationOrdering *)stationOrdering_{
    if (self =[super init]) {
        stationOrdering=stationOrdering_;
        if (stationOrdering.order) {
            listProductSingle=[CashierBusinessModel arrayProductOrderWithProductOrdersJSON:[TKOrder arrayProductJSON:stationOrdering.order]];
            NSString *currency=[TKOrder currency:stationOrdering.order];
            if (currency==nil) {
                currency=[TKGeneralSetting getCurrency:[Controller getGeneralSetting]];
            }
            amountDisplay=[[TKAmountDisplay alloc] initWithCurrency:currency];
        }else{
            listProductSingle=@[];
            NSString *currency=[TKGeneralSetting getCurrency:[Controller getGeneralSetting]];
            amountDisplay=[[TKAmountDisplay alloc] initWithCurrency:currency];
        }
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    languageKey=[LanguageUtil sharedLanguageUtil];
    _bgOrderCode.backgroundColor=tkColorMainBackground;
    _tvSpecial.backgroundColor=[UIColor whiteColor];
    _tvSpecial.layer.cornerRadius=tkCornerRadiusViewPopup;
    _tvSpecial.layer.borderColor=tkColorFrameBorder.CGColor;
    _tvSpecial.layer.borderWidth=1;
    _tvSpecial.textContainerInset=UIEdgeInsetsMake(5,5,5,5);
    [_tvSpecial setEditable:NO];
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellProductInDetail" bundle:nil] forCellReuseIdentifier:CellProductIndentify];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    NSDictionary *customerInfo=[stationOrdering customerInfo];
    NSString *urlImage=[customerInfo[@"image"] convertNullToNil];
    if (urlImage) {
        [_vHeaderInfo setImageAvataWithURL:urlImage];
    }
    _tvSpecial.text=[stationOrdering specialRequest];
    _vHeaderInfo.lbEmail.text =[stationOrdering customerEmail];
    _vHeaderInfo.lbPhoneNumber.text =[stationOrdering customerPhoneDisplay];
    _vHeaderInfo.lbName.text =[stationOrdering customerName];
    _lbOrderCode.text=[NSString stringWithFormat:@"%@: %@",[languageKey stringByKey:@"ordering.list.order-code-title"],[stationOrdering orderCode]];
    TypeStatusOrdering status=[stationOrdering getTypeStatusStationOrdering];
    switch (status) {
        case TypeStatusOrderingWaiting:{
            [_vHeaderInfo setLbStatusColor:tkColorDarkRed text:[StationOrdering typeStringByType:status]];
            break;
        }
        case TypeStatusOrderingConfirmed:{
            [_vHeaderInfo setLbStatusColor:tkColorPending text:[StationOrdering typeStringByType:status]];
            break;
        }
        case TypeStatusOrderingServing:{
            [_vHeaderInfo setLbStatusColor:tkColorReady text:[StationOrdering typeStringByType:status]];
            break;
        }
        default:
            break;
    }
    _lbTitleSpecail.text=[languageKey stringByKey:@"ordering.dt.special-request"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma TableOrderDelegate
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellProductInDetail *cell=[tableView dequeueReusableCellWithIdentifier:CellProductIndentify];
    ProductOrderSingle *pro=listProductSingle[indexPath.row];
    cell.lbTitle.text=pro.productName;
    [cell setVariant:pro.variantName remark:pro.variantRemark];
    cell.tfQuantity.text=[NSString stringWithFormat:@"x%ld",(long)pro.quantity];
    double amount=[CashierBusinessModel totalAmountHaveDiscountOfProductOrder:pro];
    cell.lbAmount.text=[amountDisplay stringAmountHaveCurrency:amount];
    cell.vLine.hidden=(indexPath.row==listProductSingle.count-1);
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProductOrderSingle *pro=listProductSingle[indexPath.row];
    return [TableCellProductInDetail heightCellWithVariant:pro.variantName remark:pro.variantRemark];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listProductSingle.count;
}
-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  TableOrderingConfirmedDetailVC.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define CellProductIndentify @"cellProduct"
#import "TableOrderingConfirmedDetailVC.h"
#import "Constant.h"
#import "TableCellProductInDetail.h"
#import "ProductOrderSingle.h"
#import "CashierBusinessModel.h"
@interface TableOrderingConfirmedDetailVC ()

@end

@implementation TableOrderingConfirmedDetailVC
{
      LanguageUtil *languageKey;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    languageKey =[LanguageUtil sharedLanguageUtil];
    self.view.backgroundColor=[UIColor clearColor];
    UIView *vCover=[[UIView alloc] initWithFrame:self.view.frame];
    [vCover setViewCoverDefault];
    [self.view insertSubview:vCover belowSubview:_vMain];
    _vMain.layer.cornerRadius=tkCornerRadiusButton;
    _vMain.clipsToBounds=YES;
    
    [_lblViewHeader setBackgroundColor: tkColorMainBackground];
    
    _btnCancelAndRefund.layer.cornerRadius=tkCornerRadiusButton;
    _btnCancelAndRefund.backgroundColor=tkColorDarkRed;
    
    _btnReady.layer.cornerRadius=tkCornerRadiusButton;
    _btnReady.backgroundColor=tkColorMain;
       // set data
    _lblTitleOrdering.text = [stationOrdering stationName];
}
-(void)viewWillAppear:(BOOL)animated{
    [self addOrderDetail];
    [_btnReady setTitle:[languageKey stringByKey:@"general.bt.ready"] forState:UIControlStateNormal];
    [_btnCancelAndRefund setTitle:[languageKey stringByKey:@"ordering.dt.bt-cancel-refund"] forState:UIControlStateNormal];
}
-(void)addOrderDetail{
    if (_orderDetailVC) {
        [_orderDetailVC.view removeFromSuperview];
        [_orderDetailVC removeFromParentViewController];
        _orderDetailVC=nil;
    }
    _orderDetailVC=[[OrderDetailVC alloc] initWithStationOrdering:stationOrdering];
    [self addChildViewController:_orderDetailVC];
    _orderDetailVC.view.frame=_vFrameDetail.frame;
    [_vMain addSubview:_orderDetailVC.view];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickCloseTable:(id)sender {
    if (_delegate) {
        [_delegate tableOrderingDetail:self clickCancel:YES];
    }
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (IBAction)clickReady:(id)sender {
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"ordering.ms-confirm-ready-order"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    currentAlert=alert;
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            if (_delegate) {
                [_delegate tableOrderingConfirm:self readyStationOrdering:stationOrdering];
            }
        }
    }];
}
- (IBAction)clickCancelAndRefund:(id)sender {
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"ordering.ms-confirm-cancel-refund"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    currentAlert=alert;
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            if (_delegate) {
                [_delegate tableOrderingConfirm:self cancelAndRefundStationOrdering:stationOrdering];
            }
        }
    }];

}

@end

//
//  WaitingAmendVC.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/13/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define CellProductIndentify @"cellProduct"
#import "WaitingAmendVC.h"
#import "TableCellProductInWaitingAmend.h"
#import "Constant.h"
#import "ProductOrderSingle.h"
#import "TKAmountDisplay.h"
#import "CashierBusinessModel.h"
#import "TKGeneralSetting.h"
#import "Controller.h"

@interface WaitingAmendVC ()

@end

@implementation WaitingAmendVC{
    NSMutableIndexSet *indexSetRowSelected;
    TKAmountDisplay *amountDisplay;
    NSArray *oldProductSingles;
    NSMutableArray *newProductSingles;
    LanguageUtil *languageKey;
    __weak UIAlertView *currentAlert;
}
-(instancetype)initWithStationOrdering:(id)stationOrdering_{
    if (self =[super init]) {
        stationOrdering=stationOrdering_;
        oldProductSingles=[CashierBusinessModel arrayProductOrderWithProductOrdersJSON:[TKOrder arrayProductJSON:stationOrdering.order]];
        newProductSingles=[[NSMutableArray alloc] init];
        for (ProductOrderSingle *proSingle in oldProductSingles) {
            [newProductSingles addObject:[proSingle copy]];
        }
        NSString *currency=[TKOrder currency:stationOrdering.order];
        if (currency==nil) {
            currency=[TKGeneralSetting getCurrency:[Controller getGeneralSetting]];
        }
        amountDisplay=[[TKAmountDisplay alloc] initWithCurrency:currency];
        indexSetRowSelected=[[NSMutableIndexSet alloc] initWithIndexesInRange:NSMakeRange(0, newProductSingles.count)];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    languageKey=[LanguageUtil sharedLanguageUtil];
    _bgTitle.backgroundColor=tkColorMainBackground;
    _btAccept.backgroundColor=tkColorMain;
    _btAccept.layer.cornerRadius=tkCornerRadiusButton;
    _tvMessage.backgroundColor=[UIColor whiteColor];
    _tvMessage.layer.cornerRadius=tkCornerRadiusViewPopup;
    _tvMessage.layer.borderColor=tkColorFrameBorder.CGColor;
    _tvMessage.layer.borderWidth=1;
    _tvMessage.textContainerInset=UIEdgeInsetsMake(5,5,5,5);
    _bgOrderCode.backgroundColor=tkColorMainBackground;
    _lbTitleMessage.text=@"";
    _tvMessage.text=@"";
    _tvMessage.editable=NO;
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellProductInWaitingAmend" bundle:nil] forCellReuseIdentifier:CellProductIndentify];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    _lbOrderCode.text=[NSString stringWithFormat:@"%@: %@",[languageKey stringByKey:@"ordering.list.order-code-title"],[stationOrdering orderCode]];
    _lbTitleMessage.text=[languageKey stringByKey:@"ordering.amend.title-message"];
    _lbTitle.text=[languageKey stringByKey:@"ordering.amend.title"];
    [_btAccept setTitle:[languageKey stringByKey:@"ordering.dt.bt-amend"] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma TableOrderDelegate
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellProductInWaitingAmend *cell=[tableView dequeueReusableCellWithIdentifier:CellProductIndentify];
    [cell setTick:[indexSetRowSelected containsIndex:indexPath.row]];
    cell.delegate=self;
    cell.indexPathCell=indexPath;
    
    ProductOrderSingle *pro=newProductSingles[indexPath.row];
    cell.lbTitle.text=pro.productName;
    [cell setVariant:pro.variantName remark:pro.variantRemark];
    cell.tfQuantity.text=[NSString stringWithFormat:@"%ld",(long)pro.quantity];
    double amount=[CashierBusinessModel totalAmountHaveDiscountOfProductOrder:pro];
    cell.lbAmount.text=[amountDisplay stringAmountHaveCurrency:amount];
    cell.vLine.hidden=(indexPath.row==newProductSingles.count-1);
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProductOrderSingle *productSingle=newProductSingles[indexPath.row];
    return [TableCellProductInWaitingAmend   heightCellWithVariant:productSingle.variantName remark:productSingle.variantRemark];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return newProductSingles.count;
}
-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}
#pragma TextFieldDelegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    CGRect frame=self.view.superview.frame;
    frame.origin.y-=255;
    self.view.superview.frame=frame;
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    CGRect frame=self.view.superview.frame;
    frame.origin.y+=255;
    self.view.superview.frame=frame;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma TableCellProductInWaiting
-(BOOL)tableCellProductInWaitingAmend:(id)sender shouldChangeStatusTick:(BOOL)newStatusTick indexPath:(NSIndexPath *)indexPath{
    if (newStatusTick) {
        [indexSetRowSelected addIndex:indexPath.row];
    }else{
        [indexSetRowSelected removeIndex:indexPath.row];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateMessageSend];
    });
    return YES;
}
-(void)tableCellProductInWaitingAmend:(TableCellProductInWaitingAmend*)sender didBeginEditing:(UITextField *)textField indexPath:(NSIndexPath *)indexPath{
    CGRect frame=self.view.superview.frame;
    frame.origin.y-=75;
    self.view.superview.frame=frame;
}
-(void)tableCellProductInWaitingAmend:(id)sender didEndEditing:(UITextField *)textField indexPath:(NSIndexPath *)indexPath{
    if (textField.text.length==0) {
        textField.text=@"0";
    }
    CGRect frame=self.view.superview.frame;
    frame.origin.y+=75;
    self.view.superview.frame=frame;
}
-(BOOL)tableCellProductInWaitingAmend:(TableCellProductInWaitingAmend*)sender shouldChangeQuantity:(UITextField *)textField to:(NSInteger)newQuantity indexPath:(NSIndexPath *)indexPath{
    ProductOrderSingle *oldProSingle=oldProductSingles[indexPath.row];
    if (newQuantity>oldProSingle.quantity) {
        return NO;
    }
    //else
    ProductOrderSingle *productSingle=newProductSingles[indexPath.row];
    productSingle.quantity=newQuantity;
    double amount=[CashierBusinessModel totalAmountHaveDiscountOfProductOrder:productSingle];
    sender.lbAmount.text=[amountDisplay stringAmountHaveCurrency:amount];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateMessageSend];
    });
    return YES;
}
- (IBAction)clickAcceptAndRefund:(id)sender {
    if (![self haveChangeData]) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Please make change to continue" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        currentAlert=alert;
        [alert show];
        return;
    }
    if ([self numberOfProductItemAvailable]==0) {
        // message not selected
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Please selecte least a item" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        currentAlert=alert;
        [alert show];
    }else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Confirm" message:@"Are you sure you want amend this order" delegate:nil cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
        currentAlert=alert;
        [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex==1) {
                if (_delegate) {
                    [_delegate waitingAmend:self amendStationOrdering:stationOrdering listProductFrom:oldProductSingles to:newProductSingles indexSetItemAvailable:indexSetRowSelected message:[self messageSend]];
                }
            }
        }];
    }
}
-(NSInteger)numberOfProductItemAvailable{
    NSInteger totalQuantity=0;
    for (NSInteger i=0; i<newProductSingles.count; i++) {
        if ([indexSetRowSelected containsIndex:i]) {
            ProductOrderSingle *productSingle=newProductSingles[i];
            totalQuantity += productSingle.quantity;
        }
    }
    return totalQuantity;
}
-(BOOL)haveChangeData{
    if (indexSetRowSelected.count == newProductSingles.count) {
        BOOL haveChange=NO;
        for (NSInteger i=0;i<newProductSingles.count; i++) {
            ProductOrderSingle *oldPr=oldProductSingles[i];
            ProductOrderSingle *newPr=newProductSingles[i];
            if (newPr.quantity!=oldPr.quantity) {
                haveChange=YES;
                break;
            }
        }
        return haveChange;
    }
    return YES;
}
-(void)updateMessageSend{ 
    NSString *message=[self messageSend];
    if (message.length>0) {
        NSString *firstCharacter=[message substringWithRange:NSMakeRange(0, 1)];
        message = [message stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[firstCharacter lowercaseString]];
        _tvMessage.text=[NSString stringWithFormat:@"Hi %@, %@We will do a refund back to you.",[stationOrdering customerFirstName],message];
    }else{
        _tvMessage.text=@"";
    }
}
-(NSString*)messageSend{
    NSMutableString *messageDisableProduct=[[NSMutableString alloc] initWithString:@""];
    NSMutableString *messageChangeQuantity=[[NSMutableString alloc] initWithString:@""];
    for (NSInteger indexCell=0; indexCell<newProductSingles.count; indexCell++) {
        ProductOrderSingle *newProSingle=newProductSingles[indexCell];
        ProductOrderSingle *oldProSingle=oldProductSingles[indexCell];
        if (![indexSetRowSelected containsIndex:indexCell]) {
            [messageDisableProduct appendFormat:@"%@, ",[CashierBusinessModel titleVariantOfProductOrder:newProSingle]];
        }else if(newProSingle.quantity != oldProSingle.quantity){
            [messageChangeQuantity appendFormat:@"item %@ change quantity from %ld to %ld, ",[CashierBusinessModel titleVariantOfProductOrder:newProSingle],oldProSingle.quantity,newProSingle.quantity];
        }
    }
    
    NSMutableString *message=[[NSMutableString alloc] initWithString:@""];
    if (messageDisableProduct.length>0) {
        [messageDisableProduct replaceCharactersInRange:NSMakeRange(0, 0) withString:@"The following item(s) is currently unavailable: "];
        [messageDisableProduct deleteCharactersInRange:NSMakeRange(messageDisableProduct.length-2, 2)];
        [messageDisableProduct appendString:@". "];
    }
    if (messageChangeQuantity.length>0) {
        [messageChangeQuantity replaceCharactersInRange:NSMakeRange(0, 1) withString:@"I"];
        [messageChangeQuantity deleteCharactersInRange:NSMakeRange(messageChangeQuantity.length-2, 2)];
        [messageChangeQuantity appendString:@". "];
    }
    if (messageDisableProduct.length>0 || messageChangeQuantity.length>0) {
        if (messageDisableProduct.length>0) {
            [message appendString:messageDisableProduct];
        }
        if (messageChangeQuantity.length>0) {
            [message appendString:messageChangeQuantity];
        }
    }
    return message;
}
- (IBAction)clickCancel:(id)sender {
    if (_delegate) {
        [_delegate waitingAmend:self clickCancel:YES];
    }
}
-(void)dealloc{
    if (currentAlert) {
        [currentAlert dismissWithClickedButtonIndex:-1 animated:YES];
    }
}
@end

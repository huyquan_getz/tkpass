//
//  TableOrderingWaitingDetailVC.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableOrderingWaitingDetailVC.h"
#import "Constant.h"
#import "WaitingAmendVC.h"



@interface TableOrderingWaitingDetailVC ()

@end

@implementation TableOrderingWaitingDetailVC
{
      LanguageUtil *languageKey;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _vFrameDetail.hidden=YES;
    languageKey =[LanguageUtil sharedLanguageUtil];
    self.view.backgroundColor=[UIColor clearColor];
    UIView *vCover=[[UIView alloc] initWithFrame:self.view.frame];
    [vCover setViewCoverDefault];
    [self.view insertSubview:vCover belowSubview:_vTableOrderDetail];
    _vTableOrderDetail.layer.cornerRadius=tkCornerRadiusButton;
    _vTableOrderDetail.clipsToBounds=YES;
    
    _lblViewHeader.backgroundColor= tkColorMainBackground;
    _lblViewHeader.layer.cornerRadius=tkCornerRadiusButton;

    _lblTitleTableOrder.text = [stationOrdering stationName];
    
    _btApprove.backgroundColor=tkColorReady;
    _btApprove.layer.cornerRadius=tkCornerRadiusButton;
    
    _btAmend.backgroundColor=tkColorMain;
    _btAmend.layer.cornerRadius=tkCornerRadiusButton;
    
    _btReject.backgroundColor=tkColorDarkRed;
    _btReject.layer.cornerRadius=tkCornerRadiusButton;

}
-(void)viewWillAppear:(BOOL)animated{
    [self addOrderDetail];
    [_btAmend setTitle:[languageKey stringByKey:@"ordering.dt.bt-amend"] forState:UIControlStateNormal];
    [_btApprove setTitle:[languageKey stringByKey:@"general.bt.approve"] forState:UIControlStateNormal];
    [_btReject setTitle:[languageKey stringByKey:@"general.bt.reject"] forState:UIControlStateNormal];
}
-(void)addOrderDetail{
    if (_orderDetailVC) {
        [_orderDetailVC.view removeFromSuperview];
        [_orderDetailVC removeFromParentViewController];
        _orderDetailVC=nil;
    }
    _orderDetailVC=[[OrderDetailVC alloc] initWithStationOrdering:stationOrdering];
    [self addChildViewController:_orderDetailVC];
    _orderDetailVC.view.frame=_vFrameDetail.frame;
    [_vTableOrderDetail addSubview:_orderDetailVC.view];
}

-(void)addWaitingAmend{
    if (_waitingAmendVC) {
        [_waitingAmendVC.view removeFromSuperview];
        [_waitingAmendVC removeFromParentViewController];
        _waitingAmendVC=nil;
    }
    _waitingAmendVC=[[WaitingAmendVC alloc] initWithStationOrdering:stationOrdering];
    _waitingAmendVC.delegate=self;
    [self addChildViewController:_waitingAmendVC];
    CGRect frame=_vTableOrderDetail.frame;
    frame.origin=CGPointZero;
    _waitingAmendVC.view.frame=frame;
    [_vTableOrderDetail addSubview:_waitingAmendVC.view];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnCancel:(id)sender {
    if (_delegate) {
        [_delegate tableOrderingDetail:self clickCancel:YES];
    }
}

#pragma WaitingDetailDelegate
-(IBAction)clickApprove:(id)sender{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"ordering.ms-confirm-approve-order"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    currentAlert=alert;
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            if (_delegate) {
                [_delegate tableOrderingWaiting:self approveStationOrdering:stationOrdering];
            }
        }
    }];
}
-(IBAction)clickAmend:(id)sender{
    [self addWaitingAmend];
    
}
-(IBAction)clickReject:(id)sender{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"ordering.ms-confirm-reject-refund"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    currentAlert=alert;
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            if (_delegate) {
                [_delegate tableOrderingWaiting:self rejectStationOrdering:stationOrdering];
            }
        }
    }];
}

#pragma WaitingAmendDelegate
-(void)waitingAmend:(id)sender clickCancel:(BOOL)none{
    [_waitingAmendVC.view removeFromSuperview];
    [_waitingAmendVC removeFromParentViewController];
    _waitingAmendVC=nil;
}
-(void)waitingAmend:(id)sender amendStationOrdering:(StationOrdering *)stationOrdering_ listProductFrom:(NSArray *)oldProductSingles to:(NSArray *)newProductSingles indexSetItemAvailable:(NSIndexSet *)setItemAvailable message:(NSString *)message{
    if (_delegate) {
        [_delegate tableOrderingWaiting:self amendStationOrdering:stationOrdering_ listProductFrom:oldProductSingles to:newProductSingles indexSetItemAvailable:setItemAvailable message:message];
    }
}
@end

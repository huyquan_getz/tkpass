//
//  TableOrderingServingDetailVC.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableOrderingServingDetailVC.h"
#import "Constant.h"
#import "OrderDetailVC.h"
@interface TableOrderingServingDetailVC ()

@end

@implementation TableOrderingServingDetailVC{
    LanguageUtil *languageKey;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    languageKey=[LanguageUtil sharedLanguageUtil];
    self.view.backgroundColor=[UIColor clearColor];
    UIView *vCover=[[UIView alloc] initWithFrame:self.view.frame];
    [vCover setViewCoverDefault];
    [self.view insertSubview:vCover belowSubview:_vMain];
    
    _vMain.layer.cornerRadius=tkCornerRadiusButton;
    _vMain.clipsToBounds=YES;
    _lblTitle.layer.cornerRadius=tkCornerRadiusButton;
    [_bgTitle setBackgroundColor:tkColorMainBackground];
    _btnClearTable.layer.cornerRadius=tkCornerRadiusButton;
    _btnClearTable.backgroundColor=tkColorMain;
}
-(void)viewWillAppear:(BOOL)animated{
    [self addOrderDetail];
    _lblTitle.text=[stationOrdering stationName];
    [_btnClearTable setTitle:[languageKey stringByKey:@"ordering.dt.bt-clear-table"] forState:UIControlStateNormal];
}
-(void)addOrderDetail{
    if (_orderDetailVC) {
        [_orderDetailVC.view removeFromSuperview];
        [_orderDetailVC removeFromParentViewController];
        _orderDetailVC=nil;
    }
    _orderDetailVC=[[OrderDetailVC alloc] initWithStationOrdering:stationOrdering];
    [self addChildViewController:_orderDetailVC];
    _orderDetailVC.view.frame=_vFrameOrderDetail.frame;
    [_vMain addSubview:_orderDetailVC.view];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickCloseTable:(id)sender {
    if (_delegate) {
        [_delegate tableOrderingDetail:self clickCancel:YES];
    }
}

- (IBAction)clickClearTable:(id)sender {
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"ordering.ms-confirm-clear-table"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    currentAlert=alert;
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            if (_delegate) {
                [_delegate tableOrderingServing:self clearStationOrdering:stationOrdering];
            }
        }
    }];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  1;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    return cell;
}
@end

//
//  TableOrderingServingDetailVC.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableOrderingDetailVC.h"
#import "ViewHeaderOrderingDetail.h"
#import "OrderDetailVC.h"

@interface TableOrderingServingDetailVC : TableOrderingDetailVC <UIScrollViewDelegate,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *bgTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnClearTable;
@property (weak, nonatomic) IBOutlet UIView *vFrameOrderDetail;
@property (strong,nonatomic) OrderDetailVC *orderDetailVC;
- (IBAction)clickCloseTable:(id)sender;
- (IBAction)clickClearTable:(id)sender;
@property (weak, nonatomic) id<TableOrderingServingDelegate> delegate;
@end

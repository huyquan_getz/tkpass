//
//  TableOrderingDetailVC.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableOrderingDetailVC.h"
#import "CashierBusinessModel.h"

@interface TableOrderingDetailVC ()

@end

@implementation TableOrderingDetailVC{
    NSString *oldReversionOrdering;
    NSString *oldReversionStation;
}
-(instancetype)initWithStationOrdering:(StationOrdering *)stationOrdering_{
    if (self =[super init]) {
        stationOrdering=stationOrdering_;
        oldReversionStation=[TKItem getRev:stationOrdering.station];
        if (stationOrdering.order) {
            oldReversionOrdering=[TKItem getRev:stationOrdering.order];
        }
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
}
-(BOOL)checkDataChangeFromWeb{
    if (stationOrdering.order) {
        return ![oldReversionOrdering isEqualToString:[TKItem getRev:stationOrdering.order]];
    }else{
        return ![oldReversionStation isEqualToString:[TKItem getRev:stationOrdering.station]];
    }
}
-(void)dismissCurrentAlertView{
    if (currentAlert) {
        [currentAlert dismissWithClickedButtonIndex:-1 animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
//    if (currentAlert) {
//        [currentAlert dismissWithClickedButtonIndex:-1 animated:YES];
//    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  ViewHeaderOrderingDetail.h
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/20/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLImageView.h"
#import "LBorderView.h"

@interface ViewHeaderOrderingDetail : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet DLImageView *ivAvata;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbStatus;
@property (weak, nonatomic) IBOutlet LBorderView *borderStatus;
-(void)setLbStatusColor:(UIColor*)color text:(NSString*)text;
-(void)setImageAvataWithURL:(NSString*)urlString;
-(void)setValueDefault;
@end

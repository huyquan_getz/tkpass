//
//  ListTableOrderingVC.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define SecondToReshowWaitingTime 1
#define CellOrderingIndentify @"cellOrdering"
#import "ListTableOrderingVC.h"
#import "TableCellOrdering.h"
#import "TKOrder.h"

@interface ListTableOrderingVC ()

@end

@implementation ListTableOrderingVC{
    BOOL isReloading;
    NSTimer *timerReshowWaitingTime;
    NSArray *listButtonSort;
}
-(instancetype)init{
    if (self =[super init]) {
        listStationOrdering=[[NSMutableArray alloc] init];
        sortType=SortTypeByStationName;
        sortByDescending=YES;
        isReloading=NO;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellOrdering" bundle:nil] forCellReuseIdentifier:CellOrderingIndentify];
    _vHeaderTable.backgroundColor=tkColorMainBackground;
    timerReshowWaitingTime=[NSTimer scheduledTimerWithTimeInterval:SecondToReshowWaitingTime target:self selector:@selector(reshowWaitingTime:) userInfo:nil repeats:YES];
    
    listButtonSort=@[_btSortStation,_btSortSessionTime,_btSortWaitingTime,_btSortStatus];
    //assign SortType for tag of button sort
    _btSortStation.tag=SortTypeByStationName;
    _btSortSessionTime.tag=SortTypeBySessionTime;
    _btSortWaitingTime.tag=SortTypeByWaitingTime;
    _btSortStatus.tag=SortTypeByStatus;
    
    // disable some button sort
    _btSortName.enabled=NO;
    _btSortOrderCode.enabled=NO;
    _btSortItem.enabled=NO;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [_btSortStation setTitle:[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.list.station-title"] forState:UIControlStateNormal];
    [_btSortName setTitle:[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.list.custome-name-title"] forState:UIControlStateNormal];
    [_btSortOrderCode setTitle:[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.list.order-code-title"] forState:UIControlStateNormal];
    [_btSortItem setTitle:[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.list.nb-item-title"] forState:UIControlStateNormal];
    [_btSortSessionTime setTitle:[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.list.session-time-title"] forState:UIControlStateNormal];
    [_btSortWaitingTime setTitle:[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.list.waiting-time-title"] forState:UIControlStateNormal];
    [_btSortStatus setTitle:[[LanguageUtil sharedLanguageUtil] stringByKey:@"ordering.list.status-title"] forState:UIControlStateNormal];
    for (UIButton *bt in listButtonSort) {
        [self decorateIconSortForButton:bt];
    }
    [self clickSort:_btSortStation];
}
- (void)decorateIconSortForButton:(UIButton*)button{
    NSInteger widthTitle=[button.titleLabel.text widthWithFont:button.titleLabel.font];
    [button setTitleColor:tkColorMain forState:UIControlStateNormal];
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, widthTitle, 0, 0)];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, -22 , 0, 0)];
    [button setImage:[UIImage imageNamed:@"icon_sort.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"icon_sort.png"] forState:UIControlStateHighlighted];
}
- (IBAction)clickSort:(UIButton*)sender {
    if (sender.tag == sortType) {
        sortByDescending= !sortByDescending;
    }else{
        sortByDescending=NO;
        //disable button sort last
        for (UIButton *bt in listButtonSort) {
            if (bt.tag == sortType) {
                [bt setImage:[UIImage imageNamed:@"icon_sort.png"] forState:UIControlStateNormal];
                [bt setImage:[UIImage imageNamed:@"icon_sort.png"] forState:UIControlStateHighlighted];
                break;
            }
        }
        sortType=(int)sender.tag;
    }
    if (sortByDescending) {
        [sender setImage:[UIImage imageNamed:@"icon_sort_down.png"] forState:UIControlStateNormal];
        [sender setImage:[UIImage imageNamed:@"icon_sort_down.png"] forState:UIControlStateHighlighted];
    }else{
        [sender setImage:[UIImage imageNamed:@"icon_sort_up.png"] forState:UIControlStateNormal];
        [sender setImage:[UIImage imageNamed:@"icon_sort_up.png"] forState:UIControlStateHighlighted];
    }
    [self resetWithListTableOrdering:listStationOrdering];
}
-(void)resetWithListTableOrdering:(NSArray *)listStationOrdering_{
    isReloading=YES;
    if (listStationOrdering_ != listStationOrdering) {
        [listStationOrdering removeAllObjects];
        [listStationOrdering addObjectsFromArray:listStationOrdering_];
    }
    [self reSortListStationOrdering];
    [_tbvMain reloadData];
    [self reselectedCell];
    isReloading=NO;
}
-(void)reSortListStationOrdering{
    switch (sortType) {
        case SortTypeByStationName:
            [self sortListStationOrderingByStationName:sortByDescending];
            break;
        case SortTypeBySessionTime:
            [self sortListStationOrderingBySessionTime:sortByDescending];
            break;
        case SortTypeByWaitingTime:
            [self sortListStationOrderingByWaitingTime:sortByDescending];
            break;
        case SortTypeByStatus:
            [self  sortListStationOrderingByStatus:sortByDescending];
            break;
        default:
            break;
    }
}
-(void)sortListStationOrderingByStationName:(BOOL)descending{
    if (descending) {
        [listStationOrdering sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            StationOrdering *stOrder1=obj1;
            StationOrdering *stOrder2=obj2;
            return [stOrder2.stationName compare:stOrder1.stationName];
        }];
    }else{
        [listStationOrdering sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            StationOrdering *stOrder1=obj1;
            StationOrdering *stOrder2=obj2;
            return [stOrder1.stationName compare:stOrder2.stationName];
        }];
    }

}
-(void)sortListStationOrderingByStatus:(BOOL)descending{
    if (descending) {
        [listStationOrdering sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            StationOrdering *stOrder1=obj1;
            StationOrdering *stOrder2=obj2;
            TypeStatusOrdering status1=stOrder1.getTypeStatusStationOrdering;
            TypeStatusOrdering status2=stOrder2.getTypeStatusStationOrdering;
            if (status1==status2) {
                return NSOrderedSame;
            }else if(status1 > status2){
                return NSOrderedAscending;
            }else{
                return NSOrderedDescending;
            }
        }];
    }else{
        [listStationOrdering sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            StationOrdering *stOrder1=obj1;
            StationOrdering *stOrder2=obj2;
            TypeStatusOrdering status1=stOrder1.getTypeStatusStationOrdering;
            TypeStatusOrdering status2=stOrder2.getTypeStatusStationOrdering;
            if (status1==status2) {
                return NSOrderedSame;
            }else if(status1 < status2){
                return NSOrderedAscending;
            }else{
                return NSOrderedDescending;
            }
        }];
    }
}
-(void)sortListStationOrderingByWaitingTime:(BOOL)descending{
    if (descending) {
        [listStationOrdering sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            StationOrdering *stOrder1=obj1;
            StationOrdering *stOrder2=obj2;
            TypeStatusOrdering status1=stOrder1.getTypeStatusStationOrdering;
            TypeStatusOrdering status2=stOrder2.getTypeStatusStationOrdering;
            if (status1== TypeStatusOrderingEmpty || status1== TypeStatusOrderingOccupied) {
                return NSOrderedDescending;
            }
            if (status2== TypeStatusOrderingEmpty || status2== TypeStatusOrderingOccupied) {
                return NSOrderedAscending;
            }
            if (status1 != TypeStatusOrderingWaiting) {
                return NSOrderedDescending;
            }
            if (status2 != TypeStatusOrderingWaiting) {
                return NSOrderedAscending;
            }
            return [stOrder1.startWaitingDate compare:stOrder2.startWaitingDate];
        }];
    }else{
        [listStationOrdering sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            StationOrdering *stOrder1=obj1;
            StationOrdering *stOrder2=obj2;
            TypeStatusOrdering status1=stOrder1.getTypeStatusStationOrdering;
            TypeStatusOrdering status2=stOrder2.getTypeStatusStationOrdering;
            if (status1== TypeStatusOrderingEmpty || status1== TypeStatusOrderingOccupied) {
                return NSOrderedDescending;
            }
            if (status2== TypeStatusOrderingEmpty || status2== TypeStatusOrderingOccupied) {
                return NSOrderedAscending;
            }
            if (status1 != TypeStatusOrderingWaiting) {
                return NSOrderedDescending;
            }
            if (status2 != TypeStatusOrderingWaiting) {
                return NSOrderedAscending;
            }
            return [stOrder2.startWaitingDate compare:stOrder1.startWaitingDate];
        }];
    }
}
-(void)sortListStationOrderingBySessionTime:(BOOL)descending{
    if (descending) {
        [listStationOrdering sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            StationOrdering *stOrder1=obj1;
            StationOrdering *stOrder2=obj2;
            TypeStatusOrdering status1=stOrder1.getTypeStatusStationOrdering;
            TypeStatusOrdering status2=stOrder2.getTypeStatusStationOrdering;
            if (status1== TypeStatusOrderingEmpty || status1== TypeStatusOrderingOccupied) {
                return NSOrderedDescending;
            }
            if (status2== TypeStatusOrderingEmpty || status2== TypeStatusOrderingOccupied) {
                return NSOrderedAscending;
            }
            return [stOrder1.startWaitingDate compare:stOrder2.startWaitingDate];
        }];
    }else{
        [listStationOrdering sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            StationOrdering *stOrder1=obj1;
            StationOrdering *stOrder2=obj2;
            TypeStatusOrdering status1=stOrder1.getTypeStatusStationOrdering;
            TypeStatusOrdering status2=stOrder2.getTypeStatusStationOrdering;
            if (status1== TypeStatusOrderingEmpty || status1== TypeStatusOrderingOccupied) {
                return NSOrderedDescending;
            }
            if (status2== TypeStatusOrderingEmpty || status2== TypeStatusOrderingOccupied) {
                return NSOrderedAscending;
            }
            return [stOrder2.startWaitingDate compare:stOrder1.startWaitingDate];
        }];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)reselectedCell{
    // reselected cell
    StationOrdering *stationOrderingSelected =[_delegate listTableOrdering:self getStationOrderingSelected:YES];
    if (stationOrderingSelected) {
        NSInteger indexExisted=[listStationOrdering indexOfObject:stationOrderingSelected];
        if (indexExisted !=NSNotFound) {
            [_tbvMain selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexExisted inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    _lbNoOrdering.hidden=(listStationOrdering.count != 0);
    return listStationOrdering.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellOrdering *cell=[tableView dequeueReusableCellWithIdentifier:CellOrderingIndentify];
    [cell setValueDefault];
    StationOrdering *stationOrdering=listStationOrdering[indexPath.row];
    TypeStatusOrdering status=[stationOrdering getTypeStatusStationOrdering];
    [cell setStatus:status];
    cell.lbStatus.text=[StationOrdering typeStringByType:status];
    cell.lbStation.text=[stationOrdering stationName];
    if (status != TypeStatusOrderingEmpty && status != TypeStatusOrderingOccupied) {
        cell.lbName.text=[stationOrdering customerName];
        cell.lbOrderCode.text=[stationOrdering orderCode];
        cell.lbItem.text=[NSString stringWithFormat:@"%li",(long)[stationOrdering totalProductItem]];
        cell.startWaitingDate=[stationOrdering startWaitingDate];
        [cell reShowWaitingTime];
    }else if (status == TypeStatusOrderingOccupied){
       cell.lbName.text=[stationOrdering customerName];
    }
    return cell;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return _vHeaderTable;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_delegate) {
        [_delegate listTableOrdering:self clickDetail:listStationOrdering[indexPath.row]];
    }
}
-(void)reshowWaitingTime:(id)sender{
    if (!isReloading) {
        //reshow cell visible
        for (TableCellOrdering *cell in [_tbvMain visibleCells]) {
            [cell reShowWaitingTime];
        }
    }
}
@end

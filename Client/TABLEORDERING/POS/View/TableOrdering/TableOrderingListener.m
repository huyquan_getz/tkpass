//
//  TableOrderingListener.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableOrderingListener.h"
#import "Controller.h"
#import "StationOrdering.h"
#import "TKTableOrderingSetting.h"
#import "QueueSendMail.h"
#import "TKOrder.h"
#import "SMSTemplate.h"
#import "TKStation.h"
#import "PlayAudio.h"
@implementation TableOrderingListener{
    BOOL theFirstLoad;
}
+(instancetype)sharedObject{
    static TableOrderingListener *_tableOrderingListener;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _tableOrderingListener=[[TableOrderingListener alloc] init];
    });
    return _tableOrderingListener;
}
-(instancetype)init{
    if (self=[super init]) {
        theFirstLoad=YES;
        startedListener=NO;
        listStations=[[NSMutableArray alloc] init];
        listOrderings=[[NSMutableArray alloc] init];
    }
    return self;
}
-(void)startListener{
    if (!startedListener) {
        [self reSetupQueryLive];
        startedListener=YES;
    }
}
-(void)reSetupQueryLive{
    [self removeQueryLive];
    // query station
    CBLQuery *queryStation=[Controller queryAllStation];
    liveQueryStation=[queryStation asLiveQuery];
    [liveQueryStation addObserver:self forKeyPath:@"rows" options:0 context:NULL];
    [liveQueryStation start];
    //query ordering
    CBLQuery *queryOrdering=[Controller queryAllOrdering];
    liveQueryOrdering=[queryOrdering asLiveQuery];
    [liveQueryOrdering addObserver:self forKeyPath:@"rows" options:0 context:NULL];
    [liveQueryOrdering start];
}
-(void)removeQueryLive{
    if (liveQueryStation) {
        [liveQueryStation stop];
        [liveQueryStation removeObserver:self forKeyPath:@"rows"];
        liveQueryStation=nil;
    }
    if (liveQueryOrdering) {
        [liveQueryOrdering stop];
        [liveQueryOrdering removeObserver:self forKeyPath:@"rows"];
        liveQueryOrdering=nil;
    }
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (object == liveQueryStation) {
        [self updateStation:liveQueryStation.rows];
    }else if (object == liveQueryOrdering){
        [self updateOrdering:liveQueryOrdering.rows];
    }
}
-(void)updateStation:(CBLQueryEnumerator*)enumRows{
    NSMutableArray *stations=[[NSMutableArray alloc] init];
    for (CBLQueryRow *row in enumRows) {
        [stations addObject:row.document];
    }
    NSLog(@"have %ld station",stations.count);
    NSArray *newStations=[self checkStationNewsWithNewList:stations];
    [listStations removeAllObjects];
    [listStations addObjectsFromArray:stations];
    NSArray *dataShow=[self dataShow];
    NSArray *newStationOrderings=[self subNewStationOrderings:dataShow withOrderingNews:newStations];
    if (_delegate) {
        [_delegate tableOrderingListener:self listStationOrderings:dataShow stationOrderingNews:newStationOrderings];
    }
}
-(void)updateOrdering:(CBLQueryEnumerator*)enumRows{
    NSMutableArray *orderings=[[NSMutableArray alloc] init];
    for (CBLQueryRow *row in enumRows) {
        [orderings addObject:row.document];
        // NSLog(@"print stationOrdering %@",row.document.properties);
    }
    NSLog(@"have %ld ordering",orderings.count);
    NSArray *newOrderings=[self checkOrderingNewsWithNewList:orderings];
    if (newOrderings.count>0 && theFirstLoad==NO) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self haveNewOrdering:newOrderings];
        });
    }else{
        theFirstLoad=NO;
    }
    [listOrderings removeAllObjects];
    [listOrderings addObjectsFromArray:orderings];
    NSArray *dataShow=[self dataShow];
    NSArray *newStationOrderings=[self subNewStationOrderings:dataShow withOrderingNews:newOrderings];
    if (_delegate) {
        [_delegate tableOrderingListener:self listStationOrderings:dataShow stationOrderingNews:newStationOrderings];
    }
}

-(NSArray*)subNewStationOrderings:(NSArray*)stations withOrderingNews:newOrderings{
    NSMutableArray *newStationOrderings=[[NSMutableArray alloc] init];
    for (StationOrdering *stOrd in stations) {
        if ([newOrderings containsObject:stOrd.order]) {
            [newStationOrderings addObject:stOrd];
        }
    }
    return newStationOrderings;
}
-(NSArray*)subNewStationOrderings:(NSArray*)stations withStationsNews:newStations{
    NSMutableArray *newStationOrderings=[[NSMutableArray alloc] init];
    for (StationOrdering *stOrd in stations) {
        if ([newStations containsObject:stOrd.station]) {
            [newStations addObject:stOrd];
        }
    }
    return newStationOrderings;
}
-(NSArray*)dataShow{
    NSMutableArray *stationOrderings=[[NSMutableArray alloc] init];
    for (CBLDocument *station in listStations) {
        NSArray *listOrderByStationId=[self listOrderingByStationId:station.documentID from:listOrderings];
        if (listOrderByStationId.count>0) {
            for (CBLDocument *ordering in listOrderByStationId) {
                [stationOrderings addObject:[[StationOrdering alloc] initWithStation:station order:ordering]];
            }
            // add station ordering when there more occupied in this station
            if ([TKStation getStatusStation:station] == TypeStatusOrderingOccupied) {
                // check order existed
                BOOL orderExistedInList=NO;
                NSString *orderId=[TKStation orderId:station];
                if (orderId) {
                    CBLDocument *orderDoc=[[SyncManager sharedSyncManager] documentWithDocumentId:orderId];
                    if (orderDoc && [listOrderings containsObject:orderDoc]) {
                        orderExistedInList=YES;
                    }
                }
                if (!orderExistedInList) {
                    [stationOrderings addObject:[[StationOrdering alloc] initWithStation:station order:nil]];
                }
            }
        }else{
            [stationOrderings addObject:[[StationOrdering alloc] initWithStation:station order:nil]];
        }
    }
    return stationOrderings;
}
-(NSArray*)listOrderingByStationId:(NSString*)stationId from:(NSArray*)orderings{
    NSMutableArray *listOrderByStation=[[NSMutableArray alloc] init];
    for (CBLDocument *ordering in orderings) {
        if ([[TKOrder getStationId:ordering] isEqualToString:stationId]) {
            [listOrderByStation addObject:ordering];
        }
    }
    return listOrderByStation;
}
-(NSArray *)shareListStationOrderings{
    return [self dataShow];
}
-(NSArray*)checkOrderingNewsWithNewList:(NSArray*)newList{
    NSMutableArray *newOrderings=[newList mutableCopy];
    [newOrderings removeObjectsInArray:listOrderings];
    return newOrderings;
}
-(NSArray*)checkStationNewsWithNewList:(NSArray*)newList{
    NSMutableArray *newStations=[newList mutableCopy];
    [newStations removeObjectsInArray:listStations];
    return newStations;
}

-(void)haveNewOrdering:(NSArray*)newOrderings{
    //play audio ring ring
    // check allow Sound
    if ([Controller allowShowWhenHaveNewOrder]) {
        [PlayAudio playAudioNewOrdering];
    }
    //check auto status
    if ([self checkAutoAcceptConfirmOrdering]) {
        NSMutableArray *listOrderingWaitings=[[NSMutableArray alloc] init];
        for (CBLDocument *ordering in newOrderings) {
            if ([TKOrder orderStatus:ordering] == OrderStatusDineInWaiting) {
                [listOrderingWaitings addObject:ordering];
            }
        }
        [Controller changeStatusWaitingToConfirm:listOrderingWaitings];
    }
}
-(BOOL)checkAutoAcceptConfirmOrdering{
    CBLDocument *orderingSetting=[Controller getTableOrderingSetting];
    return [TKTableOrderingSetting getAllowAutoAcceptOrder:orderingSetting];
}


-(void)dealloc{
    [liveQueryStation removeObserver:self forKeyPath:@"rows"];
    [liveQueryOrdering removeObserver:self forKeyPath:@"rows"];
}
@end

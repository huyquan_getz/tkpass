//
//  CashierBusinessModel.h
//  POS
//
//  Created by Nha Duong Cong on 10/20/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "ProductOrderSingle.h"
#import "TKOrder.h"
#import "EmailData.h"
#import "TKTaxServiceCharge.h"
#import "TKAmountDisplay.h"
#import "AdhocProduct.h"

@interface CashierBusinessModel : NSObject{
    TKAmountDisplay *amountDisplay;
    NSMutableArray *listProductOrder;
    NSMutableArray *listAdhocProduct;
    NSArray *listTax;
    TKTaxServiceCharge *serviceCharge;
    TKDiscount *discount;
}
@property (readonly)TKAmountDisplay *amountDisplay;
@property (readonly)TKTaxServiceCharge *serviceCharge;
@property (readonly) NSArray *listTax;
@property (readonly) TKDiscount *discount;
-(instancetype)initServiceDefault;
-(instancetype)initWithDiscountTotal:(TKDiscount*)discount_ listTax:(NSArray*)listTax_ serviceCharge:(TKTaxServiceCharge*)serviceCharge_ currency:(NSString*)currency_;

-(void)resetListProductOrder:(NSArray*)listProductOrder_;
-(void)resetListAdhocProduct:(NSArray*)listAdhocProduct_;
-(void)setDiscountTotal:(TKDiscount*)discount_;
-(double)subTotal;
-(double)subTotalHaveServiceCharge;
-(double)total;
-(double)grandTotal;
-(double)roundAmount;
-(double)totalDiscount;
-(double)totalCartDiscountValue;
-(double)totalTax;
-(double)totalServiceCharges;
-(CBLDocument*)createNewOrder;

-(double)totalDiscountOfProductOrder:(ProductOrderSingle*)productOrder;
-(double)totalAmountCrudeOfProductOrder:(ProductOrderSingle*)productOrder;
-(double)totalAmountHaveDiscountOfProductOrder:(ProductOrderSingle*)productOrder;

-(double)totalDiscountOfAdhocProduct:(AdhocProduct*)adhocProduct;
-(double)totalAmountCrudeOfAdhocProduct:(AdhocProduct*)adhocProduct;
-(double)totalAmountHaveDiscountOfAdhocProduct:(AdhocProduct*)adhocProduct;

//PUBLIC
+(double)totalDiscountOfProductOrder:(ProductOrderSingle*)productOrder;
+(double)totalAmountCrudeOfProductOrder:(ProductOrderSingle*)productOrder;
+(double)totalAmountHaveDiscountOfProductOrder:(ProductOrderSingle*)productOrder;

+(double)totalDiscountOfAdhocProduct:(AdhocProduct*)adhocProduct;
+(double)totalAmountCrudeOfAdhocProduct:(AdhocProduct*)adhocProduct;
+(double)totalAmountHaveDiscountOfAdhocProduct:(AdhocProduct*)adhocProduct;

+(NSString*)titleVariantOfProductOrder:(ProductOrderSingle*)productOrder;
+(double)discountAmount:(double)amount withDiscount:(TKDiscount*)discount;
+(double)taxServiceAmount:(double)amount withTaxService:(TKTaxServiceCharge*)taxService;

+(NSDictionary*)taxJSONWithTotalAmount:(double)amount withTax:(TKTaxServiceCharge*)tax;
+(NSArray*)listTaxesJSONWithTotalAmount:(double)amount withListTaxes:(NSArray*)listTaxes;
+(NSDictionary*)serviceChargeJSONWithTotalAmount:(double)amount withServiceCharge:(TKTaxServiceCharge*)serviceCharge;

+(NSDictionary*)mergeProductOrderSameProduct:(NSArray*)productOrderSameProduct;
+(NSArray*)arrayProductOrderWithProductOrdersJSON:(NSArray*)listProductOrderJSON;
+(NSArray*)arrayAdhocProductWithAdhocProductJSON:(NSArray*)listAdhocProductJSON;
+(NSArray*)arrayAdhocProductJSONWithAdhocProduct:(NSArray *)listAdhocProduct;
+(NSString*)orderCodeWithDate:(NSDate*)date;


@end

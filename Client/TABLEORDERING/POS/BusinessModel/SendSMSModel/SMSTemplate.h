//
//  SMSTemplate.h
//  TABLEORDERING
//
//  Created by Nguyen Anh Dao (Zin) on 3/18/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
typedef enum _TypeMessageSMS{
    TypeMessageSMSNone=-1,
    TypeMessageSMSConfirmed = 0,    // if status of ordering is confirm
    TypeMessageSMSReject = 1,       // if status of ordering is waiting and user cancel this order
    TypeMessageSMSCancel = 2,       // if status of ordering is confirm and user cancel  this order
    TypeMessageSMSReady = 3,        // if status of ordering is ready
    TypeMessageSMSAmend = 4,        // if status of ordering is amend
    
}TypeMessageSMS;

#import <Foundation/Foundation.h>
@interface SMSTemplate : NSObject
+(NSString*)messageOfOrder:(NSInteger)status firstName:(NSString*)firstName tableName:(NSString*)tableName message:(NSString*) message;
@end

//
//  SMSTemplate.m
//  TABLEORDERING
//
//  Created by Nguyen Anh Dao (Zin) on 3/18/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#import "SMSTemplate.h"
#import "Constant.h"
@implementation SMSTemplate
+(NSString*)messageOfOrder:(NSInteger)status firstName:(NSString*)firstName tableName:(NSString*)tableName message:(NSString*) message
{
    NSString *messageSend;
    switch (status) {
        case TypeMessageSMSReject:
            messageSend = [NSString stringWithFormat:@"Hi %@,\nYour order for %@ has been rejected.\n We will do a refund back to you.",firstName,tableName];
            break;
        case TypeMessageSMSCancel:
            messageSend = [NSString stringWithFormat:@"Hi %@,\nYour order for %@ has been cancel.\n We will make refund back to you.",firstName,tableName];
            break;
        case TypeMessageSMSConfirmed:
              messageSend = [NSString stringWithFormat:@"Hi %@,\nYour order for %@ has been confirmed.",firstName,tableName];
            break;
       
        case TypeMessageSMSReady:
            messageSend = [NSString stringWithFormat:@"Hi %@,\nYour order for %@ is ready.",firstName,tableName];
            break;
        case TypeMessageSMSAmend:
        {
            // message will append here
            // first character update to lowercase.
            NSString *firstCharacter=[message substringWithRange:NSMakeRange(0, 1)];
            message = [message stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[firstCharacter lowercaseString]];
            messageSend = [NSString stringWithFormat:@"Hi %@, %@ We will do a refund back to you.",firstName,message];
            break;
        }
        default:
            
            break;
    }
    return messageSend;
}
@end

//
//  PaymentBusinessModel.m
//  POS
//
//  Created by Nha Duong Cong on 12/1/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "PaymentBusinessModel.h"
#import "TKOrder.h"
#import "TKAmountDisplay.h"

@implementation PaymentBusinessModel

+(double)totalChargeWithListPaymentChild:(NSArray *)listChargeChildren{
    double totalCharge=0;
    for (PaymentChild *paymentChild in listChargeChildren) {
        totalCharge += paymentChild.charged;
    }
    return [TKAmountDisplay tkRoundCash:totalCharge];
}
+(double)totalChangeDueWithListPaymentChild:(NSArray*)listChargeChildren{
    double totalChangeDue=0;
    for (PaymentChild *pm in listChargeChildren) {
        totalChangeDue+=pm.changeDue;
    }
    return [TKAmountDisplay tkRoundCash:totalChangeDue];
}
+(double)totalCustomerSentWithListPaymentChild:(NSArray*)listChargeChildren{
    double totalCustomerSent=[self totalChangeDueWithListPaymentChild:listChargeChildren] + [self totalChangeDueWithListPaymentChild:listChargeChildren];
    return [TKAmountDisplay tkRoundCash:totalCustomerSent];
}
+(NSArray *)subListPaymentChildWithListPaymentChild:(NSArray *)listChargeChildren isPayCash:(BOOL)isPayCash{
    NSMutableArray *array=[[NSMutableArray alloc] init];
    for (PaymentChild *paymentChild in listChargeChildren) {
        if ([paymentChild.paymentType isEqualToString:StringPaymentTypeCash] == isPayCash) {
            [array addObject:paymentChild];
        }
    }
    return array;
}
+(NSArray *)subListPaymentChildWithListPaymentChild:(NSArray *)listChargeChildren paymentType:(NSString *)paymentType{
    NSMutableArray *array=[[NSMutableArray alloc] init];
    for (PaymentChild *paymentChild in listChargeChildren) {
        if ([paymentChild.paymentType isEqualToString:paymentType]) {
            [array addObject:paymentChild];
        }
    }
    return array;
}
+(NSArray *)listPaymentTypeWithListPaymentChild:(NSArray *)listChargeChildren{
    NSMutableArray *listPaymentTypeDifferent=[[NSMutableArray alloc] init];
    for (PaymentChild *paymentChild in listChargeChildren) {
        BOOL existed=NO;
        for (NSString *typeExisted in listPaymentTypeDifferent) {
            if ([paymentChild.paymentType isEqualToString:typeExisted]) {
                existed=YES;
                break;
            }
        }
        if (!existed) {
            [listPaymentTypeDifferent addObject:paymentChild.paymentType];
        }
    }
    return listPaymentTypeDifferent;
}
+(CBLDocument*)completedOrder:(CBLDocument *)orderDocument withListPaymentChildren:(NSArray *)listPaymentChildren changeDue:(double)changeDue{
    NSMutableArray *arrayJsonPayment=[[NSMutableArray alloc] init];
    for (PaymentChild *paymentChild in listPaymentChildren) {
        [arrayJsonPayment addObject:[paymentChild jsonData]];
    }
    return [[SyncManager sharedSyncManager] updateDocument:orderDocument propertyDoc:@{@"arrayPayment":arrayJsonPayment,@"changeDue":@(changeDue),@"paymentStatus":@(PaymentStatusPaid)}];
}
+(CBLDocument*)removePaymentInfoWithOrder:(CBLDocument*)orderDocument{
    return [[SyncManager sharedSyncManager] updateDocument:orderDocument propertyDoc:@{@"arrayPayment":@[],@"changeDue":@(0),@"paymentStatus":@(PaymentStatusPending)}];
}
+(NSArray*)getListCreditPayment{
    return @[@"Master Card",@"Visa",@"NETs"];
}
@end

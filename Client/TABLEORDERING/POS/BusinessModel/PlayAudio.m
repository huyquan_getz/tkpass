//
//  PlayAudio.m
//  TABLEORDERING
//
//  Created by Cong Nha Duong on 3/11/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "PlayAudio.h"

@implementation PlayAudio
+(instancetype)sharedObject{
    static PlayAudio *_playAudio;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _playAudio=[[PlayAudio alloc] init];
    });
    return _playAudio;
}
-(instancetype)init{
    if (self =[super init]) {
        listAudio=[[NSMutableArray alloc] init];
    }
    return self;
}
-(void)addAudio:(Audio *)audio{
    audio.delegate=self;
    [listAudio addObject:audio];
}
#pragma AudioDelegate
-(void)audio:(id)sender beginInterruption:(BOOL)none{
    [listAudio removeObject:sender];
}
-(void)audio:(id)sender finishWithError:(NSError *)error{
    [listAudio removeObject:sender];
    NSLog(@"audio %lu",(unsigned long)listAudio.count);
}
+(void)playAudioNewOrdering{
    Audio *audio=[[Audio alloc] initWithFileName:@"new-ordering" ofType:@"mp3"];
    [[PlayAudio sharedObject] addAudio:audio];
    [audio playingWithLoop:NO];
}
@end

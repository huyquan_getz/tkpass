//
//  CouchBaseBusinessModel.m
//  POS
//
//  Created by Nha Duong Cong on 10/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define ViewSearchEmloyeeID @"searchEmployeeID" //0
#define ViewAllCategory @"allCategory" //1
#define ViewAllShiftDrawer @"allShiftDrawer" //2
#define ViewAllProductSortByCategorySortKeyNormal @"allProductSortByCategorySortKeyNormal" //3
#define ViewAllProduct @"allProduct" //4
#define ViewSearchProduct @"searchProductByName" //5
#define ViewAllReceiptByLimitTime @"allReceiptByLimitTime" //6
#define ViewSearchReceiptByOrderCode @"searchReceiptByOrderCode" //7
#define ViewAllOrderWaiting @"allOrderWaiting" //8
#define ViewAllPrinter @"allPrinter" //9
#define ViewAllPrinting @"allPrinting" //10
#define ViewSearchProductBySKU @"productBySKU" //11
#define ViewTaxService @"allTaxService" //12
#define ViewGeneralSetting @"generalSetting" //13
#define ViewAllStation @"allStation" //14
#define ViewTableOrderingSetting @"tableOrderingSetting" //15
#define ViewAllOrdering @"allOrdering"//16
#define ViewAllListDevicesSetting @"allListDevicesSetting" //17

#import "CouchBaseBusinessModel.h"
#import "UserDefaultModel.h"
#import "TKOrder.h"
#import "Controller.h"

@implementation CouchBaseBusinessModel
+(CBLQuery *)querySearchEmployeePinCode:(NSString *)titleSearch{
    NSString *titleView=ViewSearchEmloyeeID;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view=    [[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView compareMap:@{tkKeyTable:tkUserCashierTable} keySearch:tkKeyEmployeeId];
    CBLQuery *query =[view createQuery];
    // search only some letter
    query.fullTextQuery = [NSString stringWithFormat:@"%@",titleSearch];
    query.fullTextSnippets=NO;
    query.fullTextRanking = YES;
    query.descending=YES;
    return query;
}
+(CBLQuery *)queryGetAllCategory{
    NSString *titleView =ViewAllCategory;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView * view= [[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView titleKey:tkKeyTable titleCompare:tkCategoryTable keyMain:tkSortKeyNormal];
    CBLQuery *query =[view createQuery];
    query.descending=YES;
    return query;
    
}

+(CBLQuery *)queryShiftDrawerInYear:(NSInteger)year month:(NSInteger)month{
    NSString *titleView =ViewAllShiftDrawer;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view =[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([doc[tkKeyTable] isEqualToString:tkShiftDrawerTable]) {
            return YES;
        }
        return NO;
    } keyMain:tkKeyModifiedDate];
    CBLQuery *query =[view createQuery];
    NSDate *dateEnd=[NSDate dateWithYear:year month:month day:1 timeZone:[NSTimeZone localTimeZone]];
    NSDate *dateStart =[NSDate dateWithYear:year month:month day:[NSDate numberDaysOfYear:year month:month] timeZone:[NSTimeZone localTimeZone] hour:23 minute:59 second:59];
    query.startKey=[CBLJSON JSONObjectWithDate:dateStart];
    query.endKey=[CBLJSON JSONObjectWithDate:dateEnd];
    query.descending=YES;
    return query;
}
+(CBLQuery*)queryAllProductByCategoryIDAndSortKeyMain{
    NSString *titleView =ViewAllProductSortByCategorySortKeyNormal;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view=[[SyncManager sharedSyncManager] viewMapReduceWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        NSString *table=[doc[tkKeyTable] convertNullToNil];
        if ([table isEqualToString:tkProductItemTable] && [doc[tkKeyIndexStore] integerValue]== [Controller storeIndexDefault]) {
            return YES;
        }
        return NO;
    } keyMain1:tkKeyCategoryId keyMain2:tkSortKeyNormal];
    CBLQuery *query =[view createQuery];
    query.descending=YES;
    query.mapOnly=YES;
    return query;
}
+(CBLQuery *)queryAllProductSortKeyMain{
    NSString *titleView=ViewAllProduct;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view =[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([doc[tkKeyTable] isEqualToString:tkProductItemTable] && [doc[tkKeyIndexStore] integerValue]== [Controller storeIndexDefault]) {
            return YES;
        }
        return NO;
            
    } keyMain:tkSortKeyNormal];
    CBLQuery *query =[view createQuery];
    query.descending=YES;
    return query;
}
+(CBLQuery *)querySearchProductItemWithTitleKey:(NSString *)titleSearch{
    NSString *titleView=ViewSearchProduct;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view =[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([doc[tkKeyTable] isEqualToString:tkProductItemTable] && [doc[tkKeyIndexStore] integerValue]==[Controller storeIndexDefault]) {
            return YES;
        }
        return NO;
    } keySearch:tkKeyTitle];
    CBLQuery *query =[view createQuery];
    // search only some letter
    query.fullTextQuery = [NSString stringWithFormat:@"%@*",titleSearch];
    query.fullTextSnippets=NO;
    query.fullTextRanking = YES;
    query.descending=YES;
    return query;
}

+(CBLQuery *)queryTKGetReceiptLimitDateInMonth:(NSInteger)numberMonthBefore{
    NSString *titleView =ViewAllReceiptByLimitTime;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view=[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        NSString *table=[doc[tkKeyTable] convertNullToNil];
        if ([table isEqualToString:tkOrderTable]) {
            if ([doc[tkKeyOrderStatus] integerValue]== OrderStatusCollected || [doc[tkKeyOrderStatus] integerValue]== OrderStatusRefund || [doc[tkKeyOrderStatus] integerValue]== OrderStatusCanceled) {
                return YES;
            }
        }
        return NO;
    } keyMain:tkKeyModifiedDate];
    CBLQuery *query =[view createQuery];
    NSDate *date=[NSDate date];
    NSDate *dateBefore3Month =[NSDate tkDateStartBeforeIn:numberMonthBefore toCurrentDate:date];
    query.descending=YES;
    query.endKey=[CBLJSON JSONObjectWithDate:dateBefore3Month];
    return query;
}
+(CBLQuery *)querySearchReceiptWithTitleKey:(NSString *)titleSearch{
    NSString *titleView=ViewSearchReceiptByOrderCode;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view=[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        NSString *table=[doc[tkKeyTable] convertNullToNil];
        if ([table isEqualToString:tkOrderTable]) {
            if ([doc[tkKeyOrderStatus] integerValue]== OrderStatusCollected || [doc[tkKeyOrderStatus] integerValue]== OrderStatusRefund || [doc[tkKeyOrderStatus] integerValue]== OrderStatusCanceled) {
                return YES;
            }
        }
        return NO;
    } keySearch:tkKeyOrderCode];
    CBLQuery *query =[view createQuery];
    // search only some letter
    query.fullTextQuery = [NSString stringWithFormat:@"%@*",titleSearch];
    query.fullTextSnippets=NO;
    query.fullTextRanking = YES;
    query.descending=YES;
    return query;
}

+(CBLQuery *)queryGetAllOrderWaiting{
    NSString *titleView =ViewAllOrderWaiting;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view =[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        NSString *table=[doc[tkKeyTable] convertNullToNil];
        if ([table isEqualToString:tkOrderTable]) { // table order
            if ([doc[tkKeyInventory] convertNullToNil] && [[doc[tkKeyInventory] objectForKey:tkKeyIndexStore] integerValue]==[Controller storeIndexDefault]) { // my store
                NSInteger orderStatus=[doc[tkKeyOrderStatus] integerValue];
                NSInteger paymentStatus =[doc[tkKeyPaymentStatus]integerValue];
                NSInteger orderType =[doc[tkKeyOrderType] integerValue];
                if ((orderStatus == OrderStatusPendingWaiting && orderType==OrderTypeSelfCollect)|| (orderType==OrderTypeSelfCollect && paymentStatus == PaymentStatusPaid && (orderStatus==OrderStatusOrdered || orderStatus== OrderStatusReadyCollection))) {
                    return YES;
                }
            }

        }
        return NO;
    } keyMain:tkSortKeyNormal];
    CBLQuery *query =[view createQuery];
    query.descending=YES;
    return query;
}

+(CBLQuery *)queryGetAllPrinter{
    NSString *titleView =ViewAllPrinter;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView * view= [[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([doc[tkKeyTable] isEqualToString:tkPrinterTable]) {
            return YES;
        }
        return NO;
    } keyMain:tkKeyCreatedDate];
    CBLQuery *query =[view createQuery];
    query.descending=NO;
    return query;
}
+(CBLQuery *)queryGetAllPrinting{
    NSString *titleView =ViewAllPrinting;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView * view= [[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([doc[tkKeyTable] isEqualToString:tkPrintingTable]) {
            return YES;
        }
        return NO;
    } keyMain:tkKeyCreatedDate];
    CBLQuery *query =[view createQuery];
    query.descending=NO;
    return query;
}
+(CBLQuery *)querySearchProductBySKU:(NSString *)sku{
    NSString *titleView=ViewSearchProductBySKU;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view =[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([doc[tkKeyTable] isEqualToString:tkProductItemTable] && [doc[tkKeyIndexStore] integerValue]==[Controller storeIndexDefault]) {
            return YES;
        }
        return NO;
    } keySearch:tkKeySKU];
    CBLQuery *query =[view createQuery];
    // search only some letter
    query.fullTextQuery = [NSString stringWithFormat:@"%@",sku];
    query.fullTextSnippets=NO;
    query.fullTextRanking = YES;
    query.descending=YES;
    return query;
}
+(CBLDocument *)productWithSKU:(NSString *)sku{
    CBLQuery *query=[self querySearchProductBySKU:sku];
    CBLQueryEnumerator *queryEnum=[query run:nil];
    if (queryEnum && queryEnum.count>0) {
        return [queryEnum nextRow].document;
    }
    return nil;
}
+(CBLQuery *)queryAllTaxServiceCharge{
    NSString *titleView;
    CBLView *view;
    titleView=ViewTaxService;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    view =[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([[doc[tkKeyTable] convertNullToNil] isEqualToString:tkTaxTable]) {
            return YES;
        }
        return NO;
        
    } keyMain:tkSortKeyNormal];
    CBLQuery *query =[view createQuery];
    query.descending=YES;
    return query;
}
+(CBLQuery *)queryTableOrderingSetting{
    NSString *titleView;
    CBLView *view;
    titleView=ViewTableOrderingSetting;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    view =[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([[doc[tkKeyTable] convertNullToNil] isEqualToString:tkTableOrderingSettingTable]) {
            return YES;
        }
        return NO;
        
    } keyMain:tkSortKeyNormal];
    CBLQuery *query =[view createQuery];
    query.descending=YES;
    return query;
}
+(CBLQuery *)queryAllGeneralSetting{
    NSString *titleView;
    CBLView *view;
    titleView=ViewGeneralSetting;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    view =[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([[doc[tkKeyTable] convertNullToNil] isEqualToString:tkMerchantGeneralSetting]) {
            return YES;
        }
        return NO;
        
    } keyMain:tkSortKeyNormal];
    CBLQuery *query =[view createQuery];
    query.descending=YES;
    return query;
}
+(CBLDocument*)taxServiceSetting{
    CBLQuery *query=[self queryAllTaxServiceCharge];
    CBLQueryEnumerator *queryEnum=[query run:nil];
    if (queryEnum && queryEnum.count>0) {
        return [queryEnum nextRow].document;
    }
    return nil;
}
+(CBLDocument *)generalSetting{
    CBLQuery *query=[self queryAllGeneralSetting];
    CBLQueryEnumerator *queryEnum=[query run:nil];
    if (queryEnum && queryEnum.count>0) {
        return [queryEnum nextRow].document;
    }
    return nil;
}

+(CBLDocument *)tableOrderingSetting{
    CBLQuery *query=[self queryTableOrderingSetting];
    CBLQueryEnumerator *queryEnum=[query run:nil];
    if (queryEnum && queryEnum.count>0) {
        return [queryEnum nextRow].document;
    }
    return nil;
}

+(CBLQuery *)queryAllStation{
    NSString *titleView=ViewAllStation;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view =[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([doc[tkKeyTable] isEqualToString:tkStationTable]) {
            return YES;
        }
        return NO;
        
    } keyMain:tkSortKeyNormal];
    CBLQuery *query =[view createQuery];
    query.descending=YES;
    return query;
}
+(CBLQuery *)queryAllOrdering{
    NSString *titleView=ViewAllOrdering;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view =[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([doc[tkKeyTable] isEqualToString:tkOrderTable] && [doc[tkKeyOrderType] integerValue] == OrderTypeDineIn) {
            OrderStatus  status=(OrderStatus)[doc[tkKeyOrderStatus] integerValue];
            if (status==OrderStatusDineInWaiting || status == OrderStatusDineInConfirm || status==OrderStatusDineInServing) {
                return YES;
            }
        }
        return NO;
        
    } keyMain:tkSortKeyNormal];
    CBLQuery *query =[view createQuery];
    query.descending=YES;
    return query;
}
+(CBLQuery *)queryGetAllDevicesSetting{
    NSString *titleView=ViewAllListDevicesSetting;
#if DEBUG
    //[[SyncManager sharedSyncManager] deleteViewWithViewName:titleView]; // reset view to query
#endif
    CBLView *view =[[SyncManager sharedSyncManager] viewListDocumentWithTitleView:titleView blockCondition:^BOOL(NSDictionary *doc) {
        if ([doc[tkKeyTable] isEqualToString:tkListDevicesSettingTable]) {
            return YES;
        }
        return NO;
        
    } keyMain:tkSortKeyNormal];
    CBLQuery *query =[view createQuery];
    query.descending=YES;
    return query;
}
+(CBLDocument *)listDevicesSetting{
    CBLQuery *query=[self queryGetAllDevicesSetting];
    CBLQueryEnumerator *queryEnum=[query run:nil];
    if (queryEnum && queryEnum.count>0) {
        return [queryEnum nextRow].document;
    }
    return nil;
}
+(BOOL)reloadView:(ViewType)viewType{
    SyncManager *sync=[SyncManager sharedSyncManager];
    NSString *viewName;
    CBLQuery *query;
    switch (viewType) {
        case ViewTypeSearchEmloyeeID:{
            viewName= ViewSearchEmloyeeID;
            [sync deleteViewWithViewName:viewName];
            query=[self querySearchEmployeePinCode:@"0"];
            break;
        }
//        case ViewTypeAllCategory:{
//            viewName=ViewAllCategory;
//            [sync deleteViewWithViewName:viewName];
//            query=[self queryGetAllCategory];
//            break;
//        }
//        case ViewTypeAllShiftDrawer:{
//            viewName=ViewAllShiftDrawer;
//            [sync deleteViewWithViewName:viewName];
//            query=[self queryShiftDrawerInYear:2015 month:1];
//            break;
//        }
//        case ViewTypeAllProductSortByCategorySortKeyNormal:{
//            viewName=ViewAllProductSortByCategorySortKeyNormal;
//            [sync deleteViewWithViewName:viewName];
//            query=[self queryAllProductByCategoryIDAndSortKeyMain];
//            break;
//        }
//        case ViewTypeAllProduct:{
//            viewName=ViewAllProduct;
//            [sync deleteViewWithViewName:viewName];
//            query=[self queryAllProductSortKeyMain];
//            break;
//        }
//        case ViewTypeSearchProduct:{
//            viewName=ViewSearchProduct;
//            [sync deleteViewWithViewName:viewName];
//            query=[self querySearchProductItemWithTitleKey:@"z"];
//            break;
//        }
//        case ViewTypeAllReceiptByLimitTime:{
//            viewName=ViewAllReceiptByLimitTime;
//            [sync deleteViewWithViewName:viewName];
//            query=[self queryTKGetReceiptLimitDateInMonth:1];
//            break;
//        }
//        case ViewTypeSearchReceiptByOrderCode:{
//            viewName=ViewSearchReceiptByOrderCode;
//            [sync deleteViewWithViewName:viewName];
//            query=[self querySearchReceiptWithTitleKey:@"z"];
//            break;
//        }
//        case ViewTypeAllOrderWaiting:{
//            viewName=ViewAllOrderWaiting;
//            [sync deleteViewWithViewName:viewName];
//            query=[self queryGetAllOrderWaiting];
//            break;
//        }
        case ViewTypeAllPrinter:{
            viewName=ViewAllPrinter;
            [sync deleteViewWithViewName:viewName];
            query=[self queryGetAllPrinter];
            break;
        }
        case ViewTypeAllPrinting:{
            viewName=ViewAllPrinting;
            [sync deleteViewWithViewName:viewName];
            query=[self queryGetAllPrinting];
            break;
        }
//        case ViewTypeSearchProductBySKU:{
//            viewName=ViewSearchProductBySKU;
//            [sync deleteViewWithViewName:viewName];
//            query=[self querySearchProductBySKU:@"z"];
//            break;
//        }
        case ViewTypeGeneralSetting:{
            viewName=ViewGeneralSetting;
            [sync deleteViewWithViewName:viewName];
            query=[self queryAllGeneralSetting];
            break;
        }
        case ViewTypeTaxService:{
            viewName=ViewTaxService;
            [sync deleteViewWithViewName:viewName];
            query=[self queryAllTaxServiceCharge];
            break;
        }
        case ViewTypeAllStation:{
            viewName=ViewAllStation;
            [sync deleteViewWithViewName:viewName];
            query=[self queryAllStation];
            break;
        }
        case ViewTypeAllOrdering:{
            viewName=ViewAllOrdering;
            [sync deleteViewWithViewName:viewName];
            query=[self queryAllOrdering];
            break;
        }
        case ViewTypeAllTableOrderingSetting:{
            viewName=ViewTableOrderingSetting;
            [sync deleteViewWithViewName:viewName];
            query=[self queryTableOrderingSetting];
            break;
        }
        case ViewTypeAllListDevices:{
            viewName=ViewAllListDevicesSetting;
            [sync deleteViewWithViewName:viewName];
            query=[self queryGetAllDevicesSetting];
            break;
        }
        default:
            break;
    }
    if (query) {
        [query run:nil];
    }
    return YES;
}


@end

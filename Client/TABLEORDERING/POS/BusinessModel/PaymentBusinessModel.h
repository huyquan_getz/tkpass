//
//  PaymentBusinessModel.h
//  POS
//
//  Created by Nha Duong Cong on 12/1/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentChild.h"
#import "Constant.h"

@interface PaymentBusinessModel : NSObject
+(double)totalChargeWithListPaymentChild:(NSArray*)listChargeChildren;
+(double)totalChangeDueWithListPaymentChild:(NSArray*)listChargeChildren;
+(double)totalCustomerSentWithListPaymentChild:(NSArray*)listChargeChildren;
+(NSArray*)subListPaymentChildWithListPaymentChild:(NSArray*)listChargeChildren isPayCash:(BOOL)isPayCash;

+(NSArray*)subListPaymentChildWithListPaymentChild:(NSArray *)listChargeChildren paymentType:(NSString *)paymentType;

+(NSArray *)listPaymentTypeWithListPaymentChild:(NSArray *)listChargeChildren;

+(NSArray*)getListCreditPayment;
@end

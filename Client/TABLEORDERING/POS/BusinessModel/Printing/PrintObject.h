//
//  PrintObject.h
//  POS
//
//  Created by Hoang Van Quynh on 13/01/2015.
//  Copyright (c) Năm 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrintObjectData.h"
@interface PrintObject : NSObject
- (void)addPrintData:(PrintData*)printData;
- (BOOL)openDrawer;
-(id)initWithPrintObjectData:(PrintObjectData*)printObjectData_;
- (BOOL)checkPrinterOnline;
-(void)closePrinter;
@property(nonatomic,strong)PrintObjectData *printObjectData;
@property (nonatomic ,strong)  NSMutableArray *data;
@end



//
//
//  PrintingTemplateManagement.m
//  POS
//
//  Created by Cong Nha Duong on 1/15/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define FormatDateTime @"MM-dd-yyyy  --  HH:mm:ss"
#import "PrintingTemplateManagement.h"
#import "Constant.h"
#import "PrintingTemplateReceipt.h"
#import "CellProductInReceipt.h"
#import "CellViewAmount.h"
#import "CashierBusinessModel.h"
#import "PaymentBusinessModel.h"
#import "PaymentChild.h"
#import "Controller.h"
#import "Printing.h"
#import "PrinterController.h"


@implementation PrintingTemplateManagement{
    LanguageUtil *languageKey;
    TKAmountDisplay *amountDisplay;
}
+(instancetype)sharedObject{
    __strong static PrintingTemplateManagement* _printingTemplateManagement;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _printingTemplateManagement = [[PrintingTemplateManagement alloc] init];
    });
    return _printingTemplateManagement;
}
-(instancetype)init{
    if (self =[super init]) {
        languageKey=[LanguageUtil sharedLanguageUtil];
        amountDisplay =[[TKAmountDisplay alloc]initWithCurrency:[Controller currencyDefault]];
    }
    return self;
}
-(UIImage *)printingSingleOrderChit:(NSArray *)listProductOrder listAdhoc:(NSArray *)listAdhoc withOrderInfo:(NSDictionary *)info totalOder:(BOOL)totalOrder{
    /*
     info: orderCode, createdDate, employeeName,
     */
    PrintingTemplateReceipt *template=[[PrintingTemplateReceipt alloc] initWithLoadView];
    template.infoOrderId.text=info[@"orderCode"];
    template.infoStaff.text=info[@"employeeName"];
    NSDate *createdDate=info[tkKeyCreatedDate];
    template.infoDate.text=[createdDate getStringWithFormat:FormatDateTime];
    if (totalOrder) {
        template.infoName.text=[languageKey stringByKey:@"print.total.print-display"];
    }else{
        template.infoName.text=[languageKey stringByKey:@"print.single.print-display"];
    }
    CGPoint startPoint=CGPointZero;
    [template addInfoViewAt:&startPoint];
    [template addSpaceDefaultAt:&startPoint loop:2];
    [template addLineAt:&startPoint center:YES];
    [template addSpaceDefaultAt:&startPoint loop:4];
    startPoint.x=20;
    //product single
    for (ProductOrderSingle *singleProductOrder in listProductOrder) {
        CellViewAmount *cell=[[CellViewAmount alloc] initWithTitle:[NSString stringWithFormat:@"%li",(long)singleProductOrder.quantity] value:[CashierBusinessModel titleVariantOfProductOrder:singleProductOrder] bold:NO];
        [cell setFrame:CGRectMake(0, 0, template.view.frame.size.width,30)];
        cell.font=tkBoldFontMainWithSize(26);
        cell.ratio=0.15;
        cell.lbValue.textAlignment=NSTextAlignmentLeft;
        if ([cell numberLinesWidthFont:tkBoldFontMainWithSize(26)]==2) {
            CGRect frame=cell.frame;
            frame.size.height=65;
            cell.frame=frame;
        }
        [template addView:cell at:&startPoint];
        [template addSpaceDefaultAt:&startPoint];
    }
    //adhoc product
    for (AdhocProduct *adhocProduct in listAdhoc) {
        CellViewAmount *cell=[[CellViewAmount alloc] initWithTitle:[NSString stringWithFormat:@"%li",(long)adhocProduct.quantity] value:adhocProduct.nameWithVariant bold:NO];
        [cell setFrame:CGRectMake(0, 0, template.view.frame.size.width,30)];
        cell.font=tkBoldFontMainWithSize(26);
        cell.ratio=0.15;
        cell.lbValue.textAlignment=NSTextAlignmentLeft;
        if ([cell numberLinesWidthFont:tkBoldFontMainWithSize(26)]==2) {
            CGRect frame=cell.frame;
            frame.size.height=65;
            cell.frame=frame;
        }
        [template addView:cell at:&startPoint];
        [template addSpaceDefaultAt:&startPoint];
    }
    
    //bottom
    startPoint.x=0;
    [template addSpaceDefaultAt:&startPoint loop:4];
    CGRect frame=template.view.frame;
    frame.size.height=startPoint.y;
    template.view.frame=frame;
    return [UIImage imageWithUIView:template.view];
}

-(UIImage *)printingImageWithReceipt:(CBLDocument *)receipt receiptCopy:(BOOL)receiptCopy reprint:(BOOL)reprint{
    PrintingTemplateReceipt *template=[[PrintingTemplateReceipt alloc] initWithLoadView];
    template.bottomThank.text=[languageKey stringByKey:@"print.receipt.ms-thanks"];
    if (receiptCopy) {
        template.bottomName.text=@"***CUSTOMER COPY***";
    }else{
        template.bottomName.text=@"***CUSTOMER***";
    }
    if (reprint) {
        template.infoName.text=[NSString stringWithFormat:@"%@-%@",[languageKey stringByKey:@"print.receipt.print-display"],[languageKey stringByKey:@"print.receipt.text-reprint"]];
    }else{
        template.infoName.text=[languageKey stringByKey:@"print.receipt.print-display"];
    }
    template.infoOrderId.text=[TKOrder orderCode:receipt];
    template.infoStaff.text=[TKOrder employeeName:receipt];
    NSDate *createdDate=[TKOrder createdDate:receipt];
    template.infoDate.text=[createdDate getStringWithFormat:FormatDateTime];
    CGPoint startPoint=CGPointZero;
    [template setValueTopViewDefault];
    [template addTopViewAt:&startPoint];
    [template addSpaceDefaultAt:&startPoint];
    
    //Refund
    if ([TKOrder orderStatus:receipt]==OrderStatusRefund) {
        if (reprint) {
            template.refundName.text=[NSString stringWithFormat:@"%@-%@",[languageKey stringByKey:@"print.receipt.refund-print-display"],[languageKey stringByKey:@"print.receipt.text-reprint"]];
        }else{
            template.refundName.text=[languageKey stringByKey:@"print.receipt.refund-print-display"];
        }
        template.refundDate.text=[[TKOrder refundDate:receipt] getStringWithFormat:FormatDateTime];
        template.refundStaff.text=[TKOrder refundEmployeeName:receipt];
        template.refundReason.text=[TKOrder refundDisplayReason:receipt];
        [template addRefundViewAt:&startPoint];
        [template addSpaceDefaultAt:&startPoint loop:2];
        template.infoName.text=[languageKey stringByKey:@"print.receipt.receipt-name-in-refund"];
        // add payment refund
        UILabel *pmInfo=[template labelDefault];
        pmInfo.text=[[languageKey stringByKey:@"print.receipt.lb-refund-info"] stringByAppendingString:@":"];
        [template addView:pmInfo at:&startPoint];
        [template addSpaceDefaultAt:&startPoint];
        
        startPoint.x=170;
        for (NSDictionary *paymentRefund in [self arrayPaymentRefundWithReceipt:receipt]) {
            CellViewAmount *cell=[[CellViewAmount alloc] initWithTitle:paymentRefund[@"title"] value:paymentRefund[@"value"] bold:NO];
            [cell setFrame:CGRectMake(0, 0, template.view.frame.size.width,25)];
            cell.font=tkFontMainWithSize(22);
            cell.ratio=1;
            [template addView:cell at:&startPoint];
            [template addSpaceDefaultAt:&startPoint];
        }
        startPoint.x=0;
        [template addLineAt:&startPoint center:YES];
        [template addSpaceDefaultAt:&startPoint loop:4];
        
    }
    startPoint.x=0;
    [template addInfoViewAt:&startPoint];
    [template addSpaceDefaultAt:&startPoint loop:3];
    
    NSDictionary *total=[self totalCharge:receipt];
    template.lbTotal.text=[NSString stringWithFormat:@"%@: %@",total[@"title"],total[@"value"]];
    // add product
    NSArray *listProductOrder=[CashierBusinessModel arrayProductOrderWithProductOrdersJSON:[TKOrder arrayProductJSON:receipt]];// split product to some variant
    NSArray *listAdhocProduct=[CashierBusinessModel arrayAdhocProductWithAdhocProductJSON:[TKOrder arrayAdhocProductJSON:receipt]];
    NSMutableArray *arrayDisplayProductSale=[[self arrayProductSaleWithListProductOrder:listProductOrder] mutableCopy];
    [arrayDisplayProductSale addObjectsFromArray:[self arrayAdhocSaleWithListAdhocProduct:listAdhocProduct]];
    for (NSDictionary *productSale in arrayDisplayProductSale) {
        CellProductInReceipt *cellPr;
        if ([productSale[@"haveDiscount"] boolValue]) {
            cellPr=[[CellProductInReceipt alloc] initWithQuantity:productSale[@"quantity"] title:productSale[@"title"] amount:productSale[@"amount"] discountTitle:productSale[@"titleDiscount"] amountDiscount:productSale[@"amountDiscount"]];
        }else{
            cellPr=[[CellProductInReceipt alloc] initWithQuantity:productSale[@"quantity"] title:productSale[@"title"] amount:productSale[@"amount"]];
        }
        cellPr.frame=CGRectMake(0, 0, template.view.frame.size.width, [cellPr maxHeight]);
        [template addView:cellPr at:&startPoint];
        [template addSpaceDefaultAt:&startPoint loop:2];
    }
    //add cart discount value
    if ([TKOrder discountCart:receipt]) {
        startPoint.x=67;
        NSString *amountCartDiscount=[NSString stringWithFormat:@"-%@",[amountDisplay stringAmountHaveCurrency:[TKOrder discountCartValue:receipt]]];
        NSDictionary *cartDiscountValue=@{@"title":@"Cart Discount",@"value":amountCartDiscount};
        CellViewAmount *cell=[[CellViewAmount alloc] initWithTitle:cartDiscountValue[@"title"] value:cartDiscountValue[@"value"] bold:NO];
        [cell setFrame:CGRectMake(0, 0, template.view.frame.size.width,25)];
        cell.font=tkFontMainWithSize(22);
        cell.ratio=1;
        [template addView:cell at:&startPoint];
        [template addSpaceDefaultAt:&startPoint];
    }
    //
    startPoint.x=170;
    [template addSpaceDefaultAt:&startPoint loop:2];
    [template addLineAt:&startPoint center:NO];
    [template addSpaceDefaultAt:&startPoint loop:2];
    
    for (NSDictionary *amountCell in [self arrayDiscountServiceCharge:receipt]) {
        CellViewAmount *cell=[[CellViewAmount alloc] initWithTitle:amountCell[@"title"] value:amountCell[@"value"] bold:NO];
        [cell setFrame:CGRectMake(0, 0, template.view.frame.size.width,25)];
        cell.font=tkFontMainWithSize(22);
        cell.ratio=1;
        [template addView:cell at:&startPoint];
        [template addSpaceDefaultAt:&startPoint];

    }
    // change due, round amount
    for (NSDictionary *amountCell in [self arrayRoundAmtChangeDue:receipt]) {
        CellViewAmount *cell=[[CellViewAmount alloc] initWithTitle:amountCell[@"title"] value:amountCell[@"value"] bold:NO];
        [cell setFrame:CGRectMake(0, 0, template.view.frame.size.width,25)];
        cell.font=tkFontMainWithSize(22);
        cell.ratio=1;
        [template addView:cell at:&startPoint];
        [template addSpaceDefaultAt:&startPoint];
        
    }
    
    startPoint.x=0;
    [template addSpaceDefaultAt:&startPoint loop:2];
    [template addTotalAt:&startPoint];
    [template addSpaceDefaultAt:&startPoint loop:2];
    startPoint.x=0;
    [template addLineAt:&startPoint center:NO];
    [template addSpaceDefaultAt:&startPoint loop:2];
    
    UILabel *lb=[template labelDefault];
    lb.text=[NSString stringWithFormat:@"%@:",[languageKey stringByKey:@"print.receipt.lb-payment-mode"]];
    lb.frame=CGRectMake(0,0, 140, 25);
    [template addView:lb at:&startPoint staticPoint:YES];
    //
    startPoint.x=170;
    
    NSArray *jsonArrayPayment=[TKOrder arrayPaymentJSON:receipt];
    NSMutableArray *listPaymentChild=[[NSMutableArray alloc] init];
    for (NSDictionary *jsonPaymentChild in jsonArrayPayment) {
        [listPaymentChild addObject:[[PaymentChild alloc] initFromJsonData:jsonPaymentChild]];
    }
    
    for (NSDictionary *amountCell in [self arrayPaymentWithListPaymentChild:listPaymentChild]) {
        CellViewAmount *cell=[[CellViewAmount alloc] initWithTitle:amountCell[@"title"] value:amountCell[@"value"] bold:NO];
        [cell setFrame:CGRectMake(0, 0, template.view.frame.size.width,25)];
        cell.font=tkFontMainWithSize(22);
        cell.ratio=1;
        [template addView:cell at:&startPoint];
        NSArray *arrayApprovalCodes=amountCell[@"approvalCodes"];
        for (NSString* aproval in arrayApprovalCodes) {
            UILabel *lbApv=[template labelDefault];
            lbApv.text=[NSString stringWithFormat:@"%@: %@",[languageKey stringByKey:@"print.receipt.lb-approval-code"],aproval];
            [template addView:lbApv at:&startPoint];
        }
        [template addSpaceDefaultAt:&startPoint];
        
    }
    
    //bottom
    startPoint.x=0;
    [template addSpaceDefaultAt:&startPoint loop:2];
    if (receiptCopy) {
        [template addBottomViewAt:&startPoint];
        [template addSpaceDefaultAt:&startPoint];
    }
    CGRect frame=template.view.frame;
    frame.size.height=startPoint.y;
    template.view.frame=frame;
    return [UIImage imageWithUIView:template.view];
}

-(NSArray*)arrayProductSaleWithListProductOrder:(NSArray*)listProductOrder{
    NSMutableArray *arrayPr=[[NSMutableArray alloc] init];
    for (ProductOrderSingle *productOrder in listProductOrder) {
        NSString *title=[CashierBusinessModel titleVariantOfProductOrder:productOrder];
        NSString *quantity = [NSString stringWithFormat:@"%ld",(long)productOrder.quantity];
        double totalCrude =[CashierBusinessModel totalAmountCrudeOfProductOrder:productOrder];
        NSMutableDictionary *dict=[@{@"title":title,@"amount":[amountDisplay stringAmountHaveCurrency:totalCrude],@"quantity":quantity,@"haveDiscount":@(NO)} mutableCopy];
        TKDiscount *discount=[productOrder discount];
        if (discount) {
            [dict setObject:@(YES) forKey:@"haveDiscount"];
            NSMutableString *discountShow=[[NSMutableString alloc]initWithString:discount.name];//[languageKey stringByKey:@"cashier.order.text-discount"]];
            if (discount.type==DiscountTypePercent) {
                [discountShow appendFormat:@" (%.2f%%)",discount.discountValue];
            }
            [dict setObject:discountShow forKey:@"titleDiscount"];
            NSString *amountDiscount=[NSString stringWithFormat:@"-%@",[amountDisplay stringAmountHaveCurrency:[CashierBusinessModel totalDiscountOfProductOrder:productOrder]]];
            [dict setObject:amountDiscount forKey:@"amountDiscount"];
        }
        [arrayPr addObject:dict];
        // discount haveDiscount  "titleDiscount","amountDiscount"
    }
    return arrayPr;
}
-(NSArray*)arrayAdhocSaleWithListAdhocProduct:(NSArray*)listAdhocProduct{
    NSMutableArray *arrayAdhoc=[[NSMutableArray alloc] init];
    for (AdhocProduct *adhoc in listAdhocProduct) {
        NSString *title=adhoc.nameWithVariant;
        NSString *quantity = [NSString stringWithFormat:@"%ld",(long)adhoc.quantity];
        double totalCrude =[CashierBusinessModel totalAmountCrudeOfAdhocProduct:adhoc];
        NSMutableDictionary *dict=[@{@"title":title,@"amount":[amountDisplay stringAmountHaveCurrency:totalCrude],@"quantity":quantity,@"haveDiscount":@(NO)} mutableCopy];
        TKDiscount *discount=[adhoc discountProductItem];
        if (discount) {
            [dict setObject:@(YES) forKey:@"haveDiscount"];
            NSMutableString *discountShow=[[NSMutableString alloc]initWithString:discount.name];//[languageKey stringByKey:@"cashier.order.text-discount"]];
            if (discount.type==DiscountTypePercent) {
                [discountShow appendFormat:@" (%.2f%%)",discount.discountValue];
            }
            [dict setObject:discountShow forKey:@"titleDiscount"];
            NSString *amountDiscount=[NSString stringWithFormat:@"-%@",[amountDisplay stringAmountHaveCurrency:[CashierBusinessModel totalDiscountOfAdhocProduct:adhoc]]];
            [dict setObject:amountDiscount forKey:@"amountDiscount"];
        }
        [arrayAdhoc addObject:dict];
        // discount haveDiscount  "titleDiscount","amountDiscount"
    }
    return arrayAdhoc;
}
-(NSArray*)arrayDiscountServiceCharge:(CBLDocument*)receiptDoc{
    NSMutableArray *arrayPr=[[NSMutableArray alloc] init];
    [arrayPr addObject:@{@"title":[languageKey stringByKey:@"cashier.order.text-total-discount"],
                         @"value":[amountDisplay stringAmountHaveCurrency:[TKOrder discountTotalAmount:receiptDoc]]}];
    
    NSString *subTotalCharg =[amountDisplay stringAmountHaveCurrency:[TKOrder subTotal:receiptDoc]];
    [arrayPr addObject:@{@"title":[languageKey stringByKey:@"receipt.rc-dt.lb.sub-total"],
                         @"value":subTotalCharg}];
    NSDictionary *serviceCharegeDisplay=[TKOrder serviceChargeDisplay:receiptDoc];
    if (serviceCharegeDisplay) {
        double serviceCharge=[serviceCharegeDisplay[@"serviceChargeValue"] doubleValue];
        [arrayPr addObject:@{@"title":[languageKey stringByKey:@"cashier.order.text-service-charges"],
                             @"value":[amountDisplay stringAmountHaveCurrency:serviceCharge]}];
    }
    for (NSDictionary *taxDisplay in [TKOrder listTaxDisplayes:receiptDoc]) {
        double tax=[taxDisplay[@"taxValue"] doubleValue];
        [arrayPr addObject:@{@"title":taxDisplay[@"taxName"],
                             @"value":[amountDisplay stringAmountHaveCurrency:tax]}];
    }
    return arrayPr;
}

-(NSDictionary*)totalCharge:(CBLDocument*)receiptDoc{
    // total charge round amount total
    NSString *totalCharge =[amountDisplay stringAmountHaveCurrency:[TKOrder grandTotal:receiptDoc]];
    return @{@"title":[languageKey stringByKey:@"receipt.rc-dt.lb.total"],@"value":totalCharge};
}
-(NSArray*)arrayPaymentWithListPaymentChild:(NSArray*)listPaymentChild{
    NSArray *listTypeDifferent =[PaymentBusinessModel listPaymentTypeWithListPaymentChild:listPaymentChild];
    NSMutableArray *arrayPr=[[NSMutableArray alloc] init];
    for (NSString *paymentType in listTypeDifferent) {
        NSArray *array =[PaymentBusinessModel subListPaymentChildWithListPaymentChild:listPaymentChild paymentType:paymentType];
        PaymentChild *firstObject=[array firstObject];
        NSString *title =[firstObject displayName];
        NSString *value =[amountDisplay stringAmountHaveCurrency:[PaymentBusinessModel totalChargeWithListPaymentChild:array]];
        NSMutableArray *listApprovalCodes=[[NSMutableArray alloc] init];
        if (![firstObject.paymentType isEqualToString:StringPaymentTypeCash]) {
            for (PaymentChild *pc in array) {
                [listApprovalCodes addObject:pc.approvedCode];
            }
        }
        [arrayPr addObject:@{@"title":title,@"value":value,@"approvalCodes":listApprovalCodes}];
    }
    return arrayPr;
}
-(NSArray*)arrayPaymentRefundWithReceipt:(CBLDocument*)receipt{
    NSArray *arrayPaymentJSON=[TKOrder arrayPaymentRefundJSON:receipt];
    NSMutableArray *listPaymentChild=[[NSMutableArray alloc] init];
    for (NSDictionary *json in arrayPaymentJSON) {
        [listPaymentChild addObject:[[PaymentChild alloc] initFromJsonData:json]];
    }
    NSArray *listTypeDifferent =[PaymentBusinessModel listPaymentTypeWithListPaymentChild:listPaymentChild];
    NSMutableArray *arrayPr=[[NSMutableArray alloc] init];
    for (NSString *paymentType in listTypeDifferent) {
        NSArray *array =[PaymentBusinessModel subListPaymentChildWithListPaymentChild:listPaymentChild paymentType:paymentType];
        PaymentChild *firstObject=[array firstObject];
        NSString *title =[firstObject displayName];
        NSString *value =[amountDisplay stringAmountHaveCurrency:[PaymentBusinessModel totalChargeWithListPaymentChild:array]];
        [arrayPr addObject:@{@"title":title,@"value":value}];
    }
    return arrayPr;
}
-(NSArray*)arrayRoundAmtChangeDue:(CBLDocument*)receiptDoc{
    NSMutableArray *arrayPr=[[NSMutableArray alloc] init];
    double roundAmout=[TKAmountDisplay tkRoundCash:[TKOrder roundAmount:receiptDoc]];
    double changeDue=[TKOrder changeDue:receiptDoc];
    if(![TKAmountDisplay isZero:roundAmout]){
        NSString *value;
        if (roundAmout>0) {
            value=[amountDisplay stringAmountHaveCurrency:roundAmout];
        }else{
            value=[NSString stringWithFormat:@"-%@",[amountDisplay stringAmountHaveCurrency:-roundAmout]];
        }
        [arrayPr addObject:@{@"title":[languageKey stringByKey:@"receipt.rc-dt.lb.round-amt"],@"value":value}];
    }
    if(![TKAmountDisplay isZero:changeDue] && changeDue>0){
        [arrayPr addObject:@{@"title":[languageKey stringByKey:@"receipt.rc-dt.lb.change-due"],@"value":[amountDisplay stringAmountHaveCurrency:changeDue]}];
    }
    return arrayPr;
}

-(void)printReceipt:(CBLDocument *)receipt rePrint:(BOOL)isReprint{
    [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
        CBLQuery *queryPrinter=[Controller queryGetAllPrinting];
        CBLQueryEnumerator *enumRowPrinter=[queryPrinter run:nil];
        
        for (CBLQueryRow *row in enumRowPrinter) {
            int type = [Printing getPrintingType:row.document ];
            if (type == PrintingTypeReceipt || type == PrintingTypeReceiptCopy) {
                UIImage *image = [[PrintingTemplateManagement sharedObject] printingImageWithReceipt:receipt receiptCopy:(type==PrintingTypeReceiptCopy) reprint:isReprint];
                NSString *printerIpAdress =[Printing getPrinterIpAddress:row.document ];
                [[PrinterController sharedInstance] addPrintWithImage:image Height:image.size.height Ip:printerIpAdress Type:0 Name:@"" Infor:nil];
            }
        }
    }];

}
@end

//
//  PrinterManagement.m
//  POS
//
//  Created by Cong Nha Duong on 1/12/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "PrinterManagement.h"
#import "PrinterController.h"
#import "Constant.h"
#import "ePOS-Print.h"
#import "ePOS-Print.h"
#import "PrintObject.h"
#import <SystemConfiguration/CaptiveNetwork.h>

@implementation PrinterManagement{
    NSMutableArray *printDatas;
    NSMutableArray *printObjectDatas;
    NSMutableArray *printObjects;
    NSMutableArray *printFails;
    UIAlertView *myAlertView;
    BOOL tryAgainPrint;
    NSString *ms;
}

- (id)init
{
    self = [super init];
    printObjects=[[NSMutableArray alloc] init];
    NSData *data = [NSData dataWithContentsOfFile:[NSHomeDirectory() stringByAppendingString:@"/Documents/printDatas.bin"]];
    printDatas = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if(printDatas==nil){
        printDatas=[[NSMutableArray alloc]init];
        [self savePrintDatas];
    }
    data = [NSData dataWithContentsOfFile:[NSHomeDirectory() stringByAppendingString:@"/Documents/PrintObjectDatas.bin"]];
    printObjectDatas = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if(printObjectDatas==nil){
        printObjectDatas=[[NSMutableArray alloc]init];
        [self savePrintObjectDatas];
    }
    count = 0;
    printFails=[[NSMutableArray alloc] init];
    return self;
    
}
- (void) closeAllPrinter {
    for(int i= 0;i<[printObjects count];i++){
        PrintObject *object=printObjects[i];
        [object closePrinter];
    }
    [EpsonIoFinder stop];
}
- (BOOL)openDrawerWithIP:(NSString*)ip{
    BOOL checkPrint=FALSE;
    for(int i= 0;i<[printObjects count];i++){
        PrintObject *object=printObjects[i];
        if([object.printObjectData.ip isEqualToString:ip]){
            return [object openDrawer];
            break;
        }
    }
    if(!checkPrint){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"Can not connect to cash drawer. Please check the cash drawer connection and try again."] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    return YES;
}


- (void)savePrintDatas{
    NSString *filename = [NSHomeDirectory() stringByAppendingString:@"/Documents/printDatas.bin"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:printDatas];
    [data writeToFile:filename atomically:YES];
}
- (void)savePrintObjectDatas{
    NSString *filename = [NSHomeDirectory() stringByAppendingString:@"/Documents/PrintObjectDatas.bin"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:printDatas];
    [data writeToFile:filename atomically:YES];
}
-(BOOL) checkConnectionWithPrinterWithIP:(NSString *)ipAddress port:(NSInteger)port{
    NSArray *printers = [self listPrint];
    BOOL isOnline  = NO;
    PrintObject *object;
    for (int i= 0; i < [printers count]; i++) {
        object=printers[i];
        if([object.printObjectData.ip isEqualToString:ipAddress]){
            isOnline = YES;
            break;
        }
    }
    if (isOnline) {
       isOnline = [object checkPrinterOnline];
    }
    return isOnline;
}


- (NSArray*)getAllPrintData{
    return printDatas;
}
- (NSArray *)listPrint{
    return printObjects;
}
- (id)fetchSSIDInfo {
    NSArray *ifs = (__bridge_transfer NSArray *)CNCopySupportedInterfaces();
    NSLog(@"Supported interfaces: %@", ifs);
    NSDictionary *info;
    for (NSString *ifnam in ifs) {
        info = (__bridge_transfer NSDictionary *)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        NSLog(@"%@ => %@", ifnam, info);
        if (info && [info count]) { break; }
    }
    return info;
}
- (void)processingPrint{
    [self searchPrint];
}
- (void)searchPrint{
    NSDictionary *dictData=[self fetchSSIDInfo];
    NSString *networName=@"";
    if(dictData !=nil&&[dictData objectForKey:@"SSID"])
        networName=[dictData objectForKey:@"SSID"];
    [SNLog Log:@"printObjectsFirst=%@",printObjects];
    [EpsonIoFinder start:EPSONIO_OC_DEVTYPE_TCP FindOption:@"255.255.255.255"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        int errStatus = EPSONIO_OC_SUCCESS;
        NSArray *array = [EpsonIoFinder getResult:&errStatus];
        //debug
        NSString *tmpLog=@"";
        if(array != nil && errStatus == EPSONIO_OC_SUCCESS){
            for (int  i = 0; i < printObjects.count; i++ ) {
                BOOL valiDate=NO;
                PrintObject *object=printObjects[i] ;
                for(int k=0;k<[array count];k++){
                    
                    if([object.printObjectData.ip isEqualToString:array[k]]){
                        valiDate=YES;
                        break;
                    }
                }
                if(!valiDate){
                    if(tmpLog.length==0)
                        tmpLog=[NSString stringWithFormat:@"%@",object.printObjectData.ip];
                    else tmpLog=[NSString stringWithFormat:@"%@,%@",tmpLog,object.printObjectData.ip];
                }
            }
//            if(tmpLog.length>0){
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"Can not find your printers (%@) on the network %@. Please check your network connection and try again.",tmpLog,networName] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Try Again",nil];
//                [alertView show];
//                return ;
//            }
        }
        if(array.count==0)
        {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"Can not find your printer on the network %@. Please check your network connection and try again.",networName] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Try Again",nil];
//            [alertView show];
        }else
            if(array != nil && errStatus == EPSONIO_OC_SUCCESS){
                if(tryAgainPrint){
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"All printers have been founded.Please select OK to continue." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                    tryAgainPrint=NO;
                }
                for (int  i = 0; i < array.count; i++ ) {
                    BOOL valiDate=NO;
                    for(int k=0;k<[printObjects count];k++){
                        PrintObject *object=printObjects[k] ;
                        if([object.printObjectData.ip isEqualToString:array[i]]){
                            valiDate=YES;
                            break;
                        }
                    }
                    if(!valiDate){
                        PrintObjectData *dataPrint=[[PrintObjectData alloc]initWithIp:array[i] Name:array[i]];
                        [self updatePrintObjectData:dataPrint atIndex:99999];
                        PrintObject *printObject=[[PrintObject alloc]initWithPrintObjectData:dataPrint];
                        [printObjects addObject:printObject];
                    }
                }
            }else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"Can not find your printer on the network %@. Please check your network connection and try again.",networName] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Try Again",nil];
                [alertView show];
            }
        NSLog(@"printObjectsEnd=%@",printObjects);
        [EpsonIoFinder stop];
    });
    
}

- (void)handlePrint{
    int countdata=0;
    for(int k=0;k<[printObjects count];k++){
        PrintObject *object=printObjects[k] ;
        if([object.data count]==0)
            countdata++;
    }
    if(countdata==printObjects.count){
        [self removeAllPrintData];
    }
}

- (void)removeAllPrintData{
    [printDatas removeAllObjects];
    //[self savePrintDatas];
}
- (void)addPrintData:(PrintData*)printData{
    BOOL checkPrint=FALSE;
    for(int i= 0;i<[printObjects count];i++){
        PrintObject *object=printObjects[i];
        if([object.printObjectData.ip isEqualToString:printData.ip]){
            [object addPrintData:printData];
            object.printObjectData.name = printData.name;
            checkPrint=TRUE;
            break;
        }
    }
    if(!checkPrint){
        [printFails addObject:printData];
        if (myAlertView == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                myAlertView = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Can not connect to printer %@. Please check the printer connection and try again.",printData.ip] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Try Again", nil];
                [myAlertView show];
            });
        }else {
            [myAlertView show];
        }
    }
}

- (void)updatePrintData:(PrintData*)printData atIndex:(int)index{
    if(index==0&&printDatas.count==0){
        [printDatas addObject:printData];
        printData.index=[NSString stringWithFormat:@"%lu",[printDatas count]-1];
    }
    else
        if (printDatas.count >= index){
            [printDatas replaceObjectAtIndex:index withObject:printData];
            printData.index=[NSString stringWithFormat:@"%i",index];
        }
        else {
            [printDatas addObject:printData];
            printData.index=[NSString stringWithFormat:@"%lu",[printDatas count]-1];
        }
    if(![printData.status boolValue])
        [[PrinterController sharedInstance] addPrintData:printData];
    //xu dung khi can backup App crack ko in duoc
    //[self savePrintDatas];
}


- (void)updatePrintObjectData:(PrintObjectData*)PrintObjectData atIndex:(int)index{
    if(index==0&&printObjectDatas.count==0)
        [printObjectDatas addObject:PrintObjectData];
    else
        if (printObjectDatas.count >= index)
            [printObjectDatas replaceObjectAtIndex:index withObject:PrintObjectData];
        else [printObjectDatas addObject:PrintObjectData];
    //[self savePrintObjectDatas];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex  == 1) {
        tryAgainPrint=TRUE;
        [self processingPrint];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self tryAgainPrintingOffline];
        });
        
    }else {
        [printFails removeAllObjects];
    }
}

#pragma mark Try Again Print Offline
- (void)tryAgainPrintingOffline {
    NSMutableArray *tmpArray = [[NSMutableArray alloc] initWithArray:printFails];
    [printFails removeAllObjects];
    for (int i =0; i< tmpArray.count; i++) {
        [self addPrintData:tmpArray[i]];
    }
    
}

@end

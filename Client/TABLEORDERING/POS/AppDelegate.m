//
//  AppDelegate.m
//  POS
//
//  Created by Nha Duong Cong on 10/13/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginVC.h"
#import "LoginCashierVC.h"
#import "UserDefaultModel.h"
#import "LanguageUtil.h"
#import "NetworkUtil.h"
#import "QueueSendMail.h"
#import "ImplementTestView.h"
#import "SyncManager.h"
#import "PrinterController.h"
#import "Controller.h"
#import "QueueWebserviceRealTime.h"
#import "SNLog.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    //set appDelegate
    [LanguageUtil sharedLanguageUtil];
    [NetworkUtil sharedInternetConnection].allowNotiWhenChangeStatusInternet=NO;
    [UserDefaultModel saveRequestPincodeWhenEnterForeground:NO];
    [[NetworkUtil sharedInternetConnection]  addDelegate:[QueueSendMail sharedQueue]];
    [[NetworkUtil sharedInternetConnection] addDelegate:[QueueWebserviceRealTime sharedQueue]];
    if (![UserDefaultModel wasFirstSyncCompleted]) {
        //create slideMenu for app
        _slideMenuController=[[SlideMenuController alloc] initWithCenterViewController:[[LoginVC alloc] init] leftDrawerViewController:nil];
    }else{
        //create slideMenu for app
        _slideMenuController=[[SlideMenuController alloc] initWithCenterViewController:[[LoginCashierVC alloc] init] leftDrawerViewController:nil];
        [self resetApplicationNumberBadge];
        [[SyncManager sharedSyncManager] startPush];
        [[SyncManager sharedSyncManager] startPull];
        [[QueueWebserviceRealTime sharedQueue] reCallUpdating];
    }
    // check show test view
#if (TARGET_IPHONE_SIMULATOR)
    if ([ImplementTestView sharedTestView].showTest) {
        _slideMenuController=[[SlideMenuController alloc] initWithCenterViewController:[ImplementTestView sharedTestView].viewShow leftDrawerViewController:nil];
    }
#endif
    [_slideMenuController setShowsShadow:YES];
    [_slideMenuController setRestorationIdentifier:@"MMDrawer"];
    [_slideMenuController setMaximumLeftDrawerWidth:350];
    [_slideMenuController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [_slideMenuController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[PrinterController sharedInstance] processingPrint];
    });
    self.window.rootViewController=_slideMenuController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];

    // register remote noti
    [AppDelegate registerAPNs:application];
    return YES;
}
+(void)registerAPNs:(UIApplication*)application{
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings =[ UIUserNotificationSettings settingsForTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge categories:nil];
        [application registerUserNotificationSettings:settings];
    }else{
        UIRemoteNotificationType myTypes=UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge;
        [application registerForRemoteNotificationTypes:myTypes];
    }
}
-(void)resetApplicationNumberBadge{
    UIApplication *application=[UIApplication sharedApplication];
    if (application.applicationIconBadgeNumber>0) {
        dispatch_queue_t myQueue=dispatch_queue_create("queue.resetApplicationNumberBadge", NULL);
        dispatch_async(myQueue, ^{
            BOOL result=[Controller requestResetApplicationNumberBagde];
            if (result) {
                [application setApplicationIconBadgeNumber:0];
            }else{
                // reupdate after 3 minute when fail
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2*60 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self resetApplicationNumberBadge];
                });
            }
            [SNLog Log:@"reset NumberBadge %@",(result)?@"success":@"fail"];
        });
    }
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    //[[PrinterController sharedInstance] closeAllPrinter];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self resetApplicationNumberBadge];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([[Controller sharedObject] isLogined] && [UserDefaultModel getRequestPincodeWhenEnterForeground]) {
            [[Controller sharedObject] showPincodeSecurity];
        }
    });
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
   // [[PrinterController sharedInstance] closeAllPrinter];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// push notification
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"Remote Data: %@",userInfo);
    //restart sync pull
    //check state application
    [Controller restartSyncPull];
    [self resetApplicationNumberBadge];
}
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"content---%@", token);
    [Controller saveDeviceToken:token];
}
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"Error register Remote Notification:%@",error);
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Fail to register remote notification. Do you want to try again?" delegate:nil cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    [alertView showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            [AppDelegate registerAPNs:application];
        }
    }];
}
-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    if (notificationSettings.types != UIUserNotificationTypeNone) {
        NSLog(@"did register user");
        [application registerForRemoteNotifications];
    }
}
//

+(CGSize)size{
    if ([self isPortrait]) {
        // portrait
        return [[UIScreen mainScreen] bounds].size;
    } else {
        // landscape
        CGSize size =[[UIScreen mainScreen] bounds].size;
        return CGSizeMake(size.height, size.width);
    }
    
    
}
+(CGRect)bounds{
    if ([self isPortrait]) {
        // portrait
        return [[UIScreen mainScreen] bounds];
    } else {
        // landscape
        CGRect bounds =[[UIScreen mainScreen] bounds];
        return CGRectMake(0, 0, bounds.size.height, bounds.size.width);
    }
}
+(UIInterfaceOrientation)orientation{
    return [[UIApplication sharedApplication] statusBarOrientation];
}
+(BOOL)isPortrait{
    return UIInterfaceOrientationIsPortrait([self orientation]);
}
-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)windowd{
    return UIInterfaceOrientationMaskAll;
}


@end

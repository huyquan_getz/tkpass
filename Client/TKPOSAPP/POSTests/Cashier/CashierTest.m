//
//  CashierTest.m
//  POS
//
//  Created by Nha Duong Cong on 10/17/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CashierVC.h"
#import "CouchBaseBusinessModel.h"

@interface CashierTest : XCTestCase{
    CashierVC *cashierVC;
    SyncManager *sync;
}

@end

@implementation CashierTest

- (void)setUp
{
    [super setUp];
    cashierVC=[[CashierVC alloc] initWithNibName:@"CashierVC" bundle:[NSBundle mainBundle]];
    [cashierVC performSelectorOnMainThread:@selector(loadView) withObject:nil waitUntilDone:YES];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    cashierVC=nil;
}


@end

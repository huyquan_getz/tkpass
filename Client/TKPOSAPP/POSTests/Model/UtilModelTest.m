//
//  UtilModelTest.m
//  POS
//
//  Created by Nha Duong Cong on 11/1/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UtilBusinessModel.h"
#import "HttpClient.h"
#import "UserDefaultModel.h"

@interface UtilModelTest : XCTestCase

@end

@implementation UtilModelTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
-(BOOL)compareArrayString:(NSArray*)arr1 andArrayString:(NSArray*)arr2{
    if (arr1.count==arr2.count) {
        for (int i=0;i<arr1.count;i++) {
            if (![arr1[i] isEqualToString:arr2[i]]) {
                return NO;
            }
        }
        return YES;
    }else{
        return NO;
    }
}
- (void)testGetSyncInfor
{
    NSDictionary *dict=@{tkKeyReturnStatus: @(ReturnCodeResponseInvalid),
                         tkKeyResult:@{
                                     @"expires_in": @(1209599),
                                     @"serverSync":@"http://couchbase.qgs.vn",
                                     @"databaseSync":@"sync_gateway",
                                     @"userAccessSync":@"congnha@qgs.com",
                                     @"passAccessSync":@"congnha@123!@#",
                                     @"channels":@[@"default"],
                                     @"stores":@[@{@"branch_name":@"Store1",@"channel":@"store1",@"storeId":@"store1"},
                                                 @{@"branch_name":@"Store 2",@"channel":@"store2",@"storeId":@"store2"}]
                                     }};
    [UserDefaultModel saveMerchantSyncInfor:dict[tkKeyResult]];
    NSString *url=[UtilBusinessModel stringUrlSyncData];
    XCTAssert([url isEqualToString:@"http://couchbase.qgs.vn:4984/sync_gateway"], "get string url Sync not true");
    
    NSArray *arrayChanels =[UtilBusinessModel channelsSync];
    XCTAssert([self compareArrayString:dict[tkKeyResult][@"channels"] andArrayString:arrayChanels], "get chanels not true");
    
    NSMutableArray *newChannels=[arrayChanels mutableCopy];
    [newChannels addObject:@"store1"];
    [UtilBusinessModel addChannels:@[@"store1"]];
    XCTAssert([self compareArrayString:[UtilBusinessModel channelsSync] andArrayString:newChannels], "add chanels not true");
}

@end

//
//  ReceiptVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/9/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Controller.h"
#import "ListReceiptVC.h"
#import "ReceiptDetailVC.h"
#import "SendEmailReceipt2VC.h"
#import "RefundVC.h"

@interface ReceiptVC : UIViewController<ListReceiptDelegate,SendMailReceipt2Delegate,RefundDelegate,UISearchBarDelegate>{
    NSMutableArray *listReceipt;
    CBLLiveQuery *liveQuery;
    NSString *oldSearchKey;
    BOOL isShowSearching;
    
    CBLDocument *receiptSelected;
    
}
@property (strong, nonatomic) IBOutlet UIView *vSearch;
@property (weak, nonatomic) IBOutlet UIButton *btCancelSearch;
- (IBAction)clickCancelSearch:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *lbNoPlaceOrder;
@property (weak, nonatomic) IBOutlet UIImageView *ivNaviBar;
@property (weak, nonatomic) IBOutlet UIButton *btMenu;
- (IBAction)clickMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleListReceipt;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleInformation;
- (IBAction)clickSendMail:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btSendMail;
@property (strong,nonatomic)SendEmailReceipt2VC *sendEmailVC;
@property (weak, nonatomic) IBOutlet UIView *vFrameListReceipt;
@property (weak, nonatomic) IBOutlet UIView *vFrameReceiptDetail;
@property (strong, nonatomic) ListReceiptVC *listReceiptVC;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (strong, nonatomic) ReceiptDetailVC  *receiptDetailVC;
@property (strong,nonatomic) RefundVC *refundVC;
@property (weak, nonatomic) IBOutlet UIButton *btPrint;
- (IBAction)clickPrint:(id)sender;
- (IBAction)clickSearch:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btRefund;
- (IBAction)clickRefund:(id)sender;
@end

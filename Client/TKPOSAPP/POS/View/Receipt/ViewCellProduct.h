//
//  ViewCellProduct.h
//  POS
//
//  Created by Cong Nha Duong on 3/2/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewCellProduct : UIView
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lbAmount;
@property (weak, nonatomic) IBOutlet UILabel *lbVarian;
@property (weak, nonatomic) IBOutlet UILabel *lbDiscount;
@property (strong, nonatomic) IBOutlet ViewCellProduct *view;
@property (weak, nonatomic) IBOutlet UILabel *lbAmountDiscount;
@property (weak, nonatomic) IBOutlet UIView *vProduct;
-(void)setShowVariant:(BOOL)variant showDiscount:(BOOL)discount;

@end

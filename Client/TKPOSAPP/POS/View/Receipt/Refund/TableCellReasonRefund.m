//
//  TableCellReasonRefund.m
//  POS
//
//  Created by Cong Nha Duong on 1/22/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableCellReasonRefund.h"
#import "Constant.h"

@implementation TableCellReasonRefund

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _ivTick.hidden=!selected;
    // Configure the view for the selected state
}
-(void)setValueDefault{
    _lbTitle.text=@"";
    UIView *bg=[[UIView alloc] init];
    bg.backgroundColor=tkColorFrameBorder;
    self.selectedBackgroundView=bg;
    _ivTick.hidden=YES;
    _vLine.backgroundColor=tkColorFrameBorder;
}
-(void)setTick:(BOOL)tick{
    _ivTick.hidden=!tick;
}
-(BOOL)currentTick{
    return !_ivTick.hidden;
}
@end

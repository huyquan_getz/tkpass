//
//  ListReceiptVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/9/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "Constant.h"
@protocol ListReceiptDelegate <NSObject>
@required
-(void)listReceipt:(id)sender clickDetail:(CBLDocument*)receipt;
-(CBLDocument*)listReceiptRequireReceiptSelected:(id)sender;

@end
#import <UIKit/UIKit.h>
#import "TableCellReceipt.h"

@interface ListReceiptVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>{
    NSMutableArray *listReceiptDay;
    NSString *currency;
}
@property (weak, nonatomic) IBOutlet UILabel *lbNoReceipt;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak,nonatomic) id<ListReceiptDelegate> delegate;
-(void)setListReceipt:(NSArray*)listReceipt fromSearch:(BOOL)search;
@end

//
//  SendEmailReceipt2VC.m
//  POS
//
//  Created by Nha Duong Cong on 12/12/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "SendEmailReceipt2VC.h"
#import "QueueSendMail.h"
#import "WebserviceJSON.h"
#import "TKOrder.h"
#import "Controller.h"

@interface SendEmailReceipt2VC ()

@end

@implementation SendEmailReceipt2VC{
    LanguageUtil *languageKey;
}
-(instancetype)initWithReceipt:(CBLDocument *)receipt_{
    if (self =[super init]) {
        receiptDoc=receipt_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_vMain setShadowDefault];
    [_vCover setViewCoverDefault];
    _btSend.layer.cornerRadius=tkCornerRadiusButton;
    [_btSend setBackgroundColor:tkColorMainAccept];
    UIView *vLeft=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 1)];
    UIView *vRight=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 140, 1)];
    _tfEmail.leftView=vLeft;
    _tfEmail.leftViewMode=UITextFieldViewModeAlways;
    _tfEmail.rightView=vRight;
    _tfEmail.rightViewMode=UITextFieldViewModeAlways;
    [self showMessageError:@""];
    _vTitle.backgroundColor=tkColorMainBackground;
    _vTitle.layer.cornerRadius=tkCornerRadiusViewPopup;
    _vLine.backgroundColor=tkColorFrameBorder;
    _tfEmail.backgroundColor=tkColorMainBackground;
    _tfEmail.layer.borderColor=tkColorFrameBorder.CGColor;
    _tfEmail.layer.borderWidth=1;
    _tfEmail.layer.cornerRadius=tkCornerRadiusButton;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    _lbTitle.text=[languageKey stringByKey:@"receipt.send-email.title-send-receipt"];
}
-(void)viewDidAppear:(BOOL)animated{
    [_tfEmail becomeFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tapCoverView:(id)sender{
    [self clickCancel:nil];
}
- (IBAction)clickCancel:(id)sender {
    [_delegate sendMainReceipt2:self completed:NO inQueue:NO];
}
#pragma TextfieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect frame=self.vMain.frame;
    frame.origin.y-= 200;
    [self.vMain setFrame:frame];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    CGRect frame=self.vMain.frame;
    frame.origin.y+= 200;
    [self.vMain setFrame:frame];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self clickSend:nil];
    return YES;
}

-(BOOL)validateInput{
    if ([_tfEmail isEmptyText]) {
        [self showMessageError:[languageKey stringByKey:@"cashier.order.send-email.email-empty"]];
        return NO;
    }
    if (![_tfEmail.text isEmailFormat]) {
        [self showMessageError:[languageKey stringByKey:@"cashier.order.send-email.email-invalid"]];
        return NO;
    }
    return YES;
}
-(BOOL)isEmailEmpty{
    return [_tfEmail isEmptyText];
}
-(void)hideKeyboard{
    [self.view endEditing:YES];
}
-(void)clearStatus{
    _lbMessageStatus.text=@"";
}
-(void)showMessageError:(NSString*)error{
    _lbMessageStatus.textColor=[UIColor redColor];
    _lbMessageStatus.text=error;
}
-(void)showMessageSuccess:(NSString*)message{
    _lbMessageStatus.textColor=[UIColor blueColor];
    _lbMessageStatus.text=message;
}
- (IBAction)clickSend:(id)sender {
    if (!_delegate) {
        return;
    }
    if (![self validateInput]) {
        return;
    }
    [self hideKeyboard];
    [self clearStatus];
    if (![NetworkUtil checkInternetConnection]) {
        [[NetworkUtil sharedInternetConnection]  addDelegate:[QueueSendMail sharedQueue]];
        [[QueueSendMail sharedQueue] addToQueueReceipt:receiptDoc.properties toEmail:_tfEmail.text type:[self mailType]];
        UIAlertView  *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-message"] message:[languageKey stringByKey:@"cashier.order.send-email.send-email-later"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.ok"] otherButtonTitles: nil];
        [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [_delegate sendMainReceipt2:self completed:NO inQueue:YES];
        }];
        return;
    }
    MBProgressHUD *hub=[[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hub];
    hub.labelText=[languageKey stringByKey:@"cashier.order.send-email.text-sending"];
    [hub show:YES];
    dispatch_queue_t myQueue=dispatch_queue_create("queue.SendEmail", NULL);
    dispatch_async(myQueue, ^{
        BOOL sendSuccess=[self sendEmailReceipt];
        dispatch_async(dispatch_get_main_queue(), ^{
            [hub hide:YES];
            [hub removeFromSuperview];
            if (sendSuccess) {
                //show alert and done payment
                UIAlertView  *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-message"] message:[languageKey stringByKey:@"cashier.order.send-email.ms-send-receipt-success"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.ok"] otherButtonTitles: nil];
                [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    [_delegate sendMainReceipt2:self completed:sendSuccess inQueue:NO];
                }];
            }else{
                [self showMessageError:[languageKey stringByKey:@"cashier.order.send-email.ms-send-receipt-fail"]];
            }
        });
    });
}

-(MailType)mailType{
    OrderStatus status=[TKOrder orderStatus:receiptDoc];
    switch (status) {
        case OrderStatusCanceled:
            return MailTypeReSendCancel;
            break;
        case OrderStatusCollected:
            return MailTypeReSendCollected;
            break;
        case OrderStatusRefund:
            return MailTypeReSendRefund;
            break;
        default:
            break;
    }
    return MailTypeNone;
}
-(BOOL)sendEmailReceipt{
    return [Controller sendReceipt:receiptDoc.properties toEmail:_tfEmail.text type:[self mailType]];
}
@end

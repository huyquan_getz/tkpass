//
//  TableCellReceipt.m
//  POS
//
//  Created by Nha Duong Cong on 12/9/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TableCellReceipt.h"
#import "Constant.h"

@implementation TableCellReceipt

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        _lbTitle.textColor=[UIColor whiteColor];
        _lbAmount.textColor=[UIColor whiteColor];
        _lbTime.textColor=[UIColor whiteColor];
        _ivStatusRefund.image=_imgStatusSelected;
    }else{
        _lbTitle.textColor=[UIColor blackColor];
        _lbAmount.textColor=[UIColor darkGrayColor];
        _lbTime.textColor=[UIColor blackColor];
        _ivStatusRefund.image=_imgStatusDeselected;
    }
    // Configure the view for the selected state
}
-(void)setValueDefault{
    UIView *bgSelected=[[UIView alloc] init];
    [bgSelected setBackgroundColor:tkColorMainSelected];
    [super setSelectedBackgroundView:bgSelected];
    _vLine.backgroundColor=tkColorFrameBorder;
    _ivStatusRefund.image=nil;
    _imgStatusSelected=nil;
    _imgStatusDeselected=nil;
}
@end

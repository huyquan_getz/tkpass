//
//  CellViewAmount.h
//  POS
//
//  Created by Nha Duong Cong on 12/11/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellViewAmount : UIView{
    NSString *title;
    NSString *value;
    BOOL bold;
}
@property (strong,nonatomic) UILabel *lbTitle;
@property (strong,nonatomic) UILabel *lbValue;
@property (strong,nonatomic) UIFont *font;
@property (assign) CGFloat ratio;
-(instancetype)initWithTitle:(NSString*)title value:(NSString*)value bold:(BOOL)bold;
-(void)setBold:(BOOL)bold_;
-(void)setTitle:(NSString*)title;
-(void)setValue:(NSString*)value;
-(NSInteger)numberLinesWidthFont:(UIFont*)font;
@end

//
//  ReceiptDetailVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/9/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define SpaceCellY 10
#define HeightCell 25
#define SpaceCellX 0
#define SpaceLeftRight 30
#define SpaceLeftAmount 320
#import "ReceiptDetailVC.h"
#import "CellViewAmount.h"
#include "TKOrder.h"
#import "CashierBusinessModel.h"
#import "PaymentBusinessModel.h"
#import "PaymentChild.h"
#import "Constant.h"
#import "PrintingTemplateManagement.h"
#import "PrinterController.h"
#import "Controller.h"
#import "Printing.h"
#import "Printer.h"
#import "ViewCellProduct.h"
@interface ReceiptDetailVC ()

@end

@implementation ReceiptDetailVC{
    NSArray *listProductOrder;
    NSArray *listAdhocProduct;
    NSMutableArray *listPaymentChild;
    LanguageUtil *languageKey;
    TKAmountDisplay *amountDisplay;
}
-(instancetype)initWithReceipt:(CBLDocument *)document{
    if (self = [super init]) {
        NSDictionary *dict=document.properties;
        [dict allKeys];
        receiptDoc=document;
        amountDisplay=[[TKAmountDisplay alloc] initWithCurrency:[Controller currencyDefault]];
    }
    return self;
}
-(instancetype)init{
    if (self=[super init]) {
        amountDisplay=[[TKAmountDisplay alloc] initWithCurrency:[Controller currencyDefault]];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *lines=@[_vLineDetail1,_vLineDetail2,_vLineDetail3,_vLineRefund1,_vLineRefund2,_vLineRefund3];
    for (UIView *vv in lines) {
        [vv setBackgroundColor:tkColorFrameBorder];
    }
    _vBgTitleDetail.backgroundColor=tkColorMainBackground;
    _vBgTitleRefund.backgroundColor=tkColorMainBackground;
    [self setValueDefault];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [self addContentToScrollView];
    _ivQRCode.image=[UIImage mdQRCodeForString:[TKOrder orderCode:receiptDoc] size:_ivQRCode.frame.size.width fillColor:[UIColor blackColor]];
    _lbManager.text=[TKOrder employeeName:receiptDoc];
    if (_lbManager.text.length==0) {
        _lbManager.text=[languageKey stringByKey:@"receipt.rc-dt.text-walk-in"];
    }
    _lbTitle.text=[TKOrder displayName:receiptDoc];
    _lbTime.text=[[TKOrder completedDate:receiptDoc] getStringWithFormat:@"E, dd MMM yyyy"];
    _lbStatus.text=[self stringStatusWithOrderStatus:[TKOrder orderStatus:receiptDoc]];
    NSString *customerName=[TKOrder customerReceiverName:receiptDoc];
    if (customerName) {
        _lbCustomer.text=customerName;
    }else{
        _lbCustomer.text=@"Walk-in";
    }
}
-(NSString*)stringStatusWithOrderStatus:(OrderStatus)status{
    switch (status) {
        case OrderStatusCollected:
            return @"Collected";
            break;
        case OrderStatusCanceled:
            return @"Canceled";
            break;
        case OrderStatusRefund:
            return @"Refunded";
            break;
        default:
            return @"";
            break;
    }
}
-(void)setValueDefault{
    NSArray *jsonArrayPro=[TKOrder arrayProductJSON:receiptDoc];
    listProductOrder=[CashierBusinessModel arrayProductOrderWithProductOrdersJSON:jsonArrayPro];// split product to some variant
    NSArray *jsonArrayPayment=[TKOrder arrayPaymentJSON:receiptDoc];
    listPaymentChild=[[NSMutableArray alloc] init];
    for (NSDictionary *jsonPaymentChild in jsonArrayPayment) {
        [listPaymentChild addObject:[[PaymentChild alloc] initFromJsonData:jsonPaymentChild]];
    }
    listAdhocProduct=[CashierBusinessModel arrayAdhocProductWithAdhocProductJSON:[TKOrder arrayAdhocProductJSON:receiptDoc]];
}
-(void)addContentToScrollView{
    CGPoint startPoint=CGPointZero;
    
    //addRefund
    if ([TKOrder orderStatus:receiptDoc]==OrderStatusRefund) {
        CGRect frame=_vHeaderRefund.frame;
        frame.size.width =_scrMain.frame.size.width;
        _vHeaderRefund.frame=frame;
        [_scrMain addSubview:_vHeaderRefund];
        startPoint.y+=frame.size.height;
        
        startPoint.x=SpaceLeftAmount;
        startPoint.y+=SpaceCellY;
        // add payment
        NSArray *arrayPaymentRefund=[self arrayPaymentRefund];
        for (NSInteger i=0 ; i<arrayPaymentRefund.count ; i++) {
            NSDictionary *payment=arrayPaymentRefund[i];
            [self addAmountCellWithTitle:payment[@"title"] value:payment[@"value"] bold:NO toPoint:&startPoint];
            if (i< arrayPaymentRefund.count-1) {
                [self addLineCellToPoint:&startPoint];
            }
        }
        _lbStaffRefund.text=[TKOrder refundEmployeeName:receiptDoc];
        _lbTimeRefund.text=[[TKOrder refundDate:receiptDoc] getStringWithFormat:@"E, dd MMM yyyy"];
        _lbReasonRefund.text=[TKOrder refundDisplayReason:receiptDoc];
        _lbReasonRefund.textColor=[UIColor redColor];
    }
    startPoint.x=0;
    // add lb Receipt
    
    CGRect frame =_vHeaderReceipt.frame;
    frame.origin=startPoint;
    frame.size.width = _scrMain.frame.size.width;
    [_vHeaderReceipt setFrame:frame];
    [_scrMain addSubview:_vHeaderReceipt];
    startPoint.y+=_vHeaderReceipt.frame.size.height;
    startPoint.y+=SpaceCellY;
    
    startPoint.x=SpaceLeftRight;
    // add product
    for (NSDictionary *product in [self arrayProductSale]) {
        ViewCellProduct *viewcell=[[ViewCellProduct alloc] init];
        [viewcell setShowVariant:([product[@"description"] length]>0) showDiscount:[product[@"haveDiscount"] boolValue]];
        CGRect frame=viewcell.frame;
        frame.origin=startPoint;
        [viewcell setFrame:frame];
        viewcell.lbTitle.text=product[@"title"];
        viewcell.lbVarian.text=product[@"description"];
        viewcell.lbAmount.text=product[@"amount"];
        viewcell.lbQuantity.text=product[@"quantity"];
        viewcell.lbDiscount.text=product[@"titleDiscount"];
        viewcell.lbAmountDiscount.text=product[@"amountDiscount"];
        [_scrMain addSubview:viewcell];
        startPoint.y+=viewcell.frame.size.height+SpaceCellY;
        [self addLineCellToPoint:&startPoint];

    }
    if ([TKOrder discountCart:receiptDoc]) {
        NSString *amountCartDiscount=[NSString stringWithFormat:@"-%@",[amountDisplay stringAmountHaveCurrency:[TKOrder discountCartValue:receiptDoc]]];
        NSDictionary *cartDiscountValue=@{@"title":@"Cart Discount",@"value":amountCartDiscount};
        [self addAmountCellWithTitle:cartDiscountValue[@"title"] value:cartDiscountValue[@"value"] bold:NO toPoint:&startPoint];
        [self addLineCellToPoint:&startPoint];
    }
    startPoint.x=SpaceLeftAmount;
    // add discount, service charge, VAT
    for (NSDictionary *dssv in [self arrayDiscountServiceCharge]) {
        [self addAmountCellWithTitle:dssv[@"title"] value:dssv[@"value"] bold:NO toPoint:&startPoint];
        [self addLineCellToPoint:&startPoint];
    }
    // add Total
    NSDictionary *total=[self totalCharge];
    [self addAmountCellWithTitle:total[@"title"] value:total[@"value"] bold:YES toPoint:&startPoint];
    [self addLineCellToPoint:&startPoint];
    // add payment
    for (NSDictionary *payment in [self arrayPayment]) {
        [self addAmountCellWithTitle:payment[@"title"] value:payment[@"value"] bold:NO toPoint:&startPoint];
        [self addLineCellToPoint:&startPoint];
    }
    // add roundAmtChange
    for (NSDictionary *racd in [self arrayRoundAmtChangeDue]) {
        [self addAmountCellWithTitle:racd[@"title"] value:racd[@"value"] bold:NO toPoint:&startPoint];
        [self addLineCellToPoint:&startPoint];
    }
    [_scrMain setContentSize:CGSizeMake(_scrMain.frame.size.width, startPoint.y)];
}
-(NSArray*)arrayProductSale{
    NSMutableArray *arrayPr=[[NSMutableArray alloc] init];
    for (ProductOrderSingle *productOrder in listProductOrder) {
        NSString *title=productOrder.productName;
        NSString *quantity = [NSString stringWithFormat:@"x%ld",(long)productOrder.quantity];
        NSString *description=productOrder.variantName;
        double totalCrude =[CashierBusinessModel totalAmountCrudeOfProductOrder:productOrder];
        NSMutableDictionary *dict=[@{@"title":title,@"description":description,@"amount":[amountDisplay stringAmountHaveCurrency:totalCrude],@"quantity":quantity,@"haveDiscount":@(NO)} mutableCopy];
        TKDiscount *discount=[productOrder discount];
        if (discount) {
            [dict setObject:@(YES) forKey:@"haveDiscount"];
            NSMutableString *discountShow=[[NSMutableString alloc]initWithString:discount.name];//[languageKey stringByKey:@"cashier.order.text-discount"]];
            if (discount.type==DiscountTypePercent) {
                [discountShow appendFormat:@" (%.2f%%)",discount.discountValue];
            }
            [dict setObject:discountShow forKey:@"titleDiscount"];
            NSString *amountDiscount=[NSString stringWithFormat:@"-%@",[amountDisplay stringAmountHaveCurrency:[CashierBusinessModel totalDiscountOfProductOrder:productOrder]]];
            [dict setObject:amountDiscount forKey:@"amountDiscount"];
        }
        [arrayPr addObject:dict];
    }
    NSArray *arrayAdhocProduct=[self arrayAdhocProduct];
    [arrayPr addObjectsFromArray:arrayAdhocProduct];
    return arrayPr;
}
-(NSArray*)arrayAdhocProduct{
    NSMutableArray *arrAdhocPr=[[NSMutableArray alloc] init];
    for (AdhocProduct *adhoc in listAdhocProduct) {
        NSString *title=adhoc.title;
        NSString *quantity = [NSString stringWithFormat:@"x%ld",(long)adhoc.quantity];
        
        double totalCrude =[CashierBusinessModel totalAmountCrudeOfAdhocProduct:adhoc];
        NSMutableDictionary *dict=[@{@"title":title,@"description":adhoc.variantName,@"amount":[amountDisplay stringAmountHaveCurrency:totalCrude],@"quantity":quantity,@"haveDiscount":@(NO)} mutableCopy];
        TKDiscount *discount=[adhoc discountProductItem];
        if (discount) {
            [dict setObject:@(YES) forKey:@"haveDiscount"];
            NSMutableString *discountShow=[[NSMutableString alloc]initWithString:discount.name];//[languageKey stringByKey:@"cashier.order.text-discount"]];
            if (discount.type==DiscountTypePercent) {
                [discountShow appendFormat:@" (%.2f%%)",discount.discountValue];
            }
            [dict setObject:discountShow forKey:@"titleDiscount"];
            NSString *amountDiscount=[NSString stringWithFormat:@"-%@",[amountDisplay stringAmountHaveCurrency:[CashierBusinessModel totalDiscountOfAdhocProduct:adhoc]]];
            [dict setObject:amountDiscount forKey:@"amountDiscount"];
        }
        [arrAdhocPr addObject:dict];
    }
    return arrAdhocPr;
}
-(NSArray*)arrayDiscountServiceCharge{
    NSMutableArray *arrayPr=[[NSMutableArray alloc] init];
    [arrayPr addObject:@{@"title":[languageKey stringByKey:@"cashier.order.text-total-discount"],
                         @"value":[amountDisplay stringAmountHaveCurrency:[TKOrder discountTotalAmount:receiptDoc]]}];
    
    NSString *subTotalCharg =[amountDisplay stringAmountHaveCurrency:[TKOrder subTotal:receiptDoc]];
    [arrayPr addObject:@{@"title":[languageKey stringByKey:@"receipt.rc-dt.lb.sub-total"],
                         @"value":subTotalCharg}];
    NSDictionary *serviceCharegeDisplay=[TKOrder serviceChargeDisplay:receiptDoc];
    if (serviceCharegeDisplay) {
        double serviceCharge=[serviceCharegeDisplay[@"serviceChargeValue"] doubleValue];
        [arrayPr addObject:@{@"title":[languageKey stringByKey:@"cashier.order.text-service-charges"],
                                 @"value":[amountDisplay stringAmountHaveCurrency:serviceCharge]}];
    }
    for (NSDictionary *taxDisplay in [TKOrder listTaxDisplayes:receiptDoc]) {
        double tax=[taxDisplay[@"taxValue"] doubleValue];
        [arrayPr addObject:@{@"title":taxDisplay[@"taxName"],
                                 @"value":[amountDisplay stringAmountHaveCurrency:tax]}];
    }
    return arrayPr;
}
-(NSDictionary*)totalCharge{
    // total charge round amount total
    NSString *totalCharge =[amountDisplay stringAmountHaveCurrency:[TKOrder grandTotal:receiptDoc]];
    return @{@"title":[languageKey stringByKey:@"receipt.rc-dt.lb.total"],@"value":totalCharge};
}
-(NSArray*)arrayPayment{
    NSArray *listTypeDifferent =[PaymentBusinessModel listPaymentTypeWithListPaymentChild:listPaymentChild];
    NSMutableArray *arrayPr=[[NSMutableArray alloc] init];
    for (NSString *paymentType in listTypeDifferent) {
        NSArray *array =[PaymentBusinessModel subListPaymentChildWithListPaymentChild:listPaymentChild paymentType:paymentType];
        NSString *title =[(PaymentChild*)[array firstObject] displayName];
        NSString *value =[amountDisplay stringAmountHaveCurrency:[PaymentBusinessModel totalChargeWithListPaymentChild:array]];
        [arrayPr addObject:@{@"title":title,@"value":value}];
    }
    return arrayPr;
}
-(NSArray*)arrayPaymentRefund{
    NSArray *arrayPaymentRefundJSON=[TKOrder arrayPaymentRefundJSON:receiptDoc];
    NSMutableArray *arrayPaymentRefund=[[NSMutableArray alloc] init];
    for (NSDictionary *json in arrayPaymentRefundJSON) {
        [arrayPaymentRefund addObject:[[PaymentChild alloc] initFromJsonData:json]];
    }
    NSArray *listTypeDifferent =[PaymentBusinessModel listPaymentTypeWithListPaymentChild:arrayPaymentRefund];
    NSMutableArray *arrayPr=[[NSMutableArray alloc] init];
    for (NSString *paymentType in listTypeDifferent) {
        NSArray *array =[PaymentBusinessModel subListPaymentChildWithListPaymentChild:listPaymentChild paymentType:paymentType];
        NSString *title =[(PaymentChild*)[array firstObject] displayName];
        NSString *value =[amountDisplay stringAmountHaveCurrency:[PaymentBusinessModel totalChargeWithListPaymentChild:array]];
        [arrayPr addObject:@{@"title":title,@"value":value}];
    }
    return arrayPr;
}
-(NSArray*)arrayRoundAmtChangeDue{
    NSMutableArray *arrayPr=[[NSMutableArray alloc] init];
    
    double roundAmout=[TKAmountDisplay tkRoundCash:[TKOrder roundAmount:receiptDoc]];
    double changeDue=[TKOrder changeDue:receiptDoc];
    if(![TKAmountDisplay isZero:roundAmout]){
        NSString *value;
        if (roundAmout>0) {
            value=[amountDisplay stringAmountHaveCurrency:roundAmout];
        }else{
            value=[NSString stringWithFormat:@"-%@",[amountDisplay stringAmountHaveCurrency:-roundAmout]];
        }
        [arrayPr addObject:@{@"title":[languageKey stringByKey:@"receipt.rc-dt.lb.round-amt"],@"value":value}];
    }
    if(![TKAmountDisplay isZero:changeDue] && changeDue>0){
        [arrayPr addObject:@{@"title":[languageKey stringByKey:@"receipt.rc-dt.lb.change-due"],@"value":[amountDisplay stringAmountHaveCurrency:changeDue]}];
    }
    return arrayPr;
}

-(void)addLineCellToPoint:(CGPoint*)pointStart{
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake((*pointStart).x, (*pointStart).y, _scrMain.frame.size.width -(*pointStart).x - SpaceCellX, 1)];
    [line setBackgroundColor:tkColorFrameBorder];
    [_scrMain addSubview:line];
    (*pointStart).y+=line.frame.size.height;
    (*pointStart).y+=SpaceCellY;
}
-(void)addAmountCellWithTitle:(NSString*)title value:(NSString*)value bold:(BOOL)bold toPoint:(CGPoint*)pointStart{
    CGRect frame=CGRectZero;
    frame.origin=(*pointStart);
    frame.size.width=_scrMain.frame.size.width-frame.origin.x-SpaceLeftRight;
    frame.size.height=HeightCell;
    CellViewAmount *cell=[[CellViewAmount alloc] initWithTitle:title value:value bold:bold];
    [cell setFrame:frame];
    [_scrMain addSubview:cell];
    (*pointStart).y+=cell.frame.size.height;
    (*pointStart).y+=SpaceCellY;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

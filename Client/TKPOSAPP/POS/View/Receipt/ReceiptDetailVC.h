//
//  ReceiptDetailVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/9/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface ReceiptDetailVC : UIViewController{
    CBLDocument *receiptDoc;
}
-(instancetype)initWithReceipt:(CBLDocument*)document;
@property (weak, nonatomic) IBOutlet UIImageView *ivQRCode;
@property (weak, nonatomic) IBOutlet UIScrollView *scrMain;
@property (weak, nonatomic) IBOutlet UIView *vHeaderReceipt;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbManager;
@property (weak, nonatomic) IBOutlet UILabel *lbStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lbStaffRefund;
@property (weak, nonatomic) IBOutlet UILabel *lbTimeRefund;
@property (weak, nonatomic) IBOutlet UILabel *lbReasonRefund;
@property (strong, nonatomic) IBOutlet UIView *vHeaderRefund;
@property (weak, nonatomic) IBOutlet UILabel *lbTime;
@property (weak, nonatomic) IBOutlet UIView *vBgTitleDetail;
@property (weak, nonatomic) IBOutlet UIView *vBgTitleRefund;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleRefund;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleDetail;
@property (weak, nonatomic) IBOutlet UIView *vLineDetail1;
@property (weak, nonatomic) IBOutlet UIView *vLineDetail2;
@property (weak, nonatomic) IBOutlet UIView *vLineDetail3;
@property (weak, nonatomic) IBOutlet UIView *vLineRefund1;
@property (weak, nonatomic) IBOutlet UIView *vLineRefund2;
@property (weak, nonatomic) IBOutlet UIView *vLineRefund3;
@end

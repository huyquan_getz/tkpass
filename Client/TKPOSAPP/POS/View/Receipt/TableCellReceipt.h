//
//  TableCellReceipt.h
//  POS
//
//  Created by Nha Duong Cong on 12/9/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellReceipt : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbTime;
@property (weak, nonatomic) IBOutlet UILabel *lbAmount;
@property (weak, nonatomic) IBOutlet UIImageView *ivStatusRefund;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (strong,nonatomic) UIImage *imgStatusDeselected;
@property (strong,nonatomic) UIImage *imgStatusSelected;
-(void)setValueDefault;
@end

//
//  PinCodeVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/22/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define MaxLength 10
#import "PinCodeVC.h"
#import "Constant.h"
#import "Controller.h"
#import "TKEmployee.h"

@interface PinCodeVC ()

@end

@implementation PinCodeVC{
    LanguageUtil *languageKey;
    UIView *viewCorverToptip;
}
-(instancetype)initWithEmployee:(CBLDocument *)employee_ checkLogin:(BOOL)checkLogin_{
    if (self=[super init]) {
        employee=employee_;
        checkInLogin=checkLogin_;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor clearColor];
    if (!checkInLogin) {
        UIView *viewCover=[[UIView alloc] initWithFrame:self.view.frame];
        [viewCover setViewCoverDefault];
        [self.view insertSubview:viewCover belowSubview:_vInput];
        CGRect frame=_vInput.frame;
        frame.origin.y-=80;
        _vInput.frame=frame;
    }
    languageKey = [LanguageUtil sharedLanguageUtil];
    [_vInput setShadowWithCornerRadius:tkCornerRadiusViewPopup];
    _vInput.backgroundColor=tkColorMainBackground;
    _vTextField.layer.borderColor=tkColorFrameBorder.CGColor;
    _vTextField.layer.borderWidth=1;
    _vTextField.layer.cornerRadius=tkCornerRadiusViewPopup;
    NSArray *lines=@[_vLine1,_vLine21,_vLine22,_vLine23,_vLine31,_vLine32,_vLine33,_vLine34,_vLine35,_vLine36];
    for (UIView *vLine in lines) {
        vLine.backgroundColor=tkColorFrameBorder;
        vLine.backgroundColor=tkColorFrameBorder;
    }
    NSArray *buttons=@[_btNumber0,_btNumber1,_btNumber2,_btNumber3,_btNumber4,_btNumber5,_btNumber6,_btNumber7,_btNumber8,_btNumber9];
    for (int i=0; i<buttons.count; i++) {
        UIButton *bt=buttons[i];
        bt.tag=i;
    }
    _lbStatusMessage.text=[NSString stringWithFormat:@"%@ %d",[languageKey stringByKey:@"login.pincode.ms-pincode-empty"],[TKEmployee getEmployeeID:employee]];
    _btMoreInfo.hidden=YES;
    _btBack.hidden=!checkInLogin;
    _btLock.hidden=checkInLogin;
}
-(void)viewWillAppear:(BOOL)animated
{
    _tfID1.text=@"";
    _tfID2.text=@"";
    _tfID3.text=@"";
    _tfID4.text=@"";
    [_btClear setTitle:[languageKey stringByKey:@"login.bt.clear"] forState:UIControlStateNormal];
    [_btEnter setTitle:[languageKey stringByKey:@"login.bt.enter"] forState:UIControlStateNormal];
    _lbToptip.text=[languageKey stringByKey:@"login.pincode.ms-direct-set-pincode"];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)clickClear:(id)sender {
    _tfID1.text=@"";
    _tfID2.text=@"";
    _tfID3.text=@"";
    _tfID4.text=@"";
}
-(void)clickNumber:(UIButton*)sender{
    int number=(int)sender.tag;
    NSString *newstring=[[self pincodeInput] stringByAppendingString:[NSString stringWithFormat:@"%d",number]];
    [self setPincode:newstring];
}
-(NSString*)pincodeInput{
    return [NSString stringWithFormat:@"%@%@%@%@",_tfID1.text,_tfID2.text,_tfID3.text,_tfID4.text];
}
-(void)setPincode:(NSString*)string{
    _tfID1.text=@"";
    _tfID2.text=@"";
    _tfID3.text=@"";
    _tfID4.text=@"";
    if (string.length>0) {
        _tfID1.text=[string substringWithRange:NSMakeRange(0, 1)];
    }
    if (string.length>1) {
        _tfID2.text=[string substringWithRange:NSMakeRange(1, 1)];
    }
    if (string.length>2) {
        _tfID3.text=[string substringWithRange:NSMakeRange(2, 1)];
    }
    if (string.length>3) {
        _tfID4.text=[string substringWithRange:NSMakeRange(3, 1)];
    }
}
-(void)showMessageError:(NSString*)error{
    [_lbStatusMessage setTextColor:tkColorStatusMessageError];
    [_lbStatusMessage setText:error];
}
-(void)clearStatus{
    [_lbStatusMessage setText:@""];
    _btMoreInfo.hidden=YES;
}
- (IBAction)clickEnter:(id)sender {
    if (![self checkPinCode]) {
        return;
    }
    [self clearStatus];
    if (_delegate) {
        [_delegate pinCodeVerify:self completed:YES];
    }
}
-(BOOL)checkPinCode{
    if ([[[TKEmployee getPincode:employee] convertNullToNil] isEqualToString:@""]) {
        //
        [self showMessageError:[NSString stringWithFormat:@"%@ %d",[languageKey stringByKey:@"login.pincode.ms-not-set-pincode"],[TKEmployee getEmployeeID:employee]]];
        CGRect frame=_btMoreInfo.frame;
        frame.origin.x=_lbStatusMessage.frame.origin.x + _lbStatusMessage.frame.size.width/2 + [_lbStatusMessage.text widthWithFont:_lbStatusMessage.font]/2 +5;
        _btMoreInfo.frame=frame;
        _btMoreInfo.hidden=NO;
        return NO;
    }
    if ([self pincodeInput].length!=4) {
        [self showMessageError:[NSString stringWithFormat:@"%@ %d",[languageKey stringByKey:@"login.pincode.ms-pincode-empty"],[TKEmployee getEmployeeID:employee]]];
        return NO;
    }
    if (![[self pincodeInput] isEqualToString:[TKEmployee getPincode:employee]]) {
        [self showMessageError:[NSString stringWithFormat:@"%@ %d",[languageKey stringByKey:@"login.pincode.ms-pincode-incorrect"],[TKEmployee getEmployeeID:employee]]];
        return NO;
    }
    return YES;
}
- (IBAction)clickBack:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(pinCodeVerify:canceled:)]) {
        [_delegate pinCodeVerify:self canceled:YES];
    }
}

- (IBAction)clickLock:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(pinCodeVerify:switchUser:)]) {
        [_delegate pinCodeVerify:self switchUser:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickMoreInfo:(id)sender {
    CGRect frame=_vToptip.frame;
    frame.origin=CGPointZero;
    UIView *v=_btMoreInfo;
    while (v!=self.view) {
        frame.origin.x+=v.frame.origin.x;
        frame.origin.y+=v.frame.origin.y;
        v=v.superview;
    }
    frame.origin.y-=frame.size.height - 10;
    frame.origin.x-=12;
    _vToptip.frame=frame;
    [self.view addSubview:_vToptip];
    if (viewCorverToptip) {
        [viewCorverToptip removeFromSuperview];
    }
    viewCorverToptip=[[UIView alloc] initWithFrame:self.view.frame];
    viewCorverToptip.backgroundColor=[UIColor clearColor];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverTiptop:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [viewCorverToptip addGestureRecognizer:tap];
    [self.view addSubview:viewCorverToptip];
}
-(void)tapCoverTiptop:(id)sender{
    //remove toptip
    [_vToptip removeFromSuperview];
    [viewCorverToptip removeFromSuperview];
}
@end

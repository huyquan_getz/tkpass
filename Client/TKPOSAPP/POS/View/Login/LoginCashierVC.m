//
//  LoginCashierVC.m
//  POS
//
//  Created by Nha Duong Cong on 10/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "LoginCashierVC.h"
#import "Controller.h"
#import "UIAlertView+Blocks.h"
#import "UITextField+Util.h"
#import "CouchBaseBusinessModel.h"
#import "TKEmployee.h"
#import "LeftMenuController.h"


@interface LoginCashierVC ()
{
    LanguageUtil *languageKey;
    CBLDocument *employee;
}
@end

@implementation LoginCashierVC
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_vInput setShadowWithCornerRadius:tkCornerRadiusViewPopup];
    _btManager.layer.cornerRadius=tkCornerRadiusButton;
    _btForgotPass.layer.cornerRadius=tkCornerRadiusButton;
    _btManager.layer.borderColor=tkColorFrameBorder.CGColor;
    _vInput.backgroundColor=tkColorMainBackground;
    _vTextField.layer.borderColor=tkColorFrameBorder.CGColor;
    _vTextField.layer.borderWidth=1;
    _vTextField.layer.cornerRadius=tkCornerRadiusViewPopup;
    NSArray *lines=@[_vLine1,_vLine21,_vLine22,_vLine23,_vLine31,_vLine32,_vLine33,_vLine34,_vLine35,_vLine36];
    for (UIView *vLine in lines) {
        vLine.backgroundColor=tkColorFrameBorder;
        vLine.backgroundColor=tkColorFrameBorder;
    }
    NSArray *buttons=@[_btNumber0,_btNumber1,_btNumber2,_btNumber3,_btNumber4,_btNumber5,_btNumber6,_btNumber7,_btNumber8,_btNumber9];
    for (int i=0; i<buttons.count; i++) {
        UIButton *bt=buttons[i];
        bt.tag=i;
    }
    
    _btForgotPass.hidden=YES;
    _btManager.hidden=YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    languageKey = [LanguageUtil sharedLanguageUtil];
    _tfID1.text=@"";
    _tfID2.text=@"";
    _tfID3.text=@"";
    _tfID4.text=@"";
    [_btForgotPass setTitle:[languageKey stringByKey:@"login.bt.forgot"] forState:UIControlStateNormal];
     [_btManager setTitle:[languageKey stringByKey:@"login.bt.manager"] forState:UIControlStateNormal];
     [_btClear setTitle:[languageKey stringByKey:@"login.bt.clear"] forState:UIControlStateNormal];
     [_btEnter setTitle:[languageKey stringByKey:@"login.bt.enter"] forState:UIControlStateNormal];
    [self reshowTitle];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickForgotID:(id)sender {

}

- (IBAction)clickClear:(id)sender {
    _tfID1.text=@"";
    _tfID2.text=@"";
    _tfID3.text=@"";
    _tfID4.text=@"";
}
-(void)clickNumber:(UIButton*)sender{
    int number=(int)sender.tag;
    NSString *newstring=[[self userIDInput] stringByAppendingString:[NSString stringWithFormat:@"%d",number]];
    [self setUserID:newstring];
}
-(NSString*)userIDInput{
    return [NSString stringWithFormat:@"%@%@%@%@",_tfID1.text,_tfID2.text,_tfID3.text,_tfID4.text];
}
-(void)setUserID:(NSString*)string{
    _tfID1.text=@"";
    _tfID2.text=@"";
    _tfID3.text=@"";
    _tfID4.text=@"";
    if (string.length>0) {
        _tfID1.text=[string substringWithRange:NSMakeRange(0, 1)];
    }
    if (string.length>1) {
        _tfID2.text=[string substringWithRange:NSMakeRange(1, 1)];
    }
    if (string.length>2) {
        _tfID3.text=[string substringWithRange:NSMakeRange(2, 1)];
    }
    if (string.length>3) {
        _tfID4.text=[string substringWithRange:NSMakeRange(3, 1)];
    }
}
-(void)showMessageError:(NSString*)error{
    [_lbStatusMessage setTextColor:tkColorStatusMessageError];
    [_lbStatusMessage setText:error];
}
-(void)reshowTitle{
    [_lbStatusMessage setText:[languageKey stringByKey:@"login.cashier.ms-user-id-empty"]];
    [_lbStatusMessage setTextColor:[UIColor darkGrayColor]];
}
- (IBAction)clickEnter:(id)sender{
    if (![self validateInput]) {
        return;
    }
    [self reshowTitle];
    self.view.userInteractionEnabled=NO;
    UIActivityIndicatorView *activity=tkActivityIndicatorViewDefault;
    activity.center=_btEnter.center;
    [_btEnter.superview addSubview:activity];
    [activity startAnimating];
    [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
        employee =[Controller employeeWithPinCode:[self userIDInput]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [activity stopAnimating];
            [activity removeFromSuperview];
            self.view.userInteractionEnabled=YES;
            [SNLog Log:@"%@",employee.properties];
            if (employee && ([TKEmployee getRoles:employee storeIndex:2].count>0 || [TKEmployee isMerchant:employee])) {
                [self hidePinCodeVC];
                _pincodeVC=[[PinCodeVC alloc] initWithEmployee:employee checkLogin:YES];
                _pincodeVC.delegate=self;
                [self addChildViewController:_pincodeVC];
                [self.view addSubview:_pincodeVC.view];
            }else{
                [self showMessageError:[languageKey stringByKey:@"login.controller.ms-cashier-id-not-exist"]];
            }
        });
    }];
}
-(BOOL)validateInput{
    if ([self userIDInput].length!=4) {
        [self showMessageError:[languageKey stringByKey:@"login.cashier.ms-user-id-empty"]];
        return NO;
    }
    return YES;
}
- (IBAction)clickManager:(id)sender {
    [Controller showManagerLoginView];
}
-(void)hidePinCodeVC{
    if (_pincodeVC) {
        [_pincodeVC.view removeFromSuperview];
        [_pincodeVC removeFromParentViewController];
        _pincodeVC=nil;
    }
}
#pragma PinCodeDelegate
-(void)pinCodeVerify:(id)sender completed:(BOOL)completed{
    [self hidePinCodeVC];
    [Controller saveUserLogined:employee];
    [[LeftMenuController sharedLeftMenu] setSelectedCellType:CellLeftMenuCashier withAction:YES];
}
-(void)pinCodeVerify:(id)sender canceled:(BOOL)canceled{
    [self hidePinCodeVC];
}
@end

//
//  CashDrawerConfigVC.h
//  POS
//
//  Created by Cong Nha Duong on 1/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CashDrawerConfigVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btSave;
- (IBAction)clickSave:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleDrawerName;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleIPAddress;
@property (weak, nonatomic) IBOutlet UILabel *lbTitlePort;
@property (weak, nonatomic) IBOutlet UITextField *tfDrawerName;
@property (weak, nonatomic) IBOutlet UITextField *tfIPAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfPort;
@property (weak, nonatomic) IBOutlet UIView *vLine1;
@property (weak, nonatomic) IBOutlet UIView *vLine2;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIView *vInput;
@property (weak, nonatomic) IBOutlet UIButton *btTestConnect;

@property (weak, nonatomic) IBOutlet UIImageView *ivNaviBar;
- (IBAction)clickTestCashDrawer:(id)sender;

@end

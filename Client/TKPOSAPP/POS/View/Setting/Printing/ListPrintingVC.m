//
//  ListPrintingVC.m
//  POS
//
//  Created by Cong Nha Duong on 1/9/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define HeighCell 50
#define MaxPrintingQuery 1000
#define CellPrintingIdentify @"cellPrinting"
#import "ListPrintingVC.h"
#include "Controller.h"
#import "TableCellPrinting.h"
#import "AddNewPrintingVC.h"
#import "Printing.h"

@interface ListPrintingVC ()

@end

@implementation ListPrintingVC
-(instancetype)init{
    if (self =[super init]) {
        listPrinting=[[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _ivNaviBar.backgroundColor=[UIColor statusNaviBar];
    _tbvMain.clipsToBounds=YES;
    _tbvMain.layer.cornerRadius=tkCornerRadiusViewPopup;
    _tbvMain.layer.borderColor=tkColorFrameBorder.CGColor;
    _tbvMain.layer.borderWidth=1;
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellPrinting" bundle:nil] forCellReuseIdentifier:CellPrintingIdentify];
    // Do any additional setup after loading the view from its nib.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self reSetupQueryLiveWithQuery:[Controller queryGetAllPrinting]];
    });
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    _lbNoDefaultPrinter.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"setting.printing.lb-no-printing"];
}
-(void)reSetupQueryLiveWithQuery:(CBLQuery*)query{
    if (query==nil) {
        return;
    }
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery=[query asLiveQuery];
    [liveQuery addObserver:self forKeyPath:@"rows" options:0 context:NULL];
    liveQuery.limit = MaxPrintingQuery;
    liveQuery.descending=NO;
    [liveQuery start];
}
-(void)removeQueryLive{
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery=nil;
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (object == liveQuery) {
        [self displayRows:liveQuery.rows];
    }
}
-(void)displayRows:(CBLQueryEnumerator*)enumRows{
    NSMutableArray *printers=[[NSMutableArray alloc] init];
    for (CBLQueryRow *row in enumRows) {
        [printers addObject:row.document];
        [SNLog Log:@"%@",row.documentID];
    }
    [listPrinting removeAllObjects];
    [listPrinting addObjectsFromArray:printers];
    [_tbvMain reloadData];
    [self resetFrameTableView];
}
-(void)resetFrameTableView{
    CGRect frame=_tbvMain.frame;
    NSInteger heightContent=listPrinting.count * HeighCell;
    if (heightContent>658) {
        heightContent=658;
    }
    frame.size.height=heightContent;
    [_tbvMain setFrame:frame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    _lbNoDefaultPrinter.hidden=(listPrinting.count!=0);
    return listPrinting.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellPrinting *cell=[tableView dequeueReusableCellWithIdentifier:CellPrintingIdentify];
    [cell setValueDefault];
    CBLDocument *doc=listPrinting[indexPath.row];
    cell.lbTitle.text=[Printing getPrintingDisplay:doc];
    cell.lbPrinter.text=[Printing getPrinterNameAssign:doc];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AddNewPrintingVC *editPrinter=[[AddNewPrintingVC alloc] initWithPrinting:listPrinting[indexPath.row]];
    [self.navigationController pushViewController:editPrinter animated:YES];
}
- (IBAction)clickAdd:(id)sender {
    AddNewPrintingVC *addNew=[[AddNewPrintingVC alloc] initWithPrinting:nil];
    [self.navigationController pushViewController:addNew animated:YES];
}
-(void)dealloc{
    [liveQuery removeObserver:self forKeyPath:@"rows"];
}

@end

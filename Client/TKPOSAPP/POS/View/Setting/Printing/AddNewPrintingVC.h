//
//  AddNewPrintingVC.h
//  POS
//
//  Created by Cong Nha Duong on 1/9/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Printing.h"
#import "PopoverListController.h"

@interface AddNewPrintingVC : UIViewController<PopoverListDelegate,UIPopoverControllerDelegate,UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *listPrinter;
    NSMutableArray *listCategory;
    NSMutableArray *listCategorySelected;
    CBLDocument *printerSelected;
    PrintingType printingTypeSelect;
    CBLDocument *printingDoc;
}
@property (weak, nonatomic) IBOutlet UIImageView *ivNavBar;
-(instancetype)initWithPrinting:(CBLDocument*)printing;
@property (weak, nonatomic) IBOutlet UIButton *btRemove;
- (IBAction)clickRemove:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btBack;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleSelect;
@property (weak, nonatomic) IBOutlet UIButton *btSave;
@property (strong, nonatomic) IBOutlet UIView *vCombine;
@property (weak, nonatomic) IBOutlet UISwitch *swCombine;
- (IBAction)clickSwitchCombine:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleCombine;
- (IBAction)clickSave:(id)sender;
- (IBAction)clickBack:(id)sender;
- (IBAction)clickChooseType:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btChooseType;
- (IBAction)clickChoosePrinter:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btChoosePrinter;
@property (weak, nonatomic) IBOutlet UIView *vInput;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleType;
@property (weak, nonatomic) IBOutlet UILabel *lbDescriptionType;
@property (weak, nonatomic) IBOutlet UIImageView *vRequireReceiptType;
@property (weak, nonatomic) IBOutlet UILabel *lbTitlePrinter;
@property (weak, nonatomic) IBOutlet UIImageView *vRequirePrinter;
@property (weak, nonatomic) IBOutlet UILabel *lbDescriptionPrinter;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (weak, nonatomic) IBOutlet UITableView *tbvCategory;
@property (strong,nonatomic) PopoverListController *popoverListType;
@property (strong,nonatomic) PopoverListController *popoverListPrinter;
@end

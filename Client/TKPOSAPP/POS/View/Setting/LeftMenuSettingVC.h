//
//  LeftMenuSettingVC.h
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
@protocol LefMenuSettingDelegate <NSObject>
@required;
-(void)leftMenuSetting:(id)sender clickInStoreGeneral:(BOOL)none;
-(void)leftMenuSetting:(id)sender clickInStoreModule:(BOOL)none;
-(void)leftMenuSetting:(id)sender clickHardwarePrinterList:(BOOL)none;
-(void)leftMenuSetting:(id)sender clickHardwarePrinting:(BOOL)none;
-(void)leftMenuSetting:(id)sender clickHardwareCashDrawer:(BOOL)none;

@end

typedef enum _SettingSectionType{
    SettingSectionTypeInStore=0,
    SettingSectionTypeHardware=1
}SettingSectionType;

typedef enum _SettingTypeInStore{
    SettingTypeInStoreGeneral =0,
    SettingTypeInStoreModule =1
}SettingTypeInStore;
typedef enum _SettingTypeHardware{
    SettingTypeHardwarePrinterList =0,
    SettingTypeHardwarePrinting =1,
    SettingTypeHardwareCashDrawer =2
}SettingTypeHardware;

#import <UIKit/UIKit.h>

@interface LeftMenuSettingVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSIndexPath *indexPathSelected;
}
@property (weak,nonatomic) id<LefMenuSettingDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;

@end

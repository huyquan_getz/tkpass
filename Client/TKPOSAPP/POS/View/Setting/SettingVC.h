//
//  SettingVC.h
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftMenuSettingVC.h"

@interface SettingVC : UIViewController<LefMenuSettingDelegate>
@property (strong,nonatomic)LeftMenuSettingVC *leftMenuSettingVC;
@property (strong,nonatomic)UINavigationController *naviControllerCenter;
@property (weak, nonatomic) IBOutlet UIButton *btMenu;
- (IBAction)clickMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vFrameLeftMenu;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (weak, nonatomic) IBOutlet UIView *vFrameCenter;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleSetting;
@property (weak, nonatomic) IBOutlet UIImageView *ivNaviBar;
// test commit
@end

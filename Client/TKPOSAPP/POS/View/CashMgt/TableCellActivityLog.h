//
//  TableCellActivityLog.h
//  POS
//
//  Created by Nha Duong Cong on 12/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellActivityLog : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbDateTime;
@property (weak, nonatomic) IBOutlet UILabel *lbStaff;
@property (weak, nonatomic) IBOutlet UILabel *lbCashIn;
@property (weak, nonatomic) IBOutlet UILabel *lbCashOut;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UILabel *lbType;
-(void)setValueDefault;
@end

//
//  ShiftDrawerDetailVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol ShiftDrawerDetailDelegate <NSObject>
@required
-(void)shiftDrawerDetail:(id)sender deposit:(BOOL)none;
-(void)shiftDrawerDetail:(id)sender payOut:(BOOL)none;
-(void)shiftDrawerDetail:(id)sender closeCurrentShift:(BOOL)none;
-(void)shiftDrawerDetail:(id)sender startNewShift:(BOOL)none;

@end
#import <UIKit/UIKit.h>
#import "ShiftDrawerManager.h"

@interface ShiftDrawerDetailVC : UIViewController{
    ShiftDrawer *shiftDrawer;
    NSString *currency;
    BOOL enablePaidInOut;
    BOOL enableStartClose;
}
@property (weak, nonatomic) IBOutlet UILabel *lbTimeStartClose;
@property (weak,nonatomic) id<ShiftDrawerDetailDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbCash;
@property (weak, nonatomic) IBOutlet UIButton *btStartCloseDrawer;
- (IBAction)clickStartClose:(id)sender;
-(instancetype)initWithShiftDrawer:(CBLDocument*)shiftDrawerDoc_ enableStartClose:(BOOL)startClose;
-(instancetype)initEmptyWithCurrency:(NSString*)currency_ enableStartClose:(BOOL)startClose;
@end

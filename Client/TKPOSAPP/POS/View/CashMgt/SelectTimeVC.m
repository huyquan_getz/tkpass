//
//  SelectTimeVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/25/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define NumberLoopMonth 200
#import "SelectTimeVC.h"
#import "Constant.h"

@interface SelectTimeVC ()

@end

@implementation SelectTimeVC{
    NSDateFormatter *dateFormat;
}
-(instancetype)init{
    if (self =[super init]) {
        maxYear=5000;
        minYear=0000;
        dateFormat=[[NSDateFormatter alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor clearColor];
    UIView *vCorver=[[UIView alloc] initWithFrame:self.view.bounds];
    [vCorver setViewCoverDefault];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [vCorver addGestureRecognizer:tap];
    [self.view insertSubview:vCorver belowSubview:_vMain];
    [_vMain setShadowDefault];
    _btDone.layer.cornerRadius=tkCornerRadiusButton;
    _btDone.backgroundColor=tkColorMainAccept;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tapCoverView:(id)sender{
    [self clickCancel:nil];
}
- (IBAction)clickCancel:(id)sender {
    if (_delegate) {
        [_delegate selectTime:self clickCancel:YES];
    }
}
- (IBAction)clickDone:(id)sender {
    if (_delegate) {
        NSInteger year=[_pkYear selectedRowInComponent:0]+minYear;
        NSInteger month =[_pkMonth selectedRowInComponent:0]%12 + 1;
        [_delegate selectTime:self clickDoneWithYear:year month:month];
    }
}

#pragma PickerViewDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView==_pkYear){
        return maxYear-minYear+1;
    }else if(pickerView==_pkMonth){
        return 12*NumberLoopMonth;
    }
    return 0;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView==_pkYear){
        return [NSString stringWithFormat:@"%i",minYear+row];
    }else if(pickerView==_pkMonth){
        return [dateFormat shortMonthSymbols][row%12];
    }
    return @"";
}
-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (pickerView==_pkMonth) {
        NSInteger newRow =12*NumberLoopMonth/2 +row%12;
        [_pkMonth selectRow:newRow inComponent:0 animated:NO];
    }
    _lbTitleDateSelected.text=[NSString stringWithFormat:@"%@ %@",[self pickerView:_pkYear titleForRow:[_pkYear selectedRowInComponent:0] forComponent:0],[self pickerView:_pkMonth titleForRow:[_pkMonth selectedRowInComponent:0] forComponent:0]];
}
-(void)setYear:(NSInteger)year month:(NSInteger)month{
    [_pkYear selectRow:(year-minYear) inComponent:0 animated:YES];
    [_pkMonth selectRow:(month-1) inComponent:0 animated:YES];
    [self pickerView:_pkYear didSelectRow:(year-minYear) inComponent:0];
    [self pickerView:_pkMonth didSelectRow:month-1 inComponent:0];
}
@end


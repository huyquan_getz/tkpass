//
//  SelectTimeVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/25/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol SelectTimeDelegate <NSObject>

@required
-(void)selectTime:(id)sender clickDoneWithYear:(NSInteger)year month:(NSInteger)month;
-(void)selectTime:(id)sender clickCancel:(BOOL)none;

@end
#import <UIKit/UIKit.h>

@interface SelectTimeVC : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>{
    NSInteger maxYear;
    NSInteger minYear;
}
@property (weak, nonatomic) id<SelectTimeDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
- (IBAction)clickCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btDone;
- (IBAction)clickDone:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleDateSelected;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UIPickerView *pkYear;
@property (weak, nonatomic) IBOutlet UIPickerView *pkMonth;
-(void)setYear:(NSInteger)year month:(NSInteger)month;
@end

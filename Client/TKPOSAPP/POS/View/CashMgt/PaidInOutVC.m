//
//  PaidInOutVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/25/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define MaxLengthDescription 50
#import "PaidInOutVC.h"
#import "Constant.h"
#import "CashierBusinessModel.h"

@interface PaidInOutVC ()

@end

@implementation PaidInOutVC{

}
@synthesize isPaidIn;
-(instancetype)initWithPaidIn:(BOOL)paidIn currency:(NSString *)currency_{
    if (self=[super init]) {
        currency=currency_;
        isPaidIn=paidIn;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _tfAmount.text=@"";
    _tfAmount.placeholder=[TKAmountDisplay stringAmount:0 withCurrency:currency];
    _btSave.layer.cornerRadius=tkCornerRadiusButton;
    [_vMain setShadowDefault];
    _vInputTextfield.layer.cornerRadius=tkCornerRadiusViewPopup;
    _vInputTextfield.layer.borderColor=tkColorFrameBorder.CGColor;
    _vInputTextfield.layer.borderWidth=1;
    _vLine.backgroundColor=tkColorFrameBorder;
    
    _btSave.layer.cornerRadius=tkCornerRadiusButton;
    _btSave.backgroundColor=tkColorMainAccept;
    self.view.backgroundColor=[UIColor clearColor];
    _vMain.backgroundColor=[UIColor whiteColor];
    _vInputTextfield.backgroundColor=[UIColor whiteColor];
    
    UIView *vCorver=[[UIView alloc] initWithFrame:self.view.bounds];
    [vCorver setViewCoverDefault];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [vCorver addGestureRecognizer:tap];
    [self.view insertSubview:vCorver belowSubview:_vMain];
    [self showMessageError:@""];
    // Do any additional setup after loading the view from its nib.
}
-(void)showMessageError:(NSString*)error{
    [_lbMessageStatus setTextColor:[UIColor redColor]];
    _lbMessageStatus.text=error;
}
-(void)viewWillAppear:(BOOL)animated{
    LanguageUtil *languageKey=[LanguageUtil sharedLanguageUtil];
    if (isPaidIn) {
        _lbTitle.text=[languageKey stringByKey:@"cash-mgt.pay-in-out.title-deposit"];
    }else{
        _lbTitle.text=[languageKey stringByKey:@"cash-mgt.pay-in-out.title-pay-out"];
    }
    _lbTitleAmount.text=[languageKey stringByKey:@"cash-mgt.pay-in-out.lb-amount"];
    _lbTitleDescription.text=[languageKey stringByKey:@"cash-mgt.pay-in-out.description"];
    [_btSave setTitle:[languageKey stringByKey:@"general.bt.save"] forState:UIControlStateNormal];
}
-(void)viewDidAppear:(BOOL)animated{
    [_tfAmount becomeFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tapCoverView:(id)sender{
    [self clickCancel:nil];
}
- (IBAction)clickCancel:(id)sender {
    if (_delegate) {
        [_delegate paidInOut:self cancel:YES];
    }
}
- (IBAction)clickSave:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self showMessageError:@""];
    if (_delegate) {
        double amount =[_tfAmount.text doubleValue];
        [_delegate paidInOut:self paidAmount:amount comment:_tfDescription.text isPaidIn:isPaidIn];
    }
}

#pragma TextfieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    if (textField==_tfAmount) {
        NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
        if (![newString isTKAmountFormat]) {
            return NO;
        }
    }else if (textField==_tfDescription){
        NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
        if (newString.length>MaxLengthDescription) {
            return NO;
        }
    }
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect frame=_vMain.frame;
    frame.origin.y-= 160;
    [_vMain setFrame:frame];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    CGRect frame=_vMain.frame;
    frame.origin.y+= 160;
    [_vMain setFrame:frame];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == _tfAmount){
        [_tfDescription becomeFirstResponder];
    }else if(textField== _tfDescription){
        [textField resignFirstResponder];
    }
    return YES;
}
-(BOOL)validateInput{
    if ([_tfAmount isEmptyText]) {
        [self showMessageError:[[LanguageUtil sharedLanguageUtil] stringByKey:@"cash-mgt.pay-in-out.empty-amount"]];
        return NO;
    }
    return YES;
}
@end

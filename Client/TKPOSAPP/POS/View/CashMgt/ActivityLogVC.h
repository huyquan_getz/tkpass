//
//  ActivityLogVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShiftDrawerManager.h"
#import "ActitivityDrawer.h"

@interface ActivityLogVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *listActivity;
    ShiftDrawer *shiftDrawer;
    NSString *currency;
}
-initWithShiftDrawer:(CBLDocument*)shiftDrawerDoc_;
@property (strong, nonatomic) IBOutlet UIView *vTableHeader;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak, nonatomic) IBOutlet UILabel *lbDateTime;
@property (weak, nonatomic) IBOutlet UILabel *lbStaff;
@property (weak, nonatomic) IBOutlet UILabel *lbCashIn;
@property (weak, nonatomic) IBOutlet UILabel *lbCashOut;
@property (weak, nonatomic) IBOutlet UILabel *lbType;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;

@end

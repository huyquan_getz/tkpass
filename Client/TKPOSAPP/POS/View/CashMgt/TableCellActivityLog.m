//
//  TableCellActivityLog.m
//  POS
//
//  Created by Nha Duong Cong on 12/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TableCellActivityLog.h"

@implementation TableCellActivityLog

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setValueDefault{
    _lbCashIn.text=@"";
    _lbCashOut.text=@"";
    _lbDateTime.text=@"";
    _lbDescription.text=@"";
    _lbStaff.text=@"";
    _lbType.text=@"";
}
@end

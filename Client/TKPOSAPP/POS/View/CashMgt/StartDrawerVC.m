//
//  StartDrawerVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/26/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "StartDrawerVC.h"
#import "CashierBusinessModel.h"

@interface StartDrawerVC ()

@end

@implementation StartDrawerVC{

}
-(instancetype)initWithCurrency:(NSString *)currency_{
    if (self=[super init]) {
        currency=currency_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _tfAmount.text=@"";
    _tfAmount.placeholder=[TKAmountDisplay stringAmount:0 withCurrency:currency];
    [_vMain setShadowDefault];
    _vInputTextfield.layer.cornerRadius=tkCornerRadiusViewPopup;
    _vInputTextfield.layer.borderColor=tkColorFrameBorder.CGColor;
    _vInputTextfield.layer.borderWidth=1;
    
    _btStart.layer.cornerRadius=tkCornerRadiusButton;
    _btStart.backgroundColor=tkColorMainAccept;
    self.view.backgroundColor=[UIColor clearColor];
    _vMain.backgroundColor=[UIColor whiteColor];
    _vInputTextfield.backgroundColor=[UIColor whiteColor];
    
    UIView *vCorver=[[UIView alloc] initWithFrame:self.view.bounds];
    [vCorver setViewCoverDefault];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [vCorver addGestureRecognizer:tap];
    [self.view insertSubview:vCorver belowSubview:_vMain];
    
    [self showMessageError:@""];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    LanguageUtil *languageKey =[LanguageUtil sharedLanguageUtil];
    _lbTitle.text=[languageKey stringByKey:@"cash-mgt.start-drawer.title"];
    _lbTitleStartCash.text=[languageKey stringByKey:@"cash-mgt.start-drawer.lb-start-cash-in"];
}
-(void)viewDidAppear:(BOOL)animated{
    [_tfAmount becomeFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showMessageError:(NSString*)message{
    [_lbMessageStatus setTintColor:[UIColor redColor]];
    _lbMessageStatus.text=message;
}
-(void)clearMessageStatus{
    _lbMessageStatus.text=@"";
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tapCoverView:(id)sender{
    [self clickCancel:nil];
}
- (IBAction)clickCancel:(id)sender {
    if (_delegate) {
        [_delegate startDrawer:self cancel:YES];
    }
}
- (IBAction)clickStart:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self clearMessageStatus];
    if (_delegate) {
        double startingCash=[_tfAmount.text doubleValue];
        [_delegate startDrawer:self startingCash:startingCash];
    }
}
#pragma TextfieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    if (textField==_tfAmount) {
        NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
        if (![newString isTKAmountFormat]) {
            return NO;
        }
    }
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect frame=_vMain.frame;
    frame.origin.y-= 160;
    [_vMain setFrame:frame];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    CGRect frame=_vMain.frame;
    frame.origin.y+= 160;
    [_vMain setFrame:frame];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}
-(BOOL)validateInput{
    if ([_tfAmount isEmptyText]) {
        [self showMessageError:[[LanguageUtil sharedLanguageUtil] stringByKey:@"cash-mgt.pay-in-out.empty-amount"]];
        return NO;
    }
    return YES;
}
@end

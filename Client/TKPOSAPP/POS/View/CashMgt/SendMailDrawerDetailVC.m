//
//  SendMailDrawerDetailVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/30/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "SendMailDrawerDetailVC.h"
#import "Constant.h"
#import "Controller.h"
#import "QueueSendMail.h"
#import "ShiftDrawer.h"
#import "TKInventory.h"

@interface SendMailDrawerDetailVC ()

@end

@implementation SendMailDrawerDetailVC{
    LanguageUtil *languageKey;
}
-(instancetype)initWithShifDrawer:(CBLDocument *)shiftDrawer_{
    if (self=[super init]) {
        shiftDrawer=shiftDrawer_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_vMain setShadowDefault];
    _btSend.layer.cornerRadius=tkCornerRadiusButton;
    [_btSend setBackgroundColor:tkColorMainAccept];
    UIView *vLeft=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 1)];
    UIView *vRight=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 140, 1)];
    _tfEmail.leftView=vLeft;
    _tfEmail.leftViewMode=UITextFieldViewModeAlways;
    _tfEmail.rightView=vRight;
    _tfEmail.rightViewMode=UITextFieldViewModeAlways;
    self.view.backgroundColor=[UIColor clearColor];
    _vMain.backgroundColor=[UIColor whiteColor];
    UIView *vCorver=[[UIView alloc] initWithFrame:self.view.bounds];
    [vCorver setViewCoverDefault];
    [self.view insertSubview:vCorver belowSubview:_vMain];
    [self showMessageError:@""];
    _vTitle.backgroundColor=tkColorMainBackground;
    _vTitle.layer.cornerRadius=tkCornerRadiusViewPopup;
    _vLine.backgroundColor=tkColorFrameBorder;
    _tfEmail.backgroundColor=tkColorMainBackground;
    _tfEmail.layer.borderColor=tkColorFrameBorder.CGColor;
    _tfEmail.layer.borderWidth=1;
    _tfEmail.layer.cornerRadius=tkCornerRadiusButton;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
}
-(void)viewDidAppear:(BOOL)animated{
    [_tfEmail becomeFirstResponder];
}
-(void)showMessageError:(NSString*)error{
    _lbMessageStatus.textColor=[UIColor redColor];
    _lbMessageStatus.text=error;
}
-(void)showMessageSuccess:(NSString*)message{
    _lbMessageStatus.textColor=[UIColor blueColor];
    _lbMessageStatus.text=message;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma TextfieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect frame=self.vMain.frame;
    frame.origin.y-= 200;
    [self.vMain setFrame:frame];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    CGRect frame=self.vMain.frame;
    frame.origin.y+= 200;
    [self.vMain setFrame:frame];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self clickSend:nil];
    return YES;
}
-(void)tapCoverView:(id)sender{
    [self clickCancel:nil];
}
- (IBAction)clickCancel:(id)sender {
    if (_delegate) {
        [_delegate sendMailDrawer:self cancel:YES];
    }
}
- (IBAction)clickSend:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [_tfEmail resignFirstResponder];
    [self showMessageError:@""];// remove message status
    
    if (![NetworkUtil checkInternetConnection]) {
        [[NetworkUtil sharedInternetConnection]  addDelegate:[QueueSendMail sharedQueue]];
        NSDictionary *dict=[self dictionarySendMail];
        [[QueueSendMail sharedQueue] addToQueueReceipt:dict toEmail:_tfEmail.text type:MailTypeReportCashDrawer];
        UIAlertView  *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-message"] message:[languageKey stringByKey:@"cashier.order.send-email.send-email-later"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.ok"] otherButtonTitles: nil];
        [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [_delegate sendMailDrawer:self success:NO inQueue:YES];
        }];
        [self showMessageError:[languageKey stringByKey:@"general.ms.not-connect-internet"]];
        return;
    }
    MBProgressHUD *hub=[[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hub];
    hub.labelText=[languageKey stringByKey:@"cashier.order.send-email.text-sending"];
    [hub show:YES];
    dispatch_queue_t myQueue=dispatch_queue_create("queue.SendEmail", NULL);
    dispatch_async(myQueue, ^{
        BOOL sendSuccess=[self sendEmailReport];
        dispatch_async(dispatch_get_main_queue(), ^{
            [hub hide:YES];
            [hub removeFromSuperview];
            if (sendSuccess) {
                //show alert and done payment
                UIAlertView  *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-message"] message:[languageKey stringByKey:@"cash-mgt.send-email.ms-send-receipt-success"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.ok"] otherButtonTitles: nil];
                [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    [_delegate sendMailDrawer:self success:sendSuccess inQueue:NO];
                }];
            }else{
                [self showMessageError:[languageKey stringByKey:@"cash-mgt.send-email.ms-send-receipt-fail"]];
            }

        });
    });
}
-(BOOL)sendEmailReport{
    NSDictionary *dict=[self dictionarySendMail];
    return [Controller sendReceipt:dict toEmail:_tfEmail.text type:MailTypeReportCashDrawer];
}
-(NSDictionary*)dictionarySendMail{
    NSMutableDictionary *dict=[shiftDrawer.properties mutableCopy];
    ShiftDrawer *shiftObject=[[ShiftDrawer alloc] initWithDocument:shiftDrawer];
    [dict setObject:[shiftObject dictionaryReportInfo] forKey:@"reportInfo"];
    [dict setObject:[TKInventory getTimzone:[Controller getStoreInfoDoc]] forKey:@"branchTimezone"];
    return dict;
}
-(BOOL)validateInput{
    if ([_tfEmail isEmptyText]) {
        [self showMessageError:[languageKey stringByKey:@"cashier.order.send-email.email-empty"]];
        return NO;
    }
    if (![_tfEmail.text isEmailFormat]) {
        [self showMessageError:[languageKey stringByKey:@"cashier.order.send-email.email-invalid"]];
        return NO;
    }
    return YES;
}
@end

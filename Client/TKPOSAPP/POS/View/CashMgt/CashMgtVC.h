//
//  CashMgtVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
typedef enum _ShowingDrawerMain{
    ShowingDrawerMainDetail,
    ShowingDrawerMainActivityLog
}ShowingDrawerMain;

#import <UIKit/UIKit.h>
#import "ListShiftDrawerVC.h"
#import "ShiftDrawerDetailVC.h"
#import "ActivityLogVC.h"
#import "PaidInOutVC.h"
#import "SelectTimeVC.h"
#import "StartDrawerVC.h"
#import "CloseDrawerVC.h"
#import "SendMailDrawerDetailVC.h"

@interface CashMgtVC : UIViewController<ShiftDrawerDetailDelegate,PaidInOutDelegate,ListShiftDrawerDelegate,SelectTimeDelegate,StartDrawerDelegate,CloseDrawerDelegate,SendMailDrawerDelegate>{
    CBLDocument *shiftDrawerSelected;
}
- (IBAction)clickMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
- (IBAction)clickSegment:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btMenu;
@property (weak, nonatomic) IBOutlet UIImageView *ivNaviBar;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btSendMail;
- (IBAction)clickSendMail:(id)sender;
- (IBAction)clickPrint:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btPrint;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (weak, nonatomic) IBOutlet UIView *vFrameLeft;
@property (weak, nonatomic) IBOutlet UIView *vFrameMain;
@property (strong, nonatomic) ListShiftDrawerVC *listShiftDrawerVC;
@property (assign) ShowingDrawerMain showingDrawerMain;
@property (strong, nonatomic) ShiftDrawerDetailVC *shiftDrawerDetailVC;
@property (strong, nonatomic) ActivityLogVC *activityLogVC;
@property (strong, nonatomic) PaidInOutVC *paidInOutVC;
@property (strong, nonatomic) SelectTimeVC *selectTimeVC;
@property (strong, nonatomic) StartDrawerVC *startDrawerVC;
@property (strong, nonatomic) CloseDrawerVC *closeDrawerVC;
@property (strong, nonatomic) SendMailDrawerDetailVC *sendMainDrawerReportVC;
@end

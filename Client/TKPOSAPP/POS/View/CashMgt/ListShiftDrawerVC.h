//
//  ListShiftDrawerVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "ShiftDrawerManager.h"
@protocol ListShiftDrawerDelegate <NSObject>

@required
-(void)listShiftDrawer:(id)sender clickDetail:(CBLDocument*)shiftDrawer;
-(void)listShiftDrawer:(id)sender clickCalender:(BOOL)none;
-(CBLDocument*)listShiftDrawer:(id)sender requireDrawerSelected:(BOOL)none;

@end

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface ListShiftDrawerVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *listShifDrawer;
    NSInteger month;
    NSInteger year;
    CBLLiveQuery *liveQuery;
}
-(instancetype)initWithYear:(NSInteger)year_ month:(NSInteger)month_;
@property (weak, nonatomic) id<ListShiftDrawerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak, nonatomic) IBOutlet UIButton *btCalendar;
- (IBAction)clickCalendar:(id)sender;
-(void)reloadShiftDrawerWithYear:(NSInteger)year_ month:(NSInteger)month_;
@end

//
//  TableCellShiftDrawer.m
//  POS
//
//  Created by Nha Duong Cong on 12/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TableCellShiftDrawer.h"
#import "Constant.h"

@implementation TableCellShiftDrawer

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        _lbTitle.textColor=[UIColor whiteColor];
        _lbTitleTime.textColor=[UIColor whiteColor];
    }else{
        _lbTitle.textColor=[UIColor blackColor];
        _lbTitleTime.textColor=[UIColor darkGrayColor];
    }
    // Configure the view for the selected state
}
-(void)setValueDefault{
    UIView *bgSelected=[[UIView alloc] init];
    [bgSelected setBackgroundColor:tkColorMainSelected];
    [super setSelectedBackgroundView:bgSelected];
    _lbTitle.text=@"";
    _lbTitleTime.text=@"";
}

@end

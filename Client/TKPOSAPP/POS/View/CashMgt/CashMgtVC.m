//
//  CashMgtVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "CashMgtVC.h"
#import "Controller.h"
#import "TKEmployee.h"
#import "CashierBusinessModel.h"
#import "PrintingTemplateManagement.h"

@interface CashMgtVC ()

@end

@implementation CashMgtVC{
    LanguageUtil *languageKey;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _vFrameLeft.hidden=YES;
    _vFrameMain.hidden=YES;
    _segment.tintColor=tkColorMainSelected;
    [_vLine setBackgroundColor:tkColorFrameBorder];
    [_ivNaviBar setBackgroundColor:[UIColor statusNaviBar]];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    _lbTitle.text=[languageKey stringByKey:@"cash-mgt.title-drawer-summary"];
    [_segment setTitle:[languageKey stringByKey:@"cash-mgt.title-drawer-detail"] forSegmentAtIndex:0];
    [_segment setTitle:[languageKey stringByKey:@"cash-mgt.title-activity-log"] forSegmentAtIndex:1];
    [self reAddListShiftDrawer];
    [self addDetailViewDetail];
}
-(void)addDetailViewDetail{
    _showingDrawerMain=ShowingDrawerMainDetail;
    _segment.selectedSegmentIndex=0;
    [self showMainViewWithType:_showingDrawerMain];
}
-(void)reAddListShiftDrawer{
    if (_listShiftDrawerVC) {
        [_listShiftDrawerVC.view removeFromSuperview];
        [_listShiftDrawerVC removeFromParentViewController];
        _listShiftDrawerVC=nil;
    }
    NSDate *date=[NSDate date];
    _listShiftDrawerVC=[[ListShiftDrawerVC alloc] initWithYear:[[date getStringWithFormat:@"yyyy"] integerValue] month:[[date getStringWithFormat:@"MM"] integerValue]];
    _listShiftDrawerVC.delegate=self;
    _listShiftDrawerVC.view.frame=_vFrameLeft.frame;
    [self addChildViewController:_listShiftDrawerVC];
    [self.view addSubview:_listShiftDrawerVC.view];
}
-(void)removeShiftDrawerDetailVC{
    if (_shiftDrawerDetailVC) {
        [_shiftDrawerDetailVC.view removeFromSuperview];
        [_shiftDrawerDetailVC removeFromParentViewController];
        _shiftDrawerDetailVC=nil;
    }
}
-(void)removeActivityLogVC{
    if (_activityLogVC) {
        [_activityLogVC.view removeFromSuperview];
        [_activityLogVC removeFromParentViewController];
        _activityLogVC=nil;
    }
}
-(void)showMainViewWithType:(ShowingDrawerMain)typeShow{
    [self removeShiftDrawerDetailVC];
    [self removeActivityLogVC];
    _showingDrawerMain=typeShow;
    switch (_showingDrawerMain) {
        case ShowingDrawerMainDetail:{
            [self addUIShiftDrawerDetail];
            break;
        }
        case ShowingDrawerMainActivityLog:{
            [self addUIActivityLog];
            break;
        }
        default:
            break;
    }
}
-(void)addUIShiftDrawerDetail{
    CBLDocument *shiftDrawerOpen =[[ShiftDrawerManager sharedDrawerManager] currentShiftDrawer];
    BOOL enableStartClose=NO;
    if (shiftDrawerOpen == nil || shiftDrawerOpen==shiftDrawerSelected) {
        enableStartClose=YES;
    }
    if (shiftDrawerSelected) {
        _shiftDrawerDetailVC=[[ShiftDrawerDetailVC alloc] initWithShiftDrawer:shiftDrawerSelected enableStartClose:enableStartClose];
    }else{
        _shiftDrawerDetailVC =[[ShiftDrawerDetailVC alloc] initEmptyWithCurrency:[Controller currencyDefault] enableStartClose:enableStartClose];
    }
    _shiftDrawerDetailVC.delegate=self;
    _shiftDrawerDetailVC.view.frame=_vFrameMain.frame;
    [self addChildViewController:_shiftDrawerDetailVC];
    [self.view addSubview:_shiftDrawerDetailVC.view];
}
-(void)addUIActivityLog{
    _activityLogVC=[[ActivityLogVC alloc] initWithShiftDrawer:shiftDrawerSelected];
    _activityLogVC.view.frame=_vFrameMain.frame;
    [self addChildViewController:_activityLogVC];
    [self.view addSubview:_activityLogVC.view];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickMenu:(id)sender {
    [Controller showLeftMenu:self];
}
- (IBAction)clickSendMail:(id)sender {
    if (shiftDrawerSelected) {
        if (_sendMainDrawerReportVC) {
            [_sendMainDrawerReportVC.view removeFromSuperview];
            [_sendMainDrawerReportVC removeFromParentViewController];
            _sendMainDrawerReportVC=nil;
        }
        _sendMainDrawerReportVC=[[SendMailDrawerDetailVC alloc] initWithShifDrawer:shiftDrawerSelected];
        _sendMainDrawerReportVC.delegate=self;
        [self addChildViewController:_sendMainDrawerReportVC];
        [self.view addSubview:_sendMainDrawerReportVC.view];
    }
}

- (IBAction)clickPrint:(id)sender {
    if(shiftDrawerSelected){
        [[PrintingTemplateManagement sharedObject] printShiftDrawerReport:shiftDrawerSelected];
    }
}
- (IBAction)clickSegment:(UISegmentedControl*)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:{
            [self showMainViewWithType:ShowingDrawerMainDetail];
            break;
        }
        case 1:{
            [self showMainViewWithType:ShowingDrawerMainActivityLog];
            break;
        }
        default:
            break;
    }
}
#pragma  ShiftDrawerDelegate
-(void)shiftDrawerDetail:(id)sender deposit:(BOOL)none{
    _paidInOutVC=[[PaidInOutVC alloc] initWithPaidIn:YES currency:[Controller currencyDefault]];
    _paidInOutVC.delegate=self;
    [self addChildViewController:_paidInOutVC];
    [self.view addSubview:_paidInOutVC.view];
}
-(void)shiftDrawerDetail:(id)sender payOut:(BOOL)none{
    _paidInOutVC=[[PaidInOutVC alloc] initWithPaidIn:NO currency:[Controller currencyDefault]];
    _paidInOutVC.delegate=self;
    [self addChildViewController:_paidInOutVC];
    [self.view addSubview:_paidInOutVC.view];
}
-(void)shiftDrawerDetail:(id)sender startNewShift:(BOOL)none{
    if (_startDrawerVC) {
        [_startDrawerVC.view removeFromSuperview];
        [_startDrawerVC removeFromParentViewController];
        _startDrawerVC=nil;
    }
    _startDrawerVC=[[StartDrawerVC alloc] initWithCurrency:[Controller currencyDefault]];
    _startDrawerVC.delegate=self;
    [self addChildViewController:_startDrawerVC];
    [self.view addSubview:_startDrawerVC.view];
}
-(void)shiftDrawerDetail:(id)sender closeCurrentShift:(BOOL)none{
    if (_closeDrawerVC) {
        [_closeDrawerVC.view removeFromSuperview];
        [_closeDrawerVC removeFromParentViewController];
        _closeDrawerVC=nil;
    }
    ShiftDrawer *shiftDrawer=[[ShiftDrawer alloc] initWithDocument:shiftDrawerSelected]; // create tmp;
    _closeDrawerVC=[[CloseDrawerVC alloc] initWithExpectedCash:shiftDrawer.expectedTotal currency:[Controller currencyDefault]];
    _closeDrawerVC.delegate=self;
    [self addChildViewController:_closeDrawerVC];
    [self.view addSubview:_closeDrawerVC.view];
}
#pragma PaidInOutDelegate
-(void)paidInOut:(id)sender cancel:(BOOL)none{
    if (_paidInOutVC) {
        [_paidInOutVC.view removeFromSuperview];
        [_paidInOutVC removeFromParentViewController];
        _paidInOutVC=nil;
    }
}
-(void)paidInOut:(id)sender paidAmount:(double)amount comment:(NSString *)comment isPaidIn:(BOOL)isPaidIn{
    if (_paidInOutVC) {
        [_paidInOutVC.view removeFromSuperview];
        [_paidInOutVC removeFromParentViewController];
        _paidInOutVC=nil;
    }
    if (isPaidIn) {
        CBLDocument *userLogin =[Controller getUserLoginedDoc];
        ActitivityDrawer *activity =[[ActitivityDrawer alloc] initWithEmployeeId:[TKEmployee getEmployeeID:userLogin] employeeName:[TKEmployee getEmployeeName:userLogin] deposit:amount comment:comment];
        if (shiftDrawerSelected) {
            if (shiftDrawerSelected==[[ShiftDrawerManager sharedDrawerManager] currentShiftDrawer]) { // check is current
                [[ShiftDrawerManager sharedDrawerManager] addToCurrentShiftWithActivity:activity];
            }else{
                ShiftDrawer *shiftDrawer=[[ShiftDrawer alloc] initWithDocument:shiftDrawerSelected]; // create tmp;
                [shiftDrawer addActitvity:activity];
            }
        }
    }else{
        CBLDocument *userLogin =[Controller getUserLoginedDoc];
        ActitivityDrawer *activity =[[ActitivityDrawer alloc] initWithEmployeeId:[TKEmployee getEmployeeID:userLogin] employeeName:[TKEmployee getEmployeeName:userLogin] payOut:amount comment:comment];
        if (shiftDrawerSelected) {
            if (shiftDrawerSelected==[[ShiftDrawerManager sharedDrawerManager] currentShiftDrawer]) { // check is current
                [[ShiftDrawerManager sharedDrawerManager] addToCurrentShiftWithActivity:activity];
            }else{
                ShiftDrawer *shiftDrawer=[[ShiftDrawer alloc] initWithDocument:shiftDrawerSelected]; // create tmp;
                [shiftDrawer addActitvity:activity];
            }
        }
    }
}

#pragma ListShiftDrawerDelegate
-(void)listShiftDrawer:(id)sender clickDetail:(CBLDocument *)shiftDrawer{
    shiftDrawerSelected=shiftDrawer;
    [self showMainViewWithType:_showingDrawerMain];
}
-(void)listShiftDrawer:(id)sender clickCalender:(BOOL)none{
    _selectTimeVC=[[SelectTimeVC alloc] init];
    _selectTimeVC.delegate=self;
    [self addChildViewController:_selectTimeVC];
    [self.view addSubview:_selectTimeVC.view];
    NSDate *date=[NSDate date];
    [_selectTimeVC setYear:[[date getStringWithFormat:@"yyyy"] integerValue] month:[[date getStringWithFormat:@"MM"] integerValue]];
}
-(CBLDocument*)listShiftDrawer:(id)sender requireDrawerSelected:(BOOL)none{
    return shiftDrawerSelected;
}

#pragma SlectTimeDelegate
-(void)selectTime:(id)sender clickCancel:(BOOL)none{
    if (_selectTimeVC) {
        [_selectTimeVC.view removeFromSuperview];
        [_selectTimeVC removeFromParentViewController];
        _selectTimeVC=nil;
    }
}
-(void)selectTime:(id)sender clickDoneWithYear:(NSInteger)year month:(NSInteger)month{
    if (_selectTimeVC) {
        [_selectTimeVC.view removeFromSuperview];
        [_selectTimeVC removeFromParentViewController];
        _selectTimeVC=nil;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (_listShiftDrawerVC) {
            shiftDrawerSelected=nil;
            [_listShiftDrawerVC reloadShiftDrawerWithYear:year month:month];
        }
    });
}
#pragma StartDrawerDelegate
-(void)startDrawer:(id)sender cancel:(BOOL)cancel{
    if (_startDrawerVC) {
        [_startDrawerVC.view removeFromSuperview];
        [_startDrawerVC removeFromParentViewController];
        _startDrawerVC=nil;
    }
}
-(void)startDrawer:(id)sender startingCash:(double)startingCash{
    if (_startDrawerVC) {
        [_startDrawerVC.view removeFromSuperview];
        [_startDrawerVC removeFromParentViewController];
        _startDrawerVC=nil;
    }
    //execute
    shiftDrawerSelected=[[ShiftDrawerManager sharedDrawerManager] startNewShiftDrawer:startingCash currency:[Controller currencyDefault]];
}

#pragma CloseDrawerDelegate
-(void)closeDrawer:(id)sender cancel:(BOOL)none{
    if (_closeDrawerVC) {
        [_closeDrawerVC.view removeFromSuperview];
        [_closeDrawerVC removeFromParentViewController];
        _closeDrawerVC=nil;
    }
}
-(void)closeDrawer:(id)sender closingCash:(double)closingCash{
    if (_closeDrawerVC) {
        [_closeDrawerVC.view removeFromSuperview];
        [_closeDrawerVC removeFromParentViewController];
        _closeDrawerVC=nil;
    }
    //execute
    [[ShiftDrawerManager sharedDrawerManager] closeCurrentShiftDrawer:closingCash notCheckDrawer:NO];
}
#pragma SendMainDrawerReportDelegate
-(void)sendMailDrawer:(id)sender cancel:(BOOL)none{
    if (_sendMainDrawerReportVC) {
        [_sendMainDrawerReportVC.view removeFromSuperview];
        [_sendMainDrawerReportVC removeFromParentViewController];
        _sendMainDrawerReportVC=nil;
    }
}
-(void)sendMailDrawer:(id)sender success:(BOOL)success inQueue:(BOOL)inQueue{
    if (_sendMainDrawerReportVC) {
        [_sendMainDrawerReportVC.view removeFromSuperview];
        [_sendMainDrawerReportVC removeFromParentViewController];
        _sendMainDrawerReportVC=nil;
    }
}
@end

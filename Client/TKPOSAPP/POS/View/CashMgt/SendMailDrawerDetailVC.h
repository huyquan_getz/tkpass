//
//  SendMailDrawerDetailVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/30/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol SendMailDrawerDelegate <NSObject>
@required
-(void)sendMailDrawer:(id)sender success:(BOOL)success inQueue:(BOOL)inQueue;
-(void)sendMailDrawer:(id)sender cancel:(BOOL)none;

@end
#import <UIKit/UIKit.h>
#import "Constant.h"

@interface SendMailDrawerDetailVC : UIViewController<UITextFieldDelegate>{
    CBLDocument *shiftDrawer;
}
-(instancetype)initWithShifDrawer:(CBLDocument*)shiftDrawer_;
@property (weak,nonatomic) id<SendMailDrawerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
- (IBAction)clickCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
@property (weak, nonatomic) IBOutlet UIButton *btSend;
- (IBAction)clickSend:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UIView *vTitle;
@property (weak, nonatomic) IBOutlet UIView *vLine;

@end

//
//  PaidInOutVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/25/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol PaidInOutDelegate <NSObject>
@required
-(void)paidInOut:(id)sender cancel:(BOOL)none;
-(void)paidInOut:(id)sender paidAmount:(double)amount comment:(NSString*)comment isPaidIn:(BOOL)isPaidIn;

@end
#import <UIKit/UIKit.h>

@interface PaidInOutVC : UIViewController{
    NSString *currency;
    BOOL isPaidIn;
}
-(instancetype)initWithPaidIn:(BOOL)paidIn currency:(NSString*)currency;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
@property (readonly) BOOL isPaidIn;
@property (weak, nonatomic) id<PaidInOutDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
- (IBAction)clickCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btSave;
- (IBAction)clickSave:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *tfAmount;
@property (weak, nonatomic) IBOutlet UITextField *tfDescription;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UIView *vInputTextfield;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleAmount;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleDescription;

@end

//
//  CustomerVC.h
//  POS
//
//  Created by Cong Nha Duong on 3/4/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListCustomerVC.h"
#import "AddCustomerVC.h"
#import "ViewCustomerVC.h"

@interface CustomerVC : UIViewController<AddCustomerDelegate,ViewCustomerDelegate,ListCustomerDelegate>{
    NSDictionary *customerSelected;
}
@property (strong,nonatomic)ListCustomerVC *listCustomerVC;
@property (strong,nonatomic)AddCustomerVC *addCustomerVC;
@property (strong,nonatomic)ViewCustomerVC *viewCustomerVC;
@property (strong,nonatomic)UINavigationController *naviControllerCenter;
@property (weak, nonatomic) IBOutlet UIView *vFrameLeft;
@property (weak, nonatomic) IBOutlet UIView *vFrameCenter;
- (IBAction)clickAdd:(id)sender;
- (IBAction)clickMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (weak, nonatomic) IBOutlet UIImageView *ivNaviBar;
@property (weak, nonatomic) IBOutlet UILabel *lbAllCustomer;
@property (weak, nonatomic) IBOutlet UIButton *btAdd;

@end

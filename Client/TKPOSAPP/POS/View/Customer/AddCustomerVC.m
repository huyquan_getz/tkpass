//
//  AddCustomerVC.m
//  POS
//
//  Created by Cong Nha Duong on 3/4/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "AddCustomerVC.h"
#import "Constant.h"
#import "UIActionSheet+Blocks.h"
#import "UIViewController+OrientationFix.h"
#import "Controller.h"

@interface AddCustomerVC ()

@end

@implementation AddCustomerVC{
    NSDate *birthday;
    GenderType gender;
    UIView *vOverlayEditImage;
}
-(instancetype)initWithCustomer:(NSDictionary *)oldCustomer_{
    if (self=[super init]) {
        oldCustomer=oldCustomer_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _ivNaviBar.backgroundColor=[UIColor statusNaviBar];
    _bg1.backgroundColor=[UIColor whiteColor];
    _bg1.layer.borderColor=tkColorFrameBorder.CGColor;
    _bg1.layer.borderWidth=1;
    _bg1.layer.cornerRadius=tkCornerRadiusViewPopup;
    _bg2.backgroundColor=[UIColor whiteColor];
    _bg2.layer.borderColor=tkColorFrameBorder.CGColor;
    _bg2.layer.borderWidth=1;
    _bg2.layer.cornerRadius=tkCornerRadiusViewPopup;
    _bg3.backgroundColor=[UIColor whiteColor];
    _bg3.layer.borderColor=tkColorFrameBorder.CGColor;
    _bg3.layer.borderWidth=1;
    _bg3.layer.cornerRadius=tkCornerRadiusViewPopup;
    _ivAvata.layer.cornerRadius=_ivAvata.frame.size.width/2;
    _ivAvata.clipsToBounds=YES;
    _ivAvata.layer.borderColor=tkColorFrameBorder.CGColor;
    _ivAvata.layer.borderWidth=1;
    
    [_swAcceptNewsletter setOnTintColor:tkColorMainSelected];
    [_swAcceptSMS setOnTintColor:tkColorMainSelected];
    [_swActive setOnTintColor:tkColorMainSelected];
    _scrMain.backgroundColor=tkColorMainBackground;
    self.view.backgroundColor=tkColorMainBackground;
    _scrMain.contentSize=CGSizeMake(_scrMain.frame.size.width, 825);
    
    
    vOverlayEditImage=[[UIView alloc] init];
    vOverlayEditImage.layer.borderColor=[UIColor colorWithRed:1 green:1 blue:1 alpha:0.4].CGColor;
    vOverlayEditImage.layer.borderWidth=1;
    vOverlayEditImage.userInteractionEnabled=NO;

    // Do any additional setup after loading the view from its nib.
}
-(void)setValueDefault{
    if (oldCustomer) {
        _tfFirstName.text=oldCustomer[@"fullName"];
        _tfPhoneNumber.text=oldCustomer[@"phone"];
        _lbTitle.text=@"Update Customer Profile";
    }else{
        [_tfFirstName clearText];
        [_tfLastName clearText];
        [_tfEmail clearText];
        [_tfPhoneNumber clearText];
        [_tfGender clearText];
        [_tfBirthDay clearText];
        [_tfStreetAddress clearText];
        [_tfZipcode clearText];
        [_tfState clearText];
        [_tfCountry clearText];
        [_tfRemax clearText];
        [_swAcceptNewsletter setOn:YES];
        [_swAcceptSMS setOn:YES];
        [_swActive setOn:YES];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [self hideMessageError];
    [self setValueDefault];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)showMessageError:(NSString*)error{
    _lbMessage.text=error;
    if (_lbMessage.hidden) {
        _lbMessage.hidden=NO;
        CGRect frame=_scrMain.frame;
        frame.origin.y+=25;
        frame.size.height-=25;
        _scrMain.frame=frame;
    }
}
-(void)hideMessageError{
    if (_lbMessage.hidden==NO) {
        _lbMessage.text=@"";
        _lbMessage.hidden=YES;
        CGRect frame=_scrMain.frame;
        frame.origin.y-=25;
        frame.size.height+=25;
        _scrMain.frame=frame;
    }
}
- (IBAction)clickBack:(id)sender {
    if (_delegate) {
        [_delegate addCustomer:self clickBack:YES];
    }
}

- (IBAction)clickSave:(id)sender {
    if (![self validateInput]) {
        return;
    }else{
        if (_delegate) {
            [_delegate addCustomer:self editCustomer:nil withNewData:nil];
        }
    }
}
-(BOOL)validateInput{
    if ([_tfFirstName isEmptyText]) {
        [self showMessageError:@"Please enter firstname to continue"];
        [_lbTitleFirstName setTextColor:[UIColor redColor] occurenceOfString:@"*"];
        return NO;
    }else{
        [_lbTitleFirstName setTextColor:[UIColor darkGrayColor] occurenceOfString:@"*"];
    }
    if ([_tfLastName isEmptyText]) {
        [self showMessageError:@"Please enter lastname to continue"];
        [_lbTitleLastname setTextColor:[UIColor redColor] occurenceOfString:@"*"];
        return NO;
    }else{
        [_lbTitleLastname setTextColor:[UIColor darkGrayColor] occurenceOfString:@"*"];
    }
    if ([_tfEmail isEmptyText]) {
        [self showMessageError:@"Please enter email to continue"];
        [_lbTitleEmail setTextColor:[UIColor redColor] occurenceOfString:@"*"];
        return NO;
    }else{
        [_lbTitleEmail setTextColor:[UIColor darkGrayColor] occurenceOfString:@"*"];
    }
    if (![_tfEmail.text isEmailFormat]) {
        [self showMessageError:@"Email is invalid"];
        [_lbTitleEmail setTextColor:[UIColor redColor] occurenceOfString:@"*"];
        return NO;
    }
    if ([_tfPhoneNumber isEmptyText]) {
        [self showMessageError:@"Please enter phone number to continue"];
        [_lbTitlePhone setTextColor:[UIColor redColor] occurenceOfString:@"*"];
        return NO;
    }else{
        [_lbTitlePhone setTextColor:[UIColor darkGrayColor] occurenceOfString:@"*"];
    }
    return YES;
}
- (IBAction)clickDelete:(id)sender {
    
}
- (IBAction)clickSelectGender:(id)sender {
    [self.view endEditing:YES];
    BRListPicker *list=[[BRListPicker alloc] initWithListObject:@[@"Male",@"Female"] setDisplay:^NSString *(id object) {
        return (NSString*)object;
    }];
    [list setIndexDefault:gender];
    list.delegate=self;
    [list showPickerOnVC:self];
}

- (IBAction)clickSelectDate:(id)sender {
    [self.view endEditing:YES];
    BRDatePicker *datePicker=[[BRDatePicker alloc] init];
    [datePicker setDateDefault:birthday];
    datePicker.delegate=self;
    [datePicker showPickerOnVC:self];
}

- (IBAction)clickSelectAvata:(id)sender {
    [self.view endEditing:YES];
    UIActionSheet *actionSheet=[[UIActionSheet alloc] initWithTitle:@"Select Picture" delegate:nil cancelButtonTitle:nil destructiveButtonTitle:@"Take Photo" otherButtonTitles:@"Choose from library", nil];
    actionSheet.delegate=self;
    [actionSheet showFromRect:_ivAvata.frame inView:self.scrMain animated:YES];
}
#pragma ActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.allowsEditing = YES;
        imagePickerController.delegate = self;
        
        if ((![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])) {
            [UIAlertView showMessage:@"Your device doesn't support camera."];
            return;
        }
        else{
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            [imagePickerController.navigationBar setTintColor:[UIColor whiteColor]];
            [imagePickerController.navigationBar setTranslucent:NO];
            [imagePickerController.navigationBar setBarTintColor:tkColorMain];
            [[Controller appDelegate].slideMenuController presentModalViewController:imagePickerController animated:NO];
        }
    }else if (buttonIndex==1){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
        [imagePickerController.navigationBar setTintColor:[UIColor whiteColor]];
        [imagePickerController.navigationBar setTranslucent:NO];
        [imagePickerController.navigationBar setBarTintColor:tkColorMain];
        [[Controller appDelegate].slideMenuController presentModalViewController:imagePickerController animated:YES];
    }
}

#pragma BRDatePickerDelegate
-(void)brDatePicker:(id)sender cancelSetDate:(BOOL)none{
    
}
-(void)brDatePicker:(id)sender clickClear:(BOOL)none{
    _tfBirthDay.text=@"";
}
-(void)brDatePicker:(id)sender setDate:(NSDate *)dateSet{
    _tfBirthDay.text=[dateSet getStringWithFormat:@"MMM dd, yyyy"];
    birthday=dateSet;
}
#pragma BRListPickerDelegate
-(void)brListPicker:(id)sender cancelSelect:(BOOL)none{
    
}
-(void)brListPicker:(id)sender selectIndex:(NSInteger)indexSelected valueSelected:(id)object{
    _tfGender.text=object;
    gender=(GenderType)indexSelected;
}
#pragma UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image =[self cropImageWithInfo:info];//[info objectForKey:UIImagePickerControllerOriginalImage];
    image=[image resizeImageWithSizeMax:CGSizeMake(200, 200) cropSquare:YES];
    _ivAvata.image=image;
    [[Controller appDelegate].slideMenuController dismissViewControllerAnimated:YES completion:nil];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [[Controller appDelegate].slideMenuController dismissViewControllerAnimated:YES completion:nil];
}
- (UIImage *)cropImageWithInfo:(NSDictionary *)info
{
    // gets the original image along with it's size.
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
    CGSize size = image.size;
    
    // crops the crop rect that the user selected.
    CGRect cropRect = [info[@"UIImagePickerControllerCropRect"] CGRectValue];
    
    // creates a graphics context of the correct size.
    UIGraphicsBeginImageContext(cropRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // checks and corrects the image orientation.
    UIImageOrientation orientation = [image imageOrientation];
    if(orientation == UIImageOrientationUp) {
        CGContextTranslateCTM(context, 0, size.height);
        CGContextScaleCTM(context, 1, -1);
        
        cropRect = CGRectMake(cropRect.origin.x,
                              -cropRect.origin.y,
                              cropRect.size.width,
                              cropRect.size.height);
    }
    else if(orientation == UIImageOrientationRight) {
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextRotateCTM(context, -M_PI/2);
        size = CGSizeMake(size.height, size.width);
        
        cropRect = CGRectMake(cropRect.origin.y,
                              cropRect.origin.x,
                              cropRect.size.height,
                              cropRect.size.width);
    }
    else if(orientation == UIImageOrientationDown) {
        CGContextTranslateCTM(context, size.width, 0);
        CGContextScaleCTM(context, -1, 1);
        
        cropRect = CGRectMake(-cropRect.origin.x,
                              cropRect.origin.y,
                              cropRect.size.width,
                              cropRect.size.height);
    }
    
    // draws the image in the correct place.
    CGContextTranslateCTM(context, -cropRect.origin.x, -cropRect.origin.y);
    CGContextDrawImage(context,
                       CGRectMake(0,0, size.width, size.height),
                       image.CGImage);
    
    // and pull out the cropped image
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return croppedImage;
}
#pragma NavigatonControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([viewController isKindOfClass:NSClassFromString(@"PUUIImageViewController")] || [viewController isKindOfClass:NSClassFromString(@"PLUIImageViewController")]) {  // PUUIImageViewController ios8 , PLUIImageViewController ios 7
        CGRect frame=viewController.view.frame;
        frame.origin.y=51;
        frame.size.height=666;
        frame.origin.x=(frame.size.width-666)/2;
        frame.size.width=666;
        vOverlayEditImage.frame=frame;
        [viewController.view addSubview:vOverlayEditImage];
    }else{
        
    }
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


#pragma TextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == _tfStreetAddress|| textField==_tfZipcode || textField==_tfState ||textField== _tfCountry ||textField==_tfRemax) {
        CGRect frame=_scrMain.frame;
        frame.origin.y-=300;
        _scrMain.frame=frame;
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == _tfStreetAddress|| textField==_tfZipcode || textField==_tfState ||textField== _tfCountry ||textField==_tfRemax) {
        CGRect frame=_scrMain.frame;
        frame.origin.y+=300;
        _scrMain.frame=frame;
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField==_tfFirstName) {
        [_tfLastName becomeFirstResponder];
    }else if (textField==_tfLastName){
        [_tfEmail becomeFirstResponder];
    }else if (textField== _tfEmail){
        [_tfPhoneNumber becomeFirstResponder];
    }else if (textField== _tfStreetAddress){
        [_tfZipcode becomeFirstResponder];
    }else if(textField==_tfZipcode){
        [_tfState becomeFirstResponder];
    }else if (textField==_tfState){
        [_tfCountry becomeFirstResponder];
    }else if (textField==_tfCountry){
        [_tfRemax becomeFirstResponder];
    }
    
    else [textField resignFirstResponder];
    return YES;
}
@end

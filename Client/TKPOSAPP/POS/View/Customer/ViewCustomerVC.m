//
//  ViewCustomerVC.m
//  POS
//
//  Created by Cong Nha Duong on 3/4/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "ViewCustomerVC.h"
#import "Constant.h"

@interface ViewCustomerVC ()

@end

@implementation ViewCustomerVC
-(instancetype)initWithCustomer:(NSDictionary *)customer_{
    if (self=[super init]) {
        customer=customer_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _ivNaviBar.backgroundColor=[UIColor statusNaviBar];
    _scrMain.backgroundColor=tkColorMainBackground;
    NSArray *bgs=@[_bg1,_bg2,_bg3,_bg4];
    for (UIView *vv in bgs) {
        vv.backgroundColor=[UIColor whiteColor];
        vv.layer.cornerRadius=tkCornerRadiusViewPopup;
        vv.layer.borderWidth=1;
        vv.layer.borderColor=tkColorFrameBorder.CGColor;
    }
    NSArray *tfs=@[_tfMemberID,_tfName,_tfEmail,_tfPhoneNb,_tfGender,_tfBirthday,_tfAddress,_tfZipcode,_tfState,_tfCountry,_tfRemart,_tfLastLogin,_tfLastOrder,_tfTotalOrder,_tfTotalSpend,_tfMemberSince,_tfStatusAcceptNewsletter,_tfStatusAcceptSMS,_tfStatusActive];
    for (UITextField *tf  in tfs) {
        tf.enabled=NO;
    }
    _scrMain.contentSize=CGSizeMake(_scrMain.frame.size.width, 1125);
    // Do any additional setup after loading the view from its nib.
    if (customer==nil) {
        _scrMain.hidden=YES;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    _tfName.text=customer[@"fullName"];
    _tfPhoneNb.text=customer[@"phone"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickEdit:(id)sender {
    if (_delegate) {
        [_delegate viewCustomer:self clickEditCustomer:customer];
    }
}
@end

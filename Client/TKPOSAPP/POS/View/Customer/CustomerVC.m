//
//  CustomerVC.m
//  POS
//
//  Created by Cong Nha Duong on 3/4/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "CustomerVC.h"
#import "Constant.h"
#import "Controller.h"
#import "AddCustomerVC.h"
#import "ViewCustomerVC.h"

@interface CustomerVC ()

@end

@implementation CustomerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _vFrameLeft.hidden=YES;
    _vFrameCenter.hidden=YES;
    _vLine.backgroundColor=tkColorFrameBorder;
    _naviControllerCenter =[[UINavigationController alloc] init];
    [_naviControllerCenter setNavigationBarHidden:YES];
    [self addChildViewController:_naviControllerCenter];
    _naviControllerCenter.view.frame=_vFrameCenter.frame;
    [self.view addSubview:_naviControllerCenter.view];
    [_ivNaviBar setBackgroundColor:[UIColor statusNaviBar]];
    [self addListCustomerVC];
    // Do any additional setup after loading the view from its nib.
}
-(void)addListCustomerVC{
    if (_listCustomerVC) {
        [_listCustomerVC.view removeFromSuperview];
        [_listCustomerVC removeFromParentViewController];
        _listCustomerVC=nil;
    }
    _listCustomerVC=[[ListCustomerVC alloc] init];
    _listCustomerVC.delegate=self;
    [self addChildViewController:_listCustomerVC];
    _listCustomerVC.view.frame=_vFrameLeft.frame;
    [self.view addSubview:_listCustomerVC.view];
    
}
-(void)addCenterView:(UIViewController*)centerView{
    [_naviControllerCenter setViewControllers:@[centerView] animated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickAdd:(id)sender {
    _btAdd.enabled=NO;
    AddCustomerVC *add=[[AddCustomerVC alloc] init];
    add.delegate=self;
    add.view.frame=_vFrameCenter.frame;
    [_naviControllerCenter pushViewController:add animated:YES];
}

- (IBAction)clickMenu:(id)sender {
    [Controller showLeftMenu:self];
}
#pragma AddCustomerDelegate
-(void)addCustomer:(id)sender clickBack:(BOOL)none{
    [_naviControllerCenter popViewControllerAnimated:YES];
    _btAdd.enabled=YES;
}
-(void)addCustomer:(id)sender editCustomer:(NSDictionary *)oldCustomer withNewData:(NSDictionary *)newData{
    [_naviControllerCenter popViewControllerAnimated:YES];
    _btAdd.enabled=YES;
}
#pragma ViewCustomerDelegate
-(void)viewCustomer:(id)sender clickEditCustomer:(NSDictionary *)customer{
    _btAdd.enabled=NO;
    AddCustomerVC *edit=[[AddCustomerVC alloc] initWithCustomer:customer];
    edit.delegate=self;
    edit.view.frame=_vFrameCenter.frame;
    [_naviControllerCenter pushViewController:edit animated:YES];
}

#pragma ListCustomerDelegate
-(void)listCustomer:(id)sender clickDetailWithCustomer:(NSDictionary *)customer{
    customerSelected =customer;
    ViewCustomerVC *viewc=[[ViewCustomerVC alloc] initWithCustomer:customer];
    viewc.delegate=self;
    [self addCenterView:viewc];
    _btAdd.enabled=YES;
}
-(NSDictionary *)listCustomer:(id)sender requireCustomerSelected:(BOOL)none{
    return customerSelected;
}
-(void)listCustomer:(id)sender resetCustomerSelected:(BOOL)none{
    customerSelected=nil;
}
@end

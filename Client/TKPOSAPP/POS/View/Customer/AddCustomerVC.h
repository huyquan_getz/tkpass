//
//  AddCustomerVC.h
//  POS
//
//  Created by Cong Nha Duong on 3/4/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
@protocol AddCustomerDelegate <NSObject>

-(void)addCustomer:(id)sender clickBack:(BOOL)none;
-(void)addCustomer:(id)sender editCustomer:(NSDictionary*)oldCustomer withNewData:(NSDictionary*)newData;

@end
#import <UIKit/UIKit.h>
#import "BRDatePicker.h"
#import "BRListPicker.h"

@interface AddCustomerVC : UIViewController<BRDatePickerDelegate,BRListPicker,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UITextFieldDelegate>{
    NSDictionary *oldCustomer;
}
-(instancetype)initWithCustomer:(NSDictionary*)oldCustomer_;
@property (weak,nonatomic) id<AddCustomerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *ivNaviBar;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
- (IBAction)clickBack:(id)sender;
- (IBAction)clickSave:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btDelete;
- (IBAction)clickDelete:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrMain;
@property (weak, nonatomic) IBOutlet UILabel *bg1;
@property (weak, nonatomic) IBOutlet UILabel *bg2;
@property (weak, nonatomic) IBOutlet UILabel *bg3;
@property (weak, nonatomic) IBOutlet UIImageView *ivAvata;
@property (weak, nonatomic) IBOutlet UITextField *tfFirstName;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfLastName;
@property (weak, nonatomic) IBOutlet UITextField *tfPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfGender;
@property (weak, nonatomic) IBOutlet UITextField *tfBirthDay;
@property (weak, nonatomic) IBOutlet UITextField *tfStreetAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfZipcode;
@property (weak, nonatomic) IBOutlet UITextField *tfState;
@property (weak, nonatomic) IBOutlet UITextField *tfCountry;
@property (weak, nonatomic) IBOutlet UITextField *tfRemax;
@property (weak, nonatomic) IBOutlet UISwitch *swAcceptNewsletter;
@property (weak, nonatomic) IBOutlet UISwitch *swAcceptSMS;
@property (weak, nonatomic) IBOutlet UIButton *btSelectAvata;
@property (weak, nonatomic) IBOutlet UISwitch *swActive;
- (IBAction)clickSelectGender:(id)sender;
- (IBAction)clickSelectDate:(id)sender;
- (IBAction)clickSelectAvata:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbMessage;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleFirstName;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleLastname;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbTitlePhone;


@end

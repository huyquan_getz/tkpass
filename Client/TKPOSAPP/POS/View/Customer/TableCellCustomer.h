//
//  TableCellCustomer.h
//  POS
//
//  Created by Cong Nha Duong on 3/4/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLImageView.h"

@interface TableCellCustomer : UITableViewCell
@property (weak, nonatomic) IBOutlet DLImageView *ivMain;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (weak, nonatomic) IBOutlet UILabel *lbShortName;
-(void)setValueDefault;
@end

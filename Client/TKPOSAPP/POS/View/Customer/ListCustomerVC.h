//
//  ListCustomerVC.h
//  POS
//
//  Created by Cong Nha Duong on 3/4/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
@protocol ListCustomerDelegate <NSObject>
-(void)listCustomer:(id)sender clickDetailWithCustomer:(NSDictionary*)customer;
-(NSDictionary*)listCustomer:(id)sender requireCustomerSelected:(BOOL)none;
-(void)listCustomer:(id)sender resetCustomerSelected:(BOOL)none;
@end

#import <UIKit/UIKit.h>

@interface ListCustomerVC : UIViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *listCustomerOrginal;
    NSMutableArray *listCustomerShow;
    BOOL isSearching;
    NSString *oldKeySearch;
}
@property(weak,nonatomic) id<ListCustomerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *btCancelSearch;
- (IBAction)clickCancelSearch:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbNoCustomer;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak, nonatomic) IBOutlet UIView *vLine;

@end

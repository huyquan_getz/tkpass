//
//  PaymentVC.m
//  POS
//
//  Created by Nha Duong Cong on 11/28/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "PaymentVC.h"
#import "PaymentCashVC.h"
#import "PaymentExternalVC.h"


@interface PaymentVC ()

@end

@implementation PaymentVC{
    LanguageUtil *languageKey;
}
@synthesize totalCharge;
-(instancetype)initWithTotalCharge:(double)ttCharge currency:(NSString *)currency_{
    if (self = [super init]) {
        currency=currency_;
        totalCharge=ttCharge;
        listPaymentChild=[[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [_vCover addGestureRecognizer:tap];
    
    [_vMain setShadowWithCornerRadius:tkCornerRadiusViewPopup];
    
    [_btPayCash setBackgroundImage:[UIImage imageWithColor:tkColorMainSelected] forState:UIControlStateSelected];
    [_btPayCash setBackgroundImage:[UIImage imageWithColor:tkColorMainSelected] forState:UIControlStateHighlighted];
    _btPayCash.clipsToBounds=YES;
    _btPayCash.layer.borderColor=tkColorMainSelected.CGColor;
    _btPayCash.layer.borderWidth=1;
    _btPayCash.layer.cornerRadius=tkCornerRadiusButton;
    _btPayCash.backgroundColor=[UIColor clearColor];
    [_btPayCash setTitleColor:tkColorMainSelected forState:UIControlStateNormal];
    [_btPayCash setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_btPayCash setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    [_btPayExternal setBackgroundImage:[UIImage imageWithColor:tkColorMainSelected] forState:UIControlStateSelected];
    [_btPayExternal setBackgroundImage:[UIImage imageWithColor:tkColorMainSelected] forState:UIControlStateHighlighted];
    _btPayExternal.clipsToBounds=YES;
    _btPayExternal.layer.borderColor=tkColorMainSelected.CGColor;
    _btPayExternal.layer.borderWidth=1;
    _btPayExternal.layer.cornerRadius=tkCornerRadiusButton;
    _btPayExternal.backgroundColor=[UIColor clearColor];
    [_btPayExternal setTitleColor:tkColorMainSelected forState:UIControlStateNormal];
    [_btPayExternal setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_btPayExternal setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    _vLine1.backgroundColor=tkColorFrameBorder;
    _vLine2.backgroundColor=tkColorFrameBorder;
    
    [self setValueDefault];
    [self reAddPaymenter];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [self resetRemainCharge];
    [_btPayCash setTitle:[languageKey stringByKey:@"cashier.payment.bt.pay-cash"] forState:UIControlStateNormal];
    [_btPayExternal setTitle:[languageKey stringByKey:@"cashier.payment.bt.pay-external"] forState:UIControlStateNormal];
}
-(void)resetRemainCharge{
    double currentCharge=totalCharge - [PaymentBusinessModel totalChargeWithListPaymentChild:listPaymentChild];
    _lbTitlePayment.text=[TKAmountDisplay stringAmount:currentCharge withCurrency:currency];
}
-(void)setValueDefault{
    currentPaymenterType=PaymenterTypeCash;
    _btPayCash.selected=YES;
    _btPayExternal.selected=NO;
}
-(void)reAddPaymenter{
    if (_paymenter) {
        [_paymenter.view removeFromSuperview];
        [_paymenter removeFromParentViewController];
        _paymenter=nil;
    }
    double currentCharge=totalCharge - [PaymentBusinessModel totalChargeWithListPaymentChild:listPaymentChild];
    _paymenter=[self createPaymenterWithType:currentPaymenterType currentCharge:currentCharge];
    if(_paymenter){
        [self addChildViewController:_paymenter];
        _paymenter.view.frame=_vFramePay.frame;
        [_vMain addSubview:_paymenter.view];
    }
}
-(UIViewController<PaymenterDataSource>*)createPaymenterWithType:(PaymenterType)type currentCharge:(double)crCharge{
    if (type==PaymenterTypeCash) {
        PaymentCashVC *cash=[[PaymentCashVC alloc] initWithAmountPay:crCharge currency:currency];
        cash.delegate=self;
        return cash;
    }else if(type==PaymenterTypeExternal){
        PaymentExternalVC *external = [[PaymentExternalVC alloc]initWithAmountPay:crCharge currency:currency listCreditTypes:[PaymentBusinessModel getListCreditPayment]];
        external.delegate=self;
        return external;
    }
    
    return nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tapCoverView:(id)sender{
    //[self clickCancel:nil];
}
- (IBAction)clickCancel:(id)sender {
    UIAlertView *alert =[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"cashier.order.send-email.confirm-cancel-payment"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            if (_delegate) {
                [_delegate paymentCancel:self];
            }
        }
    }];
}
- (IBAction)clickPayCash:(id)sender {
    
    _btPayExternal.selected=NO;
    _btPayCash.selected=YES;
    //CODE
    currentPaymenterType=PaymenterTypeCash;
    [self reAddPaymenter];
}
-(IBAction)clickPayExternal:(id)sender{
    _btPayCash.selected=NO;
    _btPayExternal.selected=YES;
    //CODE
    currentPaymenterType=PaymenterTypeExternal;
    [self reAddPaymenter];
}
#pragma ChargeLessDelegate
-(void)chargeLess:(id)sender chargeWithType:(PaymenterType)typeCharge{
    [self hidenChargeLess];
    [self resetRemainCharge];
    switch (typeCharge) {
        case PaymenterTypeCash:
            [self clickPayCash:nil];
            break;
        case PaymenterTypeExternal:
            [self clickPayExternal:nil];
            break;
        default:
            break;
    }
}
#pragma PaymenterDelegate
-(void)paymenter:(id)sender pay:(PaymentChild *)paymentChild{
    if (listPaymentChild==nil) {
        listPaymentChild=[[NSMutableArray alloc] init];
    }
    [listPaymentChild addObject:paymentChild];
    [self checkTotalCharged];
}

-(void)checkTotalCharged{
    double ttCharged =[PaymentBusinessModel totalChargeWithListPaymentChild:listPaymentChild];
    ttCharged =[TKAmountDisplay tkRoundCash:ttCharged];
    if (ttCharged <totalCharge) {
        [self addChargeLess];
    }else{
        if (_delegate) {
            [_delegate payment:self payment:listPaymentChild completed:YES];
        }
    }
}

-(void)addChargeLess{
    if (_chargeLess) {
        [_chargeLess.view removeFromSuperview];
        [_chargeLess removeFromParentViewController];
        _chargeLess=nil;
    }
     _chargeLess =[[ChargeLessVC alloc] initWithTotalAmout:totalCharge listPaymentChildren:listPaymentChild currency:currency];
    _chargeLess.delegate=self;
    [self addChildViewController:_chargeLess];
    CGRect frame=_chargeLess.view.frame;
    frame.origin.y=71;
    frame.size.height =self.vMain.frame.size.height-frame.origin.y -15;
    [_chargeLess.view setFrame:frame];
    [self.vMain addSubview:_chargeLess.view];
}
-(void)hidenChargeLess{
    if (_chargeLess) {
        [_chargeLess.view removeFromSuperview];
        [_chargeLess removeFromParentViewController];
        _chargeLess=nil;
    }
}
-(NSArray *)listPaymentChild{
    return [listPaymentChild copy];
}
@end

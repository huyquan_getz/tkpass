//
//  SendEmailReceiptVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/1/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol SendEmailDelegate <NSObject>

@required
-(void)sendEmailFinishPayment:(id)sender;
-(void)sendEmailCancelPayment:(id)sender;

@end

#import <UIKit/UIKit.h>
#import "Constant.h"
@interface SendEmailReceiptVC : UIViewController<UITextFieldDelegate>{
    double lastGetCharge;
    double outChange;
    NSString *currency;
    CBLDocument *orderReceipt;
}
-(instancetype)initWithLastCharge:(double)lastGetCharge outChange:(double)outChange currency:(NSString*)currency order:(CBLDocument*)orderReceipt;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
- (IBAction)clickCancel:(id)sender;
@property (weak,nonatomic) id<SendEmailDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbChange;
@property (weak, nonatomic) IBOutlet UILabel *lbLastCharge;
@property (weak, nonatomic) IBOutlet UILabel *lbSendReceipt;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UIButton *btSend;
- (IBAction)clickSend:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
@property (weak, nonatomic) IBOutlet UIButton *btClose;
- (IBAction)clickClose:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vMainSendMail;
@property (weak, nonatomic) IBOutlet UIView *vMain;

@end

//
//  OrderVC.h
//  POS
//
//  Created by Nha Duong Cong on 11/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "ProductOrderSingle.h"
#import "AdhocProduct.h"

@protocol OrderDelegate <NSObject>
@required
-(void)orderClose:(id)sender andReAddNew:(BOOL)reAdd;
-(void)orderCancel:(id)sender listProductOrder:(NSArray*)listProductOrder;
-(void)order:(id)sender clickDetail:(ProductOrderSingle*)productOrder;
-(void)order:(id)sender clickDetailAdhoc:(AdhocProduct*)adhocProduct;
-(void)order:(id)sender clickPayment:(double)totalCharge;
@end
#import <UIKit/UIKit.h>
#import "TableCellProductOrder.h"
#import "CashierBusinessModel.h"
#import "TKOrder.h"
#import "TKTaxServiceCharge.h"
@interface OrderVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *listProductOrders;
    NSMutableArray *listAdhocProduct;
    NSDate *createdDate;
    CashierBusinessModel *cashierBusiness;
    CBLDocument *orderDoc;
}
-(instancetype)initWithDefault;
-(instancetype)initWithOrder:(CBLDocument*)orderDocument;
@property (weak, nonatomic) IBOutlet UIButton *btCustomer;
-(BOOL)isEmptyOrder;
@property (weak,nonatomic) id<OrderDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *btCancelNewOrder;
- (IBAction)clickCancelNewOrder:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTimeNewOrder;
@property (weak, nonatomic) IBOutlet UILabel *lbOrderCode;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak, nonatomic) IBOutlet UIButton *btPayment;
- (IBAction)clickPayment:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vLine1;
@property (weak, nonatomic) IBOutlet UIView *vLine2;

-(void)editProductOrder:(ProductOrderSingle*)oldProductOrder withProductOrder:(ProductOrderSingle*)newProductOrder ;
-(void)editAdhocProduct:(AdhocProduct *)oldAdhocProduct withAdhocProduct:(AdhocProduct *)newAdhocProduct;
-(void)editDiscountCart:(TKDiscount*)oldDiscount withDiscountCart:(TKDiscount*)newDiscount;
-(NSUInteger)currentQuanityWithProductOrder:(ProductOrderSingle*)ProductOrderCheck withEditOldProductOrder:(ProductOrderSingle*)oldProductOrder;
-(NSUInteger)currentQuantityOrderWithProductId:(NSString *)productId;
-(CBLDocument*)orderDocument;
-(TKDiscount*)currentDiscountCart;
-(void)willRemoveSelf;
-(void)cancelPayment;
@end

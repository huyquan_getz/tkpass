//
//  ChargeLessVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/1/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "ChargeLessVC.h"
#import "CashierBusinessModel.h"
#import "PaymentBusinessModel.h"
#import "Constant.h"

@interface ChargeLessVC ()

@end

@implementation ChargeLessVC{
    LanguageUtil *languageKey;
}
-(instancetype)initWithTotalAmout:(double)totalAmount_ listPaymentChildren:(NSArray*)listPaymentChildren_ currency:(NSString *)currency_{
    if (self = [super init]) {
        totalAmount=totalAmount_;
        listPaymentChildren=listPaymentChildren_;
        currency=currency_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_btPayCash setBackgroundImage:[UIImage imageWithColor:tkColorMainSelected] forState:UIControlStateHighlighted];
    _btPayCash.clipsToBounds=YES;
    _btPayCash.layer.borderColor=tkColorMainSelected.CGColor;
    _btPayCash.layer.borderWidth=1;
    _btPayCash.layer.cornerRadius=tkCornerRadiusButton;
    _btPayCash.backgroundColor=[UIColor clearColor];
    [_btPayCash setTitleColor:tkColorMainSelected forState:UIControlStateNormal];
    [_btPayCash setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    [_btPayExternal setBackgroundImage:[UIImage imageWithColor:tkColorMainSelected] forState:UIControlStateHighlighted];
    _btPayExternal.clipsToBounds=YES;
    _btPayExternal.layer.borderColor=tkColorMainSelected.CGColor;
    _btPayExternal.layer.borderWidth=1;
    _btPayExternal.layer.cornerRadius=tkCornerRadiusButton;
    _btPayExternal.backgroundColor=[UIColor clearColor];
    [_btPayExternal setTitleColor:tkColorMainSelected forState:UIControlStateNormal];
    [_btPayExternal setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [_btPayCash setTitle:[languageKey stringByKey:@"cashier.payment.bt.pay-cash"] forState:UIControlStateNormal];
    [_btPayExternal setTitle:[languageKey stringByKey:@"cashier.payment.bt.pay-external"] forState:UIControlStateNormal];
    
    double amountDue =totalAmount - [PaymentBusinessModel totalChargeWithListPaymentChild:listPaymentChildren];
    _lbTitleAmountDue.text=[NSString stringWithFormat:@"%@ %@",[languageKey stringByKey:@"cashier.order.charge-less.text-amount-due"],[TKAmountDisplay stringAmount:amountDue withCurrency:currency]];
    _lbMessageContinue.text=[languageKey stringByKey:@"cashier.order.charge-less.lb.title-continue-to-pay"];
    _lbTitlePaidAmount.text=[languageKey stringByKey:@"cashier.order.charge-less.lb.paid-amount"];
    [self addPaidAmount];
}
-(void)addPaidAmount{
    CGPoint pointStart=CGPointZero;
    NSInteger heighCell=30;
    NSInteger spaceY=10;
    NSInteger spaceX=10;
    NSInteger widthTitle=150;
    pointStart.x=275;
    pointStart.y=18;

    NSArray *listPaymentType=[PaymentBusinessModel listPaymentTypeWithListPaymentChild:listPaymentChildren];
    
    NSInteger maxWidthDetail=0;
    for (NSString *paymentType in listPaymentType) {
        NSArray *array=[PaymentBusinessModel subListPaymentChildWithListPaymentChild:listPaymentChildren paymentType:paymentType];
        NSString *string=[TKAmountDisplay stringAmount:[PaymentBusinessModel totalChargeWithListPaymentChild:array] withCurrency:currency];
        UILabel *lbDetail=[self lablePaidAmount];
        NSInteger currentWidth=[string sizeWithAttributes:@{NSFontAttributeName:lbDetail.font}].width;
        if (currentWidth > maxWidthDetail) {
            maxWidthDetail=currentWidth;
        }
    }
    maxWidthDetail +=10;
    for (NSString *paymentType in listPaymentType) {
        NSArray *array=[PaymentBusinessModel subListPaymentChildWithListPaymentChild:listPaymentChildren paymentType:paymentType];
        UILabel *lbTitle=[self lablePaidAmount];
        UILabel *lbDetail=[self lablePaidAmount];
        [lbTitle setFrame:CGRectMake(pointStart.x, pointStart.y, widthTitle, heighCell)];
        lbTitle.text =[NSString stringWithFormat:@"%@:",[(PaymentChild*)[array firstObject] displayName]];
        [_scrPaidAmount addSubview:lbTitle];
        pointStart.x +=(widthTitle+spaceX);
        [lbDetail setFrame:CGRectMake(pointStart.x, pointStart.y, maxWidthDetail, heighCell)];
        pointStart.y +=heighCell + spaceY;
        pointStart.x -=(widthTitle +spaceX);
        
        lbDetail.text=[TKAmountDisplay stringAmount:[PaymentBusinessModel totalChargeWithListPaymentChild:array] withCurrency:currency];
        [_scrPaidAmount addSubview:lbDetail];
    }
    [_scrPaidAmount setContentSize:CGSizeMake(_scrPaidAmount.frame.size.width, pointStart.y)];
    
}
-(UILabel*)lablePaidAmount{
    UILabel *lb=[[UILabel alloc] init];
    [lb setFont:tkFontMainText];
    lb.backgroundColor=[UIColor clearColor];
    lb.textAlignment=NSTextAlignmentRight;
    return lb;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickPayCash:(id)sender {
    if (_delegate) {
        [_delegate chargeLess:self chargeWithType:PaymenterTypeCash];
    }
}

- (IBAction)clickPayExternal:(id)sender {
    if (_delegate) {
        [_delegate chargeLess:self chargeWithType:PaymenterTypeExternal];
    }
}
@end

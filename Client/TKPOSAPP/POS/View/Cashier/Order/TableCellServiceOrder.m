//
//  TableCellServiceOrder.m
//  POS
//
//  Created by Cong Nha Duong on 1/27/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableCellServiceOrder.h"
#import "Constant.h"

@implementation TableCellServiceOrder

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        _lbTitle.textColor=[UIColor whiteColor];
        _lbValue.textColor=[UIColor whiteColor];
    }else{
        _lbTitle.textColor=[UIColor blackColor];
        _lbValue.textColor=[UIColor blackColor];
    }
    // Configure the view for the selected state
}
-(void)resetValueDefault{
    UIView *backgroundView =[[UIView alloc] init];
    [backgroundView setBackgroundColor:tkColorMainSelected];
    [super setSelectedBackgroundView:backgroundView];
    self.backgroundColor=[UIColor whiteColor];
    _vLine.backgroundColor=tkColorFrameBorder;
    _lbValue.font=tkFontMainText;
}

@end

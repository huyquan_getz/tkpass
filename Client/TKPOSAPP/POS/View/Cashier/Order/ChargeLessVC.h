//
//  ChargeLessVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/1/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "Paymenter.h"
@protocol ChargeLessDelegate <NSObject>

@required
-(void)chargeLess:(id)sender chargeWithType:(PaymenterType)typeCharge;

@end

#import <UIKit/UIKit.h>

@interface ChargeLessVC : UIViewController{
    double totalAmount;
    NSArray *listPaymentChildren;
    NSString *currency;
}
-(instancetype)initWithTotalAmout:(double)totalAmount_ listPaymentChildren:(NSArray*)listPaymentChildren_ currency:(NSString *)currency_;
@property (weak, nonatomic) IBOutlet UIScrollView *scrPaidAmount;
@property (weak,nonatomic) id<ChargeLessDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageContinue;
@property (weak, nonatomic) IBOutlet UIButton *btPayCash;
@property (weak, nonatomic) IBOutlet UILabel *lbTitlePaidAmount;
@property (weak, nonatomic) IBOutlet UIButton *btPayExternal;
- (IBAction)clickPayCash:(id)sender;
- (IBAction)clickPayExternal:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleAmountDue;
@end

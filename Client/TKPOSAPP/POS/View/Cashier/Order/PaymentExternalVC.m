//
//  PaymentExternalVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/3/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define MaxLenghApprovedCode 15

#define MaxNumberCardType 7
#define MaxWidthTotalButton 900
#define MaxWidthButton 150
#define MinWidthButton 110
#define MaxWidthSpace 75
#define MinWidthSpace 20

#define HeightButton 50

#import "PaymentExternalVC.h"
#import "CashierBusinessModel.h"
#import "PaymentBusinessModel.h"

@interface PaymentExternalVC ()

@end

@implementation PaymentExternalVC{
    LanguageUtil *languageKey;
    NSMutableArray *listButtonCardType;
}

-(instancetype)initWithAmountPay:(double)amPay_ currency:(NSString *)currency_ listCreditTypes:(NSArray *)listCreditTypes_{
    if (self=[super init]) {
        amountPay=amPay_;
        currency=currency_;
        listCreditTypes=listCreditTypes_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_vLine setBackgroundColor:tkColorFrameBorder];
    _btPay.layer.cornerRadius=tkCornerRadiusButton;
    _btPay.backgroundColor=tkColorMainAccept;
    [_btPay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btPay setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    
    _tfCharged.layer.borderColor=tkColorFrameBorder.CGColor;
    _tfCharged.layer.borderWidth=1;
    _tfCharged.layer.cornerRadius=tkCornerRadiusButton;
    [_tfCharged setBackgroundColor:[UIColor clearColor]];
    
    _tfApprovedCode.layer.borderColor=tkColorFrameBorder.CGColor;
    _tfApprovedCode.layer.borderWidth=1;
    _tfApprovedCode.layer.cornerRadius=tkCornerRadiusButton;
    [_tfApprovedCode setBackgroundColor:[UIColor clearColor]];
    _lbMessageStatus.text=@"";
    [self addButtonCreditType];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey = [LanguageUtil sharedLanguageUtil];
    _tfApprovedCode.placeholder=[languageKey stringByKey:@"cashier.payment.holder-approved-code"];
    [_btPay setTitle:[languageKey stringByKey:@"cashier.payment.bt.external-pay"] forState:UIControlStateNormal];
    _tfCharged.text=[TKAmountDisplay stringAmount:amountPay withCurrency:currency];
}
-(void)addButtonCreditType{
    NSInteger numberCardAdd=listCreditTypes.count;
    if (numberCardAdd>7) {
        numberCardAdd=7;
    }
    NSInteger widthBt;
    NSInteger heightBt=HeightButton;
    NSInteger widthSp;
    NSInteger totalWidth;
    CGPoint pointStart;
    if (numberCardAdd * MaxWidthButton + (numberCardAdd-1)*MaxWidthSpace <= MaxWidthTotalButton) {
        // full
        widthBt=MaxWidthButton;
        widthSp=MaxWidthSpace;
        totalWidth =numberCardAdd*widthBt + (numberCardAdd-1)*widthSp;
    }else if(numberCardAdd * MaxWidthButton + (numberCardAdd-1)*MinWidthSpace <= MaxWidthTotalButton){
        //scale space
        widthBt=MaxWidthButton;
        totalWidth =MaxWidthTotalButton;
        widthSp=(totalWidth - numberCardAdd*widthBt)/(numberCardAdd-1);
    }else{
        widthSp=MinWidthSpace;
        totalWidth=MaxWidthTotalButton;
        widthBt=(totalWidth -(numberCardAdd-1)*widthSp)/numberCardAdd;
    }
    pointStart.x=(self.view.frame.size.width - totalWidth)/2;
    pointStart.y=50;
    listButtonCardType=[[NSMutableArray alloc] init];
    for (int i=0; i<numberCardAdd; i++) {
        UIButton *button=[self buttonCard];
        [button setTitle:listCreditTypes[i] forState:UIControlStateNormal];
        [button setFrame:CGRectMake(pointStart.x, pointStart.y, widthBt, heightBt)];
        button.tag=i;
        [button addTarget:self action:@selector(clickCardType:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        [listButtonCardType addObject:button];
        pointStart.x+= widthBt+widthSp;
    }
    
}
-(UIButton*)buttonCard{
    UIButton *bt=[[UIButton alloc] init];
    UIColor *colorMainBt=tkColorMainSelected;
    bt.clipsToBounds=YES;
    bt.layer.cornerRadius=tkCornerRadiusButton;
    bt.layer.borderWidth=1;
    bt.layer.borderColor=colorMainBt.CGColor;
    [bt setBackgroundColor:[UIColor clearColor]];
    [bt setTitleColor:colorMainBt forState:UIControlStateNormal];
    [bt setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [bt setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [bt setBackgroundImage:[UIImage imageWithColor:colorMainBt] forState:UIControlStateHighlighted];
    [bt setBackgroundImage:[UIImage imageWithColor:colorMainBt] forState:UIControlStateSelected];
    return bt;
}
-(void)clickCardType:(UIButton*)sender{
    sender.selected=YES;
    for (UIButton *button in listButtonCardType) {
        if (button != sender) {
            button.selected=NO;
        }
    }
}
- (IBAction)clickPay:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self.view endEditing:YES];
    _lbMessageStatus.text=@"";
    double chargeGet=[[_tfCharged.text substringFromIndex:currency.length] doubleValue];
    if (_delegate) {
        PaymentChild *paymentChild=[[PaymentChild alloc] initPaymentCreditWithCharged:chargeGet paymentType:[self cardTypeSelected] approvedCode:_tfApprovedCode.text];
        [_delegate paymenter:self pay:paymentChild];
    }
    
}
-(NSString*)cardTypeSelected{
    for (UIButton *bt in listButtonCardType) {
        if (bt.selected==YES) {
            return listCreditTypes[bt.tag];
        }
    }
    return nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma Textfield Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    if (textField == _tfCharged) {
        NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
        if (newString.length <currency.length+1) { // currency + space
            return NO;
        }
        if (![[newString substringFromIndex:currency.length+1] isTKAmountFormat]) {
            return NO;
        }
        if ([[newString substringFromIndex:currency.length+1] doubleValue] >amountPay) {
            return NO;
        }
        return YES;
    }else if(textField == _tfApprovedCode){
        NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
        if (newString.length>MaxLenghApprovedCode) {
            return NO;
        }
        if ([newString rangeOfString:@" "].location != NSNotFound) {
            return NO;
        }
        return YES;
    }
    
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect frame=self.view.superview.frame;
    frame.origin.y -= 200;
    [self.view.superview setFrame:frame];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    CGRect frame=self.view.superview.frame;
    frame.origin.y += 200;
    [self.view.superview setFrame:frame];
}
-(BOOL)validateInput{
    if ([self cardTypeSelected]==nil) {
        [self showMessageError:[languageKey stringByKey:@"cashier.payment.ms-please-choose-card"]];
        return NO;
    }
    if ([_tfApprovedCode isEmptyText]) {
        [self showMessageError:[languageKey stringByKey:@"cashier.payment.ms-please-input-approved-code"]];
        return NO;
    }
    return YES;
}
-(void)showMessageError:(NSString*)error{
    _lbMessageStatus.textColor=[UIColor redColor];
    _lbMessageStatus.text=error;
}
-(void)showMessageSuccess:(NSString*)message{
    _lbMessageStatus.textColor=[UIColor blueColor];
    _lbMessageStatus.text=message;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

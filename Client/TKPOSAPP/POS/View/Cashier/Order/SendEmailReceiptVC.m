//
//  SendEmailReceiptVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/1/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "SendEmailReceiptVC.h"
#import "CashierBusinessModel.h"
#import "WebserviceJSON.h"
#import "QueueSendMail.h"
#import "Controller.h"


@interface SendEmailReceiptVC ()

@end

@implementation SendEmailReceiptVC{
    LanguageUtil *languageKey;
}
-(instancetype)initWithLastCharge:(double)lastGetCharge_ outChange:(double)outChange_ currency:(NSString *)currency_ order:(CBLDocument *)orderReceipt_{
    if (self=[super init]) {
        lastGetCharge=lastGetCharge_;
        outChange=outChange_;
        currency=currency_;
        orderReceipt=orderReceipt_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _lbMessageStatus.text=@"";
    [_vMainSendMail setBackgroundColor:[UIColor clearColor]];
    _vMainSendMail.layer.cornerRadius=tkCornerRadiusViewPopup;
    _vMainSendMail.layer.borderWidth=1;
    _vMainSendMail.layer.borderColor=tkColorFrameBorder.CGColor;
    [_vMain setShadowWithCornerRadius:tkCornerRadiusViewPopup];
    _btSend.layer.cornerRadius=tkCornerRadiusButton;
    _btSend.backgroundColor=tkColorMainSelected;
    
    _btClose.layer.cornerRadius=tkCornerRadiusButton;
    _btClose.backgroundColor=tkColorMainAccept;
    [_lbMessageStatus setTextColor:[UIColor blueColor]];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [_btSend setTitle:[languageKey stringByKey:@"general.bt.send"] forState:UIControlStateNormal];
    [_btClose setTitle:[languageKey stringByKey:@"cashier.order.send-email.payment-finish"] forState:UIControlStateNormal];
    _tfEmail.placeholder=[languageKey stringByKey:@"cashier.order.send-email.holder-email"];
    
    if(outChange==0){
        _lbChange.text=[languageKey stringByKey:@"cashier.order.send-email.no-change"];
        _lbChange.textAlignment=NSTextAlignmentCenter;
        CGPoint center=_lbChange.center;
        center.x=self.vMain.frame.size.width/2;
        _lbChange.center=center;
        _lbLastCharge.hidden=YES;
    }else{
        _lbChange.text=[NSString stringWithFormat:@"%@ %@",[TKAmountDisplay stringAmount:outChange withCurrency:currency],[languageKey stringByKey:@"cashier.order.send-email.text-change"]];
        _lbLastCharge.text=[NSString stringWithFormat:@"%@ %@",[languageKey stringByKey:@"cashier.order.send-email.text-out-of"],[TKAmountDisplay stringAmount:lastGetCharge withCurrency:currency]];
        [self resetFrameChangeOutCharge];
    }
}
-(void)resetFrameChangeOutCharge{
    CGFloat widthChange=[_lbChange.text sizeWithAttributes:@{NSFontAttributeName:_lbChange.font}].width;
    CGFloat widthCharge=[_lbLastCharge.text sizeWithAttributes:@{NSFontAttributeName:_lbLastCharge.font}].width;
    CGFloat maxWidth=self.vMain.frame.size.width - 30;
    if (widthChange > maxWidth - 30) {
        widthChange=maxWidth;
        widthCharge=0;
        _lbLastCharge.hidden=YES;
    }else if(widthCharge > (maxWidth - widthChange)){
        widthCharge =maxWidth-widthChange;
    }
    CGFloat totalWidth =widthChange+widthCharge+10;
    CGRect frame=_lbChange.frame;
    frame.size.width=widthChange;
    frame.origin.x=(self.vMain.frame.size.width -totalWidth)/2;
    [_lbChange setFrame:frame];
    
    frame=_lbLastCharge.frame;
    frame.size.width=widthCharge;
    frame.origin.x=(self.vMain.frame.size.width -totalWidth)/2 + widthChange + 10;
    [_lbLastCharge setFrame:frame];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma TextfieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self clickClose:nil];
    return YES;
}

- (IBAction)clickClose:(id)sender {// finish
    if (!_delegate) {
        return;
    }
    if (![self isEmailEmpty] && ![self validateInput]) {
        return;
    }
    //finish order
    [self finishOrder];
    if ([self isEmailEmpty]) {
        [self finish];
        return;
    }
    [self hideKeyboard];
    [self clearStatus];
    if (![NetworkUtil checkInternetConnection]) {
        [[NetworkUtil sharedInternetConnection]  addDelegate:[QueueSendMail sharedQueue]];
        [[QueueSendMail sharedQueue] addToQueueReceipt:orderReceipt.properties toEmail:_tfEmail.text type:MailTypeCollected];
        UIAlertView  *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-message"] message:[languageKey stringByKey:@"cashier.order.send-email.send-email-later"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.ok"] otherButtonTitles: nil];
        [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [self finish];
        }];
        return;
    }
    MBProgressHUD *hub=[[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hub];
    hub.labelText=[languageKey stringByKey:@"cashier.order.send-email.text-sending"];
    [hub show:YES];
    dispatch_queue_t myQueue=dispatch_queue_create("queue.SendEmail", NULL);
    dispatch_async(myQueue, ^{
        BOOL sendSuccess=[self sendEmailReceipt];
        dispatch_async(dispatch_get_main_queue(), ^{
            [hub hide:YES];
            [hub removeFromSuperview];
            NSString *message;
            if (sendSuccess) {
                //show alert and done payment
                message=[languageKey stringByKey:@"cashier.order.send-email.ms-send-receipt-success"];
            }else{
                message =[languageKey stringByKey:@"cashier.order.send-email.ms-send-receipt-fail"];
            }
            UIAlertView  *alert=[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-message"] message:message delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.ok"] otherButtonTitles: nil];
            [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [self finish];
            }];
        });
    });
}
-(BOOL)sendEmailReceipt{
    return [Controller sendReceipt:orderReceipt.properties toEmail:_tfEmail.text type:MailTypeCollected];
}
- (IBAction)clickSend:(id)sender {
}
-(void)finish{
    if (_delegate) {
        [_delegate sendEmailFinishPayment:self];
    }
}
-(void)finishOrder{
    [CashierBusinessModel finishOrder:orderReceipt];
}
-(BOOL)validateInput{
    if ([_tfEmail isEmptyText]) {
        [self showMessageError:[languageKey stringByKey:@"cashier.order.send-email.email-empty"]];
        return NO;
    }
    if (![_tfEmail.text isEmailFormat]) {
        [self showMessageError:[languageKey stringByKey:@"cashier.order.send-email.email-invalid"]];
        return NO;
    }
    return YES;
}
-(BOOL)isEmailEmpty{
    return [_tfEmail isEmptyText];
}
-(void)hideKeyboard{
    [self.view endEditing:YES];
}
-(void)clearStatus{
    _lbMessageStatus.text=@"";
}
-(void)showMessageError:(NSString*)error{
    _lbMessageStatus.textColor=[UIColor redColor];
    _lbMessageStatus.text=error;
}
-(void)showMessageSuccess:(NSString*)message{
    _lbMessageStatus.textColor=[UIColor blueColor];
    _lbMessageStatus.text=message;
}
- (IBAction)clickCancel:(id)sender {
    UIAlertView *alert =[[UIAlertView alloc] initWithTitle:[languageKey stringByKey:@"general.text-confirm"] message:[languageKey stringByKey:@"cashier.order.send-email.confirm-cancel-payment"] delegate:nil cancelButtonTitle:[languageKey stringByKey:@"general.bt.no"] otherButtonTitles:[languageKey stringByKey:@"general.bt.yes"], nil];
    [alert showWithHandler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex==1) {
            if (_delegate) {
                [_delegate sendEmailCancelPayment:self];
            }
        }
    }];
}
@end

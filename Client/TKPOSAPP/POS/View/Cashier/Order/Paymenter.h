//
//  Paymenter.h
//  POS
//
//  Created by Nha Duong Cong on 11/28/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@class  PaymentChild;
@protocol PaymenterDelegate <NSObject>

-(void)paymenter:(id)sender pay:(PaymentChild*)paymentChild;

@end

@protocol PaymenterDataSource <NSObject>
@end

typedef enum _PaymenterType{
    PaymenterTypeCash,
    PaymenterTypeExternal,
    PaymentTypeUnknow
}PaymenterType;

#define StringPaymenterTypeCash @"Cash"

#import <Foundation/Foundation.h>

@interface Paymenter : NSObject
+(NSString*)stringByPaymenterType:(PaymenterType)type;
+(PaymenterType)paymenterTypeByString:(NSString*)stringType;
@end

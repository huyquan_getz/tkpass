//
//  PaymentExternalVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/3/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Paymenter.h"

@interface PaymentExternalVC : UIViewController<PaymenterDataSource,UITextFieldDelegate>{
    double amountPay;
    NSString *currency;
    NSArray *listCreditTypes;
}
@property (weak,nonatomic) id<PaymenterDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *vLine;
@property (weak, nonatomic) IBOutlet UITextField *tfCharged;
@property (weak, nonatomic) IBOutlet UITextField *tfApprovedCode;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
@property (weak, nonatomic) IBOutlet UIButton *btPay;
- (IBAction)clickPay:(id)sender;
-(instancetype)initWithAmountPay:(double)amPay_ currency:(NSString*)currency_ listCreditTypes:(NSArray*)listCreditTypes_;
@end

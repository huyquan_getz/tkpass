//
//  PaymentVC.h
//  POS
//
//  Created by Nha Duong Cong on 11/28/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol PaymentDelegate <NSObject>
@required
-(void)paymentCancel:(id)sender;
-(void)payment:(id)sender payment:(NSArray*)paymenterCharges completed:(BOOL)completed;

@end
#import <UIKit/UIKit.h>
#import "Paymenter.h"
#import "ChargeLessVC.h"
#import "CashierBusinessModel.h"
#import "PaymentBusinessModel.h"

@interface PaymentVC : UIViewController<PaymenterDelegate,ChargeLessDelegate>{
    NSString *currency;
    double totalCharge;
    NSMutableArray *listPaymentChild;
    PaymenterType currentPaymenterType;
}
@property (readonly) double totalCharge;
-(instancetype)initWithTotalCharge:(double)totalCharge currency:(NSString*)currency_;

@property (weak,nonatomic) id<PaymentDelegate> delegate;
@property (strong,nonatomic) ChargeLessVC *chargeLess;
@property (strong,nonatomic) UIViewController<PaymenterDataSource> *paymenter;
@property (weak, nonatomic) IBOutlet UIView *vCover;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
- (IBAction)clickCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTitlePayment;
@property (weak, nonatomic) IBOutlet UIButton *btPayCash;
- (IBAction)clickPayCash:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vLine1;
@property (weak, nonatomic) IBOutlet UIView *vLine2;
@property (weak, nonatomic) IBOutlet UIButton *btPayExternal;
@property (weak, nonatomic) IBOutlet UIView *vFramePay;
- (IBAction)clickPayExternal:(id)sender;
-(NSArray*)listPaymentChild;
@end

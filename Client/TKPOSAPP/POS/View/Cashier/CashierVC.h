//
//  CashierVC.h
//  POS
//
//  Created by Nha Duong Cong on 10/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
typedef enum _ShowingRight{
    ShowingRightNone,
    ShowingRightOrderOpen,
    ShowingRightOrderOrded, // order payment from web
}ShowingRight;

typedef enum _ShowingMain{
    ShowingMainProduct,
    ShowingMainOrder
}ShowingMain;

typedef enum _TypeProductShower{
    ProductShowerGrid,
    ProductShowerList
}TypeProductShower;

#import <UIKit/UIKit.h>
#import "SearchCategory.h"
#import "PageProductVC.h"
#import "ProductShower.h"
#import "ProductShowerDelegate.h"
#import "ProductDetailVC.h"
#import "OrderSelfCollectVC.h"
#import "PaymentVC.h"
#import "SendEmailReceiptVC.h"
#import "PageOrderWaitingVC.h"
#import "OrderVC.h"
#import "AdHocProductVC.h"
#import "DiscountVC.h"

@interface CashierVC : UIViewController<SearchCategoryDelegate,ProductShowerDelegate,UISearchBarDelegate,UIPopoverControllerDelegate,ProductDetailDelegate,OrderDelegate,PaymentDelegate,SendEmailDelegate,PageOrderWaitingDelegate,OrderSelfCollectDelegate,AdHocProductDelegate,DiscountDelegate>{
    CBLDocument *categorySelected;
    CBLDocument *productSelected;
    BOOL searching;
    NSString *oldSearchKey;//key have just search
    CBLLiveQuery *liveQuery;
    NSMutableArray *listProducts;
    NSArray *listTagButton;
    CBLDocument *currentOrderDocumentPaymented;
    ShowingMain showingMain;
    ShowingRight showingRight;
}
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (weak, nonatomic) IBOutlet UILabel *lbRequireNewOrder;
@property (weak, nonatomic) IBOutlet UIView *vFrameOrderList;
@property (weak, nonatomic) IBOutlet UIButton *btNewOrder;
- (IBAction)clickNewOrder:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btChangeTypeShowProduct;
- (IBAction)clickChangeTypeShowProduct:(id)sender;
- (IBAction)clickCancelSearch:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btSearch;
- (IBAction)clickSearch:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btCancelSearchCustom;
@property (weak, nonatomic) IBOutlet UIView *vBehideSearchBar;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *btTagProduct;
@property (weak, nonatomic) IBOutlet UIButton *btTagOrders;
@property (weak, nonatomic) IBOutlet UIImageView *ivNaviBar;
@property (weak, nonatomic) IBOutlet UIButton *btAddDiscount;
- (IBAction)clickAddDiscount:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *vFrameShowProduct;

@property (assign) TypeProductShower typeProductShower;
@property (strong,nonatomic) UIViewController<ProductShower> *productsShower;
@property (strong,nonatomic) PageOrderWaitingVC *pageOrderWaitingVC;
@property (strong,nonatomic) OrderVC *orderVC;
@property (strong,nonatomic) OrderSelfCollectVC *orderSelfCollectVC;
@property (strong,nonatomic)PaymentVC *paymentVC;
@property (strong,nonatomic)SendEmailReceiptVC *sendEmailRecieptVC;
@property (strong,nonatomic)ProductDetailVC *productDetailVC;
@property (strong,nonatomic)AdHocProductVC *adhocProductVC;
@property (strong,nonatomic)DiscountVC *discountCartVC;
- (IBAction)clickMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btMenu;
- (IBAction)clickProduct:(id)sender;
- (IBAction)clickOrders:(id)sender;
@property (strong, nonatomic)UIPopoverController *popover;
@end

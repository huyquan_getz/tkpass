//
//  CollectionCellOrder.h
//  POS
//
//  Created by Nha Duong Cong on 12/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKOrder.h"

@interface CollectionCellOrder : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbTotalCharge;
@property (weak, nonatomic) IBOutlet UILabel *lbCustomerName;
-(void)setValueDefault;
-(void)setCustomer:(NSString *)customer orderStatus:(OrderStatus)orderStatus cancelRequire:(BOOL)cancelRequire;
@end

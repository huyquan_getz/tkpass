//
//  ContentPageOrderVC.h
//  POS
//
//  Created by Nha Duong Cong on 12/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "Constant.h"
@protocol ContentPageOrderDelegate <NSObject>
@required
-(void)contentPageOrder:(id)sender clickDetailOrder:(CBLDocument*)orderOpen;
-(CBLDocument*)contentPageOrderRequireCurrentOrder:(id)sender;

@end

#import <UIKit/UIKit.h>

@interface ContentPageOrderVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    NSMutableArray *listOrders;
    CGSize tableSize;
    CGSize cellSize;
}
@property (weak, nonatomic) id<ContentPageOrderDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbNoOrder;
@property (weak, nonatomic) IBOutlet UICollectionView *clvMain;
-(instancetype)initWithOrders:(NSArray*)listNewOrders tableSize:(CGSize)sizeTable;
@end

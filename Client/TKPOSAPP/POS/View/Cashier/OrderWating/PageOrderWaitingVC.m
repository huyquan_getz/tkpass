//
//  PageOrderWatingVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define SpaceX 20
#define SpaceY 30
#define MaxOrderQuery 1000
#define HeightPageControl 30
#import "PageOrderWaitingVC.h"
#import "Controller.h"

@interface PageOrderWaitingVC ()

@end

@implementation PageOrderWaitingVC
@synthesize pageControl;
@synthesize pageVC;
-(instancetype)init{
    if ( self =[super init]) {
        totalPage=0;
        numberItemInPage=25;
        currentPageIndex=0;
        listOrderWaiting =[[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    pageControl.currentPage=-1;
    pageControl.numberOfPages=1;
    [pageControl addTarget:self action:@selector(pageControlChangeValue:) forControlEvents:UIControlEventValueChanged];
    [pageControl setPageIndicatorImage:[UIImage imageNamed:@"pageDot"]];
    [pageControl setCurrentPageIndicatorImage:[UIImage imageNamed:@"currentPageDot"]];
    CGRect frame=self.view.frame;
    frame.size.height=HeightPageControl;
    frame.origin.y=self.view.frame.size.height - HeightPageControl;
    [pageControl setFrame:frame];
    
    pageVC =[[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:[NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:50.0f] forKey:UIPageViewControllerOptionInterPageSpacingKey]];
    pageVC.dataSource=self;
    pageVC.delegate=self;
    frame=self.view.frame;
    frame.size.height-= (HeightPageControl + SpaceY);
    frame.size.width-=(2*SpaceX);
    frame.origin.x=SpaceX;
    frame.origin.y=SpaceY;
    [pageVC.view setFrame:frame];
    pageVC.view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self reloadPageWithPageIndex:currentPageIndex];
    [self addChildViewController:pageVC];
    [self.view addSubview:pageVC.view];
    [pageVC didMoveToParentViewController:self];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated{
    [self reSetupQueryLiveWithQuery:[Controller queryGetAllOrderWaiting]];
}
-(void)reSetupQueryLiveWithQuery:(CBLQuery*)query{
    if (query==nil) {
        return;
    }
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery =[query asLiveQuery];
    [liveQuery addObserver:self forKeyPath:@"rows" options:0 context:NULL];
    liveQuery.limit=MaxOrderQuery;
    liveQuery.descending=YES;
    [liveQuery start];
}
-(void)removeQueryLive{
    if (liveQuery) {
        [liveQuery stop];
        [liveQuery removeObserver:self forKeyPath:@"rows"];
    }
    liveQuery=nil;
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (object == liveQuery) {
        [self displayRows:liveQuery.rows resetPostion:NO];
    }
}
-(void)displayRows:(CBLQueryEnumerator*)enumRows resetPostion:(BOOL)resetPostion{
    NSMutableArray *orders=[[NSMutableArray alloc] init];
    for (CBLQueryRow *row in enumRows) {
        [orders addObject:row.document];
    }
    [self reloadWithList:orders showPageIndex:currentPageIndex];
}
-(void)reloadData{
    [self reloadPageWithPageIndex:currentPageIndex];
}
-(void)reloadInPageWithList:(NSArray *)listOrdersNew{
    //reload Order with list Order and hold pageCurrent;
    [self reloadWithList:listOrdersNew showPageIndex:currentPageIndex];
}
-(void)reloadWithList:(NSArray*)listOrdersNew showPageIndex:(NSInteger)pageIndex{
    // reload Order with listOrder new and move to page with index
    if (listOrdersNew != listOrderWaiting) {
        [listOrderWaiting removeAllObjects];
        [listOrderWaiting addObjectsFromArray:listOrdersNew];
    }
    [self reloadPageWithPageIndex:pageIndex];
    // check order slected
    CBLDocument *currentOrderSeleted =[_delegate pageOrderWaitingRequireCurrentOrder:self];
    if (currentOrderSeleted) {
        NSInteger indexExisted=[listOrderWaiting indexOfObject:currentOrderSeleted];
        if (indexExisted !=NSNotFound) {
            // reload indexExisted if
            [_delegate pageOrderWaiting:self reloadCurrentOrder:currentOrderSeleted];
        }else{
            //
            [_delegate pageOrderWaiting:self clickDetailOrder:nil];
        }
    }
}
-(void)reloadPageWithPageIndex:(NSInteger)pageIndex{
    // reload Order and move to page with index
    totalPage=[self totalPageWithNumberOrders:listOrderWaiting.count];
    currentPageIndex = pageIndex;
    if (pageIndex >= totalPage) {
        currentPageIndex = totalPage-1;
    }
    if (pageIndex<0) {
        currentPageIndex=0;
    }
    [self resetPageControl];
    UIViewController *contentVC=[self contentVCWithIndex:currentPageIndex];
    [pageVC setViewControllers:@[contentVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}
-(void)moveToPage:(NSInteger)pageIndex animation:(BOOL)animation{
    //move page to new page index
    if (pageIndex <0 || pageIndex >= totalPage || pageIndex == currentPageIndex) {
        return;
    }else{
        UIPageViewControllerNavigationDirection direction;
        if (pageIndex > currentPageIndex) {
            direction =UIPageViewControllerNavigationDirectionForward;
        }else{
            direction =UIPageViewControllerNavigationDirectionReverse;
        }
        currentPageIndex=pageIndex;
        UIViewController *contentVC=[self contentVCWithIndex:currentPageIndex];
        [pageVC setViewControllers:@[contentVC] direction:direction animated:animation completion:nil];
    }
}
-(NSInteger)totalPageWithNumberOrders:(NSInteger)numberOfOrder{
    NSInteger nbPageOrder =(NSInteger)ceil(numberOfOrder/(float)numberItemInPage);
    if (nbPageOrder==0) {// have 1 OrderVC show Empty document
        nbPageOrder+=1;
    }
    return nbPageOrder;
}
-(UIViewController*)contentVCWithIndex:(NSInteger)contentIndex{
    //create contentVC with index.  Assign contentIndex to tag of View of ViewController
    ContentPageOrderVC *viewVC=[[ContentPageOrderVC alloc] initWithOrders:[self listOrdersWithPageIndex:contentIndex] tableSize:CGSizeMake(5, 5)];
    [viewVC.view setFrame:pageVC.view.frame];
    viewVC.view.tag=contentIndex;
    viewVC.delegate=self;
    return viewVC;
}
-(NSInteger)indexOfContentVC:(UIViewController*)contentVC{
    return contentVC.view.tag;
}
#pragma PageViewControllerDelegate
-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    currentPageIndex =[self indexOfContentVC:[pageViewController.viewControllers firstObject]];
    [self resetPageControl];
}
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    NSInteger viewIndex=[self indexOfContentVC:viewController];
    viewIndex ++;
    if (viewIndex >= totalPage) {
        return nil;
    }else{
        return [self contentVCWithIndex:viewIndex];
    }
}
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    NSInteger viewIndex=[self indexOfContentVC:viewController];
    viewIndex--;
    if (viewIndex<0) {
        return nil;
    }else{
        return [self contentVCWithIndex:viewIndex];
    }
}
-(NSArray*)listOrdersWithPageIndex:(NSInteger)pageIndex{
    if (pageIndex<0 || pageIndex >=totalPage) {
        return nil;
    }else{
        return [listOrderWaiting subarrayWithRange:NSMakeRange(pageIndex*numberItemInPage, [self numberItemInPageWithPageIndex:pageIndex])];
    }
}
-(NSInteger)numberItemInPageWithPageIndex:(NSInteger)pageIndex{
    if (pageIndex<0 || pageIndex>totalPage) {
        return 0;
    }else if(pageIndex<totalPage-1){
        return numberItemInPage;
    }else{
        return listOrderWaiting.count - pageIndex*numberItemInPage;
    }
}
-(void)pageControlChangeValue:(id)sender{
    NSInteger curPage=pageControl.currentPage;
    [self moveToPage:curPage animation:YES];
}

// PageControl
-(void)resetPageControl{
    pageControl.numberOfPages=totalPage;
    pageControl.currentPage=currentPageIndex;
    [pageControl setNeedsDisplay];
}

#pragma ContentPageOrder
-(void)contentPageOrder:(id)sender clickDetailOrder:(CBLDocument *)orderOpen{
    if (_delegate) {
        [_delegate pageOrderWaiting:self clickDetailOrder:orderOpen];
    }
    
}
-(CBLDocument *)contentPageOrderRequireCurrentOrder:(id)sender{
    if (_delegate ) {
        return [_delegate pageOrderWaitingRequireCurrentOrder:self];
    }
    return nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [liveQuery removeObserver:self forKeyPath:@"rows"];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

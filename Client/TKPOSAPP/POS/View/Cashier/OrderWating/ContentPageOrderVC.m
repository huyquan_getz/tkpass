//
//  ContentPageOrderVC.m
//  POS
//
//  Created by Nha Duong Cong on 12/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define SpaceCellX 20
#define SpaceCellY 20
#define CellOrderIdentifier @"cellOrder"
#import "ContentPageOrderVC.h"
#import "CashierBusinessModel.h"
#import "CollectionCellOrder.h"
#import "Controller.h"

@interface ContentPageOrderVC ()

@end

@implementation ContentPageOrderVC

-(instancetype)initWithOrders:(NSArray *)listNewOrders tableSize:(CGSize)sizeTable_{
    if (self=[super init] ) {
        listOrders =[[NSMutableArray alloc] initWithArray:listNewOrders];
        tableSize=sizeTable_;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self resetCellSize];
    _clvMain.scrollEnabled=NO;
    UINib *nib=[UINib nibWithNibName:@"CollectionCellOrder" bundle:nil];
    [_clvMain registerNib:nib forCellWithReuseIdentifier:CellOrderIdentifier];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    _lbNoOrder.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"cashier.list-order.ms-no-order"];
    [self resetCellSize];
    [_clvMain reloadData];
    [self resetSelectedItem];
}
-(void)resetCellSize{
    NSInteger cX=(NSInteger)(self.view.frame.size.width - (tableSize.width - 1) * SpaceCellX)/tableSize.width;
    NSInteger cY=(NSInteger)(self.view.frame.size.height - (tableSize.height - 1) * SpaceCellY)/tableSize.height;
    cellSize=CGSizeMake(cX, cY);
}
-(void)resetSelectedItem{
    // reset Selected Cell
    CBLDocument *currentOrderSeleted =[_delegate contentPageOrderRequireCurrentOrder:self];
    NSIndexPath *indexPathSelected;
    if (currentOrderSeleted) {
        NSInteger indexExisted=[listOrders indexOfObject:currentOrderSeleted];
        if (indexExisted !=NSNotFound) {
            indexPathSelected=[NSIndexPath indexPathForRow:indexExisted inSection:0];
        }
    }
    
    //selected item
    if (indexPathSelected) {
        [_clvMain selectItemAtIndexPath:indexPathSelected animated:YES scrollPosition:UICollectionViewScrollPositionTop];
    }

    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    _lbNoOrder.hidden=(listOrders.count!=0);
    return listOrders.count;
}
-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CollectionCellOrder *cell=[collectionView dequeueReusableCellWithReuseIdentifier:CellOrderIdentifier forIndexPath:indexPath];
    CBLDocument *doc=listOrders[indexPath.row];
    [cell setValueDefault];
    cell.lbTitle.text=[TKOrder orderCode:doc];
    NSString *currency=[TKOrder currency:doc];
    if (currency==nil) {
        currency=[Controller currencyDefault];
    }
    cell.lbTotalCharge.text=[TKAmountDisplay stringAmount:[TKOrder grandTotal:doc] withCurrency:currency];
    OrderStatus status=[TKOrder orderStatus:doc];
    [cell setCustomer:[TKOrder customerReceiverName:doc] orderStatus:status cancelRequire:[TKOrder isCancelRequest:doc]];
    return cell;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return cellSize;
}
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return SpaceCellX;
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return SpaceCellY;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CBLDocument *doc=listOrders[indexPath.row];
    if (_delegate) {
        [_delegate contentPageOrder:self clickDetailOrder:doc];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

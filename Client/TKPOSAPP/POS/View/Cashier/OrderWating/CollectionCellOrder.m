//
//  CollectionCellOrder.m
//  POS
//
//  Created by Nha Duong Cong on 12/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "CollectionCellOrder.h"
#import "Constant.h"

@implementation CollectionCellOrder

- (void)awakeFromNib {
    // Initialization code
}
-(void)setValueDefault{
    self.clipsToBounds=YES;
    self.layer.borderWidth=1;
    self.layer.borderColor=tkColorMainSelected.CGColor;
    self.layer.cornerRadius=tkCornerRadiusButton;
    _lbCustomerName.hidden=YES;
}
-(void)setCustomer:(NSString *)customer orderStatus:(OrderStatus)orderStatus cancelRequire:(BOOL)cancelRequire{
    _lbCustomerName.text=customer;
    _lbCustomerName.hidden=NO;
    if (cancelRequire) {
        [_lbCustomerName setBackgroundColor:[UIColor redColor]];
        self.layer.borderColor=[UIColor redColor].CGColor;
    }else{
        switch (orderStatus) {
            case OrderStatusOrdered:
                [_lbCustomerName setBackgroundColor:tkColorPending];
                self.layer.borderColor=tkColorPending.CGColor;
                break;
            case OrderStatusReadyCollection:
                [_lbCustomerName setBackgroundColor:tkColorReady];
                self.layer.borderColor=tkColorReady.CGColor;
                break;
            default:
                _lbCustomerName.hidden=YES;
                break;
        }
    }
}
-(void)setSelected:(BOOL)selected{
    if (selected) {
        _lbTitle.textColor=[UIColor whiteColor];
        _lbTotalCharge.textColor=[UIColor whiteColor];
        self.backgroundColor=tkColorMainSelected;
    }else{
        _lbTitle.textColor=[UIColor darkGrayColor];
        _lbTotalCharge.textColor=[UIColor darkGrayColor];
        self.backgroundColor=[UIColor whiteColor];
    }
}
@end

//
//  ListPageVC.m
//  POS
//
//  Created by Nha Duong Cong on 11/14/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define HeightMax 440
#define HeightCell 44
#define Width 132
#import "ListPageVC.h"

@interface ListPageVC ()

@end

@implementation ListPageVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithTotalPage:(NSInteger)totalP currentPage:(NSInteger)currentP{
    if (self =[super init]) {
        totalPage=totalP;
        currentPage=currentP;
        CGRect frame= CGRectZero;
        frame.size =[self sizeWithTotalPage:totalPage];
        self.view.frame=frame;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated{
    [_tbvMain scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:currentPage inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}
-(CGSize)sizeWithTotalPage:(NSInteger)total{
    NSInteger totalHeightContent = total * HeightCell;
    if (totalHeightContent > HeightMax) {
        totalHeightContent=HeightMax;
    }
    NSInteger widthCharacter = 8;
    NSInteger padding = 20;
    NSInteger nbTotal=(NSInteger)floor(log10(total)) + 1;
    NSInteger widthContent=2*padding + widthCharacter*(nbTotal * 2 + 3);
    return CGSizeMake(widthContent, totalHeightContent);
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return HeightCell;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* identify=@"cellListPage";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identify];
    if (cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify];
    }
    cell.textLabel.text=[NSString stringWithFormat:@"%li / %li",(long)indexPath.row+1,(long)totalPage];
    [cell.textLabel setTextAlignment:NSTextAlignmentRight];
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return totalPage;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_delegate) {
        [_delegate listPage:self selected:indexPath.row];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

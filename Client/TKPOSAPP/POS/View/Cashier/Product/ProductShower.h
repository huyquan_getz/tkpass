//
//  ProductShower.h
//  POS
//
//  Created by Nha Duong Cong on 11/11/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductShowerDelegate.h"

@protocol ProductShower <NSObject>
-(void)showListProducts:(NSArray*)products resetPosition:(BOOL)resetPosition;
-(NSInteger)currentIndexItemShow;
-(void)resetCurrentIndexItemShow;
-(void)setProductShowerDelegate:(id<ProductShowerDelegate>)delegate_;
-(void)reloadItemWithProductSelected:(CBLDocument*)documentSelected;
@end

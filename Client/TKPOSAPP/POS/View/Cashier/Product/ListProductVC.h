//
//  ListProductVC.h
//  POS
//
//  Created by Nha Duong Cong on 11/11/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductShower.h"
#import "ProductShowerDelegate.h"

@interface ListProductVC : UIViewController<UITableViewDataSource,UITableViewDelegate,ProductShower>{
    NSMutableArray *listProducts;
    NSInteger indexCellTop;
}
@property (weak, nonatomic) IBOutlet UILabel *lbNoneItem;
@property (weak) id<ProductShowerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tbMain;
@property (weak, nonatomic) IBOutlet UIImageView *ivAdhoc;
@property (weak, nonatomic) IBOutlet UILabel *lbAdhocTitle;
@property (strong, nonatomic) IBOutlet UIView *vAdhoc;
@property (weak, nonatomic) IBOutlet UIView *vLineAdhoc;
-(void)reloadListWithProducts:(NSArray*)products resetPosition:(BOOL)resetPosition;
-(id)initWithListProducts:(NSArray*)listPro;
-(id)initWithListProducts:(NSArray *)listPro indexItemShow:(NSInteger)indexItemShow;
@end

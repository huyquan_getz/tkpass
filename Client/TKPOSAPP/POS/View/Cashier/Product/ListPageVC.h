//
//  ListPageVC.h
//  POS
//
//  Created by Nha Duong Cong on 11/14/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@protocol ListPageDelegate <NSObject>
-(void)listPage:(id)sender selected:(NSInteger)indexSelect;
@end
#import <UIKit/UIKit.h>

@interface ListPageVC : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSInteger totalPage;
    NSInteger currentPage;
}
@property (weak,nonatomic) id<ListPageDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
-(id)initWithTotalPage:(NSInteger)totalP currentPage:(NSInteger)currentP;
@end

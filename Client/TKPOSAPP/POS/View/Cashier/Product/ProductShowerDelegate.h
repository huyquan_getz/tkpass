//
//  ProductShowerDelegate.h
//  POS
//
//  Created by Nha Duong Cong on 11/11/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "Constant.h"
#import <Foundation/Foundation.h>

@protocol ProductShowerDelegate <NSObject>
-(void)productShower:(id)sender showProductDetail:(CBLDocument*)product;
-(void)productShower:(id)sender showAdhocProduct:(BOOL)none;
-(CBLDocument*)productShower:(id)sender requireProductSelected:(BOOL)none;
@end

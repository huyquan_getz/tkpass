//
//  ProductDetailVC.h
//  POS
//
//  Created by Nha Duong Cong on 11/17/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "Constant.h"
#import "ProductOrderSingle.h"
@protocol ProductDetailDelegate <NSObject>
-(void)productDetail:(id)sender editProductOrder:(ProductOrderSingle *)oldProductOrder withProductOrder:(ProductOrderSingle *)newProductOrder productOriginal:(CBLDocument*)product;
-(void)productDetailCancel:(id)sender;
@end

#import <UIKit/UIKit.h>

@interface ProductDetailVC : UIViewController<UITextFieldDelegate>{
    CBLDocument *product;
    NSDictionary *variantSelected;
    NSMutableArray *variantOptionSelected;
    ProductOrderSingle *oldProductOrder;
}
-(instancetype)initWithProduct:(CBLDocument*)product onlyShow:(BOOL)onlyShow;
-(instancetype)initWithProductOrder:(ProductOrderSingle *)productOrder fromOrder:(BOOL)fromOrder;
@property (weak, nonatomic) IBOutlet UIView *vCover;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageError;
@property (weak, nonatomic) IBOutlet UIButton *btRemoveOrderList;
@property (weak, nonatomic) IBOutlet UILabel *lbAmountCrude;

@property (weak, nonatomic) IBOutlet UIView *vInfoProduct;
@property (weak, nonatomic) IBOutlet UILabel *lbAvailableQuantity;
@property (weak) id<ProductDetailDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollMain;
- (IBAction)clickCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btAdd;
- (IBAction)clickAdd:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lbAmount;
@property (weak, nonatomic) IBOutlet UITextField *tfQuanity;
@property (weak, nonatomic) IBOutlet UIButton *btIncrease;
- (IBAction)clickIncrease:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btDecrease;
- (IBAction)clickDecrease:(id)sender;
- (IBAction)clickRemoveFromOrderList:(id)sender;
@end

//
//  ContentPageProductVC.m
//  POS
//
//  Created by Nha Duong Cong on 10/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define SpaceCellX 20
#define SpaceCellY 20
#define CellProductIdentifier @"cellProduct"
#import "ContentPageProductVC.h"
#import "CollectionCellProduct.h"
#import "DLImageLoader.h"
#import "TKProduct.h"
#import "TKCategory.h"
#import "TKProductItem.h"

@interface ContentPageProductVC ()

@end

@implementation ContentPageProductVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithProducts:(NSArray *)products tableSize:(CGSize)tableS_{
    if (self=[super init]) {
        listProducts=[[NSMutableArray alloc] initWithArray:products];
        tableSize = tableS_;
    }
    return self;
}
-(void)resetWithProducts:(NSArray *)productsNew{
    [listProducts removeAllObjects];
    [listProducts addObjectsFromArray:productsNew];
    [_clvMain reloadData];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self resetCellSize];
    _clvMain.dataSource=self;
    _clvMain.delegate=self;
    _clvMain.scrollEnabled=NO;
    UINib *nib=[UINib nibWithNibName:@"CollectionCellProduct" bundle:nil];
    [_clvMain registerNib:nib forCellWithReuseIdentifier:CellProductIdentifier];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [_lbNoneItem setText:[[LanguageUtil sharedLanguageUtil] stringByKey:@"cashier.lb-no-product-item"]];
    [_clvMain reloadData];
    [self resetCellSize];
    [self resetSelectedItem];
}
-(void)resetSelectedItem{
    // reset Selected Cell
    CBLDocument *productSelected =[_delegate contentPageProductRequireProductSelected:self];
    NSInteger indexSelected=-1;
    if (productSelected) {
        indexSelected=[listProducts indexOfObject:productSelected];
        if (indexSelected !=NSNotFound) {
            NSInteger indexCell=indexSelected+1;
            [_clvMain selectItemAtIndexPath:[NSIndexPath indexPathForItem:indexCell inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionTop];
        }
    }
}
-(void)resetCellSize{
    NSInteger cX=(NSInteger)(self.view.frame.size.width - (tableSize.width - 1) * SpaceCellX)/tableSize.width;
    NSInteger cY=(NSInteger)(self.view.frame.size.height - (tableSize.height - 1) * SpaceCellY)/tableSize.height;
    cellSize=CGSizeMake(cX, cY);
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma CollectionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    _lbNoneItem.hidden=(listProducts.count!=0);
    return listProducts.count + 1;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        CollectionCellProduct *cellAdhoc =(CollectionCellProduct*) [collectionView dequeueReusableCellWithReuseIdentifier:CellProductIdentifier forIndexPath:indexPath];
        [cellAdhoc.ivMain cancelLoading];
        [cellAdhoc setValueDefault];
        cellAdhoc.lbTitle.text=[[LanguageUtil sharedLanguageUtil ] stringByKey:@"cashier.text-ad-hoc"];
        cellAdhoc.ivMain.image=[UIImage imageNamed:@"icon_adhoc"];
        return cellAdhoc;
    }
    //else
    CollectionCellProduct *cell =(CollectionCellProduct*) [collectionView dequeueReusableCellWithReuseIdentifier:CellProductIdentifier forIndexPath:indexPath];
    NSInteger indexInListProduct=indexPath.row-1;
    CBLDocument *doc=[listProducts objectAtIndex:indexInListProduct];
    [cell.ivMain cancelLoading];
    [cell setValueDefault];
    cell.lbTitle.text=[TKProduct getName:doc];
    NSInteger quantityWarining=[TKProductItem getMiniQuantityStockWarning:doc];
    if (quantityWarining>=0) {
        if (quantityWarining==0) {
            [cell setEmptyStock];
        }else{
            [cell setRemainFew:quantityWarining];
        }
    }else{
        //<0 quantity not check;
    }
    cell.ivMain.image=nil;
    NSString *urlImage=[TKProduct getImageUrlDefault:doc];
    if (urlImage) {
        [[DLImageLoader sharedInstance] loadImageFromUrl:urlImage completed:^(NSError *error, UIImage *image) {
            if (image) {
                cell.ivMain.image=image;
            }else{
                cell.ivMain.image =[UIImage imageDefaultWithSortName:[TKProduct getShortName:doc]];
            }
        }];
    }else{
        cell.ivMain.image =[UIImage imageDefaultWithSortName:[TKProduct getShortName:doc]];
    }
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        if (_delegate && [_delegate respondsToSelector:@selector(contentPageProduct:clickAdhoc:)]) {
            [_delegate contentPageProduct:self clickAdhoc:YES];
        }
        return;
    }
    //else
    NSInteger indexInListProduct=indexPath.row-1;
    CBLDocument *doc=[listProducts objectAtIndex:indexInListProduct];
    if (_delegate && [_delegate respondsToSelector:@selector(contentPageProduct:selectedProduct:)]) {
        [_delegate contentPageProduct:self selectedProduct:doc];
    }
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return cellSize;
}
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return SpaceCellX;
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return SpaceCellY;
}
-(void)reloadItemWithProductSelected:(CBLDocument *)documentSelected{
    [_clvMain reloadData];
}

@end

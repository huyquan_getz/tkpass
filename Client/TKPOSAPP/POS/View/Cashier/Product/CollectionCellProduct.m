//
//  CollectionCellProduct.m
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "CollectionCellProduct.h"
#import "Constant.h"

@implementation CollectionCellProduct

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)setValueDefault{
    UIView *backgroundView =[[UIView alloc] init];
    [backgroundView setBackgroundColor:tkColorMainSelected];
    [super setSelectedBackgroundView:backgroundView];
    _btStatus.hidden=YES;
    _vCover.hidden=YES;
    _btStatus.clipsToBounds=NO;
    _btStatus.userInteractionEnabled=NO;
    [_btStatus setBackgroundImage:nil forState:UIControlStateNormal];
}
-(void)setEmptyStock{
    [_btStatus setBackgroundColor:[UIColor clearColor]];
    [_btStatus setBackgroundImage:[UIImage imageNamed:@"icon-warning.png"] forState:UIControlStateNormal];
    [_btStatus setTitle:@"" forState:UIControlStateNormal];
    [_vCover setBackgroundColor:[UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:0.4]];
    _btStatus.hidden=NO;
    _vCover.hidden=NO;
}
-(void)setRemainFew:(NSInteger)nbProducts{
    _btStatus.clipsToBounds=YES;
    _btStatus.layer.cornerRadius=5;
    [_btStatus setBackgroundImage:[UIImage imageWithColor:[UIColor redColor]] forState:UIControlStateNormal];
    [_btStatus setTitle:[NSString stringWithFormat:@"%li",(long)nbProducts] forState:UIControlStateNormal];
    _btStatus.hidden=NO;
}
@end

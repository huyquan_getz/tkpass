//
//  ProductDetailVC.m
//  POS
//
//  Created by Nha Duong Cong on 11/17/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define MaxHeightViewShow 700
#define QuantityDefault 1
#define MaxValueOptionInLine 5
#import "ProductDetailVC.h"
#import "TKProduct.h"
#import "TKProductItem.h"
#import "Controller.h"
#import "ProductOrderSingle.h"
#import "CashierBusinessModel.h"
#import "TKVariant.h"

@interface ProductDetailVC ()

@end

@implementation ProductDetailVC{
    LanguageUtil *languageKey;
    NSMutableArray *listButtonOption;
    NSArray *listOption; // not init when class init
    NSInteger quantityFirst;
    BOOL onlyShow; // hiden button add
}
-(instancetype)initWithProduct:(CBLDocument *)product_ onlyShow:(BOOL)onlyShow_{
    if ( self =[super init]) {
        onlyShow=onlyShow_;
        listButtonOption=[[NSMutableArray alloc] init];
        product =product_;
        NSLog(@"%@",product.documentID);
    }
    return self;
}
-(instancetype)initWithProductOrder:(ProductOrderSingle *)productOrder fromOrder:(BOOL)fromOrder_{
    if (self=[super init]) {
        oldProductOrder=productOrder;
        product =[[SyncManager sharedSyncManager] documentWithDocumentId:[oldProductOrder productId]];
        variantOptionSelected=[oldProductOrder.variantOptions mutableCopy];
        quantityFirst =oldProductOrder.quantity;
        listButtonOption=[[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [_vInfoProduct setShadowWithCornerRadius:tkCornerRadiusViewPopup];
    _btAdd.layer.cornerRadius =tkCornerRadiusButton;
    _tfQuanity.layer.borderWidth =1;
    _tfQuanity.layer.borderColor = tkColorMain.CGColor;
    _tfQuanity.layer.cornerRadius =tkCornerRadiusButton;
    _btDecrease.layer.borderWidth =1;
    _btDecrease.layer.borderColor = tkColorMain.CGColor;
    _btDecrease.layer.cornerRadius =tkCornerRadiusButton;
    [_btDecrease setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btDecrease setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [_btDecrease setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    
    _btIncrease.layer.borderWidth =1;
    _btIncrease.layer.borderColor = tkColorMain.CGColor;
    _btIncrease.layer.cornerRadius =tkCornerRadiusButton;
    [_btIncrease setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btIncrease setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [_btIncrease setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    _btRemoveOrderList.layer.cornerRadius=tkCornerRadiusButton;
    _scrollMain.contentMode = UIViewContentModeTop;
    
    _tfQuanity.text=[NSString stringWithFormat:@"%i",QuantityDefault];
    _lbAvailableQuantity.hidden=YES;
    _lbTitle.text=@"";
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [_vCover addGestureRecognizer:tap];
    [self clearMessageError];
    [self setAmountCrude:@""];
    //set size scroll
    
    if (oldProductOrder) {
        CGRect frame=_scrollMain.frame;
        frame.size.height=405;
        [_scrollMain setFrame:frame];
        _btRemoveOrderList.hidden=NO;
    }else{
        _btRemoveOrderList.hidden=YES;
    }
    
    //if onlyShow, hiden button add
    _btAdd.hidden=onlyShow;
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    languageKey =[LanguageUtil sharedLanguageUtil];
    if (oldProductOrder) {
        [_btAdd setTitle:[languageKey stringByKey:@"general.bt.save"] forState:UIControlStateNormal];
        _lbTitle.text=[NSString stringWithFormat:@"%@: %@",[languageKey stringByKey:@"cashier.pr-dt.text-update"],[TKProduct getName:product]];
        
    }else if(!onlyShow){
        [_btAdd setTitle:[languageKey stringByKey:@"general.bt.add"] forState:UIControlStateNormal];
        _lbTitle.text=[NSString stringWithFormat:@"%@: %@",[languageKey stringByKey:@"cashier.pr-dt.text-add"],[TKProduct getName:product]];
    }else{
        //only show
        _btAdd.hidden=YES;
        _lbTitle.text=[TKProduct getName:product];
    }
    
    [_btIncrease setTitle:[languageKey stringByKey:@"cashier.pr-dt.bt.increase"] forState:UIControlStateNormal];
    [_btDecrease setTitle:[languageKey stringByKey:@"cashier.pr-dt.bt.decrease"] forState:UIControlStateNormal];
    
    if (quantityFirst<1) {
        quantityFirst=1;
    }
    _tfQuanity.text=[NSString stringWithFormat:@"%li",(long)quantityFirst];
    [self fillPrice];
    [self addOptions];
    [self setValueDefault];
}
-(void)setAmountCrude:(NSString*)amountCrude{
    NSMutableAttributedString *stringAttr = [[NSMutableAttributedString alloc] initWithString:amountCrude];
    [stringAttr addAttributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)} range:NSMakeRange(0, amountCrude.length)];
    [_lbAmountCrude setAttributedText:stringAttr];
}
-(void)addOptions{
    NSInteger position=0;
     listOption =[TKProduct getArrayOptions:product];
    //add ui option to scrollview
    [listButtonOption removeAllObjects];
    for (int i =0 ;i< listOption.count ;i++) {
        position = [self addOption:listOption[i] toScrollAtPosition:position optionIndex:i];
    }
    
    [_scrollMain setContentSize:CGSizeMake(_scrollMain.frame.size.width, position)];
}

-(void)updateUIButtonOptionWihtVariantOptionSelected:(NSArray*)optionsSelected{
    for (int i=0; i<listOption.count; i++) {
        NSArray *valuesOption=[listOption[i] objectForKey:@"options"];
        NSString *nameOption=[listOption[i] objectForKey:@"name"];
        
        NSString *keyOptionCompare=[[optionsSelected[i] convertNullToNil] objectForKey:@"optionKey"];
        NSString *valueOptionCompare=[[optionsSelected[i] convertNullToNil] objectForKey:@"optionValue"];
        for (int j=0; j<valuesOption.count;j++) {
            NSString *vlOption=valuesOption[j];
            UIButton *bt=(UIButton*)listButtonOption[i][j];
            bt.selected=NO;
            if ([nameOption isEqualToString:keyOptionCompare] && [vlOption isEqualToString:valueOptionCompare ]) {
                bt.selected=YES;
            }
        }
    }
}
-(NSInteger)addOption:(NSDictionary*)option toScrollAtPosition:(NSInteger)positionY optionIndex:(NSInteger)optionIndex{
    UILabel *lbName =[[UILabel alloc] initWithFrame:CGRectMake(15, positionY + 15, 560, 30)];
    [lbName setTextColor:[UIColor blackColor]];
    [lbName setFont:tkBoldFontMainText];
    [lbName setBackgroundColor:[UIColor clearColor]];
    lbName.text =(NSString*)[option[@"name"] convertNullToNil];
    [_scrollMain addSubview:lbName];
    
    CGPoint pointOrigin =CGPointMake(15, 44);
    NSArray *optionValues=option[@"options"];
    //        widthbutton = (     total width    -   margin       -  separate   )  / number button
    NSInteger widthButton =( _scrollMain.frame.size.width - 2*15 - (MaxValueOptionInLine -1 )* 30 )/ MaxValueOptionInLine;
    NSMutableArray *buttonsInOption=[[NSMutableArray alloc] init];
    for (int i =0 ;i< optionValues.count ; i++) {
        pointOrigin.x = 15 + (i%MaxValueOptionInLine)*(widthButton + 30);
        pointOrigin.y = positionY + 44 + (i/MaxValueOptionInLine)*65;
        UIButton *button =[[UIButton alloc] initWithFrame:CGRectMake(pointOrigin.x, pointOrigin.y, widthButton, 50)];
        [button setTitle:optionValues[i] forState:UIControlStateNormal];
        button.contentEdgeInsets=UIEdgeInsetsMake(0, 5, 0, 5);
        [button.titleLabel setFont:tkFontMainTitleButton];
        button.titleLabel.contentMode=UIViewContentModeLeft;
        button.titleLabel.textAlignment=NSTextAlignmentCenter;
        button.titleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        button.titleLabel.numberOfLines=2;
        button.titleLabel.baselineAdjustment=UIBaselineAdjustmentAlignBaselines;
        [button setBackgroundColor:[UIColor whiteColor]];
        [button setBackgroundImage:[UIImage imageNamed:@"bg_option_selected.png"] forState:UIControlStateSelected];
        //[button setBackgroundImage:[UIImage imageNamed:@"bg_option_disable.png"] forState:UIControlStateDisabled];
        [button setTitleColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1] forState:UIControlStateDisabled];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setTitleColor:tkColorMainSelected forState:UIControlStateHighlighted];
        button.layer.cornerRadius =tkCornerRadiusButton;
        button.layer.borderWidth=1;
        button.layer.borderColor=[UIColor blackColor].CGColor;
        // tag button is (x*100 + y)    x index's Option , y index's Value in Option
        button.tag = optionIndex * 100 + i;
        if (oldProductOrder ==nil) {// editing
            [button addTarget:self action:@selector(clickOption:) forControlEvents:UIControlEventTouchUpInside];
        }
        [_scrollMain addSubview:button];
        [buttonsInOption addObject:button];
    }
    NSInteger nbLine=ceilf(optionValues.count*1.0/MaxValueOptionInLine);
    [listButtonOption addObject:buttonsInOption];
    return  positionY + 44 + 65*nbLine;
}
-(void)clickOption:(UIButton*)sender{
    [self clearMessageError];
    sender.selected=YES;
    NSInteger indexOption =sender.tag/100;
    //    NSInteger indexValue  =sender.tag%100;
    for (UIButton *buttonValue in listButtonOption[indexOption]) {
        if (buttonValue != sender) {
            buttonValue.selected=NO;
        }
    }
    [self resetWithNewOptionWithIndexOptionCurrentSelected:indexOption];
}
-(void)resetWithNewOptionWithIndexOptionCurrentSelected:(NSInteger)indexOption{

    NSArray *listOptionSelected=[self getOptionSelected];
    
    // check fill all option
    BOOL fill =YES;
    for (id object in listOptionSelected) {
        if (object == [NSNull null]) {
            fill=NO;
            break;
        }
    }
    //file cost
    variantSelected =nil;
    variantOptionSelected = nil;
    if (fill) {
        NSArray *variantAvailable =[self variantsAvailableWithOption:listOptionSelected];
        if (variantAvailable.count > 0) {
            variantSelected = [variantAvailable firstObject];
            variantOptionSelected = [listOptionSelected copy];
        }
        [self fillPrice];
    }

    [self resetQuantity];
    NSInteger fistOptionNotSelect =[listOptionSelected indexOfObject:[NSNull null]];
    if (fistOptionNotSelect <= indexOption) {
        return;// option before not select
    }
    
    //reset button;
    for (NSInteger index=indexOption; index< listOption.count - 1; index++) { // right option
        NSArray *variantAvailable =[self variantsAvailableWithOption:[listOptionSelected subarrayWithRange:NSMakeRange(0, index+1)]];
        NSArray *valuesOptionNext=[listOption[index+1] objectForKey:@"options"];
        NSString *optionKeyNext=[listOption[index+1] objectForKey:@"name"];
        BOOL stop=YES;
        for (NSInteger indexValue =0; indexValue< valuesOptionNext.count ; indexValue++) { // all value in option
            UIButton *btOptionNext =(UIButton*)[listButtonOption[index+1] objectAtIndex:indexValue];
            btOptionNext.enabled=YES;
            btOptionNext.layer.borderColor=[UIColor blackColor].CGColor;
            if ([self existOptionKey:optionKeyNext optionValue:valuesOptionNext[indexValue] inVariants:variantAvailable]) {
                if (btOptionNext.selected) {
                    stop=NO;
                }
            }else{
                btOptionNext.selected=NO;
                btOptionNext.enabled=NO;
                btOptionNext.layer.borderColor=[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1].CGColor;
            }
        }
        
        if (stop) {
            break;
        }
    }
    
}
-(void)setButtonOptionFirst{ //disable  value option not exist
    if (listOption.count == 0) {
        return;
    }
    
    if (variantOptionSelected) {
        if (variantOptionSelected.count < listOption.count) {
            for (NSInteger i=variantOptionSelected.count; i<listOption.count; i++) {
                [variantOptionSelected addObject:[NSNull null]];
            }
        }
        [self updateUIButtonOptionWihtVariantOptionSelected:variantOptionSelected];
        [self resetWithNewOptionWithIndexOptionCurrentSelected:variantOptionSelected.count-1];
    }
    
    NSArray *variantAvailable =[self variantsAvailableWithOption:@[]];
    NSArray *valuesOptionFirst=[[listOption firstObject] objectForKey:@"options"];
    NSString *optionKeyFirst=[[listOption firstObject] objectForKey:@"name"];
    for (NSInteger indexValue =0; indexValue< valuesOptionFirst.count ; indexValue++) { // all value in option
        UIButton *btOptionNext =(UIButton*)[[listButtonOption firstObject] objectAtIndex:indexValue];
        btOptionNext.enabled=YES;
        if (![self existOptionKey:optionKeyFirst optionValue:valuesOptionFirst[indexValue] inVariants:variantAvailable]) {
            btOptionNext.selected=NO;
            btOptionNext.enabled=NO;
            btOptionNext.layer.borderColor=[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1].CGColor;
        }
    }
}
-(BOOL)existOptionKey:(NSString*)optionKey optionValue:(NSString*)optionValue inVariants:(NSArray*)listVariants{
    for (NSDictionary *variant in listVariants) {
        NSString *valueInVariant =[variant objectForKey:optionKey];
        if ([valueInVariant isEqualToString:optionValue]) {
            return YES;
        }
    }
    return NO;
}
-(NSArray*)getOptionSelected{
    NSMutableArray *optionSelected=[[NSMutableArray alloc] init];
    for (int i=0; i< listOption.count; i++) { // null same not selected
        [optionSelected addObject:[NSNull null]];
    }
    for (NSArray *btValuesInOption in listButtonOption) {
        for (UIButton *btValue in btValuesInOption) {
            if (btValue.selected) {
                // save option selected
                NSInteger indexOption =btValue.tag/100;
                NSInteger indexValue  =btValue.tag%100;
                NSString *keyOption =[listOption[indexOption] objectForKey:@"name"];
                NSString *valueOption =[[listOption[indexOption] objectForKey:@"options"] objectAtIndex:indexValue];
                [optionSelected replaceObjectAtIndex:indexOption withObject:@{@"optionKey":keyOption,@"optionValue":valueOption}];
                //
                break;
            }
        }
    }
    return optionSelected;

}
-(void)resetQuantity{
    if (variantSelected) {
        if ([self quanityOfVarientSelected]  < 0) {
            _lbAvailableQuantity.text=@"";
        }else{
            _lbAvailableQuantity.text=[NSString stringWithFormat:@"%@ %li",[languageKey stringByKey:@"cashier.pr-dt.lb.available-quantity"], (long)[self quanityOfVarientSelected]];
        }
        if (oldProductOrder == nil) {// if edit product order from Order
            if (![self validateQuantity:[_tfQuanity.text integerValue]]) {
                _tfQuanity.text=[NSString stringWithFormat:@"%i",QuantityDefault];
            }
        }
        
        _lbAvailableQuantity.hidden=NO;
        _tfQuanity.enabled=YES;
        _tfQuanity.textColor=[UIColor blackColor];
        _btDecrease.enabled=YES;
        _btIncrease.enabled=YES;
    }else{
        _lbAvailableQuantity.hidden=YES;
        _tfQuanity.enabled=NO;
        _tfQuanity.textColor=[UIColor grayColor];
        _btDecrease.enabled=NO;
        _btIncrease.enabled=NO;
    }
}
-(NSArray*)variantsAvailableWithOption:(NSArray*)optionSelected{ // object include NSDictionary {optionKey, optionValue}, if not select null
    NSArray *arrayVariant =[TKProductItem getArrayVariantAvailable:product];
    NSMutableArray *variantsCurentAvailable =[[NSMutableArray alloc] init];
    for (NSDictionary *variant in arrayVariant) {
        BOOL equal = YES;
        for (int i=0;i< optionSelected.count ;i++) {
            if (optionSelected[i] != [NSNull null]) {
                NSDictionary *option =optionSelected[i];
                if (![option[@"optionValue"] isEqualToString:variant[option[@"optionKey"]]]) {
                    equal=NO;
                    break;
                }
            }
        }
        if (equal) {
            [variantsCurentAvailable addObject:variant];
        }
    }
    return variantsCurentAvailable;
}

//-(void)fillPrice:(double)price{
//    _lbAmount.text = [CashierBusinessModel stringAmount:price withCurrency:[CashierBusinessModel currencyDefault]];
//}
-(void)fillPrice{
    if (variantOptionSelected==nil) {
         _lbAmount.text = [TKAmountDisplay stringAmount:0 withCurrency:[Controller currencyDefault]];
        [self setAmountCrude:@""];
        return;
    }
    double priceCrude = [TKVariant getPrice:variantSelected];
    double pricePaid=priceCrude;
    TKDiscount *discount = [TKProductItem checkDiscount:product];
    if (discount) {
        double discountAmount=[CashierBusinessModel discountAmount:pricePaid withDiscount:discount];
        pricePaid -= discountAmount;
        [self setAmountCrude:[TKAmountDisplay stringAmount:priceCrude withCurrency:[Controller currencyDefault]]];
    }else{
        [self setAmountCrude:@""];
    }
    _lbAmount.text = [TKAmountDisplay stringAmount:pricePaid withCurrency:[Controller currencyDefault]];
}
-(void)setValueDefault{
    [self setButtonOptionFirst];
    if (listOption.count == 0) {
        NSArray *arrayVariant =[TKProductItem getArrayAllVariant:product];
        if (arrayVariant && arrayVariant.count>0) {
            variantSelected =[arrayVariant firstObject];
            variantOptionSelected=[[NSMutableArray alloc] init];
            [self fillPrice];
        }
    }
    [self resetQuantity];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tapCoverView:(id)sender{
    [self clickCancel:nil];
}
-(IBAction)clickCancel:(id)sender{
    if (_delegate) {
        [_delegate productDetailCancel:self];
    }
}
- (IBAction)clickAdd:(id)sender{
    if (![self validateInput]) {
        return;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (_delegate) {
            NSMutableDictionary *property=[product.properties mutableCopy];
            TKDiscount *discount=[TKProductItem checkDiscount:product];
            [property setObject:@(discount!=nil) forKey:tkKeyDiscount];
            ProductOrderSingle *newProductOrder=[[ProductOrderSingle alloc] initWithProductProperties:property variantOrder:variantSelected variantOptions:variantOptionSelected quantity:[_tfQuanity.text integerValue]];
            [_delegate productDetail:self editProductOrder:oldProductOrder withProductOrder:newProductOrder productOriginal:product];
        }
    });
}
-(IBAction)clickRemoveFromOrderList:(id)sender{
    if (_delegate) {
        [_delegate productDetail:self editProductOrder:oldProductOrder withProductOrder:nil productOriginal:product];
    }
}
-(void)showMessageError:(NSString*)error{
    _lbMessageError.text=error;
    _lbMessageError.hidden=NO;
    _lbMessageError.textColor=[UIColor redColor];
}
-(void)clearMessageError{
    _lbMessageError.hidden=YES;
    _lbMessageError.text=@"";
}
- (IBAction)clickIncrease:(id)sender {
    NSUInteger current=[self currentQuantity];
    current++;
    if ([self validateQuantity:current]) {
        [_tfQuanity setText:[NSString stringWithFormat:@"%lu",(unsigned long)current]];
    }
}
-(IBAction)clickDecrease:(id)sender{
    NSUInteger current=[self currentQuantity];
    current--;
    if ([self validateQuantity:current]) {
        [_tfQuanity setText:[NSString stringWithFormat:@"%lu",(unsigned long)current]];
    }
}

-(NSUInteger)currentQuantity{
    return [_tfQuanity.text integerValue];
}

#pragma TextfieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [self clearMessageError];
    // key return
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    // key backspace(delete)
    if (string.length==0) {
        return YES;
    }
    //validate
    NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (![newString isUIntegerFormat]) {
        return NO;
    }
    
    if (variantSelected && ![self validateQuantity:[newString integerValue]]) {
        return NO;
    }
    
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (_vInfoProduct.frame.origin.y > 100) {
        CGRect frame=_vInfoProduct.frame;
        frame.origin.y=100;
        _vInfoProduct.frame=frame;
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    _vInfoProduct.center=self.view.center;
    if (_tfQuanity.text.length==0) {
        _tfQuanity.text=[NSString stringWithFormat:@"%i",QuantityDefault];
    }
}
-(NSInteger)quanityOfVarientSelected{
    NSInteger quantityAvailable=[TKVariant getQuantity:variantSelected];
    if (quantityAvailable>=0) {
        NSInteger oldQuantity =(oldProductOrder)?oldProductOrder.quantity:0;
        quantityAvailable += oldQuantity;
    }else{
        // no check quantity
    }
    return quantityAvailable;
}
-(BOOL)validateQuantity:(NSInteger)quantity{
    if (quantity<=0) {
        return NO;
    }
    
    if ([self quanityOfVarientSelected]<0) { // quantity =-1 is not limit
        return YES;
    }
    
    if (quantity > [self quanityOfVarientSelected]) {
        return NO;
    }
    return YES;
}
-(BOOL)validateInput{
    if (variantSelected == nil || variantOptionSelected==nil) {
        [self showMessageError:[languageKey stringByKey:@"cashier.pr-dt.ms-no-choose-option"]];
        return NO;
    }
    if (![self validateQuantity:[_tfQuanity.text integerValue]]) {
        [self showMessageError:[languageKey stringByKey:@"cashier.pr-dt.ms-invalid-quantity"]];
        return NO;
    }
    return YES;
}
@end

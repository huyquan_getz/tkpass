//
//  TableCellProduct.h
//  POS
//
//  Created by Nha Duong Cong on 11/22/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLImageView.h"

@interface TableCellProduct : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *vCover;
@property (weak, nonatomic) IBOutlet UIButton *btStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet DLImageView *ivMain;
-(void)setValueDefault;
-(void)setEmptyStock;
-(void)setRemainFew:(NSInteger)nbProducts;
@end

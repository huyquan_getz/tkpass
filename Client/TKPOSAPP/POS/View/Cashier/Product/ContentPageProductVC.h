//
//  ContentPageProductVC.h
//  POS
//
//  Created by Nha Duong Cong on 10/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "Constant.h"

@protocol ContentPageProductDelegate <NSObject>
@required
-(void)contentPageProduct:(id)contentProduct selectedProduct:(CBLDocument*)product;
-(CBLDocument*)contentPageProductRequireProductSelected:(id)sender;
-(void)contentPageProduct:(id)contentProduct clickAdhoc:(BOOL)none;
@end

#import <UIKit/UIKit.h>

@interface ContentPageProductVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    NSMutableArray *listProducts;
    CGSize tableSize;
    CGSize cellSize;
}
@property (weak, nonatomic) IBOutlet UILabel *lbNoneItem;
@property (weak,nonatomic) id<ContentPageProductDelegate>delegate;
@property (weak, nonatomic) IBOutlet UICollectionView *clvMain;
-(id)initWithProducts:(NSArray*)products tableSize:(CGSize)tableS;
-(void)resetWithProducts:(NSArray*)productsNew;
-(void)reloadItemWithProductSelected:(CBLDocument*)documentSelected;
@end

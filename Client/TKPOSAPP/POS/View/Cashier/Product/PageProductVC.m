//
//  PageProductVC.m
//  POS
//
//  Created by Nha Duong Cong on 10/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define SpaceX 20
#define SpaceY 30
#define MaxPageDot 5
#define HeightPageControl 30
#import "PageProductVC.h"
#import "ContentPageProductVC.h"
#import "CollectionCellProduct.h"
#import "Controller.h"
#import "ListPageVC.h"

@interface PageProductVC ()

@end

@implementation PageProductVC
@synthesize pageControl;
@synthesize pageVC;
@synthesize currentPageIndex;
@synthesize totalPage;
@synthesize listProducts;
@synthesize numberItemInPage;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithListProducts:(NSArray *)listPro indexItemShow:(NSInteger)indexItemShow{
    if (self = [super init]) {
        if (listPro) {
            listProducts=[[NSMutableArray alloc] initWithArray:listPro];
        }else{
            listProducts=[[NSMutableArray alloc] init];
        }
        totalPage=0;
        numberItemInPage= 24 ;
        currentPageIndex =[self pageIndexWithIndexProductShow:indexItemShow];
    }
    return self;
}
-(id)initWithListProducts:(NSArray *)listPro{
    return [self initWithListProducts:listProducts indexItemShow:0];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    pageControl.currentPage=-1;
    pageControl.numberOfPages=1;
    [pageControl addTarget:self action:@selector(pageControlChangeValue:) forControlEvents:UIControlEventValueChanged];
	[pageControl setPageIndicatorImage:[UIImage imageNamed:@"pageDot"]];
	[pageControl setCurrentPageIndicatorImage:[UIImage imageNamed:@"currentPageDot"]];
    CGRect frame=self.view.frame;
    frame.size.height=HeightPageControl;
    frame.origin.y=self.view.frame.size.height - HeightPageControl;
    [pageControl setFrame:frame];
    [_listPageControl setFrame:frame];
    [self hideListPageControl];
    [self.view addSubview:_listPageControl];
    pageControl.hidden=YES;
    _listPageControl.hidden=YES;
    
    pageVC =[[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:[NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:50.0f] forKey:UIPageViewControllerOptionInterPageSpacingKey]];
    pageVC.dataSource=self;
    pageVC.delegate=self;
    frame=self.view.frame;
    frame.size.height-= (HeightPageControl+SpaceY);
    frame.size.width-=(2*SpaceX);
    frame.origin.x=SpaceX;
    frame.origin.y=SpaceY;
    [pageVC.view setFrame:frame];
    pageVC.view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self reloadPageWithPageIndex:currentPageIndex];
    [self addChildViewController:pageVC];
    [self.view addSubview:pageVC.view];
    [pageVC didMoveToParentViewController:self];
    
    // Do any additional setup after loading the view from its nib.
}
-(NSInteger)pageIndexWithIndexProductShow:(NSInteger)indexProductShow{
    return (NSInteger)(indexProductShow/numberItemInPage);
}
-(void)reloadInPageWithListProducts:(NSArray *)listProductsNew{
    //reload Product with list product and hold pageCurrent;
    [self reloadWithListProducts:listProductsNew showPageIndex:currentPageIndex];
}
-(void)reloadWithListProducts:(NSArray*)listProductsNew showPageIndex:(NSInteger)pageIndex{
    // reload Product with listproduct new and move to page with index
    if (listProductsNew != listProducts) {
        [listProducts removeAllObjects];
        [listProducts addObjectsFromArray:listProductsNew];
    }

    [self reloadPageWithPageIndex:pageIndex];
}
-(void)reloadPageWithPageIndex:(NSInteger)pageIndex{
    // reload Product and move to page with index
    totalPage=[self totalPageWithNumberProduct:listProducts.count];
    currentPageIndex = pageIndex;
    if (pageIndex >= totalPage) {
        currentPageIndex = totalPage-1;
    }
    if (pageIndex<0) {
        currentPageIndex=0;
    }
    [self resetPageControl];
    UIViewController *contentVC=[self contentVCWithIndex:currentPageIndex];
    [pageVC setViewControllers:@[contentVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}
-(void)moveToPage:(NSInteger)pageIndex animation:(BOOL)animation{
    //move page to new page index
    if (pageIndex <0 || pageIndex >= totalPage || pageIndex == currentPageIndex) {
        return;
    }else{
        UIPageViewControllerNavigationDirection direction;
        if (pageIndex > currentPageIndex) {
            direction =UIPageViewControllerNavigationDirectionForward;
        }else{
            direction =UIPageViewControllerNavigationDirectionReverse;
        }
        currentPageIndex=pageIndex;
        UIViewController *contentVC=[self contentVCWithIndex:currentPageIndex];
        [pageVC setViewControllers:@[contentVC] direction:direction animated:animation completion:nil];
    }
}
-(NSInteger)totalPageWithNumberProduct:(NSInteger)numberOfProduct{
    NSInteger nbPageProduct =(NSInteger)ceil(numberOfProduct/(float)numberItemInPage);
    if (nbPageProduct==0) {// have 1 productVC show Empty document
        nbPageProduct+=1;
    }
    return nbPageProduct;
}
-(UIViewController*)contentVCWithIndex:(NSInteger)contentIndex{
    //create contentVC with index.  Assign contentIndex to tag of View of ViewController
    ContentPageProductVC *viewVC=[[ContentPageProductVC alloc] initWithProducts:[self listProductsWithPageIndex:contentIndex] tableSize:CGSizeMake(5, 5)];
    [viewVC.view setFrame:pageVC.view.frame];
    viewVC.delegate=self;
    viewVC.view.tag=contentIndex;
    return viewVC;
}
-(NSInteger)indexOfContentVC:(UIViewController*)contentVC{
    return contentVC.view.tag;
}
#pragma PageViewControllerDelegate
-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
    currentPageIndex =[self indexOfContentVC:[pageViewController.viewControllers firstObject]];
    [self resetPageControl];
}
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    NSInteger viewIndex=[self indexOfContentVC:viewController];
    viewIndex ++;
    if (viewIndex >= totalPage) {
        return nil;
    }else{
        return [self contentVCWithIndex:viewIndex];
    }
}
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    NSInteger viewIndex=[self indexOfContentVC:viewController];
    viewIndex--;
    if (viewIndex<0) {
        return nil;
    }else{
        return [self contentVCWithIndex:viewIndex];
    }
}
-(NSArray*)listProductsWithPageIndex:(NSInteger)pageIndex{
    if (pageIndex<0 || pageIndex >=totalPage) {
        return nil;
    }else{
        return [listProducts subarrayWithRange:NSMakeRange(pageIndex*numberItemInPage, [self numberItemInPageWithPageIndex:pageIndex])];
    }
}
-(NSInteger)numberItemInPageWithPageIndex:(NSInteger)pageIndex{
    if (pageIndex<0 || pageIndex>totalPage) {
        return 0;
    }else if(pageIndex<totalPage-1){
        return numberItemInPage;
    }else{
        return listProducts.count - pageIndex*numberItemInPage;
    }
}
-(CBLDocument*)productWithCurrentPage:(NSInteger)currentPage indexItem:(NSInteger)indexItem{
    return [listProducts objectAtIndex:currentPage * numberItemInPage + indexItem];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma ContentProductDelegate;
-(void)contentPageProduct:(id)contentProduct selectedProduct:(CBLDocument *)product{
    if (_delegate && [_delegate respondsToSelector:@selector(productShower:showProductDetail:)]) {
        [_delegate productShower:self showProductDetail:product];
    }
}
- (void)contentPageProduct:(id)contentProduct clickAdhoc:(BOOL)none{
    if (_delegate && [_delegate respondsToSelector:@selector(productShower:showAdhocProduct:)]) {
        [_delegate productShower:self showAdhocProduct:YES];
    }
}
-(void)reloadItemWithProductSelected:(CBLDocument *)documentSelected{
    ContentPageProductVC *contentPage =(ContentPageProductVC*)[pageVC.viewControllers firstObject];
    [contentPage reloadItemWithProductSelected:documentSelected];
}
-(CBLDocument *)contentPageProductRequireProductSelected:(id)sender{
    if (_delegate) {
        return [_delegate productShower:self requireProductSelected:YES];
    }
    return nil;
}
#pragma ShowProducts
-(void)showListProducts:(NSArray *)products resetPosition:(BOOL)resetPosition{
    if (resetPosition) {
        [self reloadWithListProducts:products showPageIndex:0];
    }else{
        [self reloadInPageWithListProducts:products];
    }
}
-(NSInteger)currentIndexItemShow{
    return currentPageIndex * numberItemInPage;
}
-(void)resetCurrentIndexItemShow{
    currentPageIndex=0;
    [self moveToPage:currentPageIndex animation:NO];
}

-(void)pageControlChangeValue:(id)sender{
    NSInteger curPage=pageControl.currentPage;
    [self moveToPage:curPage animation:YES];
}
// PageControl
-(void)resetPageControl{
    if ( totalPage <= MaxPageDot) {
        [self hideListPageControl];
        pageControl.hidden=NO;
        pageControl.numberOfPages=totalPage;
        pageControl.currentPage=currentPageIndex;
        [pageControl setNeedsDisplay];
    }else{
        pageControl.hidden=YES;
        _listPageControl.hidden=NO;
        [self displayListPageControl];
    }
}
- (IBAction)listPageControl_clickLeft:(id)sender{
    [self moveToPage:currentPageIndex - 1 animation:YES];
    [self displayListPageControl];
}
- (IBAction)listPageControl_clickRight:(id)sender{
    [self moveToPage:currentPageIndex + 1 animation:YES];
    [self displayListPageControl];
}
- (IBAction)listPageControl_clickCenter:(id)sender{
    ListPageVC *listPage=[[ListPageVC alloc] initWithTotalPage:totalPage currentPage:currentPageIndex];
    listPage.delegate=self;
    CGRect frame=listPage.view.frame;
    popoverListPage=[[UIPopoverController alloc] initWithContentViewController:listPage];
    popoverListPage.delegate=self;
    popoverListPage.popoverContentSize=frame.size;
    [popoverListPage presentPopoverFromRect:_listPageControl.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
#pragma UIPopupover
-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    popoverListPage=nil;
}

-(void)hideListPageControl{
    _listPageControl.hidden=YES;
    [_listPageControl_btCenter setTitle:@"" forState:UIControlStateNormal];
}
-(void)displayListPageControl{
    NSString *string=[NSString stringWithFormat:@"%i / %li",currentPageIndex+1,(long)totalPage];
    [_listPageControl_btCenter setTitle:string forState:UIControlStateNormal];
}
-(void)setProductShowerDelegate:(id<ProductShowerDelegate>)delegate_{
    _delegate=delegate_;
}

#pragma ListPageDelegate
-(void)listPage:(id)sender selected:(NSInteger)indexSelect{
    [popoverListPage dismissPopoverAnimated:YES];
    [self moveToPage:indexSelect animation:NO];
    [self displayListPageControl];
}


@end

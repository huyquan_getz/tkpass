//
//  AdHocProductVC.h
//  POS
//
//  Created by Cong Nha Duong on 2/11/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#import "AdhocProduct.h"
@protocol AdHocProductDelegate <NSObject>
-(void)adhocProduct:(id)sender close:(BOOL)none;
-(void)adhocProduct:(id)sender updateAdhoc:(AdhocProduct*)oldAdhoc withNew:(AdhocProduct*)newAdhoc;

@end

#import <UIKit/UIKit.h>

@interface AdHocProductVC : UIViewController<UITextFieldDelegate,UITextViewDelegate>{
    AdhocProduct *oldAddHoc;
}
-(instancetype)initWithAdHoc:(AdhocProduct*)adhoc onlyShow:(BOOL)onlyShow_;
@property (weak,nonatomic) id<AdHocProductDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *tfTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbTitlePrice;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleApplyDiscount;
@property (weak, nonatomic) IBOutlet UISwitch *swApplyDiscount;
@property (weak, nonatomic) IBOutlet UITextView *tvDescription;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
@property (weak, nonatomic) IBOutlet UIView *vLine;
@property (weak, nonatomic) IBOutlet UIView *vTitle;
@property (weak, nonatomic) IBOutlet UIView *vMain;
@property (weak, nonatomic) IBOutlet UIButton *btClose;
- (IBAction)clickClose:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btAdd;
- (IBAction)clickAdd:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btDescription;
- (IBAction)clickDescription:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btRemove;
- (IBAction)clickRemove:(id)sender;

@end

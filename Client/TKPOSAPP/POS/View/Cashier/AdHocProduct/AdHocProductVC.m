//
//  AdHocProductVC.m
//  POS
//
//  Created by Cong Nha Duong on 2/11/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define MaxLengthTitle 50
#define MaxLengthDiscription 300
#import "AdHocProductVC.h"
#import "Controller.h"
#import "Constant.h"
#import "TKAmountDisplay.h"
#import "TKEmployee.h"
#import "TKCategory.h"

@interface AdHocProductVC ()

@end

@implementation AdHocProductVC{
    BOOL onlyShow;
}
-(instancetype)initWithAdHoc:(AdhocProduct *)adhoc onlyShow:(BOOL)onlyShow_{
    if (self =[super init]) {
        oldAddHoc=adhoc;
        onlyShow=onlyShow_;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor clearColor];
    UIView *vCorver=[[UIView alloc] initWithFrame:self.view.bounds];
    [vCorver setViewCoverDefault];
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [vCorver addGestureRecognizer:tap];
    [self.view insertSubview:vCorver belowSubview:_vMain];
    [_vMain setShadowDefault];
    _tfTitle.placeholder=@"Ad-hoc";
    _tfPrice.placeholder=[TKAmountDisplay stringByRoundCashAmout:0];
    _tfTitle.backgroundColor=tkColorMainBackground;
    _tfTitle.layer.borderColor=tkColorFrameBorder.CGColor;
    _tfTitle.layer.borderWidth=1;
    _tfTitle.layer.cornerRadius=tkCornerRadiusButton;
    _tfPrice.layer.borderColor=tkColorFrameBorder.CGColor;
    _tfPrice.layer.borderWidth=1;
    _tfPrice.layer.cornerRadius=tkCornerRadiusButton;
    _tfPrice.backgroundColor=tkColorMainBackground;
    
    UIView *left1=[[UIView alloc] initWithFrame:CGRectMake(0, 0,30+_lbTitleTitle.frame.size.width, 1)];
    UIView *right1=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 1)];
    _tfTitle.rightView=right1;
    _tfTitle.leftView=left1;
    _tfTitle.leftViewMode=UITextFieldViewModeAlways;
    _tfTitle.rightViewMode=UITextFieldViewModeAlways;
    UIView *left2=[[UIView alloc] initWithFrame:CGRectMake(0, 0,30+_lbTitlePrice.frame.size.width, 1)];
    UIView *right2=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 1)];
    _tfPrice.rightView=right2;
    _tfPrice.leftView=left2;
    _tfPrice.leftViewMode=UITextFieldViewModeAlways;
    _tfPrice.rightViewMode=UITextFieldViewModeAlways;
    _tvDescription.layer.borderColor=tkColorFrameBorder.CGColor;
    _tvDescription.layer.borderWidth=1;
    _tvDescription.layer.cornerRadius=tkCornerRadiusButton;
    _tvDescription.backgroundColor=tkColorMainBackground;
    _vLine.backgroundColor=tkColorFrameBorder;
    _tvDescription.textContainerInset=UIEdgeInsetsMake(9,17,10,20);
    _vMain.layer.cornerRadius=tkCornerRadiusViewPopup;
    _vTitle.layer.cornerRadius=tkCornerRadiusViewPopup;
    _vMain.clipsToBounds=YES;
    _vTitle.clipsToBounds=YES;
    _btRemove.layer.cornerRadius=tkCornerRadiusButton;
    [self showMessageError:@""];
    _btAdd.hidden=onlyShow;
    [_swApplyDiscount setOnTintColor:tkColorMain];
    if (oldAddHoc==nil) {
        CGRect frame=_vMain.frame;
        frame.size.height-=60;
        _vMain.frame=frame;
        _btRemove.hidden=YES;
        [_swApplyDiscount setOn:NO];
    
    }else{
        _tfTitle.text=oldAddHoc.title;
        _tfPrice.text=[TKAmountDisplay stringByRoundCashAmout:oldAddHoc.price];
        _tvDescription.text=oldAddHoc.comment;
        [_swApplyDiscount setOn:oldAddHoc.applyDiscount];
    }
    if (onlyShow) {
        _btAdd.hidden=YES;
    }
    _btDescription.hidden=(_tvDescription.text.length!=0);
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    _lbTitleApplyDiscount.text=[[LanguageUtil sharedLanguageUtil] stringByKey:@"cashier.ad-hoc.lb-apply-total-discount"];
}
-(void)showMessageError:(NSString*)error{
    _lbMessageStatus.textColor=[UIColor redColor];
    _lbMessageStatus.text=error;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma TextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect frame=_vMain.frame;
    frame.origin.y-=100;
    _vMain.frame=frame;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    CGRect frame=_vMain.frame;
    frame.origin.y+=100;
    _vMain.frame=frame;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField==_tfTitle) {
        [_tfPrice becomeFirstResponder];
    }
    if (textField==_tfPrice) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_tvDescription becomeFirstResponder];
        });
    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newString=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField==_tfTitle) {
        return (newString.length <= MaxLengthTitle);
    }
    if (textField==_tfPrice) {
        return [newString isTKAmountFormat];
    }
    return YES;
}
#pragma TextViewDelegate
-(void)textViewDidBeginEditing:(UITextView *)textView{
    CGRect frame=_vMain.frame;
    frame.origin.y-=100;
    _vMain.frame=frame;
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    CGRect frame=_vMain.frame;
    frame.origin.y+=100;
    _vMain.frame=frame;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString *newString=[textView.text stringByReplacingCharactersInRange:range withString:text];
    _btDescription.hidden=newString.length!=0;
    return (newString.length <=MaxLengthDiscription);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickDescription:(id)sender {
    [_tvDescription becomeFirstResponder];
}
-(void)tapCoverView:(id)sender{
    [self clickClose:nil];
}
- (IBAction)clickClose:(id)sender {
    if (_delegate) {
        [_delegate adhocProduct:self close:YES];
    }
}
- (IBAction)clickAdd:(id)sender {
    if (![self validateInput]) {
        return;
    }
    [self showMessageError:@""];
    [self.view endEditing:YES];
    if (_delegate) {
        CBLDocument *logined=[Controller getUserLoginedDoc];
        double price=[_tfPrice.text doubleValue];
        AdhocProduct *newAdhoc=[[AdhocProduct alloc] initWithTitle:_tfTitle.text description:_tvDescription.text price:price quantity:1 employeeID:[TKEmployee getEmployeeID:logined] employeeName:[TKEmployee getEmployeeName:logined] applyDiscount:YES categoryId:[TKCategory categoryIdAdhocDefault]];
        [_delegate adhocProduct:self updateAdhoc:oldAddHoc withNew:newAdhoc];
    }
}
-(BOOL)validateInput{
    if (_tfTitle.text.length==0) {
        [self showMessageError:@"Please input title for ad-hoc"];
        return NO;
    }
    if (_tfPrice.text.length==0) {
        [self showMessageError:@"Please input price for ad-hoc"];
        return NO;
    }
    return YES;
}
- (IBAction)clickRemove:(id)sender {
    if (_delegate) {
        [_delegate adhocProduct:self updateAdhoc:oldAddHoc withNew:nil];
    }
}
@end

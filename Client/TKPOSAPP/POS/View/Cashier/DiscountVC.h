//
//  DiscountVC.h
//  POS
//
//  Created by Cong Nha Duong on 2/9/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#import "TKDiscount.h"
@protocol  DiscountDelegate <NSObject>
@required
-(void)discount:(id)sender clickClose:(BOOL)none;
-(void)discount:(id)sender updateDiscount:(TKDiscount*)oldDiscount withNew:(TKDiscount*)newDiscount;
@end
#import <UIKit/UIKit.h>
#import "TKDiscount.h"

@interface DiscountVC : UIViewController<UITextFieldDelegate>{
    TKDiscount *oldDiscount;
}
@property(weak,nonatomic) id<DiscountDelegate> delegate;
-(instancetype)initWithDiscount:(TKDiscount*)discount;
@property (weak, nonatomic) IBOutlet UIButton *btClose;
- (IBAction)clickClose:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btAdd;
- (IBAction)clickAdd:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
@property (weak, nonatomic) IBOutlet UITextField *tfValue;
@property (weak, nonatomic) IBOutlet UIButton *btClear;
- (IBAction)clickClear:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbMessageStatus;
@property (weak, nonatomic) IBOutlet UIButton *btPercent5;
@property (weak, nonatomic) IBOutlet UIButton *btPercent10;
@property (weak, nonatomic) IBOutlet UIButton *btPercent20;
@property (weak, nonatomic) IBOutlet UIButton *btPercent30;
@property (weak, nonatomic) IBOutlet UIButton *btPercent50;
- (IBAction)clickPercent:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btNumber1;
@property (weak, nonatomic) IBOutlet UIButton *btNumber2;
@property (weak, nonatomic) IBOutlet UIButton *btNumber3;
@property (weak, nonatomic) IBOutlet UIButton *btNumber4;
@property (weak, nonatomic) IBOutlet UIView *vTitle;
- (IBAction)clickSegment:(id)sender;
- (IBAction)clickNumber:(id)sender;
- (IBAction)clickDot:(id)sender;
- (IBAction)clickZeroZero:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vMain;

@property (weak, nonatomic) IBOutlet UIButton *btNumber5;
@property (weak, nonatomic) IBOutlet UIButton *btNumber6;
@property (weak, nonatomic) IBOutlet UIButton *btNumber7;
@property (weak, nonatomic) IBOutlet UIButton *btNumber8;
@property (weak, nonatomic) IBOutlet UIButton *btNumber9;
@property (weak, nonatomic) IBOutlet UIButton *btNumber0;
@property (weak, nonatomic) IBOutlet UIButton *btDot;
@property (weak, nonatomic) IBOutlet UIButton *btZeroZero;
@property (weak, nonatomic) IBOutlet UIView *vLine1;
@property (weak, nonatomic) IBOutlet UIView *vLine2;
@property (weak, nonatomic) IBOutlet UIView *vLine3;
@property (weak, nonatomic) IBOutlet UIView *vLine4;
@property (weak, nonatomic) IBOutlet UIView *vLine5;
@property (weak, nonatomic) IBOutlet UIView *vLine6;
@property (weak, nonatomic) IBOutlet UIView *vLine10;
@property (weak, nonatomic) IBOutlet UIView *vLine11;
@property (weak, nonatomic) IBOutlet UIView *vLine12;
@end

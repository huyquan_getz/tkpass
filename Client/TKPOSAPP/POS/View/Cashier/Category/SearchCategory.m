//
//  SearchCategory.m
//  POS
//
//  Created by Nha Duong Cong on 10/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define CellCategoryIdentify @"cellCategory"
#define MaxHeightCategory 454
#define HeightCell 50
#define KeyPathNoti @"rows"
#import "SearchCategory.h"
#import "DLImageLoader.h"
#import "DLImageView.h"
#import "TKCategory.h"
#import "Controller.h"
#import "TableCellCategory.h"

@interface SearchCategory ()

@end

@implementation SearchCategory{
    LanguageUtil *languageKey;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [_tbvMain registerNib:[UINib nibWithNibName:@"TableCellCategory" bundle:nil] forCellReuseIdentifier:CellCategoryIdentify];
    _ivTickAllProduct.hidden=YES;
    _vLineAllProduct.backgroundColor=tkColorFrameBorder;
    listCategory=[[NSMutableArray alloc] init];
    listNumberProductInCategory=[[NSMutableArray alloc] init];
    _vCellAllProduct.backgroundColor=tkColorMainBackground;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    languageKey=[LanguageUtil sharedLanguageUtil];
    [_lbAllProduct setTitle:[languageKey stringByKey:@"cashier.category.text-all-category"] forState:UIControlStateNormal];
    _lbNoCategory.text=[languageKey stringByKey:@"cashier.category.lb-no-category"];
}
-(void)viewDidAppear:(BOOL)animated{
    [self setupQueryLive];
}

-(void)setSelecteCell{
    CBLDocument *categorySelected=[_delegate categorySelected];
    NSIndexPath *indexPathSelected=nil;
    if (categorySelected) {
        NSInteger indexExisted=[listCategory indexOfObject:categorySelected];
        if (indexExisted!=NSNotFound) {
            indexPathSelected=[NSIndexPath indexPathForRow:indexExisted inSection:0];
        }
    }
    if (indexPathSelected) {
        [_tbvMain selectRowAtIndexPath:indexPathSelected animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
    _ivTickAllProduct.hidden=(indexPathSelected!=nil);
    
}
-(NSInteger)heightWithTotalPage:(NSInteger)total{
    NSInteger totalHeightContent = total * HeightCell + 14;
    if (totalHeightContent > MaxHeightCategory) {
        totalHeightContent=MaxHeightCategory;
    }
    return totalHeightContent;
}
-(void)setupQueryLive{
    CBLQuery *query =[Controller queryGetAllCategoryDocuments];
    liveQuery =[query asLiveQuery];
    liveQuery.descending=YES;
    [liveQuery addObserver:self forKeyPath:KeyPathNoti options:0 context:NULL];
    [liveQuery start];
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if (object == liveQuery) {
        [self displayRows:liveQuery.rows];
    }
}
-(void)displayRows:(CBLQueryEnumerator*)enumRows{
    NSMutableArray *array=[[NSMutableArray alloc] init];
    for (CBLQueryRow *row in enumRows) {
        [array addObject:row.document];
    }
    [self updateCategoriesWithNewCategory:array];
}
-(void)updateCategoriesWithNewCategory:(NSArray *)categories{
    [[SyncManager sharedSyncManager] backgroundTellWithBlock:^(CBLDatabase *database) {
        CBLQuery *query=[Controller queryAllProductByCategoryIDAndSortKeyMain];
        query.mapOnly=NO;
        query.descending=YES;
        // get total
        CBLQueryEnumerator *enumerRow=[query run:nil];
        totalNumberProduct=[enumerRow.nextRow.value integerValue];
        NSMutableArray *listNb=[[NSMutableArray alloc] init];
        for (CBLDocument *category in categories) {
            query.startKey=@[category.documentID,@{}];
            query.endKey=@[category.documentID];
            CBLQueryEnumerator *enumerRow=[query run:nil];
            if (enumerRow.count>0) {
                [listNb addObject:enumerRow.nextRow.value];
            }else{
                [listNb addObject:@(0)];
            }
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [listCategory removeAllObjects];
            [listCategory addObjectsFromArray:categories];
            [listNumberProductInCategory removeAllObjects];
            [listNumberProductInCategory addObjectsFromArray:listNb];
            [_tbvMain reloadData];
            [self setSelecteCell];
        });
    }];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    _lbNoCategory.hidden=(listCategory.count!=0);
    return listCategory.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableCellCategory *cell =[tableView dequeueReusableCellWithIdentifier:CellCategoryIdentify];
    CBLDocument *doc = [listCategory objectAtIndex:indexPath.row];
    [cell setValueDefault];
    cell.lbTitle.text =[TKCategory getName:doc];
    cell.lbDescription.text = [NSString stringWithFormat:@"%d %@",[listNumberProductInCategory[indexPath.row] integerValue],[languageKey stringByKey:@"cashier.text-items"]];
    [cell.ivMain cancelLoading];
    NSString *urlImage=[TKCategory getImageUrl:doc];
    if (urlImage) {
        [cell.ivMain loadImageFromUrl:urlImage completed:^(NSError *error, UIImage *image) {
            if (image) {
                cell.ivMain.image=image;
            }else{
                cell.ivMain.image =[UIImage imageDefaultWithSortName:[TKCategory getShortName:doc]];
            }
        }];
    }else{
        cell.ivMain.image =[UIImage imageDefaultWithSortName:[TKCategory getShortName:doc]];
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CBLDocument *doc = [listCategory objectAtIndex:indexPath.row];
    if (_delegate) {
        [_delegate searchCategoryVC:self data:doc];
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return HeightCell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return HeightCell;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return _vCellAllProduct;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [liveQuery stop];
    [liveQuery removeObserver:self forKeyPath:KeyPathNoti];
}
- (IBAction)clickAllProduct:(id)sender {
    _ivTickAllProduct.hidden=NO;
    if (_delegate) {
        [_delegate searchCategoryVC:self data:nil];
    }
}
@end

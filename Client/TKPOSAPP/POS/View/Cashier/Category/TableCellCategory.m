//
//  TableCellCategory.m
//  POS
//
//  Created by Nha Duong Cong on 11/22/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TableCellCategory.h"
#import "Constant.h"

@implementation TableCellCategory

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        _lbTitle.textColor=[UIColor whiteColor];
        _lbDescription.textColor=[UIColor whiteColor];
        _ivMain.layer.borderColor=[UIColor whiteColor].CGColor;
    }else{
        _lbTitle.textColor=[UIColor blackColor];
        _lbDescription.textColor=[UIColor darkGrayColor];
        _ivMain.layer.borderColor=tkColorFrameBorder.CGColor;
    }
    // Configure the view for the selected state
}
-(void)setValueDefault{
    UIView *backgroundView =[[UIView alloc] init];
    [backgroundView setBackgroundColor:tkColorMainSelected];
    [super setSelectedBackgroundView:backgroundView];
    _vLine.backgroundColor=tkColorFrameBorder;
    _ivMain.layer.cornerRadius=_ivMain.frame.size.width/2;
    _ivMain.layer.borderWidth=1;
    _ivMain.layer.borderColor=tkColorFrameBorder.CGColor;
}
@end

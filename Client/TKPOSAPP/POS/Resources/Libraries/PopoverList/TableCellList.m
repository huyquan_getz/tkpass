//
//  TableCellList.m
//  POS
//
//  Created by Cong Nha Duong on 1/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "TableCellList.h"
#import "Constant.h"

@implementation TableCellList

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setValueDefault{
    _vLine.hidden=NO;
    _vLine.backgroundColor=tkColorFrameBorder;
}
@end

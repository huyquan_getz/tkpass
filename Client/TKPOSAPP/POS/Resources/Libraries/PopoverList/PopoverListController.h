//
//  PopoverListController.h
//  POS
//
//  Created by Cong Nha Duong on 1/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
@protocol PopoverListDelegate <NSObject>
@required
-(void)popoverList:(id)sender dismissWithCellIndex:(NSInteger)cellIndex valude:(NSString*)value;

@end
#import <UIKit/UIKit.h>
#import "TableListVC.h"

@interface PopoverListController : UIPopoverController<TableListDelegate>{
    TableListVC *tableList;
    NSArray *listString;
    NSInteger heightCell;
}
@property (weak,nonatomic)id<PopoverListDelegate> listDelegate;
@property (strong,nonatomic) UIColor *colorLine;
@property (strong,nonatomic) UIColor *colorText;
@property (strong,nonatomic) UIFont *fontText;

//@override
-(instancetype)initWithListString:(NSArray*)listString_ messageEmptyList:(NSString*)emptyMessage;
-(void)presentPopoverFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated;
@end

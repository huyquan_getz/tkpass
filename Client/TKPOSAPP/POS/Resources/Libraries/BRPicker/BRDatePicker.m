//
//  BRDatePicker.m
//  POS
//
//  Created by Cong Nha Duong on 3/5/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import "BRDatePicker.h"

@interface BRDatePicker ()

@end

@implementation BRDatePicker
- (void)viewDidLoad {
    [super viewDidLoad];
    _vMain.layer.cornerRadius=4;
    _vMain.layer.borderColor=[UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1].CGColor;
    _vMain.layer.borderWidth=1;
    [_vMain.layer setShadowColor:[UIColor blackColor].CGColor];
    [_vMain.layer setShadowOpacity:0.7];
    [_vMain.layer setShadowRadius:4];
    [_vMain.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    _pkDate.datePickerMode=UIDatePickerModeDate;
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCoverView:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [_vCorver addGestureRecognizer:tap];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    if (_dateDefault) {
        [_pkDate setDate:_dateDefault];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tapCoverView:(id)sender{
    [self hidePicker];
    if (_delegate && [_delegate respondsToSelector:@selector(brDatePicker:cancelSetDate:)]) {
        [_delegate brDatePicker:self cancelSetDate:YES];
    }
}
- (IBAction)clickClear:(id)sender {
    [self hidePicker];
    if (_delegate && [_delegate respondsToSelector:@selector(brDatePicker:clickClear:)]) {
        [_delegate brDatePicker:self clickClear:YES];
    }
}

- (IBAction)clickSetDate:(id)sender {
    [self hidePicker];
    if (_delegate && [_delegate respondsToSelector:@selector(brDatePicker:setDate:)]) {
        [_delegate brDatePicker:self setDate:_pkDate.date];
    }
}
-(void)showPickerOnVC:(UIViewController *)parentVC{
    [parentVC addChildViewController:self];
    self.view.frame=parentVC.view.frame;
    [parentVC.view addSubview:self.view];
}
-(void)hidePicker{
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}
@end

//
//  NSObject+Util.h
//  POS
//
//  Created by Nha Duong Cong on 11/7/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Util)
-(id)convertNullToNil;
@end

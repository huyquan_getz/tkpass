//
//  UIColor+Util.h
//  POS
//
//  Created by Nha Duong Cong on 11/6/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Util)
+(UIColor*)statusNaviBar;
@end

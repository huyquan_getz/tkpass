//
//  NSDate+Util.m
//  POS
//
//  Created by Nha Duong Cong on 11/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "NSDate+Util.h"

@implementation NSDate (Util)
-(NSString *)getStringWithFormat:(NSString *)formatString{
    NSDateFormatter *fm =[[NSDateFormatter alloc]init];
    [fm setDateFormat:formatString];
    [fm setTimeZone:[NSTimeZone localTimeZone]]; // to show , don't change local zone
    return [fm stringFromDate:self];
}
-(BOOL)sameDateWithDate:(NSDate *)date{
    NSString *selfD = [self getStringWithFormat:@"yyMMdd"];
    NSString *anotherD =[date getStringWithFormat:@"yyMMdd"];
    return [selfD isEqualToString:anotherD];
}
+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day timeZone:(NSTimeZone*)timezone{
    return [self dateWithYear:year month:month day:day timeZone:timezone hour:0 minute:0 second:0];
}
+(NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day timeZone:(NSTimeZone *)timezone hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    // Assign the year, month and day components.
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    [components setHour:hour];
    [components setMinute:minute];
    [components setSecond:second];
    [components setTimeZone:timezone];
    // Zero out the hour, minute and second components.
    
    // Generate a valid NSDate and return it.
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    return [gregorianCalendar dateFromComponents:components];
}
+(NSDate*)tkDateStartPeriodIn:(NSInteger)monthBefore toCurrentDate:(NSDate*)currentDate{
    // in 3 month
    NSInteger date=1;
    NSInteger month=[[currentDate getStringWithFormat:@"MM"] integerValue];
    NSInteger year =[[currentDate getStringWithFormat:@"yyyy"] integerValue];
    month -= (monthBefore-1);
    if (month<1) {
        month+=12;
        year-=1;
    }
    return [self dateWithYear:year month:month day:date timeZone:[NSTimeZone localTimeZone]];
}
+(NSDate*)tkDateStartBeforeIn:(NSInteger)monthBefore toCurrentDate:(NSDate*)currentDate{
    // in 3 month
    NSInteger date=[[currentDate getStringWithFormat:@"dd"] integerValue];
    NSInteger month=[[currentDate getStringWithFormat:@"MM"] integerValue];
    NSInteger year =[[currentDate getStringWithFormat:@"yyyy"] integerValue];
    month -= monthBefore;
    if (month<1) {
        month+=12;
        year-=1;
    }
    NSInteger numberDateCurrentMonth=[self numberDaysOfYear:year month:month];
    if (date>numberDateCurrentMonth) {
        date=numberDateCurrentMonth;
    }
    return [self dateWithYear:year month:month day:date timeZone:[NSTimeZone localTimeZone]];
}
+(NSInteger)numberDaysOfYear:(NSInteger)year month:(NSInteger)month{
    NSDate *firtDate=[self dateWithYear:year month:month day:1 timeZone:[NSTimeZone localTimeZone]];
    NSCalendar *c =[NSCalendar currentCalendar];
    return [c rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:firtDate].length;
}
@end

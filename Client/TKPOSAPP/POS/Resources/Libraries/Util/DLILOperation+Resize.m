//
//  DLILOperation+Resize.m
//  POS
//
//  Created by Nha Duong Cong on 11/21/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "DLILOperation+Resize.h"
#import "UIImage+Util.h"

@implementation DLILOperation (Resize)
-(UIImage *)resizeImageBeforeSave:(UIImage *)image{
    return [image resizeImageWithSizeMax:CGSizeMake(80, 80)];
}
@end

//
//  NSString+Util.m
//  POS
//
//  Created by Nha Duong Cong on 10/14/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "NSString+Util.h"

@implementation NSString (Util)
-(BOOL)isEmailFormat{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailPredicate evaluateWithObject:self];
}
-(NSString *)encodeURL{
    return (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)self,
                                                                NULL, (CFStringRef)@"`@#$%^&=+[{]}\\|;:'\",<>/? ",
                                                                CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)));

}
-(NSString *)decodeURL{
    return [self stringByRemovingPercentEncoding];
}
-(BOOL)isUIntegerFormat{
    for (int i=0; i<self.length; i++) {
        if ([self characterAtIndex:i] < '0' || [self characterAtIndex:i] > '9') {
            return NO;
        }
    }
    return YES;
}
-(BOOL)isFloatFormat{
    NSInteger nbDot=0;
    for (int i=0; i<self.length; i++) {
        if ([self characterAtIndex:i] == '.') {
            nbDot++;
            continue;
        }
        if ([self characterAtIndex:i] < '0' || [self characterAtIndex:i] > '9') {
            return NO;
        }
    }
    return nbDot<=1;
}
-(BOOL)isTKAmountFormat{
    if (self.length>37) {
        return NO;
    }
    if ([self isFloatFormat]) {
        NSInteger indexDot=[self rangeOfString:@"."].location;
        if (indexDot==NSNotFound || self.length< 3 ||(self.length>=3 && indexDot>=self.length-3)) {
            return YES;
        }
    }
    return NO;
}
-(BOOL)isIPv4AddressFormat{
    for (NSInteger i=0;i<self.length;i++) {
        unichar c=[self characterAtIndex:i];
        if ((c<'0' || c>'9') && c!='.') {
            return NO;
        }
    }
    NSArray *elements=[self componentsSeparatedByString:@"."];
    if (elements.count!=4) {
        return NO;
    }
    for (NSString *eIp in elements) {
        NSInteger eNumber=[eIp integerValue];
        if (eIp.length==0 || eNumber> 255) {
            return NO;
        }
    }
    return YES;
}
-(BOOL)isPrefixIPv4AddressFormat{
    for (NSInteger i=0;i<self.length;i++) {
        unichar c=[self characterAtIndex:i];
        if ((c<'0' || c>'9') && c!='.') {
            return NO;
        }
    }
    NSArray *elements=[self componentsSeparatedByString:@"."];
    if (elements.count>4) {
        return NO;
    }
    for (NSInteger i=0; i<elements.count-1; i++) {
        NSString *eIp=elements[i];
        NSInteger eNumber=[eIp integerValue];
        if (eIp.length==0 || eNumber> 255) {
            return NO;
        }
    }
    NSString *lastEIp=[elements lastObject];
    if (lastEIp.length>0 && [lastEIp integerValue]>255) {
        return NO;
    }
    return YES;
}
-(BOOL)isURLFormat{
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlPredic = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    BOOL isValidURL = [urlPredic evaluateWithObject:self];
    return isValidURL;
}
-(BOOL)isPortFormat{
    return [self isUIntegerFormat] && [self integerValue]<65536;
}
-(CGFloat)widthWithFont:(UIFont*)font;{
    CGSize size = [self sizeWithAttributes:@{NSFontAttributeName: font}];
    return size.width;
}
+(CGFloat)heightWithFont:(UIFont *)font{
    CGSize size=[@"A" sizeWithAttributes:@{NSFontAttributeName: font}];
    return size.height;
}
-(NSInteger)heightWithFont:(UIFont *)font width:(CGFloat)width{
    NSInteger  height=[self sizeWithFont:font constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:NSLineBreakByWordWrapping].height;
    return height;
}
@end

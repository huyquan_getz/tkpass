//
//  TapListener.h
//  POS
//
//  Created by Nha Duong Cong on 10/31/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
/*
 Define class to listener tap gesture recognizer
 */
#import <Foundation/Foundation.h>

@interface TapListener : NSObject{
    BOOL autoRemove;
    UITapGestureRecognizer *tapGesture;
    void (^decorateBlock)(UIView *viewDecorate);
}
@property (weak,nonatomic)UIView *viewHolder; //view holder with property weak
@property (strong,nonatomic) void (^tappedBlock)(void);
-(id)initWithView:(UIView*)view tappedBlock:(void (^)())tappedBl autoRemove:(BOOL)autoRemove;
-(id)initWithView:(UIView*)view tappedBlock:(void (^)())tappedBl;
-(void)tappedToView;// auto call when tapped click , in method will delete tapGesture and call tappedBlock
+(void)removeAllTapListener;
+(NSArray*)allTapListener;
@end

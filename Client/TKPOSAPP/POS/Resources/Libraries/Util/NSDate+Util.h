//
//  NSDate+Util.h
//  POS
//
//  Created by Nha Duong Cong on 11/24/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Util)
// timezone is localTimezone
-(NSString *)getStringWithFormat:(NSString *)formatString;
-(BOOL)sameDateWithDate:(NSDate*)date; // same date
+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day timeZone:(NSTimeZone*)timezone;
+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day timeZone:(NSTimeZone*)timezone hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second;
// timezone is localTimezone
+(NSDate*)tkDateStartPeriodIn:(NSInteger)monthBefore toCurrentDate:(NSDate*)currentDate;
+(NSDate*)tkDateStartBeforeIn:(NSInteger)monthBefore toCurrentDate:(NSDate*)currentDate;
+(NSInteger)numberDaysOfYear:(NSInteger)year month:(NSInteger)month;
@end

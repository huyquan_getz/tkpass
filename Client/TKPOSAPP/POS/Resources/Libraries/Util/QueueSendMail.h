//
//  QueueSendMail.h
//  POS
//
//  Created by Nha Duong Cong on 12/2/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "NetworkUtil.h"
#import "EmailReceipt.h"
@interface QueueSendMail : NSObject<NetworkDelegate>{
    NSString *filePathSaveReceiptWatingSend;
    BOOL resending;
}
+(QueueSendMail*)sharedQueue;
-(void)addToQueueReceipt:(NSDictionary*)receipt toEmail:(NSString*)email type:(MailType)mailType;
-(void)reSendEmailReciept;
-(void)sendReceipt:(NSDictionary *)receipt toEmail:(NSString *)email type:(MailType)mailType;
@end


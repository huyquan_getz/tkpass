//
//  TapListener.m
//  POS
//
//  Created by Nha Duong Cong on 10/31/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TapListener.h"
#import "SNLog.h"
static NSMutableArray *_listTapLister;
@implementation TapListener
+(void)initialize{
    _listTapLister =[[NSMutableArray alloc] init];
}
-(id)initWithView:(UIView *)view_ tappedBlock:(void (^)())tappedBl autoRemove:(BOOL)autoRemove_{
    if (self = [super init]) {
        autoRemove=autoRemove_;
        _viewHolder=view_;
        _tappedBlock=tappedBl;
        tapGesture =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedToView)];
        tapGesture.numberOfTapsRequired=1;
        tapGesture.numberOfTouchesRequired=1;
        [_viewHolder addGestureRecognizer:tapGesture];
        [_listTapLister addObject:self];
    }
    return self;
}
-(id)initWithView:(UIView *)view tappedBlock:(void (^)())tappedBl{
    return [self initWithView:view tappedBlock:tappedBl autoRemove:YES];
}
-(void)tappedToView{
    if (_tappedBlock) {
        _tappedBlock();
    }
    if (autoRemove) {
        if (_viewHolder) {
            [_viewHolder removeGestureRecognizer:tapGesture];
        }
        [_viewHolder removeGestureRecognizer:tapGesture];
        [_listTapLister removeObject:self];
    }
}
+(void)removeAllTapListener{
    [_listTapLister removeAllObjects];
}
+(NSArray*)allTapListener{
    return [_listTapLister copy];
}
-(void)dealloc{
    [SNLog Log:12 :@"TapListener Dealloc"];
}
@end

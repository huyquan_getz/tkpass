//
//  QueueSendMail.m
//  POS
//
//  Created by Nha Duong Cong on 12/2/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#import "QueueSendMail.h"
#import "EmailReceipt.h"
#import "Controller.h"
#import "SNLog.h"

@implementation QueueSendMail
+(QueueSendMail*)sharedQueue{
    __strong static QueueSendMail *_queueSendMail=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _queueSendMail=[[QueueSendMail alloc] init];
    });
    return _queueSendMail;
}
-(instancetype)init{
    if (self =[super init]) {
        filePathSaveReceiptWatingSend=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/data.structure"];
    }
    return self;
}
-(void)sendReceipt:(NSDictionary *)receipt toEmail:(NSString *)email type:(MailType)mailType{
    dispatch_queue_t myQueue = dispatch_queue_create("queue.sendEmail", NULL);
    dispatch_async(myQueue, ^{
        BOOL sent=NO;
        if ([NetworkUtil checkInternetConnection]) {
            sent =[Controller sendReceipt:receipt toEmail:email type:mailType];
            if (sent) {
                [SNLog Log:@"sent receipt:%@ (idDoc:%@) to email:%@",receipt[tkKeyOrderCode],receipt[tkKeyId],email];
            }
        }
        if (!sent) {
            [self addToQueueReceipt:receipt toEmail:email type:mailType];
        }
    });
}
-(void)addToQueueReceipt:(NSDictionary *)receipt toEmail:(NSString *)email type:(MailType)mailType{
    [SNLog Log:@"receipt:%@ send mail into queue",receipt[tkKeyId]];
    EmailReceipt *newEmailReceipt=[[EmailReceipt alloc] initWithReceipt:receipt email:email type:mailType];
    NSMutableArray *array =[[NSMutableArray alloc] initWithArray:[self getListQueueMail]];
    NSInteger indexEmailReceiptExisted=-1;
    for (EmailReceipt *emailReceipt in array) {
        if ([[newEmailReceipt reversionReceipt] isEqualToString:[emailReceipt reversionReceipt]] && [newEmailReceipt.email isEqualToString:newEmailReceipt.email]) {
            indexEmailReceiptExisted=[array indexOfObject:emailReceipt];
            break;
        }
    }
    if (indexEmailReceiptExisted >=0) {
        [array replaceObjectAtIndex:indexEmailReceiptExisted withObject:newEmailReceipt];
    }else{
        [array addObject:newEmailReceipt];
    }
    [self saveReceipt:array];
}
-(void)reSendEmailReciept{
    if (resending) {
        return;
    }
    resending=YES;
    // code resending
    NSMutableArray *listEmail=[[NSMutableArray alloc]initWithArray:[self getListQueueMail]];
    [SNLog Log:@"resent %i receipt",listEmail.count];
    NSMutableArray *listEmailSendSuccess=[[NSMutableArray alloc] init];
    for (EmailReceipt *emailReceipt in listEmail) {
        if ([Controller sendReceipt:emailReceipt.receipt toEmail:emailReceipt.email type:emailReceipt.emailType]) {
            [listEmailSendSuccess addObject:emailReceipt];
            [SNLog Log:@"ReSent receipt:%@ (idDoc:%@) to email:%@",emailReceipt.receipt[tkKeyOrderCode],emailReceipt.receipt[tkKeyId],emailReceipt.email];
        }
        [emailReceipt increaseResendCount];
    }
    [listEmail removeObjectsInArray:listEmailSendSuccess];
    NSMutableArray *listEmailSendExceedLimitResend=[[NSMutableArray alloc] init];
    for (EmailReceipt *email in  listEmail) {
        if ([email exceedingResendcount]) {
            [listEmailSendExceedLimitResend addObject:email];
        }
    }
    [listEmail removeObjectsInArray:listEmailSendExceedLimitResend];
    [self saveReceipt:listEmail];
    resending=NO;
}
-(NSArray*)getListQueueMail{
    NSFileManager *fm =[NSFileManager defaultManager];
    if (![fm fileExistsAtPath:filePathSaveReceiptWatingSend]) {
        return @[];
    }else{
        return [NSKeyedUnarchiver unarchiveObjectWithFile:filePathSaveReceiptWatingSend];
    }
}
-(BOOL)saveReceipt:(NSArray *)receipts{
    return [NSKeyedArchiver archiveRootObject:receipts toFile:filePathSaveReceiptWatingSend];
}

#pragma  Network Delegate
-(void)networkBackgroundNotiConnection:(BOOL)connectionOn{
    //background noti;
    if (connectionOn) {
        [self reSendEmailReciept];
    }
}
@end
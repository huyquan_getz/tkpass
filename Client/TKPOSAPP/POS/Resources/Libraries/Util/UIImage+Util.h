//
//  UIImage+Util.h
//  POS
//
//  Created by Nha Duong Cong on 11/4/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Util)
- (UIImage *) resizeImageWithSizeMax:(CGSize)size;
- (UIImage *) resizeImageWithSizeMax:(CGSize)size cropSquare:(BOOL)cropSquare;
+(UIImage*)imageCornerTopWithColor:(UIColor*)color frame:(CGRect)frame corner:(NSInteger)corner;
+ (UIImage *)imageWithColor:(UIColor *)color;
+(UIImage *)addText:(UIImage *)img text:(NSString *)text1 point:(CGPoint)point;
+(UIImage *)imageDefaultWithSortName:(NSString*)sortName;
+(UIImage *)noImage;
+(UIImage*)imageWithUIView:(UIView*)view;
@end

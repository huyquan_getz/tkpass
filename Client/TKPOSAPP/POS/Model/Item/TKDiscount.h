//
//  TKDiscount.h
//  POS
//
//  Created by Cong Nha Duong on 2/4/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
typedef enum _DiscountType{
    DiscountTypePercent=0,
    DiscountTypeValue=1
}DiscountType;
#import <Foundation/Foundation.h>

@interface TKDiscount : NSObject{
    NSString *discountID;
    NSString *name;
    NSInteger type;
    double discountValue;
    NSDate *beginDate;
    NSDate *endDate;
    BOOL neverExpired;
    NSDate *createdDate;
    NSDate *modifiedDate;
}
@property(readonly) BOOL neverExpired;
@property(readonly) NSString *discountID;
@property(readonly) NSString *name;
@property(readonly) NSInteger type;
@property(readonly) double discountValue;
@property(readonly) NSDate *beginDate;
@property(readonly) NSDate *endDate;
-(instancetype)initFromJsonData:(NSDictionary*)jsonData;
-(instancetype)initWithID:(NSString*)disID name:(NSString*)name_ type:(DiscountType)type_ discountValue:(double)value_ noLimitTime:(BOOL)noLimit beginDate:(NSDate*)date_ endDate:(NSDate*)endDate_;
-(NSDictionary*)jsonValue;
-(BOOL)expiredTime;
@end

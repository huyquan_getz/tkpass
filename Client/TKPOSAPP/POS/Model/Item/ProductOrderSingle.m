//
//  ProductOrderSingle.m
//  POS
//
//  Created by Nha Duong Cong on 12/11/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "ProductOrderSingle.h"
#import "Constant.h"

@implementation ProductOrderSingle
-(instancetype)initWithProductProperties:(NSMutableDictionary *)productPr variantOrder:(NSDictionary *)variantOrder_ variantOptions:(NSArray *)variantOptions_ quantity:(NSInteger)quantity_{
    if (self =[super init]) {
        _productProperies=productPr;
        _variantOptions=variantOptions_;
        _variantOrder=variantOrder_;
        _quantity=quantity_;
    }
    return self;
}

-(NSString *)categoryId{
    return _productProperies[tkKeyCategoryId];
}
-(NSString *)productId{
    return _productProperies[tkKeyId];
}
-(NSString *)productSKU{
    return _productProperies[tkKeySKU];
}
-(NSString *)variantSKU{
    return _variantOrder[tkKeySKU];
}

-(NSString*)productName{
    return _productProperies[tkKeyTitle];
}
-(NSString*)variantName{
    NSMutableArray *optionsN =[_variantOptions mutableCopy];
    [optionsN removeObject:[NSNull null]];
    NSMutableArray* arrayValue=[[NSMutableArray alloc] init];
    for (NSDictionary *dic in optionsN) {
        [arrayValue addObject:dic[@"optionValue"]];
    }
    return [arrayValue componentsJoinedByString:@"-"];
}
-(instancetype)copy{
    return [[ProductOrderSingle alloc] initWithProductProperties:[_productProperies copy] variantOrder:[_variantOrder copy] variantOptions:[_variantOptions copy] quantity:_quantity];
}
-(TKDiscount *)discount{
    if ([_productProperies[tkKeyDiscount] boolValue]) {
        NSDictionary *json=[_productProperies[tkKeyDiscountProductItem] convertNullToNil];
        if (json) {
            return [[TKDiscount alloc] initFromJsonData:_productProperies[tkKeyDiscountProductItem]];
        }
    }
    return nil;
}
-(void)setDiscount:(TKDiscount *)discount{
    [_productProperies setObject:@(discount!=nil) forKey:tkKeyDiscount];
    if (discount) {
        [_productProperies setObject:[discount jsonValue] forKey:tkKeyDiscountProductItem];
    }else{
        [_productProperies setObject:[NSNull null] forKey:tkKeyDiscountProductItem];
    }
}
-(TKDiscount *)checkDiscount{
    if ([_productProperies[tkKeyDiscount] boolValue]) {
        TKDiscount *discount=[self discount];
        if (discount==nil || [discount expiredTime]) {
            return nil;
        }
        return discount;
    }else{
        return nil;
    }
}
-(double)price{
    return [_variantOrder[@"price"] doubleValue];
}
@end

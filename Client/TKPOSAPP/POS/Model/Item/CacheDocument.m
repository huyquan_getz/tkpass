//
//  CacheDocument.m
//  POS
//
//  Created by Nha Duong Cong on 11/12/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "CacheDocument.h"

@implementation CacheDocument
+(CacheDocument*)shareCacheDocment{
    __strong static CacheDocument *_cacheDocument=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _cacheDocument=[[CacheDocument alloc] init];
    });
    return _cacheDocument;
}
@end
//
//  TKCategory.h
//  POS
//
//  Created by Nha Duong Cong on 11/7/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TKItem.h"
#import "Constant.h"

@interface TKCategory : TKItem
+(NSString*)getImageUrl:(CBLDocument*)doc;
+(NSString *)getShortName:(CBLDocument*)doc;
+(NSInteger)getNumberProduct:(CBLDocument*)doc;

+(NSString*)categoryIdAdhocDefault;
@end

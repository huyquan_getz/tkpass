//
//  CacheDocument.h
//  POS
//
//  Created by Nha Duong Cong on 11/12/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheDocument : NSObject{
    NSCache *cacheDocument;
}
+(CacheDocument*)shareCacheDocment;
@end

//
//  ShiftDrawer.h
//  POS
//
//  Created by Nha Duong Cong on 12/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "ActitivityDrawer.h"

@interface ShiftDrawer : NSObject<CBLJSONEncoding>{
    CBLDocument *document;
    NSMutableArray *arrayActivity;
    NSDate *startedDate;
    NSDate *modifiedDate;
    NSDate *closedDate;
    double startingCash;
    double closingCash;
    bool closed;
    NSString *currency;
}
@property (readonly) NSArray *arrayActivity;
@property (readonly) NSDate *startedDate;
@property (readonly) NSDate *modifiedDate;
@property (readonly) NSDate *closedDate;
@property (readonly) double startingCash;
@property (readonly) double closingCash;
@property (readonly) bool closed;
@property (readonly) NSString*currency;
@property (readonly) CBLDocument *document;

-(instancetype)initWithDocument:(CBLDocument*)shiftDrawer;
-(instancetype)initWithStartingCash:(double)startingCash_ currency:(NSString*)currency_;
-(void)addActitvity:(ActitivityDrawer*)activityDrawer;
-(id)encodeAsJSON;
-(instancetype)initWithJSON:(id)jsonObject;
-(void)closeDrawerWithClosingCash:(double)closingCash_;
-(void)closeDrawerNotClosingCashWithDate:(NSDate*)closedDate;
-(NSString*)documentId;
-(double)totalCashInWithType:(CashInType)cashInType_;
-(double)totalCashOutWithType:(CashOutType)cashOutType_;
-(double)expectedTotal;
-(NSDictionary*)dictionaryReportInfo;
@end

//
//  Printer.m
//  POS
//
//  Created by Cong Nha Duong on 1/8/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define keyPrinterName @"printerName"
#define keyIpAddress @"ipAddress"
#define keyPort @"port"
#import "Printer.h"

@implementation Printer
+(CBLDocument *)createPrinterWithName:(NSString *)printerName ip:(NSString *)ipAddress port:(NSInteger)port{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    NSDate *currentDate=[NSDate date];
    [dict setObject:tkPrinterTable forKey:tkKeyTable];
    [dict setObject:[CBLJSON JSONObjectWithDate:currentDate] forKey:tkKeyCreatedDate];
    [dict setObject:[CBLJSON JSONObjectWithDate:currentDate] forKey:tkKeyModifiedDate];
    [dict setObject:printerName forKey:keyPrinterName];
    [dict setObject:ipAddress forKey:keyIpAddress];
    [dict setObject:@(port) forKey:keyPort];
    [dict setObject:@(YES) forKey:tkKeyStatus];
    [dict setObject:@(YES) forKey:tkKeyDisplay];
    return [[SyncManager sharedSyncManager]createDocumentLocalOnly:dict];
}
+(CBLDocument *)updatePrinter:(CBLDocument *)printerDocument withName:(NSString *)printerName ip:(NSString *)ipAddress port:(NSInteger)port{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setObject:[CBLJSON JSONObjectWithDate:[NSDate date]] forKey:tkKeyModifiedDate];
    [dict setObject:printerName forKey:keyPrinterName];
    [dict setObject:ipAddress forKey:keyIpAddress];
    [dict setObject:@(port) forKey:keyPort];
    return  [[SyncManager sharedSyncManager] updateDocument:printerDocument propertyDoc:dict];
}
+(void)removePrinter:(CBLDocument *)printer{
    [[SyncManager sharedSyncManager] deleteDocument:printer];
}
+(NSString *)getPrinterName:(CBLDocument *)document{
    return document.properties[keyPrinterName];
}
+(NSString *)getIpAddress:(CBLDocument *)document{
    return document.properties[keyIpAddress];
}
+(NSInteger)getPort:(CBLDocument *)document{
    return [document.properties[keyPort] integerValue];
}
@end

//
//  ActitivityDrawer.m
//  POS
//
//  Created by Nha Duong Cong on 12/23/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "ActitivityDrawer.h"

@implementation ActitivityDrawer
-(instancetype)initWithEmployeeId:(NSInteger)employeeId_ employeeName:(NSString *)employeeName_ createdDate:(NSDate *)createdDate_ cashInValue:(double)cashInValue_ cashInType:(CashInType)cashInType_ cashOutValue:(double)cashOutValue_ cashOutType:(CashOutType)cashOutType_ comment:(NSString *)comment_ type:(NSString *)type_{
    if (self =[super init]) {
        _employeeId=employeeId_;
        _employeeName=employeeName_;
        _createdDate=createdDate_;
        _cashInType=cashInType_;
        _cashInValue=cashInValue_;
        _cashOutType=cashOutType_;
        _cashOutValue=cashOutValue_;
        _comment=comment_;
        _type=type_;
    }
    return self;
}
-(instancetype)initWithEmployeeId:(NSInteger)employeeId_ employeeName:(NSString *)employeeName_ deposit:(double)depositAmount comment:(NSString *)comment{
    if (self=[super init]) {
        self =[self initWithEmployeeId:employeeId_ employeeName:employeeName_ createdDate:[NSDate date] cashInValue:depositAmount cashInType:CashInTypeDeposit cashOutValue:0 cashOutType:CashOutTypeNone comment:comment type:@"Deposit"];
    }
    return self;
}
-(instancetype)initWithEmployeeId:(NSInteger)employeeId_ employeeName:(NSString *)employeeName_ payOut:(double)payOutAmount comment:(NSString *)comment{
    if (self=[super init]) {
        self =[self initWithEmployeeId:employeeId_ employeeName:employeeName_ createdDate:[NSDate date] cashInValue:0 cashInType:CashInTypeNone cashOutValue:payOutAmount cashOutType:CashOutTypePayOut  comment:comment type:@"Payout"];
    }
    return self;
}
-(instancetype)initWithEmployeeId:(NSInteger)employeeId_ employeeName:(NSString *)employeeName_ paymentCash:(double)paymentAmount changeDue:(double)changeDue comment:(NSString *)comment{
    if (self=[super init]) {
        CashOutType cashOutType=CashOutTypeNone;
        if (changeDue>0) {
            cashOutType=CashOutTypeChangeDue;
        }
        self =[self initWithEmployeeId:employeeId_ employeeName:employeeName_ createdDate:[NSDate date] cashInValue:paymentAmount cashInType:CashInTypeTender cashOutValue:changeDue cashOutType:cashOutType  comment:comment type:@"Check"];
    }
    return self;
}
-(instancetype)initWithEmployeeId:(NSInteger)employeeId_ employeeName:(NSString *)employeeName_ refund:(double)refundAmount comment:(NSString *)comment{
    self =[self initWithEmployeeId:employeeId_ employeeName:employeeName_ createdDate:[NSDate date] cashInValue:0 cashInType:CashInTypeNone cashOutValue:refundAmount cashOutType:CashOutTypeRefund  comment:comment type:@"Refund"];
    return self;
}
-(instancetype)initWithJSON:(id)jsonObject{
    if (self=[super init]) {
        NSDictionary *json=(NSDictionary*)jsonObject;
        _employeeId=[json[tkKeyEmployeeId] integerValue];
        _employeeName=json[@"employeeName"];
        _createdDate=[CBLJSON dateWithJSONObject:json[tkKeyCreatedDate]];
        _cashInType=(CashInType)[json[@"cashInType"] integerValue];
        _cashInValue=[json[@"cashInValue"] doubleValue];
        _cashOutType=(CashOutType)[json[@"cashOutType"] integerValue];
        _cashOutValue=[json[@"cashOutValue"] doubleValue];
        _comment=json[@"comment"];
        _type=json[@"type"];
    }
    return self;
}
-(id)encodeAsJSON{
    return @{tkKeyEmployeeId:@(_employeeId),
             @"employeeName":_employeeName,
             tkKeyCreatedDate:[CBLJSON JSONObjectWithDate:_createdDate],
             @"cashInType":@(_cashInType),
             @"cashInValue":@(_cashInValue),
             @"cashOutType":@(_cashOutType),
             @"cashOutValue":@(_cashOutValue),
             @"comment":_comment,
             @"type":_type};
}
@end

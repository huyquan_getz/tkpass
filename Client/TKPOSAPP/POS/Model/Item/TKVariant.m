//
//  TKVariant.m
//  POS
//
//  Created by Nha Duong Cong on 12/19/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "TKVariant.h"
#import "Constant.h"
#import "TKAmountDisplay.h"

@implementation TKVariant
+(double)getPrice:(NSDictionary *)variant{
    return [TKAmountDisplay tkRoundCash:[variant[@"price"] doubleValue]];
}
+(NSInteger)getQuantity:(NSDictionary *)variant{
    return [variant[@"quantity"] integerValue];
}
+(NSInteger)getRemindStock:(NSDictionary *)variant{
    return [variant[@"remindStock"] integerValue];
}
+(BOOL)isWarningQuantity:(NSDictionary *)variant{
    if ([self getQuantity:variant] <0 || [self getQuantity:variant] > [self getRemindStock:variant]) {   // if don't track quantity or quantity large than mini stock
        return NO;
    }else{
        return YES;
    }
}
+(NSString *)getSKU:(NSDictionary *)variant{
    return variant[tkKeySKU];
}
@end

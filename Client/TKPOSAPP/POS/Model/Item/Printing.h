//
//  Printing.h
//  POS
//
//  Created by Cong Nha Duong on 1/9/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
typedef enum _PrintingType{
    PrintingTypeNone=-1,
    PrintingTypeReceipt=0,
    PrintingTypeReceiptCopy=1,
    PrintingTypeSingleOrderChit=2,
    PrintingTypeTotalOrderChit=3
}PrintingType;

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface Printing : NSObject
+(NSArray*)getListPrintingTypeDefault;
+(NSString*)getPrintingDisplayWithPrintingType:(PrintingType)type;

+(CBLDocument*)createPrintingWithType:(PrintingType)type printer:(CBLDocument*)printer listCategory:(NSArray*)listCategory combine:(BOOL)combine;
+(CBLDocument*)updatePrinting:(CBLDocument*)printing withType:(PrintingType)type printer:(CBLDocument*)printer listCategory:(NSArray*)listCategory combine:(BOOL)combine;
+(PrintingType)getPrintingType:(CBLDocument*)document;
+(NSString*)getPrintingDisplay:(CBLDocument*)document;
+(NSString *)getPrinterIpAddress:(CBLDocument *)document;
+(BOOL)getCombine:(CBLDocument*)document;
+(CBLDocument*)getPrinter:(CBLDocument*)document;
+(NSArray*)getArrayCategoryId:(CBLDocument*)document;
+(NSString*)getPrinterNameAssign:(CBLDocument*)document;
+(void)removePrinting:(CBLDocument*)document;
+(void)printingAfterPayment:(CBLDocument*)orderDocumentPaymented;
@end

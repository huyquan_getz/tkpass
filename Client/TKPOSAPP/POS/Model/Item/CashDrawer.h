//
//  CashDrawer.h
//  POS
//
//  Created by Cong Nha Duong on 1/10/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface CashDrawer : NSObject{
    CBLDocument *currentCashDrawer;
}
+(instancetype)sharedCashDrawer;
-(void)updateCashDrawerWithName:(NSString*)cashDrawerName ip:(NSString*)ipAddress port:(NSInteger)port;
-(NSString*)getCashDrawerName;
-(NSString*)getIpAddress;
-(NSInteger)getPort; // not have port asign -1
@end

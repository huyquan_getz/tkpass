//
//  TKDiscount.m
//  POS
//
//  Created by Cong Nha Duong on 2/4/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#import "TKDiscount.h"
#import "Constant.h"

@implementation TKDiscount
@synthesize discountID;
@synthesize name;
@synthesize type;
@synthesize discountValue;
@synthesize beginDate;
@synthesize endDate;
@synthesize neverExpired;
-(instancetype)initFromJsonData:(NSDictionary *)jsonData{
    if (self=[super init]) {
        discountID=jsonData[@"discountID"];
        name=jsonData[@"name"];
        neverExpired=[jsonData[@"neverExpired"] boolValue];
        type=[jsonData[@"type"] integerValue];
        discountValue=[jsonData[@"discountValue"] doubleValue];
        beginDate=[CBLJSON dateWithJSONObject:jsonData[@"beginDate"]];
        endDate=[CBLJSON dateWithJSONObject:jsonData[@"endDate"]];
        if (jsonData[tkKeyCreatedDate]) {
            createdDate=[CBLJSON dateWithJSONObject:jsonData[tkKeyCreatedDate]];
        }
        if (jsonData[tkKeyModifiedDate]) {
            modifiedDate=[CBLJSON dateWithJSONObject:jsonData[tkKeyModifiedDate]];
        }
    }
    return self;
}
-(instancetype)initWithID:(NSString *)disID name:(NSString *)name_ type:(DiscountType)type_ discountValue:(double)value_ noLimitTime:(BOOL)noLimit beginDate:(NSDate *)beginDate_ endDate:(NSDate *)endDate_{
    if (self=[super init]) {
        discountID=disID;
        name=name_;
        type=type_;
        discountValue=value_;
        neverExpired=noLimit;
        beginDate=beginDate_;
        endDate=endDate_;
        createdDate=[NSDate date];
        modifiedDate=createdDate;
    }
    return self;
}
-(NSDictionary *)jsonValue{
    NSMutableDictionary *dict=[@{@"discountID":discountID,@"name":name,@"type":@(type),@"discountValue":@(discountValue),@"beginDate":[CBLJSON JSONObjectWithDate:beginDate],@"endDate":[CBLJSON JSONObjectWithDate:endDate],@"neverExpired":@(neverExpired)}mutableCopy];
    if (beginDate) {
        [dict setObject:[CBLJSON JSONObjectWithDate:beginDate] forKey:@"beginDate"];
    }else{
        [dict setObject:[NSNull null] forKey:@"beginDate"];
    }
    if (endDate) {
        [dict setObject:[CBLJSON JSONObjectWithDate:endDate] forKey:@"endDate"];
    }else{
        [dict setObject:[NSNull null] forKey:@"endDate"];
    }
    
    if (createdDate) {
        [dict setObject:[CBLJSON JSONObjectWithDate:createdDate] forKey:tkKeyCreatedDate];
    }
    if (modifiedDate) {
        [dict setObject:[CBLJSON JSONObjectWithDate:modifiedDate] forKey:tkKeyModifiedDate];
    }
    return dict;
    
}
-(BOOL)expiredTime{
    if (neverExpired) {
        return NO;
    }
    NSDate *currentDate=[NSDate date];
    if ([currentDate compare:beginDate]!=NSOrderedDescending) {
        return YES;
    }
    if ([currentDate compare:endDate] !=NSOrderedAscending) {
        return YES;
    }
    return NO;
}
@end

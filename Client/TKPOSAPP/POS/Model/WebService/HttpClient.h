//
//  HttpClient.h
//  POS
//
//  Created by Nha Duong Cong on 10/21/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
#define tkKeyReturnStatus @"status"
#define tkKeyResult @"result"
#define tkKeyMessage @"message"
#define tkKeyResponseString @"responseString"
#define tkKeyException @"exception"

typedef enum _TypeReturnCode{
    ReturnCodeSuccess = 1,//user hight layer, when exactly success
    ReturnCodeFail = 0, // user hight layer, when exactly fail
    ReturnCodeSuccessNotUpdate =2,
    ReturnCodeDataResponseEmpty = -1, //RESPONSE HAVE RESPONSE STRING NIL OR EMPTY
    ReturnCodeParseStringError = -2,  //ERROR WHEN PARSE RESPONSE TRING TO JSON_OBJECT
    ReturnCodeExceiption = -3, //ERROR EXCEPTION WHEN REQUEST
    ReturnCodeRequestError =-4, //ERROR REQUEST ERROR
    ReturnCodeResponseInvalid =-5, // RESPONSE NOT FORMAT IN RULE example valid JSONObjct: {returnCode:123,key1:value1} request have returnCode;
    
    ReturnCodeAccessTokenExpired= -1050,
    ReturnCodeAccessTokenNotProvided = -1051,
    ReturnCodeUserPassInvalid = -1002,
    ReturnCodeBusinessNotExist= -1100
}TypeReturnCode;

#import <Foundation/Foundation.h>

@interface HttpClient : NSObject{
    NSString *oldTokenAuthentication;
}
@property (assign)int timeoutDefault;
+(HttpClient*) sharedHttpClient;

- (NSDictionary*)requestWithURL:(NSString*)url method:(NSString*)method parameters:(NSDictionary*)parameters headers:(NSDictionary*)headers timeout:(int)timeout;
- (NSDictionary*)postRequestWithURL:(NSString*)url parameters:(NSDictionary*)parameters headers:(NSDictionary*)headers timeout:(int)timeout;
- (NSDictionary*)postRequestWithURL:(NSString*)url parameters:(NSDictionary*)parameters headers:(NSDictionary*)headers;
-(NSDictionary *)headerAuthenticationBasicWithUsername:(NSString *)username password:(NSString *)password;
@end

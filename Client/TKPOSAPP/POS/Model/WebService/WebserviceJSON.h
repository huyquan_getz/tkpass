//
//  WebserviceJSON.h
//  POS
//
//  Created by Nha Duong Cong on 10/16/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EmailReceipt.h"

@interface WebserviceJSON : NSObject
+(NSDictionary*)authenticationWithBusinessName:(NSString*)businessName username:(NSString*)username password:(NSString*)password;
+(NSDictionary*)getTokenWithBusinessName:(NSString*)businessName username:(NSString*)username password:(NSString*)password;
+(NSDictionary*)sendReceipt:(NSDictionary*)receipt toEmail:(NSString*)email type:(MailType)mailType;
@end

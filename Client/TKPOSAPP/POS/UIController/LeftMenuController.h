//
//  LeftMenuController.h
//  POS
//
//  Created by Nha Duong Cong on 10/14/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
typedef enum CellLeftMenuType{
    CellLeftMenuCashier=0,
    CellLeftMenuActivities=1,// Receipt
    CellLeftMenuCashManagement=2,
    CellLeftMenuCustomerMgt=3,
    CellLeftMenuEmployeeMgt=4,
    CellLeftMenuTimeClock=5,
    CellLeftMenuSetting=6,//5
}CellLeftMenuType;
#import <UIKit/UIKit.h>

@interface LeftMenuController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    CellLeftMenuType typeCellSelected;
}
+(instancetype)sharedLeftMenu;
@property (weak, nonatomic) IBOutlet UIButton *btLock;
@property (weak, nonatomic) IBOutlet UITableView *tbvMain;
@property (weak, nonatomic) IBOutlet UIView *vBgNavi;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
- (IBAction)clickLock:(id)sender;
-(void)setSelectedCellType:(CellLeftMenuType)celltype withAction:(BOOL)action;


@end

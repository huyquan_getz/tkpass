//
//  SlideMenuController.m
//  POS
//
//  Created by Nha Duong Cong on 10/15/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "SlideMenuController.h"

@interface SlideMenuController ()

@end

@implementation SlideMenuController

//override
-(id)initWithCenterViewController:(UIViewController *)centerViewController leftDrawerViewController:(UIViewController *)leftDrawerViewController{
    UINavigationController *naviCenter;
    if ([centerViewController isKindOfClass:[UINavigationController class]]) {
        naviCenter=(UINavigationController*)centerViewController;
    }else{
        naviCenter=[[UINavigationController alloc] initWithRootViewController:centerViewController];
    }
    if (self=[super initWithCenterViewController:naviCenter leftDrawerViewController:leftDrawerViewController]) {
        //
    }
    naviCenter.navigationBarHidden=YES;
    return self;
}
/* setCenterViewController with UIViewController, if parameter not UINavi then create navi with rootViewController is parameter */
//override
-(void)setCenterViewController:(UIViewController *)newCenterViewController withFullCloseAnimation:(BOOL)fullCloseAnimated completion:(void (^)(BOOL))completion{
    
    UINavigationController *naviCenter;
    if ([newCenterViewController isKindOfClass:[UINavigationController class]]) {
        naviCenter=(UINavigationController*)newCenterViewController;
    }else{
        naviCenter=[[UINavigationController alloc] initWithRootViewController:newCenterViewController];
    }
    naviCenter.navigationBarHidden=YES;
    [super setCenterViewController:naviCenter withFullCloseAnimation:fullCloseAnimated completion:completion];
}
 /* setCenterViewController with UIViewController, if parameter not UINavi then create navi with rootViewController is parameter */
//override
-(void)setCenterViewController:(UIViewController *)centerViewController withCloseAnimation:(BOOL)closeAnimated completion:(void (^)(BOOL))completion{
    UINavigationController *naviCenter;
    if ([centerViewController isKindOfClass:[UINavigationController class]]) {
        naviCenter=(UINavigationController*)centerViewController;
    }else{
        naviCenter=[[UINavigationController alloc] initWithRootViewController:centerViewController];
    }
    naviCenter.navigationBarHidden=YES;
    [super setCenterViewController:naviCenter withCloseAnimation:closeAnimated completion:completion];
}
 /* setCenterViewController with list UIViewController, create navi and assing viewControllers with parammeter */
//new
-(void)setCenterViewControllers:(NSArray *)centerViewControllers withCloseAnimation:(BOOL)closeAnimated completion:(void (^)(BOOL))completion{
    UINavigationController *naviCenter=[[UINavigationController alloc] init];
    [naviCenter setViewControllers:centerViewControllers];
    naviCenter.navigationBarHidden=YES;
    [super setCenterViewController:naviCenter withCloseAnimation:closeAnimated completion:completion];
}
 /* setCenterViewController with list UIViewController, create navi and assing viewControllers with parammeter */
//new
-(void)setCenterViewControllers:(NSArray *)newCenterViewControllers withFullCloseAnimation:(BOOL)fullCloseAnimated completion:(void (^)(BOOL))completion{
    UINavigationController *naviCenter=[[UINavigationController alloc] init];
    [naviCenter setViewControllers:newCenterViewControllers];
    naviCenter.navigationBarHidden=YES;
    [super setCenterViewController:naviCenter withFullCloseAnimation:fullCloseAnimated completion:completion];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

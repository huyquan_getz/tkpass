//
//  UtilBusinessModel.m
//  POS
//
//  Created by Nha Duong Cong on 11/1/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//

#import "UtilBusinessModel.h"
#import "UserDefaultModel.h"
#import "NSString+Util.h"
#import "Constant.h"

@implementation UtilBusinessModel
+(NSString *)stringUrlSyncData{
    NSDictionary *dicSync=[UserDefaultModel getMerchantSyncInfor];
    if (dicSync) {
        NSString *server=dicSync[@"serverSync"];
        NSString *databaseName=dicSync[@"databaseSync"];
        NSString *portSync=dicSync[@"portSync"];
        NSString *urlString=[NSString stringWithFormat:@"%@:%@/%@",server,portSync,databaseName];
        return urlString;
    }else{
        return nil;
    }
}
+(id<CBLAuthenticator>)authenticationBasic{
    NSDictionary *dicSync=[UserDefaultModel getMerchantSyncInfor];
    if (dicSync) {
        NSString *username=dicSync[@"userAccessSync"];
        NSString *password=dicSync[@"passAccessSync"];
        id<CBLAuthenticator> auth=[CBLAuthenticator basicAuthenticatorWithName:username password:password];
        return auth;
    }else{
        return nil;
    }
}
+(NSArray*)channelsSync{
    NSDictionary *dicSync=[UserDefaultModel getMerchantSyncInfor];
    if (dicSync) {
        return dicSync[tkKeyChannels];
    }
    return nil;
}
+(void)addChannels:(NSArray *)channelsAdd{
    NSDictionary *dictSync=[UserDefaultModel getMerchantSyncInfor];
    if (dictSync) {
        NSMutableDictionary *newDict=[dictSync mutableCopy];
        NSMutableArray *channels =[newDict[tkKeyChannels] mutableCopy];
        [channels addObjectsFromArray:channelsAdd];
        [newDict setObject:channels forKey:tkKeyChannels];
        [UserDefaultModel saveMerchantSyncInfor:newDict];
    }
}
@end

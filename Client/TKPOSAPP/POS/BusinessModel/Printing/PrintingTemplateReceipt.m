//
//  PrintingTemplateReceipt.m
//  POS
//
//  Created by Cong Nha Duong on 1/15/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//
#define ColorLine [UIColor blackColor]
#define HeightSpace 10
#import "PrintingTemplateReceipt.h"
#import "Constant.h"
#import "Controller.h"
#import "TKInventory.h"
#import "TKGeneralSetting.h"

@interface PrintingTemplateReceipt ()

@end

@implementation PrintingTemplateReceipt
-(instancetype)initWithLoadView{
    if (self =[super init]) {
        [self loadView];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)clearDataStatic{
    _topNameStore.text=@"";
    _topAddress.text=@"";
    _topTel.text=@"";
    _topRegNumber.text=@"";
    
    _infoTitleDate.text=@"";
    _infoDate.text=@"";
    _infoTitleStaff.text=@"";
    _infoTitleOrderId.text=@"";;
    _infoOrderId.text=@"";
    
    _bottomName.text=@"";
    _bottomThank.text=@"";
    
    _lbTotal.text=@"";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setValueTopViewDefault{
    CBLDocument *store=[Controller getStoreInfoDoc];
    CBLDocument *merchant=[Controller getGeneralSetting];
    _topNameStore.text=[TKInventory getName:store];
    _topAddress.text=[TKInventory getAddressCombine:store];
    _topTel.text=[NSString stringWithFormat:@"Tel: %@",[TKInventory getContact:store]];
    _topRegNumber.text=[NSString stringWithFormat:@"GST Reg. No.: %@",([TKGeneralSetting getGST:merchant]==nil)?@"":[TKGeneralSetting getGST:merchant]];
}
-(void)addTopViewAt:(CGPoint*)startPoint{
    [self addView:_vTopView at:startPoint];
}
-(void)addTopViewCrudeAt:(CGPoint *)startPoint{ // same TopView but not have Reg Number
    CGRect frame=_vTopView.frame;
    frame.size.height=103;
    _vTopView.frame=frame;
    [self addView:_vTopView at:startPoint];
}
-(void)addInfoViewAt:(CGPoint*)startPoint{
    [self addView:_vInfoView at:startPoint];
}
-(void)addBottomViewAt:(CGPoint*)startPoint{
    [self addView:_vBottomView at:startPoint];
}
-(void)addRefundViewAt:(CGPoint *)startPoint{
    [self addView:_vRefundView at:startPoint];
}
-(void)addView:(UIView *)cView at:(CGPoint *)startPoint{
    [self addView:cView at:startPoint staticPoint:NO];
}
-(void)addView:(UIView *)cView at:(CGPoint *)startPoint staticPoint:(BOOL)staticPoint{
    CGRect frame=cView.frame;
    frame.origin=(*startPoint);
    frame.size.width=self.view.frame.size.width-(*startPoint).x;
    cView.frame=frame;
    [self.view addSubview:cView];
    if (staticPoint==NO) {
        (*startPoint).y+= cView.frame.size.height;
    }
}
-(void)addSpaceDefaultAt:(CGPoint *)startPoint loop:(NSUInteger)loop{
    (*startPoint).y+=(HeightSpace*loop);
}
-(void)addSpaceDefaultAt:(CGPoint *)startPoint{
    [self addSpaceDefaultAt:startPoint loop:1];
}
-(void)addLineAt:(CGPoint *)startPoint center:(BOOL)center{
    CGRect frame=CGRectZero;
    frame.origin=(*startPoint);
    if (center) {
        frame.size.width=self.view.frame.size.width-2*(*startPoint).x;
    }else{
        frame.size.width=self.view.frame.size.width-  (*startPoint).x;
    }
    frame.size.height=1;
    UIView *line=[[UIView alloc] initWithFrame:frame];
    line.autoresizingMask=UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
    line.contentMode=UIViewContentModeScaleToFill;
    line.backgroundColor=ColorLine;
    line.frame=frame;
    [self.view addSubview:line];
    (*startPoint).y+=line.frame.size.height;
}
-(void)addTotalAt:(CGPoint *)startPoint{
    CGRect frame=_lbTotal.frame;
    frame.origin=(*startPoint);
    frame.size.width=self.view.frame.size.width-(*startPoint).x;
    _lbTotal.frame=frame;
    [self.view addSubview:_lbTotal];
    (*startPoint).y+= _lbTotal.frame.size.height;
}
-(UILabel *)labelDefault{
    UILabel *lb=[[UILabel alloc] init];
    lb.backgroundColor=[UIColor clearColor];
    lb.textColor=[UIColor blackColor];
    lb.font=tkFontMainWithSize(22);
    lb.frame=CGRectMake(0, 0, self.view.frame.size.width, 25);
    return lb;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  PrintingTemplateReceipt.h
//  POS
//
//  Created by Cong Nha Duong on 1/15/15.
//  Copyright (c) 2015 Nha Duong Cong. All rights reserved.
//

/*
 Size Text:  Helvetica Neue 22.0
 
 */

#import <UIKit/UIKit.h>

@interface PrintingTemplateReceipt : UIViewController
@property (strong, nonatomic) IBOutlet UIView *vTopView;
@property (strong, nonatomic) IBOutlet UIView *vInfoView;
@property (strong, nonatomic) IBOutlet UIView *vRefundView;
@property (strong, nonatomic) IBOutlet UIView *vBottomView;
/*
 Top view
 */
@property (weak, nonatomic) IBOutlet UILabel *topNameStore;
@property (weak, nonatomic) IBOutlet UILabel *topAddress;
@property (weak, nonatomic) IBOutlet UILabel *topTel;
@property (weak, nonatomic) IBOutlet UILabel *topRegNumber;
/*
 Top view
 */
//============================
/*
 Info View
 */
@property (weak, nonatomic) IBOutlet UILabel *infoName;
@property (weak, nonatomic) IBOutlet UILabel *infoTitleDate;
@property (weak, nonatomic) IBOutlet UILabel *infoDate;
@property (weak, nonatomic) IBOutlet UILabel *infoTitleStaff;
@property (weak, nonatomic) IBOutlet UILabel *infoStaff;
@property (weak, nonatomic) IBOutlet UILabel *infoTitleOrderId;
@property (weak, nonatomic) IBOutlet UILabel *infoOrderId;

/*
 Info View
 */
//==========================================
/*
 Bottom View
 */
@property (weak, nonatomic) IBOutlet UIView *bottomLineBelowDateTime;
@property (weak, nonatomic) IBOutlet UILabel *bottomName;
@property (weak, nonatomic) IBOutlet UILabel *bottomThank;

/*
 Bottom View
 */
//=============================================
/*
 Totlal View
 */
@property (strong, nonatomic) IBOutlet UILabel *lbTotal;

/*
 Totlal View
 */
//=============================================
/*
 Refund View
 */
@property (weak, nonatomic) IBOutlet UILabel *refundName;
@property (weak, nonatomic) IBOutlet UILabel *refundTitleReason;
@property (weak, nonatomic) IBOutlet UILabel *refundReason;
@property (weak, nonatomic) IBOutlet UILabel *refundTitleDate;
@property (weak, nonatomic) IBOutlet UILabel *refundDate;
@property (weak, nonatomic) IBOutlet UILabel *refundTitleStaff;
@property (weak, nonatomic) IBOutlet UILabel *refundStaff;


/*
 Refund View
 */
//====================

-(instancetype)initWithLoadView;
-(void)setValueTopViewDefault;
-(void)addTopViewAt:(CGPoint*)startPoint;
-(void)addTopViewCrudeAt:(CGPoint*)startPoint; // no Reg Number
-(void)addInfoViewAt:(CGPoint*)startPoint;
-(void)addBottomViewAt:(CGPoint*)startPoint;
-(void)addRefundViewAt:(CGPoint*)startPoint;
-(void)addView:(UIView*)cView at:(CGPoint*)startPoint;
-(void)addView:(UIView *)cView at:(CGPoint *)startPoint staticPoint:(BOOL)staticPoint;
-(void)addSpaceDefaultAt:(CGPoint*)startPoint;
-(void)addSpaceDefaultAt:(CGPoint*)startPoint loop:(NSUInteger)loop;
-(void)addLineAt:(CGPoint*)startPoint center:(BOOL)center;
-(void)addTotalAt:(CGPoint*)startPoint;

-(UILabel*)labelDefault;
@end

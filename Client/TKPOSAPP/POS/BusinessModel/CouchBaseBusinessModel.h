//
//  CouchBaseBusinessModel.h
//  POS
//
//  Created by Nha Duong Cong on 10/15/14 .
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
// 123 3
#import "CouchbaseViewController.h"
#import <Foundation/Foundation.h>
#import <CouchbaseLite/CouchbaseLite.h>

@interface CouchBaseBusinessModel : NSObject
+(CBLQuery*)querySearchEmployeePinCode:(NSString*)titleSearch;
+(CBLQuery*)queryGetAllCategory;
+(CBLQuery*)queryAllProductByCategoryIDAndSortKeyMain;
+(CBLQuery*)queryAllProductSortKeyMain;
+(CBLQuery*)querySearchProductItemWithTitleKey:(NSString*)titleSearch;
+(CBLQuery*)querySearchProductBySKU:(NSString*)sku;

+(CBLQuery *)queryTKGetReceiptLimitDateInMonth:(NSInteger)numberMonthBefore; // limit 3month or 90 date
+(CBLQuery*)querySearchReceiptWithTitleKey:(NSString *)titleSearch;
+(CBLQuery*)queryAllTaxServiceCharge;
+(CBLQuery*)queryAllGeneralSetting;
// both local and server
+(CBLQuery*)queryGetAllOrderWaiting;

// local
+(CBLQuery*)queryShiftDrawerInYear:(NSInteger)year month:(NSInteger)month;
+(CBLQuery*)queryGetAllPrinter;
+(CBLQuery*)queryGetAllPrinting;

+(CBLDocument*)productWithSKU:(NSString*)sku;
+(CBLDocument*)taxServiceSetting;
+(CBLDocument*)generalSetting;

+(BOOL)reloadView:(ViewType)viewType;
@end

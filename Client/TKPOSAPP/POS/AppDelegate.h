//
//  AppDelegate.h
//  POS
//
//  Created by Nha Duong Cong on 10/13/14.
//  Copyright (c) 2014 Nha Duong Cong. All rights reserved.
//
@class AppDelegate;
#import <UIKit/UIKit.h>
#import "SlideMenuController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic)SlideMenuController *slideMenuController;


+(CGSize)size;
+(CGRect)bounds;
@end

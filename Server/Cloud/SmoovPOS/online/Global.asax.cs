﻿using log4net;
using Microsoft.AspNet.SignalR;
using SmoovPOS.UI;
using SmoovPOS.UI.App_Start;
using SmoovPOS.Utility.Cache;
using SmoovPOS.Utility.WSSiteReference;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Com.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(MvcApplication));

        void Application_Error(Object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError().GetBaseException();

            logger.Error("App_Error", ex);
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleMobileConfig.RegisterBundles(BundleTable.Bundles);
            AutoFacConfig.RegisterContainers();
            AutoMapperConfig.RegisterProfiles();
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;

            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));

            //GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(1);
            //Microsoft.AspNet.SignalR.Transports.LongPollingTransport = 5000;
            //RouteTable.Routes.MapHubs();
        }
        protected void Session_Start(object sender, EventArgs e)
        {
            try
            {
                var siteID = GetSiteIDFromHost();
                if (CacheHelper.Categories != null && CacheHelper.Categories.Where(c => c != null && c.siteID == siteID).Any())
                {
                    CacheHelper.Categories = CacheHelper.Categories.Where(c => c != null && c.siteID != siteID).ToList();
                }
                if (CacheHelper.Collections != null && CacheHelper.Collections.Where(c => c != null && c.siteID == siteID).Any())
                {
                    CacheHelper.Collections = CacheHelper.Collections.Where(c => c != null && c.siteID != siteID).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                CacheHelper.Categories = new List<global::SmoovPOS.Entity.Menu.Menu>() { };
                CacheHelper.Collections = new List<global::SmoovPOS.Entity.Menu.Menu>() { };
            }
        }

        private string GetSiteIDFromHost()
        {
            string host = Request.Url.Host;
            WSSiteClient client = new WSSiteClient();
            var siteAliases = client.WSGetAllSiteAlias();
            string siteID = string.Empty;

            string subdomain = host;

            if (subdomain.IndexOf("://", StringComparison.Ordinal) != -1)
            {
                subdomain = subdomain.Remove(0, subdomain.IndexOf("://", StringComparison.Ordinal) + 3);
            }
            if (subdomain.IndexOf("\\\\", StringComparison.Ordinal) != -1)
            {
                subdomain = subdomain.Remove(0, subdomain.IndexOf("\\\\", StringComparison.Ordinal) + 2);
            }

            //subdomain = subdomain.Split('.')[0];

            foreach (var item in siteAliases)
            {
                if (item.CheckSubdomain == true)
                {
                    if (!string.IsNullOrEmpty(item.HTTPAlias) && item.HTTPAlias.Contains(subdomain))
                    {
                        siteID = item.SiteID;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(item.HTTP) && item.HTTP.Contains(subdomain))
                    {
                        siteID = item.SiteID;
                    }
                }
            }

            client.Close();

            return siteID;
        }
    }
}

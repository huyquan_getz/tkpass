﻿(function ($) {

    // replacer RegExp
    var dateISO = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:[.,]\d+)?Z/i;
    //var dateNet = /\/Date\((\d+)(?:[-\+]\d+)?\)\//i;
    var dateNet = /\/Date\((-?\d+)\)\//

    var jsonDateConverter = function (value) {
        if (typeof (value) == "string") {
            if (dateISO.test(value)) {
                return new Date(value);
            }
            if (dateNet.test(value)) {
                return new Date(parseInt(dateNet.exec(value)[1], 10));
            }
        }
        return value;
    };

    var converter = function (json) {
        $.each(json, function (k, v) {
            if (typeof v == 'object') {
                // recurse
                if (v != null) {
                    converter(v);
                }
            }
            else {
                json[k] = jsonDateConverter(v);
            }
        });
        return json;
    };

    $.convertJSONDates = converter;

})(jQuery);
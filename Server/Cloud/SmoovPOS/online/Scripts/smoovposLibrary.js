﻿$(document).ready(function () {
    $('#q-search').mouseover(function () {
        $(".q-menu-search").css("display", "block");
    });
    $('.q-menu-search').mouseleave(function () {
        $(".q-menu-search").css("display", "none");
    });
    $('#q-menu').mouseleave(function () {
        $(".q-menu-search").css("display", "none");
    });
});

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function CalculateDimensions(sourceWidth, sourceHeight, maxWidth, maxHeight) {
    var widthPercent = maxWidth / sourceWidth;
    var heightPercent = maxHeight / sourceHeight;

    var percent = widthPercent;
    if (heightPercent < widthPercent && heightPercent != 0) {
        percent = heightPercent;
    }

    var destWidth = (sourceWidth * percent);
    var destHeight = (sourceHeight * percent);

    var size = new Array(2);
    size[0] = destWidth;
    size[1] = destHeight;

    return size;
}

function AutoResizeWidthHeightImage(imageItem, elementResizeId, maxWidth, maxHeight) {
    var sourceWidth = imageItem.width();
    var sourceHeight = imageItem.height();

    var size = CalculateDimensions(sourceWidth, sourceHeight, maxWidth, maxHeight);
    var destWidth = size[0];
    var destHeight = size[1];

    elementResize = $('#' + elementResizeId);
    elementResize.removeAttr('width');
    elementResize.removeAttr('height');
    elementResize.attr('width', destWidth);
    elementResize.attr('height', destHeight);
    elementResize.css('Width', destWidth.toFixed(0).toString() + 'px');
    elementResize.css('Height', destHeight.toFixed(0).toString() + 'px');
}

function SetAutoResizeWidthHeightImage(elementResizeId, maxWidth, maxHeight) {
    var imageItem = $('#' + elementResizeId);
    var sourceWidth = imageItem.width();
    var sourceHeight = imageItem.height();
    if (sourceWidth > 0 && sourceHeight > 0) {
        AutoResizeWidthHeightImage(imageItem, elementResizeId, maxWidth, maxHeight);
    }
}
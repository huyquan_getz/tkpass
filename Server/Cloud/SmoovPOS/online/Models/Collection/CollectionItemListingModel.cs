﻿using SmoovPOS.Entity.Menu;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace online.Models
{
    public class CollectionItemListingModel
    {
        public IEnumerable<Menu> CollectionList { get; set; }
        public IEnumerable<Com.SmoovPOS.Entity.ProductItem> ProductItemList { get; set; }
        public EntityPaging Paging { get; set; }

        public string Currency { get; set; }
        public Com.SmoovPOS.Entity.Collection Collection { get; set; }
    }
}
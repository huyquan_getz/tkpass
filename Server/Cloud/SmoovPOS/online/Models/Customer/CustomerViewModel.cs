﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Customer
{
    public class CustomerViewModel
    {
        public string _id { get; set; }

        public bool acceptsNewsletter { get; set; }

        public bool acceptsSms { get; set; }

        [LocalizedDisplayName("Address1")]
        public string address { get; set; }

        public Country arrCountry { get; set; }

        public string birthday { get; set; }

        public string city { get; set; }

        public string country { get; set; }

        public bool display { get; set; }

        [LocalizedDisplayName("email_Title")]
        public string email { get; set; }

        [LocalizedDisplayName("FirstName")]
        public string firstName { get; set; }

        [LocalizedDisplayName("fullName")]
        public string fullName { get; set; }

        public int gender { get; set; }

        [LocalizedDisplayName("LastName")]
        public string lastName { get; set; }

        public Dictionary<string, string> listDefaultTimeZones { get; set; }

        public string state { get; set; }
        
        public Boolean status { get; set; }

        public string timeZoneFullName { get; set; }

        public string password { get; set; }

        public string passwordConfirm { get; set; }

        [LocalizedDisplayName("phone")]
        public string phone { get; set; }

        public string phoneRegion { get; set; }

        public string zipCode { get; set; }
    }
}
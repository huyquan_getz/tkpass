﻿using SmoovPOS.Entity.Models;
using SmoovPOS.Entity.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.TableOrder
{
    public class TableOrderPaymentViewModel
    {
        public TableOrderModel tableOrderModel { get; set; }
        public AccountSmoovpay accountSmoovPay { get; set; }
        public string transactionId { get; set; }
    }

}
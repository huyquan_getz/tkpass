﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class TableOrderViewModel
    {
        //Station Info
        public string id { get; set; }
        public string branchName { get; set; }
        public int branchIndex { get; set; }
        public string stationName { get; set; }
        public int stationSize { get; set; }
        public string stationQRCode { get; set; }
        public int statusOrdering { get; set; }
        public string currency { get; set; }
        public bool isHost { get; set; }
        public bool isStartOrder { get; set; }
        public string TitleHostOrClient
        {
            get
            {
                if (isHost)
                {
                    return ConstantSmoovs.StationTable.Host;
                }

                return ConstantSmoovs.StationTable.Client;
            }
        }


        //Customer Info
        public string memberID { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("FirstName")]
        public string firstName { get; set; }
        //[LocalizedRequired]
        [LocalizedDisplayName("LastName")]
        public string lastName { get; set; }
        public string fullName { get; set; }
        [LocalizedRequired]
        [RegularExpression(@"(^(\+?\-? *[0-9]+)([0-9 ]*)([0-9 ])*$)|(^ *$)", ErrorMessage = "Please input number only.")]
        public string phoneRegion { get; set; }
        [LocalizedRequired]
        [RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        [LocalizedDisplayName("PhoneNumber")]
        public string phone { get; set; }
        [LocalizedRequired]
        [EmailAddress]
        [LocalizedDisplayName("email_Title")]
        public string email { get; set; }

        public string remark { get; set; }
        //Order Info
        public int countOfItemsInCart { get; set; }
        public string orderId { get; set; }
        public List<ProductItem> listProductItems { get; set; }
        public ReceiptValue receiptValue { get; set; } //Receipt value of member's cart
        public string timeZone { get; set; }
        public string ref_id { get; set; }
        public string paymentDoneDate { get; set; }
        public string businessName { get; set; }

    }
}
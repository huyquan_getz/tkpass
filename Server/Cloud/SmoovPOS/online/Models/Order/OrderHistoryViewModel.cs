﻿using Com.SmoovPOS.TimeZone;
using SmoovPOS.Common;
using System;
using System.Text;
using System.Linq;

namespace SmoovPOS.UI.Models.Order
{
    public class OrderHistoryViewModel : Com.SmoovPOS.Entity.Order
    {
        public OrderHistoryViewModel() { } 

        public OrderHistoryViewModel(Com.SmoovPOS.Entity.Order order, string siteID, string timezone)
        {
            _id = !string.IsNullOrWhiteSpace(order._id) ? order._id : Guid.NewGuid().ToString();
            orderType = order.orderType;
            orderCode = order.orderCode;
            orderStatus = order.orderStatus;
            isCancelRequest = order.isCancelRequest;

            customerSender = order.customerSender;
            customerReceiver = order.customerReceiver;

            discount = order.discount;
            subTotal = order.subTotal;
         //   tax = order.tax;
            roundAmount = order.roundAmount;
            grandTotal = order.grandTotal;

            currency = order.currency;
            listTaxvalue = order.listTaxvalue;
            remark = order.remark;

            arrayProduct = order.arrayProduct;
            arrayPayment = order.arrayPayment;
            inventory = order.inventory;

            OrderTimezoneDisplayForCustomer = ConstantSmoovs.Enums.ListTimeZone[timezone];
            createdDate = TimeZoneUtil.ConvertUTCToTimeZone(order.createdDate, timezone, siteID);
            serviceShipping = order.serviceShipping;

            discountCart = order.discountCart;
            discountCartItem = order.discountCartItem;
            discountCartValue = order.discountCartValue;
            arrayAdhocProduct = order.arrayAdhocProduct;
        }

        public string OrderDateDisplayForCustomer
        {
            get
            {
                StringBuilder timeStore = new StringBuilder();

                timeStore.Append(createdDate.ToString("MM/dd/yyyy"));
                timeStore.Append(" ");
                timeStore.Append(createdDate.ToString("HH:mm:ss"));

                return (!string.IsNullOrWhiteSpace(timeStore.ToString()) ? timeStore.ToString() : "");
            }
        }

        public string OrderTotalDisplayForCustomer
        {
            get
            {
                string total = currency + " " + String.Format("{0:0.00}", grandTotal);

                return (!string.IsNullOrWhiteSpace(total) ? total : "");
            }
        }

        public string OrderStatusDisplayForCustomer
        {
            get
            {
                return ConstantSmoovs.Enums.OrderStatusCustomer.ContainsKey(orderStatus) ? ConstantSmoovs.Enums.OrderStatusCustomer[orderStatus] : string.Empty;
            }
        }

        public string OrderTimezoneDisplayForCustomer { get; set; }

        public string ShowInventoryAdrress
        {
            get
            {
                System.Text.StringBuilder addressShow = new System.Text.StringBuilder();
                if (inventory != null)
                {

                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.streetAddress) ? inventory.streetAddress : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.streetAddress) ? ", " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.city) ? inventory.city : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.city) ? ", " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.state) ? inventory.state : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.state) ? " " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.zipCode) ? inventory.zipCode : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.zipCode) ? ", " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.country) ? inventory.country : "");
                }

                return !string.IsNullOrWhiteSpace(addressShow.ToString()) ? addressShow.ToString() : "";
            }
        }

        public double ShippingFeeShow
        {
            get
            {
                double shippingFee = 0.00;
                if (serviceShipping != null && serviceShipping.arrayServiceDelivery != null && serviceShipping.arrayServiceDelivery.Count > 0)
                {
                    shippingFee = serviceShipping.arrayServiceDelivery.FirstOrDefault().arrayAreaShipping.Count() > 0 ? 
                        serviceShipping.arrayServiceDelivery.FirstOrDefault().arrayAreaShipping.FirstOrDefault().areaPrice : serviceShipping.arrayServiceDelivery.FirstOrDefault().shippingPrice;
                }
                return shippingFee;
            }
        }
    }
}
﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace online.Models
{
    public class PaymentModel
    {
        public Country arrCountry { get; set; }
        //public DeliveryCountry arrCountry { get; set; }
        public IEnumerable<Inventory> arrInventory { get; set; }

        public MyCart mycart { get; set; }
        public Com.SmoovPOS.Model.PaymentModel payConfig { get; set; }
        public string transactionId { get; set; }
        public string signature { get; set; }

        public ServiceShipping[] arrServiceShipping { get; set; }
        public Instruction[] arrInstruction { get; set; }
        public string orderId { get; set; }

        public Customer member { get; set; }
        public CheckoutSetting checkoutSetting { get; set; }
        public List<SelectListItem> countrys { get; set; }
        public List<SelectListItem> countrySettings { get; set; }
        public string country { get; set; }
        public string country1 { get; set; }
        public string country2 { get; set; }
        public List<SelectListItem> areas { get; set; }
        public List<SelectListItem> areaSettings { get; set; }
        public string area { get; set; }
        public string area1 { get; set; }
        public List<ServiceDelivery> arrayService { get; set; }
        public AccountSmoovpay accountSmoovPay { get; set; }
    }
}
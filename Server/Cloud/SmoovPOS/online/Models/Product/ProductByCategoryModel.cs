﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Models.Paging;
using System.Collections.Generic;

namespace online.Models
{
    public class ProductByCategoryModel
    {
        public string Category { get; set; }

        public List<ProductItem> ProductItemList { get; set; }

        public EntityPaging Paging { get; set; }

        public string Currency { get; set; }

        public int countOfBestSellerProduct { get; set; }

        public string searchKeyWord { get; set; }

        public int countOfSearchResults { get; set; }
    }
}
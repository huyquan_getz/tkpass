﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Models.Paging;
using System.Collections.Generic;

namespace online.Models
{
    public class SearchProductModel
    {
        public IEnumerable<ProductItem> ProductItemList { get; set; }

        public EntityPaging Paging { get; set; }

        public string Currency { get; set; }
    }
}
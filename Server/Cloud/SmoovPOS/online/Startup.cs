﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Com.UI.Startup))]
namespace Com.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}

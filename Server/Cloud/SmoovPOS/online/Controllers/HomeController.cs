﻿
using SmoovPOS.Utility.Cache;
using System;
using System.Linq;
using System.Web.Mvc;

namespace online.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Chat()
        {
            return View();
        }
        public ActionResult Index(string theme = "", string previewtheme = "")
        {
            if (Session["Online"] == null)
            {
                Session["Online"] = DateTime.Now;
            }
            else
            {
                var date = (DateTime)Session["Online"];
                if ((DateTime.Now - date).TotalMinutes > 30)
                {
                    Session["Online"] = DateTime.Now;

                    var siteID = GetSiteIDFromHost();
                    if (CacheHelper.Categories != null && CacheHelper.Categories.Where(c => c != null && c.siteID == siteID).Any())
                    {
                        CacheHelper.Categories = CacheHelper.Categories.Where(c => c.siteID != siteID).ToList();
                    }
                    if (CacheHelper.Collections != null && CacheHelper.Collections.Where(c => c != null && c.siteID == siteID).Any())
                    {
                        CacheHelper.Collections = CacheHelper.Collections.Where(c => c.siteID != siteID).ToList();
                    }
                }
            }

            ViewBag.Class = "home";
            ViewBag.previewtheme = previewtheme;
            GetThemeInfo(theme);
            return View();
        }

        public ActionResult Home(string theme = "", string previewtheme = "")
        {
            ViewBag.Class = "home";
            ViewBag.previewtheme = previewtheme;
            return View();
        }

        public ActionResult About(string theme = "")
        {
            ViewBag.Class = "home";
            GetThemeInfo();
            return View();
        }

        public ActionResult Contact(string theme = "")
        {
            ViewBag.Class = "home";
            GetThemeInfo();
            return View();
        }
        public ActionResult FAQs(string theme = "")
        {
            ViewBag.Class = "home";
            GetThemeInfo();
            return View();
        }
        public ActionResult ShippingInfo(string theme = "")
        {
            ViewBag.Class = "home";
            GetThemeInfo();
            return View();
        }
        public ActionResult ReturnExchange(string theme = "")
        {
            ViewBag.Class = "home";
            GetThemeInfo();
            return View();
        }
        public ActionResult PrivacyPolicy(string theme = "")
        {
            ViewBag.Class = "home";
            GetThemeInfo();
            return View();
        }
        public ActionResult TermConditions(string theme = "")
        {
            ViewBag.Class = "home";
            GetThemeInfo();
            return View();
        }

        public ActionResult PreviewTheme(string theme)
        {
            ViewBag.Class = "home";
            GetThemeInfo(theme);
            return View();
        }
    }
}
﻿using online.Controllers;
using online.Models;
using SmoovPOS.Utility.WSProductItemReference;
using SmoovPOS.Utility.WSDiscountReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;

namespace SmoovPOS.UI.Controllers
{
    public class DiscountController : BaseController
    {
        // GET: Discount
        public ActionResult Index()
        {
            ViewBag.Class = "discount";
            GetThemeInfo();
            DiscountItemListingModel discount = new DiscountItemListingModel();
            try
            {
                BaseController baseController = new BaseController();
                discount.ProductItemList = baseController.GetAllProductItems(GetSiteID());
            }
            catch (Exception ex)
            {
                string a = ex.Message;
            }
            return View(discount);
        }
    }
}
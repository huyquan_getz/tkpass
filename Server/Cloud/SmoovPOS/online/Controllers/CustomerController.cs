﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.MailServer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using online.Controllers;
using online.Models;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Com.SmoovPOS.UI.Models;
using SmoovPOS.Utility.WSCustomerReference;
using AutoMapper;
using SmoovPOS.UI.Models.Customer;
using SmoovPOS.Utility.IWSEmailTemplateReference;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSMerchantManagementReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Entity.Models;
using Com.SmoovPOS.Model;

namespace SmoovPOS.UI.Controllers
{
    public class CustomerController : BaseController
    {
        /// <summary>
        /// Init authentication using authen.owin
        /// </summary>
        public CustomerController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }

        public CustomerController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        #region Sign in
        private void SignIn(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }
        /// <summary>
        /// end Enit authentication using authen.owin
        /// </summary>

        [HttpPost]
        public int SignIn(string username, string password, Boolean remember)
        {
            GetSiteID();
            WSCustomerClient client = new WSCustomerClient();
            Customer cus = client.WSCheckAccountLogin(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckUserPass, username, VariableConfigController.MD5Hash(password), GetSiteID());

            if (cus != null)
            {
                if (cus.active)
                {
                    if (cus.status)
                    {
                        #region add user to session
                        (new SmoovPOS.Utility.Controllers.UserController()).AddUserSessionOnline(cus);
                        #endregion
                        //Set "remember me" status
                        if (remember)
                        {
                            Response.Cookies["username"].Value = username;
                            Response.Cookies["password"].Value = password;
                            Response.Cookies["remember"].Value = "1";
                            Response.Cookies["remember"].Expires = DateTime.Now.AddDays(1);
                            Response.Cookies["username"].Expires = DateTime.Now.AddDays(1);
                            Response.Cookies["password"].Expires = DateTime.Now.AddDays(1);
                        }
                        else
                        {
                            Response.Cookies["username"].Value = "";
                            Response.Cookies["password"].Value = "";
                            Response.Cookies["remember"].Value = "0";
                            Response.Cookies["remember"].Expires = DateTime.Now.AddDays(1);
                            Response.Cookies["username"].Expires = DateTime.Now.AddDays(1);
                            Response.Cookies["password"].Expires = DateTime.Now.AddDays(1);
                        }
                        cus.lastLogin = DateTime.UtcNow;
                        client.WSUpdateCustomer(cus, GetSiteID());
                        client.Close();

                        //Set flag to sync myCart data
                        Session["isJustLoggedIn"] = true;

                        //success
                        return 1;
                    }
                    else
                    {
                        client.Close();
                        //this account has disable by merchant                 
                        return 3;
                    }

                }
                else
                {
                    client.Close();
                    //success but dont active
                    return 2;
                }

            }
            else
            {
                client.Close();
                //fail!Dont exist
                return 0;
            }
        }

        /// <summary>
        /// Sign out for customer
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>23/12/2014-10:07 PM</createddate>
        /// 
        public ActionResult SignOut(string idProduct = "")
        {
            #region add user to session
            (new SmoovPOS.Utility.Controllers.UserController()).RemoveUserSessionOnline();
            #endregion
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Sign in for customer
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>23/12/2014-10:07 PM</createddate>
        /// 
        public ActionResult SignIn(string idProduct = "")
        {
            GetThemeInfo();
            ViewBag.idProduct = idProduct;
            return View();
        }

        /// <summary>
        /// Forgot password 
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>26/12/2014-10:07 PM</createddate>
        /// 
        public ActionResult ForgotPassword()
        {
            GetThemeInfo();
            return View();
        }
        #endregion

        #region Sign up
        /// <summary>
        /// Sign up for customer
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>23/12/2014-10:07 PM</createddate>
        /// 
        public ActionResult SignUp()
        {
            GetThemeInfo();
            return View();
        }

        /// <summary>
        /// Sign up for customer
        /// </summary>
        /// <param>FormCollection</param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>24/12/2014-10:07 PM</createddate>
        /// 
        [HttpPost]
        public ActionResult SignUp(FormCollection collection)
        {
            GetThemeInfo();

            Customer cus = new Customer();

            WSCustomerClient clientCus = new WSCustomerClient();
            cus = clientCus.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, collection["email"], GetSiteID());
            if (cus == null)
            {
                cus = CreateNewMember(collection);
            }
            else
            {
                cus = UpdateGuestMember(collection, cus);
            }
            clientCus.Close();
            string storeUrl = Request.Url.Host;

            //send email verify account
            IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
            var siteID = GetSiteID();
            var modelMerchant = clientMerchant.WSGetMerchantManagementBySiteId(siteID);
            clientMerchant.Close();

            CreateASPUser(cus, collection["password"].ToString());

            SendMailVerify(cus, storeUrl, modelMerchant != null ? (!string.IsNullOrWhiteSpace(modelMerchant.businessName) ? modelMerchant.businessName : "") : "");

            return RedirectToAction("Success");
        }

        private void CreateASPUser(Customer cus, string password)
        {
            #region create asp user

            string userName = cus.email.ToLower().Trim();
            var user = UserManager.FindByEmail(userName);
            if (user == null)
            {
                ApplicationDbContext _db = new ApplicationDbContext();
                user = new ApplicationUser() { UserName = userName, Email = userName, Id = cus._id };

                var resultStatus = UserManager.Create(user, password);

                if (resultStatus.Succeeded)
                {
                    //assign user to role merchant
                    ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));

                    _db.AddUserToRole(userManager, user.Id, ConstantSmoovs.RoleNames.Member);
                }
            }
            else
            {
                UserManager.RemovePassword(user.Id);
                UserManager.AddPassword(user.Id, password);
            }
            #endregion
        }

        private Customer CreateNewMember(FormCollection collection)
        {
            Customer cus = new Customer();
            GetCustomerFromFormCollection(collection, cus);
            cus._id = Guid.NewGuid().ToString();

            cus.status = false;
            cus.active = false;
            long dateTimeInt = DateTime.Today.Ticks;
            cus.memberID = "onl-" + (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();

            cus.pinCode = Guid.NewGuid().ToString();
            cus.acceptsNewsletter = true;
            cus.acceptsSms = true;
            cus.gender = 2;

            WSCustomerClient client = new WSCustomerClient();
            int statusCode = client.WSCreateCustomer(cus, ViewBag.siteID);
            client.Close();

            return cus;
        }

        private Customer UpdateGuestMember(FormCollection collection, Customer cus)
        {
            GetCustomerFromFormCollection(collection, cus);

            WSCustomerClient client = new WSCustomerClient();
            int statusCode = client.WSUpdateCustomer(cus, ViewBag.siteID);
            client.Close();

            return cus;
        }

        private void GetCustomerFromFormCollection(FormCollection collection, Customer cus)
        {
            cus.createdDate = DateTime.UtcNow;
            cus.modifiedDate = DateTime.UtcNow;
            cus.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            cus.firstName = collection["firstName"];
            cus.lastName = collection["lastName"];
            cus.fullName = cus.firstName + " " + cus.lastName;
            cus.email = collection["email"];
            cus.password = VariableConfigController.MD5Hash(collection["password"]);
            cus.passwordConfirm = VariableConfigController.MD5Hash(collection["password"]);
            cus.channels = new string[] { "server" };
            cus.display = true;

            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            try
            {
                cus.timeZoneFullName = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, GetSiteID()).TimeZoneFullName;
            }
            catch
            {
                cus.timeZoneFullName = "Singapore Standard Time";
            }
            merchantGeneralSettingClient.Close();

        }

        /// <summary>
        /// Check email exist of customer
        /// </summary>
        /// <param>string email</param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>23/12/2014-10:07 PM</createddate>
        /// 
        public int checkEmailCustomerExist(string email)
        {
            WSCustomerClient client = new WSCustomerClient();
            Customer cus = new Customer();
            var count = client.WSCheckEmailExist(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, GetSiteID());
            if (count > 0)
            {
                cus = client.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, GetSiteID());
                if (string.IsNullOrEmpty(cus.memberID))
                {
                    //Case: email is registered as guest -> allow to sign up to member
                    count = 0;
                }
            }
            client.Close();
            return count;
        }

        /// <summary>
        /// update status for customer when customer active mail verify
        /// </summary>
        /// <param>string memberID</param>
        /// <param>string pinCode</param>
        /// <returns>status(true/false)</returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>24/12/2014-10:07 PM</createddate>
        /// 
        public ActionResult ActiveAccount(string memberID, string pinCode)
        {
            GetThemeInfo();
            Customer cus = new Customer();
            WSCustomerClient client = new WSCustomerClient();
            cus = client.WSDetailCustomer(memberID, GetSiteID());
            if (cus != null)
            {
                if (cus.pinCode == pinCode)
                {
                    cus.active = true;
                    cus.status = true;
                    cus.verify = true;
                    client.WSUpdateCustomer(cus, GetSiteID());
                    client.Close();
                    // 
                    IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
                    string siteID = "";
                    siteID = !string.IsNullOrWhiteSpace(GetSiteID()) ? GetSiteID() : Request.Url.Host;
                    var modelMerchant = clientMerchant.WSGetMerchantManagementBySiteId(siteID);
                    clientMerchant.Close();
                    SendMailActiveSuccess(cus, Request.Url.Host, modelMerchant != null ? (!string.IsNullOrWhiteSpace(modelMerchant.businessName) ? modelMerchant.businessName : "") : "");

                    return RedirectToAction("VerifyEmail");
                }
                else
                {
                    ViewBag.status = 2;
                }
            }
            else
            {
                ViewBag.status = 3;
            }
            client.Close();
            return View();
        }

        /// <summary>
        /// Verify Email
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>23/12/2014-10:07 PM</createddate>
        /// 
        public ActionResult VerifyEmail()
        {
            GetThemeInfo();
            return View();
        }

        /// <summary>
        /// Sign in success
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>23/12/2014-10:07 PM</createddate>
        /// 
        public ActionResult Success()
        {
            GetThemeInfo();
            return View();
        }
        #endregion

        #region My Profile
        public ActionResult MyProfile()
        {
            string email = GetEmailMember();
            if (!string.IsNullOrEmpty(email))
            {
                GetThemeInfo();
                Customer cus = new Customer();
                WSCustomerClient client = new WSCustomerClient();
                cus = client.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, GetSiteID());
                client.Close();
                var viewmodel = Mapper.Map<Customer, CustomerViewModel>(cus);
                viewmodel.listDefaultTimeZones = SmoovPOS.Common.ConstantSmoovs.Enums.ListTimeZone;
                return View(viewmodel);
            }
            else
            {
                return RedirectToAction("SignIn");
            }

        }

        public ActionResult EditProfile()
        {
            string email = Session[ConstantSmoovs.Users.userName].ToString();
            if (!string.IsNullOrEmpty(email))
            {
                GetThemeInfo();
                Customer cus = new Customer();
                WSCustomerClient client = new WSCustomerClient();
                cus = client.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, GetSiteID());
                client.Close();
                var viewmodel = Mapper.Map<Customer, CustomerViewModel>(cus);
                WSInventoryClient clientInventory = new WSInventoryClient();
                Country[] countrys = clientInventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, GetSiteID());
                viewmodel.arrCountry = countrys[0];
                //Get saved general settings from couchbase
                viewmodel.listDefaultTimeZones = SmoovPOS.Common.ConstantSmoovs.Enums.ListTimeZone;
                return View(viewmodel);
            }
            else
            {
                return RedirectToAction("SignIn");
            }

        }
        /// <summary>
        /// update member profile
        /// </summary>
        /// <param name="CustomerViewModel">model</param>
        /// <author>"Hung.Do"</author>
        /// <datetime>1/20/2015-5:26 PM</datetime>
        [HttpPost]
        public ActionResult EditProfile(CustomerViewModel model)
        {
            Customer cus = new Customer();
            WSCustomerClient client = new WSCustomerClient();
            cus = client.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, model.email, GetSiteID());
            cus.firstName = model.firstName;
            cus.lastName = model.lastName;
            cus.fullName = model.firstName + " " + model.lastName;
            cus.gender = model.gender;
            cus.birthday = model.birthday;
            cus.address = model.address;
            cus.country = model.country;
            cus.city = model.city;
            cus.state = model.state;
            cus.zipCode = model.zipCode;
            cus.phoneRegion = model.phoneRegion;
            cus.phone = model.phone;
            cus.modifiedDate = DateTime.UtcNow;
            cus.timeZoneFullName = model.timeZoneFullName;
            cus.acceptsNewsletter = model.acceptsNewsletter;
            cus.acceptsSms = model.acceptsSms;
            client.WSUpdateCustomer(cus, GetSiteID());
            client.Close();
            return RedirectToAction("MyProfile", new { email = model.email });
        }
        #endregion

        #region Change Password
        public ActionResult ChangePassword()
        {
            string email = Session[ConstantSmoovs.Users.userName].ToString();
            if (!string.IsNullOrEmpty(email))
            {
                GetThemeInfo();
                Customer cus = new Customer();
                WSCustomerClient client = new WSCustomerClient();
                cus = client.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, GetSiteID());
                client.Close();
                var viewmodel = Mapper.Map<Customer, CustomerChangePasswordViewModel>(cus);
                viewmodel.OldPassword = cus.password;
                return View(viewmodel);
            }
            else
            {
                return RedirectToAction("SiginIn");
            }

        }
        /// <summary>
        /// Change Password 
        /// </summary>
        /// <param>ChangePasswordViewModel</param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>26/12/2014-10:07 PM</createddate>
        /// 
        [HttpPost]
        public bool ChangePassword(string email, string password)
        {
            Customer cus = new Customer();
            try
            {
                WSCustomerClient client = new WSCustomerClient();
                cus = client.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, GetSiteID());
                cus.password = VariableConfigController.MD5Hash(password);
                cus.passwordConfirm = VariableConfigController.MD5Hash(password);
                cus.modifiedDate = DateTime.UtcNow;
                client.WSUpdateCustomer(cus, GetSiteID());
                client.Close();

                CreateASPUser(cus, password);

                return true;
            }
            catch
            {
                return false;
            }
        }
        [HttpPost]
        public bool MD5Password(string password, string oldPassword)
        {
            return VariableConfigController.MD5Hash(password) == oldPassword ? true : false;

        }
        /// <summary>
        /// Reset password 
        /// </summary>
        /// <param>string email</param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>26/12/2014-10:07 PM</createddate>
        /// 
        public ActionResult ResetPassword(string email, string pinCode, bool HttpGet = true)
        {
            GetThemeInfo();
            Customer cus = new Customer();
            WSCustomerClient client = new WSCustomerClient();
            cus = client.WSCheckEmailAndPinCode(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckWithEmailAndPinCode, email, pinCode, GetSiteID());
            if (cus != null)
            {
                cus.active = true;
                client.WSUpdateCustomer(cus, GetSiteID());
            }
            client.Close();
            return View(cus);
        }

        /// <summary>
        /// Reset password 
        /// </summary>
        /// <param>string email</param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>26/12/2014-10:07 PM</createddate>
        /// 
        [HttpPost]
        public int ResetPassword(string email, string password)
        {
            Customer cus = new Customer();
            WSCustomerClient client = new WSCustomerClient();
            cus = client.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, GetSiteID());
            if (cus != null)
            {
                cus.pinCode = Guid.NewGuid().ToString();
                cus.password = VariableConfigController.MD5Hash(password);
                cus.passwordConfirm = VariableConfigController.MD5Hash(password);
                cus.status = true;
                cus.verify = true;
                client.WSUpdateCustomer(cus, GetSiteID());
                client.Close();
                //Alert reset password successfully
                return 1;
            }

            else
            {
                //Alert was reset and need go to page forgot password to receive email reset password
                client.Close();
                return 0;

            }
        }
        #endregion

        #region Send notification email
        /// <summary>
        /// Sends the mail verify.
        /// </summary>
        /// <param name="cus">The cus.</param>
        /// <param name="storeUrl">The store URL.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/20/2015-5:26 PM</datetime>
        public void SendMailVerify(Customer cus, string storeUrl, string businessName)
        {
            //string body = RenderRazorViewToStringNoModel("~/Views/Customer/TempleSendVerifyEmail.cshtml");
            IWSEmailTemplateClient clientEmail = new IWSEmailTemplateClient();
            TemplateEmail[] listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, GetSiteID());
            if (listEmail.Count() == 0)
            {
                listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, GetSiteID());
            }
            string body = "";
            try
            {
                TemplateEmail emailItem = listEmail.FirstOrDefault(r => r.index == ConstantSmoovs.BodyEmail.MemberVerifyAccountEmail);
                body = emailItem != null ? (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "") : "";
            }
            catch
            {

            }
            if (string.IsNullOrWhiteSpace(body))
            {
                body = RenderRazorViewToStringNoModel("~/Views/Customer/TempleSendVerifyEmail.cshtml");
            }
            StringBuilder result = new StringBuilder(body.ToString());
            result.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfCustomer, cus.firstName);
            result.Replace(ConstantSmoovs.EmailKeyword.BusinessName, businessName);
            string linkVerifyAccount = "http://" + storeUrl + "/Customer/ActiveAccount?memberID=" + cus._id + "&pinCode=" + cus.pinCode;

            result.Replace(ConstantSmoovs.EmailKeyword.OnlineshopLink, linkVerifyAccount);
            result.Replace(ConstantSmoovs.EmailKeyword.IDMember, cus._id);
            result.Replace(ConstantSmoovs.EmailKeyword.ActicationCode, cus.pinCode);

            bool isSend = SendEmail(ConstantSmoovs.ConfigMail.Merchant, businessName, cus.email, ConstantSmoovs.TitleEmail.VerifyAccountEmail, result.ToString());
        }

        /// <summary>
        /// Sends the mail active success.
        /// </summary>
        /// <param name="cus">The cus.</param>
        /// <param name="storeUrl">The store URL.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/20/2015-5:08 PM</datetime>
        public void SendMailActiveSuccess(Customer cus, string storeUrl, string businessName)
        {
            //string body = RenderRazorViewToStringNoModel("~/Views/Customer/TempleSendEmailRegisterComplete.cshtml");
            IWSEmailTemplateClient clientEmail = new IWSEmailTemplateClient();
            TemplateEmail[] listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, GetSiteID());
            if (listEmail.Count() == 0)
            {
                listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, GetSiteID());
            }
            string body = "";

            try
            {
                TemplateEmail emailItem = listEmail.FirstOrDefault(r => r.index == ConstantSmoovs.BodyEmail.MemberRegisterComplete);
                body = emailItem != null ? (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "") : "";
            }
            catch
            {

            }
            if (string.IsNullOrWhiteSpace(body))
            {
                body = RenderRazorViewToStringNoModel("~/Views/Customer/TempleSendEmailRegisterComplete.cshtml");
            }
            StringBuilder result = new StringBuilder(body.ToString());
            result.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfCustomer, cus.firstName);
            result.Replace(ConstantSmoovs.EmailKeyword.BusinessName, businessName);
            string OnlineshopLink = "http://" + storeUrl;
            result.Replace(ConstantSmoovs.EmailKeyword.OnlineshopLink, OnlineshopLink);
            bool isSend = SendEmail(ConstantSmoovs.ConfigMail.Merchant, businessName, cus.email, ConstantSmoovs.TitleEmail.RegisterSuccessfully + businessName, result.ToString());

        }
        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="merchantName">Name of the merchant.</param>
        /// <param name="email">The email.</param>
        /// <param name="titleEmail">The title email.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/13/2015-9:46 AM</datetime>
        public bool SendEmail(int type, string merchantName, string email, string titleEmail, string body)
        {
            MailServerUtil sendMail = new MailServerUtil();
            // MailConfigBusiness mailConfig = new MailConfigBusiness();
            var merchantInfor = GetMerchantManagement();
            merchantName = merchantInfor != null ? merchantInfor.businessName : "";
            bool isSend = false;
            if (merchantInfor != null)
            {
                isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body, merchantInfor.merchantID) : false;
            }
            else
            {
                isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body) : false;
            }

            return isSend;


        }

        /// <summary>
        /// Gets the merchant management.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/13/2015-9:47 AM</datetime>
        public MerchantManagementModel GetMerchantManagement()
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            string siteID = GetSiteID();
            var model = client.WSGetMerchantManagementBySiteId(siteID);
            client.Close();
            return model;
        }
        /// <summary>
        /// Sends the mail reset password.
        /// </summary>
        /// <param name="cus">The cus.</param>
        /// <param name="storeUrl">The store URL.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/20/2015-5:06 PM</datetime>
        public void SendMailResetPassword(Customer cus, string storeUrl, string businessName)
        {
            // string body = RenderRazorViewToStringNoModel("~/Views/Customer/TempleSendMailForgotPass.cshtml");
            IWSEmailTemplateClient clientEmail = new IWSEmailTemplateClient();
            TemplateEmail[] listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, GetSiteID());
            if (listEmail.Count() == 0)
            {
                listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, GetSiteID());
            }
            string body = "";

            try
            {
                TemplateEmail emailItem = listEmail.FirstOrDefault(r => r.index == ConstantSmoovs.BodyEmail.MemberForgotPassword);
                body = emailItem != null ? (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "") : "";
            }
            catch
            {

            }
            if (string.IsNullOrWhiteSpace(body))
            {
                body = RenderRazorViewToStringNoModel("~/Views/Customer/TempleSendMailForgotPass.cshtml");
            }
            StringBuilder result = new StringBuilder(body.ToString());
            //---------------------------
            result.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfCustomer, cus.firstName);
            result.Replace(ConstantSmoovs.EmailKeyword.BusinessName, businessName);
            result.Replace(ConstantSmoovs.EmailKeyword.SupportEmailOfMerchant, "support@smoovpos.com");
            string linkResetPassword = "http://" + storeUrl + "/Customer/ResetPassword?email=" + cus.email + "&pinCode=" + cus.pinCode;
            result.Replace(ConstantSmoovs.EmailKeyword.LinkResetPasswork, linkResetPassword);

            bool isSend = SendEmail(ConstantSmoovs.ConfigMail.Merchant, businessName, cus.email, ConstantSmoovs.TitleEmail.ResetPassword, result.ToString());
        }

        /// <summary>
        /// Verify Email For Reset Password 
        /// </summary>
        /// <param>string email</param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>26/12/2014-10:07 PM</createddate>
        /// 
        [HttpPost]
        public int VerifyEmailForResetPassword(string email)
        {
            Customer cus = new Customer();
            WSCustomerClient client = new WSCustomerClient();
            cus = client.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, GetSiteID());
            if (cus != null)
            {
                cus.pinCode = Guid.NewGuid().ToString(); ;
                client.WSUpdateCustomer(cus, GetSiteID());
                client.Close();
                //Send email reset password with new password
                IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
                string siteID = "";
                siteID = !string.IsNullOrWhiteSpace(GetSiteID()) ? GetSiteID() : Request.Url.Host;
                var modelMerchant = clientMerchant.WSGetMerchantManagementBySiteId(siteID);
                clientMerchant.Close();
                SendMailResetPassword(cus, Request.Url.Host, modelMerchant != null ? (!string.IsNullOrWhiteSpace(modelMerchant.businessName) ? modelMerchant.businessName : "") : "");
                return 1;
            }
            else
            {
                client.Close();
                return 0;

            }
        }
        #endregion

        #region Subscribe/Unsubscribe newsletter
        [HttpPost]
        public int SubscribeNewsletter(string email)
        {
            Customer cus = new Customer();

            WSCustomerClient clientCus = new WSCustomerClient();
            cus = clientCus.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, GetSiteID());

            if (cus != null)
            {
                cus.acceptsNewsletter = true;
                cus.modifiedDate = DateTime.UtcNow;

                clientCus.WSUpdateCustomer(cus, GetSiteID());
                clientCus.Close();
            }
            else
            {
                cus = InitializeNewCustomer(GetSiteID());
                cus.email = email;
                cus.acceptsNewsletter = true;
                cus.modifiedDate = DateTime.UtcNow;

                clientCus.WSCreateCustomer(cus, GetSiteID());
                clientCus.Close();
            }

            return 1;
        }
        #endregion
    }
}
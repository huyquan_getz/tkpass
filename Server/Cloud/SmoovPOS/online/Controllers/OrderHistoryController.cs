﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Model;
using Microsoft.AspNet.Identity;
using online.Controllers;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Models.Paging;
using SmoovPOS.UI.Models.Order;
using SmoovPOS.Utility.WSCustomerReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSOrderReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class OrderHistoryController : BaseController
    {
        // GET: OrderHistory
        public ActionResult Index()
        {
            GetThemeInfo();
            string siteID = GetSiteID();
            string email = GetEmailMember();

            //Get customer by email
            WSCustomerClient clientCus = new WSCustomerClient();
            Customer cus = clientCus.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, siteID);
            clientCus.Close();

            //Get all orders of customer
            OrderHistoryListingModel orderList = new OrderHistoryListingModel();
            WSOrderClient clientOrder = new WSOrderClient();
            if (cus == null || cus.timeZoneFullName == null)
            {
                //Get saved general settings from couchbase
                WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
                string defaultTimezone = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).TimeZoneFullName;
                merchantGeneralSettingClient.Close();

                orderList.CustomerTimezone = defaultTimezone + ConstantSmoovs.Enums.ListTimeZone[defaultTimezone];
                orderList.listOrderHistory = clientOrder.GetAllOrderByCustomerEmail(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAllByCustomerEmail, siteID, email).Where(r => r.orderStatus != 0 && r.orderStatus != 10).OrderByDescending(r => r.createdDate).Select(r => new OrderHistoryViewModel(r, siteID, defaultTimezone)).ToList();
            }
            else
            {
                string defaultTimezone = cus.timeZoneFullName;
                orderList.CustomerTimezone = cus.timeZoneFullName + " " + ConstantSmoovs.Enums.ListTimeZone[cus.timeZoneFullName];
                orderList.listOrderHistory = clientOrder.GetAllOrderByCustomerEmail(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.OrderViewNameAllByCustomerEmail, siteID, email).Where(r => r.orderStatus != 0 && r.orderStatus != 10).OrderByDescending(r => r.createdDate).Select(r => new OrderHistoryViewModel(r, siteID, defaultTimezone)).ToList();
            }
            clientOrder.Close();
            return View(orderList);
        }

        public ActionResult Detail(string orderID)
        {
            GetThemeInfo();
            ViewBag.Class = "OrderHistory";
            string siteID = GetSiteID();
            string email = GetEmailMember();

            WSCustomerClient clientCus = new WSCustomerClient();
            Customer cus = clientCus.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, siteID);
            clientCus.Close();

            OrderHistoryViewModel orderDetail = new OrderHistoryViewModel();
            WSOrderClient client = new WSOrderClient();
            if (!string.IsNullOrWhiteSpace(orderID))
            {
                Order order = new Order();
                order = client.WSGetOrder(orderID, siteID);

                if (order != null)
                {
                    if (cus == null || cus.timeZoneFullName == null)
                    {
                        //Get saved general settings from couchbase
                        WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
                        string defaultTimezone = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).TimeZoneFullName;
                        merchantGeneralSettingClient.Close();

                        orderDetail = new OrderHistoryViewModel(order, siteID, defaultTimezone);
                    }
                    else orderDetail = new OrderHistoryViewModel(order, siteID, cus.timeZoneFullName);
                }
            }

            return View(orderDetail);
        }

        [HttpPost]
        public void CancelMyOrder(string orderID)
        {
            GetThemeInfo();
            ViewBag.Class = "OrderHistory";
            string siteID = GetSiteID();
            
            if (!string.IsNullOrWhiteSpace(orderID))
            {
                WSOrderClient client = new WSOrderClient();
                Order order = new Order();
                order = client.WSGetOrder(orderID, siteID);

                if (order != null)
                {
                    order.modifiedDate = DateTime.UtcNow;
                    order.isCancelRequest = true;
                    client.WSUpdateOrder(order, siteID);
                }

                client.Close();
            }
        }
    }
}
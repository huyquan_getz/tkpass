﻿using Com.SmoovPOS.Entity;
using Microsoft.AspNet.Identity;
using online.Models;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Models.Paging;
using SmoovPOS.Utility.WSCheckoutSettingReference;
using SmoovPOS.Utility.WSCollectionReference;
using SmoovPOS.Utility.WSCustomerReference;
using SmoovPOS.Utility.WSDeliveryCountryReference;
using SmoovPOS.Utility.WSDeliverySettingReference;
using SmoovPOS.Utility.WSInstructionReference;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSMyCartReference;
using SmoovPOS.Utility.WSOnlineReference;
using SmoovPOS.Utility.WSOrderReference;
using SmoovPOS.Utility.WSPaymentGatewayReference;
using SmoovPOS.Utility.WSProductItemReference;
using SmoovPOS.Utility.WSServiceShippingReference;
using SmoovPOS.Utility.WSTaxReference;
using SmoovPOS.Utility.WSWishListReference;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace online.Controllers
{
    public class ProductController : BaseController
    {
        #region Product
        public ActionResult ProductByCategory(string category, string startKey = null, string nextKey = null, int limit = 20, int pageGoTo = 1, int pageCurrent = 1, string sort = "_id", bool desc = false, string theme = "")
        {
            ViewBag.Class = "product";
            GetThemeInfo(theme, category);
            string siteID = ViewBag.siteID;
            WSOnlineClient clientOline = new WSOnlineClient();
            ProductByCategoryModel productItemByCategoryList = new ProductByCategoryModel();
            productItemByCategoryList.Category = category;
            productItemByCategoryList.Paging = new EntityPaging()
            {
                startKey = startKey,
                nextKey = nextKey,
                limit = limit,
                pageGoTo = pageGoTo,
                pageCurrent = pageGoTo,
                sort = sort,
                desc = desc
            };
            var listsAll = clientOline.WSGetAllActiveProductItemByCategory(ConstantSmoovs.CouchBase.DesignDocOnline, ConstantSmoovs.CouchBase.GetAllProductByCategoryWithDate, category, productItemByCategoryList.Paging, siteID);
            var allProductItemList = listsAll.Where(r => r.categoryName == category);
            clientOline.Close();
            productItemByCategoryList.ProductItemList = allProductItemList.AsEnumerable().Take(limit).ToList();

            // Add discount to products
            BaseController baseController = new BaseController();
            productItemByCategoryList.ProductItemList = baseController.SetDiscountToListProductItems(productItemByCategoryList.ProductItemList, siteID);


            ViewBag.category = category;

            //Get and set merchant's currency
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            string currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).Currency;
            merchantGeneralSettingClient.Close();
            productItemByCategoryList.Currency = ViewBag.currency = !string.IsNullOrWhiteSpace(currency) ? currency : "";

            return View(productItemByCategoryList);
        }

        [HttpPost]
        public ActionResult LoadProductByCategory(string category, int limit, int pageGoTo, int pageCurrent, string sort = "title", bool desc = false)
        {
            GetThemeInfo();
            string siteID = ViewBag.siteID;
            WSOnlineClient clientOline = new WSOnlineClient();
            ProductByCategoryModel productItemByCategoryList = new ProductByCategoryModel();
            productItemByCategoryList.Category = category;
            productItemByCategoryList.Paging = new EntityPaging()
            {
                limit = limit,
                pageGoTo = pageGoTo,
                pageCurrent = pageGoTo,
                sort = sort,
                desc = desc
            };
            var listsAll = clientOline.WSGetAllActiveProductItemByCategory(ConstantSmoovs.CouchBase.DesignDocOnline, ConstantSmoovs.CouchBase.GetAllProductByCategoryWithDate, category, productItemByCategoryList.Paging, siteID);
            var allProductItemList = listsAll.Where(r => r.categoryName == category);

            clientOline.Close();

            //Get and set merchant's currency
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            string currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).Currency;
            merchantGeneralSettingClient.Close();
            productItemByCategoryList.Currency = ViewBag.currency = !string.IsNullOrWhiteSpace(currency) ? currency : "";

            productItemByCategoryList.ProductItemList = allProductItemList.Take(limit).ToList();

            // Add discount to products
            BaseController baseController = new BaseController();
            productItemByCategoryList.ProductItemList = baseController.SetDiscountToListProductItems(productItemByCategoryList.ProductItemList, siteID);

            return Json(new { Data = productItemByCategoryList });
        }

        public ActionResult AllProducts(string siteID, string theme = "")
        {
            ViewBag.Class = "product";
            GetThemeInfo(theme);

            WSOnlineClient clientOline = new WSOnlineClient();
            var lists = GetAllCategory();
            return View(lists);
        }

        /// <summary>
        /// View Product detail
        /// </summary>
        /// <param name="productItemId"></param>
        /// <returns></returns>
        public ActionResult ProductDetail(string productItemId, string collectionId)
        {
            WSInventoryClient clientIventory = new WSInventoryClient();

            ViewBag.Class = "product";
            GetThemeInfo();
            string siteID = ViewBag.siteID;

            //Get and set merchant's currency
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            string currency = ViewBag.currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).Currency;
            merchantGeneralSettingClient.Close();

            WSOnlineClient clientOline = new WSOnlineClient();
            ProductItemDetailModel productItemdetail = new ProductItemDetailModel();
            productItemdetail.ProductItem = clientOline.WSGetProductItemDetail(productItemId, GetSiteID());
            clientOline.Close();

            var arrayVarient = productItemdetail.ProductItem.arrayVarient;
            productItemdetail.ProductItem.arrayVarient = new List<VarientItem>();

            for (var i = 0; i < arrayVarient.Count(); i++)
            {
                if (arrayVarient[i].status)
                {
                    arrayVarient[i].quantityInStock = arrayVarient[i].quantity;
                    productItemdetail.ProductItem.arrayVarient.Add(arrayVarient[i]);
                }
            }
            ViewBag.productItem = productItemId;
            //get collection
            if (!string.IsNullOrWhiteSpace(collectionId))
            {
                WSCollectionClient CollectionClient = new WSCollectionClient();
                Collection collec = CollectionClient.WSDetailCollection(collectionId, GetSiteID());
                productItemdetail.Collection = collec;
            }

            // Add discount to products
            BaseController baseController = new BaseController();
            productItemdetail.ProductItem = baseController.SetDiscountToProductItem(productItemdetail.ProductItem, siteID);

            return View(productItemdetail);
        }

        /// <summary>
        /// Searches the product.
        /// </summary>
        /// <param name="searchCondition">The search condition.</param>
        /// <param name="theme">The theme.</param>
        /// <returns></returns>
        /// <createdby>nhu.dang</createdby>
        /// <createddate>12/9/2014-5:07 PM</createddate>
        public ActionResult SearchProduct(string searchCondition, string theme = "")
        {
            ViewBag.Class = "product";
            GetThemeInfo(theme);
            string siteID = ViewBag.siteID;

            WSOnlineClient clientOline = new WSOnlineClient();
            //  SearchProductModel searchProductModel = new SearchProductModel();
            ProductByCategoryModel searchProductModel = new ProductByCategoryModel();
            searchProductModel.Paging = new EntityPaging()
            {
                limit = 20,
                pageGoTo = 1,
                pageCurrent = 1
            };

            //var productItemList = clientOline.WSSearchAllActiveProductItem(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByStore, searchCondition, siteID); -- HuyQTA: change GetAllByStore -> GetAllProductByCategoryWithDate
            var productItemList = clientOline.WSSearchAllActiveProductItem(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByStore, searchCondition, siteID);

            clientOline.Close();
            var countAll = productItemList.Count();
            productItemList = productItemList.Skip(0).Take(searchProductModel.Paging.limit).ToArray();

            searchProductModel.ProductItemList = productItemList.ToList();

            // Add discount to products
            BaseController baseController = new BaseController();
            searchProductModel.ProductItemList = baseController.SetDiscountToListProductItems(searchProductModel.ProductItemList, siteID);

            //Get and set merchant's currency
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            string currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).Currency;
            merchantGeneralSettingClient.Close();
            searchProductModel.Currency = ViewBag.currency = !string.IsNullOrWhiteSpace(currency) ? currency : "";
            searchProductModel.searchKeyWord = searchProductModel.Category = searchCondition;
            searchProductModel.countOfSearchResults = searchProductModel.countOfBestSellerProduct = countAll;
            return View(searchProductModel);
        }

        /// <summary>
        /// Searches the product for load more.
        /// </summary>
        /// <param name="paging">The paging.</param>
        /// <returns></returns>
        /// <createdby>nhu.dang</createdby>
        /// <createddate>12/9/2014-5:07 PM</createddate>
        [HttpPost]
        //public ActionResult SearchProductLoadMore(ProductItem[] productItemList)
        public ActionResult SearchProductLoadMore(int limit, int pageGoTo, string currency, string searchCondition)
        {
            string siteID = GetSiteID();
            ProductByCategoryModel searchProductModel = new ProductByCategoryModel();
            searchProductModel.Paging = new EntityPaging()
            {
                limit = limit,
                pageGoTo = pageGoTo,
                pageCurrent = pageGoTo
            };

            WSOnlineClient clientOline = new WSOnlineClient();

            var productItemList = clientOline.WSSearchAllActiveProductItem(ConstantSmoovs.CouchBase.DesignDocOnline, ConstantSmoovs.CouchBase.GetAllProductItem, searchCondition, siteID);
            clientOline.Close();

            var countAll = productItemList.Count();

            productItemList = productItemList.Skip((searchProductModel.Paging.pageGoTo - 1) * searchProductModel.Paging.limit).Take(searchProductModel.Paging.limit).ToArray();

            searchProductModel.ProductItemList = productItemList.ToList();

            // Add discount to products
            BaseController baseController = new BaseController();
            searchProductModel.ProductItemList = baseController.SetDiscountToListProductItems(searchProductModel.ProductItemList, siteID);

            if (string.IsNullOrEmpty(currency))
            {
                WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
                MerchantGeneralSetting generalSettings = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, GetSiteID());
                merchantGeneralSettingClient.Close();

                currency = !string.IsNullOrWhiteSpace(generalSettings.Currency) ? generalSettings.Currency : "";
            }

            searchProductModel.Currency = !string.IsNullOrWhiteSpace(currency) ? currency : "";
            searchProductModel.Category = searchCondition;

            return PartialView("~/Views/Product/ProductItemLoadMore.cshtml", searchProductModel);
        }
        #endregion

        #region Wishlist
        public ActionResult WishList()
        {
            string siteID = GetSiteID();

            if (Session[ConstantSmoovs.Users.userName] != null && !string.IsNullOrEmpty(Session[ConstantSmoovs.Users.userName].ToString()))
            {
                WSWishListClient client = new WSWishListClient();
                WishList wishlist = new WishList();
                string email = GetEmailMember();
                wishlist = client.WSGetWishListWithEmail(ConstantSmoovs.CouchBase.DesignDocWishlist, ConstantSmoovs.CouchBase.GetByEmail, email, siteID);
                GetThemeInfo();
                if (wishlist == null)
                {
                    wishlist = new WishList();
                    wishlist._id = Guid.NewGuid().ToString();
                    wishlist.display = true;
                    wishlist.email = GetEmailMember();
                    wishlist.status = true;
                    wishlist.modifiedDate = DateTime.UtcNow;
                    List<ProductItem> list = new List<ProductItem>();
                    wishlist.arrayProductItem = list;
                    int result = client.WSCreateWishList(wishlist, siteID);
                }
                client.Close();
                return View(wishlist);
            }
            else
            {
                return RedirectToAction("SignIn", "Customer");
            }
        }

        [HttpPost]
        public int AddToWishlist(ProductItem productItem)
        {
            string siteID = GetSiteID();
            WishList wishlist = null;
            string email = GetEmailMember();

            WSWishListClient client = new WSWishListClient();
            wishlist = client.WSGetWishListWithEmail(ConstantSmoovs.CouchBase.DesignDocWishlist, ConstantSmoovs.CouchBase.GetByEmail, email, siteID);
            if (wishlist != null)
            {
                ProductItem product = new ProductItem();
                product = wishlist.arrayProductItem.Where(r => r._id == productItem._id).FirstOrDefault();
                if (product != null)
                {
                    VarientItem variant = product.arrayVarient.Where(r => r._id == productItem.arrayVarient[0]._id).FirstOrDefault();
                    if (variant == null)
                    {
                        wishlist.arrayProductItem.Where(r => r._id == productItem._id).FirstOrDefault().arrayVarient.Add(productItem.arrayVarient[0]);
                        client.WSUpdateWishList(wishlist, siteID);
                        client.Close();
                        //add variant into product item for customer
                        return 1;
                    }
                    else
                    {
                        client.Close();
                        //variant is exist in customer wishlist
                        return 0;
                    }
                }
                else
                {
                    wishlist.arrayProductItem.Add(productItem);
                    client.WSUpdateWishList(wishlist, siteID);
                    client.Close();
                    //add productItem into wishlist customer
                    return 1;
                }
            }
            else
            {
                wishlist = new WishList();
                wishlist._id = Guid.NewGuid().ToString();
                wishlist.display = true;
                wishlist.email = email;
                wishlist.status = true;
                wishlist.modifiedDate = DateTime.UtcNow;
                List<ProductItem> list = new List<ProductItem>();
                list.Add(productItem);
                wishlist.arrayProductItem = list;
                int result = client.WSCreateWishList(wishlist, siteID);
                client.Close();
                //Create new wishlist for customer
                return 1;
            }
        }

        public ActionResult removeVariantWish(string productItemId, string varientId)
        {
            string siteID = GetSiteID();
            if (!string.IsNullOrEmpty(Session[ConstantSmoovs.Users.userName].ToString()))
            {
                WSWishListClient client = new WSWishListClient();
                WishList wishlist = new WishList();
                string email = GetEmailMember();
                wishlist = client.WSGetWishListWithEmail(ConstantSmoovs.CouchBase.DesignDocWishlist, ConstantSmoovs.CouchBase.GetByEmail, email, siteID);
                foreach (var itemMycart in wishlist.arrayProductItem.Where(r => r._id == productItemId))
                {
                    itemMycart.arrayVarient.RemoveAll(r => r._id == varientId);
                }
                int result = client.WSUpdateWishList(wishlist, siteID);
                client.Close();
            }
            else
            {
                return RedirectToAction("SignIn", "Customer");
            }

            return RedirectToAction("WishList", "Product");
        }
        #endregion

        #region MyCart
        public ActionResult MyCart()
        {
            GetThemeInfo();
            string siteID = GetSiteID();

            MyCart myCartInSession = GetMyCartItems();
            myCartInSession.receiptValue = CalculateMyCartReceipt(myCartInSession.listProductItems, 0);
            Session[ConstantSmoovs.CardOrder.myCart] = myCartInSession;

            //Get and set merchant's currency
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            ViewBag.currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).Currency;
            merchantGeneralSettingClient.Close();

            return View(myCartInSession);
        }

        [HttpPost]
        public int AddToCart(ProductItem productItem)
        {
            if (productItem != null)
            {
                //Case: product to add is not null -> update mycart in session and in coughbase

                MyCart myCartInSession = null;
                if ((Object)Session[ConstantSmoovs.CardOrder.myCart] != null) myCartInSession = Session[ConstantSmoovs.CardOrder.myCart] as MyCart;
                else Session[ConstantSmoovs.CardOrder.myCart] = myCartInSession = InitializeNewMyCartObject();

                //Do adding product item into cart
                myCartInSession = AddProductIntoCart(productItem, myCartInSession);
                //Re-assign session cart
                Session[ConstantSmoovs.CardOrder.myCart] = myCartInSession;
                //If customer logged in -> uppdate coughbase cart
                StoreMyCartIntoCoughbase(GetSiteID(), myCartInSession);

                return 1;
            }
            return 0;
        }

        private MyCart AddProductIntoCart(ProductItem productItem, MyCart myCartInSession)
        {
            List<ProductItem> listItemsInSession = myCartInSession.listProductItems;

            //Set flag to check this variant to be checked out
            productItem.arrayVarient[0].isCheckedToCheckOut = true;

            //Check current myCart is null or not
            if (listItemsInSession != null && listItemsInSession.Count() > 0)
            {
                //Case: current mycart is not null

                if (listItemsInSession.FindIndex(i => i._id == productItem._id) != -1)
                {
                    //Case: productItem contains in listItemsInSession --> check list variant to update quantity or add new variant

                    List<VarientItem> listVariant = listItemsInSession.FirstOrDefault(i => i._id == productItem._id).arrayVarient;
                    VarientItem productVariantItem = productItem.arrayVarient[0];
                    listItemsInSession.FirstOrDefault(i => i._id == productItem._id).arrayVarient = AddVariantIntoCart(productVariantItem, listVariant);
                }
                else
                {
                    //Case: productItem doesn't contain in listItemsInSession --> add new item
                    listItemsInSession.Add(productItem);
                }
            }
            else
            {
                //Case: current mycart is null

                //Create temp list item
                List<ProductItem> list = new List<ProductItem>();
                list.Add(productItem);
                //Assign temp value into real variable
                listItemsInSession = list;
            }

            //Re-assign value
            myCartInSession.modifiedDate = DateTime.UtcNow;
            myCartInSession.listProductItems = listItemsInSession;

            return myCartInSession;
        }

        private static List<VarientItem> AddVariantIntoCart(VarientItem productVariantItem, List<VarientItem> listVariant)
        {
            if (listVariant != null && listVariant.Count > 0)
            {
                //Case: listVariant is not null -> add or update listVariant

                if (listVariant.FindIndex(i => i.sku == productVariantItem.sku) != -1)
                {
                    //Case: productItem.variant is existed in listVariant -> update variant's quantity
                    listVariant.FirstOrDefault(v => v.sku == productVariantItem.sku).quantity += productVariantItem.quantity;
                    if (!string.IsNullOrEmpty(productVariantItem.variantRemark))
                    {
                        listVariant.FirstOrDefault(v => v.sku == productVariantItem.sku).variantRemark += productVariantItem.variantRemark;
                    }
                    listVariant.FirstOrDefault(v => v.sku == productVariantItem.sku).isCheckedToCheckOut = productVariantItem.isCheckedToCheckOut;
                }
                else
                {
                    //Case: productItem.variant is not existed in listVariant -> add new variant
                    listVariant.Add(productVariantItem);
                }
            }
            else
            {
                //Case: listVariant is null -> create new listVariant

                //Create temp list item
                List<VarientItem> list = new List<VarientItem>();
                list.Add(productVariantItem);
                //Assign temp value into real variable
                listVariant = list;
            }

            return listVariant;
        }

        [HttpPost]
        public int changeQuantity(string variantSku, string productItemId, string qty)
        {
            string siteID = GetSiteID();

            WSOnlineClient clientOline = new WSOnlineClient();
            ProductItemDetailModel productItemdetail = new ProductItemDetailModel();
            List<VarientItem> variants = clientOline.WSGetProductItemDetail(productItemId, siteID).arrayVarient;
            clientOline.Close();

            VarientItem variantCurrent = variants.Where(r => r.sku == variantSku).FirstOrDefault();
            MyCart myCartInSession = Session[ConstantSmoovs.CardOrder.myCart] as MyCart;

            foreach (var itemMycart in myCartInSession.listProductItems.Where(r => r._id == productItemId))
            {
                foreach (var varientCart in itemMycart.arrayVarient.Where(r => r.sku == variantSku))
                {
                    varientCart.quantity = Convert.ToInt32(qty);
                    myCartInSession.receiptValue = CalculateMyCartReceipt(myCartInSession.listProductItems, 0);
                    myCartInSession.modifiedDate = DateTime.UtcNow;

                    Session[ConstantSmoovs.CardOrder.myCart] = myCartInSession;
                    StoreMyCartIntoCoughbase(siteID, myCartInSession);

                    return 1;
                }
            }

            return 1;
        }

        public ActionResult removeVariantCart(string productItemId, string variantSku)
        {
            string siteID = GetSiteID();
            MyCart myCartInSession = Session[ConstantSmoovs.CardOrder.myCart] as MyCart;

            //Remove variant
            foreach (var itemMycart in myCartInSession.listProductItems.Where(i => i._id == productItemId))
            {
                itemMycart.arrayVarient.RemoveAll(r => r.sku == variantSku);
            }
            //Remove null productItem
            if (myCartInSession.listProductItems.FirstOrDefault(i => i._id == productItemId).arrayVarient.Count == 0)
            {
                myCartInSession.listProductItems.RemoveAll(r => r._id == productItemId);
            }

            //Re-calculate the receipt
            CalculatorReceipt calculatorReceipt = new CalculatorReceipt();
            myCartInSession.receiptValue = CalculateMyCartReceipt(myCartInSession.listProductItems, 0);
            myCartInSession.modifiedDate = DateTime.UtcNow;

            Session[ConstantSmoovs.CardOrder.myCart] = myCartInSession;
            StoreMyCartIntoCoughbase(siteID, myCartInSession);

            return RedirectToAction("MyCart", "Product");
        }

        [HttpPost]
        public void checkAllMyCartItemToCheckOut(string isChecked)
        {
            MyCart myCartInSession = Session[ConstantSmoovs.CardOrder.myCart] as MyCart;

            if (isChecked == "true")
            {
                //Select all item in listSelectedProducts
                foreach (var item in myCartInSession.listProductItems)
                {
                    foreach (var variant in item.arrayVarient)
                    {
                        variant.isCheckedToCheckOut = true;
                    }
                }

                //Calculate session's receipt value with listSelectedProducts is checked all
                myCartInSession.receiptValue = CalculateMyCartReceipt(myCartInSession.listProductItems, 0);
            }
            else
            {
                //De-Select all item in listSelectedProducts
                foreach (var item in myCartInSession.listProductItems)
                {
                    foreach (var variant in item.arrayVarient)
                    {
                        variant.isCheckedToCheckOut = false;
                    }
                }

                //Set session's receipt value = null when listSelectedProducts is un-checked all
                myCartInSession.receiptValue = new ReceiptValue();
            }

            Session[ConstantSmoovs.CardOrder.myCart] = myCartInSession;
        }

        [HttpPost]
        public void selectMyCartItemToCheckOut(string productId, string variantSku, string isChecked)
        {
            MyCart myCartInSession = (MyCart)Session[ConstantSmoovs.CardOrder.myCart];

            //Set flag
            myCartInSession.listProductItems.FirstOrDefault(i => i._id == productId)
                            .arrayVarient.FirstOrDefault(v => v.sku == variantSku).isCheckedToCheckOut = Convert.ToBoolean(isChecked);

            //Calculate session's receipt value with listSelectedProducts
            myCartInSession.receiptValue = CalculateMyCartReceipt(myCartInSession.listProductItems, 0);

            Session[ConstantSmoovs.CardOrder.myCart] = myCartInSession;
        }
        #endregion

        #region Checkout
        //Save the changes of remark into session and coughbase
        [HttpPost]
        public int CheckOut_OnClick(string remark)
        {
            MyCart myCartInSession = new MyCart();
            myCartInSession = Session[ConstantSmoovs.CardOrder.myCart] as MyCart;

            if (myCartInSession != null && myCartInSession.listProductItems != null)
            {
                bool isCheckedOneCheckbox = false;
                int countOfCheckedCheckbox = 0;
                int countOfValidVariantQuantity = 0;

                foreach (var item in myCartInSession.listProductItems)
                {
                    foreach (var variant in item.arrayVarient)
                    {
                        if (variant.isCheckedToCheckOut)
                        {
                            isCheckedOneCheckbox = true;
                            countOfCheckedCheckbox++;
                            if (variant.quantity <= variant.quantityInStock || variant.quantityInStock == -1) countOfValidVariantQuantity++;
                        }
                    }
                }

                if (isCheckedOneCheckbox)
                {
                    if (countOfCheckedCheckbox == countOfValidVariantQuantity)
                    {
                        myCartInSession.remark = remark;
                        myCartInSession.modifiedDate = DateTime.UtcNow;

                        Session[ConstantSmoovs.CardOrder.myCart] = myCartInSession;
                        StoreMyCartIntoCoughbase(GetSiteID(), myCartInSession);

                        //Initialize new myCart for checking-out process            
                        MyCart myOrder = InitializeNewMyCartObject();
                        myOrder.email = myCartInSession.email;
                        myOrder.remark = myCartInSession.remark;
                        myOrder.listProductItems = GetMyCartCheckedOutItems(myCartInSession.listProductItems);
                        myOrder.receiptValue = CalculateMyCartReceipt(myOrder.listProductItems, 0);
                        Session[ConstantSmoovs.CardOrder.myOrder] = myOrder;

                        return 0;//Success
                    }
                    else return 1;//Failed due to variant quantity is not valid
                }
                else return 2;//Failed due to nothing is select to check out 
            }

            return 3;//Failed due to myCart is null
        }

        public ActionResult CheckOut()
        {
            GetThemeInfo();
            string siteID = GetSiteID();

            MyCart myCartInSession = ((Object)Session[ConstantSmoovs.CardOrder.myOrder] != null) ? (MyCart)Session[ConstantSmoovs.CardOrder.myOrder] : InitializeNewMyCartObject();
            ViewBag.Remark = myCartInSession.remark;
            Session[ConstantSmoovs.CardOrder.myOrder] = myCartInSession;

            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            ViewBag.currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, ViewBag.siteID).Currency;
            merchantGeneralSettingClient.Close();

            return View(myCartInSession);
        }

        public ActionResult Payment()
        {
            GetThemeInfo();
            string siteID = GetSiteID();
            PaymentModel paymentModel = new PaymentModel();
            WSInventoryClient clientInventory = new WSInventoryClient();
            string selectedCountry = ConstantSmoovs.Countrys.Singapore.ToUpper();
            if ((Object)Session[ConstantSmoovs.CardOrder.myOrder] != null) paymentModel.mycart = Session[ConstantSmoovs.CardOrder.myOrder] as MyCart;

            //Get and set merchant's currency
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            ViewBag.currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).Currency;
            merchantGeneralSettingClient.Close();

            //Initialize customer
            Customer customer = InitializeNewCustomer(siteID);
            //Check customer is authenticated or not to execute
            if (CheckIsAuthentication())
            {
                //customer is authenticated (customer == member) -> get paymentModel.member from coughbase
                string email = GetEmailMember();
                WSCustomerClient client = new WSCustomerClient();
                paymentModel.member = client.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, siteID);
                selectedCountry = string.IsNullOrEmpty(paymentModel.member.country) ? ConstantSmoovs.Countrys.Singapore.ToUpper() : paymentModel.member.country.ToUpper();
                client.Close();

            }
            else
            {
                //customer is not authenticated (customer == guest) -> assign paymentModel.member by temp value
                paymentModel.member = customer;
            }


            Country[] countrys = clientInventory.WSGetAllCountry("country", "get_all_country", siteID);
            WSDeliveryCountryClient clientCountry = new WSDeliveryCountryClient();

            var resultCountrys = clientCountry.WSGetAllDeliveryCountry(ConstantSmoovs.CouchBase.DesignDocDeliveryCountry, ConstantSmoovs.CouchBase.DeliveryCountryViewAll, siteID);
            clientCountry.Close();
            var listItems = new List<SelectListItem>() { };
            var arrayArea = new List<SelectListItem>() { };
            foreach (var country in resultCountrys.OrderBy(c => c.country))
            {
                listItems.Add(new SelectListItem()
                {
                    Value = country._id,
                    Text = country.country,
                    Selected = country.country.ToUpper() == selectedCountry
                });
                if (country.country.ToUpper() == ConstantSmoovs.Countrys.Singapore.ToUpper())
                {
                    arrayArea = LoadArea(siteID, country);
                }
            }
            paymentModel.countrys = listItems;
            paymentModel.areas = arrayArea;

            WSDeliverySettingClient clientSetting = new WSDeliverySettingClient();
            var resultSettings = clientSetting.WSGetAllDeliverySetting(ConstantSmoovs.CouchBase.DesignDocDeliverySetting, ConstantSmoovs.CouchBase.DeliverySettingViewAll, siteID);
            clientSetting.Close();

            var listItemSettings = new List<SelectListItem>() { };
            var arrayAreaSetting = new List<SelectListItem>() { };
            var arrayService = new List<ServiceDelivery>() { };
            foreach (var setting in resultSettings.OrderBy(c => c.country.country))
            {
                if (setting.country != null &&
                    setting.arrayServiceDelivery != null &&
                    setting.arrayServiceDelivery.Count > 0 &&
                    setting.arrayServiceDelivery.Where(s => s.arrayAreaShipping != null && s.arrayAreaShipping.Where(a => a.isSelect).Any()).Any())
                {
                    listItemSettings.Add(new SelectListItem()
                    {
                        Value = setting.country._id,
                        Text = setting.country.country,
                        Selected = setting.country.country.ToUpper() == ConstantSmoovs.Countrys.Singapore.ToUpper()
                    });
                    if (setting.country.country.ToUpper() == ConstantSmoovs.Countrys.Singapore.ToUpper())
                    {
                        arrayAreaSetting = LoadAreaSetting(siteID, setting.country);
                        if (arrayAreaSetting != null && arrayAreaSetting.Count() > 0)
                        {
                            arrayService = LoadServiceSelectedByArea(setting._id, arrayArea[0].Value);
                        }

                    }
                }
            }
            paymentModel.countrySettings = listItemSettings;
            paymentModel.areaSettings = arrayAreaSetting;
            paymentModel.arrayService = arrayService;

            paymentModel.arrCountry = countrys[0];
            Inventory[] ListInventory = clientInventory.WSGetAll(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, siteID);
            paymentModel.arrInventory = ListInventory.Where(r => r.index > 1);
            clientInventory.Close();

            WSInstructionClient clientInstruction = new WSInstructionClient();
            Instruction[] arrInstruction = clientInstruction.WSGetAllInstruction(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.GetAllInstruction, siteID);
            clientInstruction.Close();

            WSServiceShippingClient clientShipping = new WSServiceShippingClient();
            ServiceShipping[] arrServiceShipping = clientShipping.WSGetAllServiceShipping(ConstantSmoovs.CouchBase.DesignDocOrder, ConstantSmoovs.CouchBase.GetAllServiceShipping, siteID);
            clientShipping.Close();

            paymentModel.arrServiceShipping = arrServiceShipping;
            paymentModel.arrInstruction = arrInstruction;

            //get Checkout Setting 
            WSCheckoutSettingClient clientCheckout = new WSCheckoutSettingClient();
            var checkoutSetting = clientCheckout.GetDetailCheckoutSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAllCheckoutSetting, siteID);
            paymentModel.checkoutSetting = checkoutSetting;
            clientCheckout.Close();

            if (paymentModel.mycart == null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(paymentModel);
        }

        public ActionResult PaymentOrder(FormCollection fc)
        {
            GetThemeInfo();
            string siteID = GetSiteID();

            //Get and set merchant's currency
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            ViewBag.currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).Currency;
            merchantGeneralSettingClient.Close();

            OrderModel orderModel = new OrderModel();
            orderModel.Order = new Order();
            double shippingFee = 0;
            //init buyer's and recipient's information 
            Customer buyer = InitializeNewCustomer(siteID);
            Customer recipient = InitializeNewCustomer(siteID);

            var country = fc["country2"];
            if (fc["optionPayment"].Equals("paymentDeliverly"))
            {
                if (!string.IsNullOrEmpty(country))
                {
                    WSDeliverySettingClient clientSetting = new WSDeliverySettingClient();
                    var deliverySetting = clientSetting.WSGetDeliverySettingByCountry(ConstantSmoovs.CouchBase.DesignDocDeliverySetting, ConstantSmoovs.CouchBase.DeliverySettingViewByCountry, GetSiteID(), country);
                    clientSetting.Close();

                    var delivery = deliverySetting;
                    var services = new List<ServiceDelivery>() { };
                    var areaShippings = new List<AreaShipping>() { };

                    var area = fc["area"];
                    var delivery_service = fc["delivery_service"];
                    if (!string.IsNullOrEmpty(delivery_service))
                    {
                        var lstServices = delivery_service.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        if (lstServices.Length > 0)
                        {
                            services = deliverySetting.arrayServiceDelivery.Where(s => s._id == lstServices[0]).ToList();
                            if (services != null && services.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(area))
                                {
                                    areaShippings = services[0].arrayAreaShipping.Where(a => a.areaName == area).ToList();
                                }
                                services[0].arrayAreaShipping = areaShippings;
                            }
                        }
                    }

                    delivery.arrayServiceDelivery = services;
                    if (services.Count() > 0)
                    {
                        shippingFee = services[0].arrayAreaShipping.Count() > 0 ? Math.Round(services[0].arrayAreaShipping.FirstOrDefault().areaPrice, 2) : services[0].shippingPrice;
                    }
                    //set country and shipping information for customer and Order
                    orderModel.Order.serviceShipping = delivery;
                    recipient.country = delivery.country.country;
                }

                SetDeliveryCustomersByDataInForm(fc, siteID, orderModel, buyer, recipient);

                orderModel.Order.customerReceiver = recipient;
            }
            else
            {
                SetSelfCollectCustomersByDataInForm(fc, siteID, orderModel, buyer);

                orderModel.Order.customerReceiver = buyer;
                var idMerchant = VariableConfigController.GetIDMerchant();
                orderModel.Order.channels = new String[] { idMerchant + "_" + orderModel.Order.inventory.index.ToString() };
            }


            orderModel.Order.customerSender = buyer;
            orderModel.Order._id = Guid.NewGuid().ToString();
            orderModel.Order.createdDate = DateTime.UtcNow;
            orderModel.Order.modifiedDate = DateTime.UtcNow;
            orderModel.Order.currency = ViewBag.currency;
            orderModel.Order.status = true;
            orderModel.Order.transactionID = "TR" + DateTime.UtcNow.ToString("ddMMyyyyHHmmssffff");
            string transactionId = orderModel.Order.transactionID;
            orderModel.Order.completedDate = DateTime.UtcNow;



            MyCart myCartInSession = Session[ConstantSmoovs.CardOrder.myOrder] as MyCart;
            if (shippingFee > 0)
            {
                myCartInSession.receiptValue = CalculateMyCartReceipt(myCartInSession.listProductItems, shippingFee);
            }
            orderModel.Order.subTotal = myCartInSession.receiptValue.subTotal;
            orderModel.Order.listTaxvalue = myCartInSession.receiptValue.listTaxOrder;
            orderModel.Order.grandTotal = myCartInSession.receiptValue.grandTotal;
            orderModel.Order.roundAmount = myCartInSession.receiptValue.rountAmt;
            orderModel.Order.discount = myCartInSession.receiptValue.discountTotal;

            //Initialize list order
            List<Orderdetail> listOrderDetails = new List<Orderdetail>();
            Orderdetail orderDetail = new Orderdetail();
            orderDetail.dateOrder = orderModel.Order.completedDate;
            orderDetail.orderCode = orderModel.Order.orderCode;
            orderDetail.payType = "SmoovPay";
            orderDetail.total = myCartInSession.receiptValue.grandTotal;
            orderDetail.timeZone = orderModel.Order.inventory.timeZone;

            //Retrieve member by email
            WSCustomerClient clientCustomer = new WSCustomerClient();
            Customer member = clientCustomer.WSGetDetailCustomerByEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, buyer.email, siteID);

            if (member == null)
            {
                //Case: email has not used before -> create new guest-role member

                listOrderDetails.Add(orderDetail);
                //Create new guest-role member
                buyer.listOrder = listOrderDetails;
                buyer.totalOrder = 1;
                buyer.totalSpend = myCartInSession.receiptValue.grandTotal;
                buyer.modifiedDate = orderModel.Order.completedDate;
                buyer.lastOrder = orderModel.Order.completedDate;
                clientCustomer.WSCreateCustomer(buyer, siteID);
            }
            else
            {
                //Case: email has used before -> update the existed member (member-role or guest-roles)

                if (member.listOrder != null) listOrderDetails = member.listOrder;
                listOrderDetails.Add(orderDetail);

                //Update member's info
                member.lastOrder = orderModel.Order.completedDate;
                member.modifiedDate = orderModel.Order.completedDate;
                member.listOrder = listOrderDetails;
                member.totalOrder += 1;
                member.totalSpend += myCartInSession.receiptValue.grandTotal;
                clientCustomer.WSUpdateCustomer(member, siteID);
            }
            clientCustomer.Close();

            orderModel.Order.paymentStatus = 1;
            orderModel.Order.orderStatus = 0;
            orderModel.Order.status = true;

            //Update order's list product items
            List<ProductOrder> ProductOrderList = new List<ProductOrder>();
            foreach (var productItem in myCartInSession.listProductItems)
            {
                ProductOrder productOrder = new ProductOrder();
                productOrder._id = productItem._id;
                productOrder.sku = productItem.sku;
                productOrder.title = productItem.title;
                productOrder.categoryId = productItem.categoryId;
                productOrder.arrayVariantOrder = productItem.arrayVarient;
                productOrder.arrayInventory = productItem.arrayInventory;
                productOrder.discount = productItem.discount;
                productOrder.discountProductItem = productItem.DiscountProductItem;
                ProductOrderList.Add(productOrder);
            }
            orderModel.Order.arrayProduct = ProductOrderList;

            //Update order's remark
            orderModel.Order.remark = myCartInSession.remark;

            //Save order into coughbase
            WSOrderClient client = new WSOrderClient();
            client.WSCreateOrder(orderModel.Order, siteID);
            client.Close();

            return RedirectToAction("PaymentSubmit", new { transactionId = transactionId, orderId = orderModel.Order._id });
        }

        private static void SetSelfCollectCustomersByDataInForm(FormCollection fc, string siteID, OrderModel orderModel, Customer buyer)
        {
            if (!string.IsNullOrWhiteSpace(fc["buyer_firstnameS"])) buyer.firstName = fc["buyer_firstnameS"];
            else buyer.firstName = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_lastnameS"])) buyer.lastName = fc["buyer_lastnameS"];
            else buyer.lastName = "";
            buyer.fullName = buyer.firstName + " " + buyer.lastName;

            if (!string.IsNullOrWhiteSpace(fc["buyer_emailS"])) buyer.email = fc["buyer_emailS"];
            else buyer.email = "";

            string phoneRegion = "";
            string phoneNumber = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_contactS"])) phoneRegion = fc["buyer_contactS"];
            else phoneRegion = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_mobileS"])) phoneNumber = fc["buyer_mobileS"];
            else phoneNumber = "";
            buyer.phoneRegion = phoneRegion;
            buyer.phone = phoneNumber;

            if (!string.IsNullOrWhiteSpace(fc["buyer_addressS"])) buyer.address = fc["buyer_addressS"];
            else buyer.address = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_countryS"])) buyer.country = fc["buyer_countryS"];
            else buyer.country = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_cityS"])) buyer.city = fc["buyer_cityS"];
            else buyer.city = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_stateS"])) buyer.state = fc["buyer_stateS"];
            else buyer.state = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_postcodeS"])) buyer.zipCode = fc["buyer_postcodeS"];
            else buyer.zipCode = ""; buyer.addressCombine = buyer.address;
            if (!string.IsNullOrWhiteSpace(buyer.city)) buyer.addressCombine += ", " + buyer.city + ",";
            if (!string.IsNullOrWhiteSpace(buyer.state)) buyer.addressCombine += " " + buyer.state;
            if (!string.IsNullOrWhiteSpace(buyer.zipCode)) buyer.addressCombine += " " + buyer.zipCode;
            if (!string.IsNullOrWhiteSpace(buyer.country)) buyer.addressCombine += ", " + buyer.country;

            orderModel.Order.orderType = 1;
            string storeId = fc["buyer_pickupS"];

            WSInventoryClient clientIventory = new WSInventoryClient();
            orderModel.Order.inventory = clientIventory.WSDetailInventory(storeId, siteID);
            orderModel.Order.orderCode = orderModel.Order.inventory.index.ToString() + "-" + DateTime.UtcNow.ToString("ddMMyyyyHHmmss");
            clientIventory.Close();
        }

        private static void SetDeliveryCustomersByDataInForm(FormCollection fc, string siteID, OrderModel orderModel, Customer buyer, Customer recipient)
        {
            #region Input Buyer
            if (!string.IsNullOrWhiteSpace(fc["buyer_firstname"])) buyer.firstName = fc["buyer_firstname"];
            else buyer.firstName = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_lastname"])) buyer.lastName = fc["buyer_lastname"];
            else buyer.lastName = "";
            buyer.fullName = buyer.firstName + " " + buyer.lastName;

            if (!string.IsNullOrWhiteSpace(fc["buyer_email"])) buyer.email = fc["buyer_email"];
            else buyer.email = "";

            string phoneRegion = "";
            string phoneNumber = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_contact"])) phoneRegion = fc["buyer_contact"];
            else phoneRegion = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_mobile"])) phoneNumber = fc["buyer_mobile"];
            else phoneNumber = "";
            buyer.phoneRegion = phoneRegion;
            buyer.phone = phoneNumber;

            if (!string.IsNullOrWhiteSpace(fc["buyer_address"])) buyer.address = fc["buyer_address"];
            else buyer.address = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_country"])) buyer.country = fc["buyer_country"];
            else buyer.country = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_city"])) buyer.city = fc["buyer_city"];
            else buyer.city = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_state"])) buyer.state = fc["buyer_state"];
            else buyer.state = "";
            if (!string.IsNullOrWhiteSpace(fc["buyer_postcode"])) buyer.zipCode = fc["buyer_postcode"];
            else buyer.zipCode = "";
            buyer.addressCombine = buyer.address;
            if (!string.IsNullOrWhiteSpace(buyer.city)) buyer.addressCombine += ", " + buyer.city + ",";
            if (!string.IsNullOrWhiteSpace(buyer.state)) buyer.addressCombine += " " + buyer.state;
            if (!string.IsNullOrWhiteSpace(buyer.zipCode)) buyer.addressCombine += " " + buyer.zipCode;
            if (!string.IsNullOrWhiteSpace(buyer.country)) buyer.addressCombine += ", " + buyer.country;
            #endregion

            #region Input Recipient
            if (!string.IsNullOrWhiteSpace(fc["recipient_firstname"])) recipient.firstName = fc["recipient_firstname"];
            else recipient.firstName = "";
            if (!string.IsNullOrWhiteSpace(fc["recipient_lastname"])) recipient.lastName = fc["recipient_lastname"];
            else recipient.lastName = "";
            recipient.fullName = recipient.firstName + " " + recipient.lastName;

            if (!string.IsNullOrWhiteSpace(fc["recipient_email"])) recipient.email = fc["recipient_email"];
            else recipient.email = "";

            string phoneRegionR = "";
            string phoneNumberR = "";
            if (!string.IsNullOrWhiteSpace(fc["recipient_contact"])) phoneRegionR = fc["recipient_contact"];
            else phoneRegionR = "";
            if (!string.IsNullOrWhiteSpace(fc["recipient_mobile"])) phoneNumberR = fc["recipient_mobile"];
            else phoneNumberR = "";
            recipient.phoneRegion = phoneRegionR;
            recipient.phone = phoneNumberR;

            if (!string.IsNullOrWhiteSpace(fc["recipient_address"])) recipient.address = fc["recipient_address"];
            else recipient.address = "";
            //if (!string.IsNullOrWhiteSpace(fc["country"])) recipient.country = fc["country"];
            //else recipient.country = "";
            if (!string.IsNullOrWhiteSpace(fc["recipient_city"])) recipient.city = fc["recipient_city"];
            else recipient.city = "";
            if (!string.IsNullOrWhiteSpace(fc["area"])) recipient.state = fc["area"];
            else recipient.state = "";
            if (!string.IsNullOrWhiteSpace(fc["recipient_postcode"])) recipient.zipCode = fc["recipient_postcode"];
            else recipient.zipCode = "";
            recipient.addressCombine = recipient.address;
            if (!string.IsNullOrWhiteSpace(recipient.city)) recipient.addressCombine += ", " + recipient.city + ",";
            if (!string.IsNullOrWhiteSpace(recipient.state)) recipient.addressCombine += " " + recipient.state;
            if (!string.IsNullOrWhiteSpace(recipient.zipCode)) recipient.addressCombine += " " + recipient.zipCode;
            if (!string.IsNullOrWhiteSpace(recipient.country)) recipient.addressCombine += ", " + recipient.country;
            #endregion

            orderModel.Order.orderType = 0;
            orderModel.Order.orderCode = "1-" + DateTime.UtcNow.ToString("ddMMyyyyHHmmss");

            WSInventoryClient clientInventory = new WSInventoryClient();
            Inventory[] ListInventory = clientInventory.WSGetAll(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, siteID);
            Inventory inventory = ListInventory.Where(r => r.index == 1).FirstOrDefault();
            orderModel.Order.inventory = inventory;
            clientInventory.Close();
        }

        public ActionResult PaymentSubmit(string transactionId, string orderId)
        {
            GetThemeInfo();
            string siteID = GetSiteID();

            PaymentModel paymentModel = new PaymentModel();
            Com.SmoovPOS.Model.PaymentModel paymentGateway = new Com.SmoovPOS.Model.PaymentModel();
            WSCheckoutSettingClient clientCheckoutSetting = new WSCheckoutSettingClient();
            paymentModel.accountSmoovPay = clientCheckoutSetting.GetDetailAccountSmoovpay(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAccountSmoovPay, siteID);
            clientCheckoutSetting.Close();
            //WSPaymentGatewayClient client = new WSPaymentGatewayClient();
            //try
            //{
            //    paymentGateway = client.GetPaymentGateway();
            //}
            //catch
            //{

            //}
            //client.Close();
            paymentGateway.CancelPage = "PageCancel";
            paymentGateway.SandboxSmoovUrl = System.Configuration.ConfigurationManager.AppSettings[SmoovPOS.Common.ConstantSmoovs.AppSettings.UrlSandBox];
            paymentGateway.SmoovUrl = System.Configuration.ConfigurationManager.AppSettings[SmoovPOS.Common.ConstantSmoovs.AppSettings.UrlSmoovPay];
            paymentGateway.StrUrl = "Strhandle";
            paymentGateway.Status = 1;
            paymentGateway.SuccessPage = "PageSuccess";

            WSInventoryClient clientIventory = new WSInventoryClient();
            Country[] countrys = clientIventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, siteID);
            clientIventory.Close();

            //Get and set merchant's currency
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            ViewBag.currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteID).Currency;
            merchantGeneralSettingClient.Close();

            Session["OrderId"] = orderId;
            if ((Object)Session[ConstantSmoovs.CardOrder.myOrder] != null) paymentModel.mycart = Session[ConstantSmoovs.CardOrder.myOrder] as MyCart;
            paymentModel.payConfig = paymentGateway;
            paymentModel.transactionId = transactionId;

            return View(paymentModel);
        }

        public ActionResult PageSuccess()
        {
            string siteID = GetSiteID();
            string orderId = "";
            if ((Object)Session["OrderId"] != null) orderId = Session["OrderId"].ToString();
            WSOrderClient wsOrder = new WSOrderClient();
            Order order = wsOrder.WSGetOrder(orderId, siteID);
            order.paymentStatus = 3;
            order.paidDate = DateTime.UtcNow;
            order.orderStatus = 1;
            order.display = true;
            // update array payment
            List<PaymentItem> listPayment = new List<PaymentItem>();
            PaymentItem paymentItem = new PaymentItem();
            paymentItem.paymentType = "SmoovPay";
            paymentItem.display = true;
            paymentItem.createdDate = DateTime.UtcNow;
            paymentItem.charged = order.grandTotal;
            listPayment.Add(paymentItem);
            order.arrayPayment = listPayment;
            // end
            wsOrder.WSUpdateOrder(order, siteID);
            wsOrder.Close();
            OrderModel orderModel = new OrderModel();
            orderModel.Order = order;

            //update quantity productItem online store
            PaymentModel paymentModel = new PaymentModel();
            if ((Object)Session[ConstantSmoovs.CardOrder.myOrder] != null) paymentModel.mycart = Session[ConstantSmoovs.CardOrder.myOrder] as MyCart;

            //Update quantity product from store
            WSProductItemClient wsProductItem = new WSProductItemClient();
            wsProductItem.WSUpdateQuantityProductItemByStore(paymentModel.mycart.listProductItems.ToArray(), ConstantSmoovs.Stores.OnlineStore, siteID);
            wsProductItem.Close();

            //Reset myCart's item after checkout successfully
            #region Reset myCart's Item after checkout successfully
            if (Session[ConstantSmoovs.Users.userName] != null && !string.IsNullOrEmpty(Session[ConstantSmoovs.Users.userName].ToString()))
            {
                //Get member's cart from coughbase
                WSMyCartClient client = new WSMyCartClient();
                MyCart myCart = client.WSGetMyCartByEmail(ConstantSmoovs.CouchBase.DesignDocMyCart, ConstantSmoovs.CouchBase.ViewNameGetDetailMyCartByEmail, siteID, GetEmailMember());

                //Reset values
                myCart.listProductItems = ResetMyCartAfterCheckOut(myCart.listProductItems);
                myCart.remark = "";
                myCart.receiptValue = new ReceiptValue();
                myCart.modifiedDate = DateTime.UtcNow;

                //Update values in coughbase
                client.WSUpdateMyCart(myCart, siteID);
                client.Close();

                //Update session myCart
                Session[ConstantSmoovs.CardOrder.myCart] = myCart;
            }
            else
            {
                MyCart myCart = Session[ConstantSmoovs.CardOrder.myCart] as MyCart;

                //Reset values
                myCart.listProductItems = ResetMyCartAfterCheckOut(myCart.listProductItems);
                myCart.remark = "";
                myCart.receiptValue = new ReceiptValue();
                myCart.modifiedDate = DateTime.UtcNow;

                //Update session myCart
                Session[ConstantSmoovs.CardOrder.myCart] = myCart;
            }
            #endregion
            Session["OrderId"] = null;
            Session[ConstantSmoovs.CardOrder.myOrder] = null;

            GetThemeInfo();
            return View(orderModel);
        }

        public ActionResult PageCancel()
        {
            GetThemeInfo();
            return View();
        }

        private List<ProductItem> GetMyCartCheckedOutItems(List<ProductItem> listMyCartItems)
        {
            List<ProductItem> returnValue = new List<ProductItem>();

            foreach (var item in listMyCartItems)
            {
                foreach (var variant in item.arrayVarient)
                {
                    if (variant.isCheckedToCheckOut)
                    {
                        if (returnValue.FindIndex(i => i._id == item._id) == -1)
                        {
                            //Initialize new ProductItem
                            ProductItem temp = new ProductItem(item);

                            //Add new product item into list
                            returnValue.Add(temp);
                        }

                        //Add new variant into product item in list
                        returnValue.FirstOrDefault(i => i._id == item._id).arrayVarient.Add(variant);
                    }
                }
            }

            return returnValue;
        }

        private List<ProductItem> ResetMyCartAfterCheckOut(List<ProductItem> oldlList)
        {
            List<ProductItem> returnValue = oldlList;

            MyCart myOrder = (MyCart)Session[ConstantSmoovs.CardOrder.myOrder];
            List<ProductItem> orderList = myOrder.listProductItems;

            for (int i = returnValue.Count - 1; i >= 0; i--)
            //foreach (var item in oldlList)
            {
                for (int v = returnValue[i].arrayVarient.Count - 1; v >= 0; v--)
                {
                    if (returnValue[i].arrayVarient[v].isCheckedToCheckOut)
                    {
                        //Subtract quantity of variant of product item
                        returnValue.FirstOrDefault(c => c._id == returnValue[i]._id).arrayVarient.FirstOrDefault(r => r.sku == returnValue[i].arrayVarient[v].sku).quantity
                            -= orderList.FirstOrDefault(c => c._id == returnValue[i]._id).arrayVarient.FirstOrDefault(r => r.sku == returnValue[i].arrayVarient[v].sku).quantity;

                        //Remove this variant out of item
                        if (returnValue.FirstOrDefault(c => c._id == returnValue[i]._id).arrayVarient.FirstOrDefault(r => r.sku == returnValue[i].arrayVarient[v].sku).quantity <= 0)
                        {
                            returnValue.FirstOrDefault(c => c._id == returnValue[i]._id).arrayVarient.RemoveAll(r => r.sku == returnValue[i].arrayVarient[v].sku);
                        }
                    }
                }

                if (returnValue[i].arrayVarient.Count == 0) returnValue.RemoveAt(i);
            }

            return returnValue;
        }
        #endregion

        [HttpPost]
        public Boolean ValidationEmail(string email)
        {
            RegexUtilites regex = new RegexUtilites();
            Boolean isValid = regex.IsValidEmail(email);
            return isValid;
        }

        [HttpPost]
        public ActionResult LoadAreas(string id)
        {
            //Thread.Sleep(3000);
            if (!string.IsNullOrEmpty(id))
            {
                WSDeliveryCountryClient clientCountry = new WSDeliveryCountryClient();
                var country = clientCountry.WSGetDeliveryCountry(id, GetSiteID());
                clientCountry.Close();
                if (country != null && country.arrayArea != null && country.arrayArea.Count() > 0)
                {
                    var arrayArea = LoadArea(GetSiteID(), country);
                    //var arrayService = LoadServiceSelectedByArea(country._id, arrayArea[0].Text);

                    return Json(new { success = true, data = arrayArea }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult LoadAreaSettings(string id)
        {
            //Thread.Sleep(3000);
            if (!string.IsNullOrEmpty(id))
            {
                WSDeliveryCountryClient clientCountry = new WSDeliveryCountryClient();
                var country = clientCountry.WSGetDeliveryCountry(id, GetSiteID());
                clientCountry.Close();
                if (country != null && country.arrayArea != null && country.arrayArea.Count() > 0)
                {
                    var arrayArea = LoadAreaSetting(GetSiteID(), country);
                    //var arrayService = LoadServiceSelectedByArea(country._id, arrayArea[0].Text);

                    return Json(new { success = true, data = arrayArea }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeliveryService(string id, string area)
        {
            //Thread.Sleep(3000);
            var model = LoadServiceSelectedByArea(id, area);

            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            ViewBag.currency = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, GetSiteID()).Currency;

            return PartialView(model);
        }

        private static List<SelectListItem> LoadArea(string siteID, DeliveryCountry country)
        {
            List<SelectListItem> arrayArea = new List<SelectListItem>();

            arrayArea = (from a in country.arrayArea
                         select new SelectListItem()
                         {
                             Value = a.areaName,
                             Text = a.areaName
                         }).OrderBy(a => a.Text).ToList();

            return arrayArea;
        }
        private static List<SelectListItem> LoadAreaSetting(string siteID, DeliveryCountry country)
        {
            List<SelectListItem> arrayArea = new List<SelectListItem>();

            WSDeliverySettingClient clientSetting = new WSDeliverySettingClient();
            var deliverySetting = clientSetting.WSGetDeliverySettingByCountry(ConstantSmoovs.CouchBase.DesignDocDeliverySetting, ConstantSmoovs.CouchBase.DeliverySettingViewByCountry, siteID, country._id);
            clientSetting.Close();

            //var listArea = new List<AreaShipping>() { };
            List<string> listArea = new List<string>();
            if (deliverySetting != null && deliverySetting.arrayServiceDelivery.Count > 0)
            {
                foreach (var item in deliverySetting.arrayServiceDelivery)
                {
                    var arrayAreaShipping = item.arrayAreaShipping;
                    if (arrayAreaShipping != null && arrayAreaShipping.Count > 0 && arrayAreaShipping.Where(a => a.isSelect).Count() > 0)
                    {
                        var arrayAreaShippings = arrayAreaShipping.Where(a => a.isSelect == true && !listArea.Contains(a.areaName)).Select(a => a.areaName).ToList();
                        if (arrayAreaShippings.Count > 0)
                        {
                            listArea.AddRange(arrayAreaShippings);
                        }
                    }

                }
            }
            listArea = listArea.Distinct().OrderBy(a => a).ToList();

            arrayArea = (from a in listArea
                         select new SelectListItem()
                         {
                             Value = a,
                             Text = a
                         }).OrderBy(a => a.Text).ToList();

            return arrayArea;
        }
        private List<ServiceDelivery> LoadServiceSelectedByArea(string countryId, string areaName)
        {
            WSDeliverySettingClient clientSetting = new WSDeliverySettingClient();
            var deliverySetting = clientSetting.WSGetDeliverySettingByCountry(ConstantSmoovs.CouchBase.DesignDocDeliverySetting, ConstantSmoovs.CouchBase.DeliverySettingViewByCountry, GetSiteID(), countryId);
            clientSetting.Close();
            List<ServiceDelivery> arrayService = new List<ServiceDelivery>();

            if (deliverySetting != null && deliverySetting.arrayServiceDelivery.Count > 0)
            {
                foreach (var item in deliverySetting.arrayServiceDelivery)
                {
                    if (item.arrayAreaShipping.Count() > 0)
                    {
                        var areaContain = item.arrayAreaShipping.Where(a => a.isSelect && a.areaName == areaName).ToList();

                        if (areaContain.Count > 0 && areaContain != null)
                        {
                            item.shippingPrice = areaContain.FirstOrDefault().areaPrice;
                            arrayService.Add(item);
                        }
                    }
                }
            }
            return arrayService;
        }
    }
}
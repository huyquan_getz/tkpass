﻿using Microsoft.AspNet.SignalR;
using SmoovPOS.Utility.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI
{
    public class ChatHub : Hub
    {
        #region Data Members

        static List<UserDetail> ConnectedUsers = new List<UserDetail>();
        static List<MessageDetail> CurrentMessage = new List<MessageDetail>();

        #endregion

        #region Methods
        public void Send(string name, string message)
        {
            // Call the addNewMessageToPage method to update clients.
            Clients.All.addNewMessageToPage(name, message);
        }

        public void Connect(string userName, string stationId)
        {
            var id = Context.ConnectionId;


            if (ConnectedUsers.Count(x => x.ConnectionId == id) == 0)
            {
                var userDetail = new UserDetail() { ConnectionId = id, StationId = stationId, UserName = userName };
                ConnectedUsers.Add(userDetail);

                // send to caller
                Clients.Caller.onConnected(id, userName, new List<UserDetail>() { userDetail }, CurrentMessage);

                // send to all except caller client
                Clients.AllExcept(id).onNewUserConnected(stationId, id, userName);

            }

        }

        public void SendMessageToAll(string userName, string message)
        {
            // store last 100 messages in cache
            AddMessageinCache(userName, message);

            var users = ConnectedUsers.FirstOrDefault(x => x.UserName == userName);
            if (users == null)
            {// Broad cast message
                Clients.All.messageReceived(userName, message);
            }
            else
            {
                UserDetail toUser = new UserDetail();
                UserDetail fromUser = new UserDetail();

                if (users != null)
                {
                    if (toUser == null)
                    {
                        toUser = users;
                    }
                    if (fromUser == null)
                    {
                        fromUser = users;
                    }
                }
                sendToGroupStation(users.ConnectionId, message, users.ConnectionId, toUser, fromUser);
            }
        }

        public void SendPrivateMessage(string toUserId, string message)
        {
            string fromUserId = Context.ConnectionId;

            var toUser = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == toUserId);
            var fromUser = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == fromUserId);

            var users = ConnectedUsers.FirstOrDefault(x => x.StationId == toUserId);
            if (users != null)
            {
                if (toUser == null)
                {
                    toUser = users;
                }
                if (fromUser == null)
                {
                    fromUser = users;
                }
                if (string.IsNullOrEmpty(fromUserId))
                {
                    fromUserId = toUserId;
                }
            }

            sendToGroupStation(toUserId, message, fromUserId, toUser, fromUser);

        }

        private void sendToGroupStation(string toUserId, string message, string fromUserId, UserDetail toUser, UserDetail fromUser)
        {
            if (toUser != null && fromUser != null)
            {
                var toUsers = ConnectedUsers.Where(x => x.StationId == toUser.StationId).ToList();
                foreach (var item in toUsers)
                {
                    // send to 
                    Clients.Client(item.ConnectionId).sendPrivateMessage(fromUserId, fromUser.UserName, message);
                }

                // send to caller user
                Clients.Caller.sendPrivateMessage(toUserId, fromUser.UserName, message);
            }
        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                ConnectedUsers.Remove(item);
                if (CacheHelper.CustomerInfos != null)
                {
                    foreach (var itemCustomer in CacheHelper.CustomerInfos)
                    {
                        if (itemCustomer != null && itemCustomer.customerHost != null &&
                            itemCustomer._id == item.StationId &&
                            itemCustomer.customerHost.email.ToLower() == item.UserName.ToLower())
                        {
                            CacheHelper.CustomerInfos.Remove(itemCustomer);
                            break;
                        }
                    }
                }

                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(item.StationId, id, item.UserName);

            }

            return base.OnDisconnected(stopCalled);
        }


        #endregion

        #region private Messages

        private void AddMessageinCache(string userName, string message)
        {
            CurrentMessage.Add(new MessageDetail { UserName = userName, Message = message });

            if (CurrentMessage.Count > 100)
                CurrentMessage.RemoveAt(0);
        }

        #endregion
    }
    public class UserDetail
    {
        public string ConnectionId { get; set; }
        public string StationId { get; set; }
        public string UserName { get; set; }
    }
    public class MessageDetail
    {

        public string UserName { get; set; }

        public string Message { get; set; }

    }
}
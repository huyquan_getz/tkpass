﻿using AutoMapper;
using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Models;
using SmoovPOS.UI.Models;
using SmoovPOS.UI.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.AutoMapperProfiles
{
    public class ModelMappingProfile : AutoMapper.Profile
    {
        public override string ProfileName
        {
            get { return "ModelMappingProfile"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<CustomerViewModel, Customer>();
            Mapper.CreateMap<Customer, CustomerViewModel>();
            Mapper.CreateMap<CustomerChangePasswordViewModel, Customer>();
            Mapper.CreateMap<Customer, CustomerChangePasswordViewModel>();

            Mapper.CreateMap<Station, TableOrderViewModel>();
            Mapper.CreateMap<TableOrderViewModel, Station>();
            Mapper.CreateMap<TableOrderViewModel, CustomerInfo>();
            Mapper.CreateMap<TableOrderViewModel, Customer>();
            Mapper.CreateMap<TableOrderViewModel, TableOrderModel>();
            Mapper.CreateMap<TableOrderModel, TableOrderViewModel>();
            Mapper.CreateMap<TableOrderViewModel, TableOrderViewModel>();
            Mapper.CreateMap<TableOrderingSetting, TableOrderingSettingModel>();
        }
    }
}
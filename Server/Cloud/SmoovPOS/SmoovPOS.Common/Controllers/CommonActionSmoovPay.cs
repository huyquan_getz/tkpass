﻿using Com.SmoovPOS.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SmoovPOS.Common.WSCheckoutSettingReference;
using SmoovPOS.Common.WSInventoryReference;
using SmoovPOS.Entity.ApiSmoovPay;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace SmoovPOS.Common.Controllers
{
    public class CommonActionSmoovPay
    {
        /// <summary>
        /// Gets the authorization.
        /// </summary>
        /// <param name="merchantID">The merchant identifier.</param>
        /// <returns></returns>
        /// <author>"Hung.Do"</author>
        /// <datetime>3/30/2015-2:20 AM</datetime>
        public static string GetAuthorization(string merchantID)
        {
            if (merchantID != null)
            {
                WSCheckoutSettingClient client = new WSCheckoutSettingClient();
                var accountSmoovPay = client.GetDetailAccountSmoovpay(Common.ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAccountSmoovPay, merchantID);
                if (accountSmoovPay != null && string.IsNullOrEmpty(accountSmoovPay.authorization))
                {
                    var result = CallApiAuthorization(accountSmoovPay.emailAddress, accountSmoovPay.password);
                    if (result.status == ConstantSmoovs.ResponseStatusApiSmoovPay.Succcess)
                    {
                        accountSmoovPay.authorization = result.result.ToString();
                        client.UpdateAccountSmoovpay(merchantID, accountSmoovPay);
                        client.Close();
                        return accountSmoovPay.authorization;
                    }
                }
                else if (accountSmoovPay != null)
                {
                    client.Close();
                    return accountSmoovPay.authorization;
                }

                client.Close();
                return null;

            }
            else
            {
                return null;

            }


        }

        /// <summary>
        /// Update the authorization.
        /// </summary>
        /// <param name="merchantID">The merchant identifier.</param>
        /// <returns></returns>
        /// <author>"Hung.Do"</author>
        /// <datetime>3/30/2015-2:20 AM</datetime>

        public static bool UpdateAuthorization(string merchantID)
        {
            if (merchantID != null)
            {
                WSCheckoutSettingClient client = new WSCheckoutSettingClient();
                var accountSmoovPay = client.GetDetailAccountSmoovpay(Common.ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAccountSmoovPay, merchantID);
                if (accountSmoovPay != null)
                {
                    var result = CommonActionSmoovPay.CallApiAuthorization(accountSmoovPay.emailAddress, accountSmoovPay.password);
                    if (result.status == ConstantSmoovs.ResponseStatusApiSmoovPay.Succcess)
                    {
                        accountSmoovPay.authorization = result.result.ToString();
                        client.UpdateAccountSmoovpay(merchantID, accountSmoovPay);
                        client.Close();
                        return true;
                    }
                }
                client.Close();
                return false;
            }
            else
            {
                return false;
            }


        }

        //create online store default
        public static string GetOneTimeToken(string apiAccessKey)
        {
            using (var client = new WebClient())
            {
                var url = string.Format(ConstantSmoovs.URLSmoovPayServer.OneTimeToken2, System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.UrlApiSmoovPay]); 
                var values = new NameValueCollection
                {
                    { "apiAccessKey", apiAccessKey }
                };
                try
                {
                    var responsebytes = client.UploadValues(url, "POST", values);
                    var responsebody = Encoding.UTF8.GetString(responsebytes);
                    var JsonResponse = JsonConvert.DeserializeObject<ApiSmoovPay>(responsebody);
                    if (JsonResponse.status == ConstantSmoovs.ResponseStatusApiSmoovPay.Succcess)
                    {
                        var jObject = JObject.Parse(responsebody);
                        return jObject["result"].ToString();
                    }
                    else
                    {
                        return null;
                    }
                }
                catch
                {
                    return null;
                }
              
            }

        }
        public static ApiSmoovPay CallApiAuthorization(string username, string password)
        {
            using (var client = new WebClient())
            {
                var url = string.Format(ConstantSmoovs.URLSmoovPayServer.RequestAuthorization, System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.UrlApiSmoovPay]); ;

                var values = new NameValueCollection
                {
                    { "username", username },
                    { "password", password }
                };
                try
                {
                    var responsebytes = client.UploadValues(url, "POST", values);
                    var responsebody = Encoding.UTF8.GetString(responsebytes);
                    var JsonResponse = JsonConvert.DeserializeObject<ApiSmoovPay>(responsebody);
                    return JsonResponse;
                }
                catch
                {
                    return null;
                }
               
            }

        }
        public static ApiSmoovPay CallApiRefundOrder(string Authorization, string ReferenceCode, string RefundAmount, string Remarks="")
        {
            using (var client = new WebClient())
            {
                var url = string.Format(ConstantSmoovs.URLSmoovPayServer.RequestRefund, System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.UrlApiSmoovPay]); ;
                var values = new NameValueCollection
                {
                    { "Authorization", Authorization },
                    { "ReferenceCode", ReferenceCode },
                    { "RefundAmount", RefundAmount },
                    { "Remarks", Remarks }
                };
                try
                {
                    var responsebytes = client.UploadValues(url, "POST", values);
                    var responsebody = Encoding.UTF8.GetString(responsebytes);
                    var JsonResponse = JsonConvert.DeserializeObject<ApiSmoovPay>(responsebody);
                    //Waiting response  API Refund from Smoovpay team
                    JsonResponse.status = ConstantSmoovs.Enums.API_REFUND_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_REFUND_STATUS.Refunded).Key;
                    JsonResponse.result = "Just demo for function refund.";
                    JsonResponse.message = "Just demo for function refund.";
                    //end
                    return JsonResponse;
                }
                catch
                {
                    return null;
                }
               
            }

        }
    }
}
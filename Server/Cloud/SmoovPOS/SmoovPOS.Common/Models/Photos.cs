﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Common.UploadAmazonS3.Models
{
    public class Photos
    {
        public int ID { get; set; }
        public string ImageURL { get; set; }
        public string Caption { get; set; }
        public DateTime UploadDate { get; set; }
        public int Rank { get; set; }

    }
}
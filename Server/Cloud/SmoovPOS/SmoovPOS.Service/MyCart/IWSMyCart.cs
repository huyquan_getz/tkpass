﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.MyCart
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSMyCart" in both code and config file together.
    [ServiceContract]
    public interface IWSMyCart
    {
        [OperationContract]
        Com.SmoovPOS.Entity.MyCart WSGetMyCartByEmail(string designDoc, string viewName, string bucket, string email);
        [OperationContract]
        int WSCreateMyCart(Com.SmoovPOS.Entity.MyCart myCart, string bucket);
        [OperationContract]
        int WSUpdateMyCart(Com.SmoovPOS.Entity.MyCart myCart, string bucket);
        [OperationContract]
        int WSDeleteMyCart(string _id, string bucket);
    }
}
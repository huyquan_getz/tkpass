﻿using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.DeveloperSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Currency" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Currency.svc or Currency.svc.cs at the Solution Explorer and start debugging.
    public class Currency : IWSCurrency
    {
        public List<Com.SmoovPOS.Entity.CurrencyItem> GetAllCurrency(string designDoc, string viewName, string siteID)
        {

            IEnumerable<Com.SmoovPOS.Entity.CurrencyItem> listCurrency = Enumerable.Empty<Com.SmoovPOS.Entity.CurrencyItem>();
            try
            {
                DeveloperSettingComponent component = new DeveloperSettingComponent();
                listCurrency = component.GetAllCurrency(designDoc, viewName, siteID);
                return listCurrency.ToList();
            }
            catch
            {
                return listCurrency.ToList();
                throw new NotImplementedException();
            }
        }

        public int CreateNewCurrency(string siteID, Com.SmoovPOS.Entity.Currency currency)
        {
            DeveloperSettingComponent component = new DeveloperSettingComponent();
            return component.CreateNewCurrency(siteID, currency);
        }

        public int UpdateCurrency(string siteID, Com.SmoovPOS.Entity.Currency currency)
        {
            DeveloperSettingComponent component = new DeveloperSettingComponent();
            return component.UpdateCurrency(siteID, currency);
        }
    }
}

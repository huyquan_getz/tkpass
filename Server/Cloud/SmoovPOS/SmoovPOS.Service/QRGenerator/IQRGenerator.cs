﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.QRGenerator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IQRGenerator" in both code and config file together.
    [ServiceContract]
    public interface IQRGenerator
    {
        [OperationContract]
        string QRCodeGenerator(string Url, int width, string filename);
    }
}

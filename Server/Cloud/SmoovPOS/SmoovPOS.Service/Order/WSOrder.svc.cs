﻿using Com.SmoovPOS.Model;
using SmoovPOS.Business.Components;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
namespace SmoovPOS.Service.Order
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSOrder" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSOrder.svc or WSOrder.svc.cs at the Solution Explorer and start debugging.
    public class WSOrder : IWSOrder
    {
        public int WSCreateOrder(Com.SmoovPOS.Entity.Order order, string siteID)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.CreateOrder(order, siteID);
        }
        
        /// <summary>
        /// Update Order
        /// </summary>
        /// <param name="order">Model order</param>
        /// <returns></returns>
        /// <createdby>chuong.nguyen</createdby>
        /// <createddate></createddate>
        public int WSUpdateOrder(Com.SmoovPOS.Entity.Order order, string siteID)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.UpdateOrder(order, siteID);
        }

        public int WSDeleteAllOrder(string designDoc, string viewName, string siteID)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.DeleteAllOrder(designDoc, viewName, siteID);
        }

        public Com.SmoovPOS.Entity.Order WSGetOrder(string key, string siteID)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.GetOrder(key, siteID);
        }

        public IEnumerable<Com.SmoovPOS.Entity.Order> WSGetAllOrder(string designDoc, string viewName, string siteID)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.GetAllOrder(designDoc, viewName, siteID);
        }

        public SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel WSGetListPaidAndRefundedOrdersInPeriod(string designDoc, string viewName, string siteID, DateTime BeginDate, DateTime EndDate, bool IsNextDay, string BranchIndex)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.GetListPaidAndRefundedOrdersInPeriod(designDoc, viewName, siteID, BeginDate, EndDate, IsNextDay, BranchIndex);
        }

        public List<Com.SmoovPOS.Entity.Order> GetAllOrderByCustomerEmail(string designDoc, string viewName, string siteID, string email)
        {
            OrderComponent orderCom = new OrderComponent();
            var list =  orderCom.GetAllOrderByCustomerEmail(designDoc, viewName, siteID, email);
            return list.ToList();
        }

        /// <summary>
        /// WS get order by search condition.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="searchField">The search field.</param>
        /// <param name="searchKey">The search key.</param>
        /// <returns></returns>
        /// <createdby>nhu.dang</createdby>
        /// <createddate>12/12/2014-9:07 AM</createddate>
        public IEnumerable<Com.SmoovPOS.Entity.Order> WSGetOrderBySearchCondition(string designDoc, string viewName, SearchOrderModel searchOrderModel, string siteID)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.GetOrderBySearchCondition(designDoc, viewName, searchOrderModel, siteID);
        }

        public IEnumerable<Com.SmoovPOS.Entity.Order> WSGetAllOrderDelivery(string designDoc, string viewName, string siteID)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.GetAllOrderDelivery(designDoc, viewName, siteID);
        }
        public IEnumerable<Com.SmoovPOS.Entity.Order> WSGetAllOrderSelfCollect(string designDoc, string viewName, string siteID)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.GetAllOrderSeflCollect(designDoc, viewName, siteID);
        }
        public IEnumerable<Com.SmoovPOS.Entity.Order> WSGetAllOrderCancel(string designDoc, string viewName, string siteID)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.GetAllOrderCancel(designDoc, viewName, siteID);
        }

        public IEnumerable<Com.SmoovPOS.Entity.Order> WSGetAllOrderOrderPaging(string designDoc, string viewName, SearchOrderModel searchOrderModel, EntityPaging entityPaging, string siteID)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.GetAllOrderPaging(designDoc, viewName,searchOrderModel, entityPaging, siteID);
        }
        public SearchOrderModel WSGetOrderByFilter(SearchOrderModel searchOrderModel)
        {
            OrderComponent orderCom = new OrderComponent();
            return orderCom.GetOrder(searchOrderModel);
        }
    }
}

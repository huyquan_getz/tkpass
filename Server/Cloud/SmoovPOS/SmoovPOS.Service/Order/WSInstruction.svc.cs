﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Order
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSInstruction" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSInstruction.svc or WSInstruction.svc.cs at the Solution Explorer and start debugging.
    public class WSInstruction : IWSInstruction
    {

        public int WSCreateInstruction(Instruction instruction, string siteID)
        {
            InstructionComponent instructionComponent = new InstructionComponent();
            return instructionComponent.CreateInstruction(instruction, siteID);
        }

        public Instruction WSDetailInstruction(string _id, string siteID)
        {
            InstructionComponent instructionComponent = new InstructionComponent();
            return instructionComponent.DetailInstruction(_id, siteID);
        }

        public int WSUpdateInstruction(Instruction instruction, string siteID)
        {
            InstructionComponent instructionComponent = new InstructionComponent();
            return instructionComponent.UpdateInstruction(instruction, siteID);
        }

        public int WSDeleteInstruction(string _id, string siteID)
        {
            InstructionComponent instructionComponent = new InstructionComponent();
            return instructionComponent.DeleteInstruction(_id, siteID);
        }

        public IEnumerable<Instruction> WSGetAllInstruction(string designDoc, string viewName, string siteID)
        {
            InstructionComponent instructionComponent = new InstructionComponent();
            return instructionComponent.GetAllServiceShipping(designDoc, viewName, siteID);
        }


        public int WSUpdateInstruction(ServiceShipping serviceShipping, string siteID)
        {
            throw new NotImplementedException();
        }
    }
}

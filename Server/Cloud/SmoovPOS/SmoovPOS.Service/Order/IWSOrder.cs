﻿using Com.SmoovPOS.Model;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace SmoovPOS.Service.Order
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSOrder" in both code and config file together.
    [ServiceContract]
    public interface IWSOrder
    {
        [OperationContract]
        int WSCreateOrder(Com.SmoovPOS.Entity.Order order, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Order WSGetOrder(string key, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Order> WSGetAllOrder(string designDoc, string viewName, string siteID);

        [OperationContract]
        SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel WSGetListPaidAndRefundedOrdersInPeriod(string designDoc, string viewName, string siteID, DateTime BeginDate, DateTime EndDate, bool IsNextDay, string BranchIndex);

        [OperationContract]
        List<Com.SmoovPOS.Entity.Order> GetAllOrderByCustomerEmail(string designDoc, string viewName, string siteID, string email);

        [OperationContract]
        int WSDeleteAllOrder(string designDoc, string viewName, string siteID);

        /// <summary>
        /// WS get order by search condition.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="searchField">The search field.</param>
        /// <param name="searchKey">The search key.</param>
        /// <returns></returns>
        /// <createdby>nhu.dang</createdby>
        /// <createddate>12/12/2014-9:07 AM</createddate>
        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Order> WSGetOrderBySearchCondition(string designDoc, string viewName, SearchOrderModel searchOrderModel, string siteID);


        [OperationContract]
        int WSUpdateOrder(Com.SmoovPOS.Entity.Order order, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Order> WSGetAllOrderDelivery(string designDoc, string viewName, string siteID);
        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Order> WSGetAllOrderSelfCollect(string designDoc, string viewName, string siteID);
        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Order> WSGetAllOrderCancel(string designDoc, string viewName, string siteID);
        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Order> WSGetAllOrderOrderPaging(string designDoc, string viewName, SearchOrderModel searchOrderModel, EntityPaging entityPaging, string siteID);
        [OperationContract]
        SearchOrderModel WSGetOrderByFilter(SearchOrderModel searchOrderModel);
    }
}

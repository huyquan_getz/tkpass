﻿using SmoovPOS.Entity;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Inventory
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSInventory" in both code and config file together.
    [ServiceContract]
    public interface IWSInventory
    {
        [OperationContract]
        //C section: Create
        int WSCreateInventory(Com.SmoovPOS.Entity.Inventory inventory, string siteID);

        //R section: Read
        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Inventory> WSGetAll(string designDoc, string viewName, string siteID);

        [OperationContract]
        SearchInventoryModel WSGetAllInventory(string designDoc, string viewName, string siteID, NewPaging paging);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Country> WSGetAllCountry(string designDoc, string viewName, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Inventory> WSGetAllActiveBranchesForTableOrder(string designDoc, string viewName, string siteID);

        [OperationContract]
        int WSCountAllInventory(string designDoc, string viewName, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Inventory WSDetailInventory(string _id, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Inventory WSDetailInventoryByIndex(string designDoc, string viewName, string siteID, int BranchIndex);

        //U section: Update
        [OperationContract]
        int WSUpdateInventory(Com.SmoovPOS.Entity.Inventory inventory, string siteID);

        //D section: Delete
        [OperationContract]
        int WSDeleteInventory(string _id, string siteID);
    }
}

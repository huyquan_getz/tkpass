﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SmoovPOS.Business.Components;
using SmoovPOS.Entity.Models;
using SmoovPOS.Entity;

namespace SmoovPOS.Service.Inventory
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSInventory" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSInventory.svc or WSInventory.svc.cs at the Solution Explorer and start debugging.
    public class WSInventory : IWSInventory
    {
        //C section: Create
        public int WSCreateInventory(Com.SmoovPOS.Entity.Inventory inventory, string siteID)
        {
            int result = -1;
            try
            {
                InventoryComponent inventoryComponent = new InventoryComponent();
                result = inventoryComponent.CreateInventory(inventory, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }
        }

        //R section: Read
        public IEnumerable<Com.SmoovPOS.Entity.Inventory> WSGetAll(string designDoc, string viewName, string siteID)
        {
            IEnumerable<Com.SmoovPOS.Entity.Inventory> listInventory = Enumerable.Empty<Com.SmoovPOS.Entity.Inventory>();
            try
            {
                InventoryComponent inventoryComponent = new InventoryComponent();
                listInventory = inventoryComponent.GetAll(designDoc, viewName, siteID);
                return listInventory;
            }
            catch
            {
                return listInventory;
                throw new NotImplementedException();
            }
        }

        public SearchInventoryModel WSGetAllInventory(string designDoc, string viewName, string siteID, NewPaging paging)
        {
            SearchInventoryModel listInventory = new SearchInventoryModel();
            try
            {
                InventoryComponent inventoryComponent = new InventoryComponent();
                listInventory = inventoryComponent.GetAllInventory(designDoc, viewName, siteID, paging);
                return listInventory;
            }
            catch
            {
                return listInventory;
                throw new NotImplementedException();
            }

        }

        public IEnumerable<Com.SmoovPOS.Entity.Country> WSGetAllCountry(string designDoc, string viewName, string siteID)
        {
            IEnumerable<Com.SmoovPOS.Entity.Country> listCountry = Enumerable.Empty<Com.SmoovPOS.Entity.Country>();
            try
            {
                InventoryComponent inventoryComponent = new InventoryComponent();
                listCountry = inventoryComponent.GetAllCountry(designDoc, viewName, siteID);
                return listCountry;
            }
            catch
            {
                return listCountry;
                throw new NotImplementedException();
            }
        }

        public IEnumerable<Com.SmoovPOS.Entity.Inventory> WSGetAllActiveBranchesForTableOrder(string designDoc, string viewName, string siteID)
        {
            IEnumerable<Com.SmoovPOS.Entity.Inventory> listInventory = Enumerable.Empty<Com.SmoovPOS.Entity.Inventory>();
            try
            {
                InventoryComponent inventoryComponent = new InventoryComponent();
                //Get all branches (inventories)
                listInventory = inventoryComponent.GetAll(designDoc, viewName, siteID);
                //Don't get the "Online Shop" (that index = 1) and "Master" (that index = 0) and inactive ones (that status = false)
                listInventory = listInventory.Where(i => i.index > 1).Where(i => i.status);
                return listInventory;
            }
            catch
            {
                return listInventory;
                throw new NotImplementedException();
            }
        }

        public int WSCountAllInventory(string designDoc, string viewName, string siteID)
        {
            InventoryComponent inventoryComponent = new InventoryComponent();
            int ListInventory = inventoryComponent.CountAllInventory(designDoc, viewName, siteID);
            return ListInventory;
        }

        public Com.SmoovPOS.Entity.Inventory WSDetailInventory(string _id, string siteID)
        {
            Com.SmoovPOS.Entity.Inventory inventory = null;
            try
            {
                InventoryComponent inventoryComponent = new InventoryComponent();
                inventory = inventoryComponent.DetailInventory(_id, siteID);
                return inventory;
            }
            catch
            {
                return inventory;
                throw new NotImplementedException();
            }
        }

        public Com.SmoovPOS.Entity.Inventory WSDetailInventoryByIndex(string designDoc, string viewName, string siteID, int BranchIndex)
        {
            Com.SmoovPOS.Entity.Inventory Inventory = new Com.SmoovPOS.Entity.Inventory();
            try
            {
                InventoryComponent inventoryComponent = new InventoryComponent();
                //Get branch (inventory or store) that has index=BranchIndex excluding inactive ones (that status = false)
                Inventory = inventoryComponent.GetAll(designDoc, viewName, siteID).Where(i => i.index == BranchIndex && i.status).FirstOrDefault();
                return Inventory;
            }
            catch
            {
                return Inventory;
                throw new NotImplementedException();
            }
        }

        //U section: Update
        public int WSUpdateInventory(Com.SmoovPOS.Entity.Inventory inventory, string siteID)
        {
            int result = -1;
            try
            {
                InventoryComponent inventoryComponent = new InventoryComponent();
                result = inventoryComponent.UpdateInventory(inventory, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }
        }

        //D section: Delete
        public int WSDeleteInventory(string _id, string siteID)
        {
            int result = -1;
            try
            {
                InventoryComponent inventoryComponent = new InventoryComponent();
                result = inventoryComponent.DeleteInventory(_id, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }
        }
    }
}

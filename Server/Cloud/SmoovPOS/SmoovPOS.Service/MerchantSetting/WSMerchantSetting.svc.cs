﻿using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.MerchantSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSMerchantSetting" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSMerchantSetting.svc or WSMerchantSetting.svc.cs at the Solution Explorer and start debugging.
    public class WSMerchantSetting : IWSMerchantSetting
    {

        public IEnumerable<Com.SmoovPOS.Entity.MerchantSetting> GetAllMerchantSetting(string designDoc, string viewName, string siteID)
        {
           
            IEnumerable<Com.SmoovPOS.Entity.MerchantSetting> listMerchantSetting = Enumerable.Empty<Com.SmoovPOS.Entity.MerchantSetting>();
            try
            {
                MerchantSettingComponent component = new MerchantSettingComponent();
                listMerchantSetting = component.GetAllMerchantSetting(designDoc, viewName, siteID);
                return listMerchantSetting;
            }
            catch
            {
                return listMerchantSetting;
                throw new NotImplementedException();
            }
        }
        public Com.SmoovPOS.Entity.MerchantSetting GetMerchantSetting(string designDoc, string viewName, string siteID)
        {
            IEnumerable<Com.SmoovPOS.Entity.MerchantSetting> listMerchantSetting = Enumerable.Empty<Com.SmoovPOS.Entity.MerchantSetting>();
            Com.SmoovPOS.Entity.MerchantSetting setting = new Com.SmoovPOS.Entity.MerchantSetting();
            try
            {
                MerchantSettingComponent component = new MerchantSettingComponent();
                listMerchantSetting = component.GetAllMerchantSetting(designDoc, viewName, siteID);
                setting = listMerchantSetting.FirstOrDefault();
                return setting;
            }
            catch
            {
                return setting;
                throw new NotImplementedException();
            }
        }

        public int UpdateMerchantSetting(Com.SmoovPOS.Entity.MerchantSetting setting, string siteID)
        {
           
            try
            {
                MerchantSettingComponent component = new MerchantSettingComponent();
                return component.UpdateMerchantSetting(setting,siteID);
            }
            catch
            {
                return 0;
                throw new NotImplementedException();
            }
        }
    }
}

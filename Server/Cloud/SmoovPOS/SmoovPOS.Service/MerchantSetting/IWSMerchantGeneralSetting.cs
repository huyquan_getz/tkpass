﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.MerchantSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSMerchantGeneralSetting" in both code and config file together.
    [ServiceContract]
    public interface IWSMerchantGeneralSetting
    {
        [OperationContract]
        Com.SmoovPOS.Entity.MerchantGeneralSetting GetDetailMerchantGeneralSetting(string designDoc, string viewName, string siteID);

        [OperationContract]
        int CreateNewMerchantGeneralSetting(Com.SmoovPOS.Entity.MerchantGeneralSetting merchantGeneralSetting, string siteID);

        [OperationContract]
        int UpdateMerchantGeneralSetting(Com.SmoovPOS.Entity.MerchantGeneralSetting merchantGeneralSetting, string siteID);
    }
}

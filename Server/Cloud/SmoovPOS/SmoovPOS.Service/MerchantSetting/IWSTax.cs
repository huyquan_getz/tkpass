﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.MerchantSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSTax" in both code and config file together.
    [ServiceContract]
    public interface IWSTax
    {
        [OperationContract]
        int WSCreateTax(Tax tax, string siteId);
        [OperationContract]
        IEnumerable<Tax> GetAllTax(string designDoc, string viewName, string siteId);
        [OperationContract]
        Tax DetailTax(string id, string siteId);
        [OperationContract]
        List<TaxItem> GetTaxWithInventory(string designDoc, string viewName, string siteId, int inventoryId);
        [OperationContract]
        ServiceCharge GetServiceChargeWithInventory(string designDoc, string viewName, string siteId, int inventoryId);

        [OperationContract]
        Com.SmoovPOS.Entity.Tax GetDetailTaxByInventory(string designDoc, string viewName, string siteId, int inventoryId);
    }
}

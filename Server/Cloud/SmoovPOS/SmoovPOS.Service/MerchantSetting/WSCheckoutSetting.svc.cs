﻿using SmoovPOS.Business.Components.Settings;
using SmoovPOS.Common.SendNotify;
using SmoovPOS.Entity.DeviceToken;
using SmoovPOS.Entity.Models;
using SmoovPOS.Entity.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.MerchantSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSCheckoutSetting" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSCheckoutSetting.svc or WSCheckoutSetting.svc.cs at the Solution Explorer and start debugging.
    public class WSCheckoutSetting : IWSCheckoutSetting
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CheckoutSetting GetDetailCheckoutSetting(string designDoc, string viewName, string siteID)
        {
            CheckoutSetting checkoutSetting = null;
            try
            {
                CheckoutSettingComponent component = new CheckoutSettingComponent();
                checkoutSetting = component.GetDetailCheckoutSetting(designDoc, viewName, siteID);
                return checkoutSetting;
            }
            catch
            {
                return checkoutSetting;
                throw new NotImplementedException();
            }
        }

        public int CreateCheckoutSetting(CheckoutSetting checkoutSetting, string siteID)
        {
            CheckoutSettingComponent component = new CheckoutSettingComponent();
            return component.CreateCheckoutSetting(checkoutSetting, siteID);
        }

        public int UpdateCheckoutSetting(CheckoutSetting checkoutSetting, string siteID)
        {
            CheckoutSettingComponent component = new CheckoutSettingComponent();
            return component.UpdateCheckoutSetting(checkoutSetting, siteID);
        }
        public AccountSmoovpay GetDetailAccountSmoovpay(string designDoc, string viewName, string siteID)
        {

            AccountSmoovpay accountSmoovpay = new AccountSmoovpay();
            try
            {
                AccountSmoovPaySettingComponent component = new AccountSmoovPaySettingComponent();
                accountSmoovpay = component.GetDetailAccountSmoovPay(designDoc, viewName, siteID);
                return accountSmoovpay;
            }
            catch
            {
                return null;
                throw new NotImplementedException();
            }
        }
        public int CreateAccountSmoovpay(string siteID, AccountSmoovpay accountSmoovpay)
        {
            try
            {
                AccountSmoovPaySettingComponent component = new AccountSmoovPaySettingComponent();
                var statusCode = component.CreateAccountSmoovPay(siteID, accountSmoovpay);
                return statusCode;
            }
            catch
            {
                return -1;
                throw new NotImplementedException();
            }
        }

        public int UpdateAccountSmoovpay(string siteID, AccountSmoovpay accountSmoovpay)
        {
            try
            {
                AccountSmoovPaySettingComponent component = new AccountSmoovPaySettingComponent();
                var statusCode = component.UpdateAccountSmoovPay(siteID, accountSmoovpay);
                return statusCode;
            }
            catch
            {
                return -1;
                throw new NotImplementedException();
            }
        }

        public int WSDeleteAccountSmoovPay(string id, string siteID)
        {
            AccountSmoovPaySettingComponent component = new AccountSmoovPaySettingComponent();
            int StatusCode = component.DeleteAccountSmoovPay(id, siteID);
            return StatusCode;
        }

        public ManageDevice DetailDeviceToken(string designDoc, string viewName, string bucket)
        {
            ManageDevice model = new ManageDevice();
            DeviceTokenSettingComponent component = new DeviceTokenSettingComponent();
            model = component.DetailDeviceToken(designDoc, viewName, bucket);
            return model;
        }

        public int UpdateDeviceToken(string bucket, DeviceTokenModel device)
        {
            DeviceTokenSettingComponent component = new DeviceTokenSettingComponent();
            return component.UpdateDeviceToken(device, bucket);//bucket, designDoc, viewName, 
        }

        public int DeleteDevice(string id, string bucket)
        {
            DeviceTokenSettingComponent component = new DeviceTokenSettingComponent();
            return component.DeleteDevice(id, bucket);
        }

        public int AddDeviceToken(string bucket, string designDoc, string viewName, DeviceTokenModel device)
        {
            DeviceTokenSettingComponent component = new DeviceTokenSettingComponent();
            return component.AddDeviceToken(bucket, designDoc, viewName, device);
        }
        public bool ExistToken(string id, string bucket)
        {
            DeviceTokenSettingComponent component = new DeviceTokenSettingComponent();
            return component.ExistsToken(id, bucket);
        }
        public int UpdateManageDevice(ManageDevice manageDevice, string bucket)
        {
            DeviceTokenSettingComponent component = new DeviceTokenSettingComponent();
            return component.UpdateManageDevice(manageDevice, bucket);
        }
        /// <summary>
        /// Sends the notification.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="bucket">The bucket.</param>
        /// <param name="badge">The badge.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>4/9/2015-3:32 PM</datetime>
        public bool SendNotification(string designDoc, string viewName, string bucket, int branchIndex, string message, string appType)
        {
            try
            {
                ManageDevice model = new ManageDevice();
                DeviceTokenSettingComponent component = new DeviceTokenSettingComponent();
                model = component.DetailDeviceToken(designDoc, viewName, bucket);
                if (model != null && model.ArrayDevices.Count > 0)
                {
                    List<DeviceTokenModel> arrayDevices = new List<DeviceTokenModel>();
                    foreach (var item in model.ArrayDevices.Where(a => a.branchIndex == branchIndex && a.appType == appType).ToList())
                    {
                        item.badge++;
                        component.UpdateDeviceToken(item, bucket);

                        arrayDevices.Add(item);
                    }

                    model.ArrayDevices = arrayDevices;
                    model.bucket = bucket;
                    SendNotify.SendNotifyToApple(model, message, branchIndex, logger);
                }
            }
            catch
            {

            }
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Com.SmoovPOS.Product
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSProduct" in both code and config file together.
    [ServiceContract]
    public interface IWSProduct
    {
        [OperationContract]
        void DoWork();
        [OperationContract]
        int WSCreateProduct(Com.SmoovPOS.Entity.Product product, string siteID);
        [OperationContract]
        string WSCreateProduct_(Com.SmoovPOS.Entity.Product product, string siteID);
        [OperationContract]
        Com.SmoovPOS.Entity.Product WSDetailProduct(string _id, string siteID);

        [OperationContract]
        int WSUpdateProduct(Com.SmoovPOS.Entity.Product product, string siteID);

        [OperationContract]
        int WSDeleteProduct(string _id, string siteID);

        [OperationContract]
        int WSCountAllProduct(string designDoc, string viewName, string siteID);

        [OperationContract]
        int WSCheckSKUExist(string designDoc, string viewName, string sku, string siteID);
        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Product> WSGetAllProductActive(string designDoc, string viewName, string siteID);
    }
}

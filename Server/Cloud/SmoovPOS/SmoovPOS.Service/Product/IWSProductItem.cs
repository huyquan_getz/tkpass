﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Entity;
using SmoovPOS.Entity.Models;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace SmoovPOS.Service.Product
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSProductItem" in both code and config file together.
    [ServiceContract]
    public interface IWSProductItem
    {
        [OperationContract]
        int WSCreateProductItem(ProductItem productItem, string siteID);

        [OperationContract]
        int WSUpdateProductItem(ProductItem productItem, string siteID);

        [OperationContract]
        ProductItem WSDetailProductItem(string _id, string siteID);

        [OperationContract]
        int WSDeleteProductItem(string _id, string siteID);

        [OperationContract]
        IEnumerable<ProductItem> WSGetAllProductItem(string designDoc, string viewName, string siteID);

        [OperationContract]
        int WSCountVarients(string designDoc, string viewName, string businessID, string productID, string varientID, string siteID);

        [OperationContract]
        IEnumerable<ProductItem> WSGetAllProductItemByProductID(string designDoc, string viewName, string productID, string siteID);

        [OperationContract]
        SearchProductModel WSGetAllProductItemByStore(string designDoc, string viewName, int store, string siteID, NewPaging paging);

        [OperationContract]
        IEnumerable<ProductItem> WSGetAllActiveProductItemByStoreAndCategory(string designDoc, string viewName, string siteID, string branchName, string categoryName);

        [OperationContract]
        int WSCountAllProductItemByStore(string designDoc, string viewName, string productID, string siteID);

        [OperationContract]
        IEnumerable<ProductItem> WSGetAllProductItemByMaster(string designDoc, string viewName, string startKey, string endKey, int limit, bool allowStale, int pageCurrent, int pageGoTo, string sort, Boolean desc, string siteID);

        [OperationContract]
        int WSCountAllProductItemByMaster(string designDoc, string viewName, string siteID);

        [OperationContract]
        IEnumerable<ProductItem> WSGetAllProductItemByMasterNoLimit(string designDoc, string viewName, string siteID);

        [OperationContract]
        IEnumerable<ProductItem> WSGetProductItemByListProductID(string designDoc, string viewName, List<string> productIDs, string siteID);

        [OperationContract]
        IEnumerable<ProductItem> WSGetProductItemInDiscount(string siteID);
        [OperationContract]
        SearchProductModel WSSearchProductItem(string designDoc, string viewName, string siteID, NewPaging paging);

        [OperationContract]
        IEnumerable<ProductItem> WSGetAllProductItemOnStore(string designDoc, string viewName, string siteID);

        [OperationContract]
        int WSUpdateStatus(string[] arrID, Boolean status, string siteID);

        [OperationContract]
        int WSUpdateQuantityProductItemByStore(List<ProductItem> listProductItemsMyCart, int store, string siteID);
        [OperationContract]
        int UpdateBusinessNameProductItemByStore(string designDoc, string viewName, int store, string bucket, string businessName);
    }
}

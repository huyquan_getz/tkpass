﻿using System.Collections.Generic;
using System.ServiceModel;

namespace Com.SmoovPOS.Service.Employee
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSEmployee" in both code and config file together.
    [ServiceContract]
    public interface IWSEmployee
    {
        [OperationContract]
        int WSCreateEmployee(Com.SmoovPOS.Entity.Employee employee, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Employee WSDetailEmployee(string _id, string siteID);

        [OperationContract]
        int WSUpdateEmployee(Com.SmoovPOS.Entity.Employee employee, string siteID);

        [OperationContract]
        int WSDeleteEmployee(string _id, string siteID);

        //[OperationContract]
        //IEnumerable<Com.SmoovPOS.Entity.Employee> WSGetAllEmployee(string designDoc, string viewName, string startKey = null, string endKey = null, int limit = 25, bool allowStale = false, int pageCurrent = 0, int pageGoTo = 0, string sort = "title", Boolean desc = true);

        //[OperationContract]
        //int WSCountAllCategory(string designDoc, string viewName);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Employee> WSGetAllEmployee(string designDoc, string viewName, string siteID);

        [OperationContract]
        int WSGetNextEmployeeId(string designDoc, string viewName, string siteID);

        [OperationContract]
        int WSCheckEmailExist(string designDoc, string viewName, string email, string siteID);
        [OperationContract]
        Com.SmoovPOS.Entity.Employee WSCheckByEmailPassword(string designDoc, string viewName, string email, string pass, string siteID);
    }
}

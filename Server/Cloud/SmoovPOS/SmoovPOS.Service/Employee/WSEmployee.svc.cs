﻿using Com.SmoovPOS.Business.Components;
using Com.SmoovPOS.Service.Employee;
using System.Collections.Generic;

namespace SmoovPOS.Service.Employee
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSEmployee" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSEmployee.svc or WSEmployee.svc.cs at the Solution Explorer and start debugging.
    public class WSEmployee : IWSEmployee
    {
        public int WSCreateEmployee(Com.SmoovPOS.Entity.Employee employee, string siteID)
        {
            EmployeeComponent employeeComponent = new EmployeeComponent();
            return employeeComponent.CreateNewEmployee(employee, siteID);
        }

        public Com.SmoovPOS.Entity.Employee WSDetailEmployee(string _id, string siteID)
        {
            EmployeeComponent employeeComponent = new EmployeeComponent();
            Com.SmoovPOS.Entity.Employee employee = employeeComponent.DetailEmployee(_id, siteID);
            return employee;
        }

        public int WSUpdateEmployee(Com.SmoovPOS.Entity.Employee employee, string siteID)
        {
            EmployeeComponent employeeComponent = new EmployeeComponent();
            int statusCode = employeeComponent.UpdateEmployee(employee, siteID);
            return statusCode;
        }

        public int WSDeleteEmployee(string _id, string siteID)
        {
            EmployeeComponent employeeComponent = new EmployeeComponent();
            int statusCode = employeeComponent.DeleteEmployee(_id, siteID);
            return statusCode;
        }

        public IEnumerable<Com.SmoovPOS.Entity.Employee> WSGetAllEmployee(string designDoc, string viewName, string siteID)
        {
            EmployeeComponent employeeComponent = new EmployeeComponent();
            IEnumerable<Com.SmoovPOS.Entity.Employee> ListEmployee = employeeComponent.GetAllEmployee(designDoc, viewName, siteID);
            return ListEmployee;
        }

        public int WSGetNextEmployeeId(string designDoc, string viewName, string siteID)
        {
            EmployeeComponent employeeComponent = new EmployeeComponent();
            return employeeComponent.GetNextEmployeeId(designDoc, viewName, siteID);
        }
        public int WSCheckEmailExist(string designDoc, string viewName, string email, string siteID)
        {
            EmployeeComponent employeeComponent = new EmployeeComponent();
            return employeeComponent.CheckEmailExist(designDoc, viewName, email, siteID);
        }
        public Com.SmoovPOS.Entity.Employee WSCheckByEmailPassword(string designDoc, string viewName, string email, string pass, string siteID)
        {
            EmployeeComponent employeeComponent = new EmployeeComponent();
            return employeeComponent.CheckByEmailPassword(designDoc, viewName, email, pass, siteID);
        }
    }
}

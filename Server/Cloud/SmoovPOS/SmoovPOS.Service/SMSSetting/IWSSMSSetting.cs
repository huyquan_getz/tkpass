﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSSMSSetting" in both code and config file together.
    [ServiceContract]
    public interface IWSSMSSetting
    {
        [OperationContract]
        int WSCreateSMSSetting(SMSSetting _SMSSetting, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.SMSSetting WSDetailSMSSetting(string _id);

        [OperationContract]
        int WSUpdateSMSSetting(Com.SmoovPOS.Entity.SMSSetting _SMSSetting, string siteID);

        [OperationContract]
        int WSDeleteSMSSetting(string _id);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.SMSSetting> WSGetAllSMSSetting(string siteID);
    }
}

﻿using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSSMSSetting" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSSMSSetting.svc or WSSMSSetting.svc.cs at the Solution Explorer and start debugging.
    public class WSSMSSetting : IWSSMSSetting
    {
        public int WSCreateSMSSetting(Com.SmoovPOS.Entity.SMSSetting _SMSSetting, string bucket)
        {
            try
            {
                SMSSettingComponent component = new SMSSettingComponent();
                return component.CreateSMSSetting(_SMSSetting, bucket);
            }
            catch
            {
                throw new Exception();
            }
        }

        public Com.SmoovPOS.Entity.SMSSetting WSDetailSMSSetting(string _id)
        {
            try
            {
                SMSSettingComponent component = new SMSSettingComponent();
                return component.GetDetailSMSSetting(_id);
            }
            catch
            {
                throw new Exception();
            }
        }

        public int WSUpdateSMSSetting(Com.SmoovPOS.Entity.SMSSetting _SMSSetting, string bucket)
        {
            try
            {
                SMSSettingComponent component = new SMSSettingComponent();
                return component.UpdateSMSSetting(_SMSSetting, bucket);
            }
            catch
            {
                throw new Exception();
            }
        }

        public int WSDeleteSMSSetting(string _id)
        {
            try
            {
                SMSSettingComponent component = new SMSSettingComponent();
                return component.DeleteSMSSetting(_id);
            }
            catch
            {
                throw new Exception();
            }
        }

        public IEnumerable<Com.SmoovPOS.Entity.SMSSetting> WSGetAllSMSSetting(string bucket)
        {
            try
            {
                SMSSettingComponent component = new SMSSettingComponent();
                return component.GetAllSMSSettings(bucket);
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}

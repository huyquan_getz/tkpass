﻿using Com.SmoovPOS.UI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmoovPOS.Business.Components;
using SmoovPOS.Common;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.AuthenticationPOS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSAuthenticationPOS" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSAuthenticationPOS.svc or WSAuthenticationPOS.svc.cs at the Solution Explorer and start debugging.
    public class WSAuthenticationPOS : IWSAuthenticationPOS
    {
        //public void DoWork()
        //{
        //}
        public WSAuthenticationPOS()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public WSAuthenticationPOS(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }
        public UserManager<ApplicationUser> UserManager { get; private set; }

        public AuthenticationPOSModel WSCheckAuthenticationPOS(string businessName, string userName, string password)
        {
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(businessName))
            {
                userName = userName.Trim();
                //check login
                var user = UserManager.Find(userName, password);
                if (user != null)
                {
                    //check exist Bussiness
                    MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                    var result = merchantManagement.GetMerchantManagementByBusinessName(businessName);
                    if (result != null && result.email.ToUpper().Equals(userName.ToUpper()))
                    {
                        var CouchBaseServer = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.CouchBaseServer];
                        //var UserCouchBaseServer = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.UserCouchBaseServer];
                        //var PasswordCouchBaseServer = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.PasswordCouchBaseServer];

                        var authen = new AuthenticationPOSModel() { code = 1 };
                        if (CouchBaseServer != null)
                        {
                            authen.serverSync = CouchBaseServer.ToString();
                            //authen.databaseSync = CouchBaseServer.ToString();
                        }
                        authen.userAccessSync = string.Empty;
                        authen.passAccessSync = string.Empty;
                        //if (UserCouchBaseServer != null)
                        //{
                        //    authen.userAccessSync = UserCouchBaseServer.ToString();
                        //}
                        //if (PasswordCouchBaseServer != null)
                        //{
                        //    authen.passAccessSync = PasswordCouchBaseServer.ToString();
                        //}

                        authen.databaseSync = result.bucket;
                        string access_token;
                        long expires_in;
                        merchantManagement.GeneralSession(result.userID, out access_token, out expires_in);
                        authen.access_token = access_token;
                        authen.expires_in = expires_in;

                        authen.stores = new List<Store>();
                        InventoryComponent inventoryComponent = new InventoryComponent();
                        var listInventory = inventoryComponent.GetAll("inventory", "get_all_inventory", result.bucket);
                        foreach (var item in listInventory)
                        {
                            if (item.businessID.ToUpper() != ConstantSmoovs.Stores.Master.ToUpper() && item.index != ConstantSmoovs.Stores.OnlineStore)
                            {
                                var store = new Store();
                                store._id = item._id;
                                store.businessID = item.businessID;
                                store.index = item.index;
                                store.channels = item.channels;

                                authen.stores.Add(store);
                            }
                        }

                        return authen;
                    }
                }
            }

            return new AuthenticationPOSModel() { code = 0 };
        }
        public AuthenticationPOSModel WSGetAuthenticationPOS(string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.GetUserSessionByToken(token);
                if (authen != null)
                {
                    var CouchBaseServer = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.CouchBaseServer];
                    if (CouchBaseServer != null)
                    {
                        authen.serverSync = CouchBaseServer.ToString();
                        //authen.databaseSync = CouchBaseServer.ToString();
                    }
                    authen.userAccessSync = string.Empty;
                    authen.passAccessSync = string.Empty;
                    authen.code = 1;

                    authen.stores = new List<Store>();
                    InventoryComponent inventoryComponent = new InventoryComponent();
                    var listInventory = inventoryComponent.GetAll("inventory", "get_all_inventory", authen.databaseSync);
                    foreach (var item in listInventory)
                    {
                        if (item.businessID.ToUpper() != ConstantSmoovs.Stores.Master.ToUpper() && item.index != ConstantSmoovs.Stores.OnlineStore)
                        {
                            var store = new Store();
                            store._id = item._id;
                            store.businessID = item.businessID;
                            store.index = item.index;
                            store.channels = item.channels;

                            authen.stores.Add(store);
                        }
                    }

                    return authen;
                }
            }

            return new AuthenticationPOSModel() { code = 0 };
        }
    }
}

﻿using Com.SmoovPOS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.PaymentGateway
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSPaymentGateway" in both code and config file together.
    [ServiceContract]
    public interface IWSPaymentGateway
    {
        [OperationContract]
        void DoWork();
        [OperationContract]
        bool WsCreatOrUpdatePaymentGateway(string id,PaymentModel payment);
        [OperationContract]
        PaymentModel GetPaymentGateway();
    }
}

﻿using Com.SmoovPOS.Model;
using SmoovPOS.Business.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.PaymentGateway
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSPaymentGateway" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSPaymentGateway.svc or WSPaymentGateway.svc.cs at the Solution Explorer and start debugging.
    public class WSPaymentGateway : IWSPaymentGateway
    {
        public void DoWork()
        {
        }


        public bool WsCreatOrUpdatePaymentGateway(string id, PaymentModel payment)
        {
            PaymentBusiness paymentBusiness = new PaymentBusiness();
            return paymentBusiness.AddOrUpdatePaymentGateway(id, payment);
        }


        public PaymentModel GetPaymentGateway()
        {
            PaymentBusiness paymentBusiness = new PaymentBusiness();
            return paymentBusiness.GetPaymentGateway();
        }
    }
}

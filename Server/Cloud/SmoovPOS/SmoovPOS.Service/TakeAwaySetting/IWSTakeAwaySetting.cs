﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.TakeAwaySetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSTakeAwaySetting" in both code and config file together.
    [ServiceContract]
    public interface IWSTakeAwaySetting
    {
        [OperationContract]
        int WSCreateTakeAwaySetting(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID);
        [OperationContract]
        Com.SmoovPOS.Entity.TakeAwaySetting WSDetailTakeAwaySetting(string _id, string siteID);
        [OperationContract]
        int WSUpdateTakeAwaySetting(Com.SmoovPOS.Entity.TakeAwaySetting takeAwaySetting, string siteID);
        [OperationContract]
        int WSDeleteTakeAwaySetting(string _id, string siteID);
        [OperationContract]
        Com.SmoovPOS.Entity.TakeAwaySetting WSDetailTakeAwaySettingByBranchId(string designDoc, string viewName, string branch_id, string siteID);
        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.TakeAwaySetting> WSGetAllTakeAwaySetting(string designDoc, string viewName, string siteID);
    }
}

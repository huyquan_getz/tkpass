﻿using Com.SmoovPOS.Business;
using Com.SmoovPOS.Business.Components;
using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;

namespace SmoovPOS.Service.WebContents
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSWebContent" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSWebContent.svc or WSWebContent.svc.cs at the Solution Explorer and start debugging.
    public class WSWebContent : IWSWebContent
    {
        public int WSCreateWebContent(WebContent webContent, string siteID)
        {
            int result = -1;
            try
            {
                WebContentComponent webContentComponent = new WebContentComponent();
                result = webContentComponent.CreateNewWebContent(webContent, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }
        }

        public int WSUpdateWebContent(WebContent webContent, string siteID)
        {
            int result = -1;
            try
            {
                WebContentComponent webContentComponent = new WebContentComponent();
                result = webContentComponent.UpdateWebContent(webContent, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }
        }

        public WebContent WSDetailWebContent(string _id, string siteID)
        {
            WebContent webContent = null;
            try
            {
                WebContentComponent webContentComponent = new WebContentComponent();
                webContent = webContentComponent.DetailWebContent(_id, siteID);
                return webContent;
            }
            catch
            {
                return webContent;
                throw new NotImplementedException();
            }
        }

        public int WSDeleteWebContent(string _id, string siteID)
        {
            int result = -1;
            try
            {
                WebContentComponent webContentComponent = new WebContentComponent();
                result = webContentComponent.DeleteWebContent(_id, siteID);
                return result;
            }
            catch
            {
                return result;
                throw new NotImplementedException();
            }
        }

        public IEnumerable<WebContent> WSGetAllWebContent(string designDoc, string viewName, string siteID)
        {
            WebContentComponent categoryComp = new WebContentComponent();
            IEnumerable<WebContent> listWebContent = categoryComp.GetAllWebContent(designDoc, viewName, siteID);
            return listWebContent;
        }

        public int WSCountAllWebContent(string designDoc, string viewName, string siteID)
        {
            WebContentComponent webContent = new WebContentComponent();
            int count = webContent.CountWebContent(designDoc, viewName, siteID);
            return count;
        }

        /// <summary>
        /// WS gets primary site alias.
        /// </summary>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        public string WSGetPrimarySiteAlias(string siteID)
        {
            SiteBusiness siteBiz = new SiteBusiness();
            return siteBiz.GetPrimarySiteAlias(siteID);
        }
    }
}

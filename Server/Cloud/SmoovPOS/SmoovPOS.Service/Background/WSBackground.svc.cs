﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;
using SmoovPOS.Service.Discount;
using Com.SmoovPOS.TimeZone;
using System.Web.Configuration;
using SmoovPOS.Common;
using Com.SmoovPOS.Entity;

namespace SmoovPOS.Service.Background
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSBackground" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSBackground.svc or WSBackground.svc.cs at the Solution Explorer and start debugging.
    public class WSBackground : IWSBackground
    {


        public void DoWork()
        {
            Thread thread = new Thread(BackgroundThread.Instance.OnRunning);
            try
            {

                thread.Start();
            }
            catch
            {
                thread.Abort();
            }

        }

        public void AddBackgroundTask(BackGroundTask task)
        {
            DateTime convertTime = TimeZoneInfo.ConvertTimeToUtc(task.TimeToRun, TimeZoneInfo.FindSystemTimeZoneById(task.TimeZoneName));
            task.TimeToRun = convertTime;
            int taskIndex = BackgroundThread.ListTask.IndexOf(task);
            if (taskIndex > -1)
            {
                BackgroundThread.ListTask.RemoveAt(taskIndex);
            }
            BackgroundThread.ListTask.Add(task);
        }

        public void RemoveBackgroundTask(BackGroundTask task)
        {
            BackgroundThread.ListTask.Remove(task);
        }
    }

    public class BackgroundThread
    {
        private static BackgroundThread instance;

        private BackgroundThread() { }

        public static BackgroundThread Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BackgroundThread();
                }
                return instance;
            }
        }

        public static List<BackGroundTask> ListTask = new List<BackGroundTask>();

        public void OnRunning()
        {
            while (true)
            {
                List<string> collections = new List<string>();
                collections.Add(Common.ConstantSmoovs.CouchBase.DesigndocDiscount);
                MerchantManagement.IWSMerchantManagement merchantmannagement = new MerchantManagement.IWSMerchantManagement();
                List<string> allSiteId = merchantmannagement.WSGetAllSiteID();
                foreach (string siteId in allSiteId)
                {
                    MerchantSetting.WSMerchantGeneralSetting merchantSetting = new MerchantSetting.WSMerchantGeneralSetting();
                    var generalSetting = merchantSetting.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, siteId);
                    if (generalSetting == null) continue;
                    DateTime nowUTC = DateTime.UtcNow;
                    foreach (string collection in collections)
                    {
                        switch (collection)
                        {
                            case Common.ConstantSmoovs.CouchBase.DesigndocDiscount:
                                ActiveDiscount(nowUTC, generalSetting, siteId);
                                InactiveDiscount(nowUTC, generalSetting, siteId);
                                break;
                        }
                    }
                }
            }

        }

        private void ActiveDiscount(DateTime nowUTC, MerchantGeneralSetting generalSetting, string siteId)
        {
            //WSDiscount wsdiscount = new WSDiscount();
            //var discounts = wsdiscount.WSGetAllDiscount(Common.ConstantSmoovs.Discount.DesigndocDiscount, Common.ConstantSmoovs.Discount.ViewAllDiscount, siteId)
            //    .Where(d => DateTime.Parse(d.beginDate) <= nowUTC && DateTime.Parse(d.endDate) >= nowUTC && d.status == false).FirstOrDefault();
            //if (discounts != null)
            //{
            //    wsdiscount.WSUpdateDiscountStatus(discounts._id, true, siteId);
            //}
        }

        private void InactiveDiscount(DateTime nowUTC, MerchantGeneralSetting generalSetting, string siteId)
        {
            //WSDiscount wsdiscount = new WSDiscount();
            //var discounts = wsdiscount.WSGetAllDiscount(Common.ConstantSmoovs.Discount.DesigndocDiscount, Common.ConstantSmoovs.Discount.ViewAllDiscount, siteId)
            //    .Where(d => DateTime.Parse(d.beginDate) > nowUTC && DateTime.Parse(d.endDate) < nowUTC && d.status == true).FirstOrDefault();
            //if (discounts != null)
            //{
            //    wsdiscount.WSUpdateDiscountStatus(discounts._id, false, siteId);
            //}
        }

        public void AddBackgroundTask(BackGroundTask task)
        {
            ListTask.Add(task);
        }

        public void RemoveBackgroundTask(BackGroundTask task)
        {
            ListTask.Remove(task);
        }
    }

    public class BackGroundTask
    {
        public string TableName { get; set; }
        public DateTime TimeToRun { get; set; }
        public string TimeZoneName { get; set; }
        public string Job { get; set; }
        public string DocumentId { get; set; }
        public string SiteId { get; set; }
    }
}

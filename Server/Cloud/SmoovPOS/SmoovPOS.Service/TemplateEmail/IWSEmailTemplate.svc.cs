﻿using SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.TemplateEmail
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "IWSEmailTemplate" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select IWSEmailTemplate.svc or IWSEmailTemplate.svc.cs at the Solution Explorer and start debugging.
    public class IWSEmailTemplate : IIWSEmailTemplate
    {
        public int WSCreateTemplateEmail(Com.SmoovPOS.Entity.TemplateEmail templateEmail, string siteID)
        {
            TemplateEmailComponent templateEmailComponent = new TemplateEmailComponent();
            return templateEmailComponent.CreateTemplateEmail(templateEmail, siteID);
        }

        public Com.SmoovPOS.Entity.TemplateEmail WSGetDetailTemplateEmail(string key, string siteID)
        {
            TemplateEmailComponent templateEmailComponent = new TemplateEmailComponent();
            Com.SmoovPOS.Entity.TemplateEmail templateEmail = templateEmailComponent.GetDetailTemplateEmail(key, siteID);
            return templateEmail;
        }

        public int WSDeleteAllTemplateEmail(string designDoc, string viewName, string siteID)
        {
            TemplateEmailComponent templateEmailComponent = new TemplateEmailComponent();
            return templateEmailComponent.DeleteAllTemplateEmail(designDoc, viewName, siteID);
        }

        public int WSUpdateTemplateEmail(Com.SmoovPOS.Entity.TemplateEmail templateEmail, string siteID)
        {
            TemplateEmailComponent templateEmailComponent = new TemplateEmailComponent();
            int statusCode = templateEmailComponent.UpdateTemplateEmail(templateEmail, siteID);
            return statusCode;
        }




        public IEnumerable<Com.SmoovPOS.Entity.TemplateEmail> WSGetAllTemplateEmail(string designDoc, string viewName, string siteID)
        {
            IEnumerable<Com.SmoovPOS.Entity.TemplateEmail> listTemplateEmail = Enumerable.Empty<Com.SmoovPOS.Entity.TemplateEmail>();
            try
            {
                TemplateEmailComponent templateEmailComponent = new TemplateEmailComponent();
                listTemplateEmail = templateEmailComponent.GetAllTemplateEmail(designDoc, viewName, siteID);
                return listTemplateEmail;
            }
            catch
            {
                return listTemplateEmail;
                throw new NotImplementedException();
            }
        }
    }
}

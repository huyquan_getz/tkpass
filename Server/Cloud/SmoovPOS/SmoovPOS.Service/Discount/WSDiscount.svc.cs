﻿using Com.SmoovPOS.Business.Components;
using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Common.WSMerchantGeneralSettingReference;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Discount
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSDiscount" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSDiscount.svc or WSDiscount.svc.cs at the Solution Explorer and start debugging.
    public class WSDiscount : IWSDiscount
    {
        public int WSCreateDiscount(Com.SmoovPOS.Entity.Discount discount, string[] listProducts, string siteID)
        {
            DiscountComponent discountComponent = new DiscountComponent();

            if (listProducts != null)
            {
                //Product.WSProductItem wsProductItem = new Product.WSProductItem();

                //List<ProductItem> lisproductItems = new List<ProductItem>();

                //for (int i = 0; i < listProducts.Count(); i++)
                //{
                //    var product = wsProductItem.WSDetailProductItem(listProducts[i], siteID);
                //    product.arrayVarient = null; // Add to pass error entity too large
                //    lisproductItems.Add(product);
                //}
                discount.ListProductItems = listProducts.ToList();
            }


            return discountComponent.CreateNewDiscount(discount, siteID);
        }


        public Com.SmoovPOS.Entity.Discount WSDetailDiscount(string _id, string siteID)
        {
            DiscountComponent discountComponent = new DiscountComponent();
            Com.SmoovPOS.Entity.Discount discount = discountComponent.DetailDiscount(_id, siteID);
            return discount;
        }

        public int WSUpdateDiscount(Com.SmoovPOS.Entity.Discount discount, string[] listProducts, string siteID)
        {
            DiscountComponent discountComponent = new DiscountComponent();

            if (listProducts != null)
            {
                //Product.WSProductItem wsProductItem = new Product.WSProductItem();

                //List<ProductItem> lisproductItems = new List<ProductItem>();

                //for (int i = 0; i < listProducts.Count(); i++)
                //{
                //    var product = wsProductItem.WSDetailProductItem(listProducts[i], siteID);
                //    product.arrayVarient = null; // Add to pass error entity too large
                //    lisproductItems.Add(product);
                //}
                discount.ListProductItems = listProducts.ToList();
            }

            int statusCode = discountComponent.UpdateDiscount(discount, siteID);
            return statusCode;
        }

        public void WSDeleteDiscount(string _id, string siteID)
        {
            DiscountComponent discountComponent = new DiscountComponent();
            Com.SmoovPOS.Entity.Discount discount = discountComponent.DetailDiscount(_id, siteID);
            discount.display = false;
            discountComponent.UpdateDiscount(discount, siteID);

            //ProductItemComponent productItemComponent = new ProductItemComponent();

            //foreach (string _productID in discount.ListProductItems)
            //{
            //    IEnumerable<Com.SmoovPOS.Entity.ProductItem> listproductItem = productItemComponent.GetAllProductItemByIDProduct(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllProductItem, _productID, siteID);
            //    if (listproductItem.Count() > 0)
            //    {
            //        foreach (Com.SmoovPOS.Entity.ProductItem productItem in listproductItem)
            //        {
            //            productItem.discount = false;
            //            productItemComponent.UpdateProductItem(productItem, siteID);
            //        }
            //    }
            //}
        }

        public IEnumerable<Com.SmoovPOS.Entity.Discount> WSGetAllDiscount(string designDoc, string viewName, string siteID)
        {
            DiscountComponent discountComponent = new DiscountComponent();
            
            IEnumerable<Com.SmoovPOS.Entity.Discount> ListDiscount = discountComponent.GetAllDiscount(designDoc, viewName, siteID);
            //UpdateStatusDiscounts(designDoc, viewName, siteID);
            ListDiscount = discountComponent.GetAllDiscount(designDoc, viewName, siteID);
            return ListDiscount;
        }

        //public void UpdateStatusDiscounts(string designDoc, string viewName, string siteID)
        //{
        //    DiscountComponent discountComponent = new DiscountComponent();
        //    ProductItemComponent productItemComponent = new ProductItemComponent();

        //    IEnumerable<Com.SmoovPOS.Entity.Discount> ListDiscount = discountComponent.GetAllDiscount(designDoc, viewName, siteID).Where(l => l.endDate < DateTime.UtcNow);
        //    if (ListDiscount.Count() > 0)
        //    {
        //        foreach (Com.SmoovPOS.Entity.Discount discount in ListDiscount)
        //        {
        //            if (discount.status == false && (discount.beginDate <= DateTime.UtcNow && DateTime.UtcNow < discount.endDate))
        //            {
        //                discount.status = true;
        //                discountComponent.UpdateDiscount(discount, siteID);
        //                foreach (string producId in discount.ListProductItems)
        //                {
        //                    var ProductUpdate = productItemComponent.DetailProductItem(producId, siteID);
        //                    ProductUpdate.discount = discount.status;
        //                    productItemComponent.UpdateProductItem(ProductUpdate, siteID);
        //                }
        //            }
        //            if (discount.status == true && (discount.beginDate > DateTime.UtcNow && DateTime.UtcNow >= discount.endDate))
        //            {
        //                discount.status = false;
        //                discountComponent.UpdateDiscount(discount, siteID);
        //                foreach (string producId in discount.ListProductItems)
        //                {
        //                    var ProductUpdate = productItemComponent.DetailProductItem(producId, siteID);
        //                    ProductUpdate.discount = discount.status;
        //                    productItemComponent.UpdateProductItem(ProductUpdate, siteID);
        //                }
        //            }
        //        }
        //    }
        //}

        public List<Com.SmoovPOS.Entity.ProductItem> WSGetDiscountProducts(string siteID, string date, string discount_id, string timezone)
        {
            DiscountComponent discountComponent = new DiscountComponent();
            List<Com.SmoovPOS.Entity.ProductItem> ListProduct = new List<Com.SmoovPOS.Entity.ProductItem>();
            IEnumerable<Com.SmoovPOS.Entity.Discount> ListCurrentDiscount = discountComponent.GetAllDiscount(Common.ConstantSmoovs.CouchBase.DesigndocDiscount, Common.ConstantSmoovs.CouchBase.ViewAllDiscount, siteID);

            foreach (Com.SmoovPOS.Entity.Discount discount in ListCurrentDiscount)
            {
                //if (discount._id != discount_id
                //    && discount.endDate >= DateTime.Parse(date)
                //    && discount.beginDate < DateTime.Parse(date))
                //{
                if (discount._id != discount_id
                    && discount.endDate >= DateTime.Parse(date))
                {
                    foreach (string productid in discount.ListProductItems)
                    {
                        ProductItem productitem = new ProductItemComponent().DetailProductItem(productid, siteID);
                        ListProduct.Add(productitem);
                    }
                    
                }
            }
            return ListProduct;
        }

        public void WSUpdateDiscountStatus(string _id, bool status, string siteID)
        {
            DiscountComponent discountComponent = new DiscountComponent();
            Com.SmoovPOS.Entity.Discount discount = discountComponent.DetailDiscount(_id, siteID);
            discount.status = status;
            if (discount.status == false)
            {
                discount.endDate = DateTime.UtcNow;
            }
            discountComponent.UpdateDiscount(discount, siteID);

            //ProductItemComponent productItemComponent = new ProductItemComponent();

            //foreach (string productItemID in discount.ListProductItems)
            //{
            //    string productId = productItemComponent.DetailProductItem(productItemID, siteID).productID;
            //    IEnumerable<Com.SmoovPOS.Entity.ProductItem> listproductItem = productItemComponent.GetAllProductItemByIDProduct(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllProductItem, productId, siteID);
            //    if (listproductItem.Count() > 0)
            //    {
            //        foreach (Com.SmoovPOS.Entity.ProductItem productItem in listproductItem)
            //        {
            //            productItem.discount = status;
            //            productItemComponent.UpdateProductItem(productItem, siteID);
            //        }
            //    }
            //}
        }

        public IEnumerable<Com.SmoovPOS.Entity.Discount> WSGetAllDiscountByOnline(string designDoc, string viewName, string startKey = null, string endKey = null, int limit = 25, bool allowStale = false, int pageCurrent = 0, int pageGoTo = 0, string sort = "title", Boolean desc = true, string siteID = "smoopos")
        {
            DiscountComponent discountComponent = new DiscountComponent();
            //IEnumerable<Com.SmoovPOS.Entity.Discount> ListDiscount = discountComponent.GetAllDiscount(designDoc, viewName, startKey, endKey, limit, allowStale, pageCurrent, pageGoTo, sort, desc);
            IEnumerable<Com.SmoovPOS.Entity.Discount> ListDiscount = discountComponent.GetAllDiscount(designDoc, viewName, siteID);
            return ListDiscount;
        }
    }
}

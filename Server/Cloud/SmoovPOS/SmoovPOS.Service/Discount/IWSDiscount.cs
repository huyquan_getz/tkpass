﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.IO;

namespace SmoovPOS.Service.Discount
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSDiscount" in both code and config file together.
    [ServiceContract]
    public interface IWSDiscount
    {
        [OperationContract]
        int WSCreateDiscount(Com.SmoovPOS.Entity.Discount discount, string[] listProducts, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Discount WSDetailDiscount(string _id, string siteID);

        [OperationContract]
        int WSUpdateDiscount(Com.SmoovPOS.Entity.Discount discount, string[] listProducts, string siteID);

        [OperationContract]
        void WSDeleteDiscount(string _id, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Discount> WSGetAllDiscount(string designDoc, string viewName, string siteID);

        [OperationContract]
        List<Com.SmoovPOS.Entity.ProductItem> WSGetDiscountProducts(string siteID, string date, string discount_id, string timezone);

        [OperationContract]
        void WSUpdateDiscountStatus(string _id, bool status, string siteID);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.TableOrderingSetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSTableOrderingSetting" in both code and config file together.
    [ServiceContract]
    public interface IWSTableOrderingSetting
    {
        [OperationContract]
        int WSCreateTableOrderingSetting(Com.SmoovPOS.Entity.TableOrderingSetting tableOrderingSetting, string siteID);
        [OperationContract]
        Com.SmoovPOS.Entity.TableOrderingSetting WSDetailTableOrderingSetting(string _id, string siteID);
        [OperationContract]
        int WSUpdateTableOrderingSetting(Com.SmoovPOS.Entity.TableOrderingSetting tableOrderingSetting, string siteID);
        [OperationContract]
        int WSDeleteTableOrderingSetting(string _id, string siteID);
        [OperationContract]
        Com.SmoovPOS.Entity.TableOrderingSetting WSDetailTableOrderingSettingByBranchId(string designDoc, string viewName, string branch_id, string siteID);
        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.TableOrderingSetting> WSGetAllTableOrderingSetting(string designDoc, string viewName, string siteID);
    }
}

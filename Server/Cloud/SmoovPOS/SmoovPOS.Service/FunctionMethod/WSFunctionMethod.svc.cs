﻿using Com.SmoovPOS.Model;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.FunctionMethod
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSFunctionMethod" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSFunctionMethod.svc or WSFunctionMethod.svc.cs at the Solution Explorer and start debugging.
    public class WSFunctionMethod : IWSFunctionMethod
    {
        //public void DoWork()
        //{
        //}

        private SmoovPOS.Business.Components.FunctionMethodComponent functionMethodComponent = new SmoovPOS.Business.Components.FunctionMethodComponent();

        /// <summary>
        /// Wses the create function method.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        public Guid? WSCreateFunctionMethod(Com.SmoovPOS.Model.FunctionMethodModel result)
        {
            var value = functionMethodComponent.CreateFunctionMethod(result);
            return value;
        }

        /// <summary>
        /// Wses the update function method.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        public bool WSUpdateFunctionMethod(Com.SmoovPOS.Model.FunctionMethodModel result)
        {
            var value = functionMethodComponent.UpdateFunctionMethod(result);
            return value;
        }

        /// <summary>
        /// Wses the delete function method.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        public bool WSDeleteFunctionMethod(Guid id)
        {
            var value = functionMethodComponent.DeleteFunctionMethod(id);
            return value;
        }

        /// <summary>
        /// Wses the get function method.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        public Com.SmoovPOS.Model.FunctionMethodModel WSGetFunctionMethod(Guid id)
        {
            if (id != null)
            {
                var functionMethod = functionMethodComponent.GetFunctionMethod(id);
                return functionMethod;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Wses the get all function method.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        public IEnumerable<Com.SmoovPOS.Model.FunctionMethodModel> WSGetAllFunctionMethod()
        {
            IEnumerable<FunctionMethodModel> functionMethodModelList = functionMethodComponent.GetAllFunctionMethod();

            return functionMethodModelList;
        }

        /// <summary>
        /// Wses the name of the exist function method by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        public bool WSExistFunctionMethodByName(string name, Guid? id)
        {
            var exist = functionMethodComponent.ExistFunctionMethodByName(name, id);

            return exist;
        }

        /// <summary>
        /// Wses the get function method by filter.
        /// </summary>
        /// <param name="searchModel">The search model.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>2/2/2015-3:47 PM</datetime>
        public Com.SmoovPOS.Model.SearchFunctionMethodModel WSGetFunctionMethodByFilter(Com.SmoovPOS.Model.SearchFunctionMethodModel searchModel)
        {
            var functionMethodList = functionMethodComponent.GetFunctionMethod(searchModel);

            return functionMethodList;
        }
    }
}

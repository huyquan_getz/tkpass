﻿using Com.SmoovPOS.Business.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Customer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSCustomer" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSCustomer.svc or WSCustomer.svc.cs at the Solution Explorer and start debugging.
    public class WSCustomer : IWSCustomer
    {

        public int WSCreateCustomer(Com.SmoovPOS.Entity.Customer customer, string siteID)
        {
            CustomerComponent customerComponent = new CustomerComponent();
            return customerComponent.CreateNewCustomer(customer, siteID);
        }


        public Com.SmoovPOS.Entity.Customer WSDetailCustomer(string _id, string siteID)
        {
            CustomerComponent customerComponent = new CustomerComponent();
            Com.SmoovPOS.Entity.Customer customer = customerComponent.DetailCustomer(_id, siteID);
            return customer;
        }

        public int WSUpdateCustomer(Com.SmoovPOS.Entity.Customer customer, string siteID)
        {
            CustomerComponent customerComponent = new CustomerComponent();
            int statusCode = customerComponent.UpdateCustomer(customer, siteID);
            return statusCode;
        }

        public int WSDeleteCustomer(string _id, string siteID)
        {
            CustomerComponent customerComponent = new CustomerComponent();
            int statusCode = customerComponent.DeleteCustomer(_id, siteID);
            return statusCode;
        }

        public IEnumerable<Com.SmoovPOS.Entity.Customer> WSGetAllCustomer(string designDoc, string viewName, string siteID)
        {
            CustomerComponent customerComponent = new CustomerComponent();
            IEnumerable<Com.SmoovPOS.Entity.Customer> ListCustomer = customerComponent.GetAllCustomer(designDoc, viewName, siteID);
            return ListCustomer;
        }


        public int WSCheckEmailExist(string designDoc, string viewName, string email, string siteID)
        {
            CustomerComponent customerComponent = new CustomerComponent();
            return customerComponent.CheckEmailExist(designDoc, viewName, email, siteID);
        }


        public IEnumerable<Com.SmoovPOS.Entity.Customer> WSGetCustomerWithEmail(string designDoc, string viewName, string email, string siteID)
        {
            CustomerComponent customerComponent = new CustomerComponent();
            return customerComponent.GetCustomerWithEmail(designDoc, viewName, email, siteID);
        }

        public Com.SmoovPOS.Entity.Customer WSCheckAccountLogin(string designDoc, string viewName, string email, string password, string siteID)
        {
            CustomerComponent customerComponent = new CustomerComponent();
            return customerComponent.CheckAccountCustomer(designDoc, viewName, email,password, siteID);
        }

        public Com.SmoovPOS.Entity.Customer WSGetDetailCustomerByEmail(string designDoc, string viewName, string email, string siteID)
        {
            CustomerComponent customerComponent = new CustomerComponent();
            return customerComponent.GetDetailCustomer(designDoc, viewName, email, siteID);
        }
        public Com.SmoovPOS.Entity.Customer WSCheckEmailAndPinCode(string designDoc, string viewName, string email, string pinCode, string siteID)
        {
            CustomerComponent customerComponent = new CustomerComponent();
            return customerComponent.CheckEmailAndPinCode(designDoc, viewName, email,pinCode, siteID);
        }
         
    }
}

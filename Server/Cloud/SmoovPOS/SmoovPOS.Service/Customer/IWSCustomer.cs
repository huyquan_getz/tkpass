﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Customer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSCustomer" in both code and config file together.
    [ServiceContract]
    public interface IWSCustomer
    {

        [OperationContract]
        int WSCreateCustomer(Com.SmoovPOS.Entity.Customer customer, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Customer WSDetailCustomer(string _id, string siteID);

        [OperationContract]
        int WSUpdateCustomer(Com.SmoovPOS.Entity.Customer customer, string siteID);

        [OperationContract]
        int WSDeleteCustomer(string _id, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Customer> WSGetAllCustomer(string designDoc, string viewName, string siteID);

        [OperationContract]
        int WSCheckEmailExist(string designDoc, string viewName, string email, string siteID);

        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Customer> WSGetCustomerWithEmail(string designDoc, string viewName, string email, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Customer WSCheckAccountLogin(string designDoc, string viewName, string email, string password, string siteID);

        [OperationContract]
        Com.SmoovPOS.Entity.Customer WSGetDetailCustomerByEmail(string designDoc, string viewName, string email, string siteID);
        [OperationContract]
        Com.SmoovPOS.Entity.Customer WSCheckEmailAndPinCode(string designDoc, string viewName, string email, string pinCode, string siteID);
    }
}

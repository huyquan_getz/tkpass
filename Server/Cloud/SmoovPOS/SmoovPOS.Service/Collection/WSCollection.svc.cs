﻿using SmoovPOS.Business.Components;
using System.Collections.Generic;

namespace SmoovPOS.Service.Collection
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSCollection" in code, svc and config file together.
    // NOTE: In Collection to launch WCF Test Client for testing this service, please select WSCollection.svc or WSCollection.svc.cs at the Solution Explorer and start debugging.
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>1/30/2015-5:27 PM</datetime>
    public class WSCollection : IWSCollection
    {



        /// <summary>
        /// Wses the create collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:27 PM</datetime>
        /// <datetime>1/30/2015-5:27 PM</datetime>
        public int WSCreateCollection(Com.SmoovPOS.Entity.Collection collection, string siteID)
        {
            CollectionComponent CollectionComp = new CollectionComponent();
            int StatusCode = CollectionComp.CreateNewCollection(collection, siteID);
            return StatusCode;
        }

        /// <summary>
        /// Wses the delete collection.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:27 PM</datetime>
        /// <datetime>1/30/2015-5:27 PM</datetime>
        public int WSDeleteCollection(string id, string siteID)
        {
            CollectionComponent CollectionComp = new CollectionComponent();
            int StatusCode = CollectionComp.DeleteCollection(id, siteID);
            return StatusCode;
        }

        /// <summary>
        /// Wses the update collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:27 PM</datetime>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        public int WSUpdateCollection(Com.SmoovPOS.Entity.Collection collection, string siteID)
        {
            CollectionComponent CollectionComp = new CollectionComponent();
            int StatusCode = CollectionComp.UpdateCollection(collection, siteID);
            return StatusCode;
        }

        /// <summary>
        /// Wses the detail collection.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:27 PM</datetime>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        public Com.SmoovPOS.Entity.Collection WSDetailCollection(string id, string siteID)
        {
            CollectionComponent CollectionComp = new CollectionComponent();
            Com.SmoovPOS.Entity.Collection collection = CollectionComp.DetailCollection(id, siteID);
            return collection;
        }


        /// <summary>
        /// Wses the delete all collection.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:27 PM</datetime>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        public int WSDeleteAllCollection(string designDoc, string viewName, string siteID)
        {
            CollectionComponent CollectionCom = new CollectionComponent();
            return CollectionCom.DeleteAllCollection(designDoc, viewName, siteID);
        }

        /// <summary>
        /// Wses the get all collection.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:27 PM</datetime>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        public IEnumerable<Com.SmoovPOS.Entity.Collection> WSGetAllCollection(string designDoc, string viewName, string siteID)
        {
            CollectionComponent CollectionCom = new CollectionComponent();
            return CollectionCom.GetAllCollection(designDoc, viewName, siteID);
        }
    }
}

﻿using Couchbase;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.Collection
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSCollection" in both code and config file together.
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>1/30/2015-5:27 PM</datetime>
    [ServiceContract]
    public interface IWSCollection
    {
        /// <summary>
        /// Wses the create collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:27 PM</datetime>
        [OperationContract]
        int WSCreateCollection(Com.SmoovPOS.Entity.Collection collection, string siteID);
        //[OperationContract]
        //string WSCreateCollection(Com.SmoovPOS.Entity.Collection collection, string siteID);
        /// <summary>
        /// Wses the delete collection.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:27 PM</datetime>
        [OperationContract]
        int WSDeleteCollection(string id, string siteID);

        /// <summary>
        /// Wses the update collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        int WSUpdateCollection(Com.SmoovPOS.Entity.Collection collection, string siteID);

        /// <summary>
        /// Wses the detail collection.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        Com.SmoovPOS.Entity.Collection WSDetailCollection(string id, string siteID);

        /// <summary>
        /// Wses the get all collection.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.Collection> WSGetAllCollection(string designDoc, string viewName, string siteID);

        /// <summary>
        /// Wses the delete all collection.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:26 PM</datetime>
        [OperationContract]
       int WSDeleteAllCollection(string designDoc, string viewName, string siteID);

       
    }
}

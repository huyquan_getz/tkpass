﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.DeliverySetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSDeliveryCountry" in both code and config file together.
    [ServiceContract]
    public interface IWSDeliveryCountry
    {
        [OperationContract]
        bool WSCreateDeliveryCountry(DeliveryCountry result, string siteID);
        [OperationContract]
        bool WSUpdateDeliveryCountry(DeliveryCountry result, string siteID);
        [OperationContract]
        bool WSDeleteDeliveryCountry(string id, string siteID);
        [OperationContract]
        DeliveryCountry WSGetDeliveryCountry(string id, string siteID);
        [OperationContract]
        IEnumerable<DeliveryCountry> WSGetAllDeliveryCountry(string designDoc, string viewName, string siteID);
    }
}

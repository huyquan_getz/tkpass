﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SmoovPOS.Service.DeliverySetting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSDeliverySetting" in both code and config file together.
    [ServiceContract]
    public interface IWSDeliverySetting
    {
        [OperationContract]
        bool WSCreateDeliverySetting(Com.SmoovPOS.Entity.DeliverySetting result, string siteID);
        [OperationContract]
        bool WSUpdateDeliverySetting(Com.SmoovPOS.Entity.DeliverySetting result, string siteID);
        [OperationContract]
        bool WSDeleteDeliverySetting(string id, string siteID);
        [OperationContract]
        Com.SmoovPOS.Entity.DeliverySetting WSGetDeliverySetting(string id, string siteID);
        [OperationContract]
        IEnumerable<Com.SmoovPOS.Entity.DeliverySetting> WSGetAllDeliverySetting(string designDoc, string viewName, string siteID);
        [OperationContract]
        Com.SmoovPOS.Entity.DeliverySetting WSGetDeliverySettingByCountry(string designDoc, string viewName, string siteID,string country);
    }
}

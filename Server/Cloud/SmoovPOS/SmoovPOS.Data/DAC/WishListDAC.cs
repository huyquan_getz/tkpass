﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class WishListDAC : CBDataRepositoryBase<WishList>
    {
         string siteID = "";
         public WishListDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }

        public int CreateDAC(WishList wishList)
        {
            int StatusCode = Create(wishList);
            return StatusCode;
        }
        public int UpdateDAC(WishList wishList)
        {
            int StatusCode = Update(wishList);
            return StatusCode;
        }
        public int deleteDAC(string id)
        {
            int statuscode = Delete(id);
            return statuscode;
        }

        public Couchbase.IView<WishList> GetAll(string designDoc, string viewName)
        {
            IView<WishList> ListWishList = GetView(designDoc, viewName);
            return ListWishList;
        }
    }
}
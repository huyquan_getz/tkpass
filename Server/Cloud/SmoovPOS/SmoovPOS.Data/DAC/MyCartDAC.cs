﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class MyCartDAC : CBDataRepositoryBase<MyCart>
    {
        public MyCartDAC(string bucket)
        {
            SetClientCouchbase(bucket);
        }

        public Couchbase.IView<MyCart> GetAll(string designDoc, string viewName)
        {
            IView<MyCart> list = GetView(designDoc, viewName);
            return list;
        }

        public int CreateDAC(MyCart myCart)
        {
            int StatusCode = Create(myCart);
            return StatusCode;
        }

        public int UpdateDAC(MyCart myCart)
        {
            int StatusCode = Update(myCart);
            return StatusCode;
        }

        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }
    }
}
﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class CustomerDAC : CBDataRepositoryBase<Customer>
    {
        string siteID = "";
        public CustomerDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }

        public int CreateDAC(Customer customer)
        {
            int StatusCode = Create(customer);
            return StatusCode;
        }
        public int deleteDAC(string id)
        {
            int statuscode = Delete(id);
            return statuscode;
        }

        public int updateDAC(Customer customer)
        {
            int statuscode = Update(customer);
            return statuscode;
        }
        public Customer detailDAC(string id)
        {
            Customer customer = Get(id);
            return customer;
        }

        public IView<Customer> GetAll(string designDoc, string viewName)
        {
            IView<Customer> ListCustomer = GetView(designDoc, viewName);
            return ListCustomer;
        }
    }
}
﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;

namespace SmoovPOS.Data.DAC
{
    public class DeliveryCountryDAC : CBDataRepositoryBase<DeliveryCountry>
    {
        string siteID = "";
        /// <summary>
        /// Initializes a new instance of the <see cref="DeliveryCountryDAC"/> class.
        /// </summary>
        /// <param name="siteIDParam">The site identifier parameter.</param>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public DeliveryCountryDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public int CreateDAC(DeliveryCountry deliveryCountry)
        {
            int StatusCode = Create(deliveryCountry);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }
        public int UpdateDAC(DeliveryCountry deliveryCountry)
        {
            int StatusCode = Update(deliveryCountry);
            return StatusCode;
        }
        public DeliveryCountry DetailDAC(string id)
        {
            DeliveryCountry deliveryCountry = Get(id);
            return deliveryCountry;
        }
        public IView<DeliveryCountry> GetAllDeliveryCountryDAC(string designDoc, string viewName)
        {
            IView<DeliveryCountry> ListCategory = GetView(designDoc, viewName);
            return ListCategory;
        }
    }
}
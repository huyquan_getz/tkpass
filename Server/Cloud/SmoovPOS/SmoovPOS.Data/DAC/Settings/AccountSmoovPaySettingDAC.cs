﻿using Com.SmoovPOS.Data.Repository;
using Couchbase;
using SmoovPOS.Entity.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC.AccountSmoovPaySettings
{
    public class AccountSmoovPaySettingDAC : CBDataRepositoryBase<AccountSmoovpay>
    {
        string siteID = "";
        public AccountSmoovPaySettingDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public IView<AccountSmoovpay> GetAll(string designDoc, string viewName)
        {
            IView<AccountSmoovpay> listCheckoutSetting = GetView(designDoc, viewName);
            return listCheckoutSetting;
        }

        public int CreateDAC(AccountSmoovpay accountSmoovpay)
        {
            int StatusCode = Create(accountSmoovpay);
            return StatusCode;
        }

        public int UpdateDAC(AccountSmoovpay accountSmoovpay)
        {
            int StatusCode = Update(accountSmoovpay);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }
    }
}
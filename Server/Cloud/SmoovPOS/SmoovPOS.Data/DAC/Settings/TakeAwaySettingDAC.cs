﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Data.DAC
{
    public class TakeAwaySettingDAC : CBDataRepositoryBase<TakeAwaySetting>
    {
        string siteID = "";
        public TakeAwaySettingDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }

        public int CreateDAC(TakeAwaySetting takeAwaySetting)
        {
            int StatusCode = Create(takeAwaySetting);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }

        public int UpdateDAC(TakeAwaySetting takeAwaySetting)
        {
            int StatusCode = Update(takeAwaySetting);
            return StatusCode;
        }
        public TakeAwaySetting DetailDAC(string id)
        {
            TakeAwaySetting takeAwaySetting = Get(id);
            return takeAwaySetting;
        }

        public IView<TakeAwaySetting> GetAll(string designDoc, string viewName)
        {
            IView<TakeAwaySetting> ListTakeAwaySetting = GetView(designDoc, viewName);
            return ListTakeAwaySetting;
        }
    }
}
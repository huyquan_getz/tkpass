﻿using Com.SmoovPOS.Data.Repository;
using Couchbase;
using SmoovPOS.Entity.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC.Settings
{
    public class CheckoutSettingDAC :CBDataRepositoryBase<CheckoutSetting>
    {
         string siteID = "";
         public CheckoutSettingDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
         public IView<CheckoutSetting> GetAll(string designDoc, string viewName)
         {
             IView<CheckoutSetting> listCheckoutSetting = GetView(designDoc, viewName);
             return listCheckoutSetting;
         }

         public int CreateDAC(CheckoutSetting checkoutSetting )
         {
             int StatusCode = Create(checkoutSetting);
             return StatusCode;
         }

         public int UpdateDAC(CheckoutSetting checkoutSetting)
         {
             int StatusCode = Update(checkoutSetting);
             return StatusCode;
         }

    }
}
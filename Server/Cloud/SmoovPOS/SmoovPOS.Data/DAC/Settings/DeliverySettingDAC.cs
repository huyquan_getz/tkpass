﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class DeliverySettingDAC : CBDataRepositoryBase<DeliverySetting>
    {
        string siteID = "";
        /// <summary>
        /// Initializes a new instance of the <see cref="DeliverySettingDAC"/> class.
        /// </summary>
        /// <param name="siteIDParam">The site identifier parameter.</param>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public DeliverySettingDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public int CreateDAC(DeliverySetting deliverySetting)
        {
            int StatusCode = Create(deliverySetting);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }
        public int UpdateDAC(DeliverySetting deliverySetting)
        {
            int StatusCode = Update(deliverySetting);
            return StatusCode;
        }
        public DeliverySetting DetailDAC(string id)
        {
            DeliverySetting deliverySetting = Get(id);
            return deliverySetting;
        }
        public IView<DeliverySetting> GetAllDeliverySettingDAC(string designDoc, string viewName)
        {
            IView<DeliverySetting> ListCategory = GetView(designDoc, viewName);
            return ListCategory;
        }
    }
}
﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Data.DAC
{
    public class TableOrderingSettingDAC : CBDataRepositoryBase<TableOrderingSetting>
    {
        string siteID = "";
        public TableOrderingSettingDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }

        public int CreateDAC(TableOrderingSetting tableOrderingSetting)
        {
            int StatusCode = Create(tableOrderingSetting);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }

        public int UpdateDAC(TableOrderingSetting tableOrderingSetting)
        {
            int StatusCode = Update(tableOrderingSetting);
            return StatusCode;
        }
        public TableOrderingSetting DetailDAC(string id)
        {
            TableOrderingSetting tableOrderingSetting = Get(id);
            return tableOrderingSetting;
        }

        public IView<TableOrderingSetting> GetAll(string designDoc, string viewName)
        {
            IView<TableOrderingSetting> ListTableOrderingSetting = GetView(designDoc, viewName);
            return ListTableOrderingSetting;
        }
    }
}
﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Data.DAC
{
    public class QueueDAC : CBDataRepositoryBase<Queue>
    {
        string siteID = "";
        public QueueDAC(string siteIDParam)
        {
            SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }

        public int CreateDAC(Queue queue)
        {
            int StatusCode = Create(queue);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }

        public int UpdateDAC(Queue queue)
        {
            int StatusCode = Update(queue);
            return StatusCode;
        }
        public Queue DetailDAC(string id)
        {
            Queue queue = Get(id);
            return queue;
        }

        public IView<Queue> GetAll(string designDoc, string viewName, string bucket)
        {
            IView<Queue> ListQueue = GetView(designDoc, viewName).StartKey(new string[] { bucket, "uefff" }).EndKey(new string[] { bucket});
            return ListQueue;
        }
    }
}
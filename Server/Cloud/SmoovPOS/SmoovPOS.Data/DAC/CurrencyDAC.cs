﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class CurrencyDAC : CBDataRepositoryBase<Currency>
    {
        public CurrencyDAC(string siteIDParam)
        {
             SetClientCouchbase(siteIDParam);
        }

        public IView<Currency> GetAllCurrency(string designDoc, string viewName)
        {
            IView<Currency> listCurrency = GetView(designDoc, viewName);
            return listCurrency;
        }

        public int CreateDAC(Currency currency)
        {
            int StatusCode = Create(currency);
            return StatusCode;
        }

        public int UpdateDAC(Currency currency)
        {
            int StatusCode = Update(currency);
            return StatusCode;
        }
    }
}
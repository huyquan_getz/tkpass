﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class InventoryDAC : CBDataRepositoryBase<Inventory>
    {
        public InventoryDAC(string bucket)
        {
            SetClientCouchbase(bucket);
        }
        public int CreateDAC(Inventory inventory)
        {
            int StatusCode = Create(inventory);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }

        public int UpdateDAC(Inventory inventory)
        {
            int StatusCode = Update(inventory);
            return StatusCode;
        }
        public Inventory DetailDAC(string id)
        {
            Inventory inventory = Get(id);
            return inventory;
        }

        public IView<Inventory> GetAllDAC(string designDoc, string viewName)
        {
            IView<Inventory> ListInventory = GetView(designDoc, viewName);
            return ListInventory;
        }
        public IView<Inventory> GetAllDACWithBucket(string designDoc, string viewName, string bucket)
        {
            IView<Inventory> ListInventory = GetView(designDoc, viewName).Key(bucket);
            return ListInventory;
        }
    }
}
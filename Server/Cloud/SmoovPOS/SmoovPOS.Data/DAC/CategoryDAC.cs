﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Data.DAC
{
    public class CategoryDAC : CBDataRepositoryBase<Category>
    {   
         //string bucket = "";
         public CategoryDAC(string bucket)
        {
            SetClientCouchbase(bucket);
        }
        public int CreateDAC(Category category)
        {
            int StatusCode = Create(category);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }

        public int UpdateDAC(Category category)
        {
            int StatusCode = Update(category);
            return StatusCode;
        }
        public Category DetailDAC(string id)
        {
            Category category = Get(id);
            return category;
        }

        public IView<Category> GetAll(string designDoc, string viewName)
        {
            IView<Category> ListCategory = GetView(designDoc, viewName); 
            return ListCategory;
        }
        public IView<Category> GetAllWithBucket(string designDoc, string viewName, string bucket)
        {
            IView<Category> ListCategory = GetViewBucket(designDoc, viewName, bucket);
            return ListCategory;
        }
        public IView<Category> DeleteMerchant(string bucket, string designDoc, string view)
        {
            CategoryDAC dac = new CategoryDAC(bucket);
            var lists = dac.GetView(designDoc, view).StartKey(bucket);
            return lists;
        }

    }
}
﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Data.DAC
{
    public class ProductDAC : CBDataRepositoryBase<Product>
    {   
         public ProductDAC(string bucket)
         {
             SetClientCouchbase(bucket);
        }
        public int CreateDAC(Product product)
        {
            int StatusCode = Create(product);
            return StatusCode;
        }
        public string CreateDAC_(Product product)
        {
            string _id = Create_(product);
            return _id;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }

        public int UpdateDAC(Product product)
        {
            int StatusCode = Update(product);
            return StatusCode;
        }
        public Product DetailDAC(string id)
        {
            Product product = Get(id);
            return product;
        }

        public IView<Product> GetAllDAC(string designDoc, string viewName)
        {
            IView<Product> ListCategory = GetView(designDoc, viewName);
            return ListCategory;
        }
    }
}
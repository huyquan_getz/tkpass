﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Data.DAC
{
    public class TemplateEmailDAC : CBDataRepositoryBase<TemplateEmail>
    {   
         string siteID = "";
         public TemplateEmailDAC(string siteIDParam)
         {
             SetClientCouchbase(siteIDParam);
            siteID = siteIDParam;
        }
        public int CreateTemplateEmailDAC(TemplateEmail templateEmail)
        {
            return Create(templateEmail);
        }

        public TemplateEmail GetTemplateEmailDAC(string key)
        {
            return Get(key);
        }

        public int DeleteAllTemplateEmailDAC(string designDoc, string viewName)
        {
            IEnumerable<TemplateEmail> templateEmailList = GetView(designDoc, viewName);
            foreach (var item in templateEmailList.Where(r => r._id != null))
            {
                Delete(item._id);
            }

            return 0;
        }

        public IEnumerable<TemplateEmail> GetAllTemplateEmailDAC(string designDoc, string viewName, string siteID)
        {
            return GetView(designDoc, viewName).Key(siteID);
        }
        public int UpdateTemplateEmailDAC(TemplateEmail order)
        {
            int statuscode = Update(order);
            return statuscode;
        }
    }
}
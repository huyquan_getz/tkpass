﻿using Com.SmoovPOS.Data.Repository;
using Com.SmoovPOS.Entity;
using Couchbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Data.DAC
{
    public class WebContentDAC : CBDataRepositoryBase<WebContent>
    {
        //string siteID = "";
        public WebContentDAC(string bucket)
        {
            SetClientCouchbase(bucket);
            //siteID = siteIDParam;
        }
        public int CreateDAC(WebContent webContent)
        {
            int StatusCode = Create(webContent);
            return StatusCode;
        }
        public int DeleteDAC(string id)
        {
            int StatusCode = Delete(id);
            return StatusCode;
        }

        public int UpdateDAC(WebContent webContent)
        {
            int StatusCode = Update(webContent);
            return StatusCode;
        }
        public WebContent DetailDAC(string id)
        {
            WebContent webContent = Get(id);
            return webContent;
        }
        public IView<WebContent> GetAll(string designDoc, string viewName)
        {
            IView<WebContent> listWebContent = GetView(designDoc, viewName);
            return listWebContent;
        }


    }
}
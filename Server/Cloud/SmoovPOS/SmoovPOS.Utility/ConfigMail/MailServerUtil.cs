﻿
using SmoovPOS.Business.Business;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Sockets;
using System.Net.Security;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;
using SmoovPOS.Common;
using System.Threading;

namespace SmoovPOS.Utility.MailServer
{
    public class MailServerUtil
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //public Boolean SendMailOfficeSite(string displayName, string to, string subject, string body)
        //{
        //    try
        //    {
        //        MailConfigBusiness mailConfigBusiness = new MailConfigBusiness();
        //        MailServerModel mailServer = mailConfigBusiness.GetConfigMailServer(null);
        //        System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
        //        mail.To.Add(to);
        //        mail.From = new MailAddress(mailServer.UserName1, displayName, System.Text.Encoding.UTF8);
        //        mail.Subject = subject;
        //        mail.SubjectEncoding = System.Text.Encoding.UTF8;
        //        mail.Body = body;
        //        mail.BodyEncoding = System.Text.Encoding.UTF8;
        //        mail.IsBodyHtml = true;
        //        mail.Priority = MailPriority.High;

        //        SmtpClient client = new SmtpClient(mailServer.OutgoingSmtp, mailServer.OutgoingPort);

        //        //Add the Creddentials- use your own email id and password
        //        System.Net.NetworkCredential nt =
        //        new System.Net.NetworkCredential(mailServer.UserName1, mailServer.PassWord1);

        //        // client.Port = 587; // Gmail works on this port
        //        client.EnableSsl = mailServer.EnableSsl; //Gmail works on Server Secured Layer
        //        client.UseDefaultCredentials = false;
        //        client.Credentials = nt;
        //        client.Send(mail);
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        e.Message.ToString();
        //        return false;
        //    }
        //}

        /// <summary>
        /// Send mail 
        /// </summary>
        /// <param name="type">int : type : 0 ->OfficeSite, 1 -> Addmin, 2 -> Merchant </param>
        /// <param name="displayName">string</param>
        /// <param name="to">string </param>
        /// <param name="subject">string</param>
        /// <param name="body">string</param>
        /// <returns></returns>
        public Boolean SendMail(int type, string displayName, string to, string subject, string body)
        {
            try
            {
                MailConfigBusiness mailConfigBusiness = new MailConfigBusiness();
                MailServerModel mailServer = null;
                string userName = "";
                string passWord = "";

                if (type == ConstantSmoovs.ConfigMail.Admin)
                {
                    //mailServer = mailConfigBusiness.GetConfigMailServer(null);
                    //userName = mailServer.UserName;
                    //passWord = mailServer.PassWord;
                    return SendMailWithAmazon(displayName,to, subject, body);
                }
                else if (type == ConstantSmoovs.ConfigMail.Merchant)
                {
                    //mailServer = mailConfigBusiness.GetConfigMailServer(null, 1);
                    //userName = mailServer.UserName;
                    //passWord = mailServer.PassWord;
                    return SendMailWithAmazon(displayName, to, subject, body);
                }

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.To.Add(to);
                mail.From = new MailAddress(userName, displayName, System.Text.Encoding.UTF8);
                mail.Subject = subject;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = body;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                SmtpClient client = new SmtpClient(mailServer.OutgoingSmtp, mailServer.OutgoingPort);

                //Add the Creddentials- use your own email id and password
                System.Net.NetworkCredential nt =
                new System.Net.NetworkCredential(userName, passWord);

                // client.Port = 587; // Gmail works on this port
                client.EnableSsl = mailServer.EnableSsl; //Gmail works on Server Secured Layer
                client.UseDefaultCredentials = true;
                client.Credentials = nt;
                //new Thread(() =>
                //{
                //    client.Send(mail);
                //}).Start();
                client.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return false;
            }
        }
        /// <summary>
        /// Test send mail 
        /// </summary>
        /// <param name="type">int : type : 0 ->OfficeSite, 1 -> Addmin, 2 -> Merchant</param>
        /// <param name="displayName">string</param>
        /// <param name="to">string</param>
        /// <param name="subject">string</param>
        /// <param name="objectMail">MailObjectModel</param>
        /// <returns>true or false</returns>
        public Boolean SendMailTest(int type, string displayName, string to, string subject, MailObjectModel objectMail)
        {
            string createDate = objectMail.CreateDate;
            string body = "<div> <span>Dear Test</span><br/>Test send mail from server date {CreateDate} </div>";
            // HttpUtility.HtmlEncode(body).Replace("date", t.ToString());
            StringBuilder builder = new StringBuilder(body);
            builder.Replace("{CreateDate}", createDate.ToString());

            try
            {
                //new Thread(() =>
                //{
                //    SendMail(type, displayName, to, subject, builder.ToString());
                //}).Start();
                SendMail(type, displayName, to, subject, builder.ToString());
                return true;
            }
            catch (Exception e)
            {
                e.Message.ToString();
                return false;
            }
        }

        /// <summary>
        /// Sends the mail.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="merchantID">The merchant identifier.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-9:12 AM</datetime>
        public Boolean SendMail(int type, string displayName, string to, string subject, string body, Guid merchantID)
        {
            try
            {
                MailConfigBusiness mailConfigBusiness = new MailConfigBusiness();
                MailServerModel mailServer = mailConfigBusiness.GetConfigMailServer(merchantID);
                string userName = "";
                string passWord = "";

                switch (type)
                {
                    case ConstantSmoovs.ConfigMail.Merchant: // send from merchant to customer
                        if (mailServer != null && mailServer.isApply == false) 

                        {
                         //   mailServer = mailConfigBusiness.GetConfigMailServer(null, 1);
                            return SendMailWithAmazon(displayName, to, subject, body);
                        }
                        else if(mailServer ==null)
                        {
                            return SendMailWithAmazon(displayName, to, subject, body);
                        }
                        userName = mailServer.UserName;
                        passWord = mailServer.PassWord;
                        break;
                    case ConstantSmoovs.ConfigMail.Admin: // send from admin to merchant

                        //mailServer = mailConfigBusiness.GetConfigMailServer(null);

                        //userName = mailServer.UserName;
                        //passWord = mailServer.PassWord;
                        return SendMailWithAmazon(displayName, to, subject, body);
                      //  break;
                    default: // send from admin
                        if (mailServer != null && mailServer.isApply == false)
                        {
                           // mailServer = mailConfigBusiness.GetConfigMailServer(null, 1);
                            return SendMailWithAmazon(displayName, to, subject, body);
                        }

                        userName = mailServer.UserName;
                        passWord = mailServer.PassWord;
                        break;
                }
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.To.Add(to);
                mail.From = new MailAddress(userName, displayName, System.Text.Encoding.UTF8);
                mail.Subject = subject;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = body;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                SmtpClient client = new SmtpClient(mailServer.OutgoingSmtp, mailServer.OutgoingPort);

                //Add the Creddentials- use your own email id and password
                System.Net.NetworkCredential nt =
                new System.Net.NetworkCredential(userName, passWord);

                // client.Port = 587; // Gmail works on this port

                client.EnableSsl = mailServer.EnableSsl; //Gmail works on Server Secured Layer
                client.UseDefaultCredentials = false;
                client.Credentials = nt;

                //new Thread(() =>
                //{
                //   client.Send(mail);
                //}).Start();
                client.Send(mail);
                return true;
            }
            catch (Exception e)
            {


                e.Message.ToString();
                return false;
            }
        }

        /// <summary>
        /// Tests the configuration server SMTP.
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="portConfig">The port configuration.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/4/2015-11:54 AM</datetime>
        [HttpPost]
        public void TestConfigServerSMTP(string domain, int portConfig)
        {
            var client = new TcpClient();

            //var server = "smtp.gmail.com";
            //var port = 465;
            var server = domain;
            var port = portConfig;
            client.Connect(server, port);
            // As GMail requires SSL we should use SslStream
            // If your SMTP server doesn't support SSL you can
            // work directly with the underlying stream
            var stream = client.GetStream();
            var sslStream = new SslStream(stream);
            {
                sslStream.AuthenticateAsClient(server);
                using (var writer = new StreamWriter(sslStream))
                using (var reader = new StreamReader(sslStream))
                {
                    writer.WriteLine("EHLO " + server);
                    writer.Flush();
                    Console.WriteLine(reader.ReadLine());
                    // GMail responds with: 220 mx.google.com ESMTP
                }
            }

        }
        /// <summary>
        /// Tests the connection.
        /// </summary>
        /// <param name="smtpServerAddress">The SMTP server address.</param>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/4/2015-11:54 AM</datetime>
        [HttpPost]
        public bool TestConnection(string smtpServerAddress, int port)
        {

            try
            {
                IPHostEntry hostEntry = Dns.GetHostEntry(smtpServerAddress);
                IPEndPoint endPoint = new IPEndPoint(hostEntry.AddressList[0], port);
                using (Socket tcpSocket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
                {
                    //try to connect and test the rsponse for code 220 = success
                    try
                    {
                        tcpSocket.Connect(endPoint);
                        if (!CheckResponse(tcpSocket, 220))
                        {
                            return false;
                        }

                        // send HELO and test the response for code 250 = proper response
                        SendData(tcpSocket, string.Format("HELO {0}\r\n", Dns.GetHostName()));
                        if (!CheckResponse(tcpSocket, 250))
                        {
                            return false;
                        }

                        // if we got here it's that we can connect to the smtp server
                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }

        }
        /// <summary>
        /// Sends the data.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <param name="data">The data.</param>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/4/2015-11:54 AM</datetime>
        private void SendData(Socket socket, string data)
        {
            byte[] dataArray = Encoding.ASCII.GetBytes(data);
            socket.Send(dataArray, 0, dataArray.Length, SocketFlags.None);
        }
        /// <summary>
        /// Checks the response.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <param name="expectedCode">The expected code.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/4/2015-11:54 AM</datetime>
        private bool CheckResponse(Socket socket, int expectedCode)
        {
            while (socket.Available == 0)
            {
                System.Threading.Thread.Sleep(100);
            }
            byte[] responseArray = new byte[1024];
            socket.Receive(responseArray, 0, socket.Available, SocketFlags.None);
            string responseData = Encoding.ASCII.GetString(responseArray);
            int responseCode = Convert.ToInt32(responseData.Substring(0, 3));
            if (responseCode == expectedCode)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Valids the SMTP.
        /// </summary>
        /// <param name="hostName">Name of the host.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/4/2015-4:52 PM</datetime>
        public bool ValidSMTP(string hostName, int port)
        {
            TcpClient smtpTest = new TcpClient();
            bool valid = false;
            try
            {

                smtpTest.Connect(hostName, port);
                if (smtpTest.Connected)
                {
                    NetworkStream ns = smtpTest.GetStream();
                    StreamReader sr = new StreamReader(ns);
                    if (sr.ReadLine().Contains("220"))
                    {
                        valid = true;
                    }

                }
                smtpTest.Close();
            }
            catch
            {

            }
            finally
            {
                smtpTest.Close();

            }
            return valid;
        }
        public bool ValidSMTP(string hostName, int port, TimeSpan waitTimeSpan)
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                IAsyncResult result = tcpClient.BeginConnect(hostName, port, null, null);
                WaitHandle timeoutHandler = result.AsyncWaitHandle;
                try
                {
                    if (!result.AsyncWaitHandle.WaitOne(waitTimeSpan, false))
                    {
                        tcpClient.Close();
                        return false;
                    }

                    tcpClient.EndConnect(result);
                    tcpClient.Close();
                }
                catch
                {
                    return false;
                }
                finally
                {
                    tcpClient.Close();
                    timeoutHandler.Close();
                }
                return true;
            }
        }
        public bool ValidPOP(string hostName, int port, TimeSpan waitTimeSpan)
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                IAsyncResult result = tcpClient.BeginConnect(hostName, port, null, null);
                WaitHandle timeoutHandler = result.AsyncWaitHandle;
                try
                {
                    if (!result.AsyncWaitHandle.WaitOne(waitTimeSpan, false))
                    {
                        tcpClient.Close();
                        return false;
                    }

                    tcpClient.EndConnect(result);
                    tcpClient.Close();
                }
                catch
                {
                    return false;
                }
                finally
                {
                    tcpClient.Close();
                    timeoutHandler.Close();
                }
                return true;
            }
        }
        public bool ValidPOP(string hostName, int port, string user, string pass)
        {
            string strInfor = string.Empty;
            string str = string.Empty;
            string strTemp = string.Empty;
            using (TcpClient tc = new TcpClient())
            {
                //tc.Connect("pop3.live.com", 995);
                try
                {
                    tc.Connect(hostName, port);
                    tc.Close();
                    return true;
                }
                catch
                {
                    tc.Close();
                    return false;
                }
                //using (SslStream sl = new SslStream(tc.GetStream()))
                //{
                //    //sl.AuthenticateAsClient("pop3.live.com");
                //    sl.AuthenticateAsClient(hostName);
                //   // X509CertificateCollection clientCertificates = new X509CertificateCollection();
                //   // SslProtocols enabledSslProtocols = 0;
                //   // bool checkCertificateRevocation = false;
                //   //sl.AuthenticateAsClient(hostName, clientCertificates, enabledSslProtocols, checkCertificateRevocation);
                //   // System.Net.NetworkCredential nt = new System.Net.NetworkCredential(user, pass);

                //    // client.Port = 587; // Gmail works on this port

                //    using (StreamReader sr = new StreamReader(sl))
                //    {
                //        using (StreamWriter sw = new StreamWriter(sl))
                //        {
                //            sw.WriteLine("USER " + user);
                //            sw.Flush();
                //            sw.WriteLine("PASS " + pass);
                //            sw.Flush();
                //            sw.WriteLine("LIST");
                //            sw.Flush();
                //            sw.WriteLine("QUIT ");
                //            sw.Flush();
                //            strInfor += "USER " + user;
                //            strInfor += "  pass " + pass;

                //            while ((strTemp = sr.ReadLine()) != null)
                //            {
                //                if (strTemp == "." || strTemp.IndexOf("-ERR") != -1)
                //                {
                //                    break;
                //                }
                //                str += strTemp;
                //            }
                //        }
                //    }
                //}
            }
            //Console.WriteLine(str);
            //Console.WriteLine(strInfor);
        }

        public Boolean SendMailWithAmazon(string displayName,string emailReceive, string subject, string body)
        {
            const String FROM = "noreply@smoovpos.com";  
            const String SMTP_USERNAME = "AKIAIHSDDBMKZXZF2DUA";  // Replace with your SMTP username. 
            const String SMTP_PASSWORD = "AkXZ/qAYjqmQT0qg9XqMlDwHhyN+1rO3UWIWBSZuUF3E";  // Replace with your SMTP password.
            // Amazon SES SMTP host name. This example uses the us-west-2 region.
            const String HOST = "email-smtp.us-east-1.amazonaws.com";

            // Port we will connect to on the Amazon SES SMTP endpoint. We are choosing port 587 because we will use
            // STARTTLS to encrypt the connection.
            const int PORT = 587;

            // Create an SMTP client with the specified host name and port.
            using (System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(HOST, PORT))
            {
                // Create a network credential with your SMTP user name and password.
                client.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);

                // Use SSL when accessing Amazon SES. The SMTP session will begin on an unencrypted connection, and then 
                // the client will issue a STARTTLS command to upgrade to an encrypted connection using SSL.
                client.EnableSsl = true;
                
                // Send the email. 
                try
                {
                    logger.InfoFormat("Mail Server with in for from: {0} , Email receive{1}, subject {2} , body {3}  ", FROM, emailReceive, subject, body);
                    //  Console.WriteLine("Attempting to send an email through the Amazon SES SMTP interface...");
                    MailMessage mm = new MailMessage(FROM, emailReceive, subject, body);
                    mm.IsBodyHtml = true;
                    client.Send(mm);
                   
                    //System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                    //mail.To.Add(FROM);
                    //mail.From = new MailAddress(emailReceive, displayName, System.Text.Encoding.UTF8);
                    //mail.Subject = subject;
                    //mail.SubjectEncoding = System.Text.Encoding.UTF8;
                    //mail.Body = body;
                    //mail.BodyEncoding = System.Text.Encoding.UTF8;
                    //mail.IsBodyHtml = true;
                    //mail.Priority = MailPriority.High;

                    //client.Send(mail);
                    //  Console.WriteLine("Email sent!");
                    return true;
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message.ToString());
                    ex.Message.ToString();
                    return false;
                    // Console.WriteLine("The email was not sent.");
                    // Console.WriteLine("Error message: " + ex.Message);
                }
            }

            // Console.Write("Press any key to continue...");
            // Console.ReadKey();
        }
    }
}
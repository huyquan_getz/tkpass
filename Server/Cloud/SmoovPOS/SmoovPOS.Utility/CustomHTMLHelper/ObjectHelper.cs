﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Web.Script.Serialization;

namespace SmoovPOS.Utility.CustomHTMLHelper
{
    public static class ObjectHelper
    {
        public static T DeepClone<T>(this T obj)
        {
            var result = new JavaScriptSerializer().Serialize(obj);
            if (!string.IsNullOrEmpty(result))
            {
                JObject jObject = JObject.Parse(result);

                var tObj = jObject.ToObject<T>();

                return tObj;
            }

            return obj;
        }
    }
}
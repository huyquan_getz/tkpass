﻿using SmoovPOS.Common;
using SmoovPOS.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.Utility.CustomAttributes
{
    public class LocalizedRegularExpressionAttribute : RegularExpressionAttribute, IClientValidatable
    {
        private string Pattern { get; set; }

        public string Name { get; set; }

        public LocalizedRegularExpressionAttribute(string pattern)
            : base(pattern)
        {
            Pattern = pattern;
        }

        public LocalizedRegularExpressionAttribute(string pattern, string name)
            : base(pattern)
        {
            Pattern = pattern;
            Name = name;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var messageErrorMessage = DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText(ConstantSmoovs.Attributes.ResourceRegularInvalid);
            var fieldName = DependencyResolver.Current.GetService<ILanguageManager>().GetLabelText(Name);

            var messageResource = messageErrorMessage != ConstantSmoovs.Attributes.ResourceUndefined ?
                string.Format(messageErrorMessage, fieldName != ConstantSmoovs.Attributes.ResourceUndefined ? fieldName : metadata.PropertyName) : ConstantSmoovs.Attributes.ResourceUndefined;

            return new[] { new ModelClientValidationRegexRule(messageResource, Pattern) };
        }
    }
}
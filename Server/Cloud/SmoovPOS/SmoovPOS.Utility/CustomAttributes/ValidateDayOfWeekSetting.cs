﻿using SmoovPOS.Entity.Entity.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace SmoovPOS.Utility.CustomAttributes
{
    public class ValidateDayOfWeekSetting : ValidationAttribute
    {
        private const string _defaultErrorMessage = "Input not valid.";
        private readonly object _typeId = new object();

        public ValidateDayOfWeekSetting(string _ListDayOfWeekSetting)
            : base(_defaultErrorMessage)
        {
            ListDayOfWeekSetting = _ListDayOfWeekSetting;
        }

        public object ListDayOfWeekSetting { get; private set; }
        public string DayOfWeekValue { get; private set; }
        public string OptionSetting { get; private set; }
        public string Open_Opening { get; private set; }
        public string Open_Closing { get; private set; }
        public string Split_OpeningStart { get; private set; }
        public string Split_OpeningEnd { get; private set; }
        public string Split_ClosingStart { get; private set; }
        public string Split_ClosingEnd { get; private set; }

        public override object TypeId
        {
            get
            {
                return _typeId;
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentUICulture, ErrorMessageString);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var listDayOfWeek = new string[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
            
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(value);
            List<DayOfWeekSetting> listDayOfWeekSetting = (List<DayOfWeekSetting>)value;
            foreach (DayOfWeekSetting dows in listDayOfWeekSetting)
            {
                object dayOfWeekValue = dows.DayOfWeek;
                object optionSetting = dows.Option;

                int iDayOfWeekValue = int.Parse(dayOfWeekValue.ToString());
                string strDayOfWeek = listDayOfWeek[iDayOfWeekValue];

                switch (optionSetting.ToString())
                {
                    case "0": // Case Open
                        object open_Opening = dows.Open_Opening;
                        object open_Closing = dows.Open_Closing;
                        if (open_Opening == null || open_Closing == null)
                        {
                            return new ValidationResult(string.Format("Data input on {0} is invalid!", strDayOfWeek));
                            // return false;
                        }
                        else
                        {
                            if (open_Opening.ToString() == "__:__" || open_Closing.ToString() == "__:__")
                            {
                                return new ValidationResult(string.Format("Data input on {0} is invalid!", strDayOfWeek));
                                // return false;
                            }
                            int setting_open_opening_hour = -1;
                            int setting_open_opening_minute = -1;
                            int setting_open_closing_hour = -1;
                            int setting_open_closing_minute = -1;
                            if (int.TryParse(open_Opening.ToString().Split(':')[0], out setting_open_opening_hour)
                                && int.TryParse(open_Opening.ToString().Split(':')[1], out setting_open_opening_minute)
                                && int.TryParse(open_Closing.ToString().Split(':')[0], out setting_open_closing_hour)
                                && int.TryParse(open_Closing.ToString().Split(':')[1], out setting_open_closing_minute))
                            {
                                TimeSpan tsOpen_Opening = new TimeSpan(setting_open_opening_hour, setting_open_opening_minute, 0);
                                TimeSpan tsOpen_Closing = new TimeSpan(setting_open_closing_hour, setting_open_closing_minute, 0);
                                if (tsOpen_Opening >= tsOpen_Closing)
                                {
                                    return new ValidationResult(string.Format("Data input on {0} is invalid!", strDayOfWeek));
                                }
                            }
                        }
                        break;
                    case "1": // Case Split
                        object split_OpeningStart = dows.Split_OpeningStart;
                        object split_OpeningEnd = dows.Split_OpeningEnd;
                        object split_ClosingStart = dows.Split_ClosingStart;
                        object split_ClosingEnd = dows.Split_ClosingEnd;
                        if (split_OpeningStart == null || split_OpeningEnd == null || split_ClosingStart == null || split_ClosingEnd == null)
                        {
                            return new ValidationResult(string.Format("Data input on {0} is invalid!", strDayOfWeek));
                        }
                        else
                        {
                            if (split_OpeningStart.ToString() == "__:__" || split_OpeningEnd.ToString() == "__:__" || split_ClosingStart.ToString() == "__:__" || split_ClosingEnd.ToString() == "__:__")
                            {
                                return new ValidationResult(string.Format("Data input on {0} is invalid!", strDayOfWeek));
                            }
                            int setting_split_openingstart_hour = -1;
                            int setting_split_openingstart_minute = -1;
                            int setting_split_openingend_hour = -1;
                            int setting_split_openingend_minute = -1;
                            int setting_split_closingstart_hour = -1;
                            int setting_split_closingstart_minute = -1;
                            int setting_split_closingend_hour = -1;
                            int setting_split_closingend_minute = -1;
                            if (int.TryParse(split_OpeningStart.ToString().Split(':')[0], out setting_split_openingstart_hour)
                                && int.TryParse(split_OpeningStart.ToString().Split(':')[1], out setting_split_openingstart_minute)
                                && int.TryParse(split_OpeningEnd.ToString().Split(':')[0], out setting_split_openingend_hour)
                                && int.TryParse(split_OpeningEnd.ToString().Split(':')[1], out setting_split_openingend_minute)
                                && int.TryParse(split_ClosingStart.ToString().Split(':')[0], out setting_split_closingstart_hour)
                                && int.TryParse(split_ClosingStart.ToString().Split(':')[1], out setting_split_closingstart_minute)
                                && int.TryParse(split_ClosingEnd.ToString().Split(':')[0], out setting_split_closingend_hour)
                                && int.TryParse(split_ClosingEnd.ToString().Split(':')[1], out setting_split_closingend_minute))
                            {
                                TimeSpan tsSplit_OpeningStart = new TimeSpan(setting_split_openingstart_hour, setting_split_openingstart_minute, 0);
                                TimeSpan tsSplit_OpeningEnd = new TimeSpan(setting_split_openingend_hour, setting_split_openingend_minute, 0);
                                TimeSpan tsSplit_ClosingStart = new TimeSpan(setting_split_closingstart_hour, setting_split_closingstart_minute, 0);
                                TimeSpan tsSplit_ClosingEnd = new TimeSpan(setting_split_closingend_hour, setting_split_closingend_minute, 0);

                                if ((tsSplit_OpeningStart >= tsSplit_OpeningEnd) || (tsSplit_ClosingStart >= tsSplit_ClosingEnd))
                                {
                                    return new ValidationResult(string.Format("Data input on {0} is invalid!", strDayOfWeek));
                                }
                            }
                        }
                        break;
                }
            }
            
            return ValidationResult.Success;
        }
    }
}
﻿using SmoovPOS.Common;
using SmoovPOS.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.Utility.CustomAttributes
{
    public class LocalizedStringLengthAttribute : StringLengthAttribute, IClientValidatable
    {
        public string Name { get; set; }

        public LocalizedStringLengthAttribute(int maximumLength)
            : base(maximumLength)
        {
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var fieldName = !string.IsNullOrEmpty(Name) ? DependencyResolver.Current.GetService<ILanguageManager>().GetLabelText(Name) : metadata.PropertyName;
            var requiredMessageResource = string.Empty;
            var resourceCode = string.Empty;

            var isMaximum = MinimumLength == 0 && MaximumLength != 0;
            var isMaxAndMin = MinimumLength != 0 && MaximumLength != 0;
            var isMinimum = MinimumLength != 0 && MaximumLength == 0;

            if (isMaximum)
            {
                resourceCode = ConstantSmoovs.Attributes.ResourceStringLengthDefaultCode;
            }
            else if (isMaxAndMin)
            {
                resourceCode = ConstantSmoovs.Attributes.LengthInvalidMinAndMaxDefaultCode;
            }
            else if (isMinimum)
            {
                resourceCode = ConstantSmoovs.Attributes.LengthInvalidMinimumDefaultCode;
            }

            var messageErrorMessage = DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText(resourceCode);
            if (messageErrorMessage != ConstantSmoovs.Attributes.ResourceUndefined)
            {
                if (isMaximum)
                {
                    requiredMessageResource = string.Format(messageErrorMessage, fieldName, MaximumLength);
                }
                else if (isMaxAndMin)
                {
                    requiredMessageResource = string.Format(messageErrorMessage, fieldName, MinimumLength, MaximumLength);
                }
                else if (isMinimum)
                {
                    requiredMessageResource = string.Format(messageErrorMessage, fieldName, MinimumLength);
                }
            }
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                var errorMessage = DependencyResolver.Current.GetService<ILanguageManager>().GetLabelText(ErrorMessage);
                return new[] { new ModelClientValidationStringLengthRule(errorMessage, MinimumLength, MaximumLength) };
            }
            return new[] { new ModelClientValidationStringLengthRule(requiredMessageResource, MinimumLength, MaximumLength) };
        }
    }
}
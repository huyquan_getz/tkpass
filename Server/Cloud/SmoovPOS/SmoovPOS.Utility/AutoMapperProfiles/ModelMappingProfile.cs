﻿using AutoMapper;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Utility.AutoMapperProfiles
{
    public class ModelMappingProfile : AutoMapper.Profile
    {
        public override string ProfileName
        {
            get { return "ModelMappingProfile"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<MerchantManagementModel, ChangePasswordMerchantManagementViewModel>();
        }
    }
}
﻿using Com.SmoovPOS.Common.Controllers;
using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.WSProductItemReference;
using SmoovPOS.Utility.WSWebContentReference;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.Utility.Controllers
{
    public class UploadController : Controller
    {
        // GET: Upload
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string UploadImage()//(string imageType, int? width, int? height)
        {
            string imageType = string.Empty;
            int size_width = 0;
            int size_height = 0;
            bool isProduct = false;
            bool isLogo = false;
            string _id = string.Empty;
            //string listImage = string.Empty;
            //string totalImageStatic = string.Empty;
            int indexImage = 0;
            string html1 = "", html2 = "", html3 = "";
            NameValueCollection nvc = Request.Form;
            if (nvc.HasKeys())
            {
                int uidV = 0;
                foreach (var item in nvc.Keys)
                {
                    if (item.ToString() == "imageType")
                    {
                        imageType = nvc[item.ToString()].ToString();
                    }
                    if (item.ToString() == "width")
                    {
                        size_width = Convert.ToInt32(nvc[item.ToString()].ToString());
                    }
                    if (item.ToString() == "height")
                    {
                        size_height = Convert.ToInt32(nvc[item.ToString()].ToString());
                    }
                    if (item.ToString() == "isProduct")
                    {
                        if (int.TryParse(nvc[item.ToString()].ToString(), out uidV))
                        {
                            if (uidV != 0)
                            {
                                isProduct = true;
                            }
                        }
                    }
                    //if (item.ToString() == "listImage")
                    //{
                    //    listImage = nvc[item.ToString()].ToString();
                    //}
                    if (item.ToString() == "totalImageStatic")
                    {
                        indexImage = Convert.ToInt32(nvc[item.ToString()].ToString());
                    }
                    if (item.ToString() == "isLogo")
                    {
                        isLogo = Convert.ToBoolean(nvc[item.ToString()].ToString());
                    }
                    if (item.ToString() == "_id")
                    {
                        _id = nvc[item.ToString()].ToString();
                    }
                }
            }

            StringBuilder builder = new StringBuilder();
            string pathIcon = Url.Content("~/Content/Images/imageDefault.png");
            if (isProduct)
            {
                pathIcon = Url.Content("~/Content/Images/delete-ico.png");
            }
            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase photo = Request.Files[i] as HttpPostedFileBase;

                //====
                int index = indexImage + i + 1;
                if (photo != null && photo.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(photo.FileName);
                    fileName = Regex.Replace(fileName, @"\s|\$|\#\%", "");
                    fileName = Regex.Replace(fileName, "[^0-9a-zA-Z]+", "");

                    var fileType = photo.FileName.Substring(photo.FileName.LastIndexOf(".") + 1);
                    if (fileName.Split('.').Length < 2 || fileName.Split('.')[1] != fileType)
                    {
                        fileName = fileName.Substring(0, fileName.LastIndexOf(fileType));
                        if (fileName.Length > 150)
                        {
                            fileName = fileName.Substring(0, 150);
                        }
                        
                        fileName = fileName + "." + fileType;
                    }

                    int random = Guid.NewGuid().GetHashCode();
                    var savedFileName = Path.Combine(Server.MapPath("~/Content/Upload/"), random.ToString() + "_" + fileName);

                    PhotosController ph = new PhotosController();
                    //photo.SaveAs(savedFileName);
                    String path = savedFileName.Replace("\\", "/").Replace(" ", "");

                    #region resize image
                    var size_Image = new Size(size_width, size_height);
                    if (photo.ContentType.Contains("image"))
                    {
                        if (string.IsNullOrEmpty(imageType))
                        {
                            imageType = ConstantSmoovs.Image_Type.Normal;
                        }

                        switch (imageType)
                        {
                            case ConstantSmoovs.Image_Type.Big:
                                size_Image = ph.ResizeAndSave(photo, path, ConstantSmoovs.Image_Resize.width_big_Resize, ConstantSmoovs.Image_Resize.height_big_Resize);
                                if (size_width == 0 || size_height == 0)
                                {
                                    html1 = "<div id=\"group-image\" style=\"position: relative;min-width: 263px; min-height: 170px;width: 263px!important;height: 170px!important;\">";
                                }
                                else
                                {
                                    html1 = "<div id=\"group-image\" style=\"position: relative;min-width: 263px; min-height: 170px;width: 263px!important;height: 170px!important;\">";
                                }

                                if (size_width == 0)
                                {
                                    size_width = ConstantSmoovs.Image_Resize.width_big_Resize;
                                }
                                if (size_height == 0)
                                {
                                    size_height = ConstantSmoovs.Image_Resize.height_big_Resize;
                                }

                                break;
                            case ConstantSmoovs.Image_Type.Normal:
                                size_Image = ph.ResizeAndSave(photo, path, ConstantSmoovs.Image_Resize.width_Resize, ConstantSmoovs.Image_Resize.height_Resize);
                                if (size_width == 0)
                                {
                                    size_width = ConstantSmoovs.Image_Resize.width_Resize;
                                }
                                if (size_height == 0)
                                {
                                    size_height = ConstantSmoovs.Image_Resize.height_Resize;
                                }
                                html1 = "<div id=\"group-image\" style=\"position: relative;min-width: 263px; min-height: 170px;width: 263px!important;height: 170px!important;\">";
                                break;
                            case ConstantSmoovs.Image_Type.Small:
                                size_Image = ph.ResizeAndSave(photo, path, ConstantSmoovs.Image_Resize.width_List, ConstantSmoovs.Image_Resize.height_List);
                                if (size_width == 0)
                                {
                                    size_width = ConstantSmoovs.Image_Resize.width_List;
                                }
                                if (size_height == 0)
                                {
                                    size_height = ConstantSmoovs.Image_Resize.height_List;
                                }
                                html1 = "<div id=\"group-image\" style=\"position: relative;min-width: 263px; min-height: 170px;width: 263px!important;height: 170px!important;\">";
                                break;
                            case ConstantSmoovs.Image_Type.WebContentLogo:
                                size_Image = ph.ResizeAndSave(photo, path, ConstantSmoovs.Image_Resize.width_WebContentLogo_Big, ConstantSmoovs.Image_Resize.height_WebContentLogo_Big);
                                if (size_width == 0)
                                {
                                    size_width = ConstantSmoovs.Image_Resize.width_WebContentLogo;
                                }
                                if (size_height == 0)
                                {
                                    size_height = ConstantSmoovs.Image_Resize.height_WebContentLogo;
                                }
                                html1 = "<div id=\"group-image\" style=\"position: relative;min-width: 250px; min-height: 150px;width: 250px!important;height: 150px!important;\">";
                                break;
                            default:
                                break;
                        }
                    }
                    #endregion
                    //photo.SaveAs(savedFileName);
                    //String path = savedFileName.Replace("\\", "/").Replace(" ", "");

                    //============== SAVE TO AMAZON
                    string linkImageS3 = ph.UploadToS3Amazon(path);
                    System.IO.File.Delete(path);

                    if (isLogo)
                    {
                        ViewBag.Id = _id;
                        WSWebContentClient client = new WSWebContentClient();
                        WebContent webContent = new WebContent();
                        webContent = client.WSDetailWebContent(_id, VariableConfigController.GetBucket());
                        webContent.logo = linkImageS3;
                        int update = client.WSUpdateWebContent(webContent, VariableConfigController.GetBucket());
                        client.Close();
                    }
                    #region general html
                    size_Image = PhotosController.CalculateDimensions(size_Image, size_width, size_height);
                    //============== SAVE TO AMAZON
                    if (!string.IsNullOrWhiteSpace(linkImageS3))
                    {
                        if (isProduct)
                        {
                            //width="@size.Width" height="@size.Height" style="margin-top:@(140 - size.Height - 40)px"
                            string html4 = "", html5 = "", html6 = "";
                            html1 = "<div class=\"col-md-3\" name=\"divDynamic\" style=\"width: 150px; height: 140px;float:left;\" id=\"" + "div_img_" + index + "\">";
                            html2 = "<div class=\"row radio padding-left-0\">";

                            //html3 = "<img src='" + linkImageS3 + "'class=\"img-rounded product-image\" alt=\"140x140\" style=\"width:140px;height:105px;\" >";
                            size_Image = PhotosController.CalculateDimensions(size_Image, ConstantSmoovs.Image_Resize.width_List, ConstantSmoovs.Image_Resize.height_Resize_Product);
                            html3 = "<img src='" + linkImageS3 + "'  width=" + size_Image.Width + "  height=" + size_Image.Height + " class=\"img-rounded product-image\" style=\"margin-top:" + (140 - size_Image.Height - 40) + "px;\" >";

                            html4 = "<br /><input type=\"radio\" name=\"radioDynamic\" value=\"" + index + "\" id=\"rad_" + index + "\"style=\"margin-right:3px;\" onchange=\"changeRadio(" + index + ")\">";
                            html5 = "<label for=\"rad_" + index + "\" id=\"lbl_" + index + "\"> Set Default </label> ";
                            html6 = "<img src='" + pathIcon + "' onclick=\"javascript:deleteImage(" + index + ")\" class=\"imgRemove\" width=\"15\" height=\"15\" id=\"div_img_" + index + "\"/></div></div>";
                            builder.Append(html1).Append(html2).Append(html3).Append(html4).Append(html5).Append(html6);
                        }
                        else
                        {
                            //html1 = "<div id=\"group-image\" style=\"position: relative;min-width: 263px; min-height: 170px;width: 263px!important;height: 170px!important;\">";
                            html2 = "<img src='" + linkImageS3 + "'  width=" + size_Image.Width + "  height=" + size_Image.Height + " id=\"thumbnil\" style=\"position: absolute;width:" + size_Image.Width.ToString() + "!important; height:" + size_Image.Height.ToString() + "!important;bottom:0px;\" class=\"img-rounded category-image\" /> <div class=\"q-clear\"></div>";
                            html3 = " <button class=\"btn btn-default\" onclick=\"deleteImage()\" style=\"position: absolute;bottom: 2px;left: 2px;min-width:37px!important;padding-left: 0px!important;padding-right: 0px!important;\">   <i class=\"fa fa-trash\">    </i> </button> </div><input id=\"logoImg\" name=\"logoImg\" type=\"hidden\" value='" + linkImageS3 + "' /></div>";

                            //html2 = "<img src='" + linkImageS3 + "' id=\"thumbnil\" style=\"position: absolute;width:auto!important; height:auto!important;bottom:0px;\" class=\"img-rounded category-image\" /> <div class=\"q-clear\"></div>";

                            builder.Append(html1).Append(html2).Append(html3);
                        }
                    }
                    #endregion
                }
            }
            return builder.ToString();
        }

        /// <summary>
        /// Updates the image variant.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/30/2015-7:03 PM</datetime>
        [HttpPost]
        public int updateImageVariant()
        {
            string bucket = "";
            bucket = VariableConfigController.GetBucket();
            NameValueCollection nvc = Request.Form;
            string productIDVariant;
            string patternURL = ",";
            Regex myRegex = new Regex(patternURL);
            if (!string.IsNullOrEmpty(nvc["productIDVariant"]))
            {
                productIDVariant = nvc["productIDVariant"];
                WSProductItemClient client = new WSProductItemClient();
                var productItem = client.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, productIDVariant, bucket);
                foreach (ProductItem product in productItem)
                {

                    if (!string.IsNullOrWhiteSpace(nvc["arrayImageVariant"]))
                    {

                        string[] resultStatic = myRegex.Split(nvc["arrayImageVariant"]);
                        if (product.image != null)
                        {
                            product.image.arrayImage = new List<string>();
                            product.image.arrayImage.AddRange(resultStatic.ToList());
                        }
                        else
                        {
                            product.image = new ImageItem();
                            product.image.imageDetault = "0";
                            product.image.arrayImage.AddRange(resultStatic.ToList());
                        }

                    }
                    if (!string.IsNullOrWhiteSpace(nvc["_idVariant"]) && !string.IsNullOrWhiteSpace(nvc["imageDefaultVariant"]))
                    {
                        foreach (VarientItem item in product.arrayVarient.Where(r => r._id == nvc["_idVariant"]))
                        {
                            item.image.imageDetault = nvc["imageDefaultVariant"];
                            client.WSUpdateProductItem(product, bucket);
                        }
                    }
                }
            }


            return 1;


        }
    }
}
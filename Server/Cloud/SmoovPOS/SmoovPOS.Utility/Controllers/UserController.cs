﻿using Com.SmoovPOS.UI.Models;
using SmoovPOS.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmoovPOS.Utility.WSMerchantManagementReference;
using SmoovPOS.Utility.WSSiteReference;
using Com.SmoovPOS.Model;
using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.WSServicePacketReference;

namespace SmoovPOS.Utility.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>1/30/2015-5:30 PM</datetime>
    public class UserController : Controller
    {
        /// <summary>
        /// Loads the user information.
        /// </summary>
        /// <param name="userManager">The user manager.</param>
        /// <param name="user">The user.</param>
        /// <param name="model">The model.</param>
        /// <param name="merchant">The merchant.</param>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:30 PM</datetime>
        public void LoadUserInformation(UserManager<ApplicationUser> userManager, ApplicationUser user, LoginViewModel model, MerchantManagementModel merchant)
        {
            #region add user to session

            var userSession = new SmoovPOS.Common.Controllers.UserSession();

            userSession.Bucket = SmoovPOS.Common.ConstantSmoovs.smoovpos;
            userSession.UserId = user.Id;
            userSession.UserName = model.UserName;
            //get all role by user
            //userSession.Roles = user.Roles.Select(x => x.Role.Name).ToArray();

            if (userManager.IsInRole(user.Id, SmoovPOS.Common.ConstantSmoovs.RoleNames.Merchant))
            {
                if (merchant == null)
                {
                    IWSMerchantManagementClient client = new IWSMerchantManagementClient();
                    var result = client.WSGetMerchantManagementByUserId(user.Id);
                    client.Close();
                    merchant = result;
                }
            }

            if (merchant != null && !string.IsNullOrEmpty(merchant.bucket))
            {
                userSession.Bucket = merchant.bucket;//set bucket
                userSession.BusinessName = merchant.businessName;

                WSSiteClient clientSite = new WSSiteClient();
                var siteAlias = clientSite.WSGetSiteAliasBySiteID(merchant.bucket);
                if (siteAlias != null)
                {
                    userSession.Domain = siteAlias.HTTP;
                    userSession.SubDomain = siteAlias.HTTPAlias;
                    userSession.CheckSubDomain = siteAlias.CheckSubdomain.GetValueOrDefault();
                }

                if (merchant.servicePacketID.HasValue)
                {
                    IWSServicePacketClient clientServicePacket = new IWSServicePacketClient();
                    var servicePacket = clientServicePacket.WSGetServicePacket(merchant.servicePacketID.GetValueOrDefault());
                    clientServicePacket.Close();
                    userSession.ServicePacket = servicePacket;
                }
            }
            //var role = RoleManager.FindByName<IdentityRole>(SmoovPOS.Common.Constants.RoleNames.Admin);
            //if (role != null)
            //{
            //    UserManager.UserValidator = new UserValidator<ApplicationUser>(UserManager) { AllowOnlyAlphanumericUserNames = false };

            //    var result = await UserManager.AddToRoleAsync(user.Id, role.Name);
            //    if (result.Succeeded)
            //    {
            //        user.UserName = model.UserName;
            //        await UserManager.UpdateAsync(user);
            //    }
            //}
            #endregion
        }


        /// <summary>
        /// Add Infomation user online to session.
        /// </summary>
        /// <param name="userManager">The user manager.</param>
        /// <param name="user">The user.</param>
        /// <param name="model">The model.</param>
        /// <param name="merchant">The merchant.</param>
        /// <author>"Hung.Do"</author>
        /// <datetime>25/02/2015-5:30 PM</datetime>
        public void AddUserSessionOnline(Customer customer)
        {
            #region add user to session

            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            userSession.UserId = customer._id;
            userSession.UserName = customer.email;
            userSession.FirstName = customer.firstName;

            #endregion
        }

        /// <summary>
        /// Remove Infomation user online to session.
        /// </summary>
        /// <param name="userManager">The user manager.</param>
        /// <param name="user">The user.</param>
        /// <param name="model">The model.</param>
        /// <param name="merchant">The merchant.</param>
        /// <author>"Hung.Do"</author>
        /// <datetime>25/02/2015-5:30 PM</datetime>
        public void RemoveUserSessionOnline()
        {
            #region add user to session

            var userSession = new SmoovPOS.Common.Controllers.UserSession();
            userSession.UserId = string.Empty;
            userSession.UserName = string.Empty;
            userSession.FirstName = string.Empty;

            #endregion
        }



    }
}
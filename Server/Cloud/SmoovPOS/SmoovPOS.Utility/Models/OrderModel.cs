﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using System;
using System.Drawing;
using System.Linq;
using Com.SmoovPOS.TimeZone;
using System.Text;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;

namespace SmoovPOS.Utility.Models
{
   
    public class OrderModel : Order
    {
        public DateTime createDateShow;
        public OrderModel(Order order, string siteID)
        {
            _id = !string.IsNullOrWhiteSpace(order._id) ? order._id : Guid.NewGuid().ToString();
            orderCode = order.orderCode;
            inventory = order.inventory;
            transactionID = order.transactionID;
            // completedDate = order.completedDate;
            completedDate = orderType == 1 ? TimeZoneUtil.ConvertUTCToTimeZone(order.completedDate, inventory.timeZone, siteID) :
                inventory != null ? TimeZoneUtil.ConvertUTCToTimeZone(order.completedDate, inventory.timeZone, siteID) : order.completedDate;
            customerSender = order.customerSender;
            customerReceiver = order.customerReceiver;
            paymentStatus = order.paymentStatus;
            orderStatus = order.orderStatus;
            subTotal = order.subTotal;
            orderType = order.orderType;
            // inventory = order.inventory;

            arrayProduct = order.arrayProduct;
            remark = order.remark;
            staff = order.staff;
            arrayPayment = order.arrayPayment;
            grandTotal = order.grandTotal;
            discount = order.discount;
          //  tax = order.tax;
            roundAmount = order.roundAmount;
            listTaxvalue = order.listTaxvalue;
            isCancelRequest = order.isCancelRequest;
            refundInfo = order.refundInfo;

            createDateShow = orderType == 1 ? TimeZoneUtil.ConvertUTCToTimeZone(order.createdDate, inventory.timeZone, siteID) :
             inventory != null ? TimeZoneUtil.ConvertUTCToTimeZone(order.createdDate, inventory.timeZone, siteID) : order.createdDate;
            if (refundInfo != null && inventory != null)
            {
                refundInfo.refundDate = TimeZoneUtil.ConvertUTCToTimeZone(refundInfo.refundDate, inventory.timeZone, siteID);
            }
            serviceCharge = order.serviceCharge;
            arrayAdhocProduct = order.arrayAdhocProduct;
            modifiedDate = inventory != null ? ((inventory.index == 1) ? TimeZoneUtil.ConvertUTCToTimeZone(order.modifiedDate, inventory.timeZone, siteID)
                                                                        : TimeZoneUtil.ConvertUTCToTimeZone(order.modifiedDate, inventory.timeZone, siteID))
                                            : order.modifiedDate;
        }

        public string StoreDisplay
        {
            get
            {
                return inventory != null ? (!string.IsNullOrWhiteSpace(inventory.businessID) ? inventory.businessID : "Online store") : "Online store";
            }
        }

        public string TransactionDisplay { get { return !string.IsNullOrWhiteSpace(transactionID) ? transactionID : "N/A"; } }

        public string OrderDateDisplay
        {
            get
            {
                //TimeZoneUtil timeUtil = new TimeZoneUtil();
                //DateTime dateShow = timeUtil.ConvertTimeZoneToTimeLocal(completedDate.ToString());
                //return dateShow.ToString("MM/dd/yyyy");

                return completedDate.ToString("MM/dd/yyyy");
            }
        }


        public string PaymentStatusDisplay { get { return ConstantSmoovs.Enums.PaymentStatus.ContainsKey(paymentStatus) ? ConstantSmoovs.Enums.PaymentStatus[paymentStatus] : string.Empty; } }

        public string PaymentStatusDisplayColor
        {
            get
            {
                string color = string.Empty;
                if (ConstantSmoovs.Enums.PaymentStatus.ContainsKey(paymentStatus))
                {
                    switch (ConstantSmoovs.Enums.PaymentStatus[paymentStatus])
                    {
                        case ConstantSmoovs.PaymentStatus.Waiting:
                            color = HexConverter(Color.Orange);
                            break;
                        case ConstantSmoovs.PaymentStatus.Paid:
                            color = HexConverter(Color.ForestGreen);
                            break;
                        case ConstantSmoovs.PaymentStatus.Refunded:
                            color = HexConverter(Color.DarkMagenta);
                            break;
                        case ConstantSmoovs.PaymentStatus.Failed:
                            color = HexConverter(Color.Red);
                            break;
                    }
                }
                return color;
            }
        }

        public string OrderStatusDisplay
        {
            get
            {
                if (orderStatus == 7)
                {
                    return isCancelRequest ? ConstantSmoovs.OrderStatus.CanceledByCustomer : ConstantSmoovs.OrderStatus.CanceledByMerchant;
                }
                else
                {
                    return ConstantSmoovs.Enums.OrderStatus.ContainsKey(orderStatus) ? ConstantSmoovs.Enums.OrderStatus[orderStatus] : string.Empty;
                }
            }
        }

        public string OrderStatusDisplayForCustomer { get { return ConstantSmoovs.Enums.OrderStatusCustomer.ContainsKey(orderStatus) ? ConstantSmoovs.Enums.OrderStatusCustomer[orderStatus] : string.Empty; } }

        public string OrderStatusDisplayColor
        {
            get
            {
                string color = string.Empty;
                if (ConstantSmoovs.Enums.OrderStatus.ContainsKey(orderStatus))
                {
                    switch (ConstantSmoovs.Enums.OrderStatus[orderStatus])
                    {
                        case ConstantSmoovs.OrderStatus.NewOrder:
                            color = HexConverter(Color.Red);
                            break;
                        case ConstantSmoovs.OrderStatus.ReadyForCollection:
                            color = HexConverter(Color.Blue);
                            break;
                        case ConstantSmoovs.OrderStatus.Collected:
                        case ConstantSmoovs.OrderStatus.Received:
                            color = HexConverter(Color.DarkGray);
                            break;
                        case ConstantSmoovs.OrderStatus.Delivered:
                            color = HexConverter(Color.DeepSkyBlue);
                            break;
                        case ConstantSmoovs.OrderStatus.OnDelivery:
                            color = HexConverter(Color.ForestGreen);
                            break;
                        case ConstantSmoovs.OrderStatus.OnPreRequest:
                            color = HexConverter(Color.Orange);
                            break;
                        case ConstantSmoovs.OrderStatus.ReadyForDelivery:
                            color = HexConverter(Color.Blue);
                            break;
                        case ConstantSmoovs.OrderStatus.Canceled:
                            color = HexConverter(Color.Red);
                            break;
                    }
                }

                return color;
            }
        }

        private string HexConverter(Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        public string emailMerchantSupport { get { return ConstantSmoovs.EmailSupport.EmailSupportMerchant; } }

        public string onlineShopLink { get; set; }

        public string businessName
        {
            get
            {
                string businessName = inventory != null ? (!string.IsNullOrWhiteSpace(inventory.businessID) ? inventory.businessID : "") : "";
                return businessName;
            }
            set { }
        }

        public string addressShipping
        {
            get
            {
                System.Text.StringBuilder addressShow = new System.Text.StringBuilder();
                //addressCombine
                if (orderType == 1) // COLLECT POINT
                {
                    if (inventory != null)
                    {
                        addressShow.Append(!string.IsNullOrWhiteSpace(inventory.streetAddress) ? inventory.streetAddress : "");
                        addressShow.Append(!string.IsNullOrWhiteSpace(inventory.streetAddress) ? ", " : "");
                        addressShow.Append(!string.IsNullOrWhiteSpace(inventory.city) ? inventory.city : "");
                        addressShow.Append(!string.IsNullOrWhiteSpace(inventory.city) ? ", " : "");
                        addressShow.Append(!string.IsNullOrWhiteSpace(inventory.state) ? inventory.state : "");
                        addressShow.Append(!string.IsNullOrWhiteSpace(inventory.state) ? " " : "");
                        addressShow.Append(!string.IsNullOrWhiteSpace(inventory.zipCode) ? inventory.zipCode : "");
                        addressShow.Append(!string.IsNullOrWhiteSpace(inventory.zipCode) ? ", " : "");
                        addressShow.Append(!string.IsNullOrWhiteSpace(inventory.country) ? inventory.country : "");
                    }
                }
                else // SHIP TO
                {
                   
                        if (customerReceiver != null)
                        {

                            addressShow.Append(!string.IsNullOrWhiteSpace(customerReceiver.address) ? customerReceiver.address : "");
                            addressShow.Append(!string.IsNullOrWhiteSpace(customerReceiver.address) ? ", " : "");
                            addressShow.Append(!string.IsNullOrWhiteSpace(customerReceiver.city) ? customerReceiver.city : "");
                            addressShow.Append(!string.IsNullOrWhiteSpace(customerReceiver.city) ? ", " : "");
                            addressShow.Append(!string.IsNullOrWhiteSpace(customerReceiver.state) ? customerReceiver.state : "");
                            addressShow.Append(!string.IsNullOrWhiteSpace(customerReceiver.state) ? " " : "");
                            addressShow.Append(!string.IsNullOrWhiteSpace(customerReceiver.zipCode) ? customerReceiver.zipCode : "");
                            addressShow.Append(!string.IsNullOrWhiteSpace(customerReceiver.zipCode) ? ", " : "");
                            addressShow.Append(!string.IsNullOrWhiteSpace(customerReceiver.country) ? customerReceiver.country : "");
                        }
                    
                }
                if (inventory.index != 1)
                {
                    return !string.IsNullOrWhiteSpace(addressShow.ToString()) ? addressShow.ToString() : "Singapore";
                }
                else
                {
                    return !string.IsNullOrWhiteSpace(addressShow.ToString()) ? addressShow.ToString() : "";
                }
               
            }
        }

        public string bussinessID
        {
            get
            {
                string id = inventory != null ? (!string.IsNullOrWhiteSpace(inventory.businessID) ? inventory.businessID : "Online Store") : "Online Store";
                return id;
            }
        }

        public string staffFullName
        {
            get
            {
                string name = staff != null ? ((!string.IsNullOrWhiteSpace(staff.fullName) ? staff.fullName : "N/A")) : "N/A";
                return name;
            }
        }

        public string remarkShow
        {
            get
            {
                return (!string.IsNullOrWhiteSpace(remark) ? remark : "");
            }
        }

        public string dateAndBranchOrderShow
        {
            get
            {
                // TimeZoneUtil timeUtil = new TimeZoneUtil();
                //DateTime dateShow = timeUtil.ConvertGMTToTimeLocal(completedDate);

                StringBuilder timeStore = new StringBuilder();
                IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(String.Empty, false);
                timeStore.Append(completedDate.ToString("MM/dd/yyyy", yyyymmddFormat));
                timeStore.Append(" at "); timeStore.Append(completedDate.ToString("HH:mm:ss", yyyymmddFormat));
                timeStore.Append("   ");
                timeStore.Append(bussinessID);
                return (!string.IsNullOrWhiteSpace(timeStore.ToString()) ? timeStore.ToString() : "");
            }
        }

        public string dateOrderShow
        {
            get
            {
                StringBuilder timeStore = new StringBuilder();
                IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(String.Empty, false);
                timeStore.Append(completedDate.ToString("MM/dd/yyyy", yyyymmddFormat));
                timeStore.Append(" at "); timeStore.Append(completedDate.ToString("HH:mm:ss", yyyymmddFormat));
                return (!string.IsNullOrWhiteSpace(timeStore.ToString()) ? timeStore.ToString() : "");
            }
        }
        public string modifiedDateOrderShow
        {
            get
            {
                StringBuilder timeStore = new StringBuilder();
                IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(String.Empty, false);
                timeStore.Append(modifiedDate.ToString("MM/dd/yyyy", yyyymmddFormat));
                timeStore.Append(" at "); timeStore.Append(modifiedDate.ToString("HH:mm:ss", yyyymmddFormat));
                return (!string.IsNullOrWhiteSpace(timeStore.ToString()) ? timeStore.ToString() : "");
            }
        }
        public string createDateOrderShow
        {
            get
            {

                StringBuilder timeStore = new StringBuilder();
                IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(String.Empty, false);
                timeStore.Append(createDateShow.ToString("MM/dd/yyyy", yyyymmddFormat));
                timeStore.Append(" at "); timeStore.Append(createDateShow.ToString("HH:mm:ss", yyyymmddFormat));
                return (!string.IsNullOrWhiteSpace(timeStore.ToString()) ? timeStore.ToString() : "");
            }
        }

        public string customerFirstNameSender
        {
            get
            {
                return customerSender != null ? ((!string.IsNullOrWhiteSpace(customerSender.firstName) ? customerSender.firstName : "")) : "";
            }
        }

        public string CustomerSenderFullName
        {
            get
            {
                System.Text.StringBuilder nameShow = new System.Text.StringBuilder();
                if (customerSender != null)
                {
                    if (!string.IsNullOrWhiteSpace(customerSender.fullName))
                    {
                        nameShow.Append(customerSender.fullName);
                    }
                    else if (!string.IsNullOrWhiteSpace(customerSender.firstName) && !string.IsNullOrWhiteSpace(customerSender.lastName))
                    {
                        nameShow.Append(customerSender.firstName);
                        nameShow.Append(" ");
                        nameShow.Append(customerSender.lastName);
                    }

                    else if (!string.IsNullOrWhiteSpace(customerSender.firstName))
                    {
                        nameShow.Append(customerSender.firstName);
                    }
                    else if (!string.IsNullOrWhiteSpace(customerSender.lastName))
                    {
                        nameShow.Append(customerSender.firstName);
                    }
                }
                else
                {
                    nameShow.Append(" ");
                }

                return nameShow.ToString();
            }
        }
        public string CustomerReceiverFullName
        {
            get
            {
                System.Text.StringBuilder nameShow = new System.Text.StringBuilder();
                if (orderType == 1)
                {
                    nameShow.Append(bussinessID);
                }
                else if (customerSender != null)
                {
                    if (!string.IsNullOrWhiteSpace(customerReceiver.fullName))
                    {
                        nameShow.Append(customerReceiver.fullName);
                    }
                    else if (!string.IsNullOrWhiteSpace(customerReceiver.firstName) && !string.IsNullOrWhiteSpace(customerReceiver.lastName))
                    {
                        nameShow.Append(customerReceiver.firstName);
                        nameShow.Append(" ");
                        nameShow.Append(customerReceiver.lastName);
                    }

                    else if (!string.IsNullOrWhiteSpace(customerReceiver.firstName))
                    {
                        nameShow.Append(customerReceiver.firstName);
                    }
                    else if (!string.IsNullOrWhiteSpace(customerReceiver.lastName))
                    {
                        nameShow.Append(customerReceiver.firstName);
                    }
                }
                else
                {
                    nameShow.Append(" ");
                }

                return nameShow.ToString();
            }
        }

        public string CustomerSenderAddress
        {
            get
            {
                System.Text.StringBuilder addressShow = new System.Text.StringBuilder();
             if (customerSender != null)
                {

                    addressShow.Append(!string.IsNullOrWhiteSpace(customerSender.address) ? customerSender.address : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(customerSender.address) ? ", " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(customerSender.city) ? customerSender.city : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(customerSender.city) ? ", " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(customerSender.state) ? customerSender.state : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(customerSender.state) ? " " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(customerSender.zipCode) ? customerSender.zipCode : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(customerSender.zipCode) ? ", " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(customerSender.country) ? customerSender.country : "");
                }

                return !string.IsNullOrWhiteSpace(addressShow.ToString()) ? addressShow.ToString() : "";
            }
        }

        public string CustomerReceivePhone
        {
            get
            {
                System.Text.StringBuilder phoneShow = new System.Text.StringBuilder();
                if (orderType == 1) // Self Collect  ==> get phone of receive is  inventory's phone
                {
                    phoneShow.Append(inventory != null ? (!string.IsNullOrWhiteSpace(inventory.phone) ? inventory.phone : "") : "N/A");
                }
                else // Delivery ==> get phone of receive is  receiver's phone
                {
                    phoneShow.Append(customerReceiver != null ? (!string.IsNullOrWhiteSpace(customerReceiver.phone) ? customerReceiver.phone : "") : "N/A");
                }
                return phoneShow.ToString();
            }
        }

        public string CustomerReceiveEmail
        {
            get
            {
                System.Text.StringBuilder emailShow = new System.Text.StringBuilder();
                if (orderType == 1) // Self Collect  ==> get email of receive is  inventory's email
                {
                    emailShow.Append(inventory != null ? (!string.IsNullOrWhiteSpace(inventory.email) ? inventory.email : "") : "N/A");
                }
                else //Delivery ==> get email of receive is  receiver's email
                {
                    emailShow.Append(customerReceiver != null ? (!string.IsNullOrWhiteSpace(customerReceiver.email) ? customerReceiver.email : "") : "N/A");
                }
                return emailShow.ToString();
            }
        }

        public double ShippingFeeShow
        {
            get
            {
                double shippingFee = 0;
                if (serviceShipping != null && serviceShipping.arrayServiceDelivery != null && serviceShipping.arrayServiceDelivery.Count > 0)
                {
                    shippingFee = serviceShipping.arrayServiceDelivery.FirstOrDefault().shippingPrice;
                }
                return shippingFee;
            }
        }

        //public int indexDateSort { get; set; }

        public string RefundReason
        {
            get
            {
                return refundInfo != null ? (!string.IsNullOrWhiteSpace(refundInfo.reasonRefund) ? (orderType == 1 ? GetReasonShow : refundInfo.reasonRefund) : "") : "";
            }
        }
        public string RefundByStaff
        {
            get
            {
                return refundInfo != null ? (!string.IsNullOrWhiteSpace(refundInfo.staffFullName) ? refundInfo.staffFullName : "") : "";
            }
        }
        public string RefundDateShow
        {
            get
            {
                return refundInfo != null ? (!string.IsNullOrWhiteSpace(refundInfo.refundDate.ToString()) ? refundInfo.refundDate.ToString(ConstantSmoovs.DefaultFormats.FullDateName) : "") : "";

            }
        }
        public string GetReasonShow
        {
            get
            {

                string reason = "";
                if (ConstantSmoovs.Enums.RefundReason.ContainsKey(refundInfo.reasonRefund))
                {
                    reason = ConstantSmoovs.Enums.RefundReason.Where(i => i.Key.Contains(refundInfo.reasonRefund)).FirstOrDefault().Value;
                }
                else
                {
                    reason = refundInfo.reasonRefund;
                }
                return reason;
            }
        }

        public string ShowInventoryAdrress
        {
            get
            {
                System.Text.StringBuilder addressShow = new System.Text.StringBuilder();
                if (inventory != null)
                {

                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.streetAddress) ? inventory.streetAddress : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.streetAddress) ? ", " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.city) ? inventory.city : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.city) ? ", " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.state) ? inventory.state : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.state) ? " " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.zipCode) ? inventory.zipCode : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.zipCode) ? ", " : "");
                    addressShow.Append(!string.IsNullOrWhiteSpace(inventory.country) ? inventory.country : "");
                }

                return !string.IsNullOrWhiteSpace(addressShow.ToString()) ? addressShow.ToString() : "";
            }
        }
    }
}
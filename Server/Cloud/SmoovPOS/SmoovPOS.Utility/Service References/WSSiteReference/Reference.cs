﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34011
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmoovPOS.Utility.WSSiteReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="WSSiteReference.IWSSite")]
    public interface IWSSite {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSCreateSite", ReplyAction="http://tempuri.org/IWSSite/WSCreateSiteResponse")]
        string WSCreateSite(Com.SmoovPOS.Model.SiteModel site);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSCreateSite", ReplyAction="http://tempuri.org/IWSSite/WSCreateSiteResponse")]
        System.Threading.Tasks.Task<string> WSCreateSiteAsync(Com.SmoovPOS.Model.SiteModel site);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSCreateSiteAlias", ReplyAction="http://tempuri.org/IWSSite/WSCreateSiteAliasResponse")]
        string WSCreateSiteAlias(Com.SmoovPOS.Model.SiteAliasModel siteAlias);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSCreateSiteAlias", ReplyAction="http://tempuri.org/IWSSite/WSCreateSiteAliasResponse")]
        System.Threading.Tasks.Task<string> WSCreateSiteAliasAsync(Com.SmoovPOS.Model.SiteAliasModel siteAlias);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSUpdateSite", ReplyAction="http://tempuri.org/IWSSite/WSUpdateSiteResponse")]
        string WSUpdateSite(Com.SmoovPOS.Model.SiteModel site);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSUpdateSite", ReplyAction="http://tempuri.org/IWSSite/WSUpdateSiteResponse")]
        System.Threading.Tasks.Task<string> WSUpdateSiteAsync(Com.SmoovPOS.Model.SiteModel site);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSDeleteSite", ReplyAction="http://tempuri.org/IWSSite/WSDeleteSiteResponse")]
        string WSDeleteSite(string _id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSDeleteSite", ReplyAction="http://tempuri.org/IWSSite/WSDeleteSiteResponse")]
        System.Threading.Tasks.Task<string> WSDeleteSiteAsync(string _id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSGetAllSite", ReplyAction="http://tempuri.org/IWSSite/WSGetAllSiteResponse")]
        Com.SmoovPOS.Model.SiteModel[] WSGetAllSite();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSGetAllSite", ReplyAction="http://tempuri.org/IWSSite/WSGetAllSiteResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Model.SiteModel[]> WSGetAllSiteAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSGetAllSiteAlias", ReplyAction="http://tempuri.org/IWSSite/WSGetAllSiteAliasResponse")]
        Com.SmoovPOS.Model.SiteAliasModel[] WSGetAllSiteAlias();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSGetAllSiteAlias", ReplyAction="http://tempuri.org/IWSSite/WSGetAllSiteAliasResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Model.SiteAliasModel[]> WSGetAllSiteAliasAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSCreateSiteSQL", ReplyAction="http://tempuri.org/IWSSite/WSCreateSiteSQLResponse")]
        bool WSCreateSiteSQL(Com.SmoovPOS.Model.SiteModel site);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSCreateSiteSQL", ReplyAction="http://tempuri.org/IWSSite/WSCreateSiteSQLResponse")]
        System.Threading.Tasks.Task<bool> WSCreateSiteSQLAsync(Com.SmoovPOS.Model.SiteModel site);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSCreateSiteAliasSQL", ReplyAction="http://tempuri.org/IWSSite/WSCreateSiteAliasSQLResponse")]
        bool WSCreateSiteAliasSQL(Com.SmoovPOS.Model.SiteAliasModel siteAlias);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSCreateSiteAliasSQL", ReplyAction="http://tempuri.org/IWSSite/WSCreateSiteAliasSQLResponse")]
        System.Threading.Tasks.Task<bool> WSCreateSiteAliasSQLAsync(Com.SmoovPOS.Model.SiteAliasModel siteAlias);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSUpdateSiteSQL", ReplyAction="http://tempuri.org/IWSSite/WSUpdateSiteSQLResponse")]
        bool WSUpdateSiteSQL(Com.SmoovPOS.Model.SiteModel site, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSUpdateSiteSQL", ReplyAction="http://tempuri.org/IWSSite/WSUpdateSiteSQLResponse")]
        System.Threading.Tasks.Task<bool> WSUpdateSiteSQLAsync(Com.SmoovPOS.Model.SiteModel site, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSUpdateSiteAliasSQL", ReplyAction="http://tempuri.org/IWSSite/WSUpdateSiteAliasSQLResponse")]
        bool WSUpdateSiteAliasSQL(Com.SmoovPOS.Model.SiteAliasModel siteAlias, string siteAliasID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSUpdateSiteAliasSQL", ReplyAction="http://tempuri.org/IWSSite/WSUpdateSiteAliasSQLResponse")]
        System.Threading.Tasks.Task<bool> WSUpdateSiteAliasSQLAsync(Com.SmoovPOS.Model.SiteAliasModel siteAlias, string siteAliasID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSCheckExistUrl", ReplyAction="http://tempuri.org/IWSSite/WSCheckExistUrlResponse")]
        bool WSCheckExistUrl(string url, bool isSubDomain, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSCheckExistUrl", ReplyAction="http://tempuri.org/IWSSite/WSCheckExistUrlResponse")]
        System.Threading.Tasks.Task<bool> WSCheckExistUrlAsync(string url, bool isSubDomain, string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSGetSiteAlias", ReplyAction="http://tempuri.org/IWSSite/WSGetSiteAliasResponse")]
        Com.SmoovPOS.Model.SiteAliasModel WSGetSiteAlias(string bucket);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSGetSiteAlias", ReplyAction="http://tempuri.org/IWSSite/WSGetSiteAliasResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Model.SiteAliasModel> WSGetSiteAliasAsync(string bucket);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSGetSiteAliasByHost", ReplyAction="http://tempuri.org/IWSSite/WSGetSiteAliasByHostResponse")]
        Com.SmoovPOS.Model.SiteAliasModel WSGetSiteAliasByHost(string host);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSGetSiteAliasByHost", ReplyAction="http://tempuri.org/IWSSite/WSGetSiteAliasByHostResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Model.SiteAliasModel> WSGetSiteAliasByHostAsync(string host);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSGetSiteAliasBySiteID", ReplyAction="http://tempuri.org/IWSSite/WSGetSiteAliasBySiteIDResponse")]
        Com.SmoovPOS.Model.SiteAliasModel WSGetSiteAliasBySiteID(string siteID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWSSite/WSGetSiteAliasBySiteID", ReplyAction="http://tempuri.org/IWSSite/WSGetSiteAliasBySiteIDResponse")]
        System.Threading.Tasks.Task<Com.SmoovPOS.Model.SiteAliasModel> WSGetSiteAliasBySiteIDAsync(string siteID);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IWSSiteChannel : SmoovPOS.Utility.WSSiteReference.IWSSite, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class WSSiteClient : System.ServiceModel.ClientBase<SmoovPOS.Utility.WSSiteReference.IWSSite>, SmoovPOS.Utility.WSSiteReference.IWSSite {
        
        public WSSiteClient() {
        }
        
        public WSSiteClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public WSSiteClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSSiteClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public WSSiteClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string WSCreateSite(Com.SmoovPOS.Model.SiteModel site) {
            return base.Channel.WSCreateSite(site);
        }
        
        public System.Threading.Tasks.Task<string> WSCreateSiteAsync(Com.SmoovPOS.Model.SiteModel site) {
            return base.Channel.WSCreateSiteAsync(site);
        }
        
        public string WSCreateSiteAlias(Com.SmoovPOS.Model.SiteAliasModel siteAlias) {
            return base.Channel.WSCreateSiteAlias(siteAlias);
        }
        
        public System.Threading.Tasks.Task<string> WSCreateSiteAliasAsync(Com.SmoovPOS.Model.SiteAliasModel siteAlias) {
            return base.Channel.WSCreateSiteAliasAsync(siteAlias);
        }
        
        public string WSUpdateSite(Com.SmoovPOS.Model.SiteModel site) {
            return base.Channel.WSUpdateSite(site);
        }
        
        public System.Threading.Tasks.Task<string> WSUpdateSiteAsync(Com.SmoovPOS.Model.SiteModel site) {
            return base.Channel.WSUpdateSiteAsync(site);
        }
        
        public string WSDeleteSite(string _id) {
            return base.Channel.WSDeleteSite(_id);
        }
        
        public System.Threading.Tasks.Task<string> WSDeleteSiteAsync(string _id) {
            return base.Channel.WSDeleteSiteAsync(_id);
        }
        
        public Com.SmoovPOS.Model.SiteModel[] WSGetAllSite() {
            return base.Channel.WSGetAllSite();
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Model.SiteModel[]> WSGetAllSiteAsync() {
            return base.Channel.WSGetAllSiteAsync();
        }
        
        public Com.SmoovPOS.Model.SiteAliasModel[] WSGetAllSiteAlias() {
            return base.Channel.WSGetAllSiteAlias();
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Model.SiteAliasModel[]> WSGetAllSiteAliasAsync() {
            return base.Channel.WSGetAllSiteAliasAsync();
        }
        
        public bool WSCreateSiteSQL(Com.SmoovPOS.Model.SiteModel site) {
            return base.Channel.WSCreateSiteSQL(site);
        }
        
        public System.Threading.Tasks.Task<bool> WSCreateSiteSQLAsync(Com.SmoovPOS.Model.SiteModel site) {
            return base.Channel.WSCreateSiteSQLAsync(site);
        }
        
        public bool WSCreateSiteAliasSQL(Com.SmoovPOS.Model.SiteAliasModel siteAlias) {
            return base.Channel.WSCreateSiteAliasSQL(siteAlias);
        }
        
        public System.Threading.Tasks.Task<bool> WSCreateSiteAliasSQLAsync(Com.SmoovPOS.Model.SiteAliasModel siteAlias) {
            return base.Channel.WSCreateSiteAliasSQLAsync(siteAlias);
        }
        
        public bool WSUpdateSiteSQL(Com.SmoovPOS.Model.SiteModel site, string siteID) {
            return base.Channel.WSUpdateSiteSQL(site, siteID);
        }
        
        public System.Threading.Tasks.Task<bool> WSUpdateSiteSQLAsync(Com.SmoovPOS.Model.SiteModel site, string siteID) {
            return base.Channel.WSUpdateSiteSQLAsync(site, siteID);
        }
        
        public bool WSUpdateSiteAliasSQL(Com.SmoovPOS.Model.SiteAliasModel siteAlias, string siteAliasID) {
            return base.Channel.WSUpdateSiteAliasSQL(siteAlias, siteAliasID);
        }
        
        public System.Threading.Tasks.Task<bool> WSUpdateSiteAliasSQLAsync(Com.SmoovPOS.Model.SiteAliasModel siteAlias, string siteAliasID) {
            return base.Channel.WSUpdateSiteAliasSQLAsync(siteAlias, siteAliasID);
        }
        
        public bool WSCheckExistUrl(string url, bool isSubDomain, string siteID) {
            return base.Channel.WSCheckExistUrl(url, isSubDomain, siteID);
        }
        
        public System.Threading.Tasks.Task<bool> WSCheckExistUrlAsync(string url, bool isSubDomain, string siteID) {
            return base.Channel.WSCheckExistUrlAsync(url, isSubDomain, siteID);
        }
        
        public Com.SmoovPOS.Model.SiteAliasModel WSGetSiteAlias(string bucket) {
            return base.Channel.WSGetSiteAlias(bucket);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Model.SiteAliasModel> WSGetSiteAliasAsync(string bucket) {
            return base.Channel.WSGetSiteAliasAsync(bucket);
        }
        
        public Com.SmoovPOS.Model.SiteAliasModel WSGetSiteAliasByHost(string host) {
            return base.Channel.WSGetSiteAliasByHost(host);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Model.SiteAliasModel> WSGetSiteAliasByHostAsync(string host) {
            return base.Channel.WSGetSiteAliasByHostAsync(host);
        }
        
        public Com.SmoovPOS.Model.SiteAliasModel WSGetSiteAliasBySiteID(string siteID) {
            return base.Channel.WSGetSiteAliasBySiteID(siteID);
        }
        
        public System.Threading.Tasks.Task<Com.SmoovPOS.Model.SiteAliasModel> WSGetSiteAliasBySiteIDAsync(string siteID) {
            return base.Channel.WSGetSiteAliasBySiteIDAsync(siteID);
        }
    }
}

﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models
{
    public class ServiceDeliveryViewModel
    {
        public string deliveryid { get; set; }
        public string serviceid { get; set; }
        public bool status { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("serviceName")]
        public string serviceName { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("criteria")]
        public int criteria { get; set; }
        [LocalizedRequired]
        //[RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        public double beginValue { get; set; }
        [LocalizedRequired]
        //[RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        public double endValue { get; set; }
        public bool isUnlimit { get; set; }
        public bool isDefault { get; set; }
        public int unit { get; set; }
        [LocalizedRequired]
        //[DataType(DataType.Currency)]
        //[Range(0, 9999.99)]
        [LocalizedDisplayName("shippingPrice")]
        public double shippingPrice { get; set; }
        [LocalizedRequired]
        //[RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        [LocalizedDisplayName("beginEstimate")]
        public int beginEstimate { get; set; }
        [LocalizedRequired]
        //[RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        public int endEstimate { get; set; }
        public List<AreaShipping> arrayAreaShipping { get; set; }
        public IEnumerable<SelectListItem> criterias { get; set; }
        public DateTime createdDate { get; set; }
        public string currency { get; set; }
    }
}
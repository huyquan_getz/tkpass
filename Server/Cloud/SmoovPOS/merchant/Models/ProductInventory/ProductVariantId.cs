﻿using SmoovPOS.UI.Models.ProductInventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class ProductVariantId
    {
      public  string productID {get; set;}
      public bool status { get; set; }
      public int[] arrayVariant { get; set; }

    }
}
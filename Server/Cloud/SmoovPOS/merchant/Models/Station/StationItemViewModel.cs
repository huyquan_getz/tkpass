﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Station
{
    public class StationItemViewModel
    {
        public StationItemViewModel(Com.SmoovPOS.Entity.Station i, string BranchName, int BranchIndex)
        {
            this.ID = i._id;
            this.BranchIndex = BranchIndex;
            this.BranchName = BranchName;
            this.StationName = i.stationName;
            this.StationSize = i.stationSize;
            this.StationQRCode = i.stationQRCode;
        }

        public int BranchIndex { get; set; }
        public string BranchName { get; set; }
        public string ID { get; set; }
        public string StationName { get; set; }
        public int StationSize { get; set; }
        public string StationQRCode { get; set; }
    }
}
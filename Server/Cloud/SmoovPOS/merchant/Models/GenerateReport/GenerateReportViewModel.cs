﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.GenerateReport
{
    public class GenerateReportViewModel
    {
        //Info to display at View page
        public List<Com.SmoovPOS.Entity.Inventory> ListBranches { set; get; }
    }
}
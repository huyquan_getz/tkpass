﻿using Com.SmoovPOS.Entity.CBEntity;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.SMSSetting
{
    public class SMSSettingModel : CBBaseEntity
    {
        [LocalizedRequired]
        [LocalizedDisplayName("ApiKey")]
        [LocalizedRegularExpression(@"([a-fA-F\d]{32})", ErrorMessage = "Invalid key (32 chars and only number or alphabet)")]
        public string ApiKey { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("SecretCode")]
        [LocalizedRegularExpression(@"([a-fA-F\d]{32})", ErrorMessage = "Invalid key (32 chars and only number or alphabet)")]
        public string SecretCode { get; set; }
    }
}
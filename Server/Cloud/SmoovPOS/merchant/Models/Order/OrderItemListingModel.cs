﻿using Com.SmoovPOS.Model;
using SmoovPOS.Entity.Models.Paging;
using System.Collections.Generic;

namespace SmoovPOS.UI.Models
{
    public class OrderItemListingModel
    {
        public string TableId { get; set; }

        public string TabId { get; set; }

        public IEnumerable<OrderModel> OrderList { get; set; }

        private SearchOrderModel searchOrder;
        public SearchOrderModel SearchOrder
        {
            get
            {
                if (searchOrder == null)
                    searchOrder = new SearchOrderModel();
                return searchOrder;
            }
            set { searchOrder = value; }
        }

        private EntityPaging paging;
        public EntityPaging Paging
        {
            get
            {
                if (paging == null)
                    paging = new EntityPaging();
                return paging;
            }
            set { paging = value; }
        }
    }
}
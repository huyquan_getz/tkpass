﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Sale.ChartSample.SmoovPayTransaction
{
    public class SmoovPayTransactionModel
    {
        public string Maker { get; set; }
        public double Transaction { get; set; }
        public int Order { get; set; }
    }
}
﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Models.Paging;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models
{
    public class CustomerItemListingModel
    {
        public IEnumerable<Com.SmoovPOS.Entity.Customer> CustomerList { get; set; }

        private EntityPaging paging;

        public EntityPaging Paging
        {
            get
            {
                if (paging == null)
                    paging = new EntityPaging();
                return paging;
            }
            set { paging = value; }
        }

        public Country country { get; set; }

        public string currency { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("FirstName")]
        public string firstName { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("LastName")]
        public string lastName { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("Gender")]
        public int gender { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("PhoneNumber")]
        public string phonenumber { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("DoB")]
        public string doB { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("StreetAddress")]
        public string address { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("city")]
        public string city { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("zipCode")]
        public string zipCode { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("region")]
        public string region { get; set; }

        public string remark { get; set; }
        public string group { get; set; }

        [LocalizedDisplayName("acceptsNewsletter")]
        public bool acceptsNewsletter { get; set; }

        [LocalizedRequired]
        [EmailAddress]
        [LocalizedRemote("IsExistedEmail", "Customer", AdditionalFields = "id", ResourceCode = "EmailExist")]
        [LocalizedDisplayName("email")]
        public string email { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("zipCode")]
        public string pinCode { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("country")]
        public string countries { get; set; }

        [LocalizedRequired]
        [LocalizedDisplayName("Birthday")]
        public string Birthday { get; set; }

        public List<SelectListItem> listCountries { get; set; }
        public List<SelectListItem> listGender { get; set; }
        public string id { get; set; }
    }
}
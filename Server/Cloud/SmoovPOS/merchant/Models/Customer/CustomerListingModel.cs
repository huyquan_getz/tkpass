﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class CustomerListingModel
    {
        public IEnumerable<Com.SmoovPOS.Entity.Customer> AllCustomerList { get; set; }

        private CustomerItemListingModel realCustomerListing;

        public CustomerItemListingModel RealCustomerListing
        {
            get
            {
                if (realCustomerListing == null && AllCustomerList != null)
                {
                    realCustomerListing = new CustomerItemListingModel();
                    realCustomerListing.CustomerList = AllCustomerList.Where(r => r.memberID != "" && r.customerType == (int)ConstantSmoovs.Enums.CustomerType.Member);
                    realCustomerListing.country = country;
                    realCustomerListing.currency = currency;
                }

                return realCustomerListing;
            }
            set { realCustomerListing = value; }
        }

        private CustomerItemListingModel guestCustomerListing;

        public CustomerItemListingModel GuestCustomerListing
        {
            get
            {
                if (guestCustomerListing == null && AllCustomerList != null)
                {
                    guestCustomerListing = new CustomerItemListingModel();
                    guestCustomerListing.CustomerList = AllCustomerList.Where(r => r.memberID == "" || r.customerType == (int)ConstantSmoovs.Enums.CustomerType.Guest);
                    guestCustomerListing.country = country;
                    guestCustomerListing.currency = currency;
                }

                return guestCustomerListing;
            }
            set { guestCustomerListing = value; }
        }

        private CustomerItemListingModel newsletterListing;

        public CustomerItemListingModel NewsletterListing
        {
            get
            {
                if (newsletterListing == null && AllCustomerList != null)
                {
                    newsletterListing = new CustomerItemListingModel();
                    newsletterListing.CustomerList = AllCustomerList.Where(r => r.acceptsNewsletter);
                }

                return newsletterListing;
            }
            set { newsletterListing = value; }
        }

        public Country country { get; set; }

        public string currency { get; set; }
    }
}
﻿using Com.SmoovPOS.Entity;
using System.Collections.Generic;

namespace SmoovPOS.UI.Models
{
    public class QueueModel
    {
        public List<Inventory> ListInventory { get; set; }
        public string SelectedBranchId { get; set; }
        public Queue Queue { get; set; }
        public string table
        {
            get { return "Queue"; }
        }
    }
}
﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using SmoovPOS.UI.Models.CommonModel;
using SmoovPOS.Entity.Entity.Settings;

namespace SmoovPOS.UI.Models
{
    public class TableOrderingViewModel
    {

        //private TableOrderingSetting _TableOrderingSetting;

        public string SelectedBranchId { get; set; }

        public List<Inventory> ListInventory { get; set; }

        //public TableOrderingSetting TableOrderingSetting 
        //{
        //    get { return _TableOrderingSetting; }
        //    set 
        //    {
        //        _TableOrderingSetting = value;
        //    }
        //}

        public string table
        {
            get { return "TableOrdering"; }
        }

        //private TableOrderingSetting ValidateSetting(TableOrderingSetting tableOrderingSetting)
        //{
        //    if (tableOrderingSetting != null)
        //    {
        //        if (tableOrderingSetting.ListDayOfWeekSetting != null)
        //        {
        //            foreach (DayOfWeekSetting dows in tableOrderingSetting.ListDayOfWeekSetting)
        //            {
        //                if (dows.Option == 0)
        //                {

        //                    if (string.IsNullOrEmpty(dows.Open_Opening) || string.IsNullOrEmpty(dows.Open_Closing))
        //                        throw new NotSupportedException("Not valid time");
        //                }
        //            }
        //        }
        //    }
            
        //    return tableOrderingSetting;
        //}

        public string BranchId { get; set; }
        public string BranchName { get; set; }
        public int BranchIndex { get; set; }
        public bool AcceptTableOrdering { get; set; }
        public bool AllowAutoAcceptOrder { get; set; }
        public string Instructions { get; set; }
        public bool AllowSound { get; set; }
        public bool AllowPushNotification { get; set; }
        public bool AllowSendEmail { get; set; }
        public bool AllowSendSMS { get; set; }
        [ValidateDayOfWeekSetting("ListDayOfWeekSetting", ErrorMessage = "Wrong input.")]
        public List<DayOfWeekSetting> ListDayOfWeekSetting { get; set; }

        public bool isAddNew { get; set; }
    }
}
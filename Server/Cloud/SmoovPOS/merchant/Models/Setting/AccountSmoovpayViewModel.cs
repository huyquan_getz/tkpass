﻿using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models.Setting
{
    public class SignUpSmoovpayViewModel
    {
        [LocalizedRequired]
        [LocalizedDisplayName("UserNameForLogin")]
        [EmailAddress(ErrorMessage = "The email address format is incorrect.")]
        public string UserName { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("CompanyName")]
        public string CompanyName { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("BusinessDisplayName_2")]
        public string BusinessDisplayName { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("ContactPerson")]
        public string ContactPerson { get; set; }
        [LocalizedRequired]
        [RegularExpression(@"(^[+]([0-9])*$)", ErrorMessage = "Please enter a valid format (Ex: +65).")]
        public string PhoneRegion { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("MobileNumber")]
        [RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        public string PhoneNumber { get; set; }
        [LocalizedDisplayName("country")]
        public string Country { get; set; }
        public List<SelectListItem> Countrys { get; set; }
        [Required]
        [LocalizedDisplayName("AgreeSmoovpay")]
        [MustBeTrue(ErrorMessage = "Please Accept the Terms & Conditions")] 
        public Boolean Agree { get; set; }
        public class MustBeTrueAttribute : ValidationAttribute, IClientValidatable
        {
            public override bool IsValid(object value)
            {
                return value is bool && (bool)value;
            }
            // Implement IClientValidatable for client side Validation 
            public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
            {
                return new ModelClientValidationRule[] {
                    new ModelClientValidationRule { ValidationType = "checkboxtrue", ErrorMessage = this.ErrorMessage } };
            }
        }
    }
    public class SignInSmoovpayViewModel
    {
        [LocalizedRequired]
        [LocalizedDisplayName("UserNameForLogin")]
        [EmailAddress(ErrorMessage = "The email address format is incorrect.")]
        public string UserName { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("Password")]
        [LocalizedStringLength(100, MinimumLength = 6, ErrorMessage = "NewPasswordLength")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
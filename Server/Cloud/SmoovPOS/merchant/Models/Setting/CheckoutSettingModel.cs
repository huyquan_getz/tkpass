﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Setting
{
    public class CheckoutSettingModel : CBBaseEntity
    {
        public int typeCheckout { get; set; }
        /*Define Self-Collection*/
        public bool acceptSelfCollection { get; set; }
        public string instructionsSelfCollection { get; set; }
        public int beginDateSelf { get; set; }
        public int endDateSelf { get; set; }
        public bool timeSelfCollection { get; set; }
        /*Define Delivery*/
        public bool acceptDelivery { get; set; }
        public string instructionsDelivery { get; set; }
        public bool timeDelivery { get; set; }

        //Define Timing
        public string hourCutOff { get; set; }
        public string table
        {
            get { return "CheckoutSetting"; }
        }
        public bool isPickupPoint { get; set; }
    }
}
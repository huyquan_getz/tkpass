﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Tax
{
    public class TaxItemModel
    {
        public int indexStore { get; set; }
        public string storeName { get; set; }
        public ServiceCharge serviceCharge {get; set;}
        public IEnumerable<TaxItem> listTaxItem { get; set; } 
    }
}
﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Tax
{
    public class TaxModel
    {
        public List<TaxItemModel> listTaxItem{ set; get; }
        public Inventory[] inventoryList { get; set; }
        public string id { get; set; }
        public List<string> listStoreId  { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.MemberPoint
{
    public class MemberPointViewModel
    {
        public string Currency { get; set; }

        public string Amount { get; set; }
    }
}
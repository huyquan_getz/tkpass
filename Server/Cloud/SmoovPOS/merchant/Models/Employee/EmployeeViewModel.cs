﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.Employee
{
    public class EmployeeViewModel
    {
        public IEnumerable<Com.SmoovPOS.Entity.Employee> listEmployees { get; set; }
        public bool isAddNew { get; set; }
    }
}
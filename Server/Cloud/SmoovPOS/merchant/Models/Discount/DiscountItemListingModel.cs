﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class DiscountItemListingModel
    {
        public IEnumerable<Com.SmoovPOS.Entity.Discount> DiscountList { get; set; }
        public string tableId;
        
    }
}
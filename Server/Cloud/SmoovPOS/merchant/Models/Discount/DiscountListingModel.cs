﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.TimeZone;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.WSDiscountReference;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmoovPOS.UI.Models
{
    public class DiscountListingModel
    {
        public List<Com.SmoovPOS.Entity.Discount> AllDiscountList { get; set; }

        private DiscountItemListingModel openDiscountListing;
        public DiscountItemListingModel OpenDiscountListing
        {
            get
            {
                if (openDiscountListing == null && AllDiscountList != null)
                {
                    openDiscountListing = new DiscountItemListingModel();
                    openDiscountListing.tableId = "OpenDiscount";
                    openDiscountListing.DiscountList = AllDiscountList.Where(r => r.endDate != null && DateTime.Compare(r.endDate, DateTime.UtcNow) >= 0);
                    openDiscountListing.DiscountList = openDiscountListing.DiscountList.OrderByDescending(r => r.beginDate);
                }
                return openDiscountListing;
            }
            set { openDiscountListing = value; }
        }

        private DiscountItemListingModel expiredDiscountListing;
        public DiscountItemListingModel ExpiredDiscountListing
        {
            get
            {
                if (expiredDiscountListing == null && AllDiscountList != null)
                {
                    expiredDiscountListing = new DiscountItemListingModel();
                    expiredDiscountListing.tableId = "ExpiredDiscount";
                    expiredDiscountListing.DiscountList = AllDiscountList.Where(r => r.endDate != null && DateTime.Compare(r.endDate, DateTime.UtcNow) < 0);
                    expiredDiscountListing.DiscountList = expiredDiscountListing.DiscountList.OrderByDescending(r => r.beginDate);
                }
                return expiredDiscountListing;
            }
            set { expiredDiscountListing = value; }
        }
        public bool isAddNew { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models
{
    public class DiscountInformation
    {
        private Com.SmoovPOS.Entity.Discount _discount = new Com.SmoovPOS.Entity.Discount();
        public Com.SmoovPOS.Entity.Discount discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        public List<Com.SmoovPOS.Entity.Category> listCategories { get; set; }

        public List<Com.SmoovPOS.Entity.ProductItem> listProductItems { get; set; }

        public List<Com.SmoovPOS.Entity.ProductItem> listSelectedProductItems { get; set; }

        public List<Com.SmoovPOS.Entity.ProductItem> listDiscountProductItems { get; set; }

        public bool isExpired { get; set; }
        public string currency { get; set; }
    }
}
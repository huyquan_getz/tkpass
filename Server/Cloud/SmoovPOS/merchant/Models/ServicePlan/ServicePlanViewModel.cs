﻿using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmoovPOS.UI.Models.ServicePlan
{
    public class ServicePlanViewModel
    {
        public string MemberSince { get; set; }
        public string PackageType { get; set; }
        public string PackageStatus { get; set; }
        public string BillingCycle { get; set; }
        public string LastPayment { get; set; }
        public string ExpiredDate { get; set; }
    }
}
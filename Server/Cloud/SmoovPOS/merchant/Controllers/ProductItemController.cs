﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSMerchantSettingReference;
using SmoovPOS.Utility.WSProductItemReference;
using System;
using System.Linq;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class ProductItemController : BaseController
    {
        /// <summary>
        /// Updates the quantity.
        /// </summary>
        /// <param name="productID">The product identifier.</param>
        /// <param name="varientID">The varient identifier.</param>
        /// <param name="businessIDs">The business i ds.</param>
        /// <param name="changeNumbers">The change numbers.</param>
        /// <param name="statuses">The statuses.</param>
        /// <returns></returns>
        public ActionResult UpdateQuantity(string productID, string varientID, string[] businessIDs, int[] changeNumbers, bool[] statuses)
        {
            bool isValid = ValidateProductItemCount(productID, varientID, changeNumbers);

            if (isValid)
            {
                WSProductItemClient wsProductItemClient = new WSProductItemClient();

                var productItemList = wsProductItemClient.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, productID, VariableConfigController.GetBucket());
                ProductItem masterProductItem = productItemList.FirstOrDefault(r => r.store == ConstantSmoovs.Stores.Master);

                //Update for current varients
                for (int i = 0; i < businessIDs.Length; i++)
                {
                    foreach (var productItem in productItemList)
                    {
                        if (productItem.store == businessIDs[i])
                        {
                            if (productItem.arrayVarient.Count() == 0)
                            {
                                VarientItem varient = masterProductItem.arrayVarient.FirstOrDefault(r => r._id == varientID);
                                varient.quantity = 0;
                                productItem.arrayVarient.Add(varient);
                            }

                            foreach (var varient in productItem.arrayVarient.Where(r => r._id == varientID))
                            {
                                varient.quantity += changeNumbers[i];
                                varient.status = statuses[i];
                            }
                            productItem.status = productItem.arrayVarient.Select(r => r.status).Any(r => r); // duyet qua tat ca cac status. bat ky cai nao true thi bat Product.status = true

                            wsProductItemClient.WSUpdateProductItem(productItem, VariableConfigController.GetBucket());

                            break;
                        }
                    }
                }

                //Update for Master store
                foreach (var item in masterProductItem.arrayVarient.Where(r => r._id == varientID))
                {
                    item.quantity -= changeNumbers.Sum();
                }

                wsProductItemClient.WSUpdateProductItem(masterProductItem, VariableConfigController.GetBucket());

                wsProductItemClient.Close();
                WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
                MerchantSetting setting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, VariableConfigController.GetBucket());
                setting.distribute = true;
                int checkStatus = clientMerchantSetting.UpdateMerchantSetting(setting, VariableConfigController.GetBucket());
                clientMerchantSetting.Close();
            }

            return Json(new
            {
                IsSuccess = isValid,
                Message = isValid ? ConstantSmoovs.Distribution.SaveSucess : ConstantSmoovs.Distribution.QuantityGreater
            });
        }

        /// <summary>
        /// Validates the product item count.
        /// </summary>
        /// <param name="productID">The product identifier.</param>
        /// <param name="varientID">The varient identifier.</param>
        /// <param name="changeNumbers">The change numbers.</param>
        /// <returns></returns>
        public bool ValidateProductItemCount(string productID, string varientID, int[] changeNumbers)
        {
            WSProductItemClient wsProductItemClient = new WSProductItemClient();
            int countMasterVarient = wsProductItemClient.WSCountVarients(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, ConstantSmoovs.Stores.Master, productID, varientID, VariableConfigController.GetBucket());

            wsProductItemClient.Close();

            //Check NA = -1 first
            return countMasterVarient == -1 ? true : changeNumbers.Sum() <= countMasterVarient;
        }

        /// <summary>
        /// Distributes the specified product identifier.
        /// </summary>
        /// <param name="productId">The product identifier.</param>
        /// <returns></returns>
        public ActionResult Distribute(string productId)
        {
            ViewBag.Class = "product";

            WSProductItemClient clientProductItem = new WSProductItemClient();
            var listProductItem = clientProductItem.WSGetAllProductItemByProductID(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, productId, VariableConfigController.GetBucket());
            clientProductItem.Close();

            WSInventoryClient clientIventory = new WSInventoryClient();
            Country[] countrys = clientIventory.WSGetAllCountry("country", "get_all_country", VariableConfigController.GetBucket());
            clientIventory.Close();

            //Get and set merchant's currency
            ViewBag.currency = GetGeneralSetting().Currency;
            //ViewBag.NumberInit = CheckInitiation();

            return View(listProductItem);
        }
    }
}
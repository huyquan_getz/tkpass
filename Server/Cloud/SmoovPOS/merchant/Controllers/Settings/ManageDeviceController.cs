﻿using AutoMapper;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.DeviceToken;
using SmoovPOS.Entity.Models;
using SmoovPOS.UI.Models.DeviceToken;
using SmoovPOS.Utility.WSCheckoutSettingReference;
using SmoovPOS.Utility.WSInitCouchBaseReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers.Settings
{
    public class ManageDeviceController : BaseController
    {
        string SiteID = "";

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.SiteID = VariableConfigController.GetBucket();
        }
        // GET: ManageDevice
        public ActionResult Index()
        {
            ViewBag.Class = "DeviceSetting";
            DeviceViewModel model = new DeviceViewModel();
            model.Device = new DetailDeviceViewModel();
            WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
            model.manageDevice = clientSetting.DetailDeviceToken(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetListDevice, SiteID); 
            clientSetting.Close();
            return View(model);
        }
        public ActionResult Add()
        {
            DetailDeviceViewModel model = new DetailDeviceViewModel();
            return PartialView("Edit", model);
        }
        public ActionResult Edit(string uuid)
        {
            WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
            var manageDevice = clientSetting.DetailDeviceToken(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetListDevice, SiteID); 
            clientSetting.Close();
            var device = manageDevice.ArrayDevices.Where(m => m.uuid == uuid).FirstOrDefault();
            DetailDeviceViewModel model = new DetailDeviceViewModel();
            var viewmodel = Mapper.Map<DeviceTokenModel, DetailDeviceViewModel>(device);
            return PartialView("Edit", viewmodel);
        }
        [HttpPost]
        public ActionResult Edit(DetailDeviceViewModel model)
        {
            var viewmodel = Mapper.Map<DetailDeviceViewModel, DeviceTokenModel>(model);
            WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
            clientSetting.AddDeviceToken(SiteID, ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetListDevice, viewmodel);
            clientSetting.Close();
            return RedirectToAction("Listing");
        }
        public ActionResult Delete(string id)
        {
            WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
            clientSetting.DeleteDevice(id,SiteID);
            clientSetting.Close();
            return RedirectToAction("Listing");
        }
        public ActionResult Listing()
        {   
            WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
            var listDevices = clientSetting.DetailDeviceToken(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetListDevice, SiteID);
            clientSetting.Close();
            return PartialView("Listing", listDevices);
        }
        public JsonResult IsExistedToken(string UUID)
        {
            var isExists = true;
            if (string.IsNullOrEmpty(UUID))
            {
                return new JsonResult
                {
                    Data = true,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
            isExists = clientSetting.ExistToken(UUID, SiteID);
            clientSetting.Close();
            return new JsonResult
            {
                Data = !isExists,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public static int Edit(DeviceTokenModel model, string siteID)
        {
            WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
            var status  = clientSetting.AddDeviceToken(siteID, ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetListDevice, model);
            clientSetting.Close();
            return status;
        }

        public ActionResult DeleteMerchant(string bucket)
        {
            WSInitCouchBaseClient client = new WSInitCouchBaseClient();
            var status = client.DeleteBucketCouchbase("deleleMerchant", "get_all_by_bucket", bucket);
            client.Close();
            return new JsonResult
            {
                Data = status,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}        
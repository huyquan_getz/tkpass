﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Utility.WSDeliveryCountryReference;
using SmoovPOS.Utility.WSDeliverySettingReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmoovPOS.UI.Models;
using AutoMapper;
using SmoovPOS.Utility.CustomHTMLHelper;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using System.Threading;

namespace SmoovPOS.UI.Controllers
{
    public class DeliveryController : BaseController
    {
        // GET: Delivery
        public ActionResult Index()
        {
            ViewBag.Class = "delivery";

            DeliverySetting[] results;
            List<SelectListItem> listItems;
            LoadCountry(out results, out listItems);

            ViewBag.Countrys = listItems;

            var currency = ViewBag.currency = GetGeneralSetting().Currency;

            results = results.OrderByDescending(s => s.createdDate).ToArray();

            return View(results);
        }

        private static void LoadCountry(out DeliverySetting[] results, out List<SelectListItem> listItems)
        {
            WSDeliverySettingClient client = new WSDeliverySettingClient();

            results = client.WSGetAllDeliverySetting(ConstantSmoovs.CouchBase.DesignDocDeliverySetting, ConstantSmoovs.CouchBase.DeliverySettingViewAll, VariableConfigController.GetBucket());
            client.Close();

            var listCountryIds = new List<string>();
            if (results != null && results.Count() > 0)
            {
                listCountryIds = results.Select(c => c.country._id).ToList();
            }

            WSDeliveryCountryClient clientCountry = new WSDeliveryCountryClient();

            var resultCountrys = clientCountry.WSGetAllDeliveryCountry(ConstantSmoovs.CouchBase.DesignDocDeliveryCountry, ConstantSmoovs.CouchBase.DeliveryCountryViewAll, VariableConfigController.GetBucket());
            clientCountry.Close();

            listItems = new List<SelectListItem>() { };
            foreach (var country in resultCountrys.OrderBy(c => c.country))
            {
                if (listCountryIds.Count <= 0 || !listCountryIds.Contains(country._id))
                {
                    listItems.Add(new SelectListItem()
                    {
                        Value = country._id,
                        Text = country.country,
                        Selected = (country.country == ConstantSmoovs.Countrys.Singapore)
                    });
                }
            }
            if (listItems != null && listItems.Count > 0 && !listItems.Where(x => x.Selected).Any())
            {
                listItems[0].Selected = true;
            }

        }
        public ActionResult DeliveryListing()
        {
            //Thread.Sleep(3000);
            ViewBag.Class = "delivery";

            WSDeliverySettingClient client = new WSDeliverySettingClient();

            var results = client.WSGetAllDeliverySetting(ConstantSmoovs.CouchBase.DesignDocDeliverySetting, ConstantSmoovs.CouchBase.DeliverySettingViewAll, VariableConfigController.GetBucket());

            var currency = ViewBag.currency = GetGeneralSetting().Currency;

            return PartialView(results);
        }
        [HttpPost]
        public ActionResult CreateDelivery(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                WSDeliveryCountryClient clientCountry = new WSDeliveryCountryClient();
                var resultCountry = clientCountry.WSGetDeliveryCountry(id, VariableConfigController.GetBucket());
                clientCountry.Close();
                if (resultCountry != null)
                {
                    DeliverySetting setting = new DeliverySetting() { _id = Guid.NewGuid().ToString() };

                    setting.id = setting._id;
                    setting.country = resultCountry;
                    setting.country.arrayArea = new List<Area>() { };

                    setting.flag = true.ToString();
                    setting.createdDate = DateTime.UtcNow;
                    setting.modifiedDate = DateTime.UtcNow;
                    setting.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                    setting.userOwner = User.Identity.GetUserId();
                    setting.userID = setting.userOwner;

                    setting.arrayServiceDelivery = new System.Collections.Generic.List<ServiceDelivery>();

                    WSDeliverySettingClient clientSetting = new WSDeliverySettingClient();
                    var statusCode = clientSetting.WSCreateDeliverySetting(setting, VariableConfigController.GetBucket());

                    DeliverySetting[] results;
                    List<SelectListItem> listItems;
                    LoadCountry(out results, out listItems);

                    return Json(new { success = statusCode, listItems = listItems }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult RemoveZone(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                WSDeliverySettingClient client = new WSDeliverySettingClient();
                var result = client.WSGetDeliverySetting(id, VariableConfigController.GetBucket());
                if (result != null)
                {
                    var statusCode = client.WSDeleteDeliverySetting(id, VariableConfigController.GetBucket());

                    DeliverySetting[] results;
                    List<SelectListItem> listItems;
                    LoadCountry(out results, out listItems);

                    return Json(new { success = statusCode, listItems = listItems }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateService(string id, string countryId)
        {
            //Thread.Sleep(3000);
            ViewBag.Class = "delivery";
            WSDeliveryCountryClient clientCountry = new WSDeliveryCountryClient();
            var resultCountry = clientCountry.WSGetDeliveryCountry(countryId, VariableConfigController.GetBucket());
            clientCountry.Close();
            if (resultCountry != null)
            {
                ServiceDelivery service = new ServiceDelivery();
                service._id = string.Empty;
                service.status = true;
                service.serviceName = string.Empty;
                service.criteria = 0;
                service.beginValue = 0;
                service.endValue = 0;
                service.isUnlimit = false;
                service.unit = 0;
                service.shippingPrice = 0;
                service.beginEstimate = 1;
                service.endEstimate = 2;
                service.isDefault = false;

                service.arrayAreaShipping = new System.Collections.Generic.List<AreaShipping>();
                AddAreaShipping(resultCountry, service);

                service.arrayAreaShipping = service.arrayAreaShipping.OrderBy(a => a.areaName).ToList();

                var model = Mapper.Map<ServiceDelivery, ServiceDeliveryViewModel>(service);

                WSDeliverySettingClient clientSetting = new WSDeliverySettingClient();
                var deliverySetting = clientSetting.WSGetDeliverySetting(id, VariableConfigController.GetBucket());
                if (deliverySetting != null)
                {
                    if (deliverySetting.arrayServiceDelivery == null ||
                        deliverySetting.arrayServiceDelivery.Count <= 0 ||
                        deliverySetting.arrayServiceDelivery.Where(d => d.isDefault).Count() <= 0)
                    {
                        model.isDefault = true;
                    }
                }

                model.deliveryid = id;
                
                model.criterias = new List<SelectListItem>() { 
                    new SelectListItem(){Text=ConstantSmoovs.DeliverySetting.Basedonorderweight,  Value=ConstantSmoovs.Enums.Criterias.FirstOrDefault(r=>r.Value == ConstantSmoovs.DeliverySetting.Basedonorderweight).Key.ToString()},
                    new SelectListItem(){Text=ConstantSmoovs.DeliverySetting.Basedonorderprice,  Value=ConstantSmoovs.Enums.Criterias.FirstOrDefault(r=>r.Value == ConstantSmoovs.DeliverySetting.Basedonorderprice).Key.ToString()}
                };

                var currency = model.currency = GetGeneralSetting().Currency;

                return PartialView("ServiceDelivery", model);
            }
            return PartialView("ServiceDelivery");
        }
        public ActionResult EditService(string id, string serviceId)
        {
            //Thread.Sleep(3000);
            ViewBag.Class = "delivery";
            var model = LoadServiceDelivery(id, serviceId);

            return PartialView("ServiceDelivery", model);
        }

        public ActionResult ServiceDetail(string id, string serviceId)
        {
            //Thread.Sleep(3000);
            ViewBag.Class = "delivery";
            var model = LoadServiceDelivery(id, serviceId);
            return PartialView(model);
        }
        private ServiceDeliveryViewModel LoadServiceDelivery(string id, string serviceId)
        {
            WSDeliverySettingClient client = new WSDeliverySettingClient();
            var result = client.WSGetDeliverySetting(id, VariableConfigController.GetBucket());
            client.Close();
            var model = new ServiceDeliveryViewModel();
            if (result != null)
            {
                var service = result.arrayServiceDelivery.Where(s => s._id == serviceId).FirstOrDefault();
                if (service != null)
                {
                    model = Mapper.Map<ServiceDelivery, ServiceDeliveryViewModel>(service);
                    model.deliveryid = id;
                    model.serviceid = serviceId;
                    model.criterias = new List<SelectListItem>() { 
                        new SelectListItem(){Text=ConstantSmoovs.DeliverySetting.Basedonorderweight,  Value=ConstantSmoovs.Enums.Criterias.FirstOrDefault(r=>r.Value == ConstantSmoovs.DeliverySetting.Basedonorderweight).Key.ToString()},
                        new SelectListItem(){Text=ConstantSmoovs.DeliverySetting.Basedonorderprice,  Value=ConstantSmoovs.Enums.Criterias.FirstOrDefault(r=>r.Value == ConstantSmoovs.DeliverySetting.Basedonorderprice).Key.ToString()}
                    };
                    var currency = model.currency = GetGeneralSetting().Currency;
                }
            }

            model.arrayAreaShipping = model.arrayAreaShipping.OrderBy(a => a.areaName).ToList();
            return model;
        }
        private void AddAreaShipping(DeliveryCountry resultCountry, ServiceDelivery serviceDelivery)
        {
            for (int i = 0; i < resultCountry.arrayArea.Count; i++)
            {
                var area = resultCountry.arrayArea[i];

                var areaShipping = new AreaShipping();
                areaShipping.areaName = area.areaName;
                areaShipping.zipCode = area.zipCode;
                areaShipping.isSelect = false;
                areaShipping.areaPrice = 0;

                serviceDelivery.arrayAreaShipping.Add(areaShipping);
            }
        }
        [HttpPost]
        public ActionResult SaveInfo(ServiceDeliveryViewModel viewmodel, FormCollection fc)
        {
            //Thread.Sleep(3000);
            StringHelper.Trim(viewmodel);
            var isSuccess = false;
            string id = viewmodel.deliveryid;
            string serviceid = viewmodel.serviceid;
            var shippingPrice = fc["shippingPrice"];
            if (viewmodel.shippingPrice == 0)
            {
                viewmodel.shippingPrice = double.Parse(shippingPrice);
            }
            if (!string.IsNullOrEmpty(viewmodel.deliveryid))
            {
                WSDeliverySettingClient client = new WSDeliverySettingClient();
                var result = client.WSGetDeliverySetting(viewmodel.deliveryid, VariableConfigController.GetBucket());

                var model = Mapper.Map<ServiceDeliveryViewModel, ServiceDelivery>(viewmodel);
                if (string.IsNullOrEmpty(viewmodel.serviceid))
                {
                    //create ServiceDelivery
                    model._id = Guid.NewGuid().ToString();
                    serviceid = model._id;
                    model.createdDate = DateTime.UtcNow;

                    foreach (var item in model.arrayAreaShipping)
                    {
                        item.isSelect = true;
                        item.areaPrice = model.shippingPrice;
                    }

                    result.arrayServiceDelivery.Add(model);
                }
                else
                {
                    model._id = serviceid;
                    //update ServiceDelivery
                    for (int i = 0; i < result.arrayServiceDelivery.Count; i++)
                    {
                        if (result.arrayServiceDelivery[i]._id == viewmodel.serviceid)
                        {
                            result.arrayServiceDelivery[i] = model;
                            break;
                        }
                    }
                }

                if (model.isDefault)
                {
                    for (int i = 0; i < result.arrayServiceDelivery.Count; i++)
                    {
                        if (result.arrayServiceDelivery[i]._id != serviceid)
                        {
                            result.arrayServiceDelivery[i].isDefault = false;
                        }
                    }
                }

                result.userID = User.Identity.GetUserId();
                result.modifiedDate = DateTime.UtcNow;

                isSuccess = client.WSUpdateDeliverySetting(result, VariableConfigController.GetBucket());
                client.Close();
            }

            return Json(new { success = isSuccess, id = id, serviceid = serviceid }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult RemoveServiceDelivery(string id, string serviceId)
        {
            ViewBag.Class = "delivery";
            WSDeliverySettingClient client = new WSDeliverySettingClient();
            var result = client.WSGetDeliverySetting(id, VariableConfigController.GetBucket());

            var isSuccess = false;
            if (result != null)
            {
                var serviceDeliverys = result.arrayServiceDelivery.Where(s => s._id != serviceId).ToList();
                result.arrayServiceDelivery = serviceDeliverys;

                isSuccess = client.WSUpdateDeliverySetting(result, VariableConfigController.GetBucket());
                client.Close();
            }

            return Json(new { success = isSuccess }, JsonRequestBehavior.AllowGet);
        }
    }
}
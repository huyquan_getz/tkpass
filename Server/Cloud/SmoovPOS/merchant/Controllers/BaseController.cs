﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.ApiSmoovPay;
using SmoovPOS.Utility.Cache;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSMerchantSettingReference;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class BaseController : Controller
    {
        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public string RenderRazorViewToStringNoModel(string viewName)
        {

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public Com.SmoovPOS.Entity.MerchantGeneralSetting GetGeneralSetting()
        {
            //Get saved general settings from couchbase
            WSMerchantGeneralSettingClient merchantGeneralSettingClient = new WSMerchantGeneralSettingClient();
            var generalSetting = merchantGeneralSettingClient.GetDetailMerchantGeneralSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewGeneralSetting, VariableConfigController.GetBucket());
            merchantGeneralSettingClient.Close();
            return generalSetting;
        }
        public int CheckInitiation()
        {
            WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
            // MerchantSetting setting = new MerchantSetting();
            var setting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, VariableConfigController.GetBucket());
            clientMerchantSetting.Close();
            int count = 0;
            if (setting != null)
            {
                if (setting.updateProfile) count++;
                if (setting.createBranch) count++;
                if (setting.createCategory) count++;
                if (setting.addProduct) count++;
                if (setting.distribute) count++;

            }
            return count;
        }

    }

}
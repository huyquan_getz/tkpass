﻿using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Models.ServicePlan;
using SmoovPOS.Utility.WSMerchantManagementReference;
using SmoovPOS.Utility.WSServicePacketReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class ServicePlanController : BaseController
    {
        // GET: ServicePlan
        public ActionResult Index()
        {
            ViewBag.Class = "ServicePlan";
            //ViewBag.NumberInit = CheckInitiation();

            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var MerchantManagementDoc = client.WSGetMerchantManagementByUserId(VariableConfigController.GetUserID());
            client.Close();

            var userSession = new UserSession();

            ServicePlanViewModel model = new ServicePlanViewModel();
            if (MerchantManagementDoc != null)
            {
                model.MemberSince = MerchantManagementDoc.createdDate.ToString(SmoovPOS.Common.ConstantSmoovs.DefaultFormats.ShortDate);
                model.PackageType = userSession.ServicePacket.servicePacketName;
                model.PackageStatus = MerchantManagementDoc.status == true ? "Active" : "Inactive";
                model.ExpiredDate = MerchantManagementDoc.servicePacketEndDate.Value.ToString(SmoovPOS.Common.ConstantSmoovs.DefaultFormats.ShortDate);

                IWSServicePacketClient svclient = new IWSServicePacketClient();
                var servicepackage = svclient.WSGetServicePacket(userSession.ServicePacket.servicePacketID);
                svclient.Close();
                var billingServicePackets = servicepackage.BillingServicePackets.Where(b => b.billingCycleID == MerchantManagementDoc.billingCycleId).FirstOrDefault();
                if (billingServicePackets != null)
                {
                    model.BillingCycle = billingServicePackets.paymentCycleName + " - " + billingServicePackets.moneyType + " " + string.Format("{0:0.00}", billingServicePackets.moneyPerUnit);
                }
                //model.LastPayment = "";
            }

            return View(model);
        }
    }
}
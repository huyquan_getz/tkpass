﻿using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.ApiSmoovPay;
using SmoovPOS.Entity.BaseEntity;
using SmoovPOS.Entity.Settings;
using SmoovPOS.UI.Models.Setting;
using SmoovPOS.Utility.Cache;
using SmoovPOS.Utility.WSCheckoutSettingReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSMerchantManagementReference;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class SettingCheckoutController : BaseController
    {
        private string siteID = VariableConfigController.GetBucket();
        private string apiAccessKey = CommonActionSmoovPay.GetOneTimeToken(System.Configuration.ConfigurationManager.AppSettings[SmoovPOS.Common.ConstantSmoovs.AppSettings.ApiAccessKey]);
        [Authorize]
        // GET: SettingCheckout
        public ActionResult Index(string Message = "", string DivActive = "", string Email = "", string Password = "")
        {
            ViewBag.Class = "checkout";

            WSCheckoutSettingClient client = new WSCheckoutSettingClient();
            //ViewBag.NumberInit = CheckInitiation();
            ViewBag.Message = Message;
            ViewBag.DivActive = DivActive;
            ViewBag.Email = Email;
            ViewBag.Password = Password;
            ViewBag.currency = GetGeneralSetting().Currency;

            CheckoutSetting origin = client.GetDetailCheckoutSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAllCheckoutSetting, siteID);
            if (origin == null)
            {
                CheckoutSetting checkout = new CheckoutSetting();
                checkout._id = Guid.NewGuid().ToString();
                checkout.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                checkout.createdDate = DateTime.Today;
                checkout.acceptSelfCollection = true;
                checkout.display = true;
                checkout.instructionsSelfCollection = "";
                checkout.instructionsSelfCollection = "";
                checkout.isPickupPoint = false;
                checkout.status = true;
                checkout.timeDelivery = false;
                checkout.timeSelfCollection = false;
                checkout.typeCheckout = 1;
                checkout.acceptDelivery = false;
                checkout.accountSmoovPay = client.GetDetailAccountSmoovpay(Common.ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAccountSmoovPay, siteID);
                client.CreateCheckoutSetting(checkout, siteID);
                client.Close();
                return View(checkout);
            }
            else
            {
                origin.accountSmoovPay = client.GetDetailAccountSmoovpay(Common.ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAccountSmoovPay, siteID);
                client.Close();
                //  var model = Mapper.Map<CheckoutSetting, CheckoutSettingModel>(origin);
                return View(origin);
            }

        }
        [Authorize]
        [HttpPost]
        public ActionResult Edit(FormCollection fc)
        {
            ViewBag.Class = "checkout";
            WSCheckoutSettingClient client = new WSCheckoutSettingClient();
            CheckoutSetting checkoutSetting = new CheckoutSetting();
            bool acceipt_selfCollection = false;
            if (!string.IsNullOrWhiteSpace(fc["acceipt-selfCollection"]))
            {
                acceipt_selfCollection = Boolean.Parse(fc["acceipt-selfCollection"]);
            }
            checkoutSetting._id = fc["_id"];
            checkoutSetting.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            checkoutSetting.createdDate = DateTime.Today;
            checkoutSetting.acceptSelfCollection = acceipt_selfCollection;
            checkoutSetting.instructionsSelfCollection = !string.IsNullOrWhiteSpace(fc["instructionSelfcollect"]) ? fc["instructionSelfcollect"] : "";
            checkoutSetting.beginDateSelf = !string.IsNullOrWhiteSpace(fc["datefrom"]) ? fc["datefrom"] : "";
            checkoutSetting.endDateSelf = !string.IsNullOrWhiteSpace(fc["dateto"]) ? fc["dateto"] : "";
            checkoutSetting.timeSelfCollection = !string.IsNullOrWhiteSpace(fc["timeSelfCollection"]) ? Boolean.Parse(fc["timeSelfCollection"]) : false;
            bool acceipt_delivery = false;
            if (!string.IsNullOrWhiteSpace(fc["acceipt-delivery"]))
            {
                acceipt_delivery = Boolean.Parse(fc["acceipt-delivery"]);
            }
            checkoutSetting.acceptDelivery = acceipt_delivery;
            checkoutSetting.instructionsDelivery = !string.IsNullOrWhiteSpace(fc["instructionShipping"]) ? fc["instructionShipping"] : "";
            checkoutSetting.timeDelivery = !string.IsNullOrWhiteSpace(fc["timeDelivery"]) ? Boolean.Parse(fc["timeDelivery"]) : false;

            checkoutSetting.hourCutOff = !string.IsNullOrWhiteSpace(fc["cutOff"]) ? fc["cutOff"] : "";

            //var model = Mapper.Map<CheckoutSettingModel, CheckoutSetting>(checkoutSetting);
            int code = client.UpdateCheckoutSetting(checkoutSetting, siteID);
            client.Close();
            return RedirectToAction("Index");
        }
        [Authorize]
        public ActionResult Signup()
        {   
            SignUpSmoovpayViewModel model = new SignUpSmoovpayViewModel();
            ViewBag.Class = "checkout";
            InitData(model);
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Signup(SignUpSmoovpayViewModel model)
        {
            ViewBag.Class = "checkout";
            if (ModelState.IsValid)
            {
                var url = string.Format(ConstantSmoovs.URLSmoovPayServer.SignupMerchant, System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.UrlApiSmoovPay]); ;

                using (var client = new WebClient())
                {
                    var values = new NameValueCollection
                {
                    { "AccessToken", apiAccessKey },
                    { "EmailAddress", model.UserName },
                    { "CompanyName", model.CompanyName },
                    { "BusinessDisplayName", model.BusinessDisplayName },
                    { "ContactPerson", model.ContactPerson },
                    { "ContactNumber", model.PhoneNumber },
                    { "CountryCode", model.PhoneRegion },
                    { "Country", model.Country }
                };
                    var responsebytes = client.UploadValues(url, "POST", values);
                    var responsebody = Encoding.UTF8.GetString(responsebytes);
                    var JsonResponse = JsonConvert.DeserializeObject<ApiSmoovPay>(responsebody);
                    if (JsonResponse.status == ConstantSmoovs.ResponseStatusApiSmoovPay.Succcess)
                    {
                        return RedirectToAction("Index", new { Message = DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("SignupSuccessfully") });
                    }
                    else
                    {
                        InitData(model);
                        ModelState.AddModelError("keyName", JsonResponse.message);  
                        return View(model);
                    }
                }
            }
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult SignIn(SignInSmoovpayViewModel model)
        {
            if (ModelState.IsValid)
            {
                var url = string.Format(ConstantSmoovs.URLSmoovPayServer.ExistingMerchant, System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.UrlApiSmoovPay]); ;
                var username = Convert.ToString(model.UserName);
                var password = Convert.ToString(model.Password);

                using (var client = new WebClient())
                {
                    var values = new NameValueCollection
                {
                    { "AccessToken", apiAccessKey },
                    { "EmailAddress", username },
                    { "Password", password }
                };
                    var responsebytes = client.UploadValues(url, "POST", values);
                    var responsebody = Encoding.UTF8.GetString(responsebytes);
                    var JsonResponse = JsonConvert.DeserializeObject<ApiSmoovPay>(responsebody);
                    if (JsonResponse.status == ConstantSmoovs.ResponseStatusApiSmoovPay.Succcess)
                    {
                        var accountSmoovPay = JObject.Parse(JsonResponse.result.ToString()).ToObject<AccountSmoovpay>();
                        accountSmoovPay.createdDate = DateTime.UtcNow;
                        accountSmoovPay.modifiedDate = DateTime.UtcNow;
                        accountSmoovPay._id = Guid.NewGuid().ToString();
                        accountSmoovPay.status = true;
                        accountSmoovPay.display = true;
                        accountSmoovPay.password = model.Password;
                        accountSmoovPay.bucket = VariableConfigController.GetBucket();

                        WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
                        clientSetting.CreateAccountSmoovpay(siteID, accountSmoovPay);
                        clientSetting.Close();

                        // ModelState.AddModelError("keyName", DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("WellDone"));// Succcess
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        // ModelState.AddModelError("keyName", DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("AccountNotFound")); //Fail.Has Error                     
                        return RedirectToAction("Index", new { Message = JsonResponse.message, DivActive = "pay_signin", Email = model.UserName, Password = model.Password });
                    }
                }
            }

            return View(model);

        }
        [Authorize]
        [HttpPost]
        public ActionResult UpdateAccountSmoovPay(SignInSmoovpayViewModel model)
        {
            if (ModelState.IsValid)
            {
                var url = string.Format(ConstantSmoovs.URLSmoovPayServer.ExistingMerchant, System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.UrlApiSmoovPay]); ;

                var username = Convert.ToString(model.UserName);
                var password = Convert.ToString(model.Password);

                using (var client = new WebClient())
                {
                    var values = new NameValueCollection
                {
                    { "AccessToken", apiAccessKey },
                    { "EmailAddress", username },
                    { "Password", password }
                };
                    var responsebytes = client.UploadValues(url, "POST", values);
                    var responsebody = Encoding.UTF8.GetString(responsebytes);
                    var JsonResponse = JsonConvert.DeserializeObject<ApiSmoovPay>(responsebody);
                    if (JsonResponse.status == ConstantSmoovs.ResponseStatusApiSmoovPay.Succcess)
                    {
                        AccountSmoovpay accountSmoovPay = new AccountSmoovpay();
                        WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
                        accountSmoovPay = clientSetting.GetDetailAccountSmoovpay(Common.ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAccountSmoovPay, siteID);
                      
                        var accountUpdateSmoovPay = JObject.Parse(JsonResponse.result.ToString()).ToObject<AccountSmoovpay>();
                        accountUpdateSmoovPay.modifiedDate = DateTime.UtcNow;
                        accountUpdateSmoovPay._id = accountSmoovPay._id;
                        accountUpdateSmoovPay.status = accountSmoovPay.status;
                        accountUpdateSmoovPay.display = accountSmoovPay.display;
                        accountUpdateSmoovPay.password = model.Password;   
                        clientSetting.UpdateAccountSmoovpay(siteID, accountUpdateSmoovPay);
                        clientSetting.Close();                       
                        // ModelState.AddModelError("keyName", DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("WellDone"));// Succcess
                        return RedirectToAction("Index", new { Message = DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("UpdateSuccessful") });
                    }
                    else
                    {
                        // ModelState.AddModelError("keyName", DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("AccountNotFound")); //Fail.Has Error                     
                        return RedirectToAction("Index", new { Message = DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("AccountNotFound"), DivActive = "pay_signin", Email = model.UserName, Password = model.Password });
                    }
                }
            }

            return View(model);

        }
        private void InitData(SignUpSmoovpayViewModel model)
        {
            IWSMerchantManagementClient clientMerchant = new IWSMerchantManagementClient();
            var resultCountry = clientMerchant.WSGetAllCountry();
            clientMerchant.Close();
            model.Countrys = (from c in resultCountry
                              select new SelectListItem
                              {
                                  Value = c,
                                  Text = c,
                                  Selected = (c == model.Country)
                              }).ToList();
        }
        [Authorize]
        public ActionResult RemoveAccount()
        {
            WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
            AccountSmoovpay accountSmoovPay = new AccountSmoovpay();
            accountSmoovPay = clientSetting.GetDetailAccountSmoovpay(Common.ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAccountSmoovPay, siteID);
            clientSetting.WSDeleteAccountSmoovPay(accountSmoovPay._id, siteID);
            clientSetting.Close();
            return RedirectToAction("Index", new { Message = DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("DeleteSuccess") });
        }

        public ActionResult SuccessUrl()
        {
            ViewBag.Class = "checkout";
            return View();
        }
        public ActionResult CancelUrl()
        {
            ViewBag.Class = "checkout";
            return View();
        }

        #region Test Smoovpay
        public string TestParseObjectSmoopay(string model)
        {
        //    //var JsonResponse = JsonConvert.DeserializeObject<ApiSmoovPay>(model);         
        //    //if (JsonResponse.status == ConstantSmoovs.ResponseStatusApiSmoovPay.Succcess)
        //    //{
        //    //    var accountSmoovPay = JObject.Parse(JsonResponse.result.ToString()).ToObject<AccountSmoovpay>();
        //    //    accountSmoovPay.createdDate = DateTime.UtcNow;
        //    //    accountSmoovPay.modifiedDate = DateTime.UtcNow;
        //    //    accountSmoovPay._id = Guid.NewGuid().ToString();
        //    //    accountSmoovPay.status = true;
        //    //    accountSmoovPay.display = true;
        //    //}
        //    //return 1;

        //    var JsonResponse = JsonConvert.DeserializeObject<ApiSmoovPay>(model);
        //    if (JsonResponse.status == ConstantSmoovs.ResponseStatusApiSmoovPay.Succcess)
        //    {
        //        var jObject = JObject.Parse(model);
        //        return jObject["result"].ToString();
        //    }
        //    else
        //    {
        //        return null;
        //    }
            string token = CacheHelper.Get<string>(SmoovPOS.Common.ConstantSmoovs.ResponseStatusApiSmoovPay.Token);
            if (token == null)
            {
                using (var client = new WebClient())
                {
                    var url = string.Format(ConstantSmoovs.URLSmoovPayServer.OneTimeToken2, System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.UrlApiSmoovPay]); ;
                    var values = new NameValueCollection
                {
                    { "apiAccessKey", System.Configuration.ConfigurationManager.AppSettings[SmoovPOS.Common.ConstantSmoovs.AppSettings.ApiAccessKey] }
                };
                    var responsebytes = client.UploadValues(url, "POST", values);
                    var responsebody = Encoding.UTF8.GetString(responsebytes);
                    var JsonResponse = JsonConvert.DeserializeObject<ApiSmoovPay>(responsebody);
                    if (JsonResponse.status == ConstantSmoovs.ResponseStatusApiSmoovPay.Succcess)
                    {
                        var jObject = JObject.Parse(responsebody);
                        CacheHelper.Add<String>(jObject["result"].ToString(), SmoovPOS.Common.ConstantSmoovs.ResponseStatusApiSmoovPay.Token);
                        return jObject["result"].ToString();
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return token;
           
        }
        #endregion

    }
}
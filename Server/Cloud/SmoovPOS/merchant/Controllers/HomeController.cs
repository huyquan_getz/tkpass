﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Controllers;
using SmoovPOS.Utility.WSMerchantSettingReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Com.UI.Controllers
{
    public class HomeController : BaseController
    {   
        [Authorize]
        public ActionResult Index()
        {
            if (!(Request.IsAuthenticated && Session[ConstantSmoovs.Users.businessName] != null))
            {
                return RedirectToAction("Login","Account");
            }
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "dashboard";
            WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
           // MerchantSetting setting = new MerchantSetting();
            var setting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, VariableConfigController.GetBucket());
            clientMerchantSetting.Close();
            if (setting != null && setting.updateProfile && setting.createBranch && setting.createCategory && setting.addProduct && setting.distribute)
            {   
                return RedirectToAction("Dashboard");
            }
            else
            {
                ViewBag.lockMenu = "true";
                if (setting == null)
                {
                    setting = new MerchantSetting();
                }
                return View(setting);
            }
            
        }

        [Authorize]
        public ActionResult Dashboard()
        {
            WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
            // MerchantSetting setting = new MerchantSetting();
            var setting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, VariableConfigController.GetBucket());
            clientMerchantSetting.Close();
            if (setting.updateProfile && setting.createBranch && setting.createCategory && setting.addProduct && setting.distribute)
            {
                return RedirectToAction("Index", "Sale");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
         
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.Class = "dashboard";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            ViewBag.Class = "dashboard";
            return View();
        }

       // [HttpPost]
        //public int UpdateSetting(string field, string value)
        //{
        //    WSMerchantSettingClient clientMerchantSetting = new WSMerchantSettingClient();
        //    MerchantSetting setting = clientMerchantSetting.GetMerchantSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.SettingMerchantViewNameAll, VariableConfigController.GetBucket());
        //    if (field == "UpdateProfile")
        //    {
        //        setting.updateProfile = true;
        //    }
        //    if (field == "CreateBranch")
        //    {
        //        setting.createBranch = true;
        //    }
        //    if (field == "CreateCategory")
        //    {
        //        setting.createCategory = true;
        //    }
        //    if (field == "CreateProduct")
        //    {
        //        setting.addProduct = true;
        //    }
        //    if (field == "Distribute")
        //    {
        //        setting.distribute = true;
        //    }
        //    int checkStatus = clientMerchantSetting.UpdateMerchantSetting(setting,VariableConfigController.GetBucket());
        //    clientMerchantSetting.Close();
        //    return checkStatus;
        //}
    }
}
﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.UI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using SmoovPOS.Common;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.MailServer;
using SmoovPOS.Utility.WSEmailTemplateReference;
using SmoovPOS.Utility.WSMerchantManagementReference;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Models;
using SmoovPOS.LanguagePacks;
using SmoovPOS.Entity.BaseEntity;
using SmoovPOS.Utility.WSEmployeeReference;
using System.Collections.Generic;
using SmoovPOS.Utility.WSCheckoutSettingReference;
using SmoovPOS.Entity.Settings;
using SmoovPOS.Business.Business;
using Com.SmoovPOS.Model;
namespace SmoovPOS.UI.Controllers
{
    //[Authorize]
    public class AccountController : BaseController
    {
        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())), new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }
        public RoleManager<IdentityRole> RoleManager { get; private set; }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl, string business = "")
        {
            ViewBag.LoginFail = "display:none;";
            Session[ConstantSmoovs.Users.IsSet] = null;
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.business = business;
            LoginViewModel model = new LoginViewModel();
            model.Businness = business;
            return View(model);
        }
        public ActionResult ActiveMerchant(Guid id, Guid pinCode)
        {
            if (id != null && id != Guid.Empty)
            {
                Session[ConstantSmoovs.Users.IsSet] = false;

                IWSMerchantManagementClient client = new IWSMerchantManagementClient();
                //var merchantId = new Guid(id);
                var model = client.WSGetMerchantManagement(id);
                if (model != null)
                {
                    if (!model.isVerifyLink)
                    {
                        model.status = true;
                        model.verifyLink = string.Empty;
                        var result = client.WSUpdateMerchantManagement(model);
                        client.Close();
                        if (result)
                        {
                            try
                            {
                                //send mail

                                //IWSEmailTemplateClient clientEmail = new IWSEmailTemplateClient();
                                //TemplateEmail[] listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, VariableConfigController.GetBucket());
                                //if (listEmail.Count() == 0)
                                //{
                                //    listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, VariableConfigController.GetBucket());
                                //}
                                string bodyMail = "";

                                StringBuilder resultBuild;
                                //try
                                //{
                                //    TemplateEmail emailItem = listEmail.FirstOrDefault(r => r.index == ConstantSmoovs.BodyEmail.MerchantRegisterComplete);
                                //    bodyMail = emailItem != null ? (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "") : "";
                                //}
                                //catch
                                //{

                                //}
                                bodyMail = RenderRazorViewToStringNoModel("~/Views/MailTemplate/TemplateEmailRegisterComplete.cshtml");
                                resultBuild = new StringBuilder(bodyMail);
                                resultBuild.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfMerchant, model.firstName);
                                bodyMail = resultBuild.ToString();

                                bool isSend = sendEmailmerchant(1, ConstantSmoovs.SmoovPosTeam, model.email, ConstantSmoovs.TitleEmail.WelcomeToSmoovPos, bodyMail);

                                //  clientEmail.Close();


                            }
                            catch
                            {
                            }
                            // end send mail
                            return RedirectToAction("ResetPassword", new { id = id });
                        }

                    }
                    else
                    {
                        var user = UserManager.FindById(model.userID);
                        if (user != null && !user.EmailConfirmed)
                        {
                            return RedirectToAction("ResetPassword", new { id = id });
                        }
                    }

                    Session[ConstantSmoovs.Users.IsSet] = true;
                    return RedirectToAction("Success");
                }
            }
            return RedirectToAction("Login");
        }
        public ActionResult ResetPassword(Guid id)
        {
            if (id != null && id != Guid.Empty)
            {
                Session[ConstantSmoovs.Users.IsSet] = false;

                IWSMerchantManagementClient client = new IWSMerchantManagementClient();
                //var merchantId = new Guid(id);
                var model = client.WSGetMerchantManagement(id);
                client.Close();
                if (model != null)
                {
                    var user = UserManager.FindById(model.userID);
                    if (user != null && user.EmailConfirmed)
                    {
                        Session[ConstantSmoovs.Users.IsSet] = true;
                        return RedirectToAction("Success");
                    }
                    var modelChange = new ChangePasswordViewModel();
                    modelChange.userID = model.userID;

                    return View(modelChange);
                }
            }
            return RedirectToAction("Login");
        }
        [HttpPost]
        public ActionResult ResetPassword(string userID, string newPassword)
        {
            if (!string.IsNullOrWhiteSpace(userID))
            {
                var user = UserManager.FindById(userID);
                var confirmEmail = true;
                if (user != null)
                {
                    UserManager.RemovePassword(userID);

                    UserManager.AddPassword(userID, newPassword);
                    confirmEmail = user.EmailConfirmed;
                    user.EmailConfirmed = true;
                    UserManager.Update(user);
                }
                bool isSend = false;
                //----------------
                IWSMerchantManagementClient client = new IWSMerchantManagementClient();
                //var merchantId = new Guid(id);

                //   Guid guid = new Guid(model.userID);
                var modelMerchant = client.WSGetMerchantManagementByUserId(userID);
                client.Close();
                if (modelMerchant != null)
                {
                    try
                    {
                        if (!confirmEmail)
                        {
                            #region create employee merchant default
                            var employee = new Employee();
                            employee.address = modelMerchant.address;
                            employee.birthday = modelMerchant.birthday.Value.ToString();
                            employee.city = modelMerchant.city;
                            employee.country = modelMerchant.country;
                            employee.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                            employee.display = true;
                            employee.createdDate = DateTime.Now;
                            employee.email = modelMerchant.email;
                            employee.EmployeeId = 1000;
                            employee.firstName = modelMerchant.firstName;
                            employee.lastName = modelMerchant.lastName;
                            employee.fullName = modelMerchant.fullName;
                            employee.gender = modelMerchant.gender.Value ? 0 : 1;
                            employee.phone = modelMerchant.phone;
                            employee.siteId = modelMerchant.bucket;
                            employee.state = modelMerchant.region;
                            employee.status = true;
                            employee.userID = modelMerchant.userID;
                            employee.userOwner = modelMerchant.merchantID.ToString();
                            employee.zipCode = modelMerchant.zipCode;
                            employee.channels = new String[] { modelMerchant.merchantID.ToString() };
                            Permission permission = new Permission() { isMerchant = true };
                            employee.arrPermission = new List<Permission>() { permission };

                            employee.pinCode = string.Empty;

                            System.Text.StringBuilder addressShow = new System.Text.StringBuilder();
                            addressShow.Append(!string.IsNullOrWhiteSpace(modelMerchant.address) ? modelMerchant.address : "");
                            addressShow.Append(", ");
                            addressShow.Append(!string.IsNullOrWhiteSpace(modelMerchant.city) ? modelMerchant.city : "");
                            addressShow.Append(", ");
                            addressShow.Append(!string.IsNullOrWhiteSpace(modelMerchant.region) ? modelMerchant.region : "");
                            addressShow.Append(", ");
                            addressShow.Append(!string.IsNullOrWhiteSpace(modelMerchant.zipCode) ? modelMerchant.zipCode : "");
                            addressShow.Append(", ");
                            addressShow.Append(!string.IsNullOrWhiteSpace(modelMerchant.country) ? modelMerchant.country : "");

                            employee.AddressCombine = addressShow.ToString();
                            while (employee.AddressCombine.EndsWith(", "))
                            {
                                employee.AddressCombine = employee.AddressCombine.Substring(0, employee.AddressCombine.Length - 2);
                            }
                            employee._id = modelMerchant.merchantID.ToString();
                            employee.id = modelMerchant.merchantID.ToString();
                            employee.bucket = modelMerchant.merchantID.ToString();
                            WSEmployeeClient clientEmployee = new WSEmployeeClient();
                            int isresult = clientEmployee.WSCreateEmployee(employee, modelMerchant.bucket);
                            clientEmployee.Close();
                            #endregion

                            WSCheckoutSettingClient clientCheckout = new WSCheckoutSettingClient();
                            ViewBag.NumberInit = CheckInitiation();
                            CheckoutSetting checkout = new CheckoutSetting();
                            checkout._id = Guid.NewGuid().ToString();
                            checkout.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
                            checkout.createdDate = DateTime.Today;
                            checkout.acceptSelfCollection = true;
                            checkout.display = true;
                            checkout.instructionsSelfCollection = "";
                            checkout.instructionsSelfCollection = "";
                            checkout.isPickupPoint = false;
                            checkout.status = true;
                            checkout.timeDelivery = false;
                            checkout.timeSelfCollection = false;
                            checkout.typeCheckout = 1;
                            checkout.acceptDelivery = false;
                            checkout.bucket = modelMerchant.merchantID.ToString();
                            clientCheckout.CreateCheckoutSetting(checkout, modelMerchant.bucket);
                            clientCheckout.Close();
                        }
                        IWSEmailTemplateClient clientEmail = new IWSEmailTemplateClient();
                        TemplateEmail[] listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, VariableConfigController.GetBucket());
                        //if (listEmail.Count() == 0)
                        //{
                        //    listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, "smoovpos");
                        //}
                        string bodyMail = "";

                        StringBuilder resultBuild;
                        try
                        {
                            TemplateEmail emailItem = listEmail.FirstOrDefault(r => r.index == ConstantSmoovs.BodyEmail.MerchantResetPassword);
                            bodyMail = emailItem != null ? (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "") : "";
                        }
                        catch
                        {

                        }
                        if (string.IsNullOrWhiteSpace(bodyMail))
                        {
                            bodyMail = RenderRazorViewToStringNoModel("~/Views/MailTemplate/TempleResetPassword.cshtml");
                        }
                        resultBuild = new StringBuilder(bodyMail);
                        resultBuild.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfMerchant, !string.IsNullOrWhiteSpace(modelMerchant.firstName) ? modelMerchant.firstName : "");

                        bodyMail = resultBuild.ToString();
                        isSend = sendEmailmerchant(1, ConstantSmoovs.SmoovPosTeam, !string.IsNullOrWhiteSpace(modelMerchant.email) ? modelMerchant.email : "", ConstantSmoovs.TitleEmail.ResetPassword1, bodyMail);

                        clientEmail.Close();

                    }
                    catch
                    {
                    }
                }
            }

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Success()
        {
            if (Session[ConstantSmoovs.Users.IsSet] != null)
            {
                ViewBag.IsSet = (bool)Session[ConstantSmoovs.Users.IsSet];
            }
            else
            {
                return RedirectToAction("Login");
            }
            return View();
        }
        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            ViewBag.LoginFail = "display:block;";
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.Businness))
                {
                    //merchant login
                    var user = await UserManager.FindAsync(model.UserName, model.Password);
                    if (user != null && UserManager.IsInRole(user.Id, SmoovPOS.Common.ConstantSmoovs.RoleNames.Merchant))
                    {
                        bool isActive = false;
                        var merchant = new MerchantManagementModel();
                        //if (UserManager.IsInRole(user.Id, SmoovPOS.Common.ConstantSmoovs.RoleNames.Admin))
                        //{
                        //    isActive = true;
                        //}
                        //else
                        IWSMerchantManagementClient client = new IWSMerchantManagementClient();
                        merchant = client.WSGetMerchantManagementByUserId(user.Id);
                        if (merchant != null)
                        {
                            isActive = merchant.status;
                        }

                        if (isActive)
                        {
                            //if (merchant.servicePacketStartDate != null && merchant.servicePacketStartDate.Value > DateTime.Now)
                            //{
                            await SignInAsync(user, model.RememberMe);

                            #region add user to session
                            (new SmoovPOS.Utility.Controllers.UserController()).LoadUserInformation(UserManager, user, model, merchant);
                            #endregion

                            return RedirectToLocal(returnUrl);
                            //}
                            //else
                            //{
                            //    ModelState.AddModelError("", DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("WrongStartdate"));//"Account inactivated."
                            //}
                        }
                        else
                        {
                            ModelState.AddModelError("", DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("AccountInactivated"));//"Account inactivated."
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("InvalidUsernameOrPassword"));//"Invalid username or password."
                    }
                }
                else
                {
                    //employee merchant login
                    IWSMerchantManagementClient ClientMerchant = new IWSMerchantManagementClient();
                    var merchant = ClientMerchant.WSGetMerchantManagementByBusinessName(model.Businness);
                    ClientMerchant.Close();

                    if (merchant != null)
                    {
                        var userName = string.Format("{0}_{1}", merchant.bucket, model.UserName);
                        ApplicationUser user = UserManager.Find(userName, model.Password);
                        if (user != null)
                        {
                            WSEmployeeClient client = new WSEmployeeClient();
                            var employee = client.WSCheckByEmailPassword(ConstantSmoovs.CouchBase.DesignDocEmployee, ConstantSmoovs.CouchBase.CheckByEmailPassword, model.UserName, VariableConfigController.MD5Hash(model.Password), merchant.bucket);
                            client.Close();
                            var permission = employee.arrPermission.Where(m => m.store.index == 1).FirstOrDefault();
                            if (employee != null)
                            {
                                if (employee.status)
                                {
                                    if (permission == null)
                                    {
                                        ModelState.AddModelError("", DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("DontPersmission"));//"Account inactivated."

                                    }
                                    else
                                    {
                                        //ApplicationUser user = UserManager.Find(ConstantSmoovs.Customer.UserDefault, ConstantSmoovs.Customer.PassDefault);

                                        //user.Email = model.UserName;
                                        user.UserName = employee.firstName;
                                        //Set idUser for employee
                                        //user.Id = employee.userID;
                                        //merchant.userID = employee.userID;

                                        SignIn(user, model.RememberMe);
                                        #region add user to session
                                        (new SmoovPOS.Utility.Controllers.UserController()).LoadUserInformation(UserManager, user, model, merchant);
                                        #endregion
                                        return RedirectToAction("Index", "Home");
                                    }

                                }
                                else
                                {
                                    ModelState.AddModelError("", DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("AccountInactivated"));//"Account inactivated."

                                }
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("InvalidUsernameOrPassword"));//"Invalid username or password."

                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", DependencyResolver.Current.GetService<ILanguageManager>().GetMessageText("InvalidUsernameOrPassword"));//"Invalid username or password."
                    }


                }

            }
            //RoleManager.CreateAsync(new IdentityRole());
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public JsonResult CheckPasswordExist(string userName, string password, string newPassword)
        {
            var user = UserManager.Find(userName, password);
            if (user != null)
            {
                UserManager.RemovePassword(user.Id);
                UserManager.AddPassword(user.Id, newPassword);
                UserManager.Update(user);
                WSEmployeeClient client = new WSEmployeeClient();
                var empployee = client.WSDetailEmployee(user.Id, VariableConfigController.GetBucket());
                empployee.password = empployee.passwordConfirm = VariableConfigController.MD5Hash(newPassword);
                client.WSUpdateEmployee(empployee, VariableConfigController.GetBucket());
                client.Close();
                return new JsonResult
                {
                    Data = false,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return new JsonResult
                {
                    Data = true,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }


        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            user.UserName = user.UserName + "~" + "smoovpos";
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }
        private void SignIn(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        /// <summary>
        /// Sends the emailmerchant.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="merchantName">Name of the merchant.</param>
        /// <param name="email">The email.</param>
        /// <param name="titleEmail">The title email.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-3:07 PM</datetime>
        [HttpPost]
        public bool sendEmailmerchant(int type, string merchantName, string email, string titleEmail, string body)
        {
            MailServerUtil sendMail = new MailServerUtil();
            MailConfigBusiness mailConfig = new MailConfigBusiness();
            var merchantInfor = GetMerchantManagement();
            merchantName = merchantInfor != null ? merchantInfor.businessName : "";
            bool isSend = false;
            if (merchantInfor != null)
            {
                MailServerModel mailMerchant = new MailServerModel();
                mailMerchant = mailConfig.GetConfigMailServer(merchantInfor.merchantID);
                if (mailMerchant != null)
                {
                    isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body, merchantInfor.merchantID) : false;
                }
                else
                {
                    isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body) : false;
                }
            }
            else
            {
                isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body) : false;
            }

            return isSend;

        }
        /// <summary>
        /// Gets the merchant management.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-3:00 PM</datetime>
        public MerchantManagementModel GetMerchantManagement()
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            string siteID = VariableConfigController.GetBusinessName();
            var model = client.WSGetMerchantManagementByBusinessName(siteID);
            client.Close();
            return model;
        }
    }
}
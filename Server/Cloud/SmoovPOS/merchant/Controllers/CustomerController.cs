﻿using AutoMapper;
using Com.SmoovPOS.Common.Controllers;
using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Model;
using Com.SmoovPOS.TimeZone;
using SmoovPOS.Business.Business;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Models;
using SmoovPOS.UI.Models;
using SmoovPOS.UI.Models.Customer;
using SmoovPOS.Utility.IWSEmailTemplateReference;
using SmoovPOS.Utility.MailServer;
using SmoovPOS.Utility.WSCustomerReference;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using SmoovPOS.Utility.WSMerchantManagementReference;
using SmoovPOS.Utility.WSStationReference;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class CustomerController : BaseController
    {
        string SiteID = VariableConfigController.GetBucket();

        [Authorize]
        // GET: Customer
        public ActionResult Index()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "customer";
            CustomerListingModel customerListingModel = new CustomerListingModel();
            WSCustomerClient client = new WSCustomerClient();
            Customer[] ListCustomer = new Customer[25];
            try
            {
                ListCustomer = client.WSGetAllCustomer(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.GetAllCustomer, VariableConfigController.GetBucket());
                ViewBag.Reload = 0;
            }
            catch
            {
                ViewBag.Reload = 1;
            }
            customerListingModel.AllCustomerList = ListCustomer.OrderByDescending(i => i.modifiedDate);
            WSInventoryClient clientInventory = new WSInventoryClient();
            Country[] countrys = clientInventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, VariableConfigController.GetBucket());
            clientInventory.Close();
            customerListingModel.country = countrys[0];

            //Get and set merchant's currency
            customerListingModel.currency = GetGeneralSetting().Currency;

            return View(customerListingModel);
        }

        public ActionResult Create()
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "customer";
            CustomerItemListingModel customer = new CustomerItemListingModel();
            WSInventoryClient clientInventory = new WSInventoryClient();
            Country[] countrys = clientInventory.WSGetAllCountry("country", "get_all_country", VariableConfigController.GetBucket());

            //---Created by MaoNguyen--
            customer.listCountries = new List<SelectListItem>();
            var resultCustomerCountry = clientInventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, VariableConfigController.GetBucket()).Select(r => r.arrayCountry).ToList();
            foreach (string customerCountries in resultCustomerCountry[0])
            {
                customer.listCountries.Add(new SelectListItem() { Text = customerCountries, Value = customerCountries });
            }
            clientInventory.Close();
            customer.listGender = new List<SelectListItem>();
            customer.listGender.Add(new SelectListItem() { Text = "Male", Value = "0" });
            customer.listGender.Add(new SelectListItem() { Text = "Female", Value = "1" });
            customer.listGender.Add(new SelectListItem() { Text = "Others", Value = "2" });

            //customer.country = countrys[0];

            //Get and set merchant's currency
            customer.currency = GetGeneralSetting().Currency;

            return View(customer);
        }

        public JsonResult IsExistedEmail(string email, string id)
        {
            if (string.IsNullOrEmpty(email))
            {
                return new JsonResult
                {
                    Data = true,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            var isExisted = false;

            WSCustomerClient client = new WSCustomerClient();
            IEnumerable<Customer> checkCus = client.WSGetCustomerWithEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, VariableConfigController.GetBucket());
            client.Close();

            isExisted = (checkCus != null && checkCus.Count() > 0);
            if (isExisted && !string.IsNullOrEmpty(id))
            {
                isExisted = (checkCus != null && (!checkCus.Any(c => c._id == id)));
            }

            return new JsonResult
            {
                Data = !isExisted,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public ActionResult Create(FormCollection fc)
        {
            Customer customer = new Customer();
            List<Tags> listTags = new List<Tags>();
            Tags tag = new Tags();
            Orderdetail Order = new Orderdetail();
            customer._id = Guid.NewGuid().ToString();
            customer.display = true;
            customer.active = false;
            customer.channels = new string[] { "server" };
            customer.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            customer.pinCode = Guid.NewGuid().ToString();
            customer.memberID = "onl-" + (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            customer.lastOrder = DateTime.UtcNow;
            try
            {
                customer.timeZoneFullName = GetGeneralSetting().TimeZoneFullName;
            }
            catch
            {
                customer.timeZoneFullName = "Singapore Standard Time";
            }

            customer.status = false;
            customer.verify = false;
            customer.firstName = !string.IsNullOrWhiteSpace(fc["firstName"]) ? fc["firstName"] : "";
            customer.lastName = !string.IsNullOrWhiteSpace(fc["lastName"]) ? fc["lastName"] : "";
            customer.fullName = customer.firstName + " " + customer.lastName;
            customer.email = !string.IsNullOrWhiteSpace(fc["email"]) ? fc["email"] : "";
            customer.phone = !string.IsNullOrWhiteSpace(fc["phonenumber"]) ? fc["phonenumber"] : "";
            customer.birthday = !string.IsNullOrWhiteSpace(fc["birthday"]) ? fc["birthday"] : "";
            customer.gender = !string.IsNullOrWhiteSpace(fc["gender"]) ? Convert.ToInt32(fc["gender"]) : 2;
            customer.address = !string.IsNullOrWhiteSpace(fc["address"]) ? fc["address"] : "";
            customer.city = !string.IsNullOrWhiteSpace(fc["city"]) ? fc["city"] : "";
            customer.zipCode = !string.IsNullOrWhiteSpace(fc["zipcode"]) ? fc["zipcode"] : "";
            customer.country = !string.IsNullOrWhiteSpace(fc["country"]) ? fc["country"] : "";
            customer.state = !string.IsNullOrWhiteSpace(fc["state"]) ? fc["state"] : "";
            customer.passwordConfirm = !string.IsNullOrWhiteSpace(fc["confirmPass"]) ? fc["confirmPass"] : "";
            customer.password = !string.IsNullOrWhiteSpace(fc["password"]) ? fc["password"] : "";
            customer.remark = !string.IsNullOrWhiteSpace(fc["remark"]) ? fc["remark"] : "";
            bool checkMeOut = false;
            if (!string.IsNullOrWhiteSpace(fc["check_me_out"]))
            {
                checkMeOut = Boolean.Parse(fc["check_me_out"]);
                customer.acceptsNewsletter = checkMeOut;
            }

            if (!string.IsNullOrWhiteSpace(fc["myTag"]))
            {
                String[] arrTag = fc["myTag"].Split(',');
                for (int i = 0; i < arrTag.Count(); i++)
                {
                    tag.index = i;
                    tag.name = arrTag[i];
                    tag.keyName = arrTag[i];
                    listTags.Add(tag);
                }
                customer.strTags = fc["myTag"];
            }
            else
            {
                customer.strTags = "";
            }
            customer.tags = listTags;
            customer.addressCombine = customer.address + ", " + customer.city + " " + customer.zipCode + ", " + customer.state + ", " + customer.country;
            customer.channels = new String[] { "server" };
            customer.acceptsSms = !string.IsNullOrWhiteSpace(fc["sms"]) ? Boolean.Parse(fc["sms"]) : false;
            customer.image = !string.IsNullOrWhiteSpace(fc["logoImg"]) ? fc["logoImg"] : string.Empty;
            WSCustomerClient client = new WSCustomerClient();

            int result = client.WSCreateCustomer(customer, VariableConfigController.GetBucket());
            string storeUrl = VariableConfigController.GetHttpAlias();
            SendMailNewPassWord(customer, storeUrl, !string.IsNullOrWhiteSpace(VariableConfigController.GetBusinessName()) ? VariableConfigController.GetBusinessName() : "");

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(FormCollection fc, string id)
        {
            WSCustomerClient client = new WSCustomerClient();
            Customer customer = client.WSDetailCustomer(id, VariableConfigController.GetBucket());
            List<Tags> listTags = new List<Tags>();
            Tags tag = new Tags();
            try
            {
                customer.status = Boolean.Parse(fc["status"]);
            }
            catch (Exception)
            {
            }
            customer.firstName = !string.IsNullOrWhiteSpace(fc["firstName"]) ? fc["firstName"] : "";
            customer.lastName = !string.IsNullOrWhiteSpace(fc["lastName"]) ? fc["lastName"] : "";
            customer.fullName = customer.firstName + " " + customer.lastName;
            customer.email = !string.IsNullOrWhiteSpace(fc["email"]) ? fc["email"] : "";
            customer.phone = !string.IsNullOrWhiteSpace(fc["phonenumber"]) ? fc["phonenumber"] : "";
            customer.birthday = !string.IsNullOrWhiteSpace(fc["birthday"]) ? fc["birthday"] : "";
            if (!string.IsNullOrWhiteSpace(fc["gender"]))
            {
                string gender = fc["gender"];
                customer.gender = gender == "1" ? 1 : gender == "0" ? 0 : 2;
            }
            customer.address = !string.IsNullOrWhiteSpace(fc["address"]) ? fc["address"] : "";
            customer.city = !string.IsNullOrWhiteSpace(fc["city"]) ? fc["city"] : "";
            customer.zipCode = !string.IsNullOrWhiteSpace(fc["zipcode"]) ? fc["zipcode"] : "";
            customer.country = !string.IsNullOrWhiteSpace(fc["country"]) ? fc["country"] : "";
            customer.state = !string.IsNullOrWhiteSpace(fc["state"]) ? fc["state"] : "";
            customer.remark = !string.IsNullOrWhiteSpace(fc["remark"]) ? fc["remark"] : "";
            string checkmeout = fc["check_me_out"];
            string sms = fc["sms"];
            bool checkMeOut = customer.acceptsNewsletter;
            if (!string.IsNullOrWhiteSpace(fc["check_me_out"]))
            {
                checkMeOut = fc["check_me_out"] == "on" ? true : false;
                customer.acceptsNewsletter = checkMeOut;
            }
            else
            {
                customer.acceptsNewsletter = false;
            }

            if (!string.IsNullOrWhiteSpace(fc["myTag"]))
            {
                listTags.Clear();
                String[] arrTag = fc["myTag"].Split(',');
                for (int i = 0; i < arrTag.Count(); i++)
                {
                    tag.index = i;
                    tag.name = arrTag[i];
                    tag.keyName = arrTag[i];
                    listTags.Add(tag);
                }
                customer.strTags = fc["myTag"];
            }
            else
            {
                listTags.Clear();
                customer.strTags = "";
            }

            customer.tags = listTags;
            customer.addressCombine = customer.address + ", " + customer.city + " " + customer.zipCode + ", " + customer.state + ", " + customer.country;

            if (!string.IsNullOrWhiteSpace(fc["sms"]))
            {
                customer.acceptsSms = fc["sms"] == "on" ? true : false;
            }
            else
            {
                customer.acceptsSms = false;
            }
            customer.image = !string.IsNullOrWhiteSpace(fc["logoImg"]) ? fc["logoImg"] : string.Empty;
            try
            {
                int result = client.WSUpdateCustomer(customer, VariableConfigController.GetBucket());
                client.Close();
            }
            catch (Exception) { }
            return RedirectToAction("Detail", new { id = id });
        }
        [Authorize]
        public ActionResult Detail(string id)
        {
            ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "customer";
            string siteID = ViewBag.siteID = VariableConfigController.GetBucket();
            WSInventoryClient clientIventory = new WSInventoryClient();
            Country[] countrys = clientIventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, siteID);
            clientIventory.Close();

            //Get and set merchant's currency
            ViewBag.currency = GetGeneralSetting().Currency;

            WSCustomerClient client = new WSCustomerClient();
            Customer customer = client.WSDetailCustomer(id, VariableConfigController.GetBucket());
            client.Close();

            var model = Mapper.Map<Customer, CustomersViewModel>(customer);
            model.phonenumber = customer.phone;

            if (model.listOrder != null && model.listOrder.Count() > 0)
            {
                foreach (var item in model.listOrder)
                {

                    item.dateOrder = TimeZoneUtil.ConvertUTCToTimeZone(item.dateOrder, item.timeZone, siteID);
                }
                model.listOrder = model.listOrder.OrderBy(r => r.dateOrder).Take(5).ToList();
            }

            // Get amount to divide by MaoNguyen
            var generalSetting = GetGeneralSetting();
            double amount = model.totalSpend / Convert.ToDouble(generalSetting.MemberPointChangeRate);
            //Round number down
            model.Amount = Math.Floor(amount).ToString();

            return View(model);
        }

        public ActionResult LoadMore(string id)
        {
            string siteId = VariableConfigController.GetBucket();

            ViewBag.Class = "customer";
            WSInventoryClient clientIventory = new WSInventoryClient();
            Country[] countrys = clientIventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, siteId);
            clientIventory.Close();

            string siteID = ViewBag.siteID = VariableConfigController.GetBucket();
            //Get and set merchant's currency
            ViewBag.currency = GetGeneralSetting().Currency;

            WSCustomerClient client = new WSCustomerClient();
            Customer customer = client.WSDetailCustomer(id, siteId);
            client.Close();

            var model = Mapper.Map<Customer, CustomersViewModel>(customer);

            if (model.listOrder != null && model.listOrder.Count() > 0)
            {
                foreach (var item in model.listOrder)
                {

                    item.dateOrder = TimeZoneUtil.ConvertUTCToTimeZone(item.dateOrder, item.timeZone, siteID);
                }

            }
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult updateStatusCustomer(string id, bool status)
        {
            WSCustomerClient client = new WSCustomerClient();
            string siteId = VariableConfigController.GetBucket();
            var customer = client.WSDetailCustomer(id, siteId);
            customer.status = status;
            client.WSUpdateCustomer(customer, siteId);
            client.Close();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public Boolean checkEmailExist(string email)
        {
            WSCustomerClient client = new WSCustomerClient();
            IEnumerable<Customer> checkCus = client.WSGetCustomerWithEmail(ConstantSmoovs.CouchBase.DesignDocCustomer, ConstantSmoovs.CouchBase.CheckMailExist, email, VariableConfigController.GetBucket());
            client.Close();
            if (checkCus.Count() == 0) return true;
            else return false;
        }

        /*
        * Quynh.Hoang
        * step1: get detail customer by Id
        * step2: update status and call service update customer
        */
        [Authorize]
        public ActionResult Edit(string id)
        {
            ViewBag.NumberInit = CheckInitiation();
            //CustomerInfomation
            ViewBag.Class = "customer";
            WSCustomerClient client = new WSCustomerClient();
            //  CustomerInfomation customerInfomation = new CustomerInfomation();
            CustomersViewModel customerModal = new CustomersViewModel();

            WSInventoryClient clientInventory = new WSInventoryClient();
            Country[] countrys = clientInventory.WSGetAllCountry(ConstantSmoovs.CouchBase.DesignDocCountry, ConstantSmoovs.CouchBase.GetAllCountry, VariableConfigController.GetBucket());
            //  customerInfomation.country = countrys[0];
            //   customerInfomation.customer = client.WSDetailCustomer(id, VariableConfigController.GetBucket());
            Customer customer = new Customer();
            customer = client.WSDetailCustomer(id, VariableConfigController.GetBucket());

            if (customer != null)
            {
                customerModal = Mapper.Map<Customer, CustomersViewModel>(customer);
                customerModal.phonenumber = customer.phone;
            }

            customerModal.listCountry = countrys[0];
            clientInventory.Close();
            client.Close();
            //return View(customerInfomation);
            return View(customerModal);
        }

        /*
         * Quynh.Hoang
         * Delete customer by Id
         */
        public ActionResult Delete(string id)
        {
            WSCustomerClient client = new WSCustomerClient();
            int reponseCode = client.WSDeleteCustomer(id, VariableConfigController.GetBucket());
            return RedirectToAction("Index");
        }

        public void SendMailNewPassWord(Customer cus, string storeUrl, string businessName)
        {
            // string body = RenderRazorViewToStringNoModel("~/Views/MailTemplate/Customer/TemplateCreateNewPassWord.cshtml");

            IWSEmailTemplateClient clientEmail = new IWSEmailTemplateClient();
            TemplateEmail[] listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, VariableConfigController.GetBucket());
            if (listEmail.Count() == 0)
            {
                listEmail = clientEmail.WSGetAllTemplateEmail(ConstantSmoovs.CouchBase.DesignDocTemplateEmail, ConstantSmoovs.CouchBase.TemplateEmailViewAll, VariableConfigController.GetBucket());
            }
            string body = "";
            try
            {
                TemplateEmail emailItem = listEmail.FirstOrDefault(r => r.index == ConstantSmoovs.BodyEmail.MemberVerifyAccountEmail);
                body = emailItem != null ? (!string.IsNullOrWhiteSpace(emailItem.body) ? emailItem.body : "") : "";
            }
            catch
            {

            }
            if (string.IsNullOrWhiteSpace(body))
            {
                body = RenderRazorViewToStringNoModel("~/Views/MailTemplate/Customer/TemplateCreateNewPassWord.cshtml");
            }
            //------------------------
            StringBuilder result = new StringBuilder(body.ToString());
            result.Replace(ConstantSmoovs.EmailKeyword.FirstNameOfCustomer, cus.firstName);
            result.Replace(ConstantSmoovs.EmailKeyword.BusinessName, businessName);
            result.Replace(ConstantSmoovs.EmailKeyword.SupportEmailOfMerchant, "support@smoovpos.com");
            string linkResetPassword = storeUrl + "Customer/ResetPassword?email=" + cus.email + "&pinCode=" + cus.pinCode;
            result.Replace(ConstantSmoovs.EmailKeyword.LinkResetPasswork, linkResetPassword);
            result.Replace(ConstantSmoovs.EmailKeyword.OnlineshopLink, linkResetPassword);
            MailServerUtil sendMail = new MailServerUtil();
            //   bool isSend = sendMail.SendMail(2, businessName, cus.email, ConstantSmoovs.TitleEmail.VerifyAccount, result.ToString());
            sendEmailMerchant(ConstantSmoovs.ConfigMail.Merchant, businessName, cus.email, ConstantSmoovs.TitleEmail.VerifyAccount, result.ToString());
        }
        /// <summary>
        /// Sends the email merchant.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="merchantName">Name of the merchant.</param>
        /// <param name="email">The email.</param>
        /// <param name="titleEmail">The title email.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-3:02 PM</datetime>
        [HttpPost]
        public bool sendEmailMerchant(int type, string merchantName, string email, string titleEmail, string body)
        {
            MailServerUtil sendMail = new MailServerUtil();
            MailConfigBusiness mailConfig = new MailConfigBusiness();
            var merchantInfor = GetMerchantManagement();
            merchantName = merchantInfor != null ? merchantInfor.businessName : "";
            bool isSend = false;
            if (merchantInfor != null)
            {
                MailServerModel mailMerchant = new MailServerModel();
                mailMerchant = mailConfig.GetConfigMailServer(merchantInfor.merchantID);
                if (mailMerchant != null)
                {
                    isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body, merchantInfor.merchantID) : false;
                }
                else
                {
                    isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body) : false;
                }
            }
            else
            {
                isSend = !string.IsNullOrWhiteSpace(email) ? sendMail.SendMail(type, merchantName, email, titleEmail, body) : false;
            }

            return isSend;

        }
        /// <summary>
        /// Gets the merchant management.
        /// </summary>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>2/3/2015-3:00 PM</datetime>
        public MerchantManagementModel GetMerchantManagement()
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            string siteID = VariableConfigController.GetBusinessName();
            var model = client.WSGetMerchantManagementByBusinessName(siteID);
            client.Close();
            return model;
        }

    }
}
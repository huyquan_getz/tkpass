﻿using AutoMapper;
using Com.SmoovPOS.Entity;
using Microsoft.AspNet.Identity;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Models.MemberPoint;
using SmoovPOS.Utility.WSCollectionReference;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Controllers
{
    public class MemberPointController : BaseController
    {
        string siteID = VariableConfigController.GetBucket();
        // GET: MemberPoint
        public ActionResult Index()
        {
            ViewBag.Class = "MemberPoint";

            MemberPointViewModel model = new MemberPointViewModel();
            var generalSetting = GetGeneralSetting();

            //Get currency setting
            string currency = generalSetting.Currency;
            if (!string.IsNullOrEmpty(currency)) model.Currency = currency;
            else model.Currency = "SGD";

            //Get saved amount
            model.Amount = generalSetting.MemberPointChangeRate;

            return View(model);
        }

        public ActionResult UpdateInfo(FormCollection fc) 
        {
            MemberPointViewModel memberModel = new MemberPointViewModel();

            //Get the CB record
            var generalSetting = GetGeneralSetting();
            bool isCreate = generalSetting == null;
            if (isCreate)
            {
                generalSetting = new MerchantGeneralSetting();
                generalSetting._id = Guid.NewGuid().ToString();

                generalSetting.createdDate = DateTime.UtcNow;
                generalSetting.userOwner = User.Identity.GetUserId();
                generalSetting.userID = generalSetting.userOwner;
                generalSetting.modifiedDate = DateTime.UtcNow;
                var idMerchant = VariableConfigController.GetIDMerchant();
                generalSetting.channels = new String[] { idMerchant };
                generalSetting.status = true;
                generalSetting.display = true;
                generalSetting.DateTimeInt = (DateTime.UtcNow - ConstantSmoovs.year1970).TotalSeconds.ToString();
            }
            //Set the new values
            generalSetting.MemberPointChangeRate = fc["Amount"];

            //Call WS to save the update info
            try
            {
                int result = 0;
                if (isCreate)
                {
                    WSMerchantGeneralSettingClient client = new WSMerchantGeneralSettingClient();
                    result = client.CreateNewMerchantGeneralSetting(generalSetting, siteID);
                    client.Close();
                }
                else
                {
                    WSMerchantGeneralSettingClient client = new WSMerchantGeneralSettingClient();
                    result = client.UpdateMerchantGeneralSetting(generalSetting, siteID);
                    client.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Reload this page after done instead of redirecting to other page
            return RedirectToAction("Index");
        }
    }
}
﻿using Com.SmoovPOS.Entity;
using Newtonsoft.Json.Linq;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Models.Tax;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSMerchantSettingReference;
using SmoovPOS.Utility.WSTaxReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SmoovPOS.UI.Controllers
{
    public class TaxController : BaseController
    {
        [Authorize]
        // GET: Tax
        public ActionResult Index()
        {
            //ViewBag.NumberInit = CheckInitiation();
            ViewBag.Class = "tax";
            WSTaxClient client = new WSTaxClient();
            WSInventoryClient clientInventory = new WSInventoryClient();
            TaxModel taxModel = new TaxModel();
            Inventory[] ListInventory = clientInventory.WSGetAll(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, VariableConfigController.GetBucket());
            taxModel.inventoryList = ListInventory;
            var listTax = client.GetAllTax(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAllTax, VariableConfigController.GetBucket());
            List<TaxItemModel> listTaxItem = new List<TaxItemModel>();
            taxModel.listStoreId = new List<string>();
           
            if (listTax == null || listTax.Count() == 0)
            {
                Tax tax = new Tax();
                tax._id = Guid.NewGuid().ToString();
                tax.display = true;
                tax.status = true;
                tax.userID = VariableConfigController.GetUserID();
                tax.userOwner = VariableConfigController.GetBucket();                 
                var onlineInventory = ListInventory.Where(i => i.index == 1).FirstOrDefault();
                ServiceCharge servicecharge = new ServiceCharge();
                servicecharge.index = 1;
                servicecharge.serviceChargeRate = 0.00;
                servicecharge.serviceChargeValue = 0.00;
                servicecharge.store = onlineInventory;
                tax.arrServiceCharge = new List<ServiceCharge>() {servicecharge};
                taxModel.listStoreId.Add(onlineInventory._id);
                int response = client.WSCreateTax(tax, VariableConfigController.GetBucket());
                taxModel.id = tax._id;
            }
            else
            {
                foreach (Inventory inventory in ListInventory)
                {
                    TaxItemModel taxItem = new TaxItemModel();
                    taxItem.indexStore = inventory.index;
                    var serviceCharge = listTax[0].arrServiceCharge.Where(s => s.index == inventory.index).FirstOrDefault();
                    taxItem.serviceCharge = serviceCharge;                  
                    var listTaxValue = listTax[0].arrayTax != null ? listTax[0].arrayTax.Where(t => t.storeInformation.Where(s => s.index == inventory.index).Count() > 0) : new List<TaxItem>() ;
                    taxItem.listTaxItem = listTaxValue;
                    taxItem.storeName = inventory.businessID;
                    listTaxItem.Add(taxItem);
                    if(serviceCharge != null || inventory.index == 1){
                         taxModel.listStoreId.Add(inventory._id);
                    }
                }

                taxModel.id = listTax[0].id;
            }
            taxModel.listTaxItem = listTaxItem;
            client.Close();
            clientInventory.Close();
            return View(taxModel);
        }
        public ActionResult ViewNewTax()
        {
            return PartialView("NewTax");
        }

        [HttpPost]
        public ActionResult NewTax()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {

            string strIdStore = collection["idStore"];
            string strServiceCharge = collection["serviceCharge"];
            string taxId = collection["taxId"];

            if (!string.IsNullOrWhiteSpace(collection["serviceCharge"]))
            {
                WSTaxClient client = new WSTaxClient();
                Tax tax = client.DetailTax(taxId, VariableConfigController.GetBucket());
                string[] arrIdStore = strIdStore.Split(',');
                string[] arrServiceCharge = strServiceCharge.Split(',');
                for (int i = 0; i < arrIdStore.Count(); i++)
                {
                    List<ServiceCharge> listServiceCharge = tax.arrServiceCharge;
                    var item = listServiceCharge.Where(r => r.index == int.Parse(arrIdStore[i])).First();
                    if (item != null)
                    {
                        string value = arrServiceCharge[i] != "" ? arrServiceCharge[i] : "0.00";
                        item.serviceChargeValue = double.Parse(value);
                    }
                }
                int response = client.WSCreateTax(tax, VariableConfigController.GetBucket());
                client.Close();
            }

            return RedirectToAction("Index");
        }

        public Boolean checkServiceChargeExist(string[] listIndex, string id)
        {
            WSTaxClient client = new WSTaxClient();
            Tax tax = client.DetailTax(id, VariableConfigController.GetBucket());
            if (tax.arrServiceCharge == null && tax.arrServiceCharge.Count == 0) return false;
            else
            {
                foreach (string index in listIndex)
                {
                    var serviceCharge = tax.arrServiceCharge.Where(s => s.store._id == index);
                    if (serviceCharge.Count() > 0) return true;
                }     
            }
            return false;
        }

        public ActionResult addNewTax(FormCollection collection)
        {
            ViewBag.Class = "tax";
            int type = int.Parse(collection["type"]);
            double value = double.Parse(collection["taxRate"]);
            string taxName = !string.IsNullOrEmpty(collection["taxName"])? collection["taxName"] : "";
            string strStoreId = collection["storeId"];
            string isTaxItemId = collection["submit"];
            string[] arrStoreId = null;
            if(!string.IsNullOrEmpty(strStoreId)){
                arrStoreId = strStoreId.Split(',');
            }
            string id = collection["taxId"];
            WSInventoryClient clientInventory = new WSInventoryClient();
            Inventory[] ListInventory = clientInventory.WSGetAll(ConstantSmoovs.CouchBase.DesigndocInventory, ConstantSmoovs.CouchBase.GetAllInventories, VariableConfigController.GetBucket());
            WSTaxClient client = new WSTaxClient();
            Tax tax = client.DetailTax(id, VariableConfigController.GetBucket());

            if (string.IsNullOrEmpty(isTaxItemId)) // add new servicecharge or Tax
            {
                //type == 1 : service charge
                if (type > 0)
                {
                    foreach (string idStore in arrStoreId)
                    {
                        ServiceCharge serviceCharge = new ServiceCharge();
                        serviceCharge.serviceChargeRate = Math.Round(value, 2, MidpointRounding.AwayFromZero);
                        serviceCharge.serviceChargeValue = Math.Round(value, 2, MidpointRounding.AwayFromZero);
                        var inventory = ListInventory.Where(i => i._id == idStore).FirstOrDefault();
                        serviceCharge.index = inventory.index;
                        serviceCharge.store = inventory;
                        if (tax.arrServiceCharge == null)
                        {
                            tax.arrServiceCharge = new List<ServiceCharge>();
                        }
                        tax.arrServiceCharge.Add(serviceCharge);
                    }


                }
                else // type == 0 : Tax item
                {
                    TaxItem taxItem = new TaxItem();
                    taxItem._id = Guid.NewGuid().ToString();
                    var idMerchant = VariableConfigController.GetIDMerchant();
                    taxItem.channels = new string[] { idMerchant+ "_pos" };
                    taxItem.display = true;
                    taxItem.isPercent = true;
                    taxItem.valueRate = Math.Round(value, 2);
                    taxItem.status = true;
                    taxItem.name = taxName;
                    taxItem.storeInformation = new List<StoreInformation>();
                    foreach (string idStore in arrStoreId)
                    {
                        var inventory = ListInventory.Where(i => i._id == idStore).FirstOrDefault();
                        StoreInformation storeInfo = new StoreInformation();
                        storeInfo.index = inventory.index;
                        storeInfo.storeID = inventory._id;
                        storeInfo.storeName = inventory.businessID;
                        storeInfo.valueRate = Math.Round(value, 2);
                        taxItem.storeInformation.Add(storeInfo);

                    }
                    if (tax.arrayTax == null)
                    {
                        tax.arrayTax = new List<TaxItem>();
                    }
                    tax.arrayTax.Add(taxItem);
                }
            }
            else // edit 
            {
                if (type > 0)
                {
                    var serviceCharge = tax.arrServiceCharge.Where(t => t.index == int.Parse(collection["isEdit"])).FirstOrDefault();
                    serviceCharge.serviceChargeRate = serviceCharge.serviceChargeValue = Math.Round(value, 2);
                }
                else
                {
                    var taxItem = tax.arrayTax.Where(t => t._id == isTaxItemId).FirstOrDefault();
                    var storeInfo = taxItem.storeInformation.Where(s => s.index == int.Parse(collection["isEdit"])).FirstOrDefault();
                    storeInfo.valueRate = Math.Round(value, 2);
                }
              
            }
           
          
            int response = client.WSCreateTax(tax, VariableConfigController.GetBucket());
            clientInventory.Close();
            client.Close();
            return RedirectToAction("Index");
        }
        /*
       * Quynh.Hoang
       * step1: get TaxItem from view 
       * step2: update TaxItem into arrayTaxs
       * step3: update Tax
       */
        public Boolean DeleteServiceCharge(string id, int indexStore)
        {
            ViewBag.Class = "tax";
            WSTaxClient client = new WSTaxClient();
            Tax tax = client.DetailTax(id, VariableConfigController.GetBucket());
            var serviceCharge = tax.arrServiceCharge.Where(s => s.index == indexStore).FirstOrDefault();
            tax.arrServiceCharge.Remove(serviceCharge);
            int response = client.WSCreateTax(tax, VariableConfigController.GetBucket());
            client.Close();
            if (response == 0) return true;
            else return false;

        }

        public Boolean Delete(string id, int indexStore, string taxItemId)
        {
            ViewBag.Class = "tax";
            WSTaxClient client = new WSTaxClient();
            Tax tax = client.DetailTax(id, VariableConfigController.GetBucket());
            var taxItem = tax.arrayTax.Where(t => t._id == taxItemId).FirstOrDefault();
            if (taxItem.storeInformation.Count() == 1)
            {
                tax.arrayTax.Remove(taxItem);
            }
            else
            {
                taxItem.storeInformation.Remove(taxItem.storeInformation.Where(s => s.index == indexStore).FirstOrDefault());
            }
            int response = client.WSCreateTax(tax, VariableConfigController.GetBucket());
            client.Close();
            if (response == 0) return true;
            else return false;
        }
    }
}
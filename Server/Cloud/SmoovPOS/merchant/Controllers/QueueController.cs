﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UI.Models;
using SmoovPOS.Utility.WSInventoryReference;
using SmoovPOS.Utility.WSQRGeneratorReference;
using SmoovPOS.Utility.WSQueueReference;
using System;
using System.Web.Mvc;
using CouchBaseCommon = SmoovPOS.Common.ConstantSmoovs.CouchBase;

namespace SmoovPOS.UI.Controllers
{
    public class QueueController : BaseController
    {
        string ReservationLink = "";
        string SiteID = "";

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            //this.ReservationLink = System.Configuration.ConfigurationManager.AppSettings["ReservationLink"].ToString();
            this.SiteID = VariableConfigController.GetBucket();
        }

        // GET: Queue
        public ActionResult Index()
        {
            QueueModel queueModel = new QueueModel();
            queueModel.ListInventory = new System.Collections.Generic.List<Inventory>();

            WSInventoryClient inventoryClient = new WSInventoryClient();

            var inventorys = inventoryClient.WSGetAll(CouchBaseCommon.DesigndocInventory, CouchBaseCommon.GetAllInventories, this.SiteID);

            foreach (var inventory in inventorys)
            {
                if (inventory.index != Common.ConstantSmoovs.Stores.OnlineStore && inventory.businessID != Common.ConstantSmoovs.Stores.Master)
                {
                    queueModel.ListInventory.Add(inventory);
                }
            }
            if (queueModel.ListInventory.Count > 0)
            {
                queueModel.SelectedBranchId = queueModel.ListInventory[0]._id;

                WSQueueClient queueClient = new WSQueueClient();

                Queue queue = queueClient.WSDetailQueueByBranchId(CouchBaseCommon.DesigndocQueue, CouchBaseCommon.ViewNameGetAllQueue, queueModel.ListInventory[0].businessID, this.SiteID);
                queueModel.Queue = queue;

                queueClient.Close();
            }

            inventoryClient.Close();

            return View(queueModel);
        }

        public ActionResult Detail(string id)
        {
            string branchId = id;
            QueueModel queueModel = new QueueModel();
            Inventory inventory = new Inventory();

            WSInventoryClient inventoryClient = new WSInventoryClient();

            inventory = inventoryClient.WSDetailInventory(branchId, this.SiteID);

            inventoryClient.Close();

            WSQueueClient queueClient = new WSQueueClient();

            Queue queue = queueClient.WSDetailQueueByBranchId(CouchBaseCommon.DesigndocQueue, CouchBaseCommon.ViewNameGetAllQueue, branchId, this.SiteID);
            queueModel.Queue = queue;
            queueModel.SelectedBranchId = branchId;

            queueClient.Close();

            return PartialView("Detail", queueModel);
        }

        private int ConvertCmToPixel(int input_cm)
        {
            double pixel = -1;
            using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromHwnd(System.IntPtr.Zero))
            {
                double dpi = graphics.DpiX;
                pixel = (double)input_cm * dpi / 2.54d;
            }
            return int.Parse(System.Math.Round(pixel).ToString());
        }

        public ActionResult Edit(string id)
        {
            string branchId = id;

            QueueModel queueModel = new QueueModel();
            queueModel.ListInventory = new System.Collections.Generic.List<Inventory>();

            WSInventoryClient inventoryClient = new WSInventoryClient();

            var inventory = inventoryClient.WSDetailInventory(branchId, this.SiteID);

            inventoryClient.Close();

            WSQueueClient queueClient = new WSQueueClient();

            Queue queue = queueClient.WSDetailQueueByBranchId(CouchBaseCommon.DesigndocQueue, CouchBaseCommon.ViewNameGetAllQueue, branchId, this.SiteID);
            queueModel.ListInventory.Add(inventory);
            queueModel.Queue = queue;

            queueClient.Close();

            return View(queueModel);
        }

        [HttpPost]
        public string GenerateQRCode(FormCollection fc)
        {
            string qr_url = "";
            QRGeneratorClient qrClient = new QRGeneratorClient();

            Queue queue = InitQueue(fc["branch-id"]);
            queue.QRCodeThumb = qrClient.QRCodeGenerator(fc["reservation-link"], ConvertCmToPixel(8), "");
            queue.QRCodeSize1 = queue.QRCodeThumb;
            queue.QRCodeSize2 = qrClient.QRCodeGenerator(fc["reservation-link"], ConvertCmToPixel(12), "");
            queue.QRCodeSize3 = qrClient.QRCodeGenerator(fc["reservation-link"], ConvertCmToPixel(15), "");
            queue.QRCodeSize4 = qrClient.QRCodeGenerator(fc["reservation-link"], ConvertCmToPixel(30), "");
            queue.QRCodeSize5 = qrClient.QRCodeGenerator(fc["reservation-link"], ConvertCmToPixel(50), "");

            qrClient.Close();

            return qr_url;
        }

        private Queue InitQueue(string branch_id)
        {
            Queue queue = new Queue();

            WSQueueClient queueClient = new WSQueueClient();

            queue = queueClient.WSDetailQueueByBranchId(CouchBaseCommon.DesigndocQueue, CouchBaseCommon.ViewNameGetAllQueue, branch_id, this.SiteID);

            queueClient.Close();

            if (queue == null)
            {
                queue = new Queue();
                queue._id = Guid.NewGuid().ToString();
                queue.BranchId = branch_id;

                WSInventoryClient inventoryClient = new WSInventoryClient();

                var inventory = inventoryClient.WSDetailInventory(branch_id, this.SiteID);

                inventoryClient.Close();

                queue.BranchName = inventory.businessID;
            }

            return queue;
        }
    }
}
﻿using Autofac;
using Autofac.Extras.Multitenant;
using Autofac.Integration.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SmoovPOS.UI.App_Start
{
    public class AutoFacConfig
    {
        public static void RegisterContainers()
        {
            var builder = new ContainerBuilder();
            //Lingual AutoFac
            builder.RegisterAssemblyTypes(Assembly.Load("SmoovPOS.LanguagePacks"))
            .Where(t => t.Name.EndsWith("LanguageManager"))
            .AsImplementedInterfaces().SingleInstance();

            #region Customize Implementation (Multi Tenants)

            var tenantIdStrategy = new MyRequestParameterStrategy();
            builder.RegisterInstance(tenantIdStrategy).As<ITenantIdentificationStrategy>();

            var container = new MultitenantContainer(tenantIdStrategy, builder.Build());

            //container.ConfigureTenant("MLNG",
            //c =>
            //{
            //    c.RegisterType<BaseDependency>().As<IDependency>().InstancePerHttpRequest();

            //    c.RegisterAssemblyTypes(Assembly.Load("iP.Products.LanguagePacks.MLNG"))
            //    .Where(t => t.Name.EndsWith("LanguageManager"))
            //    .AsImplementedInterfaces().SingleInstance();
            //});

            #endregion

            
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
        //public static string GetTenantName()
        //{
        //    try
        //    {
        //        return RouteTable.Routes.GetRouteData(new HttpContextWrapper(HttpContext.Current)).Values["tenant"].ToString();
        //    }
        //    catch (Exception)
        //    {
        //        return "Master";
        //    }
        //}
    }
    public class MyRequestParameterStrategy : ITenantIdentificationStrategy
    {
        public bool TryIdentifyTenant(out object tenantId)
        {
            //tenantId = AutoFacConfig.GetTenantName();

            //return tenantId != null;
            tenantId = 0;
            return true;
        }
    }
}
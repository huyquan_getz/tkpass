﻿using System.Web;
using System.Web.Optimization;

namespace Com.UI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                     "~/Scripts/angular.js",
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/bootstrap-tagsinput.js",
                     "~/Scripts/bootstrap-tagsinput-angular.js",
                     "~/Scripts/jquery.maskMoney.js",                    
                     "~/Scripts/ckeditor/ckeditor.js",
                     "~/Scripts/jquery-ui.min.js",
                     "~/Scripts/respond.js",
                     "~/Scripts/switchery.js",
                     "~/Scripts/smoovposLibrary.js",
                     "~/Scripts/jquery.dataTables.js",
                     "~/Content/dataTables.bootstrap.js",
                     "~/Scripts/bootstrap-datetimepicker.min.js",
                     "~/Scripts/underscore.js",
                     "~/Scripts/async.js"
                     ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                     "~/Content/bootstrap.css",
                     "~/Content/font-awesome-4.2.0/css/font-awesome.min.css",
                     "~/Content/dashboard.css",
                     "~/Content/bootstrap-tagsinput.css",
                     "~/Content/site.css",
                     "~/Content/Responsive.css",
                     "~/Content/jquery-ui.css",
                     "~/Content/switchery.css",
                     "~/Content/dataTables.bootstrap.css",
                     "~/Content/jquery.dataTables.css",
                     "~/Content/dataTables.tableTools.css",
                     "~/Content/bootstrap-datetimepicker.min.css"
                     ));
        }
    }
}

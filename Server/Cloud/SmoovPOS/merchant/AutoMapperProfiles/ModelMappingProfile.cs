﻿using AutoMapper;
using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Model;
using SmoovPOS.Entity.DeviceToken;
using SmoovPOS.Entity.Models;
using SmoovPOS.UI.Models;
using SmoovPOS.UI.Models.DeviceToken;
using SmoovPOS.UI.Models.General;

namespace SmoovPOS.UI.AutoMapperProfiles
{
    public class ModelMappingProfile : AutoMapper.Profile
    {
        public override string ProfileName
        {
            get { return "ModelMappingProfile"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<MerchantManagementModel, MerchantManagementViewModel>();
            Mapper.CreateMap<CollectionViewModel, Collection>();
            Mapper.CreateMap<InventoryViewModel, Collection>();
            Mapper.CreateMap<InfoNewEmployee, Collection>();
            Mapper.CreateMap<CustomersViewModel, Collection>();
            //Mapper.CreateMap<ProductInventoryModel, Collection>();
            Mapper.CreateMap<CategoryViewModel, Collection>();

            Mapper.CreateMap<Collection, CollectionViewModel>();

            Mapper.CreateMap<Collection, InventoryViewModel>();
            Mapper.CreateMap<Collection, InfoNewEmployee>();
            Mapper.CreateMap<Collection, CustomersViewModel>();
            //Mapper.CreateMap<Collection, ProductInventoryModel>();
            Mapper.CreateMap<Collection, CategoryViewModel>();

            Mapper.CreateMap<MailServerModel, EmailConfigurationViewModel>();
            Mapper.CreateMap<EmailConfigurationViewModel, MailServerModel>();
            Mapper.CreateMap<Employee, EmployeeInfor>();;
            Mapper.CreateMap<ServiceDelivery, ServiceDeliveryViewModel>();
            Mapper.CreateMap<ServiceDeliveryViewModel, ServiceDelivery>();

            Mapper.CreateMap<MerchantManagementModel, ChangePasswordMerchantManagementViewModel>();
            Mapper.CreateMap<Customer, CustomersViewModel>();
            Mapper.CreateMap<DetailDeviceViewModel, DeviceTokenModel>();
            Mapper.CreateMap<DeviceTokenModel, DetailDeviceViewModel>();

            Mapper.CreateMap<TableOrderingSetting, TableOrderingViewModel>();
        }
    }
}
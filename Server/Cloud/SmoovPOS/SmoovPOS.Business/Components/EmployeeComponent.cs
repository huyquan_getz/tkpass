﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using System.Linq;

namespace Com.SmoovPOS.Business.Components
{
    public class EmployeeComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        static EmployeeComponent() { }
        public int CreateNewEmployee(Employee employee, string bucket)
        {
            employee.bucket = bucket;
            EmployeeDAC employeeDAC = new EmployeeDAC(bucketSmoovPOS);
            int statusCode = employeeDAC.CreateDAC(employee);
            return statusCode;
        }
        public int UpdateEmployee(Employee employee, string bucket)
        {
            employee.bucket = bucket;
            EmployeeDAC employeeDAC = new EmployeeDAC(bucketSmoovPOS);
            int statusCode = employeeDAC.updateDAC(employee);
            return statusCode;
        }

        public int DeleteEmployee(string id, string bucket)
        {
            EmployeeDAC employeeDAC = new EmployeeDAC(bucketSmoovPOS);
            int statusCode = employeeDAC.deleteDAC(id);
            return statusCode;
        }
        public Employee DetailEmployee(string id, string bucket)
        {
            EmployeeDAC employeeDAC = new EmployeeDAC(bucketSmoovPOS);
            Employee employee = employeeDAC.detailDAC(id);
            return employee;
        }

        public IView<Employee> GetAllEmployee(string designDoc, string viewName, string bucket)
        {
            EmployeeDAC employeeDAC = new EmployeeDAC(bucketSmoovPOS);
            IView<Employee> ListEmployee = null;
            ListEmployee = employeeDAC.GetAllWithBucket(designDoc, ConstantSmoovs.CouchBase.EmployeeViewNameAll, bucket);
            return ListEmployee;
        }

        public int GetNextEmployeeId(string designDoc, string viewName, string bucket)
        {
            EmployeeDAC employeeDAC = new EmployeeDAC(bucketSmoovPOS);
            return employeeDAC.GetNextEmployeeId(designDoc, viewName, bucket);
        }
        public int CheckEmailExist(string designDoc, string viewName, string email, string bucket)
        {
            EmployeeDAC employeeDAC = new EmployeeDAC(bucketSmoovPOS);
            IView<Employee> listEmployee = null;
            listEmployee = employeeDAC.GetAll(designDoc, viewName);
            listEmployee.Key(new string[] { bucket, email });
            return listEmployee.Count();
        }

        public Employee CheckByEmailPassword(string designDoc, string viewName, string email, string password, string bucket)
        {
            EmployeeDAC employeeDAC = new EmployeeDAC(bucketSmoovPOS);
            Employee emp = null;
            emp = employeeDAC.GetAll(designDoc, viewName).Key(new string[] { bucket, email, password }).FirstOrDefault();
            return emp;
        }
    }
}
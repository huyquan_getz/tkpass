﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Model;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;


namespace SmoovPOS.Business.Components
{
    public class OrderComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        static OrderComponent() { }
        public int CreateOrder(Order order, string bucket)
        {
            order.bucket = bucket;
            OrderDAC orderDAC = new OrderDAC(bucketSmoovPOS);
            return orderDAC.CreateDAC(order);
        }

        public Order GetOrder(string key, string bucket)
        {
            OrderDAC orderDAC = new OrderDAC(bucketSmoovPOS);
            return orderDAC.GetOrderDAC(key);
        }

        public IEnumerable<Order> GetAllOrder(string designDoc, string viewName, string bucket)
        {
            OrderDAC orderDAC = new OrderDAC(bucketSmoovPOS);
            IEnumerable<Order> orderList = orderDAC.GetAll(designDoc, viewName, bucket);
            return orderList;
        }

        public int DeleteAllOrder(string designDoc, string viewName, string bucket)
        {
            OrderDAC orderDAC = new OrderDAC(bucketSmoovPOS);
            return orderDAC.DeleteAllOrderDAC(designDoc, viewName, bucket);
        }

        public IEnumerable<Order> GetOrderBySearchCondition(string designDoc, string viewName, SearchOrderModel searchOrderModel, string bucket)
        {
            OrderDAC orderDAC = new OrderDAC(bucketSmoovPOS);
            IEnumerable<Order> orderList = orderDAC.GetAll(designDoc, viewName, bucket);

            if (searchOrderModel.OrderType.HasValue)
            {
                orderList = orderList.Where(r => r.orderType == searchOrderModel.OrderType);
            }

            if (!string.IsNullOrWhiteSpace(searchOrderModel.FilterKey))
            {
                switch (searchOrderModel.FilterField)
                {
                    case ConstantSmoovs.FilterFieldList.FilterFieldOrderCode:
                        orderList = orderList.Where(r => Contains(r.orderCode, searchOrderModel.FilterKey));
                        break;
                    case ConstantSmoovs.FilterFieldList.FilterFieldTransactionID:
                        orderList = orderList.Where(r => Contains(r.transactionID, searchOrderModel.FilterKey));
                        break;
                    case ConstantSmoovs.FilterFieldList.FilterFieldCustomer:
                        orderList = orderList.Where(r => Contains(r.CustomerDisplay, searchOrderModel.FilterKey));
                        break;
                    case ConstantSmoovs.FilterFieldList.FilterFieldStore:
                        orderList = orderList.Where(r => r.inventory != null && Contains(r.inventory.businessID, searchOrderModel.FilterKey));
                        break;
                }
            }

            if (searchOrderModel.PaymentStatusKey.HasValue)
            {
                orderList = orderList.Where(r => r.paymentStatus == searchOrderModel.PaymentStatusKey);
            }

            if (searchOrderModel.OrderStatusKey.HasValue)
            {
                orderList = orderList.Where(r => r.orderStatus == searchOrderModel.OrderStatusKey);
            }

            if (searchOrderModel.StartDate.HasValue)
            {
                orderList = orderList.Where(r => r.completedDate.Date >= searchOrderModel.StartDate.Value.Date);
            }

            if (searchOrderModel.EndDate.HasValue)
            {
                orderList = orderList.Where(r => r.completedDate.Date <= searchOrderModel.EndDate.Value.Date);
            }

            return orderList.ToList();
            //return 

        }

        private bool Contains(string str, string value)
        {
            //if (string.IsNullOrWhiteSpace(value)) return true;
            return !string.IsNullOrWhiteSpace(str) && str.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        public int UpdateOrder(Order order, string bucket)
        {
            order.bucket = bucket;
            OrderDAC orderDAC = new OrderDAC(bucketSmoovPOS);
            int statusCode = orderDAC.updateDAC(order);
            return statusCode;
        }

        public IEnumerable<Order> GetAllOrderDelivery(string designDoc, string viewName, string bucket)
        {
            OrderDAC orderDAC = new OrderDAC(bucketSmoovPOS);
            IEnumerable<Order> orderList = orderDAC.GetAllOrderDelivery(designDoc, viewName, bucket);
            return orderList;
        }
        public IEnumerable<Order> GetAllOrderSeflCollect(string designDoc, string viewName, string bucket)
        {
            OrderDAC orderDAC = new OrderDAC(bucketSmoovPOS);
            IEnumerable<Order> orderList = orderDAC.GetAllOrderSelfCollect(designDoc, viewName, bucket);
            return orderList;
        }
        public IEnumerable<Order> GetAllOrderCancel(string designDoc, string viewName, string bucket)
        {
            OrderDAC orderDAC = new OrderDAC(bucketSmoovPOS);
            IEnumerable<Order> orderList = orderDAC.GetAllOrderCancel(designDoc, viewName, bucket);
            return orderList;
        }

        public IEnumerable<Order> GetAllOrderPaging(string designDoc, string viewName, SearchOrderModel searchOrderModel, EntityPaging entityPaging, string bucket)
        {
            OrderDAC OrderDAC = new OrderDAC(bucketSmoovPOS);
            IView<Order> listOrder = null;

            listOrder = GetOrderByOrderType(designDoc, viewName, searchOrderModel, OrderDAC);
            if (searchOrderModel.PaymentStatusKey.HasValue)
            {

                listOrder = (IView<Order>)listOrder.ToList().Where(r => r.paymentStatus == searchOrderModel.PaymentStatusKey);
            }

            if (searchOrderModel.OrderStatusKey.HasValue)
            {
                listOrder = (IView<Order>)listOrder.ToList().Where(r => r.orderStatus == searchOrderModel.OrderStatusKey);
            }

            if (searchOrderModel.StartDate.HasValue)
            {
                listOrder = (IView<Order>)listOrder.ToList().Where(r => r.completedDate.Date >= searchOrderModel.StartDate.Value.Date);
            }

            if (searchOrderModel.EndDate.HasValue)
            {
                listOrder = (IView<Order>)listOrder.ToList().Where(r => r.completedDate.Date <= searchOrderModel.EndDate.Value.Date);
            }

            //--------------------------PAGING -------------------
            //get count total
            //searchOrderModel.countAll = listOrder.ToList().Count();


            //-------------------------
            if (listOrder != null && entityPaging != null && listOrder.ToList().Count() > 0)
            {
                if (!entityPaging.desc)
                {
                    listOrder.Descending(false);
                }
                else
                {
                    listOrder.Descending(true);
                }

                entityPaging.startKey = entityPaging.sort == "orderCode" ?
                    listOrder.ElementAt((entityPaging.pageGoTo - 1) * entityPaging.limit).orderCode : entityPaging.sort == "category" ?
                    listOrder.ElementAt((entityPaging.pageGoTo - 1) * entityPaging.limit).orderCode : listOrder.ElementAt((entityPaging.pageGoTo - 1) * entityPaging.limit)._id;
                entityPaging.endKey = null;

                int LastOfPage = listOrder.Count() / entityPaging.limit;
                int percent = listOrder.Count() % entityPaging.limit;
                if (percent > 0)
                {
                    LastOfPage += 1;
                }
                if (entityPaging.pageGoTo == LastOfPage)
                {
                    entityPaging.startKey = entityPaging.sort == "orderCode" ?
                        listOrder.ElementAt((entityPaging.pageGoTo - 1) * entityPaging.limit).orderCode : entityPaging.sort == "category" ?
                        listOrder.ElementAt((entityPaging.pageGoTo - 1) * entityPaging.limit).orderCode : listOrder.ElementAt((entityPaging.pageGoTo - 1) * entityPaging.limit)._id;
                    entityPaging.endKey = entityPaging.sort == "orderCode" ?
                        listOrder.ElementAt(listOrder.Count() - 1).orderCode : entityPaging.sort == "category" ?
                        listOrder.ElementAt(listOrder.Count() - 1).orderCode : listOrder.ElementAt(listOrder.Count() - 1)._id;
                }

                if (!string.IsNullOrEmpty(entityPaging.startKey))
                {
                    if (!entityPaging.allowStale) listOrder.Stale(StaleMode.False);
                    if (entityPaging.sort != "status")
                    {
                        listOrder.StartKey(entityPaging.startKey);
                        if (!string.IsNullOrEmpty(entityPaging.endKey)) listOrder.EndKey(entityPaging.endKey);
                    }
                    else
                    {
                        listOrder.StartKey(entityPaging.desc);
                        if (!string.IsNullOrEmpty(entityPaging.startKey)) listOrder.EndKey(entityPaging.endKey);
                    }
                }
                if (entityPaging.limit > 0) listOrder.Limit(entityPaging.limit * 5);
            }

            return listOrder != null ? listOrder.ToList() : null;
        }
        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <param name="searchModel">The search model.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public SearchOrderModel GetOrder(SearchOrderModel searchModel)
        {
            OrderDAC OrderDAC = new OrderDAC(bucketSmoovPOS);
            CountDAC CountDAC = new CountDAC(bucketSmoovPOS);

            var isSearch = false;
            if ((!string.IsNullOrEmpty(searchModel.FilterField) && !string.IsNullOrEmpty(searchModel.FilterKey)) ||
                searchModel.EndDate.HasValue ||
                searchModel.StartDate.HasValue ||
                searchModel.PaymentStatusKey.HasValue ||
                searchModel.OrderStatusKey.HasValue ||
                !string.IsNullOrEmpty(searchModel.StoreKey))
            {
                isSearch = true;
            }
            if (!string.IsNullOrEmpty(searchModel.sort))
            {
                if (searchModel.sort != SmoovPOS.Common.ConstantSmoovs.OrderSort.completedDate)
                {
                    isSearch = true;
                }
            }
            //bool isLimit = (!isSearch && searchModel.pageGoTo <= 5);
            var countAll = 0;
            var results = GetOrder(ConstantSmoovs.CouchBase.DesignDocOrder, searchModel, OrderDAC, CountDAC, isSearch, searchModel.siteID, out countAll);

            //search local
            if (searchModel.PaymentStatusKey.HasValue)
            {

                results = results.Where(r => r.paymentStatus == searchModel.PaymentStatusKey);
            }

            if (searchModel.OrderStatusKey.HasValue)
            {
                results = results.Where(r => r.orderStatus == searchModel.OrderStatusKey);
            }

            if (searchModel.StartDate.HasValue)
            {
                var startDate = searchModel.StartDate.Value;
                startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
                results = results.Where(r => r.modifiedDate >= startDate);
            }

            if (searchModel.EndDate.HasValue)
            {
                var endDate = searchModel.EndDate.Value;
                endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 0, 0, 0).AddDays(1);
                results = results.Where(r => r.modifiedDate < endDate);
            }
            if (!string.IsNullOrEmpty(searchModel.StoreKey))
            {
                results = results.Where(r => r.inventory != null && r.inventory.index.ToString() == searchModel.StoreKey);
            }

            if (!string.IsNullOrEmpty(searchModel.FilterKey) && !string.IsNullOrEmpty(searchModel.FilterField))
            {
                var search = searchModel.FilterKey.Trim().ToUpper();
                switch (searchModel.FilterField)
                {
                    case ConstantSmoovs.FilterFieldList.FilterFieldOrderCode:
                        results = results.Where(r => r.orderCode.ToUpper().Contains(search));
                        break;
                    case ConstantSmoovs.FilterFieldList.FilterFieldTransactionID:
                        results = results.Where(r => r.transactionID.ToUpper().Contains(search));
                        break;
                    case ConstantSmoovs.FilterFieldList.FilterFieldCustomer:
                        results = results.Where(r => r.CustomerDisplay.ToUpper().Contains(search));
                        break;
                    case ConstantSmoovs.FilterFieldList.FilterFieldStore:
                        //results = results.Where(r => r.s.Contains(search));
                        break;
                    default:
                        break;
                }
            }

            //get count total
            if (!isSearch)
            {
                searchModel.countAll = countAll;
            }
            else
            {
                searchModel.countAll = results.Count();
            }
            if (searchModel.countAll > 0)
            {
                //sort
                if (!string.IsNullOrEmpty(searchModel.sort))
                {
                    //sort by full name
                    switch (searchModel.sort)
                    {
                        case SmoovPOS.Common.ConstantSmoovs.OrderSort.orderCode:
                            if (searchModel.desc)
                            {
                                results = results.OrderByDescending(x => x.orderCode);
                            }
                            else
                            {
                                results = results.OrderBy(x => x.orderCode);
                            }
                            break;
                        case SmoovPOS.Common.ConstantSmoovs.OrderSort.completedDate:
                            if (searchModel.desc)
                            {
                                results = results.OrderByDescending(x => x.completedDate);
                            }
                            else
                            {
                                results = results.OrderBy(x => x.completedDate);
                            }
                            break;
                        case SmoovPOS.Common.ConstantSmoovs.OrderSort.CustomerDisplay:
                            if (searchModel.desc)
                            {
                                results = results.OrderByDescending(x => x.CustomerDisplay);
                            }
                            else
                            {
                                results = results.OrderBy(x => x.CustomerDisplay);
                            }
                            break;
                        case SmoovPOS.Common.ConstantSmoovs.OrderSort.grandTotal:
                            if (searchModel.desc)
                            {
                                results = results.OrderByDescending(x => x.grandTotal);
                            }
                            else
                            {
                                results = results.OrderBy(x => x.grandTotal);
                            }
                            break;
                        default:
                            break;
                    }
                }
                //else
                //{
                //    //sort default
                //    results = results.OrderByDescending(x => x.completedDate);
                //}

                //paging
                if (isSearch)
                {
                    searchModel.ListOrders = results.Skip((searchModel.pageGoTo - 1) * searchModel.limit).Take(searchModel.limit).ToList();
                }
                else
                {
                    searchModel.ListOrders = results.ToList();
                }

                searchModel.pageCurrent = searchModel.pageGoTo;//set page current
            }

            return searchModel;
        }
        /// <summary>
        /// Gets the type of the order by order.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="searchOrderModel">The search order model.</param>
        /// <param name="OrderDAC">The order dac.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        private IView<Order> GetOrderByOrderType(string designDoc, string viewName, SearchOrderModel searchOrderModel, OrderDAC OrderDAC)
        {
            IView<Order> listOrder = null; //(IView<Order>)OrderDAC.GetAllOrderDelivery(designDoc, viewName);

            switch (searchOrderModel.OrderType.GetValueOrDefault())
            {
                case ConstantSmoovs.OrderType.OrderTypeDelivery:
                    listOrder = (IView<Order>)OrderDAC.GetAllOrderDelivery(designDoc, ConstantSmoovs.CouchBase.OrderViewNameAllDelivery, searchOrderModel.siteID);
                    break;
                case ConstantSmoovs.OrderType.OrderTypeSelfCollect:
                    listOrder = (IView<Order>)OrderDAC.GetAllOrderSelfCollect(designDoc, ConstantSmoovs.CouchBase.OrderViewNameAllSelfCollect, searchOrderModel.siteID);
                    break;
                case ConstantSmoovs.OrderType.OrderTypeCancel:
                    listOrder = (IView<Order>)OrderDAC.GetAllOrderCancel(designDoc, ConstantSmoovs.CouchBase.OrderViewNameAllCancel, searchOrderModel.siteID);
                    break;
                default:
                    listOrder = (IView<Order>)OrderDAC.GetAllOrderDelivery(designDoc, ConstantSmoovs.CouchBase.OrderViewNameAll, searchOrderModel.siteID);
                    break;

            }

            return listOrder;
        }
        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="searchModel">The search model.</param>
        /// <param name="OrderDAC">The order dac.</param>
        /// <param name="CountDAC">The count dac.</param>
        /// <param name="isSearch">if set to <c>true</c> [is search].</param>
        /// <param name="countAll">The count all.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        private IEnumerable<Order> GetOrder(string designDoc, SearchOrderModel searchModel, OrderDAC OrderDAC, CountDAC CountDAC, bool isSearch, string bucket, out int countAll)
        {
            IEnumerable<Order> listOrder = null; //(IView<Order>)OrderDAC.GetAllOrderDelivery(designDoc, viewName);

            string viewName = ConstantSmoovs.CouchBase.OrderViewNameAll;
            switch (searchModel.OrderType.GetValueOrDefault())
            {
                case ConstantSmoovs.OrderType.OrderTypeDelivery:
                    viewName = ConstantSmoovs.CouchBase.OrderViewNameAllDelivery;
                    break;
                case ConstantSmoovs.OrderType.OrderTypeSelfCollect:
                    viewName = ConstantSmoovs.CouchBase.OrderViewNameAllSelfCollect;
                    break;
                case ConstantSmoovs.OrderType.OrderTypeCancel:
                    viewName = ConstantSmoovs.CouchBase.OrderViewNameAllCancel;
                    break;
                case ConstantSmoovs.OrderType.OrderTypeTableOrdering:
                    viewName = ConstantSmoovs.CouchBase.OrderViewNameAllTableOrder;
                    break;
                default:
                    viewName = ConstantSmoovs.CouchBase.OrderViewNameAll;
                    break;
            }
            if (!isSearch)
            {
                var skip = (searchModel.pageGoTo - 1) * searchModel.limit;
                //if (searchModel.pageGoTo > 5)
                //{
                //    limit = limit * (((int)searchModel.pageGoTo / 5) + 1);
                //}
                //limit = limit + 1;
                countAll = CountDAC.GetCount(designDoc, viewName,bucket);
                var desc = true;
                if (!string.IsNullOrEmpty(searchModel.sort))
                {
                    desc = searchModel.desc;
                }

                listOrder = OrderDAC.GetOrderDescendingByLimit(designDoc, viewName,bucket, desc, searchModel.limit, skip);
            }
            else
            {
                listOrder = OrderDAC.GetOrderDescendingByLimit(designDoc, viewName, bucket);
                countAll = 0;
            }
            return listOrder;
        }
        public IEnumerable<Order> GetAllOrderByCustomerEmail(string designDoc, string viewName, string siteID, string email)
        {
            //Retrieve data from coughbase
            OrderDAC orderDAC = new OrderDAC(bucketSmoovPOS);
            IView<Order> listOrder = orderDAC.GetAllOrderByCustomerEmail(designDoc, viewName, email, siteID);

            //Return value
            return listOrder.ToList();
        }

        public SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel GetListPaidAndRefundedOrdersInPeriod(string designDoc, string viewName, string siteID, DateTime BeginDate, DateTime EndDate, bool IsNextDay, string BranchIndex)
        {
            //Get status index of payment status
            int intPaymenPaid = ConstantSmoovs.Enums.PaymentStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.PaymentStatus.Paid).Key;
            int intPaymenRefunded = ConstantSmoovs.Enums.PaymentStatus.FirstOrDefault(r => r.Value == ConstantSmoovs.PaymentStatus.Refunded).Key;

            OrderDAC orderDAC = new OrderDAC(bucketSmoovPOS);

            //Get all orders
            IEnumerable<Order> orderList = orderDAC.GetAll(designDoc, viewName, siteID);

            //Filter orders from BranchName if BranchName is not empty (BranchName = "" -> get orders from all branches)
            if (!string.IsNullOrEmpty(BranchIndex)) orderList = orderList.Where(i => (i.inventory != null) && (i.inventory.index.ToString() == BranchIndex));

            //Filter orders that payment = Paid or Refunded from beginDate to endDate+1 (for the case IsNextDay=true)
            orderList = orderList.Where(i => ((i.paidDate != null) && (BeginDate <= i.paidDate && i.paidDate <= EndDate.AddDays(1)))
                                                || ((i.refundInfo != null) && (BeginDate <= i.refundInfo.refundDate && i.refundInfo.refundDate <= EndDate.AddDays(1))))
                                    .Where(i => (i.paymentStatus == intPaymenPaid) || (i.paymentStatus == intPaymenRefunded));

            SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel returnValue = new SmoovPOS.Entity.Models.ListPaidAndRefundedOrdersModel();
            List<Order> listPaid = new List<Order>();
            List<Order> listRefunded = new List<Order>();
            //For loop to execute on each day
            for (DateTime date = BeginDate; date.Date <= EndDate.Date; date = date.AddDays(1))
            {
                //Set the From/To datetime as general setting's daily report period for each day in report
                DateTime dtFrom = date;
                DateTime dtTo = date.Date.AddHours(EndDate.Hour).AddMinutes(EndDate.Minute);
                if (dtTo < dtFrom) dtTo = dtTo.AddDays(1);//Compensate the time lost due to assigning dtTo=date=BeginDate, especially when BeginTime=00:00 and EndTime=23:59 and similarly
                if (IsNextDay) dtTo = dtTo.AddDays(1);

                //Generate temporary list of orders in day X
                if (dtTo <= EndDate || (IsNextDay && (dtTo <= EndDate.AddDays(1))))
                {
                    IEnumerable<Order> tempList1 = orderList.Where(i => (i.paidDate != null) && (dtFrom <= i.paidDate && i.paidDate <= dtTo));
                    listPaid.AddRange(tempList1);
                    IEnumerable<Order> tempList2 = orderList.Where(i => (i.refundInfo != null) && (dtFrom <= i.refundInfo.refundDate && i.refundInfo.refundDate <= dtTo));
                    listRefunded.AddRange(tempList2);
                }
            }

            //Returm list of orders that is not identical
            returnValue.listPaidOrders = listPaid.Distinct().ToList();
            returnValue.listRefundedOrders = listRefunded.Distinct().ToList();
            return returnValue;
        }
    }
}
﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using System.Linq;

namespace SmoovPOS.Business.Components
{
    public class MyCartComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();

        public MyCart GetMyCartByEmail(string designDoc, string viewName, string bucket, string email)
        {
            MyCartDAC myCartDAC = new MyCartDAC(bucketSmoovPOS);
            MyCart myCart = myCartDAC.GetAll(designDoc, viewName).Key(new string[] {bucket, email}).FirstOrDefault();
            return myCart;
        }

        public int CreateMyCart(MyCart myCart, string bucket)
        {
            myCart.bucket = bucket;
            MyCartDAC myCartDAC = new MyCartDAC(bucketSmoovPOS);
            int returnCode = myCartDAC.CreateDAC(myCart);
            return returnCode;
        }

        public int UpdateMyCart(MyCart myCart, string bucket)
        {
            myCart.bucket = bucket;
            MyCartDAC myCartDAC = new MyCartDAC(bucketSmoovPOS);
            int returnCode = myCartDAC.UpdateDAC(myCart);
            return returnCode;
        }

        public int DeleteMyCart(string id, string bucket)
        {
            MyCartDAC myCartDAC = new MyCartDAC(bucketSmoovPOS);
            int returnCode = myCartDAC.DeleteDAC(id);
            return returnCode;
        }
    }
}
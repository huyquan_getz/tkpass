﻿using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Business.Components
{
    public class CustomerComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateNewCustomer(Customer customer, string bucket)
        {
            customer.bucket = bucket;
            CustomerDAC customerDAC = new CustomerDAC(bucketSmoovPOS);
            int statusCode = customerDAC.CreateDAC(customer);
            return statusCode;
        }
        public int UpdateCustomer(Customer customer, string bucket)
        {
            customer.bucket = bucket;
            CustomerDAC customerDAC = new CustomerDAC(bucketSmoovPOS);
            int statusCode = customerDAC.updateDAC(customer);
            return statusCode;
        }

        public int DeleteCustomer(string id, string bucket)
        {
            CustomerDAC customerDAC = new CustomerDAC(bucketSmoovPOS);
            int statusCode = customerDAC.deleteDAC(id);
            return statusCode;
        }
        public Customer DetailCustomer(string id, string bucket)
        {
            CustomerDAC customerDAC = new CustomerDAC(bucketSmoovPOS);
            Customer customer = customerDAC.detailDAC(id);
            return customer;
        }
        public IView<Customer> GetAllCustomer(string designDoc, string viewName, string bucket)
        {
            CustomerDAC customerDAC = new CustomerDAC(bucketSmoovPOS);
            IView<Customer> ListCustomer = null;
            ListCustomer = customerDAC.GetAll(designDoc, "get_all_customer").Key(bucket);
            return ListCustomer;
        }
        /// <summary>
        /// Check email customer exist
        /// </summary>
        /// <param name="designDoc">customer</param>
        /// <param name="viewName">get_all_customer</param>
        /// <param name="email"></param>
        /// <returns></returns>
        public int CheckEmailExist(string designDoc, string viewName, string email, string bucket)
        {
            CustomerDAC customerDAC = new CustomerDAC(bucketSmoovPOS);
            IView<Customer> listCustomer = null;
            listCustomer = customerDAC.GetAll(designDoc, viewName);
            listCustomer.Key(new string[]{ bucket, email });
            return listCustomer.Count();
        }

        /// <summary>
        /// Check email and password customer sigin in
        /// </summary>
        /// <param name="designDoc">customer</param>
        /// <param name="viewName">check_username_password</param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Customer CheckAccountCustomer(string designDoc, string viewName, string email, string password, string bucket)
        {
            CustomerDAC customerDAC = new CustomerDAC(bucketSmoovPOS);
            IView<Customer> listCustomer = null;
            listCustomer = customerDAC.GetAll(designDoc, viewName).Key(new string[] { bucket, email, password });
            Customer cus = null;
            if (listCustomer.Count() == 1)
            {
                cus = listCustomer.FirstOrDefault();
            }
            return cus;
        }

        public IView<Customer> GetCustomerWithEmail(string designDoc, string viewName, string email, string bucket)
        {
            CustomerDAC customerDAC = new CustomerDAC(bucketSmoovPOS);
            IView<Customer> listCustomer = null;
            listCustomer = customerDAC.GetAll(designDoc, viewName).Key(new string[] { bucket, email });
            return listCustomer;
        }

        /// <summary>
        /// Get detail customer by email
        /// </summary>
        /// <param>string email</param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>26/12/2014-10:07 PM</createddate>
        /// 
        public Customer GetDetailCustomer(string designDoc, string viewName, string email, string bucket)
        {
            CustomerDAC customerDAC = new CustomerDAC(bucketSmoovPOS);
            IView<Customer> listCustomer = null;
            listCustomer = customerDAC.GetAll(designDoc, viewName).Key(new string[] { bucket, email });
            Customer cus = null;
            if (listCustomer.Count() >= 1)
            {
                cus = listCustomer.FirstOrDefault();
            }
            return cus;
        }

        /// <summary>
        /// Check email and pinCode to reset password
        /// </summary>
        /// <param>string email</param>
        /// <returns></returns>
        /// <createdby>hung.do</createdby>
        /// <createddate>26/12/2014-10:07 PM</createddate>
        /// 
        public Customer CheckEmailAndPinCode(string designDoc, string viewName, string email,string pinCode, string bucket)
        {
            CustomerDAC customerDAC = new CustomerDAC(bucketSmoovPOS);
            IView<Customer> listCustomer = null;
            listCustomer = customerDAC.GetAll(designDoc, viewName).Key(new string[] { bucket, email, pinCode });
            Customer cus = null;
            if (listCustomer.Count() == 1)
            {
                cus = listCustomer.FirstOrDefault();
                return cus;
            }
            else
            {
                return cus;
            }
           
        }

    }
}
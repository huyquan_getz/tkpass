﻿using Com.SmoovPOS.Model;
using SmoovPOS.Common;
using SmoovPOS.DataSql;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class ServicePackageComponent
    {
        /// <summary>
        /// Gets all Service Package.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/13/2015-3:17 PM</datetime>
        public IEnumerable<ServicePackageModel> GetAllServicePacket()
        {
            using (var db = new EntitiesDataContext())
            {
                var results = (from s in db.ServicePackages
                               select new ServicePackageModel
                               {
                                   servicePacketID = s.servicePacketID,
                                   servicePacketName = s.servicePacketName,
                                   description = s.description,
                                   servicePacketStartDate = s.servicePacketStartDate,
                                   servicePacketExpiredDate = s.servicePacketExpiredDate,
                                   status = s.status,
                                   bussinessPlan = s.bussinessPlan,
                                   userOwner = s.userOwner,
                                   display = s.display
                               }).ToList();

                return results;
            }
        }
        public IEnumerable<BillingCycleModel> GetAllBillingCycle()
        {
            using (var db = new EntitiesDataContext())
            {
                var results = (from s in db.BillingCycles
                               select new BillingCycleModel
                               {
                                   billingCycleID = s.billingCycleID,
                                   paymentCycleName = s.paymentCycleName,
                                   number = s.number,
                                   userOwner = s.userOwner,
                                   createdDate = s.createdDate,
                                   modifyUser = s.modifyUser,
                                   modifyDate = s.modifyDate,
                                   display = s.display
                               }).ToList();

                return results;
            }
        }
        public BillingCycleModel GetBillingCycle(Guid id)
        {
            using (var db = new EntitiesDataContext())
            {
                var results = (from s in db.BillingCycles
                               where s.billingCycleID == id
                               select new BillingCycleModel
                               {
                                   billingCycleID = s.billingCycleID,
                                   paymentCycleName = s.paymentCycleName,
                                   number = s.number,
                                   userOwner = s.userOwner,
                                   createdDate = s.createdDate,
                                   modifyUser = s.modifyUser,
                                   modifyDate = s.modifyDate,
                                   display = s.display
                               }).FirstOrDefault();

                return results;
            }
        }
        /// <summary>
        /// Gets the Service Package.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/13/2015-3:17 PM</datetime>
        public ServicePackageModel GetServicePacket(Guid id)
        {
            using (var db = new EntitiesDataContext())
            {
                var result = (from m in db.ServicePackages
                              where m.servicePacketID.Equals(id)
                              select m).FirstOrDefault();

                if (result != null)
                {
                    var model = new ServicePackageModel();

                    model.servicePacketID = result.servicePacketID;
                    model.servicePacketName = result.servicePacketName;
                    model.description = result.description;
                    model.servicePacketStartDate = result.servicePacketStartDate;
                    model.servicePacketExpiredDate = result.servicePacketExpiredDate;
                    model.status = result.status;
                    model.bussinessPlan = result.bussinessPlan;

                    model.isTrial = result.isTrial.GetValueOrDefault();
                    model.trialDays = result.trialDays.GetValueOrDefault();
                    model.image = result.image;

                    model.userOwner = result.userOwner;

                    model.createdDate = result.createdDate;
                    model.modifyUser = result.modifyUser;
                    model.display = result.display;

                    model.BillingServicePackets = new List<BillingServicePackageModel>();
                    foreach (var item in result.BillingServicePackages)
                    {
                        var bill = SetValueBillingCycle(db, item);

                        model.BillingServicePackets.Add(bill);
                    }

                    model.FunctionServicePackets = new List<FunctionServicePackageModel>();
                    foreach (var item in result.FunctionServicePackages)
                    {
                        var func = SetValueFunction(db, item);

                        model.FunctionServicePackets.Add(func);
                    }

                    AddFunctionToService(db, model, true);

                    model.NoOfMerchants = result.Merchants.Where(m => m.display).Count();

                    return model;
                }
                else
                {
                    return null;
                }
            }
        }
        public ServicePackageModel GetServicePacket(string bucket)
        {
            using (var db = new EntitiesDataContext())
            {
                var component = new MerchantManagementComponent();
                var merchant = component.GetMerchantManagementBySiteId(bucket);
                if (merchant == null && merchant.servicePacketID == null)
                {
                    return null;
                }

                var model = GetServicePacket(merchant.servicePacketID.GetValueOrDefault());

                return model;
            }
        }

        private void AddFunctionToService(EntitiesDataContext db, ServicePackageModel model, bool isInsert)
        {
            var results = (from a in db.FunctionMethods
                           where a.isAddOns == true
                           select a).ToList();
            if (results.Count <= 0)
            {
                string name = ConstantSmoovs.AddOns_name.QueueManagement;
                string key = ConstantSmoovs.AddOns_Key.QUEUEMANAGEMENT;
                DateTime createDate = DateTime.UtcNow.AddMinutes(-6);
                AddFunctionMethod(db, model, name, key, createDate);

                name = ConstantSmoovs.AddOns_name.TableOrdering;
                key = ConstantSmoovs.AddOns_Key.TABLEORDERING;
                createDate = createDate.AddMinutes(1);
                AddFunctionMethod(db, model, name, key, createDate);

                name = ConstantSmoovs.AddOns_name.MembershipFee;
                key = ConstantSmoovs.AddOns_Key.MEMBERSHIPFEE;
                createDate = createDate.AddMinutes(1);
                AddFunctionMethod(db, model, name, key, createDate);

                name = ConstantSmoovs.AddOns_name.OnlineShop;
                key = ConstantSmoovs.AddOns_Key.ONLINESHOP;
                createDate = createDate.AddMinutes(1);
                AddFunctionMethod(db, model, name, key, createDate);

                name = ConstantSmoovs.AddOns_name.POSDevice;
                key = ConstantSmoovs.AddOns_Key.POSDEVICE;
                createDate = createDate.AddMinutes(1);
                AddFunctionMethod(db, model, name, key, createDate);

                name = ConstantSmoovs.AddOns_name.Database;
                key = ConstantSmoovs.AddOns_Key.DATABASE;
                createDate = createDate.AddMinutes(1);
                AddFunctionMethod(db, model, name, key, createDate);
            }

            results = (from a in db.FunctionMethods
                       where a.isAddOns == true && a.keyMethod == ConstantSmoovs.AddOns_Key.MAINPOSAPP
                       select a).ToList();
            if (results.Count <= 0)
            {
                var createDate = (from a in db.FunctionMethods
                                  where a.isAddOns == true
                                  select a).Min(x => x.createdDate);

                string name = ConstantSmoovs.AddOns_name.MainPOSApp;
                string key = ConstantSmoovs.AddOns_Key.MAINPOSAPP;
                createDate = createDate.AddMinutes(-1);
                AddFunctionMethod(db, model, name, key, createDate);
            }
            //var listBills = new List<BillingServicePackage>();
            //if (isInsert)
            //{
            //    var lstBill = model.BillingServicePackets.Select(b => b.billingCycleID).ToList();
            //    var resultBill = (from a in db.BillingCycles
            //                      where !lstBill.Contains(a.billingCycleID) && a.display
            //                      select a).ToList();
            //    foreach (var item in resultBill)
            //    {
            //        var bill = new BillingServicePackage();

            //        bill.billingServicePacketID = Guid.NewGuid();

            //        bill.billingCycleID = item.billingCycleID;
            //        bill.servicePacketID = model.servicePacketID;
            //        bill.moneyType = ConstantSmoovs.BillingCycle.SGD;
            //        bill.moneyPerUnit = 0;
            //        bill.discount = 0;
            //        bill.userOwner = model.userOwner;
            //        bill.createdDate = DateTime.UtcNow;
            //        bill.modifyUser = bill.userOwner;
            //        bill.modifyDate = DateTime.UtcNow;
            //        bill.display = true;

            //        listBills.Add(bill);
            //    }
            //    if (listBills.Count > 0)
            //    {
            //        if (isInsert)
            //        {
            //            db.BillingServicePackages.InsertAllOnSubmit(listBills);
            //        }
            //        foreach (var item in listBills)
            //        {
            //            var bill = SetValueBillingCycle(db, item);

            //            model.BillingServicePackets.Add(bill);
            //        }
            //    }
            //}

            var lstFunc = model.FunctionServicePackets.Select(b => b.functionMethodID).ToList();
            var resultFunc = (from a in db.FunctionMethods
                              where !lstFunc.Contains(a.functionMethodID) && a.display
                              orderby a.createdDate
                              select a).ToList();
            var listFunc = new List<FunctionServicePackage>();
            foreach (var item in resultFunc)
            {
                var func = new FunctionServicePackage();

                func.functionServiceID = Guid.NewGuid();
                func.functionMethodID = item.functionMethodID;
                func.servicePacketID = model.servicePacketID;
                func.limit = 0;
                func.isUnlimit = false;
                func.isSelect = true;
                func.userOwner = model.userOwner;

                if (item.isAddOns.GetValueOrDefault())
                {
                    func.createdDate = item.createdDate;
                }
                else
                {
                    func.createdDate = DateTime.UtcNow;
                }
                func.modifyUser = func.userOwner;
                func.modifyDate = DateTime.UtcNow;
                func.display = true;

                listFunc.Add(func);
            }
            if (listFunc.Count > 0)
            {
                if (isInsert)
                {
                    db.FunctionServicePackages.InsertAllOnSubmit(listFunc);
                }
                foreach (var item in listFunc)
                {
                    var func = SetValueFunction(db, item);

                    model.FunctionServicePackets.Add(func);
                }
            }

            if (isInsert && listFunc.Count > 0)
            {
                db.SubmitChanges();
            }
        }

        private void AddFunctionMethod(EntitiesDataContext db, ServicePackageModel model, string name, string key, DateTime createDate)
        {
            var func = new FunctionMethod();

            func.functionMethodID = Guid.NewGuid();
            func.name = name;
            func.keyMethod = key;
            func.isDatabaseFunction = key.Equals(ConstantSmoovs.AddOns_Key.DATABASE);
            func.dataType = key.Equals(ConstantSmoovs.AddOns_Key.DATABASE) ? "MB" : string.Empty;
            func.description = name;
            func.userOwner = model.userOwner;
            func.createdDate = createDate;
            func.modifyUser = func.userOwner;
            func.modifyDate = createDate;
            func.display = true;

            func.isAddOns = true;

            db.FunctionMethods.InsertOnSubmit(func);
            db.SubmitChanges();
        }

        private FunctionServicePackageModel SetValueFunction(EntitiesDataContext db, FunctionServicePackage item)
        {
            var func = new FunctionServicePackageModel();
            func.functionServiceID = item.functionServiceID;
            func.functionMethodID = item.functionMethodID;
            func.servicePacketID = item.servicePacketID;
            func.limit = item.limit;
            func.isSelect = item.isSelect.GetValueOrDefault();
            func.isUnlimit = item.isUnlimit.GetValueOrDefault();

            func.userOwner = item.userOwner;
            func.createdDate = item.createdDate;
            func.modifyUser = item.modifyUser;
            func.modifyDate = item.modifyDate;
            func.display = item.display;

            var functionMethod = item.FunctionMethod;
            if (functionMethod == null && item.functionMethodID != null && item.functionMethodID != Guid.Empty)
            {
                functionMethod = (from f in db.FunctionMethods
                                  where f.functionMethodID == item.functionMethodID
                                  select f).FirstOrDefault();
            }

            if (functionMethod != null)
            {
                func.FunctionName = functionMethod.name;
                func.keyMethod = functionMethod.keyMethod;
                func.isAddOns = functionMethod.isAddOns.GetValueOrDefault();
                func.isDatabaseFunction = functionMethod.isDatabaseFunction;
                func.dataType = functionMethod.dataType;
            }

            return func;
        }

        private BillingServicePackageModel SetValueBillingCycle(EntitiesDataContext db, BillingServicePackage item)
        {
            var bill = new BillingServicePackageModel();
            bill.billingCycleID = item.billingCycleID;
            bill.billingServicePacketID = item.billingServicePacketID;
            bill.servicePacketID = item.servicePacketID;
            bill.discount = item.discount;
            bill.moneyPerUnit = item.moneyPerUnit;
            bill.moneyType = item.moneyType;

            bill.userOwner = item.userOwner;
            bill.createdDate = item.createdDate;
            bill.modifyUser = item.modifyUser;
            bill.modifyDate = item.modifyDate;
            bill.display = item.display;

            var billingCycle = item.BillingCycle;
            if (billingCycle == null && item.billingCycleID != null && item.billingCycleID != Guid.Empty)
            {
                billingCycle = (from b in db.BillingCycles
                                where b.billingCycleID == item.billingCycleID
                                select b).FirstOrDefault();
            }

            if (billingCycle != null)
            {
                bill.number = billingCycle.number;
                bill.paymentCycleName = billingCycle.paymentCycleName;
            }

            return bill;
        }
        public SearchServicePackageModel GetServicePacket(SearchServicePackageModel searchModel)
        {
            using (var db = new EntitiesDataContext())
            {
                var results = from s in db.ServicePackages
                              select new ServicePackageModel
                              {
                                  servicePacketID = s.servicePacketID,
                                  servicePacketName = s.servicePacketName,
                                  description = s.description,
                                  servicePacketStartDate = s.servicePacketStartDate,
                                  servicePacketExpiredDate = s.servicePacketExpiredDate,
                                  status = s.status,
                                  bussinessPlan = s.bussinessPlan,
                                  userOwner = s.userOwner,
                                  createdDate = s.createdDate,
                                  modifyUser = s.modifyUser,
                                  modifyDate = s.modifyDate,
                                  display = s.display,
                                  NoOfMerchants = s.Merchants.Where(m => m.display).Count(),
                                  //dataDisplay = (m.FunctionServicePackages != null && m.FunctionServicePackages.Where(f => f.FunctionMethod.isDatabaseFunction).Any() ? m.FunctionServicePackages.Where(f => f.FunctionMethod.isDatabaseFunction).FirstOrDefault().limit.ToString() : ConstantSmoovs.BillingCycle.Empty),
                                  //billingServicePacketModel = (s.BillingServicePackages != null && s.BillingServicePackages.Where(b => b.BillingCycle.paymentCycleName.Equals(ConstantSmoovs.BillingCycle.Monthly)).Any() ?
                                  //            (from b in s.BillingServicePackages.Where(b => b.BillingCycle.paymentCycleName.Equals(ConstantSmoovs.BillingCycle.Monthly))
                                  //             select new BillingServicePackageModel
                                  //             {
                                  //                 moneyType = b.moneyType,
                                  //                 moneyPerUnit = b.moneyPerUnit
                                  //             }
                                  //            ).FirstOrDefault()
                                  //      : null)
                              };

                //only show record display
                results = results.Where(x => x.display == true);
                if (!string.IsNullOrEmpty(searchModel.name))
                {
                    var searchValue = searchModel.name.Trim();
                    results = results.Where(x => x.servicePacketName.ToUpper().Contains(searchValue.Trim().ToUpper()));
                }
                //get count total
                searchModel.countAll = results.Count();

                searchModel.ListServicePackages = new List<ServicePackageModel>();

                if (searchModel.countAll > 0)
                {
                    //sort
                    if (!string.IsNullOrEmpty(searchModel.sort))
                    {
                        //sort by full name
                        switch (searchModel.sort)
                        {
                            case SmoovPOS.Common.ConstantSmoovs.FunctionMethod.name:
                                if (searchModel.desc)
                                {
                                    results = results.OrderByDescending(x => x.servicePacketName);
                                }
                                else
                                {
                                    results = results.OrderBy(x => x.servicePacketName);
                                }
                                break;
                            case SmoovPOS.Common.ConstantSmoovs.FunctionMethod.status:
                                if (searchModel.desc)
                                {
                                    results = results.OrderByDescending(x => x.status);
                                }
                                else
                                {
                                    results = results.OrderBy(x => x.status);
                                }
                                break;
                            case SmoovPOS.Common.ConstantSmoovs.FunctionMethod.createddate:
                                if (searchModel.desc)
                                {
                                    results = results.OrderByDescending(x => x.createdDate);
                                }
                                else
                                {
                                    results = results.OrderBy(x => x.createdDate);
                                }
                                break;
                            case SmoovPOS.Common.ConstantSmoovs.FunctionMethod.noofmerchants:
                                if (searchModel.desc)
                                {
                                    results = results.OrderByDescending(x => x.NoOfMerchants);
                                }
                                else
                                {
                                    results = results.OrderBy(x => x.NoOfMerchants);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        //sort default
                        results = results.OrderByDescending(x => x.modifyDate);
                    }

                    //paging
                    searchModel.ListServicePackages = results.Skip((searchModel.pageGoTo - 1) * searchModel.limit).Take(searchModel.limit).ToList();

                    searchModel.pageCurrent = searchModel.pageGoTo;//set page current
                }

                return searchModel;
            }
        }
        public bool ExistServicePacketByName(string name, Guid? id)
        {
            using (var db = new EntitiesDataContext())
            {
                var isExist = false;
                if (id != null)
                {
                    isExist = (from m in db.ServicePackages
                               where m.servicePacketName.ToUpper().Equals(name.Trim().ToUpper()) && (m.servicePacketID != id.Value) && m.display == true
                               select m).Any();

                }
                else
                {
                    isExist = (from m in db.ServicePackages
                               where m.servicePacketName.ToUpper().Equals(name.Trim().ToUpper()) && m.display == true
                               select m).Any();
                }
                return isExist;
            }
        }
        public ServicePackageModel NewServicePacket(string userOwner)
        {
            using (var db = new EntitiesDataContext())
            {
                var model = new ServicePackageModel();

                model.servicePacketName = string.Empty;
                model.description = string.Empty;
                model.servicePacketStartDate = DateTime.Now;
                model.servicePacketExpiredDate = DateTime.Now;
                model.status = true;
                model.bussinessPlan = string.Empty;

                model.display = true;

                model.userOwner = userOwner;

                model.BillingServicePackets = new List<BillingServicePackageModel>();
                model.FunctionServicePackets = new List<FunctionServicePackageModel>();

                AddFunctionToService(db, model, false);

                return model;

            }
        }
        public Guid? CreateServicePacket(ServicePackageModel result)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = new ServicePackage();

                    //general Id
                    if (model.servicePacketID == null || model.servicePacketID == Guid.Empty)
                    {
                        model.servicePacketID = Guid.NewGuid();
                    }
                    else
                    {
                        model.servicePacketID = result.servicePacketID;
                    }
                    result.servicePacketID = model.servicePacketID;

                    model.servicePacketName = result.servicePacketName;
                    model.description = result.description;
                    //model.servicePacketStartDate = result.servicePacketStartDate;
                    //model.servicePacketExpiredDate = result.servicePacketExpiredDate;
                    model.status = result.status;
                    //model.bussinessPlan = result.bussinessPlan;

                    model.isTrial = result.isTrial;
                    model.trialDays = result.trialDays;
                    model.image = result.image;

                    model.userOwner = result.userOwner;//user create
                    model.createdDate = DateTime.UtcNow;//default
                    model.modifyUser = result.userOwner;
                    model.modifyDate = DateTime.UtcNow;
                    model.display = true;//default

                    db.ServicePackages.InsertOnSubmit(model);

                    AddFunctionAndBillingCycle(db, result);

                    db.SubmitChanges();

                    return model.servicePacketID;
                }

            }
            catch
            {
            }

            return null;
        }

        public bool UpdateServicePacket(ServicePackageModel result)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = (from m in db.ServicePackages
                                 where m.servicePacketID.Equals(result.servicePacketID)
                                 select m).FirstOrDefault();

                    if (model != null)
                    {

                        model.servicePacketName = result.servicePacketName;
                        model.description = result.description;
                        //model.servicePacketStartDate = result.servicePacketStartDate;
                        //model.servicePacketExpiredDate = result.servicePacketExpiredDate;
                        //model.status = result.status;
                        //model.bussinessPlan = result.bussinessPlan;
                        result.userOwner = model.userOwner;

                        model.modifyUser = result.modifyUser;//user update
                        model.modifyDate = DateTime.UtcNow;//default

                        model.isTrial = result.isTrial;
                        model.trialDays = result.trialDays;
                        model.image = result.image;

                        db.SubmitChanges();

                        db.BillingServicePackages.DeleteAllOnSubmit(model.BillingServicePackages);
                        db.FunctionServicePackages.DeleteAllOnSubmit(model.FunctionServicePackages);

                        AddFunctionAndBillingCycle(db, result);

                        db.SubmitChanges();
                        return true;
                    }
                }

            }
            catch
            {
            }
            return false;
        }

        private void AddFunctionAndBillingCycle(EntitiesDataContext db, ServicePackageModel model)
        {
            var listBills = new List<BillingServicePackage>();
            foreach (var item in model.BillingServicePackets)
            {
                var bill = new BillingServicePackage();
                if (item.servicePacketID == null || item.servicePacketID == Guid.Empty)
                {
                    bill.billingServicePacketID = Guid.NewGuid();
                    item.billingServicePacketID = bill.billingServicePacketID;
                }
                else
                {
                    bill.billingServicePacketID = item.billingServicePacketID;
                }


                bill.billingCycleID = item.billingCycleID;
                bill.servicePacketID = model.servicePacketID;
                bill.moneyType = ConstantSmoovs.BillingCycle.SGD;
                bill.moneyPerUnit = item.moneyPerUnit;
                bill.discount = item.discount;
                bill.userOwner = model.userOwner;
                bill.createdDate = DateTime.UtcNow;
                bill.modifyUser = bill.userOwner;
                bill.modifyDate = DateTime.UtcNow;
                bill.display = true;

                listBills.Add(bill);
            }
            if (listBills.Count > 0)
            {
                db.BillingServicePackages.InsertAllOnSubmit(listBills);
            }

            var listFunc = new List<FunctionServicePackage>();
            foreach (var item in model.FunctionServicePackets)
            {
                var func = new FunctionServicePackage();
                if (item.servicePacketID == null || item.servicePacketID == Guid.Empty)
                {
                    func.functionServiceID = Guid.NewGuid();
                    item.functionServiceID = func.functionServiceID;
                }
                else
                {
                    func.functionServiceID = item.functionServiceID;
                }

                func.functionMethodID = item.functionMethodID;
                func.servicePacketID = model.servicePacketID;
                func.limit = item.limit;
                func.isUnlimit = item.isUnlimit;
                func.isSelect = item.isSelect;
                func.userOwner = model.userOwner;
                func.createdDate = item.createdDate;
                func.modifyUser = func.userOwner;
                func.modifyDate = DateTime.UtcNow;
                func.display = true;

                listFunc.Add(func);
            }
            if (listFunc.Count > 0)
            {
                db.FunctionServicePackages.InsertAllOnSubmit(listFunc);
            }
        }
        public bool ChangeStatus(Guid id, bool status)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = (from m in db.ServicePackages
                                 where m.servicePacketID.Equals(id)
                                 select m).FirstOrDefault();

                    if (model != null)
                    {
                        model.status = status;

                        //model.userOwner = (new Common.Controllers.UserSession()).UserId;//user create
                        //model.createdDate = DateTime.Now;//default
                        model.modifyUser = (new Common.Controllers.UserSession()).UserId;//user update
                        model.modifyDate = DateTime.Now;//default
                        //model.display = true;//default

                        db.SubmitChanges();

                        return true;
                    }
                }

            }
            catch
            {
            }
            return false;
        }

        public List<string> GetListMerchantUsingServicePackage(string service_package_id, DateTime dtUTC)
        {
            List<string> listMerchant = new List<string>();
            using (var db = new EntitiesDataContext())
            {
                var result = from mer in db.Merchants
                             join spk in db.ServicePackages on mer.servicePacketID equals spk.servicePacketID
                             where mer.servicePacketID.Value.ToString() == service_package_id
                             && mer.servicePacketStartDate.Value <= dtUTC
                             && mer.servicePacketEndDate.Value > dtUTC
                             && mer.status == true
                             select new { spk.servicePacketName };
                if (result != null) listMerchant = result.Select(r => r.servicePacketName).ToList();
            }
            return listMerchant;
        }
    }
}
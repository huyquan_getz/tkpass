﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class DeliverySettingComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public bool CreateNewDeliverySetting(DeliverySetting deliverySetting, string bucket)
        {
            deliverySetting.bucket = bucket;
            DeliverySettingDAC deliverySettingDAC = new DeliverySettingDAC(bucketSmoovPOS);
            int StatusCode = deliverySettingDAC.CreateDAC(deliverySetting);
            return (StatusCode == 0);
        }
        public bool UpdateDeliverySetting(DeliverySetting deliverySetting, string bucket)
        {
            DeliverySettingDAC deliverySettingDAC = new DeliverySettingDAC(bucketSmoovPOS);
            int StatusCode = deliverySettingDAC.UpdateDAC(deliverySetting);
            return (StatusCode == 0);
        }
        public bool DeleteDeliverySetting(string id, string bucket)
        {
            DeliverySettingDAC deliverySettingDAC = new DeliverySettingDAC(bucketSmoovPOS);
            int StatusCode = deliverySettingDAC.DeleteDAC(id);
            return (StatusCode == 0);
        }
        public DeliverySetting DetailDeliverySetting(string id, string bucket)
        {
            DeliverySettingDAC deliverySettingDAC = new DeliverySettingDAC(bucketSmoovPOS);
            DeliverySetting deliverySetting = deliverySettingDAC.DetailDAC(id);
            return deliverySetting;
        }
        public IEnumerable<DeliverySetting> GetAllDeliverySetting(string designDoc, string viewName, string bucket)
        {
            DeliverySettingDAC deliverySettingDAC = new DeliverySettingDAC(bucketSmoovPOS);
            IEnumerable<DeliverySetting> listDeliverySetting = deliverySettingDAC.GetAllDeliverySettingDAC(designDoc, viewName).Key(bucket);// DeliverySettingDAC.GetAllDeliverySettingDAC(designDoc, viewName);

            return listDeliverySetting.OrderByDescending(d => d.createdDate).ToList();
        }
        public DeliverySetting GetDeliverySettingByCountry(string designDoc, string viewName, string bucket, string country)
        {
            DeliverySettingDAC deliverySettingDAC = new DeliverySettingDAC(bucketSmoovPOS);
            DeliverySetting deliverySetting = null;

            var deliverySettings = deliverySettingDAC.GetAllDeliverySettingDAC(designDoc, viewName).Key(new string[] { bucket, country }).ToList();
            if (deliverySettings != null && deliverySettings.Count > 0)
            {
                //deliverySetting = deliverySettings.Where(d => d.country.country.ToUpper() == country.ToUpper()).FirstOrDefault();
                deliverySetting = deliverySettings.FirstOrDefault();
            }

            return deliverySetting;
        }
    }
}
﻿using SmoovPOS.Common;
using SmoovPOS.Data.DAC.Settings;
using SmoovPOS.Entity.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components.Settings
{
    public class CheckoutSettingComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateCheckoutSetting(CheckoutSetting checkoutSetting, string bucket)
        {
            checkoutSetting.bucket = bucket;
            CheckoutSettingDAC checkoutSettingDAC = new CheckoutSettingDAC(bucketSmoovPOS);
            int statusCode = checkoutSettingDAC.CreateDAC(checkoutSetting);
            return statusCode;
        }

        public int UpdateCheckoutSetting(CheckoutSetting checkoutSetting, string bucket)
        {
            checkoutSetting.bucket = bucket;
            CheckoutSettingDAC checkoutSettingDAC = new CheckoutSettingDAC(bucketSmoovPOS);
            int statusCode = checkoutSettingDAC.UpdateDAC(checkoutSetting);
            return statusCode;
        }

        public CheckoutSetting GetDetailCheckoutSetting(string designDoc, string viewName, string bucket)
        {
            CheckoutSettingDAC checkoutSettingDAC = new CheckoutSettingDAC(bucketSmoovPOS);
            CheckoutSetting checkoutSetting = checkoutSettingDAC.GetAll(designDoc, viewName).Key(bucket).FirstOrDefault();

            return checkoutSetting;
        }
    }
}
﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Entity.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmoovPOS.Business.Components
{
    public class SMSSettingComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateSMSSetting(SMSSetting _SMSSetting, string bucket)
        {
            _SMSSetting.bucket = bucket;
            SMSSettingDAC SMSSettingDAC = new SMSSettingDAC(bucketSmoovPOS);
            int StatusCode = SMSSettingDAC.CreateDAC(_SMSSetting);
            return StatusCode;
        }

        public IView<SMSSetting> GetAllSMSSettings(string bucket)
        {
            SMSSettingDAC SMSSettingDAC = new SMSSettingDAC(bucketSmoovPOS);
            IView<SMSSetting> list = SMSSettingDAC.GetAll(ConstantSmoovs.CouchBase.DesigndocSMSSetting, ConstantSmoovs.CouchBase.GetAllSMSSetting).Key(bucket);
            return list;
        }

        public SMSSetting GetDetailSMSSetting(string id)
        {
            SMSSettingDAC SMSSettingDAC = new SMSSettingDAC(bucketSmoovPOS);
            SMSSetting _SMSSetting = SMSSettingDAC.GetDetailDAC(id);
            return _SMSSetting;
        }

        public int UpdateSMSSetting(SMSSetting _SMSSetting, string bucket)
        {
            _SMSSetting.bucket = bucket;
            SMSSettingDAC SMSSettingDAC = new SMSSettingDAC(bucketSmoovPOS);
            int StatusCode = SMSSettingDAC.UpdateDAC(_SMSSetting);
            return StatusCode;
        }

        public int DeleteSMSSetting(string id)
        {
            SMSSettingDAC SMSSettingDAC = new SMSSettingDAC(bucketSmoovPOS);
            int StatusCode = SMSSettingDAC.DeleteDAC(id);
            return StatusCode;
        }
    }
}
﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Business.Components
{
    public class RoleComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
         static RoleComponent() { }
         public int CreateNewRole(Role role, string bucket)
        {
            role.bucket = bucket;
            RoleDAC roleDAC = new RoleDAC(bucketSmoovPOS);
            int statusCode = roleDAC.CreateDAC(role);
            return statusCode;
        }
         public IView<Role> GetAllRole(string designDoc, string viewName, string bucket)
        {
            RoleDAC roleDAC = new RoleDAC(bucketSmoovPOS);
            IView<Role> listRole = null;
            listRole = roleDAC.GetAllRoles(designDoc, viewName).Key(bucket);
            return listRole;
        }
         public Role DetailRole(string id, string bucket)
        {
            RoleDAC roleDAC = new RoleDAC(bucketSmoovPOS);
            Role role = roleDAC.detailDAC(id);
            return role;
        }
    }
}
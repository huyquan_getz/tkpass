﻿using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using SmoovPOS.Entity.Models;
using System.Collections.Generic;
using System.Linq;

namespace SmoovPOS.Business.Components
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>1/30/2015-5:23 PM</datetime>
    public class CollectionComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        static CollectionComponent() { }

        /// <summary>
        /// Creates the new collection.
        /// </summary>
        /// <param name="Collection">The collection.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public int CreateNewCollection(Collection collection, string bucket)
        {
            collection.bucket = bucket;
            CollectionDAC collectionDAC = new CollectionDAC(bucketSmoovPOS);
            int StatusCode = collectionDAC.CreateDAC(collection);
            return StatusCode;
        }
     
        /// <summary>
        /// Updates the collection.
        /// </summary>
        /// <param name="Collection">The collection.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public int UpdateCollection(Collection collection, string bucket)
        {
            collection.bucket = bucket;
            CollectionDAC collectionDAC = new CollectionDAC(bucketSmoovPOS);
            int StatusCode = collectionDAC.UpdateDAC(collection);
            return StatusCode;
        }
        /// <summary>
        /// Deletes the collection.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public int DeleteCollection(string id, string bucket)
        {
            CollectionDAC collectionDAC = new CollectionDAC(bucketSmoovPOS);
            int StatusCode = collectionDAC.DeleteDAC(id);
            return StatusCode;
        }
        /// <summary>
        /// Details the collection.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public Collection DetailCollection(string id, string bucket)
        {
            CollectionDAC collectionDAC = new CollectionDAC(bucketSmoovPOS);
            Collection Collection = collectionDAC.DetailDAC(id);
            return Collection;
        }

        /// <summary>
        /// Gets all collection.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public IEnumerable<Collection> GetAllCollection(string designDoc, string viewName, string bucket)
        {
            CollectionDAC collectionDAC = new CollectionDAC(bucketSmoovPOS);
            IEnumerable<Collection> listCollection = collectionDAC.GetAllCollectionDAC(designDoc, viewName, bucket);

            return listCollection;
        }

        /// <summary>
        /// Deletes all collection.
        /// </summary>
        /// <param name="designDoc">The design document.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:23 PM</datetime>
        public int DeleteAllCollection(string designDoc, string viewName, string bucket)
        {
            CollectionDAC collectionDAC = new CollectionDAC(bucketSmoovPOS);
            return collectionDAC.DeleteAllCollectionDAC(designDoc, viewName, bucket);
        }

    }
}
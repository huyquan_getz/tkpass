﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using SmoovPOS.Entity;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class InventoryComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateInventory(Inventory inventory, string bucket)
        {
            inventory.bucket = bucket;
            InventoryDAC inventoryDAC = new InventoryDAC(bucketSmoovPOS);
            int StatusCode = inventoryDAC.CreateDAC(inventory);
            return StatusCode;
        }
        public int UpdateInventory(Inventory inventory, string bucket)
        {
            inventory.bucket = bucket;
            InventoryDAC inventoryDAC = new InventoryDAC(bucketSmoovPOS);
            int StatusCode = inventoryDAC.UpdateDAC(inventory);
            return StatusCode;
        }
        public int DeleteInventory(string id, string bucket)
        {
            InventoryDAC inventoryDAC = new InventoryDAC(bucketSmoovPOS);
            int StatusCode = inventoryDAC.DeleteDAC(id);
            return StatusCode;
        }
        public Inventory DetailInventory(string id, string bucket)
        {
            InventoryDAC inventoryDAC = new InventoryDAC(bucketSmoovPOS);
            Inventory inventory = inventoryDAC.DetailDAC(id);
            return inventory;
        }
        public SearchInventoryModel GetAllInventory(string designDoc, string viewName, string bucket, NewPaging paging)
        {
            InventoryDAC inventoryDAC = new InventoryDAC(bucketSmoovPOS);
            IEnumerable<Inventory> listSearch = null;
            listSearch = inventoryDAC.GetAllDACWithBucket(designDoc, viewName, bucket);
            SearchInventoryModel model = new SearchInventoryModel();
            model.Paging = new NewPaging();
            if (listSearch != null)
            {
                if (paging.type == "title")
                {
                    listSearch = listSearch.Where(m => m.businessID.ToUpper().Contains(paging.text.ToUpper()));
                }           

                if (paging.sort == "index")
                {
                    if (paging.desc)
                    {
                        listSearch = listSearch.OrderByDescending(m => m.index);
                    }
                    else
                    {
                        listSearch = listSearch.OrderBy(m => m.index);
                    }
                }
                else if (paging.sort == "title")
                {

                    if (paging.desc)
                    {
                        listSearch = listSearch.OrderByDescending(m => m.businessID);
                    }
                    else
                    {
                        listSearch = listSearch.OrderBy(m => m.businessID);
                    }
                }

                model.Paging.total = listSearch.Count() - 1;//Don't count inventory Master
                model.Paging.pageGoTo = paging.pageGoTo;
                model.Paging.limit = paging.limit;
                model.Paging.text = paging.text;
                model.Paging.sort = paging.sort;
                model.Paging.desc = paging.desc;
                model.Paging.type = paging.type;
                listSearch = listSearch.Skip((paging.pageGoTo - 1) * paging.limit).Take(paging.limit).ToList();
                model.Inventories = listSearch;
                return model;
            }
            else
            {
                return model;
            }  
        }
        public int CountAllInventory(string designDoc, string viewName, string bucket)
        {
            InventoryDAC inventoryDAC = new InventoryDAC(bucketSmoovPOS);
            IView<Inventory> ListInventory = null;
            ListInventory = inventoryDAC.GetAllDAC(designDoc, ConstantSmoovs.CouchBase.GetAllInventories);
            return ListInventory.Count();
        }
        public IView<Country> GetAllCountry(string designDoc, string viewName, string bucket)
        {
            CountryDAC countryDAC = new CountryDAC(bucketSmoovPOS);
            IView<Country> listCountry = null;
            listCountry = countryDAC.GetAllCountry(designDoc, ConstantSmoovs.CouchBase.GetAllCountry);
            return listCountry;
        }

        public IView<Inventory> GetAll(string designDoc, string viewName, string bucket)
        {
            InventoryDAC inventoryDAC = new InventoryDAC(bucketSmoovPOS);
            IView<Inventory> listInventory = null;
            listInventory = inventoryDAC.GetAllDACWithBucket(designDoc, viewName, bucket);
            return listInventory;
        }
    }
}
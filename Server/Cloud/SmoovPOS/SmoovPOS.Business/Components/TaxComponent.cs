﻿using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using SmoovPOS.Data.DAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Business.Components
{
    public class TaxComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        static TaxComponent() { }
        public int CreateNewRole(Tax tax, string bucket)
        {
            tax.bucket = bucket;
            TaxDAC taxDAC = new TaxDAC(bucketSmoovPOS);
            int statusCode = taxDAC.CreateDAC(tax);
            return statusCode;
        }
        public IView<Tax> GetAllTax(string designDoc, string viewName, string bucket)
        {
            TaxDAC taxDAC = new TaxDAC(bucketSmoovPOS);
            IView<Tax> listTax = null;
            listTax = taxDAC.GetTax(designDoc, viewName).Key(bucket);
            return listTax;
        }
        public Tax DetailTax(string id, string bucket)
        {
            TaxDAC taxDAC = new TaxDAC(bucketSmoovPOS);
            Tax tax = taxDAC.detailDAC(id);
            return tax;
        }
        public IEnumerable<TaxItem> GetTaxWithInventory(string designDoc, string viewName, string bucket, string inventoryId)
        {
            IView<TaxItem> listTaxs = null;
            TaxItemDAC taxDAC = new TaxItemDAC(bucketSmoovPOS);
            try
            {
                listTaxs = taxDAC.GetTaxItem(designDoc, viewName);
                listTaxs.Key(new string[] { bucket, inventoryId });
            }
            catch
            {
                return listTaxs;
            }
            return listTaxs;
        }
        public List<TaxItem> GetTaxWithStoreId(string designDoc, string viewName, string bucket, int inventoryId)
        {
            IView<Tax> listTax = null;
            List<TaxItem> listTaxItem = new List<TaxItem>();
            TaxDAC taxDAC = new TaxDAC(bucketSmoovPOS);
            try
            {
                listTax = taxDAC.GetTax(designDoc, viewName).Key(bucket);
                if (listTax != null && listTax.Count() > 0)
                {
                    Tax tax = listTax.First();
                    if (tax.arrayTax != null)
                    {
                        //listTaxItem = from item in tax.arrayTax
                        //              where item.storeInformation.Any(r => r.index == inventoryId)
                        //              select item;
                        foreach (TaxItem item in tax.arrayTax)
                        {
                            var storeInfo = item.storeInformation.Where(r => r.index == inventoryId).FirstOrDefault();
                            if (storeInfo is StoreInformation)
                            {
                                item.valueRate = storeInfo.valueRate;
                                listTaxItem.Add(item);
                            }
                        }
                    }
                }
            }
            catch
            {
                return listTaxItem;
            }
            return listTaxItem;
        }

        public ServiceCharge GetServiceChargeWithStoreId(string designDoc, string viewName, string bucket, int inventoryId)
        {
            IView<Tax> listTax = null;
            ServiceCharge serviceCharge = new ServiceCharge();
            TaxDAC taxDAC = new TaxDAC(bucketSmoovPOS);
            try
            {
                listTax = taxDAC.GetTax(designDoc, viewName).Key(bucket);
                if (listTax != null && listTax.Count() > 0)
                {
                    Tax tax = listTax.First();
                    if (tax.arrServiceCharge != null)
                    {
                        var listServiceCharge = from item in tax.arrServiceCharge
                                                where item.store.index == inventoryId
                                                select item;
                        if (listServiceCharge.Count() > 0)
                        {
                            serviceCharge = listServiceCharge.First();
                        }
                    }
                }
            }
            catch
            {
                return serviceCharge;
            }
            return serviceCharge;
        }
        public Tax GetDetailTaxWithInventory(string designDoc, string viewName, string bucket, int inventoryId)
        {
            TaxDAC taxDAC = new TaxDAC(bucketSmoovPOS);
            IView<Tax> listTax = taxDAC.GetTax(designDoc, viewName).Key(bucket);
            if (listTax != null && listTax.Count() > 0)
            {
                var tax = listTax.FirstOrDefault();
                if (tax != null)
                {
                    List<TaxItem> listTaxItem = new List<TaxItem>();
                    if (tax.arrayTax != null)
                    {
                        foreach (TaxItem item in tax.arrayTax)
                        {
                            var storeInfo = item.storeInformation.Where(r => r.index == inventoryId).FirstOrDefault();
                            if (storeInfo != null)
                            {
                                item.valueRate = storeInfo.valueRate;
                                listTaxItem.Add(item);
                            }
                        }
                    }
                    tax.arrayTax = listTaxItem;

                    if (tax.arrServiceCharge != null)
                    {
                        ServiceCharge serviceCharge = new ServiceCharge();
                        var listServiceCharge = from item in tax.arrServiceCharge
                                                where item.store.index == inventoryId
                                                select item;
                        if (listServiceCharge.Count() > 0)
                        {
                            serviceCharge = listServiceCharge.First();
                            tax.arrServiceCharge = new List<ServiceCharge>() { serviceCharge };
                        }
                        else
                        {
                            tax.arrServiceCharge = null;
                        }
                    }

                    return tax;
                }
            }
            return null;
        }
    }
}
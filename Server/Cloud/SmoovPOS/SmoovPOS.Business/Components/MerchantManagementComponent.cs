﻿using Com.SmoovPOS.Model;
using SmoovPOS.Common;
using SmoovPOS.DataSql;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmoovPOS.Business.Components
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>"Hoan.Tran"</author>
    /// <datetime>1/9/2015-10:46 AM</datetime>
    public class MerchantManagementComponent
    {
        /// <summary>
        /// Gets all merchant management.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-9:54 AM</datetime>
        public IEnumerable<MerchantManagementModel> GetAllMerchantManagement()
        {
            using (var db = new EntitiesDataContext())
            {
                var results = (from m in db.Merchants
                               join s in db.ServicePackages on m.servicePacketID equals s.servicePacketID
                               select new MerchantManagementModel
                               {
                                   merchantID = m.merchantID,
                                   fullName = m.fullName,
                                   email = m.email,
                                   phone = m.phone,
                                   servicePacketStartDate = m.servicePacketStartDate,
                                   servicePacketEndDate = m.servicePacketEndDate,
                                   servicePacketName = s.servicePacketName,
                                   servicePacketID = s.servicePacketID,
                                   status = m.status
                               }).ToList();

                return results;
            }
        }
        /// <summary>
        /// Gets the count all merchant management.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:22 PM</datetime>
        public int GetCountAllMerchantManagement()
        {
            using (var db = new EntitiesDataContext())
            {
                var results = (from m in db.Merchants
                               select m.merchantID).Count();

                return results;
            }
        }
        /// <summary>
        /// Gets the merchant management.
        /// </summary>
        /// <param name="searchModel">The search model.</param>
        /// <returns></returns>
        /// <author>
        /// "Hoan.Tran"
        /// </author>
        /// <datetime>1/9/2015-9:54 AM</datetime>
        public SearchMerchantManagementModel GetMerchantManagement(SearchMerchantManagementModel searchModel)
        {
            using (var db = new EntitiesDataContext())
            {
                var results = from m in db.Merchants
                              join s in db.ServicePackages on m.servicePacketID equals s.servicePacketID into group1
                              from g1 in group1.DefaultIfEmpty()
                              select new MerchantManagementModel
                              {
                                  merchantID = m.merchantID,
                                  fullName = m.fullName,
                                  firstName = m.firstName,
                                  lastName = m.lastName,
                                  email = m.email,
                                  phone = m.phone,
                                  servicePacketStartDate = m.servicePacketStartDate,
                                  servicePacketEndDate = m.servicePacketEndDate,
                                  servicePacketName = g1.servicePacketName,
                                  servicePacketID = g1.servicePacketID,
                                  status = m.status,
                                  display = m.display,
                                  createdDate = m.createdDate,
                                  verifyLink = m.verifyLink,
                                  billingCycleId = m.billingCycleId
                              };

                //only show record display
                results = results.Where(x => x.display == true);
                if (!string.IsNullOrEmpty(searchModel.fullName))
                {
                    var searchValue = searchModel.fullName.Trim();
                    results = results.Where(x => x.fullName.Contains(searchValue) || x.email.Contains(searchValue) || x.phone.Contains(searchValue) || x.servicePacketName.Contains(searchValue));
                }
                //get count total
                searchModel.countAll = results.Count();

                searchModel.ListMerchantManagements = new List<MerchantManagementModel>();

                if (searchModel.countAll > 0)
                {
                    //sort
                    if (!string.IsNullOrEmpty(searchModel.sort))
                    {
                        //sort by full name
                        switch (searchModel.sort)
                        {
                            case SmoovPOS.Common.ConstantSmoovs.MerchantManagement.fullName:
                                if (searchModel.desc)
                                {
                                    results = results.OrderByDescending(x => x.fullName);
                                }
                                else
                                {
                                    results = results.OrderBy(x => x.fullName);
                                }
                                break;
                            case SmoovPOS.Common.ConstantSmoovs.MerchantManagement.servicePacketEndDate:
                                if (searchModel.desc)
                                {
                                    results = results.OrderByDescending(x => x.servicePacketEndDate);
                                }
                                else
                                {
                                    results = results.OrderBy(x => x.servicePacketEndDate);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        //sort default
                        results = results.OrderByDescending(x => x.createdDate);
                    }

                    //paging
                    searchModel.ListMerchantManagements = results.Skip((searchModel.pageGoTo - 1) * searchModel.limit).Take(searchModel.limit).ToList();

                    searchModel.pageCurrent = searchModel.pageGoTo;//set page current
                }

                return searchModel;
            }
        }

        /// <summary>
        /// Gets the merchant management.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>
        /// "Hoan.Tran"
        /// </author>
        /// <datetime>1/9/2015-10:10 AM</datetime>
        public MerchantManagementModel GetMerchantManagement(Guid id)
        {
            using (var db = new EntitiesDataContext())
            {
                var result = (from m in db.Merchants
                              where m.merchantID.Equals(id)
                              select m).FirstOrDefault();

                if (result != null)
                {


                    var model = FillDataToMerchantManagementModel(result);

                    model.proxyPort = result.proxyPort.GetValueOrDefault();
                    if (result.servicePacketID != null)
                    {
                        var servicePacket = (from s in db.ServicePackages
                                             where s.servicePacketID.Equals(result.servicePacketID)
                                             select s).FirstOrDefault();
                        if (servicePacket != null)
                        {
                            model.servicePacketName = servicePacket.servicePacketName;
                        }
                    }
                    return model;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the merchant management by user identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:22 PM</datetime>
        public MerchantManagementModel GetMerchantManagementByUserId(string id)
        {
            using (var db = new EntitiesDataContext())
            {
                var result = (from m in db.Merchants
                              where m.userID.Equals(id) && m.display == true
                              select m).FirstOrDefault();

                if (result != null)
                {
                    var model = FillDataToMerchantManagementModel(result);

                    model.billingCycleId = result.billingCycleId;

                    var resultsiteAlias = (from s in db.SiteAlias
                                           where s.SiteAliasID.ToUpper().Equals(model.bucket.ToUpper())
                                           select s).FirstOrDefault();
                    if (resultsiteAlias != null)
                    {
                        model.Domain = resultsiteAlias.HTTP;
                        model.SubDomain = resultsiteAlias.HTTPAlias;
                        model.CheckSubdomain = resultsiteAlias.CheckSubdomain;
                    }
                    return model;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the name of the merchant management by business.
        /// </summary>
        /// <param name="businessName">Name of the business.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:22 PM</datetime>
        public MerchantManagementModel GetMerchantManagementByBusinessName(string businessName)
        {
            using (var db = new EntitiesDataContext())
            {
                var result = (from m in db.Merchants
                              where m.businessName.ToUpper().Equals(businessName.Trim().ToUpper()) && m.status && m.display
                              select m).FirstOrDefault();

                if (result != null)
                {
                    var model = FillDataToMerchantManagementModel(result);

                    model.proxyPort = result.proxyPort.GetValueOrDefault();
                    if (result.servicePacketID != null)
                    {
                        var servicePacket = (from s in db.ServicePackages
                                             where s.servicePacketID.Equals(result.servicePacketID)
                                             select s).FirstOrDefault();
                        if (servicePacket != null)
                        {
                            model.servicePacketName = servicePacket.servicePacketName;
                        }
                    }
                    var resultsiteAlias = (from s in db.SiteAlias
                                           where s.SiteAliasID.ToUpper().Equals(model.bucket.ToUpper())
                                           select s).FirstOrDefault();
                    if (resultsiteAlias != null)
                    {
                        model.Domain = resultsiteAlias.HTTP;
                        model.SubDomain = resultsiteAlias.HTTPAlias;
                        model.CheckSubdomain = resultsiteAlias.CheckSubdomain;
                    }
                    return model;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Exists the merchant management by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:22 PM</datetime>
        public bool ExistMerchantManagementByEmail(string email, Guid? id)
        {
            using (var db = new EntitiesDataContext())
            {
                var isExist = false;
                if (id != null)
                {
                    isExist = (from m in db.Merchants
                               where m.email.ToUpper().Equals(email.Trim().ToUpper()) && (m.merchantID != id.Value) && m.display == true
                               select m).Any();

                }
                else
                {
                    isExist = (from m in db.Merchants
                               where m.email.ToUpper().Equals(email.Trim().ToUpper()) && m.display == true
                               select m).Any();
                }
                return isExist;
            }
        }
        /// <summary>
        /// Exists the name of the merchant management by business.
        /// </summary>
        /// <param name="businessName">Name of the business.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:22 PM</datetime>
        public bool ExistMerchantManagementByBusinessName(string businessName, Guid? id)
        {
            using (var db = new EntitiesDataContext())
            {
                var isExist = false;
                if (id != null)
                {
                    isExist = (from m in db.Merchants
                               where m.businessName.ToUpper().Equals(businessName.Trim().ToUpper()) && (m.merchantID != id.Value) && m.display == true
                               select m).Any();
                }
                else
                {
                    isExist = (from m in db.Merchants
                               where m.businessName.ToUpper().Equals(businessName.Trim().ToUpper()) && m.display == true
                               select m).Any();
                }
                return isExist;
            }
        }
        /// <summary>
        /// Creates the merchant management.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-10:45 AM</datetime>
        public Guid? CreateMerchantManagement(MerchantManagementModel result)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = new Merchant();

                    //general Id
                    if (model.merchantID == null)
                    {
                        model.merchantID = Guid.NewGuid();
                    }
                    else
                    {
                        model.merchantID = result.merchantID;
                    }
                    model.firstName = result.firstName;
                    model.lastName = result.lastName;
                    model.fullName = result.fullName;
                    model.gender = result.gender;
                    model.birthday = result.birthday;
                    model.phone = result.phone;
                    model.address = result.address;
                    model.city = result.city;
                    model.zipCode = result.zipCode;
                    model.region = result.region;
                    model.email = result.email;

                    model.userID = result.userID;

                    model.title = result.title;
                    model.businessName = result.businessName;
                    model.servicePacketID = result.servicePacketID;
                    model.servicePacketStartDate = result.servicePacketStartDate;
                    model.servicePacketEndDate = result.servicePacketEndDate;
                    model.verifyLink = result.verifyLink;
                    model.status = false;//default
                    model.bucket = result.bucket;
                    model.country = result.country;

                    model.userOwner = result.userOwner;//user create
                    model.createdDate = DateTime.UtcNow;//default
                    model.modifyUser = result.userOwner;
                    model.modifyDate = DateTime.UtcNow;
                    model.display = true;//default

                    model.titleHomePage = result.titleHomePage;
                    model.phoneRegion = result.phoneRegion;
                    model.proxyPort = result.proxyPort;

                    model.billingCycleId = result.billingCycleId;

                    db.Merchants.InsertOnSubmit(model);
                    db.SubmitChanges();

                    return model.merchantID;
                }

            }
            catch
            {
            }

            return null;
        }

        /// <summary>
        /// Updates the merchant management.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-10:44 AM</datetime>
        public bool UpdateMerchantManagement(MerchantManagementModel result)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = (from m in db.Merchants
                                 where m.merchantID.Equals(result.merchantID)
                                 select m).FirstOrDefault();

                    if (model != null)
                    {
                        model.firstName = result.firstName;
                        model.lastName = result.lastName;
                        model.fullName = result.fullName;
                        model.gender = result.gender;
                        model.birthday = result.birthday;
                        model.phone = result.phone;
                        model.address = result.address;
                        model.city = result.city;
                        model.zipCode = result.zipCode;
                        model.region = result.region;
                        //model.email = result.email;

                        //model.userID = result.userID;

                        model.title = result.title;
                        model.businessName = result.businessName;
                        model.servicePacketID = result.servicePacketID;
                        model.servicePacketStartDate = result.servicePacketStartDate;
                        model.servicePacketEndDate = result.servicePacketEndDate;
                        model.verifyLink = result.verifyLink;
                        model.status = result.status;
                        model.bucket = result.bucket;
                        model.country = result.country;
                        //model.userOwner = (new Common.Controllers.UserSession()).UserId;//user create
                        //model.createdDate = DateTime.Now;//default
                        model.modifyUser = result.modifyUser;//user update
                        model.modifyDate = DateTime.UtcNow;//default
                        //model.display = true;//default
                        model.titleHomePage = result.titleHomePage;
                        model.phoneRegion = result.phoneRegion;

                        model.proxyPort = result.proxyPort;

                        model.billingCycleId = result.billingCycleId;

                        db.SubmitChanges();

                        return true;
                    }
                }

            }
            catch
            {
            }
            return false;
        }
        /// <summary>
        /// Changes the status merchant management.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="status">if set to <c>true</c> [status].</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-10:46 AM</datetime>
        public bool ChangeStatusMerchantManagement(Guid id, bool status)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = (from m in db.Merchants
                                 where m.merchantID.Equals(id)
                                 select m).FirstOrDefault();

                    if (model != null)
                    {
                        model.status = status;

                        //model.userOwner = (new Common.Controllers.UserSession()).UserId;//user create
                        //model.createdDate = DateTime.Now;//default
                        model.modifyUser = (new Common.Controllers.UserSession()).UserId;//user update
                        model.modifyDate = DateTime.Now;//default
                        //model.display = true;//default

                        db.SubmitChanges();

                        return true;
                    }
                }

            }
            catch
            {
            }
            return false;
        }
        /// <summary>
        /// Deletes the merchant management.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/9/2015-10:53 AM</datetime>
        public bool DeleteMerchantManagement(Guid id)
        {
            try
            {
                using (var db = new EntitiesDataContext())
                {
                    var model = (from m in db.Merchants
                                 where m.merchantID.Equals(id)
                                 select m).FirstOrDefault();

                    if (model != null)
                    {
                        model.display = false;//delete

                        //model.userOwner = (new Common.Controllers.UserSession()).UserId;//user create
                        //model.createdDate = DateTime.Now;//default
                        model.modifyUser = (new Common.Controllers.UserSession()).UserId;//user update
                        model.modifyDate = DateTime.Now;//default
                        //model.display = true;//default

                        db.SubmitChanges();

                        return true;
                    }
                }

            }
            catch
            {
            }
            return false;
        }

        /// <summary>
        /// Gets the merchant management by site identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <author>"Dao.Nguyen"</author>
        /// <datetime>1/21/2015-6:05 PM</datetime>
        public MerchantManagementModel GetMerchantManagementBySiteId(string id)
        {
            using (var db = new EntitiesDataContext())
            {
                var result = (from m in db.Merchants
                              where m.bucket.Equals(id) && m.display == true
                              select m).FirstOrDefault();

                if (result != null)
                {
                    var model = FillDataToMerchantManagementModel(result);

                    var resultsiteAlias = (from s in db.SiteAlias
                                           where s.SiteAliasID.ToUpper().Equals(model.bucket.ToUpper())
                                           select s).FirstOrDefault();
                    if (resultsiteAlias != null)
                    {
                        model.Domain = resultsiteAlias.HTTP;
                        model.SubDomain = resultsiteAlias.HTTPAlias;
                        model.CheckSubdomain = resultsiteAlias.CheckSubdomain;
                    }
                    return model;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// Gets the next proxy port.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:22 PM</datetime>
        public int GetNextProxyPort()
        {
            using (var db = new EntitiesDataContext())
            {
                var result = (from m in db.Merchants
                              select m.proxyPort).Max();
                if (result == null || result <= 2000)
                {
                    result = 2001;
                }
                else
                {
                    result = result + 1;
                }
                if (result == 8080)
                {
                    result = 8081;
                }

                return result.Value;
            }
        }
        /// <summary>
        /// Gets all country.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:22 PM</datetime>
        public List<string> GetAllCountry()
        {
            using (var db = new EntitiesDataContext())
            {
                var result = (from m in db.Countries
                              select m.Name).ToList();

                return result.OrderBy(c => c).ToList();
            }
        }
        /// <summary>
        /// Gets all site identifier.
        /// </summary>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:21 PM</datetime>
        public List<string> GetAllSiteID()
        {
            using (var db = new EntitiesDataContext())
            {
                var result = (from m in db.Sites
                              select m.SiteID).ToList();

                return result.OrderBy(c => c).ToList();
            }
        }
        /// <summary>
        /// Generals the session.
        /// </summary>
        /// <param name="userID">The user identifier.</param>
        /// <param name="access_token">The access_token.</param>
        /// <param name="expires_in">The expires_in.</param>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:22 PM</datetime>
        public void GeneralSession(string userID, out string access_token, out long expires_in)
        {
            access_token = Guid.NewGuid().ToString().Replace("-", string.Empty);
            var expires_in_ss = (DateTime.UtcNow.AddHours(ConstantSmoovs.MerchantManagement.expires_in) - ConstantSmoovs.year1970);
            expires_in = expires_in_ss.Ticks;

            using (var db = new EntitiesDataContext())
            {
                var result = (from m in db.UserSessions
                              where m.UserID.Equals(userID)
                              select m).FirstOrDefault();
                if (result != null)
                {
                    TimeSpan ts = TimeSpan.FromTicks(result.expires_in);
                    if (ts <= expires_in_ss)
                    {
                        //update expires
                        access_token = result.access_token;
                        result.expires_in = expires_in_ss.Ticks;

                        result.modifyUser = userID;
                        result.modifyDate = DateTime.Now;
                    }
                    else
                    {
                        //update new token
                        result.access_token = access_token;
                        result.expires_in = expires_in_ss.Ticks;

                        result.modifyUser = userID;
                        result.modifyDate = DateTime.Now;
                    }
                    db.SubmitChanges();
                }
                else
                {
                    var userSession = new UserSession();
                    userSession.UserID = userID;
                    userSession.userOwner = userID;
                    userSession.access_token = access_token;
                    userSession.expires_in = expires_in_ss.Ticks;
                    userSession.createdDate = DateTime.Now;
                    userSession.modifyUser = userID;
                    userSession.modifyDate = DateTime.Now;
                    userSession.display = true;

                    db.UserSessions.InsertOnSubmit(userSession);
                    db.SubmitChanges();
                }
            }

            expires_in = ConstantSmoovs.MerchantManagement.expires_in * 60 * 60 * 1000;
        }
        /// <summary>
        /// Gets the user session by token.
        /// </summary>
        /// <param name="access_token">The access_token.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>1/30/2015-5:22 PM</datetime>
        public AuthenticationPOSModel ExtentionSessionByToken(string access_token)
        {
            var expires_in_ss = (DateTime.UtcNow.AddHours(ConstantSmoovs.MerchantManagement.expires_in) - ConstantSmoovs.year1970);

            using (var db = new EntitiesDataContext())
            {
                var result = (from m in db.UserSessions
                              where m.access_token.Equals(access_token)
                              select m).FirstOrDefault();
                if (result != null)
                {
                    var resultMerchant = (from m in db.Merchants
                                          where m.userID.ToUpper().Equals(result.UserID.ToUpper()) && m.status && m.display
                                          select m).FirstOrDefault();
                    if (resultMerchant != null)
                    {
                        //TimeSpan ts = TimeSpan.FromTicks(result.expires_in);
                        //if (ts <= expires_in_ss)
                        //{
                        result.access_token = access_token;
                        result.expires_in = expires_in_ss.Ticks;

                        db.SubmitChanges();

                        var authen = new AuthenticationPOSModel();
                        authen.access_token = result.access_token;
                        authen.expires_in = ConstantSmoovs.MerchantManagement.expires_in * 60 * 60 * 1000; //result.expires_in;
                        authen.databaseSync = resultMerchant.bucket;
                        authen.code = 1;

                        return authen;
                        //}
                    }
                }
            }

            return null;
        }
        /// <summary>
        /// Gets the user session by token.
        /// </summary>
        /// <param name="access_token">The access_token.</param>
        /// <returns></returns>
        /// <author>"Hoan.Tran"</author>
        /// <datetime>3/3/2015-9:45 AM</datetime>
        public AuthenticationPOSModel GetUserSessionByToken(string access_token)
        {
            var expires_in_ss = (DateTime.UtcNow.AddHours(ConstantSmoovs.MerchantManagement.expires_in) - ConstantSmoovs.year1970);

            using (var db = new EntitiesDataContext())
            {
                var result = (from u in db.UserSessions
                              join m in db.Merchants on u.UserID equals m.userID
                              where u.access_token.Equals(access_token)
                              select new AuthenticationPOSModel
                              {
                                  access_token = u.access_token,
                                  expires_in = u.expires_in,
                                  databaseSync = m.bucket
                              }).FirstOrDefault();
                if (result != null)
                {
                    TimeSpan ts = TimeSpan.FromTicks(result.expires_in);
                    if (ts <= expires_in_ss)
                    {
                        var totalS = (int)(expires_in_ss - ts).TotalSeconds;
                        var expires_Seconds = (ConstantSmoovs.MerchantManagement.expires_in * 60 * 60) - totalS;

                        result.expires_in = expires_Seconds * 1000; //result.expires_in;
                    }
                    else
                    {
                        result.expires_in = 0;
                    }
                    return result;
                }
            }

            return null;
        }
        public AuthenticationPOSModel GetUserSessionBySiteID(string siteID)
        {
            var expires_in_ss = (DateTime.UtcNow.AddHours(ConstantSmoovs.MerchantManagement.expires_in) - ConstantSmoovs.year1970);

            using (var db = new EntitiesDataContext())
            {
                var result = (from u in db.UserSessions
                              join m in db.Merchants on u.UserID equals m.userID
                              where m.bucket.Equals(siteID)
                              select new AuthenticationPOSModel
                              {
                                  access_token = u.access_token,
                                  expires_in = u.expires_in,
                                  databaseSync = m.bucket
                              }).FirstOrDefault();
                if (result == null)
                {
                    result = (from m in db.Merchants
                              where m.bucket.Equals(siteID)
                              select new AuthenticationPOSModel
                              {
                                  databaseSync = m.bucket
                              }).FirstOrDefault();

                    return result;
                }
                if (result != null)
                {
                    TimeSpan ts = TimeSpan.FromTicks(result.expires_in);
                    if (ts <= expires_in_ss)
                    {
                        var totalS = (int)(expires_in_ss - ts).TotalSeconds;
                        var expires_Seconds = (ConstantSmoovs.MerchantManagement.expires_in * 60 * 60) - totalS;

                        result.expires_in = expires_Seconds * 1000; //result.expires_in;
                    }
                    else
                    {
                        result.expires_in = 0;
                    }
                    return result;
                }
            }

            return null;
        }
        public MerchantManagementModel FillDataToMerchantManagementModel(Merchant result)
        {
            var model = new MerchantManagementModel();

            model.merchantID = result.merchantID;
            model.firstName = result.firstName;
            model.lastName = result.lastName;
            model.fullName = result.fullName;
            model.gender = result.gender;
            model.birthday = result.birthday;
            model.phone = result.phone;
            model.address = result.address;
            model.city = result.city;
            model.zipCode = result.zipCode;
            model.region = result.region;
            model.email = result.email;
            model.userID = result.userID;
            model.title = result.title;
            model.businessName = result.businessName;
            model.servicePacketID = result.servicePacketID;
            model.servicePacketStartDate = result.servicePacketStartDate;
            model.servicePacketEndDate = result.servicePacketEndDate;
            model.verifyLink = result.verifyLink;
            model.status = result.status;
            model.bucket = result.bucket;
            model.userOwner = result.userOwner;
            model.createdDate = result.createdDate;
            model.modifyUser = result.modifyUser;
            model.modifyDate = result.modifyDate;
            model.display = result.display;

            model.country = result.country;
            model.titleHomePage = result.titleHomePage;
            model.phoneRegion = result.phoneRegion;
            model.billingCycleId = result.billingCycleId;

            return model;
        }
    }
}
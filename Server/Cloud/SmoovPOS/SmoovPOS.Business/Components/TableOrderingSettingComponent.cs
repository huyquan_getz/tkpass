﻿using Com.SmoovPOS.Data.DAC;
using Com.SmoovPOS.Entity;
using Couchbase;
using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Business.Components
{
    public class TableOrderingSettingComponent
    {
        readonly string bucketSmoovPOS = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
        public int CreateNewTableOrderingSetting(TableOrderingSetting tableOrderingSetting, string bucket)
        {
            tableOrderingSetting.bucket = bucket;
            TableOrderingSettingDAC tableOrderingSettingDAC = new TableOrderingSettingDAC(bucketSmoovPOS);
            int statusCode = tableOrderingSettingDAC.CreateDAC(tableOrderingSetting);
            return statusCode;
        }
        public int UpdateTableOrderingSetting(TableOrderingSetting tableOrderingSetting, string bucket)
        {
            tableOrderingSetting.bucket = bucket;
            TableOrderingSettingDAC tableOrderingSettingDAC = new TableOrderingSettingDAC(bucketSmoovPOS);
            int statusCode = tableOrderingSettingDAC.UpdateDAC(tableOrderingSetting);
            return statusCode;
        }

        public int DeleteTableOrderingSetting(string id, string bucket)
        {
            TableOrderingSettingDAC tableOrderingSettingDAC = new TableOrderingSettingDAC(bucketSmoovPOS);
            int statusCode = tableOrderingSettingDAC.DeleteDAC(id);
            return statusCode;
        }

        public TableOrderingSetting DetailTableOrderingSetting(string id, string bucket)
        {
            TableOrderingSettingDAC tableOrderingSettingDAC = new TableOrderingSettingDAC(bucketSmoovPOS);
            TableOrderingSetting tableOrderingSetting = tableOrderingSettingDAC.DetailDAC(id);
            return tableOrderingSetting;
        }

        public TableOrderingSetting DetailTableOrderingSettingByBranchId(string designDoc, string viewName, string branch_id, string bucket)
        {
            TableOrderingSettingDAC tableOrderingSettingDAC = new TableOrderingSettingDAC(bucketSmoovPOS);
            IView<TableOrderingSetting> ListTableOrderingSetting = null;
            ListTableOrderingSetting = tableOrderingSettingDAC.GetAll(designDoc, viewName);
            ListTableOrderingSetting.Key(new string[] { bucket, branch_id });
            return ListTableOrderingSetting.FirstOrDefault();
        }

        public IView<TableOrderingSetting> GetAllTableOrderingSetting(string designDoc, string viewName, string bucket)
        {
            TableOrderingSettingDAC tableOrderingSettingDAC = new TableOrderingSettingDAC(bucketSmoovPOS);
            IView<TableOrderingSetting> ListTableOrderingSetting = null;
            ListTableOrderingSetting = tableOrderingSettingDAC.GetAll(designDoc, viewName).Key(bucket);
            return ListTableOrderingSetting;
        }
    }
}
﻿using Com.SmoovPOS.Data.DAL;
using Com.SmoovPOS.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Com.SmoovPOS.Business
{
    public class SiteBusiness : BaseBussiness<SiteModel>
    {
        #region CRUD
        protected override void DoAdd(SiteModel entity)
        {
            //List<SqlParameter> sqlParameterList = new List<SqlParameter>();
            //sqlParameterList.Add(SQLRepository.CreateSqlParameter("SiteID", System.Data.SqlDbType.VarChar, entity.Name, true));
            //sqlParameterList.Add(SQLRepository.CreateSqlParameter("Name", System.Data.SqlDbType.VarChar, entity.Name));
            //sqlParameterList.Add(SQLRepository.CreateSqlParameter("Type", System.Data.SqlDbType.VarChar, entity.Type));
            //sqlParameterList.Add(SQLRepository.CreateSqlParameter("UserRegistration", System.Data.SqlDbType.Int, entity.UserRegistration));
            //sqlParameterList.Add(SQLRepository.CreateSqlParameter("ExpiryDate", System.Data.SqlDbType.DateTime, entity.ExpiryDate));

            string queryString = string.Format("insert into [Site](SiteID,Name,Type,UserRegistration,ExpiryDate) values('{0}', '{1}', '{2}', '{3}', {4})",
                entity.Name, entity.Name, entity.Type, entity.UserRegistration, "NULL");
            entity.SiteID = SQLRepository.GetData<string>(queryString, new List<SqlParameter>(), cmd => { cmd.CommandType = CommandType.Text; },
                dr => { return dr.Read() ? (dr[0] != DBNull.Value ? dr[0].ToString() : string.Empty) : string.Empty; });
        }

        protected override void DoUpdate(SiteModel entity)
        {
            throw new NotImplementedException();
        }

        protected override void DoDelete(SiteModel entity)
        {
            throw new NotImplementedException();
        }

        protected override void DoDelete(int id)
        {
            throw new NotImplementedException();
        }

        protected override SiteModel DoSelect(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        public IEnumerable<SiteModel> GetAllSite()
        {
            return new List<SiteModel>();
        }

        /// <summary>
        /// Gets all site alias.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SiteAliasModel> GetAllSiteAlias()
        {
            string queryString = "select * from [SiteAlias]";
            return SQLRepository.GetData<IEnumerable<SiteAliasModel>>(queryString, new List<SqlParameter>(), cmd => { cmd.CommandType = CommandType.Text; },
                    dr => GetAllSiteAliasReader(dr));
        }

        private IEnumerable<SiteAliasModel> GetAllSiteAliasReader(SqlDataReader reader)
        {
            List<SiteAliasModel> result = new List<SiteAliasModel>();

            while (reader.Read())
            {
                SiteAliasModel siteAlias = new SiteAliasModel();

                if (reader["SiteID"] != DBNull.Value)
                {
                    siteAlias.SiteID = Convert.ToString(reader["SiteID"]);
                }

                //if (reader["HTTPAlias"] != DBNull.Value)
                //{
                //    siteAlias.HTTPAlias = Convert.ToString(reader["HTTPAlias"]);
                //}

                if (reader["IsPrimary"] != DBNull.Value)
                {
                    siteAlias.IsPrimary = Convert.ToBoolean(reader["IsPrimary"]);
                }
                if (reader["CheckSubDomain"] != DBNull.Value)
                {
                    siteAlias.CheckSubdomain = Convert.ToBoolean(reader["CheckSubdomain"]);
                }
                siteAlias.HTTPAlias = Convert.ToString(reader["HTTPAlias"]);
                siteAlias.HTTP = Convert.ToString(reader["HTTP"]);
                

                result.Add(siteAlias);
            }

            return result;
        }

        /// <summary>
        /// Creates the site alias.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void CreateSiteAlias(SiteAliasModel entity)
        {
            string queryString = string.Format("insert into [SiteAlias](SiteAliasID,SiteID,HTTPAlias,IsPrimary) values('{0}', '{1}', '{2}', {3})",
                entity.SiteAliasID, entity.SiteID, entity.HTTPAlias, entity.IsPrimary ? "1" : "0");
            entity.SiteID = SQLRepository.GetData<string>(queryString, new List<SqlParameter>(), cmd => { cmd.CommandType = CommandType.Text; },
                dr => { return string.Empty; });
        }

        /// <summary>
        /// Gets the primary site alias.
        /// </summary>
        /// <param name="siteID">The site identifier.</param>
        /// <returns></returns>
        public string GetPrimarySiteAlias(string siteID)
        {
            string queryString = string.Format("select HTTPAlias from [SiteAlias] where SiteID = '{0}'", siteID);
            return SQLRepository.GetData<string>(queryString, new List<SqlParameter>(), cmd => { cmd.CommandType = CommandType.Text; },
                    dr => { return dr.Read() ? (dr[0] != DBNull.Value ? dr[0].ToString() : string.Empty) : string.Empty; });
        }
    }
}
﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace Com.SmoovPOS.Model
{
    //[DataContract(IsReference = true)]
    [Serializable]
    public class SearchOrderModel : EntityPaging
    {
        public IEnumerable<Order> ListOrders { get; set; }
        public string TableId { get; set; }
        public string TabId { get; set; }

        //[DataMember]
        public int? OrderType { get; set; }

        //[DataMember]
        public string FilterKey { get; set; }

        //[DataMember]
        public string FilterField { get; set; }

        //[DataMember]
        public List<SelectListItem> FilterList { get; set; }

        //[DataMember]
        public int? PaymentStatusKey { get; set; }

        //[DataMember]
        public List<SelectListItem> PaymentStatusList { get; set; }

        //[DataMember]
        public int? OrderStatusKey { get; set; }

        //[DataMember]
        public List<SelectListItem> OrderStatusList { get; set; }
        public string StoreKey { get; set; }

        //[DataMember]
        public List<SelectListItem> StoresList { get; set; }
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date, ErrorMessage = "Please enter a valid date in the format MM/dd/yyyy")]

        //[DataMember]
        public DateTime? StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date, ErrorMessage = "Please enter a valid date in the format MM/dd/yyyy")]

        //[DataMember]
        public DateTime? EndDate { get; set; }

        public string siteID { get; set; }
        public string Currency { get; set; }

        public string Bucket { get; set; }
    }
}
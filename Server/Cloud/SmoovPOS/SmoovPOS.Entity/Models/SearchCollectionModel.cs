﻿using Com.SmoovPOS.Entity;
using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Models
{
    public class SearchCollectionModel:EntityPaging
    {
        public string fullName { get; set; }
        public IEnumerable<Collection> ListCollection { get; set; }

        public string siteID { get; set; }
        public bool isAddNew { get; set; }
    }
}
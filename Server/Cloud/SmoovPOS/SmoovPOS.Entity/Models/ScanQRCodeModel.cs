﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Models
{
    public class ScanQRCodeModel
    {
        public Station station { get; set; }
        public TableOrderingSetting tableOrderingSetting { get; set; }
        public Inventory inventory { get; set; }
        public TableOrderModel tableOrderModel { get; set; }
        public string currency { get; set; }
        public string businessName { get; set; }
        public bool tableOrderingPermission { get; set; }
    }
}
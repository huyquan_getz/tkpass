﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Model
{
    public class PaymentModel 
    {
        public string Id { get; set; }
        // 0 live mode , 1 test mode (sandbox)
        public int Status { get; set; }
        public string SmoovUrl { get; set; }
        public string SuccessPage { get; set; }
        public string CancelPage { get; set; }
        public string StrUrl { get; set; }      
        
        public string SandboxSmoovUrl { get; set; }
       
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
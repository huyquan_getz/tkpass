﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Models
{
    public class MailObjectModel
    {
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerPhone { get; set; }
        public string ShippingAddress { get; set; }
        public string PickupAddress { get; set; }
        public string OrderRmark { get; set; }
        public string CreateDate { get; set; }
        public string OrderId { get; set; }
        public string TransactionId {get;set;}
        public string TotalAmount { get; set; }
        public string ConfirmLink { get; set; }

    }
}
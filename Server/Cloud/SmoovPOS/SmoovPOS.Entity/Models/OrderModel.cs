﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Models
{
    public class ListPaidAndRefundedOrdersModel
    {
        public List<Order> listPaidOrders { get; set; }
        public List<Order> listRefundedOrders { get; set; }
    }
}
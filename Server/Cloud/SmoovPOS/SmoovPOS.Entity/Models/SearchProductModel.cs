﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Models
{
    public class SearchProductModel
    {
        public IEnumerable<ProductItem> ListProductItem { get; set; }
        public NewPaging Paging { get; set; }
        public bool isAddNew { get; set; }
    }
}
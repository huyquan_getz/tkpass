﻿using Com.SmoovPOS.Entity;
using System.ComponentModel.DataAnnotations;

namespace Com.SmoovPOS.Model
{
    public class SiteAliasModel : BaseModel
    {
        public string SiteAliasID { get; set; }

        public string SiteID { get; set; }

        [StringLength(200)]
        public string HTTPAlias { get; set; }

        [Required]
        public bool IsPrimary { get; set; }
        public string HTTP { get; set; }
        public System.Nullable<bool> CheckSubdomain { get; set; }
    }
}
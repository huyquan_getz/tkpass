﻿using Com.SmoovPOS.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Com.SmoovPOS.Model
{
    public class SiteModel : BaseModel
    {

        public string SiteID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Type { get; set; }

        [Required]
        public string UserRegistration { get; set; }

        public DateTime? ExpiryDate { get; set; }

        public virtual List<SiteAliasModel> SiteAliasList { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Model
{
    public class BillingServicePackageModel
    {
        public System.Guid billingServicePacketID { get; set; }

        public System.Guid billingCycleID { get; set; }

        public System.Guid servicePacketID { get; set; }

        public string moneyType { get; set; }

        public decimal moneyPerUnit { get; set; }

        public double discount { get; set; }

        public string userOwner { get; set; }

        public System.DateTime createdDate { get; set; }

        public string modifyUser { get; set; }

        public System.Nullable<System.DateTime> modifyDate { get; set; }

        public bool display { get; set; }
        
        public string paymentCycleName { get; set; }
        public double number { get; set; }
    }
}
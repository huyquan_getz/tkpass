﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Model
{
    public class FunctionMethodModel
    {
        public System.Guid functionMethodID { get; set; }

        public string name { get; set; }

        public string keyMethod { get; set; }

        public bool isDatabaseFunction { get; set; }

        public string dataType { get; set; }

        public string description { get; set; }

        public string userOwner { get; set; }

        public System.DateTime createdDate { get; set; }

        public string modifyUser { get; set; }

        public System.Nullable<System.DateTime> modifyDate { get; set; }

        public bool display { get; set; }
        public bool isAddOns { get; set; }
    }
}
﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace Com.SmoovPOS.Model
{
    [Serializable]
    public class BaseModel : ICloneable
    {
        public BaseModel()
        {
            IsNew = true;
            ObjectId = Guid.NewGuid();
        }

        public Guid ObjectId { get; set; }

        public bool IsNew { get; set; }

        public bool IsDirty { get; set; }

        public bool IsVirtualDeleted { get; set; }

        public void MarkNew()
        {
            MarkIsNew(true);
        }

        public void MarkOld()
        {
            MarkIsNew(false);
        }

        private void MarkIsNew(bool value)
        {
            IsNew = value;

            // object child
            GetType().GetProperties().Where(pi => pi.PropertyType.BaseType == typeof(BaseModel))
                .ToList().ForEach(pi =>
                {
                    BaseModel bvm = pi.GetValue(this, null) as BaseModel;
                    if (bvm != null)
                    {
                        bvm.MarkIsNew(value);
                    }
                });

            // object list
            GetType().GetProperties().Where(pi => pi.PropertyType.FullName != null && (pi.PropertyType.FullName.Contains("List`1")
                                                                                       && pi.PropertyType.FullName.Contains("Com.SmoovPOS.Entity")))
                .ToList().ForEach(pi =>
                {
                    IList bvmList = pi.GetValue(this, null) as IList;
                    if (bvmList != null && bvmList.Count > 0)
                    {
                        foreach (var item in bvmList)
                        {
                            BaseModel bvm = item as BaseModel;
                            if (bvm != null)
                            {
                                bvm.MarkIsNew(value);
                            }
                        }
                    }
                });
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(ms, this);
            ms.Position = 0;
            object obj = bf.Deserialize(ms);
            ms.Close();
            return obj;
        }
    }
}
﻿using SmoovPOS.Entity.Models.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Model
{
    public class SearchFunctionMethodModel : EntityPaging
    {
        public string name { get; set; }
        public IEnumerable<FunctionMethodModel> ListFunctionMethods { get; set; }
    }
}
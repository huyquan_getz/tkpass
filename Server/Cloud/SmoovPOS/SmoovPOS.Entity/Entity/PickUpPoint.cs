﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class PickUpPoint : CBBaseEntity
    {
        public string table { get { return "PickUpPoint"; } }
        public string address { get; set; }
        public string city { get; set; }
        public string zipCode { get; set; }
        public string email { get; set; }
        public string pinCode { get; set; }     
      
        public string country { get; set; }
        public string state { get; set; }
        public string addressCombine { get; set; }
        public string phoneRegion { get; set; }
        public string phone { get; set; }
        public string timeZone { get; set; }

        public string namePickUpPoint { get; set; }
    }
}
﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Customer : CBBaseEntity
    {
        public string table
        {
            get { return "Customer"; }
        }

        public string memberID { get; set; }

        public string firstName { get; set; }
        public string lastName { get; set; }
        public string fullName { get; set; }
        public int gender { get; set; }
        public string birthday { get; set; }
        public string phone { get; set; }
        public string hiredDate { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string zipCode { get; set; }
        public string email { get; set; }
        public string pinCode { get; set; }
        public string password { get; set; }
        public string passwordConfirm { get; set; }
        public string image { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string remark { get; set; }
        public bool acceptsNewsletter { get; set; }
        public bool acceptsSms { get; set; }
        public List<Tags> tags { get; set; }
        public DateTime lastLogin { get; set; }
        public DateTime lastOrder { get; set; }
        public int totalOrder { get; set; }
        public double totalSpend { get; set; }
        public List<Orderdetail> listOrder { get; set; }
        public Boolean active { get; set; }
        public string strTags { get; set; }
        public string addressCombine { get; set; }
        public string phoneRegion { get; set; }
        public Boolean verify { get; set; }
        public string timeZoneFullName { get; set; }
        public int customerType { get; set; }
    }

    public class Tags
    {
        public int index { get; set; }
        public string keyName { get; set; }
        public string name { get; set; }
    }

    public class Orderdetail
    {
        public string orderCode { get; set; }
        public DateTime dateOrder { get; set; }
        public string payType { get; set; }
        public double total { get; set; }
        public string timeZone { get; set; }
    }
}
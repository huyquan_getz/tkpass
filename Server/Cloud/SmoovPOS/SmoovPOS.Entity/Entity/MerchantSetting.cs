﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class MerchantSetting : CBBaseEntity
    {
        public string table
        {
            get { return "Setting"; }
        }
        public string UrlHostOnline { get; set; }
        public bool updateProfile { get; set; }
        public bool createBranch { get; set; }
        public bool createCategory { get; set; }
        public bool addProduct { get; set; }
        public bool distribute { get; set; }
    }
}
﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using Com.SmoovPOS;
using SmoovPOS.Entity.Entity.Settings;

namespace Com.SmoovPOS.Entity
{
    public class TakeAwaySetting : CBBaseEntity
    {
        public string table
        {
            get { return "TakeAwaySetting"; }
        }
        public string BranchId { get; set; }
        public string BranchName { get; set; }
        public int BranchIndex { get; set; }
        public bool AcceptTakeAway { get; set; }
        public bool AllowAutoAcceptOrder { get; set; }
        public string TakeAwayInstructions { get; set; }
        public List<DayOfWeekSetting> ListDayOfWeekSetting { get; set; }
    }
}
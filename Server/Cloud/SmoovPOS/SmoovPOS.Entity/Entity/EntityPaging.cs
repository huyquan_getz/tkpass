﻿using System;
using System.Runtime.Serialization;
namespace SmoovPOS.Entity.Models.Paging
{
  [Serializable]
    public class EntityPaging
    {
        public EntityPaging()
        {
            sort = "title";
            limit = 20;
        }

        public string startKey { get; set; }
        public string nextKey { get; set; }
        public int limit { get; set; }
        public int pageGoTo { get; set; }
        public int pageCurrent { get; set; }
        public string sort { get; set; }
        public bool desc { get; set; }
        public string firstKey { get; set; }
        public string secondKey { get; set; }
        public string thridKey { get; set; }
        public string fourthKey { get; set; }
        public string fiveKey { get; set; }

        public bool allowStale { get; set; }

        int _countAll;
        public int countAll
        {
            get
            {
                return _countAll;
            }
            set
            {
                _countAll = value;
                if (pageGoTo <= 0)
                {
                    pageGoTo = 1;
                }
                //caculator paging
                pageCount = (countAll % limit) > 0 ? (int)(countAll / limit) + 1 : (int)(countAll / limit);

                var groupCurrent = (pageGoTo % 5) > 0 ? (int)(pageGoTo / 5) + 1 : (int)(pageGoTo / 5);

                firstKey = ((groupCurrent * 5) - 4).ToString();
                secondKey = (((groupCurrent * 5) - 3)) <= pageCount ? ((groupCurrent * 5) - 3).ToString() : string.Empty;
                thridKey = (((groupCurrent * 5) - 2)) <= pageCount ? ((groupCurrent * 5) - 2).ToString() : string.Empty;
                fourthKey = (((groupCurrent * 5) - 1)) <= pageCount ? ((groupCurrent * 5) - 1).ToString() : string.Empty;
                fiveKey = ((groupCurrent * 5)) <= pageCount ? (groupCurrent * 5).ToString() : string.Empty;
            }
        }
        public int pageCount { get; set; }
        public int startIndex { get; set; }
        
        public int TotalItemPrevious
        {
            get
            {
                if (pageCurrent > 1)
                {
                    return pageCurrent - 1;
                }
                else
                {
                    return 0;
                }
            }
        }
        public int TotalItemNext
        {
            get
            {
                if (pageCurrent < pageCount)
                {
                    return pageCurrent + 1;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string endKey { get; set; }
        public string text { get; set; }
    }
}
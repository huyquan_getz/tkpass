﻿using Com.SmoovPOS.Entity.CBEntity;
using System.Collections.Generic;

namespace Com.SmoovPOS.Entity
{
    public class DeliverySetting : CBBaseEntity
    {
        public string table
        {
            get { return "deliverysetting"; }
        }
        public DeliveryCountry country { get; set; }
        public string flag { get; set; }
        public List<ServiceDelivery> arrayServiceDelivery { get; set; }
    }
}
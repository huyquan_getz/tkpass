﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Settings
{
    //public class AccountSmoovpay : CBBaseEntity
    //{
    //    public string table
    //    {
    //        get { return "AccountSmoovPay"; }
    //    }

    //    public string UserName { get; set; }

    //    public string Password { get; set; }

    //    public string BusinessName { get; set; }

    //    public string BusinessDisplayName { get; set; }

    //    public string FullName { get; set; }

    //    public string PhoneRegion { get; set; }

    //    public string PhoneNumber { get; set; }

    //    public string Country { get; set; }

    //    public string ContactPerson { get; set; }
    //    public string CompanyName { get; set; }
    //}

    public class AccountSmoovpay : CBBaseEntity
    {
        public string table
        {
            get { return "AccountSmoovPay"; }
        }           
        public string businessDisplayName { get; set; }
        public string accountCode { get; set; }
        public string accountType { get; set; }
        public int accountStatus { get; set; }
        public string joinedDate { get; set; }
        public string websiteUrl { get; set; }
        public string companyRegistrationNo { get; set; }
        public bool wassessmentSent { get; set; }
        public string emailAddress { get; set; }
        public string companyName { get; set; }
        public string businessName { get; set; }
        public string defaultCurrency { get; set; }
        public string country { get; set; }
        public string contactPerson { get; set; }
        public string contactCountryCode { get; set; }
        public string contactNumber { get; set; }
        public string companyLogoUrl { get; set; }
        public string secretKey { get; set; }
        public string password { get; set; }
        public string authorization { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Com.SmoovPOS.Entity
{
    public class ServiceDelivery 
    {
        public string _id { get; set; }
        public bool status { get; set; }
        public string serviceName { get; set; }
        public int criteria { get; set; }
        public double beginValue { get; set; }
        public double endValue { get; set; }
        public bool isUnlimit { get; set; }
        public bool isDefault { get; set; }
        public int unit { get; set; }
        public double shippingPrice { get; set; }
        public int beginEstimate { get; set; }
        public int endEstimate { get; set; }
        public DateTime createdDate { get; set; }
        public List<AreaShipping> arrayAreaShipping { get; set; }
    }

    public class AreaShipping : Area
    {
        public bool isSelect { get; set; }
        //[RegularExpression(@"^\d+.\d{0,2}$")]
        //[Range(0, 9999.99)]
        public double areaPrice { get; set; }
    } 
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.Entity.Settings
{
    public class DayOfWeekSetting
    {
        public int DayOfWeek { get; set; }
        public int Option { get; set; }
        public string Open_Opening { get; set; }
        public string Open_Closing { get; set; }
        public string Split_OpeningStart { get; set; }
        public string Split_OpeningEnd { get; set; }
        public string Split_ClosingStart { get; set; }
        public string Split_ClosingEnd { get; set; }


    }
}
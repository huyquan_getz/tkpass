﻿using Com.SmoovPOS.Entity.CBEntity;
using SmoovPOS.Entity.Entity;
using System;
using System.Collections.Generic;

namespace Com.SmoovPOS.Entity
{
    public class Order : CBBaseEntity
    {
        public string table { get { return "Order"; } }
        public Station station { get; set; }
        public string orderCode { get; set; }
        public string transactionID { get; set; }
        public string referenceCode { get; set; }
        public DateTime completedDate { get; set; }
        public int paymentStatus { get; set; }
        public int orderStatus { get; set; }
        public string remark { get; set; }
        public double discount { get; set; }
        public double roundAmount { get; set; }
        public double subTotal { get; set; }
        public double grandTotal { get; set; }
        public virtual List<ProductOrder> arrayProduct { get; set; }
        public RefundInfor refundInfo { get; set; }
        public virtual List<PaymentItem> arrayPayment { get; set; }
        public EmployeeInfor staff { get; set; }
        public Customer customerSender { get; set; }
        public Customer customerReceiver { get; set; }
        public int orderType { get; set; }
        public bool paymentSuccess { get; set; }
        public string QRCode { get; set; }
        public string orderBy { get; set; }
        public bool isCart { get; set; }
        public Inventory inventory { get; set; }
        public double totalTax { get; set; }
        public double roundAmnt { get; set; }
        public bool isCancelRequest { get; set; }
        public List<TaxValue> listTaxvalue { get; set; }
        public DeliverySetting serviceShipping { get; set; }
        public Instruction instruction { get; set; }
        public int isPOS { get; set; } //0: onl 1: pos
        public ServiceChargePOS serviceCharge { get; set; }
        public string CustomerDisplay
        {
            get
            {
                string customerDisplay = customerSender != null
                    ? (!string.IsNullOrWhiteSpace(customerSender.fullName)) ? customerSender.fullName :
                    (!string.IsNullOrWhiteSpace(customerSender.firstName) ? customerSender.firstName : "") + " " +
                    (!string.IsNullOrWhiteSpace(customerSender.lastName) ? customerSender.lastName : "") : "Walk-in";
                return customerDisplay;
            }
        }

        public string currency { get; set; }

        public virtual List<AdHoc> arrayAdhocProduct { get; set; }

        public DateTime paidDate { get; set; }

        public bool discountCart { get; set; }

        public DiscountProductItem discountCartItem { get; set; }

        public double discountCartValue { get; set; }
        public List<OrderStatusHistory> arrayStatusHistory { get; set; }
    }
    public class OrderStatusHistory
    {
        public int orderStatus { get; set; }
        public DateTime modifiedDate { get; set; }
        public string modifiedDateDisplay { get; set; }
    }
    public class EmployeeInfor
    {
        public virtual string _id { get; set; }
        public string fullName { get; set; }
        public int EmployeeId { get; set; }
        public int gender { get; set; }
        public string birthday { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string image { get; set; }
        public string AddressCombine { get; set; }
    }

    public class ServiceChargePOS
    {
        public double serviceChargeValue { get; set; }
        public string serviceChargeName { get; set; }
        public double serviceChargeRate { get; set; }
    }
}
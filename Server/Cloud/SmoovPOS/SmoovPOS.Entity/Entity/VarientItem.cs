﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class VarientItem : CBBaseEntity
    {
        public string sku { get; set; }
        public string title { get; set; }
        public string color { get; set; }
        public string size { get; set; }
        public int quantity { get; set; }
        //This field to store the quantity of variant item that existed in store
        public int quantityInStock { get; set; }
        //This field to store the customer's remark (additional / optional request) of variant item
        public string variantRemark { get; set; }
        //public string image { get; set; }
        public virtual ImageItem image { get; set; }
        public double price { get; set; }
        public string currency { get; set; }
        public double priceCompare { get; set; }
        public double priceCost { get; set; }
        public double weight { get; set; }
        public bool requireShipping { get; set; }
        public string inventory_policy { get; set; }
        public int masterInventory { get; set; }
        public int remindStock { get; set; }

        public string productID { get; set; }

        public List<VariantOption> VariantOptionList { get; set; }

        //This field to store the flag that determine this variant is checked to check out in myCart
        public bool isCheckedToCheckOut { get; set; }
    }
}
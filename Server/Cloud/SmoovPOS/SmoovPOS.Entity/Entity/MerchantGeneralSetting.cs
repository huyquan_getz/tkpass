﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class MerchantGeneralSetting : CBBaseEntity
    {
        public string table
        {
            get { return "MerchantGeneralSetting"; }
        }

        public string TimeZoneFullName { get; set; }

        public string TimeZone { get; set; }

        public string CurrencyFullName { get; set; }

        public string Currency { get; set; }

        public string GST { get; set; }
        
        public string CountOfBestSellerProduct { get; set; }

        public string DailyReportPeriodBeginFrom { get; set; }

        public string DailyReportPeriodEndAt { get; set; }
        
        public bool isDailyReportPeriodEndAtNextDay { get; set; }

        public string MemberPointChangeRate { get; set; }
    }
}
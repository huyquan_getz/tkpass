﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class ProductItem : Product
    {
        public ProductItem(Product product, bool isMaster)
        {
            // TODO: Complete member initialization
            base._id = product._id;
            base.description = product.description;
            base.nameShort = product.nameShort;
            base.categoryId = product.categoryId;
            base.categoryName = product.categoryName;
            base.productType = product.productType;
            base.vendor = product.vendor;
            base.image = product.image;
            base.price = product.price;
            base.priceCompare = product.priceCompare;
            base.priceCost = product.priceCost;
            base.sku = product.sku;
            base.requireShipping = product.requireShipping;
            base.weight = product.weight;
            base.inventoryPolicy = product.inventoryPolicy;
            base.productMultiple = product.productMultiple;
            base.arrayInventory = product.arrayInventory;
            base.arrayVarient = product.arrayVarient;
            for (int i = 0; i < base.arrayVarient.Count(); i++)
            {
                if (!isMaster && product.inventoryPolicy.Equals("101"))
                {
                    base.arrayVarient[i].quantityInStock = base.arrayVarient[i].quantity = 0;
                }
                else
                {
                    int qtyVariant = product.masterInventory;
                    base.arrayVarient[i].quantityInStock = base.arrayVarient[i].quantity = qtyVariant;
                }
                base.arrayVarient[i].status = false;
            }
            base.masterInventory = product.masterInventory;
            base.display = product.display;
            base.name = product.name;
            base.title = product.title;
            base.excerpt = product.excerpt;
            base.editorNote = product.editorNote;
            base.sizeNFit = product.sizeNFit;
            base.measurement = product.measurement;
            base.unit = product.unit;
            base.limit = product.limit;
            base.trackStock = product.trackStock;
            base.remindStock = product.remindStock;
            base.minimumStock = product.minimumStock;
            base.table = "ProductItem";
            if (isMaster)
            {
                base.status = product.status;
            }
            else
            {
                base.status = false;
            }
            base.countOfSoldItem = product.countOfSoldItem;
        }
        public ProductItem(ProductItem productItem)
        {
            // TODO: Complete member initialization
            base._id = productItem._id;
            base.description = productItem.description;
            base.nameShort = productItem.nameShort;
            base.categoryId = productItem.categoryId;
            base.categoryName = productItem.categoryName;
            base.productType = productItem.productType;
            base.vendor = productItem.vendor;
            base.image = productItem.image;
            base.price = productItem.price;
            base.priceCompare = productItem.priceCompare;
            base.priceCost = productItem.priceCost;
            base.sku = productItem.sku;
            base.requireShipping = productItem.requireShipping;
            base.weight = productItem.weight;
            base.inventoryPolicy = productItem.inventoryPolicy;
            base.productMultiple = productItem.productMultiple;
            base.arrayInventory = productItem.arrayInventory;
            base.arrayVarient = new List<VarientItem>();
            base.masterInventory = productItem.masterInventory;
            base.display = productItem.display;
            base.name = productItem.name;
            base.title = productItem.title;
            base.excerpt = productItem.excerpt;
            base.editorNote = productItem.editorNote;
            base.sizeNFit = productItem.sizeNFit;
            base.measurement = productItem.measurement;
            base.unit = productItem.unit;
            base.limit = productItem.limit;
            base.trackStock = productItem.trackStock;
            base.remindStock = productItem.remindStock;
            base.minimumStock = productItem.minimumStock;
            base.table = "ProductItem";
            base.status = productItem.status;
            base.countOfSoldItem = productItem.countOfSoldItem;

            store = productItem.store;
            productID = productItem.productID;
            inStores = productItem.inStores;
            index = productItem.index;
            discount = productItem.discount;
            DiscountProductItem = productItem.DiscountProductItem;
        }
        public ProductItem() {}

        [Key]
        public string store { get; set; }
        public string productID { get; set; }
        public string inStores { get; set; }
        public int index { get; set; }
        public bool discount { get; set; }
        public DiscountProductItem DiscountProductItem { get; set; }
        public List<CollectionProductItem> ListCollection { get; set; }
    }
}
﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class PaymentItem : CBBaseEntity
    {
      
        public string paymentType { get; set; }
        public double charged { get; set; }
        public string approvedCode { get; set; }
        public string referenceCode { get; set; }
        public string table
        {
            get { return "PaymentItem"; }
        }
    }
}
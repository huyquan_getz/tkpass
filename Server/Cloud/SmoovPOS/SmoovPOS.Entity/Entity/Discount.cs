﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Discount : CBBaseEntity
    {
        public string table
        {
            get { return "Discount"; }
        }

        public string name { get; set; }
        public int type { get; set; }
        public double discountValue { get; set; }
        public double discountLimit { get; set; }
        public DateTime beginDate { get; set; }
        public DateTime endDate { get; set; }
        public List<string> ListProductItems { get; set; }
        public bool active { get; set; }
        public bool neverExpired { get; set; }
    }
}
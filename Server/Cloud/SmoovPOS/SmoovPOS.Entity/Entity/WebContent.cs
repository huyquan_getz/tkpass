﻿using Com.SmoovPOS.Entity.CBEntity;
using System.Collections.Generic;

namespace Com.SmoovPOS.Entity
{
    public class WebContent : CBBaseEntity
    {
        public string table
        {
            get { return "WebContent"; }
        }

        public string aboutUs { get; set; }
        public string contactUs { get; set; }
        public string faqs { get; set; }
        public string shippingInformation { get; set; }
        public string returnAndExchange { get; set; }
        public string privacyPolicy { get; set; }
        public string termAndCondition { get; set; }
        public string logo { get; set; }
        public List<SlideImage> slideImage {get;set;}
        public SocialNetwork socialNetworkLink { get; set; }
        public string theme { get; set; }
        public string PrimarySiteAlias { get; set; }
        public string titleHomePage { get; set; }
        public Country[] countries { get; set; }
        public string siteID { get; set; }
        public string businessName { get; set; }
        public string currency { get; set; }

    }

    public class SlideImage
    {
        public string name { get; set; }
        public int priority { get; set; }
        public string urlImage { get; set; }
        public string size { get; set; }
        public int status { get; set; }
        public string targetLink { get; set; }
    }

    public class SocialNetwork {
        public string facebook { get; set; }
        public string twitter { get; set; }
        public string youtube { get; set; }
        public string googlePlus { get; set; }
    }
}
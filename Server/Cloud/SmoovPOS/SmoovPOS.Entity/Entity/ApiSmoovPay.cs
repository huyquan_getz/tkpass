﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.ApiSmoovPay
{
    public class ApiSmoovPay
    {
        public int status { get; set; }
        public string message { get; set; }
        public object result { get; set; }
    }
}
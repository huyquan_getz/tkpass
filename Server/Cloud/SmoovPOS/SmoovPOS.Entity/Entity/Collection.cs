﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Collection : CBBaseEntity
    {
        public string table
        {
            get { return "Collection"; }
        }

        public string name { get; set; }
        public string description { get; set; }
        //public System.Nullable<System.DateTime> beginDate { get; set; }
        //public System.Nullable<System.DateTime> endDate { get; set; }

        public string defaultSortProduct { get; set; }
        public string image { get; set; }
        public IEnumerable<SmallProductItem> arrayProduct { get; set; }
        public bool isNeverExpires { get; set; }
    }
}
﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class TaxItem : CBBaseEntity
    {
        public List<StoreInformation> storeInformation { get; set; }
       
        public double valueRate { get; set; }
        public string name { get; set; }
        public bool isPercent { get; set; }
    }
    public class StoreInformation
        {
            public string storeName{get;set;}
            public string storeID { get; set; }
            public int index { get; set; }
            public double valueRate { get; set; }
        }
}
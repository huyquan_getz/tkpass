﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class WishList : CBBaseEntity
    {
        public string table
        {
            get { return "WishList"; }
        }
        public virtual List<ProductItem> arrayProductItem { get; set;}
        public string email { get; set; }
    }
}
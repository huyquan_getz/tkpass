﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Entity.CBEntity;
using System.Collections.Generic;

namespace Com.SmoovPOS.Entity
{
    public class MyCart : CBBaseEntity
    {
        public string table
        {
            get { return "MyCart"; }
        }
        //Email = Username -> determine member's cart
        public string email { get; set; }
        //List of products in member's cart
        public List<ProductItem> listProductItems { get; set; }
        //Remark
        public string remark { get; set; }
        //Receipt value of member's cart
        public ReceiptValue receiptValue { get; set; }
    }
}
﻿using Com.SmoovPOS.Entity.CBEntity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Category : CBBaseEntity
    {
        public string table
        {
            get { return "Category"; }
        }
        [DataType(DataType.MultilineText)]
        public string description { get; set; }
        [Required]
        [StringLength(30)]
        public string title { get; set; }
        public string product { get; set; }
        public string image { get; set; }
        public virtual List<CategoryProductItem> arrayProduct { get; set; }
    }

    public class CategoryDBContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
    }
}
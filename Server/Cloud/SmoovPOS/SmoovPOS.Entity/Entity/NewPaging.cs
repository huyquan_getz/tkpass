﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity
{
    public class NewPaging
    {
        public int total { get; set; }
        public int pageGoTo { get; set; }
        public int limit { get; set; }
        public string text { get; set; }
        public string sort { get; set; }
        public string type { get; set; }
        public bool desc { get; set; }
        bool check { get; set; }
        public int store { get; set; }

        public int firstKey
        {

            get
            {
                return pageGoTo + 1;
            }
        }
        public int secondKey
        {
            get
            {
                return pageGoTo + 2;

            }
        }

        public int previousKey
        {

            get
            {
                return pageGoTo - 1;
            }
        }
        public int olderKey
        {
            get
            {
                return pageGoTo - 2;

            }
        }

        public int totalFrom
        {
            get
            {
                if ((pageGoTo - 1) * limit <= total)
                {
                    return (pageGoTo - 1) * limit + 1;
                }
                else
                {
                    return total;
                }
            }

        }
        public int totalTo
        {
            get
            {
                if (pageGoTo * limit <= total)
                {
                    return pageGoTo * limit;
                }
                else
                {
                    return total;
                }
            }
        }

        public int LastPage
        {
            get
            {
                int LastOfPage = total / limit;
                int percent = total % limit;
                if (percent > 0)
                {
                    LastOfPage += 1;
                }

                return LastOfPage;

            }
        }

    }
}
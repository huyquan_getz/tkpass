﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Queue : CBBaseEntity
    {
        public string SubDomain { get; set; }
        public string BranchId { get; set; }
        public string BranchName { get; set; }
        public string ReservationUrl { get; set; }
        public string QRCodeThumb { get; set; }
        public string QRCodeSize1 { get; set; }
        public string QRCodeSize2 { get; set; }
        public string QRCodeSize3 { get; set; }
        public string QRCodeSize4 { get; set; }
        public string QRCodeSize5 { get; set; }

        public string table
        {
            get { return "Queue"; }
        }
    }
}
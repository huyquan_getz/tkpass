﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class SMSSetting : CBBaseEntity
    {
        public string table
        {
            get { return "SMSSetting"; }
        }
        public string ApiKey { get; set; }
        public string SecretCode { get; set; }
    }
}
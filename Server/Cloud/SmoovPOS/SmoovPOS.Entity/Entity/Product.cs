﻿using Com.SmoovPOS.Entity.CBEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class Product : CBBaseEntity
    {
        public string table{get; set;}
        public string description { get; set; }
        [Required]
        public string name { get; set; }
        public string nameShort { get; set; }
        public string categoryId { get; set; }
        public string categoryName { get; set; }
        public string productType { get; set; }
        public string vendor { get; set; }
        public ImageItem image { get; set; }
        public double price { get; set; }
        public double priceCompare { get; set; }
        public double priceCost { get; set; }
        public string sku { get; set; }
        public bool requireShipping { get; set; }
        public double weight { get; set; }
        public string inventoryPolicy { get; set; }
        public bool productMultiple { get; set; }
        public virtual List<InventoryItem> arrayInventory { get; set; }
        public virtual List<VarientItem> arrayVarient { get; set; }
        public int masterInventory { get; set; }
        public string title { get; set; }
        public string excerpt { get; set; }
        public string editorNote { get; set; }
        public string sizeNFit { get; set; }
        public string measurement { get; set; }
        public string unit { get; set; }
        public int limit { get; set; }
        public string trackStock { get; set; }
        public int remindStock { get; set; }
        public string minimumStock { get; set; }
        public int countOfSoldItem { get; set; }
    }

    public class CategoryProductItem
    {
        public string description { get; set; }
        public string name { get; set; }
        public string idProduct { get; set; }
       
    }

    public class SmallProductItem
    {
        public string name { get; set; }
        public string _id { get; set; }      
        public string urlImageProduct { get; set; }
        public string categoryName { get; set; }
        public string categoryId { get; set; }
        public DateTime createdDate { get; set; }
        public Boolean status { get; set; }
        public string productID { get; set; }
    }
}
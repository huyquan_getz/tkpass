﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Com.SmoovPOS.Entity.CBEntity;

namespace Com.SmoovPOS.Entity
{
    public class Currency : CBBaseEntity
    {
        public string table
        {
            get { return "Currency"; }
        }
        public List<CurrencyItem> listCurrency { get; set; }
    }

    public class CurrencyItem
    {
        public string country { get; set; }
        public string currencyFullName { get; set; }
        public string currency { get; set; }
        public string icon { get; set; }
    }
}
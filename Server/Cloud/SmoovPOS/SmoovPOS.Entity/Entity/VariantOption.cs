﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Com.SmoovPOS.Entity
{
    public class VariantOption
    {
        public int Index { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public List<SelectListItem> OptionNameList { get; set; }
    }
}
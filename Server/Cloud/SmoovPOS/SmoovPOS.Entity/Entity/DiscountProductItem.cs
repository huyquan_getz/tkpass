﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity
{
    public class DiscountProductItem
    {
        public string discountID { get; set; }
        public string name { get; set; }
        public int type { get; set; }
        public double discountValue { get; set; }
        public double discountLimit { get; set; }
        public DateTime beginDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.Entity.BaseEntity
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public interface ILanguageManager
    {
        string GetButtonText(string name);
        string GetLabelText(string name);
        string GetMessageText(string name);
    }
}
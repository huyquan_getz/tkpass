﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Com.SmoovPOS.Entity.CBEntity
{
    public abstract class CBBaseEntity
    {
        public virtual string id { get; set; }

        string idDoc;
        [Key]
        public virtual string _id
        {
            get
            {
                if (string.IsNullOrEmpty(idDoc))
                {
                    idDoc = id;
                }
                return idDoc;
            }
            set { idDoc = value; }
        }
        public DateTime createdDate { get; set; }
        public DateTime modifiedDate { get; set; }
        public string userID { get; set; }
        public string userOwner { get; set; }
        public Boolean status { get; set; }
        public bool display { get; set; }
        public string[] channels { get; set; }
        public string DateTimeInt { get; set; }
        public string bucket { get; set; }
    }
}
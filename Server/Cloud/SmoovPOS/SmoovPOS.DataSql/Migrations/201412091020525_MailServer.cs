namespace SmoovPOS.DataSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MailServer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MailConfig",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        IncomingPop = c.String(),
                        IncomingPort = c.Int(nullable: false),
                        UserName = c.String(),
                        PassWord = c.String(),
                        OutgoingSmtp = c.String(),
                        OutgoingPort = c.Int(nullable: false),
                        EnableSsl = c.Boolean(nullable: false),
                        Credentials = c.Boolean(nullable: false),
                        UserName1 = c.String(),
                        PassWord1 = c.String(),
                        UserName2 = c.String(),
                        PassWord2 = c.String(),
                        UserName3 = c.String(),
                        PassWord3 = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MailConfig");
        }
    }
}

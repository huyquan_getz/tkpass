﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmoovPOS.Background.Task
{
    public static class Tasks
    {
        public static List<TaskItem> TaskItems = new List<TaskItem>();
    }
    public class TaskItem
    {
        public string TableName { get; set; }
        public DateTime TimeToRun { get; set; }
        public string TimeZoneName { get; set; }
        public string Job { get; set; }
        public string DocumentId { get; set; }
        public string SiteId { get; set; }
        public string Error { get; set; }
    }
}

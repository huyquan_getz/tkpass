﻿using SmoovPOS.Background.Process;
using SmoovPOS.Business.Components;
using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceProcess;
using System.Threading;

namespace SmoovPOS.Background
{
    public partial class BackgroundService : ServiceBase
    {
        public BackgroundService()
        {
            InitializeComponent();
        }

        private ManualResetEvent _shutdownEvent = new ManualResetEvent(false);
        private Thread _thread;

        private System.Timers.Timer bgTimer;

        protected override void OnStart(string[] args)
        {
            try
            {
                _thread = new Thread(BackgroundThreadFunc);
                _thread.Name = "My Background Thread";
                _thread.IsBackground = true;
                _thread.Start();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void OnStop()
        {
            _shutdownEvent.Set();
            if (!_thread.Join(3000))
            {
                _thread.Abort();
            }
        }

        private void BackgroundThreadFunc()
        {

            while (!_shutdownEvent.WaitOne(0))
            {
                BackgroundProcess backgroundProcess = new BackgroundProcess();
                backgroundProcess.CheckStatusAndUpdate();
                Thread.Sleep(1000);
            }
        }
    }
}

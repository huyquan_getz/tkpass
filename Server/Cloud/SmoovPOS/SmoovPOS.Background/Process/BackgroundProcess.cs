﻿using Com.SmoovPOS.Business.Components;
using Com.SmoovPOS.Entity;
using SmoovPOS.Business.Components;
using SmoovPOS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using SmoovPOS.Data;
using Couchbase.Management;
using Couchbase.Configuration;
using System.IO;
using SmoovPOS.Background.Task;

namespace SmoovPOS.Background.Process
{
    public class BackgroundProcess
    {
        public BackgroundProcess() { }

        private void ActiveDiscount(IEnumerable<Discount> discounts, DateTime nowUTC, string siteId)
        {
            try
            {
                DiscountComponent wsdiscount = new DiscountComponent();

                // IEnumerable<Discount> discounts = wsdiscount.GetAllDiscount(Common.ConstantSmoovs.CouchBase.DesigndocDiscount, Common.ConstantSmoovs.CouchBase.ViewAllDiscount, siteId);
                // Discount discount = discounts.Where(d => d.beginDate <= nowUTC && d.endDate >= nowUTC && d.status == false).FirstOrDefault();
                discounts = discounts.Where(d => d.beginDate <= nowUTC && d.endDate >= nowUTC);
                foreach (Discount discount in discounts)
                {
                    if (!discount.status)
                    {
                        discount.status = true;
                        wsdiscount.UpdateDiscount(discount, siteId);
                    }

                }
                //UpdateProductItemsFollowDiscount(discounts.Select(d => d.ListProductItems), siteId);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Co loi bo qua chay tiep cac bucket / document khac
            }
        }

        private void InactiveDiscount(IEnumerable<Discount> discounts, DateTime nowUTC, string siteId)
        {
            try
            {
                DiscountComponent wsdiscount = new DiscountComponent();
                //IEnumerable<Discount> discounts = wsdiscount.GetAllDiscount(Common.ConstantSmoovs.CouchBase.DesigndocDiscount, Common.ConstantSmoovs.CouchBase.ViewAllDiscount, siteId);
                //Discount discount = discounts.Where(d => (d.beginDate > nowUTC || d.endDate < nowUTC) && d.status == true).FirstOrDefault();
                discounts = discounts.Where(d => (d.beginDate > nowUTC || d.endDate < nowUTC));
                foreach (Discount discount in discounts)
                {
                    if (discount.status)
                    {
                        discount.status = false;
                        wsdiscount.UpdateDiscount(discount, siteId);
                    }
                    //UpdateProductItemsFollowDiscount(discount, siteId);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Co loi bo qua chay tiep cac bucket / document khac
            }
        }

        private void UpdateProductItemsFollowDiscount(List<string[]> productIdDiscount, string siteId)
        {
            try
            {
                ProductComponent productComponent = new ProductComponent();
                IEnumerable<Product> products = productComponent.GetAllProductActive(ConstantSmoovs.CouchBase.DesignDocProduct, ConstantSmoovs.CouchBase.GetAllProduct, siteId);

                ProductItemComponent productItemComponent = new ProductItemComponent();

                foreach (string productID in products.Select(p => p._id))
                {
                    //string productID = productItemComponent.DetailProductItem(_productItemID, siteId).productID;
                    IEnumerable<Com.SmoovPOS.Entity.ProductItem> listproductItem = productItemComponent.GetAllProductItemByIDProduct(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, productID, siteId);
                    if (listproductItem.Count() > 0)
                    {
                        foreach (Com.SmoovPOS.Entity.ProductItem productItem in listproductItem)
                        {
                            //productItem.discount = discount.status;
                            //if (productItem.discount)
                            //{
                            //    if (productItem.DiscountProductItem == null) productItem.DiscountProductItem = new DiscountProductItem();
                            //    productItem.DiscountProductItem.beginDate = discount.beginDate;
                            //    productItem.DiscountProductItem.discountID = discount._id;
                            //    productItem.DiscountProductItem.discountLimit = discount.discountLimit;
                            //    productItem.DiscountProductItem.discountValue = discount.discountValue;
                            //    productItem.DiscountProductItem.endDate = discount.endDate;
                            //    productItem.DiscountProductItem.name = discount.name;
                            //    productItem.DiscountProductItem.type = discount.type;
                            //}
                            //else
                            //{
                            //    productItem.DiscountProductItem = null;
                            //}
                            productItemComponent.UpdateProductItem(productItem, siteId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Co loi bo qua chay tiep cac bucket / document khac
            }
        }

        private string UpdateProductItemsFollowDiscount(Discount discount, string siteId)
        {
            try
            {
                ProductComponent productComponent = new ProductComponent();
                ProductItemComponent productItemComponent = new ProductItemComponent();

                foreach (string productItemId in discount.ListProductItems)
                {
                    string productID = productItemComponent.DetailProductItem(productItemId, siteId).productID;

                    IEnumerable<Com.SmoovPOS.Entity.ProductItem> listproductItem = productItemComponent.GetAllProductItemByIDProduct(ConstantSmoovs.CouchBase.DesignDocProductItem, ConstantSmoovs.CouchBase.GetAllByProductID, productID, siteId);

                    if (listproductItem.Count() > 0)
                    {
                        foreach (Com.SmoovPOS.Entity.ProductItem productItem in listproductItem)
                        {
                            productItem.discount = discount.status;
                            if (productItem.discount)
                            {
                                if (productItem.DiscountProductItem == null) productItem.DiscountProductItem = new DiscountProductItem();
                                productItem.DiscountProductItem.beginDate = discount.beginDate;
                                productItem.DiscountProductItem.discountID = discount._id;
                                productItem.DiscountProductItem.discountLimit = discount.discountLimit;
                                productItem.DiscountProductItem.discountValue = discount.discountValue;
                                productItem.DiscountProductItem.endDate = discount.endDate;
                                productItem.DiscountProductItem.name = discount.name;
                                productItem.DiscountProductItem.type = discount.type;
                            }
                            else
                            {
                                productItem.DiscountProductItem = null;
                            }
                            productItemComponent.UpdateProductItem(productItem, siteId);
                        }
                    }
                }
                return "DONE";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Co loi bo qua chay tiep cac bucket / document khac
                return "ERROR: " + ex.Message;
            }
        }

        internal void CheckStatusAndUpdate()
        {
            try
            {
                List<string> collections = new List<string>();
                collections.Add(Common.ConstantSmoovs.CouchBase.DesigndocDiscount);

                var config = new CouchbaseClientConfiguration();
                config.Urls.Add(new Uri(Com.SmoovPOS.Data.DAL.ConfigureKeys.CouchBaseServer));
                config.Username = Com.SmoovPOS.Data.DAL.ConfigureKeys.UserCouchBase;
                config.Password = Com.SmoovPOS.Data.DAL.ConfigureKeys.PasswordCouchBase;
                var cluster = new CouchbaseCluster(config);
                string[] listBucket = cluster.ListBuckets().Select(l => l.Name).ToArray();

                //MerchantManagementComponent merchantManagementComponent = new MerchantManagementComponent();
                //var listBucket = merchantManagementComponent.GetAllSiteID();

                GetAllTaskFromAllSite(collections, listBucket);

                while (Tasks.TaskItems.Count > 0)
                {
                    TaskItem taskitem = Tasks.TaskItems.FirstOrDefault();
                    if (taskitem.TimeToRun <= DateTime.UtcNow)
                    {
                        switch (taskitem.Job)
                        {
                            case "DiscountUpdate":
                                DiscountComponent discountComponent = new DiscountComponent();
                                Discount discount = discountComponent.DetailDiscount(taskitem.DocumentId, taskitem.SiteId);
                                bool discountStatus = discount.beginDate > DateTime.UtcNow || discount.endDate < DateTime.UtcNow ? false : true;
                                if (discount.status != discountStatus)
                                {
                                    discount.status = discountStatus;
                                    discountComponent.UpdateDiscount(discount, taskitem.SiteId);
                                    if (UpdateProductItemsFollowDiscount(discount, taskitem.SiteId) == "DONE")
                                    {
                                        Tasks.TaskItems.Remove(taskitem);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Co loi bo qua chay tiep cac bucket / document khac
            }
        }

        private void GetAllTaskFromAllSite(List<string> collections, string[] listBucket)
        {
            foreach (string siteId in listBucket)
            {
                DateTime nowUTC = DateTime.UtcNow;
                foreach (string collection in collections)
                {
                    switch (collection)
                    {
                        case Common.ConstantSmoovs.CouchBase.DesigndocDiscount:
                            DiscountComponent wsdiscount = new DiscountComponent();
                            IEnumerable<Discount> discounts = wsdiscount.GetAllDiscount(Common.ConstantSmoovs.CouchBase.DesigndocDiscount, Common.ConstantSmoovs.CouchBase.ViewAllDiscount, siteId).Where(d => d.beginDate >= nowUTC || d.endDate >= nowUTC);
                            foreach (Discount discount in discounts)
                            {
                                Tasks.TaskItems.Add(new TaskItem() { DocumentId = discount._id, Job = "DiscountUpdate", SiteId = siteId, TableName = Common.ConstantSmoovs.CouchBase.DesigndocDiscount, TimeToRun = discount.beginDate < nowUTC ? discount.endDate : discount.beginDate });
                            }
                            break;
                    }
                }
            }
            Tasks.TaskItems = Tasks.TaskItems.OrderBy(t => t.TimeToRun).ToList();
        }

        private void UpdateDiscount()
        {

        }
    }
}

﻿using Com.SmoovPOS.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Utility.WSSiteReference;
using System;
using System.Linq;

namespace SmoovPOS.UnitTest
{
    [TestClass]
    public class AccountControllerTest
    {
        [TestMethod]
        public void RegisterSiteAndSiteAlias()
        {
            WSSiteClient client = new WSSiteClient();

            SiteModel site = new SiteModel();
            site.SiteID = "Store1";
            site.Name = "Store1";
            //site.ExpiryDate = siteViewModel.ExpiryDate;
            site.UserRegistration = "1";
            site.Type = "Online";

            client.WSCreateSite(site);
        }

        [TestMethod]
        public void CreateSiteAlias()
        {
            WSSiteClient client = new WSSiteClient();
            string httpAlias = "http://store1.demo.smoovpos-dev.com/";

            var siteAliases = client.WSGetAllSiteAlias();
            if (siteAliases.Any(r => r.HTTPAlias == httpAlias))
            {
                return;
            }

            SiteAliasModel siteAlias = new SiteAliasModel();
            siteAlias.SiteAliasID = "Store1";
            siteAlias.SiteID = "Store1";
            siteAlias.HTTPAlias = httpAlias;
            siteAlias.IsPrimary = true;

            client.WSCreateSiteAlias(siteAlias);
        }

        [TestMethod]
        public void GetSiteID()
        {
            WSSiteClient client = new WSSiteClient();
            var siteAliases = client.WSGetAllSiteAlias();

            string url = "http://store1.demo.smoovpos-dev.com/online";

            if (url.IndexOf("://", StringComparison.Ordinal) != -1)
            {
                url = url.Remove(0, url.IndexOf("://", StringComparison.Ordinal) + 3);
            }
            if (url.IndexOf("\\\\", StringComparison.Ordinal) != -1)
            {
                url = url.Remove(0, url.IndexOf("\\\\", StringComparison.Ordinal) + 2);
            }

            url = url.Split('.')[0];

            foreach (var item in siteAliases)
            {
                if (item.HTTPAlias.Contains(url))
                {
                    string siteID = item.SiteID;
                }
            }
        }
    }
}

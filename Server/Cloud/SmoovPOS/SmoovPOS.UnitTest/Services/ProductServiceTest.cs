﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Com.SmoovPOS.Product;
using System.Collections.Generic;
using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Common;
using System.Collections;
using SmoovPOS.Common.Controllers;
using SmoovPOS.UnitTest.Services;

namespace Com.SmoovPOS.UnitTest.Product
{
    [TestClass]
    public class ProductServiceTest
    {
        [TestMethod]
        public void WSCreateProduct()
        {
            Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
            product._id = Guid.NewGuid().ToString();
            string siteID = "smoovpos";
            product.userID = "001";
            product.userOwner = "Merchant_001";

            product.createdDate = DateTime.Today;
            product.modifiedDate = DateTime.Today;
            product.description = "description";
            product.name = "HTC TEST";
            product.nameShort = "HTC";
            product.status = false;
            product.display = true;
            string Category = "03c59955-130a-4ac3-a54a-bd591399dafe~category_69";
            int lenghtOfName = Category.Length - 37;
            product.categoryId = Category.Substring(0, 36);
            product.categoryName = Category.Substring(37, lenghtOfName);

            product.productType = "";
            product.vendor = "";
            //============= Chu y sua lai
            //  product.image = "";

            ImageItem image = new ImageItem();
            image.imageDetault = "0";
            List<string> listImage = new List<string>();
            listImage.Add("http://icons.iconarchive.com/icons/designcontest/ecommerce-business/128/mobile-icon.png");
            listImage.Add("http://icons.iconarchive.com/icons/designcontest/ecommerce-business/128/mobile-icon.png");
            image.arrayImage = new List<string>();
            image.arrayImage = listImage;
            product.image = new ImageItem();
            product.image = image;
            //=============
            product.price = 10.00;

            product.priceCompare = 12.00;
            product.priceCost = 9.00;

            product.sku = "htc";

            product.requireShipping = false;
            product.productMultiple = false;
            product.weight = 0.00;
            product.inventoryPolicy = "";

            // Require of SMOOV
            product.title = "HTC TEST";
            product.excerpt = "";
            product.editorNote = "";
            product.sizeNFit = "";
            product.measurement = "";
            product.unit = "";
            product.limit = 0;
            product.trackStock = "2";
            product.remindStock = 0;

            product.minimumStock = "";
            product.masterInventory = 20;

            //================== DU LIEU GIA
            InventoryItem inventory = new InventoryItem();
            inventory.name = "Size";
            List<string> listStr = new List<string>();
            listStr.Add("M");
            listStr.Add("XL");
            inventory.options = listStr;
            product.arrayInventory = new List<InventoryItem>();
            product.arrayInventory.Add(inventory);

            product.arrayVarient = new List<VarientItem>();
            VarientItem varient = new VarientItem();
            varient.sku = product.sku;

            varient.title = product.name;
            varient.color = "Red";
            varient.size = "M";
            varient.quantity = 10;
            //varient.image = "";
            varient.image = new ImageItem();
            varient.image = product.image;
            varient.price = 2;
            varient.priceCost = 1;
            varient.weight = 0;
            varient.requireShipping = product.requireShipping;
            varient.inventory_policy = "0";
            varient.masterInventory = 10;
            varient.remindStock = product.remindStock;
            varient.productID = product._id;
            varient._id = "001";


            varient.createdDate = product.createdDate;
            varient.currency = "$";
            varient.modifiedDate = product.modifiedDate;
            varient.userID = product.userID;
            varient.userOwner = product.userOwner;

            product.arrayVarient.Add(varient);
            VarientItem varient2 = new VarientItem();
            varient2.sku = product.sku;

            varient2.title = product.name;
            varient2.color = "Red";
            varient2.size = "M";
            varient2.quantity = 10;
            //varient2.image = "";
            varient2.image = new ImageItem();
            varient2.image = product.image;
            varient2.price = 2;
            varient2.priceCost = 1;
            varient2.weight = 0;
            varient2.requireShipping = product.requireShipping;
            varient2.inventory_policy = "0";
            varient2.masterInventory = 10;
            varient2.remindStock = product.remindStock;
            varient2.productID = product._id;
            varient2._id = "002";
            varient2.createdDate = product.createdDate;
            varient2.currency = "$";
            varient2.modifiedDate = product.modifiedDate;
            varient2.userID = product.userID;
            varient2.userOwner = product.userOwner;
            product.arrayVarient.Add(varient2);


            WSProduct WSProduct = new WSProduct();
            int expected = 0;
            int actual = WSProduct.WSCreateProduct(product, siteID);
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }
        [TestMethod]
        public void WSCreateProduct_()
        {
            Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
            product._id = Guid.NewGuid().ToString();
            string siteID = "smoovpos";
            product.userID = "001";
            product.userOwner = "Merchant_001";

            product.createdDate = DateTime.Today;
            product.modifiedDate = DateTime.Today;
            product.description = "description";
            product.name = "HTC TEST";
            product.nameShort = "HTC";
            product.status = false;

            string Category = "03c59955-130a-4ac3-a54a-bd591399dafe~category_69";
            int lenghtOfName = Category.Length - 37;
            product.categoryId = Category.Substring(0, 36);
            product.categoryName = Category.Substring(37, lenghtOfName);
            product.display = true;
            product.productType = "";
            product.vendor = "";
            //============= Chu y sua lai
            //  product.image = "";

            ImageItem image = new ImageItem();
            image.imageDetault = "0";
            List<string> listImage = new List<string>();
            listImage.Add("http://icons.iconarchive.com/icons/designcontest/ecommerce-business/128/mobile-icon.png");
            listImage.Add("http://icons.iconarchive.com/icons/designcontest/ecommerce-business/128/mobile-icon.png");
            image.arrayImage = new List<string>();
            image.arrayImage = listImage;
            product.image = new ImageItem();
            product.image = image;
            //=============
            product.price = 10.00;

            product.priceCompare = 12.00;
            product.priceCost = 9.00;

            product.sku = "htc";

            product.requireShipping = false;
            product.productMultiple = false;
            product.weight = 0.00;


            product.inventoryPolicy = "";




            // Require of SMOOV
            product.title = "HTC TEST";
            product.excerpt = "";
            product.editorNote = "";
            product.sizeNFit = "";
            product.measurement = "";
            product.unit = "";
            product.limit = 0;
            product.trackStock = "2";
            product.remindStock = 0;

            product.minimumStock = "";
            product.masterInventory = 20;

            //================== DU LIEU GIA
            InventoryItem inventory = new InventoryItem();
            inventory.name = "Size";
            List<string> listStr = new List<string>();
            listStr.Add("M");
            listStr.Add("XL");
            inventory.options = listStr;
            product.arrayInventory = new List<InventoryItem>();
            product.arrayInventory.Add(inventory);

            product.arrayVarient = new List<VarientItem>();
            VarientItem varient = new VarientItem();
            varient.sku = product.sku;

            varient.title = product.name;
            varient.color = "Red";
            varient.size = "M";
            varient.quantity = 10;
            //varient.image = "";
            varient.image = new ImageItem();
            varient.image = product.image;
            varient.price = 2;
            varient.priceCost = 1;
            varient.weight = 0;
            varient.requireShipping = product.requireShipping;
            varient.inventory_policy = "0";
            varient.masterInventory = 10;
            varient.remindStock = product.remindStock;
            varient.productID = product._id;
            varient._id = "001";


            varient.createdDate = product.createdDate;
            varient.currency = "$";
            varient.modifiedDate = product.modifiedDate;
            varient.userID = product.userID;
            varient.userOwner = product.userOwner;

            product.arrayVarient.Add(varient);
            VarientItem varient2 = new VarientItem();
            varient2.sku = product.sku;

            varient2.title = product.name;
            varient2.color = "Red";
            varient2.size = "M";
            varient2.quantity = 10;
            //varient2.image = "";
            varient2.image = new ImageItem();
            varient2.image = product.image;
            varient2.price = 2;
            varient2.priceCost = 1;
            varient2.weight = 0;
            varient2.requireShipping = product.requireShipping;
            varient2.inventory_policy = "0";
            varient2.masterInventory = 10;
            varient2.remindStock = product.remindStock;
            varient2.productID = product._id;
            varient2._id = "002";
            varient2.createdDate = product.createdDate;
            varient2.currency = "$";
            varient2.modifiedDate = product.modifiedDate;
            varient2.userID = product.userID;
            varient2.userOwner = product.userOwner;
            product.arrayVarient.Add(varient2);

            WSProduct WSProduct = new WSProduct();
            string expected = product._id;
            string actual = WSProduct.WSCreateProduct_(product, siteID);
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }
        [TestMethod]
        public void WSDetailProduct()
        {
            string siteID = "smoovpos";
            Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
            WSProduct WSProduct = new WSProduct();
            string _id = "001";
            product = WSProduct.WSDetailProduct(_id, siteID);
            Assert.IsNull(product);
        }

        [TestMethod]
        public void WSUpdateProduct()
        {
            string siteID = "smoovpos";
            Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
            WSProduct WSProduct = new WSProduct();
            int expected = -1;
            int actual = WSProduct.WSUpdateProduct(product, siteID);
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }
        [TestMethod]
        public void WSDeleteProduct()
        {
            string siteID = "smoovpos";
            Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
            WSProduct WSProduct = new WSProduct();
            string _id = "0000000";
            int expected = 1;
            int actual = WSProduct.WSDeleteProduct(_id, siteID);
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }

        [TestMethod]
        public void WSGetAllProduct()
        {
            string siteID = "smoovpos";
            Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
            WSProduct WSProduct = new WSProduct();
            string designDoc = "product";
            string viewName = "get_all_product";
            int listproduct = WSProduct.WSCountAllProduct(designDoc, viewName, siteID);
            Assert.IsNotNull(listproduct);
        }

        //[TestMethod]
        //public void UpdateStatus()
        //{

        //    Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
        //    WSProduct WSProduct = new WSProduct();
        //    string _id = "20939507-d843-4bf6-a0dd-d7db82d31f6d";    
        //    product = WSProduct.WSDetailProduct(_id);
        //    product.status = false;
        //    int response = WSProduct.WSUpdateProduct(product);
        //    Assert.AreEqual(response, 0, "The expected value did not match  the actual value");
        //}

        //[TestMethod]
        //public void updateStatusArray()
        //{
        //    string[] arrId = new string[] { "2ead51da-d4c9-47e9-9a25-64fb36ef9a5d", "39677f14-e2f8-4bc4-8a48-65fb28a8702e" };
        //    WSProduct client = new WSProduct();
        //    foreach (string id in arrId)
        //    {
        //        Com.SmoovPOS.Entity.Product objProduct = client.WSDetailProduct(id);
        //        objProduct.status = false;
        //        int expectResult = 0;
        //        int response = client.WSUpdateProduct(objProduct);
        //        Assert.AreEqual(response, expectResult, "The expected value did not match  the actual value");
        //    }
        //}
        //[TestMethod]
        //public void DeleteProduct() {
        //    string _id = "bfc40e63-ccf4-4607-9879-c70e4fe603f2";   

        //    Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
        //    WSProduct WSProduct = new WSProduct();
        //    int response = WSProduct.WSDeleteProduct(_id);
        //    Assert.AreEqual(response, 0, "The expected value did not match  the actual value");
        //}

        [TestMethod]
        public void DeleteVarient()
        {
            string siteID = "smoovpos";
            Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();

            WSProduct WSProduct = new WSProduct();
            product = WSProduct.WSDetailProduct("ae727173-b1c8-46d2-aa16", siteID);
            
            if (product != null)
            {
                string variantID = product.arrayVarient.FirstOrDefault()._id;
                if (variantID != null)
                {
                    product.arrayVarient.RemoveAll(a => a._id == variantID);
                    int result = WSProduct.WSUpdateProduct(product, siteID);
                    Assert.AreEqual(result, 0, "The expected value did not match  the actual value");
                }
            }
        }

        [TestMethod]
        public void AddVarientInEditProduct(string productID)
        {
            Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
            string siteID = "smoovpos";
            WSProduct WSProduct = new WSProduct();
            product = WSProduct.WSDetailProduct(productID, siteID);

            VarientItem varient = new VarientItem();
            varient.color = "";
            varient.title = "";
            varient.size = "";
            varient.price = 0.0;
            varient.priceCompare = 0.0;
            varient.priceCost = 0.0;
            varient.sku = "";
            varient.weight = 0.0;

            List<VarientItem> arrVarient_ = product.arrayVarient;
            if (arrVarient_ != null && arrVarient_.Count > 0)
                arrVarient_.Insert(arrVarient_.Count, varient);
            else 
                arrVarient_.Add(varient);
            int result = WSProduct.WSUpdateProduct(product, siteID);
            Assert.AreEqual(result, 1, "The expected value did not match  the actual value");
        }

        [TestMethod]

        public void EditVarient(string productID, string varientID)
        {
            Com.SmoovPOS.Entity.Product product = new Com.SmoovPOS.Entity.Product();
            string siteID = "smoovpos";
            WSProduct WSProduct = new WSProduct();
            product = WSProduct.WSDetailProduct(productID, siteID);

            List<VarientItem> arrVarient = product.arrayVarient;

            VarientItem varientItem = arrVarient.Find(a => a._id == varientID);

            varientItem.color = "";
            varientItem.title = "";
            varientItem.size = "";
            varientItem.price = 0.0;
            varientItem.priceCompare = 0.0;
            varientItem.priceCost = 0.0;
            varientItem.sku = "";
            varientItem.weight = 0.0;

            int result = WSProduct.WSUpdateProduct(product, siteID);
            Assert.AreEqual(result, 1, "The expected value did not match  the actual value");
        }

//---------------- Testing to generate varient algorithm --------------------
     
        [TestMethod]
        public void generate()
        {

            int n = 3;
            ArrayList list = new ArrayList();
            ArrayList listResult = new ArrayList();
            for (int i = 0; i < n; i++)
            {

                if (i == 0)
                    list.Insert(0, new List<string>()
	             {
	                 "QUynh",
	                 "Phuong",
	                });
                else if (i == 1)
                {
                    list.Insert(1, new List<string>()
	             {
	                 "hung",
	                  "zin",
	                });
                }
                else
                {
                    list.Insert(2, new List<string>()
                 {
                     "CHUONG",
                      "VU",
                    });
                }


            }
            VarientGenerateController varient = new VarientGenerateController();
            listResult = varient.generate(3, list);
            Assert.IsNotNull(listResult);

        }
        //---------------- End Testing to generate varient algorithm --------------------
    }
}

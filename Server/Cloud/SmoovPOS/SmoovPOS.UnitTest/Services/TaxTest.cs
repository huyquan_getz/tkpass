﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Com.SmoovPOS.Entity;
using System.Collections;
using SmoovPOS.Utility.WSTaxReference;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class TaxTest
    {
        [TestMethod]
        public void AddTax()
        {
            WSTaxClient client = new WSTaxClient();
            string siteId = "smoovpos";
            Tax tax = new Tax();
            
            List<ServiceCharge> listServiceCharge = new List<ServiceCharge>();
            ServiceCharge service = new ServiceCharge();
            service.index = 1;
            Inventory inventory = new Inventory();
            inventory._id = "234234";
            service.store = inventory;
            service.serviceChargeRate = 10;
            service.serviceChargeValue = 10;

            listServiceCharge.Add(service);
            tax.arrServiceCharge = listServiceCharge;
            // add list item tax
            List<TaxItem> listTaxItem = new List<TaxItem>();
            TaxItem taxItem = new TaxItem();
            taxItem.name = "GST";
            taxItem.isPercent = true;
            taxItem.valueRate = 10;
            List<StoreInformation> listInfor = new List<StoreInformation>();
            StoreInformation info = new StoreInformation();
            info.storeID = "a6587179-3011-48d5-843e-2681806db1f1";
            info.index = 1;
            info.storeName = "Online";
            listInfor.Add(info);
            taxItem.storeInformation = listInfor;
            listTaxItem.Add(taxItem);
            tax.arrayTax = listTaxItem;
            tax.createdDate = DateTime.UtcNow;
            tax.modifiedDate = DateTime.UtcNow;
            tax.display = true;
            tax.status = true;
            tax._id = Guid.NewGuid().ToString();
           // tax._id = "aba5518a-2b82-4cca-9055-f5ccaf4c4fee";
            int actual = client.WSCreateTax(tax,siteId);
            client.Close();
            int expected = 0;
            Assert.AreEqual(expected,actual, "The expected value did not match  the actual value");


        }
        [TestMethod]
        public void GetTaxWithInventory(){
            //string designDoc ="setting_merchant";
            //string viewName = "get_all_tax_item";
            //string siteId ="tax";
            //string inventoryId = "a6587179-3011-48d5-843e-2681806db1f1";
            //WSTaxClient client = new WSTaxClient();
            //IEnumerable<TaxItem> listTax = client.GetTaxWithInventory(designDoc, viewName, siteId, inventoryId);
            //foreach (TaxItem d in listTax)
            //{
            //    Console.Write(d.isPercent);
            //    Console.Write(d.valueRate);
            //    Console.Write(d.name);
            //}
            //client.Close();
            //Assert.IsNotNull(listTax);
        }
         [TestMethod]
        public void GetTax()
        {
            string designDoc = "setting_merchant";
            string viewName = "get_all_tax";
            string siteId = "smoovpos";
            WSTaxClient client = new WSTaxClient();
            IEnumerable<Tax> listTax = client.GetAllTax(designDoc, viewName, siteId);
            foreach (var d in listTax)
            {
                Console.WriteLine("{0}","sss");
            }
            client.Close();
            Assert.IsNotNull(listTax);
        }

    }
}

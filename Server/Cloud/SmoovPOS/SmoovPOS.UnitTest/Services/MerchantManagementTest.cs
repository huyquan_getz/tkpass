﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Entity.Models;
using SmoovPOS.Utility.WSMerchantManagementReference;
using Com.SmoovPOS.Model;
namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class MerchantManagementTest
    {
        [TestMethod]
        public void ConnectToSQL()
        {

        }

        [TestMethod]
        public void LoadDataFromSQL()
        {
            // call connect
            // test load data
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();

            //Delete all order first
            var results = client.WSGetAllMerchantManagement();

            //int expected = 0;
            //int returnCode = 1;
            //Assert.AreEqual(expected, returnCode, "not match");
            Assert.IsNotNull(results, "fails");
        }

        [TestMethod]
        public void LoadMerchantManagement()
        {
            Guid id = new Guid("1A424748-C500-422E-A59E-F529292B17BC");
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();

            var result = client.WSGetMerchantManagement(id);

            Assert.IsNotNull(result, "fails");
        }

        [TestMethod]
        public void ChangeStatusMerchantManagement()
        {
            Guid id = new Guid("1A424748-C500-422E-A59E-F529292B17BC");
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();

            var result = client.WSChangeStatusMerchantManagement(id, true);

            Assert.AreEqual(result, true, "fails");
        }
        [TestMethod]
        public void DeleteMerchantManagement()
        {
            Guid id = new Guid("1A424748-C500-422E-A59E-F529292B17BC");
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();

            var result = client.WSDeleteMerchantManagement(id);

            Assert.AreEqual(result, true, "fails");
        }

        [TestMethod]
        public void CreateMerchantManagement()
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            for (int i = 0; i < 100; i++)
            {
                int j = i + 5;

                var model = new MerchantManagementModel();


                model.firstName = string.Format("Merchant {0}", j);
                model.lastName = j.ToString();
                model.fullName = string.Format("Merchant {0} {0}", j);
                model.gender = false;
                model.birthday = DateTime.Now.AddYears(-20);
                model.phone = "00004";
                model.address = "Ong Ich Khiem";
                model.city = "Da Nang";
                model.zipCode = "59000";
                model.region = "Viet Nam";
                model.email = string.Format("merchant{0}@smoovpos.com", j);
                model.userID = "697a0a11-c760-4111-a320-dbed2eb0a601";
                model.title = "Mr";
                model.businessName = string.Format("Merchant {0}", j);
                model.servicePacketID = new Guid("9a5029b4-7639-47ae-bc2e-bda4cc7189d0");
                model.servicePacketStartDate = DateTime.Now.AddDays(-10);
                model.servicePacketEndDate = DateTime.Now.AddDays(10);
                model.verifyLink = "http://mzdjxp.axshare.com/#p=login_page";
                //model.status = "TRUE";
                model.bucket = "smoovpos";
                //model.userOwner = "7A5029B4-7639-47AE-BC2E-BDA4CC7189D0";
                //model.createdDate = "00:00.0";
                //model.modifyUser = "";
                //model.modifyDate = "04:40.0";
                //model.display = "TRUE";

                var result = client.WSCreateMerchantManagement(model);

                Assert.IsNotNull(result, "fails");


            }
        }

        [TestMethod]
        public void UpdateMerchantManagement()
        {
            IWSMerchantManagementClient client = new IWSMerchantManagementClient();
            var model = new MerchantManagementModel();

            model.merchantID = new Guid("acbe7f06-7e31-44a1-a0e7-21c8b8cede7f");
            model.firstName = "Merchant 4";
            model.lastName = "4";
            model.fullName = "Merchant 4 4";
            model.gender = false;
            model.birthday = DateTime.Now.AddYears(-20);
            model.phone = "00004";
            model.address = "Ong Ich Khiem";
            model.city = "Da Nang";
            model.zipCode = "59000";
            model.region = "Viet Nam";
            model.email = "merchant4@smoovpos.com";
            model.userID = "697a0a11-c760-4111-a320-dbed2eb0a601";
            model.title = "Mr";
            model.businessName = "Merchant 1";
            model.servicePacketID = new Guid("9a5029b4-7639-47ae-bc2e-bda4cc7189d0");
            model.servicePacketStartDate = DateTime.Now.AddDays(-10);
            model.servicePacketEndDate = DateTime.Now.AddDays(10);
            model.verifyLink = "http://mzdjxp.axshare.com/#p=login_page";
            model.status = true;
            model.bucket = "smoovpos";
            //model.userOwner = "7A5029B4-7639-47AE-BC2E-BDA4CC7189D0";
            //model.createdDate = "00:00.0";
            //model.modifyUser = "";
            //model.modifyDate = "04:40.0";
            //model.display = "TRUE";

            var result = client.WSUpdateMerchantManagement(model);

            Assert.AreEqual(result, true, "fails");
        }
    }
}

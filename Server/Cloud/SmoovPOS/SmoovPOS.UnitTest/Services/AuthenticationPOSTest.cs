﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.UnitTest.WSAuthenticationPOSReference;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class AuthenticationPOSTest
    {
        [TestMethod]
        public void TestMethod1()
        {
        }
        [TestMethod]
        public void CheckAuthenticationPOS()
        {
            // call connect
            // test load data
            WSAuthenticationPOSClient client = new WSAuthenticationPOSClient();

            var results = client.WSCheckAuthenticationPOS("smoovapp","merchant1@smoovpos.com","demo@123");

            Assert.IsNotNull(results, "fails");

            results = client.WSCheckAuthenticationPOS("smoovapp", "merchant1@smoovpos.com", "demo123");
            Assert.IsNotNull(results, "fails");
        }
    }
}

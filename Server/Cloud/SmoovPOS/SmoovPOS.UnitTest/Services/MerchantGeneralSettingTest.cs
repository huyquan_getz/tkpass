﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Business.Components;
using SmoovPOS.Utility.WSMerchantGeneralSettingReference;
using System;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class MerchantGeneralSettingTest
    {
        [TestMethod]
        public void TestMethod1()
        {
        }

        [TestMethod]
        public void GetDetailMerchantGeneralSetting()
        {
            string designDoc = "setting_merchant";
            string viewName = "get_merchant_general_setting";
            string siteId = "smoovpos";

            WSMerchantGeneralSettingClient client = new WSMerchantGeneralSettingClient();
            Com.SmoovPOS.Entity.MerchantGeneralSetting merchantGeneralSetting = client.GetDetailMerchantGeneralSetting(designDoc, viewName, siteId);
            client.Close();

            Assert.IsNotNull(merchantGeneralSetting);
        }

        [TestMethod]
        public void CreateMerchantGeneralSetting()
        {
            Com.SmoovPOS.Entity.MerchantGeneralSetting merchantGeneralSetting = new Com.SmoovPOS.Entity.MerchantGeneralSetting();
            merchantGeneralSetting._id = Guid.NewGuid().ToString();
            merchantGeneralSetting.createdDate = DateTime.Now;
            merchantGeneralSetting.modifiedDate = DateTime.Now;
            merchantGeneralSetting.status = false;
            merchantGeneralSetting.userID = "001";
            merchantGeneralSetting.userOwner = "merchant_001";
            merchantGeneralSetting.TimeZone = "GMT+7";
            merchantGeneralSetting.Currency = "VND";
            merchantGeneralSetting.GST = "1234";

            WSMerchantGeneralSettingClient client = new WSMerchantGeneralSettingClient();
            int actual = client.CreateNewMerchantGeneralSetting(merchantGeneralSetting, "");
            client.Close();

            int expect = 0;
            Assert.AreEqual(expect, actual, "The expected value did not match the actual value");
        }

        [TestMethod]
        public void UpdateMerchantGeneralSetting()
        {
            string designDoc = "setting_merchant";
            string viewName = "get_merchant_general_setting";
            string siteId = "smoovpos";

            WSMerchantGeneralSettingClient client = new WSMerchantGeneralSettingClient();
            Com.SmoovPOS.Entity.MerchantGeneralSetting merchantGeneralSetting = client.GetDetailMerchantGeneralSetting(designDoc, viewName, siteId);

            if (merchantGeneralSetting != null)
            {
                merchantGeneralSetting.TimeZone = "GMT+7";
                merchantGeneralSetting.Currency = "VND";
                merchantGeneralSetting.GST = "123456789";

                int actual = client.UpdateMerchantGeneralSetting(merchantGeneralSetting, "");
                client.Close();

                int expect = 0;
                Assert.AreEqual(expect, actual, "The expected value did not match the actual value");
            }
            else
            {
                client.Close();
                int expect = 0;
                Assert.AreEqual(expect, 1, "The expected value did not match the actual value");
            }
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Com.SmoovPOS.Entity;
using SmoovPOS.Utility.WSInstructionReference;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class InstructionTest
    {
        [TestMethod]
        public void CreateInstruction()
        {
            WSInstructionClient client = new WSInstructionClient();
            Instruction instruction = new Instruction();
            string siteId = "";
            instruction._id = Guid.NewGuid().ToString();
            instruction.createdDate = DateTime.Now;
            instruction.name = "For self collection, please visit our shop at 2 Orchard Link, Scape #02-07, Singapore 237978 between"; 
            int actual = client.WSCreateInstruction(instruction, siteId);
            client.Close();
            // Use the 'client' variable to call operations on the service.
            int expected = 0;
            Assert.AreEqual(expected, actual, "The expected value did not match  the actual value");
        }
    }
}

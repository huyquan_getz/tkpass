﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.UnitTest.WSCollectionReference;
using Com.SmoovPOS.Entity;
using System.Collections.Generic;
using Couchbase;
using SmoovPOS.Common;
namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class CollectionServiceTest
    {
        [TestMethod]
        public void CreateCollection()
        {
            string bucket = "smoovapp_14219795975512226";
            WSCollectionClient client = new WSCollectionClient();
          for (int i = 0; i<=10; i ++)
          {
            Collection collection = new Collection();
            collection._id = Guid.NewGuid().ToString();
            collection.createdDate = DateTime.UtcNow;
            collection.display = true;
            collection.status = true;
            collection.isNeverExpires = true;
            collection.name = "collection" + i.ToString();
            collection.modifiedDate = DateTime.UtcNow;
            collection.description = "description" + i.ToString();
            collection.DateTimeInt = "";
            //collection.beginDate = DateTime.UtcNow;
            //collection.endDate = DateTime.UtcNow;
            IEnumerable<SmallProductItem> arrayProduct = new List<SmallProductItem>();
            collection.arrayProduct = arrayProduct;

             int statusCode= client.WSCreateCollection(collection, bucket);
             Assert.AreEqual(0,statusCode,"fail");
          }
          client.Close();
        }

        [TestMethod]
        public void DetailCollection()
        {
            string bucket = "smoovapp_14219795975512226";
            WSCollectionClient client = new WSCollectionClient();
            string idCollection = "3cb85e40-b6c0-4b59-b160-95095891d626";
            Collection collect = client.WSDetailCollection(idCollection, bucket);
            client.Close();
            Assert.IsNotNull(collect,"failed");
        }

        [TestMethod]
        public void DeleteCollection()
        {
             string bucket = "smoovapp_14219795975512226";
            WSCollectionClient client = new WSCollectionClient();
            string idCollection = "00f9f451-82e0-4d79-addd-1d84322049b9";
            int statusCode = client.WSDeleteCollection(idCollection, bucket);
            client.Close();
            Assert.AreEqual(0, statusCode, "fail");
        }

        [TestMethod]
        public void UpdateCollection()
        {
            string bucket = "smoovapp_14219795975512226";
            WSCollectionClient client = new WSCollectionClient();
            string idCollection = "01a92a25-bf82-49a7-abd0-643f17a0d528";
            Collection collect = client.WSDetailCollection(idCollection, bucket);
            collect.name = "UPDATE";
            int statusCode = client.WSUpdateCollection(collect, bucket);
            client.Close();
            Assert.AreEqual(0, statusCode, "fail");
        }
        [TestMethod]
        public void GetAllCollection()
        {
            string bucket = "bucket_2_shadown";
            string designDoc ="collection";
            string viewName = "get_all_collection";
            WSCollectionClient client = new WSCollectionClient();
            var listCollect = client.WSGetAllCollection(designDoc, viewName, bucket);
            client.Close();
            Assert.IsNotNull(listCollect, "failed");
        }
        //[TestMethod]
        //public void DeleteAllCollection()
        //{
        //    WSCollectionClient client = new WSCollectionClient();

        //    string bucket = "smoovapp_14219795975512226";
           
        //    string designDoc = "collection";
        //    string viewName = "get_all_collection";

        //    int statusCode = client.WSDeleteAllCollection(designDoc, viewName, bucket);
        //    client.Close();
        //    Assert.AreEqual(0, statusCode, "fail");
        //}
    }
}

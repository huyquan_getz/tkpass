﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmoovPOS.Entity.Settings;
using SmoovPOS.UnitTest.WSCheckoutSettingReference;
using SmoovPOS.Common;
using Com.SmoovPOS.Entity;
using System.Collections.Generic;

namespace SmoovPOS.UnitTest.Services
{
    [TestClass]
    public class CheckoutSettingTest
    {
        
        [TestMethod]
        public void create()
        {
            CheckoutSetting checkout = new CheckoutSetting();
            checkout._id = Guid.NewGuid().ToString();
            checkout.status = true;
            checkout.userID = "001";
            checkout.display = true;
            checkout.userOwner = "merchant_001";
            checkout.acceptSelfCollection = true;
            checkout.acceptDelivery = true;
            checkout.beginDateSelf = "1";
            checkout.endDateSelf = "2";
            checkout.hourCutOff = "16:00";    
            checkout.instructionsDelivery = @"instructionsDelivery";
            checkout.instructionsSelfCollection = @"instructionsSelfCollection";
            checkout.timeDelivery = true;
            checkout.timeSelfCollection = true;
            checkout.typeCheckout = 1;
            checkout.isPickupPoint = true;
       //     checkout.listStoreInformation = new List<StoreInformation>();
            WSCheckoutSettingClient client = new WSCheckoutSettingClient();
            int actual = client.CreateCheckoutSetting(checkout, "smoovapp_14219795975512226");
            client.Close();
            int expect = 0;
            Assert.AreEqual(expect, actual, "The expected value did not match the actual value");
        }

        [TestMethod]
        public void updateDetail()
        {
            WSCheckoutSettingClient client = new WSCheckoutSettingClient();
            var checkoutSetting = client.GetDetailCheckoutSetting(ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetAllCheckoutSetting, "smoovapp_14219795975512226");
            int actual = client.UpdateCheckoutSetting(checkoutSetting, "smoovapp_14219795975512226");
             Assert.AreEqual(0, actual, "The expected value did not match the actual value");
        }
    }
}

﻿using Com.SmoovPOS.UI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using SmoovPOS.Business.Components;
using SmoovPOS.Common;
using SmoovPOS.Common.Controllers;
using SmoovPOS.Entity.Models;
using SmoovPOS.Utility.WSCheckoutSettingReference;
using SmoovPOS.Utility.WSEmployeeReference;
using SmoovPOS.Utility.WSInitCouchBaseReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SmoovPOS.API
{
    public class AuthenticationPOSController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: AuthenticationPOS
        //public void DoWork()
        //{
        //}
        public AuthenticationPOSController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AuthenticationPOSController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }
        public UserManager<ApplicationUser> UserManager { get; private set; }
        [HttpPost]
        public JsonResult WSCheckAuthenticationPOS(string businessName, string userName, string password, string deviceInfo)
        {
            try
            {
                logger.Info(string.Format("Authentication businessName={0}; userName={1}", businessName, userName));
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                if (!string.IsNullOrEmpty(businessName))
                {
                    //check exist Bussiness
                    var result = merchantManagement.GetMerchantManagementByBusinessName(businessName);
                    if (result != null)
                    {
                        userName = userName.Trim();
                        //employee login
                        var userNameEmployee = string.Format("{0}_{1}", result.bucket.Replace("-", ""), userName);
                        var user = UserManager.Find(userNameEmployee, password);
                        if (user != null)
                        {
                            WSEmployeeClient client = new WSEmployeeClient();
                            var employee = client.WSCheckByEmailPassword(ConstantSmoovs.CouchBase.DesignDocEmployee, ConstantSmoovs.CouchBase.CheckByEmailPassword, userName, VariableConfigController.MD5Hash(password), result.bucket);
                            client.Close();

                            if (employee != null)
                            {
                                return AuthenticationPOS(merchantManagement, result, user, deviceInfo);
                            }
                        }
                        else
                        {
                            //check login with merchant
                            user = UserManager.Find(userName, password);
                            if (user != null && user.Id == result.userID)
                            {
                                return AuthenticationPOS(merchantManagement, result, user, deviceInfo);
                            }
                        }

                        //MerchantInvalidEmail 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.MerchantInvalidEmail).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //MerchantWrongCredential 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.MerchantWrongCredential).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    //check login with merchant
                    var user = UserManager.Find(userName, password);
                    if (user != null)
                    {
                        var result = merchantManagement.GetMerchantManagementByUserId(user.Id);
                        return AuthenticationPOS(merchantManagement, result, user, deviceInfo);
                    }
                    else
                    {
                        //MerchantInvalidEmail 
                        var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.MerchantInvalidEmail).Key;
                        return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error("WSCheckAuthenticationPOS_Error", ex);
                //CommonNotFound 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonNotFound).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);

            }
        }


        [HttpPost]
        public JsonResult WSAddDeviceToken(string Authorization, string bucket, string deviceInfo)
        {
            try
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.GetUserSessionByToken(Authorization);
                if (authen != null)
                {
                    return AddDeviceToken(deviceInfo, bucket);

                }
                var statusCommonNotFound = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonNotFound).Key;
                return Json(new { status = statusCommonNotFound, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                logger.Error("WSCheckAuthenticationPOS_Error", ex);
                //CommonNotFound 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonNotFound).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);

            }
        }

        private JsonResult AddDeviceToken(string deviceInfo, string bucket)
        {
            try
            {
                WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
                var jsonInfoDevice = JsonConvert.DeserializeObject<DeviceTokenModel>(deviceInfo);
                jsonInfoDevice.status = true;
                jsonInfoDevice.token = jsonInfoDevice.deviceToken;

                var status = clientSetting.AddDeviceToken(bucket, ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetListDevice, jsonInfoDevice);
                clientSetting.Close();
                if (status >= 0)
                {
                    //Success 
                    var returnCode = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Success).Key;
                    return Json(new { status = returnCode, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var mess = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(m => m.Key == status).Value;
                    return Json(new { status = status, message = mess, result = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.InfomationDeviceNotFound).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.InfomationDeviceNotFound, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
            }
        }

        private JsonResult AuthenticationPOS(MerchantManagementComponent merchantManagement, Com.SmoovPOS.Model.MerchantManagementModel result, ApplicationUser user, string deviceInfo)
        {
            if (user != null)
            {
                var CouchBaseServer = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.CouchBaseServer];
                var authen = new AuthenticationPOSModel() { code = 1 };
                if (CouchBaseServer != null)
                {
                    authen.serverSync = CouchBaseServer.ToString().Split(':')[0] + ":" + CouchBaseServer.ToString().Split(':')[1];
                }
                authen.userAccessSync = string.Empty;
                authen.passAccessSync = string.Empty;
                authen.databaseSync = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.databaseSync];
                string access_token;
                long expires_in;
                merchantManagement.GeneralSession(result.userID, out access_token, out expires_in);
                authen.access_token = access_token;
                authen.expires_in = expires_in;
                authen.channels = new String[] { "app" };
                authen.merchantID = result.merchantID.ToString();
                authen.stores = new List<Store>();
                InventoryComponent inventoryComponent = new InventoryComponent();
                var listInventory = inventoryComponent.GetAll("inventory", "get_all_inventory", result.bucket);
                foreach (var item in listInventory)
                {
                    if (item.businessID.ToUpper() != ConstantSmoovs.Stores.Master.ToUpper() && item.index != ConstantSmoovs.Stores.OnlineStore)
                    {
                        var store = new Store();
                        store._id = item._id;
                        store.businessID = item.businessID;
                        store.index = item.index;
                        store.employeeID = result.merchantID.ToString();
                        store.channels = new String[] { item.index.ToString() };
                        authen.stores.Add(store);
                    }
                }
                WSCheckoutSettingClient clientSetting = new WSCheckoutSettingClient();
                try
                {
                    var jsonInfoDevice = new DeviceTokenModel();
                    try
                    {
                        jsonInfoDevice = JsonConvert.DeserializeObject<DeviceTokenModel>(deviceInfo);
                        jsonInfoDevice.status = true;
                        jsonInfoDevice.token = jsonInfoDevice.deviceToken;
                    }
                    catch (Exception ex)
                    {
                        logger.Error("WSCheckParseInfomationDevice_Error", ex);
                        var statusCheckParse = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.InfomationDeviceNotFound).Key;
                        return Json(new { status = statusCheckParse, message = ConstantSmoovs.API_RESPONSE_STATUS.InfomationDeviceNotFound, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
                    }

                    var status = clientSetting.AddDeviceToken(result.bucket, ConstantSmoovs.CouchBase.DesignDocSettingMerchant, ConstantSmoovs.CouchBase.GetListDevice, jsonInfoDevice);
                    clientSetting.Close();
                    if (status >= 0)
                    {
                        //Success 
                        return Json(new { status = authen.code, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = authen }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var mess = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(m => m.Key == status).Value;
                        return Json(new { status = status, message = mess, result = "" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("WSAuthenticationPOS_Error", ex);
                    var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.InfomationDeviceNotFound).Key;
                    return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.InfomationDeviceNotFound, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.MerchantInvalidEmail).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.MerchantInvalidEmail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public JsonResult WSResetTimeout(string Authorization)
        {
            if (!string.IsNullOrEmpty(Authorization))
            {
                MerchantManagementComponent merchantManagement = new MerchantManagementComponent();
                var authen = merchantManagement.ExtentionSessionByToken(Authorization);
                if (authen != null)
                {
                    return Json(new { status = authen.code, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = authen }, JsonRequestBehavior.AllowGet);
                }
            }

            //CommonNotFound 
            var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonNotFound).Key;
            return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> WSCreateBucket(string userName, string password, string bucketName = "", int port = 3456)
        {
            try
            {
                logger.Info(string.Format("Create Bucket userName={0}", userName));
                var status = -1;
                userName = userName.Trim();
                //check login with merchant
                var user = UserManager.Find(userName, password);
                //if (user == null)
                //{
                //    if (UserManager.Users.AsQueryable().Count() <= 0)
                //    {
                //        var user1 = new ApplicationUser() { UserName = userName, Email = password };
                //        IdentityResult result = await UserManager.CreateAsync(user1, password);
                //        if (result.Succeeded)
                //        {
                //            Com.SmoovPOS.UI.Models.ApplicationDbContext _db = new Com.SmoovPOS.UI.Models.ApplicationDbContext();
                //            //assign user to role admin
                //            ApplicationUserManager userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                //            //UserManager.AddToRole(user.Id, ConstantSmoovs.RoleNames.Admin);

                //            _db.AddUserToRole(userManager, user.Id, ConstantSmoovs.RoleNames.Admin);
                //        }
                //    }
                //}
                if (user != null && UserManager.IsInRole(user.Id, SmoovPOS.Common.ConstantSmoovs.RoleNames.Admin))
                {
                    //employee login
                    if (UserManager.IsInRole(user.Id, SmoovPOS.Common.ConstantSmoovs.RoleNames.Admin))
                    {
                        if (string.IsNullOrEmpty(bucketName))
                        {
                            bucketName = System.Configuration.ConfigurationManager.AppSettings[ConstantSmoovs.AppSettings.bucketSmoovPOS].ToString();
                        }
                        WSInitCouchBaseClient client = new WSInitCouchBaseClient();
                        try
                        {
                            client.CreateBucketCouchbase(bucketName, port);
                            Thread.Sleep(2000);
                            client.AutoGenerateViewCouchBase(bucketName);
                            client.Close();
                            status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.Success).Key;
                            return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Success, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception ex)
                        {
                            logger.Info(string.Format("Create Bucket", ex));
                            status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest).Key;
                            return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                //MerchantWrongCredential 
                status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonInvalidRequest).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error("WSCreateBucket_Error", ex);
                //CommonNotFound 
                var status = ConstantSmoovs.Enums.API_RESPONSE_STATUS.FirstOrDefault(r => r.Value == ConstantSmoovs.API_RESPONSE_STATUS.CommonNotFound).Key;
                return Json(new { status = status, message = ConstantSmoovs.API_RESPONSE_STATUS.Fail, result = (AuthenticationPOSModel)null }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
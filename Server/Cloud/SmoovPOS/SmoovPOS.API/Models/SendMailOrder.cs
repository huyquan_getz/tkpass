﻿using Com.SmoovPOS.Entity;
using Com.SmoovPOS.Model;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmoovPOS.API.Models
{
    public class SendMailOrder
    {    
        public string Bucket { get; set; }
        public MerchantSetting MerchantSetting { get; set; }
        public MerchantManagementModel MerchantManagementModel { get; set; }
    }

}
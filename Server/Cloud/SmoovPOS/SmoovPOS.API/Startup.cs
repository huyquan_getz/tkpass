﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(SmoovPOS.API.Startup))]
namespace SmoovPOS.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}

﻿using SmoovPOS.Utility.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models
{
    public class MerchantManagementViewModel
    {
        public System.Guid merchantID { get; set; }
        [LocalizedRequired]
        public string firstName { get; set; }
        [LocalizedRequired]
        public string lastName { get; set; }
        [LocalizedDisplayName("fullName")]
        public string fullName { get; set; }

        public System.Nullable<bool> gender { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("DoB")]
        //[Display(Name = "DoB")]
        public System.Nullable<System.DateTime> birthday { get; set; }
        [LocalizedRequired]
        [RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        //[RegularExpression(@"(^(\+?\-? *[0-9]+)([0-9 ]*)([0-9 ])*$)|(^ *$)", ErrorMessage = "Please input number only.")]
        [LocalizedDisplayName("Phone")]
        public string phone { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("Address")]
        public string address { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("city")]
        public string city { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("zipCode")]
        [RegularExpression(@"(^([0-9])*$)", ErrorMessage = "Please input number only.")]
        public string zipCode { get; set; }
        [LocalizedDisplayName("region")]
        public string region { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("country")]
        public string country { get; set; }

        [LocalizedRequired]
        [EmailAddress]
        //[LocalizedRegularExpression(@"(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*(;|,)\s*|\s*$))*", Name = "WrongEmail")]
        [LocalizedDisplayName("email")]
        [LocalizedRemote("IsExistedEmail", "MerchantManagement", AdditionalFields = "merchantID", ResourceCode = "EmailExisted")]
        public string email { get; set; }

        public string userID { get; set; }
        [LocalizedRequired]
        public string title { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("businessName")]
        [LocalizedRemote("IsExistedBusinessName", "MerchantManagement", AdditionalFields = "merchantID", ResourceCode = "BusinessNameExisted")]
        public string businessName { get; set; }
        [LocalizedRequired]
        public System.Nullable<System.Guid> servicePacketID { get; set; }
        //[LocalizedRequired]
        [LocalizedDisplayName("servicePacketStartDate")]
        public System.Nullable<System.DateTime> servicePacketStartDate { get; set; }
        [LocalizedDisplayName("servicePacketEndDate")]
        public System.Nullable<System.DateTime> servicePacketEndDate { get; set; }

        public string verifyLink { get; set; }
        [LocalizedDisplayName("Status")]
        public bool status { get; set; }

        public string bucket { get; set; }

        public string userOwner { get; set; }

        public System.DateTime createdDate { get; set; }

        public string modifyUser { get; set; }

        public System.Nullable<System.DateTime> modifyDate { get; set; }

        public bool display { get; set; }
        public int proxyPort { get; set; }
        public List<SelectListItem> servicePackets { get; set; }
        public List<SelectListItem> countrys { get; set; }
        public List<SelectListItem> titles { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("PhoneNumber")]
        [RegularExpression(@"(^(\+?\-? *[0-9]+)([0-9 ]*)([0-9 ])*$)|(^ *$)", ErrorMessage = "Please input number only.")]
        public string phoneRegion { get; set; }
        #region servicePacket
        [LocalizedDisplayName("servicePacketName")]
        public string servicePacketName { get; set; }
        #endregion

        public int expiredDate
        {
            get
            {
                int expiredDate = 0;
                if (servicePacketEndDate != null)
                {
                    expiredDate = (int)(servicePacketEndDate.Value - DateTime.Now).TotalDays;
                }

                return expiredDate;
            }
        }
        public bool isVerifyLink
        {
            get
            {
                return (string.IsNullOrEmpty(verifyLink));
            }
        }
        public bool isExpiredDate
        {
            get
            {
                if (servicePacketEndDate != null)
                {
                    return (expiredDate <= 7);
                }
                return false;
            }
        }

        [LocalizedDisplayName("BillingCycleName")]
        public string BillingCycleName { get; set; }

        public System.Nullable<System.Guid> billingCycleId { get; set; }
    }
}
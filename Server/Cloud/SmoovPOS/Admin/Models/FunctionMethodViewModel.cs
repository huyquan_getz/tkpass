﻿using SmoovPOS.Utility.CustomAttributes;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SmoovPOS.UI.Models
{
    public class FunctionMethodViewModel
    {
        public System.Guid functionMethodID { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("FunctionName")]
        [LocalizedRemote("IsExistedName", "FunctionMethod", AdditionalFields = "functionMethodID", ResourceCode = "NameExisted")]
        public string name { get; set; }
        [LocalizedRequired]
        [LocalizedDisplayName("keyMethod")]
        public string keyMethod { get; set; }
        [LocalizedDisplayName("ShortDescription")]
        public string description { get; set; }
        [LocalizedDisplayName("isDatabaseFunction")]
        public bool isDatabaseFunction { get; set; }
        [LocalizedDisplayName("dataType")]
        public string dataType { get; set; }

        public string userOwner { get; set; }

        public System.DateTime createdDate { get; set; }

        public string modifyUser { get; set; }

        public System.Nullable<System.DateTime> modifyDate { get; set; }

        public bool display { get; set; }
        public List<SelectListItem> dataTypes { get; set; }
        public List<SelectListItem> keyMethods { get; set; }
    }
}
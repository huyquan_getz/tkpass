﻿

using Com.SmoovPOS.Model;
using SmoovPOS.Business.Business;
using SmoovPOS.Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace admin.Controllers
{
    public class SettingController : Controller
    {
        //
        // GET: /Setting/
         [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            //WSPaymentGatewayClient client = new WSPaymentGatewayClient();
            //Com.SmoovPOS.Model.PaymentModel payment = client.GetPaymentGateway();
            //client.Close();
            ViewBag.Class = "payment";
            PaymentBusiness paymentBusiness = new PaymentBusiness();
            PaymentModel payment = paymentBusiness.GetPaymentGateway();
            return View(payment);
        }



        // POST: /Setting/Update
        [HttpPost]
        public ActionResult Update(FormCollection fc)
        {
            ViewBag.Class = "payment";
            try
            {
                // TODO: Add update logic here
                PaymentBusiness paymentBusiness = new PaymentBusiness();
                PaymentModel payment = new PaymentModel();
                string id = fc["Id"];
                if (id.Equals(""))
                {
                    payment.Id = Guid.NewGuid().ToString();
                    //payment.CreateDate = DateTime.UtcNow;
                }
                payment.Status = Int32.Parse(fc["typePay"].ToString());
                payment.SmoovUrl = fc["smoovUrl"];
                payment.SuccessPage = fc["successPage"];
                payment.CancelPage = fc["cancelPage"];
                payment.StrUrl = fc["strUrl"];

                //sandbox
                payment.SandboxSmoovUrl = fc["sandboxSmoovUrl"];

                payment.CreateDate = DateTime.UtcNow;

                payment.ModifiedDate = DateTime.UtcNow;
                paymentBusiness.AddOrUpdatePaymentGateway(id, payment);
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Index");
            }
        }
         [Authorize(Roles = "Admin")]
        public ActionResult MailServer()
        {
            ViewBag.Title = "Configure the mail server settings";
            ViewBag.Class = "mailserver";
            MailConfigBusiness mailConfig = new MailConfigBusiness();
            MailServerModel mail = mailConfig.GetConfigMailServer(null);
            return View(mail);
        }

        [HttpPost]
        public ActionResult UpdateMailServer(FormCollection fc)
        {
            ViewBag.Class = "mailserver";
            ViewBag.Title = "Configure the mail server settings";
            try
            {
                // TODO: Add update logic here
                MailConfigBusiness mailConfig = new MailConfigBusiness();
                MailServerModel mail = new MailServerModel();
                string id = fc["Id"];
                if (id.Equals(""))
                {
                    mail.Id = Guid.NewGuid().ToString();
                    //payment.CreateDate = DateTime.UtcNow;
                }
                mail.IncomingPop = fc["IncomingPop"];
                mail.IncomingPort = Int32.Parse(fc["IncomingPort"].ToString());
                mail.UserName = fc["UserName"];
                mail.PassWord = fc["PassWord"];
                mail.OutgoingSmtp = fc["OutgoingSmtp"];
                mail.OutgoingPort = Int32.Parse(fc["OutgoingPort"].ToString());
                //  mail.EnableSsl = Boolean.Parse(fc["EnableSsl"].ToString());
                bool check = false;
                try
                {
                    if (fc["EnableSsl"].Equals("on"))
                    {
                        check = true;
                    }
                }
                catch (Exception e)
                {
                    e.Message.ToString();
                }
                mail.EnableSsl = check;
                //mail.UserName1 = fc["UserName1"];
                //mail.PassWord1 = fc["PassWord1"];
                //mail.UserName2 = fc["UserName2"];
                //mail.PassWord2 = fc["PassWord2"];
                //mail.UserName3 = fc["UserName3"];
                //mail.PassWord3 = fc["PassWord3"];
                //mail.UserName3 = fc["UserName1"];
                //mail.PassWord3 = fc["PassWord1"];
                mail.CreateDate = DateTime.UtcNow;
                mail.merchantID = null;
                mail.ModifiedDate = DateTime.UtcNow;
                mailConfig.AddOrUpdateMailServer(id, mail, null);
                return RedirectToAction("MailServer");
            }
            catch
            {
                return View("MailServer");
            }
        }


        //public ActionResult DetailMailServer()
        //{
        //    return View("DetailMailServer");
        //}
    }
}

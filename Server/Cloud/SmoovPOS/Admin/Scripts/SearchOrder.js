﻿$(document).ready(function () {
    $('.OrderDatePicker').unbind();
    $('.OrderDatePicker').off();
    $('.OrderDatePicker').datetimepicker({
        autoclose: true,
        language: 'pt-BR'
    })
    .on('changeDate', function (e) {
        FilterOrder(e);
        $(this).datetimepicker('hide');
    });

    $('form.SearchForm').unbind();
    $('form.SearchForm').off();
    $('form.SearchForm').submit(function (e) {
        e.preventDefault();
        SearchOrderSubmit(e.currentTarget);
        return false;
    });
});

function FilterOrder(e) {
    //debugger;
    //var table = $('.tab-content .active table').DataTable();
    //if (table.rows().data().length > 0) {
        //table.search($(e.currentTarget).val()).draw();
    //} else {
        var form = $(e.currentTarget).parents("form")[0];
        SearchOrderSubmit(form);
    //}
}

function SearchOrderSubmit(form) {
    if ($(form).valid()) {
        $.ajax({
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            success: function (result) {
                $(".tab-content .active").html(result);
            },
            error: function (error) {
                //debugger;
            }
        });
    }

    return false;
}